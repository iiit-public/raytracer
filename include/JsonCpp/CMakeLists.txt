# Project
project(raytracer)

# Add source files
set(INC_JSON
    ${CMAKE_CURRENT_SOURCE_DIR}/json/json.h
    ${CMAKE_CURRENT_SOURCE_DIR}/json/json-forwards.h
    ${CMAKE_CURRENT_SOURCE_DIR}/jsoncpp.cpp
    PARENT_SCOPE
)