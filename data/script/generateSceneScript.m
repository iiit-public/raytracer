function generateSceneScript( fileName, saveName )

inFile = fopen(fileName);

if inFile == -1
    return;
end

outFile = fopen([saveName '.m'],'w');

fprintf(outFile,'%% Set base file name here\n');
fprintf(outFile,'baseName = ''json'';\n\n');
fprintf(outFile,'%% Set number of iterations here\n');
fprintf(outFile,'n = 1;\n\n');
fprintf(outFile,'%% Define parameters here\n');
fprintf(outFile,'%% para = [];\n\n');
fprintf(outFile,'%% Frontend parameters\n');
fprintf(outFile,'p = 8; %% Number of Threads\n');
fprintf(outFile,'w = 128; %% Chunk width\n');
fprintf(outFile,'h = 128; %% Chunk height\n\n');
fprintf(outFile,'%% Main loop\n');
fprintf(outFile,'for i = 1:n\n');
fprintf(outFile,'    %% Open file\n');
fprintf(outFile,'    filename = sprintf(''%%s%%d.json'',baseName,i);\n');
fprintf(outFile,'    fileID = fopen(filename,''w'');\n\n');
fprintf(outFile,'    %%%% Scene file\n    %% Adjust parameters and include parameter sweeps here\n');
tline = fgets(inFile);
while ischar(tline)
    tline = strrep(tline, sprintf('\n'), '');
    tline = strrep(tline, sprintf('\r'), '');
    fprintf(outFile,sprintf('    fprintf(fileID,''%s\\\\n'');\n',tline));
    tline = fgets(inFile);
end
fprintf(outFile,'    %%%% End of scene file\n');
fprintf(outFile,'    fclose(fileID);\n');
fprintf(outFile,'end\n\n');
fprintf(outFile,'if ispc\n');
fprintf(outFile,'    %% Write batch file\n');
fprintf(outFile,'    filename = sprintf(''%%s.bat'',baseName);\n');
fprintf(outFile,'    fileID = fopen(filename,''w'');\n');
fprintf(outFile,'    for i = 1:n\n');
fprintf(outFile,'        fprintf(fileID,''basicfrontend.exe %%s%%d.json -o %%s%%d.png -n -w %%d -h %%d -p %%d\\n'',baseName,i,baseName,i,w,h,p);\n');
fprintf(outFile,'    end\n');
fprintf(outFile,'    fclose(fileID);\n');
fprintf(outFile,'elseif isunix || ismac\n');
fprintf(outFile,'    %% Write shell script\n');
fprintf(outFile,'    filename = sprintf(''%%s.sh'',baseName);\n');
fprintf(outFile,'    fileID = fopen(filename,''w'');\n');
fprintf(outFile,'    for i = 1:n\n');
fprintf(outFile,'        fprintf(fileID,''./basicfrontend %%s%%d.json -o %%s%%d.png -n -w %%d -h %%d -p %%d\\n'',baseName,i,baseName,i,w,h,p);\n');
fprintf(outFile,'    end\n');
fprintf(outFile,'    fclose(fileID);\n');
fprintf(outFile,'    unix(sprintf(''chmod +x %%s'',filename));\n');
fprintf(outFile,'end\n');

fclose(outFile);
fclose(inFile);

end

