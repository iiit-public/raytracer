% PLY model generation
%
% Generates a 3D polygon model of a bumb in the PLY file format. The bumb folows a raised cosine
% wave. The surface is curved in order to be placed on top of a spherical surface.
clear variables;
close all;
clc;

%% specify parameters
dz = 10e-3; % max z deviation [m]
R = 0.5; % radius and -z position of sphere object

X = 0.05; % length of bump in x dimension [m]
Y = 0.05; % lenght of bump in y dimension [m]
r = 0.0005; % x,y-resolution of polygon grid [m]

fileName = sprintf('bump_dz_%f_R_%f.ply',dz,R);

R = max(R,max(X,Y));

if mod(X,r) ~= 0
    X = ceil(X/r) * r;
end
if mod(Y,r) ~= 0
    Y = ceil(Y/r) * r;
end

[x, y] = meshgrid(-Y/2:r:Y/2,-X/2:r:X/2);
z0 = sqrt(R^2 - x.^2 - y.^2) - R;
z = (cos(2 * pi / min(X,Y) * sqrt(x.^2 + y.^2)) + 1) * dz * 0.5;
z(sqrt(x.^2 + y.^2) > min(X,Y) / 2) = 0;
z = z + z0;

nVertices = (X / r + 1) * (Y / r + 1);
nFaces = 2 * (X / r) * (Y / r);

% surf(x,y,z);
% axis equal;

%% write header
fileID = fopen(fileName,'w');
fprintf(fileID,'ply\n');
fprintf(fileID,'format ascii 1.0\n');

% vertices
fprintf(fileID,'element vertex %d\n',nVertices);
fprintf(fileID,'property float x\n');
fprintf(fileID,'property float y\n');
fprintf(fileID,'property float z\n');

% faces
fprintf(fileID,'element face %d\n',nFaces);
fprintf(fileID,'property list int int vertex_indices\n');

% end of header
fprintf(fileID,'end_header\n');

%% write vertices
for u = 1:size(x,1)
    for v = 1:size(y,2)
        fprintf(fileID,'%10f %10f %10f\n',x(u,v),y(u,v),z(u,v));
    end
end

%% write faces
for i = 0:nVertices
    if mod(i + 1,(X / r + 1)) ~= 0 && i < nVertices - (X / r + 1)
        fprintf(fileID,'3 %d %d %d\n',i,i+1,i+X/r+2);
        fprintf(fileID,'3 %d %d %d\n',i,i+X/r+2,i+X/r+1);
    end
end

ptCloud = pcread(fileName);
pcshow(ptCloud);
xlabel('\itx \rmin m');
ylabel('\ity \rmin m');
zlabel('\itz \rmin m');
