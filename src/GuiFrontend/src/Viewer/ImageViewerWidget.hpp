// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file ImageViewerWidget.hpp
 *  \author Thomas Nuernberg
 *  \date 21.11.2014
 *  \brief Definition of class ImageViewerWindow.
 *
 *  Definition of class ImageViewerWindow for displaying images.
 */

#ifndef __IMAGEVIEWERWIDGET_HPP__
#define __IMAGEVIEWERWIDGET_HPP__

#include <QtWidgets>


namespace iiit
{

    /*! \brief Class to display image data.
     *
     *  Class derived from QGraphicsView to display image data to support additional mouse wheel
     *  zooming.
     */
    class ImageViewerWidget : public QGraphicsView
    {
        Q_OBJECT

    public:
        ImageViewerWidget(QWidget* parent = 0);
        ~ImageViewerWidget();

    private:
        virtual void wheelEvent(QWheelEvent* event);

    };

} // end of namespace iiit

#endif // __IMAGEVIEWERWIDGET_HPP__