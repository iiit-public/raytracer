// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file ImageViewerWidget.cpp
 *  \author Thomas Nuernberg
 *  \date 21.11.2014
 *  \brief Implementation of class ImageViewerWidget.
 *
 *  Implementation of class ImageViewerWindow for displaying images.
 */

#include "ImageViewerWidget.hpp"

#include <QDesktopWidget>



namespace iiit
{

    /*! \brief Default constructor.
     *	\param[in] parent Parent widget.
     */  
    ImageViewerWidget::ImageViewerWidget(QWidget* parent)
        : QGraphicsView(parent)
    {
        QDesktopWidget desktop;
        setMaximumSize(desktop.availableGeometry(this).size() * 0.7);
        setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
    }



    /*! \brief Default destructor.
     */ 
    ImageViewerWidget::~ImageViewerWidget()
    {
    }



    /*! \brief Reimplements mouse wheel event for zooming.
     *  \param[in] event Mouse wheel event.
     */
    void ImageViewerWidget::wheelEvent(QWheelEvent* event) {
 
        setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
 
        // Scale the view / do the zoom
        double scaleFactor = 1.25;
        if(event->delta() > 0) {
            // Zoom in
            scale(scaleFactor, scaleFactor);
        } else {
            // Zooming out
            scale(1.0 / scaleFactor, 1.0 / scaleFactor);
        }
    }

} // end of namespace iiit