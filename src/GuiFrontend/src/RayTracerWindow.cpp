// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file RayTracerWindow.cpp
 *  \author Thomas Nuernberg
 *  \date 21.11.2014
 *  \brief Implementation of class RayTracerWindow.
 *
 *  Implementation of class RayTracerWindow, the GUI of the ray tracer.
 */

#include "RayTracerWindow.hpp"

#include "GuiFrontend.hpp"
#include "Logger.hpp"
#include "Utilities/Constants.h"



namespace iiit
{

    /*! \brief Default constructor.
     *  \param[in] frontend Pointer to RayTracer frontend.
     *  \param[in] parent Parent widget.
     */  
    RayTracerWindow::RayTracerWindow(GuiFrontend* frontend, QWidget *parent)
        : QMainWindow(parent)
        , frontend_(frontend)
        , imageCounter_(0)
    {
        // central widget
        QWidget* centralWidget = new QWidget;

        // main layout
        QGridLayout* mainLayout = new QGridLayout;
        centralWidget->setLayout(mainLayout);

        // scene GroupBox
        QGroupBox* sceneBox = new QGroupBox(tr("Scene"));
        QGridLayout* sceneLayout = new QGridLayout;

        QLabel* sceneDescriptionLabel = new QLabel(tr("Scene description file:"));
        sceneLayout->addWidget(sceneDescriptionLabel, 0, 0);

        browseSceneButton_ = new QPushButton(tr("Browse"));
        connect(browseSceneButton_, &QPushButton::clicked, this, &RayTracerWindow::browseSceneDescription);
        sceneLayout->addWidget(browseSceneButton_, 0, 1);

        sceneDescriptionLine_ = new QLineEdit("../data/example_scene.json"); /// \todo Remove default.
        connect(sceneDescriptionLine_, &QLineEdit::returnPressed, frontend_, &GuiFrontend::run);
        sceneLayout->addWidget(sceneDescriptionLine_, 1, 0, 1, 2);

        //loadSceneButton_ = new QPushButton(tr("Load Scene"));
        //connect(loadSceneButton_, &QPushButton::clicked, this, &RayTracerWindow::loadSceneDescription);
        //sceneLayout->addWidget(loadSceneButton_, 2, 1);

        sceneBox->setLayout(sceneLayout);
        mainLayout->addWidget(sceneBox, 0, 0, 1, 3);

        //// properties GroupBox
        //QGroupBox* propertiesBox = new QGroupBox(tr("RayTracer Properties"));
        //QVBoxLayout* propertiesLayout = new QVBoxLayout;

        //// image resolution
        //QHBoxLayout* resolutionLayout = new QHBoxLayout;

        //QLabel* resolutionLabel = new QLabel(tr("Resolution:"));
        //resolutionLayout->addWidget(resolutionLabel);

        //uResLine_ = new SlimLineEdit;
        //uResLine_->setDisabled(true);
        //uResLine_->setAlignment(Qt::AlignRight);
        //resolutionLayout->addWidget(uResLine_);

        //QLabel* resolutionSeparatorLabel = new QLabel("x");
        //resolutionLayout->addWidget(resolutionSeparatorLabel);

        //vResLine_ = new SlimLineEdit;
        //vResLine_->setDisabled(true);
        //vResLine_->setAlignment(Qt::AlignRight);
        //resolutionLayout->addWidget(vResLine_);

        //propertiesLayout->addLayout(resolutionLayout);

        //QLabel* samplerLabel = new QLabel(tr("Sampler:"));
        //propertiesLayout->addWidget(samplerLabel);

        //samplerComboBox_ = new QComboBox();
        //samplerComboBox_->addItem(QString(tr("Regular")));
        //samplerComboBox_->addItem(QString(tr("PureRandom")));
        //samplerComboBox_->addItem(QString(tr("Jittered")));
        //samplerComboBox_->addItem(QString(tr("NRooks")));
        //samplerComboBox_->addItem(QString(tr("Multijittered")));
        //samplerComboBox_->addItem(QString(tr("Hammersley")));
        //samplerComboBox_->setDisabled(true);
        //propertiesLayout->addWidget(samplerComboBox_);

        //QHBoxLayout* numberOfSamplesLayout = new QHBoxLayout;
        //QLabel* numberOfSamplesLabel = new QLabel(tr("Number of Samples:"));
        //numberOfSamplesLayout->addWidget(numberOfSamplesLabel);

        //numberOfSamplesLine_ = new SlimLineEdit;
        //numberOfSamplesLine_->setText(QString("1"));
        //numberOfSamplesLine_->setDisabled(true);
        //numberOfSamplesLine_->setAlignment(Qt::AlignRight);
        //numberOfSamplesLayout->addWidget(numberOfSamplesLine_);

        //propertiesLayout->addLayout(numberOfSamplesLayout);

        //propertiesBox->setLayout(propertiesLayout);
        //mainLayout->addWidget(propertiesBox, 1, 0, 1, 3);

        // multithreading
        QGroupBox* multithreadingBox = new QGroupBox(tr("Multithreading"));
        QVBoxLayout* multithreadingLayout = new QVBoxLayout;

        multiThreadingCheckBox_ = new QCheckBox(tr("Enable Multithreading"));
        connect(multiThreadingCheckBox_, &QCheckBox::stateChanged, this, &RayTracerWindow::toggleMultiThreading);
        multithreadingLayout->addWidget(multiThreadingCheckBox_);

        // chunk size
        QHBoxLayout* chunkSizeLayout = new QHBoxLayout;

        QLabel* chunkSizeLabel = new QLabel(tr("Chunk Size:"));
        chunkSizeLayout->addWidget(chunkSizeLabel);

        chunkWidthLine_ = new SlimLineEdit;
        chunkWidthLine_->setText(QString("128")); /// \todo Remove default.
        chunkWidthLine_->setDisabled(true);
        chunkWidthLine_->setAlignment(Qt::AlignRight);
        chunkSizeLayout->addWidget(chunkWidthLine_);

        QLabel* chunkSizeSeparatorLabel = new QLabel("x");
        chunkSizeLayout->addWidget(chunkSizeSeparatorLabel);

        chunkHeightLine_ = new SlimLineEdit;
        chunkHeightLine_->setText(QString("128")); /// \todo Remove default.
        chunkHeightLine_->setDisabled(true);
        chunkHeightLine_->setAlignment(Qt::AlignRight);
        chunkSizeLayout->addWidget(chunkHeightLine_);

        multithreadingLayout->addLayout(chunkSizeLayout);

        // number of threads
        QHBoxLayout* numberOfThreadsLayout = new QHBoxLayout;

        QLabel* numberOfThreadsLabel = new QLabel(tr("Number of Threads:"));
        numberOfThreadsLayout->addWidget(numberOfThreadsLabel);

        numberOfThreadsLine_ = new SlimLineEdit;
        numberOfThreadsLine_->setText(QString("%1").arg(std::thread::hardware_concurrency()));
        numberOfThreadsLine_->setDisabled(true);
        numberOfThreadsLine_->setAlignment(Qt::AlignRight);
        numberOfThreadsLayout->addWidget(numberOfThreadsLine_);

        multithreadingLayout->addLayout(numberOfThreadsLayout);

        multithreadingBox->setLayout(multithreadingLayout);
        mainLayout->addWidget(multithreadingBox, 1, 0, 1, 3);

        // logging
        QGroupBox* loggingBox = new QGroupBox(tr("Logging"));
        QHBoxLayout* loggingLayout = new QHBoxLayout;

        QLabel* logLevelLabel = new QLabel(tr("Logging Level:"));
        loggingLayout->addWidget(logLevelLabel);

        // \todo consider compiler flag
        logLevelDropDown_ = new QComboBox;
        logLevelDropDown_->addItem("None");
        if (LOG_MAX_LEVEL >= LogLevel::LogError)
        {
            logLevelDropDown_->addItem("Error");
        }
        if (LOG_MAX_LEVEL >= LogLevel::LogWarning)
        {
            logLevelDropDown_->addItem("Warning");
        }
        if (LOG_MAX_LEVEL >= LogLevel::LogInfo)
        {
            logLevelDropDown_->addItem("Info");
        }
        if (LOG_MAX_LEVEL >= LogLevel::LogDebug)
        {
            logLevelDropDown_->addItem("Debug");
        }
        if (LOG_MAX_LEVEL >= LogLevel::LogDebugImage)
        {
            logLevelDropDown_->addItem("DebugImage");
        }
        if (LOG_MAX_LEVEL >= LogLevel::LogDebugChunk)
        {
            logLevelDropDown_->addItem("DebugChunk");
        }
        if (LOG_MAX_LEVEL >= LogLevel::LogDebugPixel)
        {
            logLevelDropDown_->addItem("DebugPixel");
        }
        if (LOG_MAX_LEVEL >= LogLevel::LogDebugRay)
        {
            logLevelDropDown_->addItem("DebugRay");
        }
        loggingLayout->addWidget(logLevelDropDown_);

        #if !LOG_MAX_LEVEL
            loggingBox->setDisabled(true);
        #endif

        loggingBox->setLayout(loggingLayout);
        mainLayout->addWidget(loggingBox, 2, 0, 1, 3);

        // start button
        startButton_ = new QPushButton(tr("Start"));
        connect(startButton_, &QPushButton::clicked, frontend_, &GuiFrontend::run);
        mainLayout->addWidget(startButton_, 3, 1);

        // abort button
        abortButton_ = new QPushButton(tr("Abort"));
        connect(abortButton_, &QPushButton::clicked, frontend_, &GuiFrontend::abort);
        abortButton_->setDisabled(true);
        mainLayout->addWidget(abortButton_, 3, 2);

        // pause button
        pauseButton_ = new QPushButton(tr("Pause"));
        connect(pauseButton_, &QPushButton::clicked, frontend_, &GuiFrontend::pause);
        pauseButton_->setDisabled(true);
        mainLayout->addWidget(pauseButton_, 4, 1);

        // resume button
        resumeButton_ = new QPushButton(tr("Resume"));
        connect(resumeButton_, &QPushButton::clicked, frontend_, &GuiFrontend::resume);
        resumeButton_->setDisabled(true);
        mainLayout->addWidget(resumeButton_, 4, 2);
        mainLayout->setRowStretch(5, 1);

        // image Viewer
        imageViewerLayout_ = new QHBoxLayout;
        createTabWidget();
        mainLayout->addLayout(imageViewerLayout_, 0, 3, 6, 1);
        createActions();
        createMenus();

        this->setCentralWidget(centralWidget);

        // window title
        setWindowTitle(tr("IIIT RayTracer"));

        //// initial window size
        //resize(600, 400);

        // set start button as default and set focus
        startButton_->setDefault(true);
        startButton_->setFocus(Qt::OtherFocusReason);

        // redirect std::cout console
        QTextEdit* console = new QTextEdit;
        mainLayout->addWidget(console, 6, 0, 1, 4);
        stdRedirector_ = std::shared_ptr<StdRedirector>(new StdRedirector(std::cout, console));

        // set up status bar
        statusBar()->showMessage(tr("Ready"));
        progressBar_ = new QProgressBar(this);
        progressBar_->setMinimum(0);
        statusBar()->addPermanentWidget(progressBar_);
        progressBar_->setVisible(false);
    }



    /*! \brief Default destructor.
     */
    RayTracerWindow::~RayTracerWindow()
    {
    }



    /*! \brief Get file name of scene description file.
     *  \return Returns content of the scene description line.
     */
    std::string RayTracerWindow::getSceneDescriptionFileName()
    {
        return sceneDescriptionLine_->text().toStdString();
    }



    /*! \brief Sets the scene description file.
     *  \param[in] sceneDescription Name of scene description file.
     */
    void RayTracerWindow::setSceneDescriptionFileName(std::string sceneDescription)
    {
        sceneDescriptionLine_->setText(QString(sceneDescription.c_str()));
    }




    /*! \brief Indicates whether to use multithreading.
     *  \return Return TRUE if multithreading should be used, FALSE otherwise.
     */
    bool RayTracerWindow::getMultiThreading() const
    {
        return multiThreadingCheckBox_->isChecked();
    }



    /*! \brief Sets whether to use multithreading.
     *  \param[in] multiThreading Flag indicating whether to use multithreading.
     */
    void RayTracerWindow::setMultiThreading(bool multiThreading)
    {
        multiThreadingCheckBox_->setChecked(multiThreading);
    }



    /*! \brief Get width of chunks in pixels.
     *  \return Returns 0 if conversion failed, else the pixel width is returned.
     */
    int RayTracerWindow::getChunkWidth() const
    {
        bool successfull;
        int chunckWidth = chunkWidthLine_->text().toInt(&successfull);
        if (successfull)
        {
            return chunckWidth;
        }
        else
        {
            return 0;
        }
    }



    /*! \brief Set width of chunks in pixels.
     *  \\param[in] chunkWidth Width of chunks in pixels.
     */
    void RayTracerWindow::setChunkWidth(int chunkWidth)
    {
        chunkWidthLine_->setText(QString("%1").arg(chunkWidth));
    }



    /*! \brief Get height of chunks in pixels.
     *  \return Returns 0 if conversion failed, else the pixel height is returned.
     */
    int RayTracerWindow::getChunkHeight() const
    {
        bool successfull;
        int chunckHeight = chunkHeightLine_->text().toInt(&successfull);
        if (successfull)
        {
            return chunckHeight;
        }
        else
        {
            return 0;
        }
    }



    /*! \brief Set height of chunks in pixels.
     *  \\param[in] chunkHeight Height of chunks in pixels.
     */
    void RayTracerWindow::setChunkHeight(int chunkHeight)
    {
        chunkHeightLine_->setText(QString("%1").arg(chunkHeight));
    }



    /*! \brief Get number of worker threads.
     *  \return Returns 1 if conversion failed, else the number of threads is returned.
     */
    size_t RayTracerWindow::getNumberOfThreads() const
    {
        bool successfull;
        int numberOfThreads = numberOfThreadsLine_->text().toUInt(&successfull);
        if (successfull)
        {
            return numberOfThreads;
        }
        else
        {
            return 1;
        }
    }



    /*! \brief Sets number of worker threads.
     *  \param[in] numberOfThreads Number of worker threads.
     */
    void RayTracerWindow::setNumberOfThreads(size_t numberOfThreads)
    {
        numberOfThreadsLine_->setText(QString("%1").arg(numberOfThreads));
    }



    /*! \brief Get logging level.
     *  \return Returns currently selected logging level LogLevel.
     */
    int RayTracerWindow::getLogLevel() const
    {
        return logLevelDropDown_->currentIndex();
    }



    /*! \brief Add image tab.
     *  \param[in] image Pointer to image object to display.
     *  \param[in] saveFlag Flag indicating whether image has been saved yet.
     *  \return Returns unique index of image to be able to call the updateImage() method for this image.
     *
     *  Method adds a new image tab to the viewer and displays the image. A unique image index is
     *  returned to be able to update this image later on.
     */
    int RayTracerWindow::addImage(std::shared_ptr<const cimg_library::CImg<float> > image, bool saveFlag)
    {
        // store image pointer
        std::shared_ptr<cimg_library::CImg<float> > imageCopy(new cimg_library::CImg<float>(*image));
        images_.push_back(imageCopy);

        // create tab widget
        QWidget* imageTab = new QWidget;
        QGridLayout* mainLayout = new QGridLayout;
        imageTab->setLayout(mainLayout);

        // create QGraphicsScene to contain image
        QGraphicsScene* graphicsScene = new QGraphicsScene;

        // display image
        showImage(graphicsScene, image);

        // save flag
        saveFlags_.push_back(saveFlag);

        // unique index
        imageIndices_.push_back(imageCounter_);
        ++imageCounter_;

        // update view actions
        updateViewActions();

        // create viewer area
        ImageViewerWidget* imageViewerWidget = new ImageViewerWidget();
        imageViewerWidget->setScene(graphicsScene);
        imageViewerWidget->setBackgroundBrush(Qt::SolidPattern);
        imageViewerWidget->setDragMode(QGraphicsView::ScrollHandDrag);
        imageViewerWidgets_.push_back(imageViewerWidget);

        // add widget to tab bar
        tabWidget_->addTab(imageTab, "");
        adjustTabLabel(tabWidget_->count() - 1, imageIndices_.back(), saveFlag);
        tabWidget_->setCurrentIndex(tabWidget_->count() - 1);

        // adjust zoom
        float factor = 1.f;
        while (static_cast<float>(graphicsScene->width()) * factor > static_cast<float>(imageViewerWidget->maximumWidth()) ||
            static_cast<float>(graphicsScene->height()) * factor > static_cast<float>(imageViewerWidget->maximumHeight()))
        {
            zoomOut();
            factor = factor * 0.8f;
        }

        // enable file actions
        updateFileActions();

        // display viewer area
        mainLayout->addWidget(imageViewerWidget, 0, 0);

        return imageIndices_.back();
    }

    

    /*! \brief Add new image tab and display empty image.
     *  \param[in] width Width of the image to display.
     *  \param[in] height Height of the image to display.
     *  \return Returns unique index of image to be able to call the updateImage() method for this image.
     *
     *  Adds a new image tab to the viewer and displays an empty image of the specified size. A unique
     *  image index is returned to be able to update this image later on.
     */
    int RayTracerWindow::initImage(int width, int height)
    {
        std::shared_ptr<cimg_library::CImg<float> > image = std::make_shared<cimg_library::CImg<float>>(width, height, 1, 3);
        image->fill(0.f);
        return addImage(image);
        
    }



    /*!
     *  \overload int RayTracerWindow::initImage(int width, int height)
     */
    int RayTracerWindow::initImage(const QSize& size)
    {
        return initImage(size.width(), size.height());
    }

    

    /*! \brief Update whole image of existing tab.
     *  \param[in] image Updated image to display.
     *  \param[in] imageIndex Unique index of image to update.
     *
     *  Updates a whole image of an existing image tab. The image is specified by the unique image
     *  index, that is returned by the addImage() or the initImage() method. If the image tab has
     *  already been closed, the tab is reopened with the updated image.
     */
    void RayTracerWindow::updateImage(std::shared_ptr<const cimg_library::CImg<float> > image, int imageIndex)
    {
        if (imageIndex >= imageCounter_)
        {
            // index has not been assigned yet
            return;
        }

        // find index of image to update
        std::vector<int>::iterator it = std::find(imageIndices_.begin(), imageIndices_.end(), imageIndex);

        if (it != imageIndices_.end())
        {
            // image found, update image
            int tabIndex = it - imageIndices_.begin();
            QGraphicsScene* scene = imageViewerWidgets_[tabIndex]->scene();
            scene->removeItem(scene->items().at(0));

            // display image
            showImage(scene, image);

            saveFlags_[tabIndex] = false;

            // adjust tab label
            adjustTabLabel(tabIndex, imageIndex, false);
        }
        else
        {
            // image not found, reopen image tab
            int tmpCounter = imageCounter_;
            imageCounter_ = imageIndex;
            addImage(image, false);
            imageCounter_ = tmpCounter;
        }
    }



    /*! \brief Update image chunk of existing tab.
     *  \param[in] image Updated image section to display.
     *  \param[in] region Region to update.
     *  \param[in] imageIndex Unique index of image to update.
     *
     *  Updates a region of an image of an existing image tab. The size of the original image is not
     *  changed. Therefore, given image region must fit in the original image.
     *
     *  The image to update is specified by the unique image index, that is returned by the addImage()
     *  or the initImage() method.
     *
     *  If the image tab has already been closed, the original image data is unknown and the image can
     *  not be updated.
     */
    void RayTracerWindow::updateChunk(std::shared_ptr<const cimg_library::CImg<float> > image, QRect region, int imageIndex)
    {
        if (imageIndex >= imageCounter_)
        {
            // index has not been assigned yet
            return;
        }

        // find index of image to update
        std::vector<int>::iterator it = std::find(imageIndices_.begin(), imageIndices_.end(), imageIndex);

        if (it != imageIndices_.end())
        {
            int tabIndex = it - imageIndices_.begin();
            QSize imageSize = getSize(tabIndex);

            // image found, check dimension
            if (region.x() >= 0 && region.x() < imageSize.width() &&
                region.y() >= 0 && region.y() < imageSize.height() &&
                region.width() <= imageSize.width() - region.x() &&
                region.height() <= imageSize.height() - region.y())
            {
                // dimensions match, update image section
                images_[tabIndex]->draw_image(region.x(), region.y(), *image);

                QGraphicsScene* scene = imageViewerWidgets_[tabIndex]->scene();
                scene->removeItem(scene->items().at(0));

                // display image
                showImage(scene, images_[tabIndex]);

                // adjust tab label
                adjustTabLabel(tabIndex, imageIndex, false);

                // repaint
                imageViewerWidgets_[tabIndex]->repaint();

                // update progress bar
                progressBar_->setValue(progressBar_->value() + 1);

#ifdef win_OS
                taskbarProgress_->setValue(progressBar_->value() + 1);
#endif
            }
        }
        else
        {
            // image not found, image can not be updated
            return;
        }
    }



    /*! \brief Show image in GUI
     *  \param[in] scene GraphicsScene pointer.
     *  \param[in] image Image to show.
     */
    void RayTracerWindow::showImage(QGraphicsScene* scene, std::shared_ptr<const cimg_library::CImg<float> > image)
    {
        // create QImage of matching size for display
        std::shared_ptr<QImage> qImage = std::make_shared<QImage>(image->width(), image->height(), QImage::Format_RGB32);

        // iterate pixels
        for (int u = 0; u < image->width(); ++u)
        {
            for (int v = 0; v < image->height(); ++v)
            {
                // copy pixel
                qImage->setPixel(u, v, qRgb(
                    static_cast<int>((*image)(u, v, 0) * TRUE_COLOR_MAX),
                    static_cast<int>((*image)(u, v, 1) * TRUE_COLOR_MAX),
                    static_cast<int>((*image)(u, v, 2) * TRUE_COLOR_MAX)));
            }
        }

        // add image
        scene->addPixmap(QPixmap::fromImage(*qImage));
    }



    /*! \brief Open image file.
     *
     *  Opens file dialog to select image file. Image is then displayed in new tab, with saveFlag set to
     *  true.
     */
    void RayTracerWindow::open()
    {
        // get image file name
        QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"), QDir::currentPath());

        if (!fileName.isEmpty())
        {
            // load image
            std::shared_ptr<cimg_library::CImg<float> > image = std::make_shared<cimg_library::CImg<float> >(fileName.toStdString().c_str());
            if (image->is_empty())
            {
                QMessageBox::information(this, tr("Image Viewer"), tr("Cannot load %1").arg(fileName));
                return;
            }

            // display image
            addImage(image, true);
        }
    }



    /*! \brief Close all image tabs.
     *  \return Returns TRUE if the tabs have closed successfully, otherwise FALSE.
     *
     *  Checks the save flags to determine, if any unsaved data could be lost and prompts a save dialog.
     */
    bool RayTracerWindow::closeAllTabs()
    {
        while (tabWidget_->count() > 0)
        {
            int index = tabWidget_->count() - 1;
            tabWidget_->setCurrentIndex(index);

            // remove save flag
            if (!saveFlags_[index])
            {
                QMessageBox messageBox;
                messageBox.setText(tr("Do you want to save the image?"));
                messageBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::NoAll | QMessageBox::Cancel);
                messageBox.setDefaultButton(QMessageBox::Yes);
                int answer = messageBox.exec();

                switch (answer)
                {
                case QMessageBox::Yes:
                    if (!save())
                    {
                        return false;
                    }
                    break;
                case QMessageBox::No:
                    break;
                case QMessageBox::NoAll:
                    return true;
                    break;
                case QMessageBox::Cancel:
                    return false;
                }
            }

            // remove save flag
            saveFlags_.erase(saveFlags_.begin() + index);

            // remove unique image index
            imageIndices_.erase(imageIndices_.begin() + index);

            // remove image viewer 
            imageViewerWidgets_.erase(imageViewerWidgets_.begin() + index);

            // delete tab widget
            if (tabWidget_->count() == 1)
            {
                // workaround for removing last tab of QTabWidget
                tabWidget_->deleteLater();
                createTabWidget();
            }
            else
            {
                QWidget* tabWidget = tabWidget_->widget(index);
                tabWidget_->removeTab(index);
                tabWidget->deleteLater();
            }

            updateFileActions();
        }

        return true;
    }



    /*! \brief Close image tab.
     *  \param[in] index Index of tab to close.
     *  \return Returns TRUE if the tab is closed successfully, otherwise FALSE.
     *
     *  Checks the save flags to determine, if any unsaved data could be lost and prompts a save dialog.
     */
    bool RayTracerWindow::closeTab(int index)
    {
        // check if image has been saved
        if (!saveFlags_[index])
        {
            QMessageBox messageBox;
            messageBox.setText(tr("Do you want to save the image?"));
            messageBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
            messageBox.setDefaultButton(QMessageBox::Yes);
            int answer = messageBox.exec();

            switch (answer)
            {
            case QMessageBox::Yes:
                if (!save())
                {
                    return false;
                }
                break;
            case QMessageBox::No:
                break;
            case QMessageBox::Cancel:
                return false;
            }
        }

        // remove save flag
        saveFlags_.erase(saveFlags_.begin() + index);

        // remove unique image index
        imageIndices_.erase(imageIndices_.begin() + index);

        // remove image viewer 
        imageViewerWidgets_.erase(imageViewerWidgets_.begin() + index);

        // delete tab widget
        if (tabWidget_->count() == 1)
        {
            // workaround for removing last tab of QTabWidget
            tabWidget_->deleteLater();
            createTabWidget();
        }
        else
        {
            QWidget* tabWidget = tabWidget_->widget(index);
            tabWidget_->removeTab(index);
            tabWidget->deleteLater();
        }

        updateFileActions();

        return true;
    }



    /*! \brief Save image to hard drive.
     *  \return Returns true if successful; otherwise returns false.
     *
     *  Opens save file dialog to specify the filename and type of the image.
     */
    bool RayTracerWindow::save()
    {
        // get image file name
        QString fileName = QFileDialog::getSaveFileName(this, tr("Save File"), QDir::currentPath(), tr("16 bit Images (*.png);;8 bit Images (*.jpg *.bmp)"));

        // test for image magick: if (cimg_library::cimg::imagemagick_path())

        if (!fileName.isEmpty())
        {
            // save image
            std::shared_ptr<cimg_library::CImg<float> > floatImage = images_[tabWidget_->currentIndex()];
            bool saved = false;

            if (!QFileInfo(fileName).suffix().compare(QString("bmp")))
            {
                std::shared_ptr<cimg_library::CImg<unsigned char> > image = std::make_shared<cimg_library::CImg<unsigned char> >(
                    floatImage->width(), floatImage->height(), floatImage->depth(), floatImage->spectrum());

                // iterate pixels
                for (int u = 0; u < image->width(); ++u)
                {
                    for (int v = 0; v < image->height(); ++v)
                    {
                        // copy pixel
                        image->operator()(u, v, 0) = static_cast<unsigned char>(floatImage->operator()(u, v, 0) * TRUE_COLOR_MAX);
                        image->operator()(u, v, 1) = static_cast<unsigned char>(floatImage->operator()(u, v, 1) * TRUE_COLOR_MAX);
                        image->operator()(u, v, 2) = static_cast<unsigned char>(floatImage->operator()(u, v, 2) * TRUE_COLOR_MAX);
                    }
                }

                saved = image->save(fileName.toStdString().c_str());
            }
            else
            {
                std::shared_ptr<cimg_library::CImg<unsigned short> > image = std::make_shared<cimg_library::CImg<unsigned short> >(
                    floatImage->width(), floatImage->height(), floatImage->depth(), floatImage->spectrum());

                // iterate pixels
                for (int u = 0; u < image->width(); ++u)
                {
                    for (int v = 0; v < image->height(); ++v)
                    {
                        // copy pixel
                        image->operator()(u, v, 0) = static_cast<unsigned short>(floatImage->operator()(u, v, 0) * HIGH_COLOR_MAX);
                        image->operator()(u, v, 1) = static_cast<unsigned short>(floatImage->operator()(u, v, 1) * HIGH_COLOR_MAX);
                        image->operator()(u, v, 2) = static_cast<unsigned short>(floatImage->operator()(u, v, 2) * HIGH_COLOR_MAX);
                    }
                }

                saved = image->save(fileName.toStdString().c_str());
            }

            if (saved)
            {
                adjustTabLabel(tabWidget_->currentIndex(), imageIndices_[tabWidget_->currentIndex()], true);
                saveFlags_[tabWidget_->currentIndex()] = true;
            }
            return saved;
        }
        return false;
    }



    /*! \brief Zoom in by 25 %.
     */
    void RayTracerWindow::zoomIn()
    {
        scaleImage(1.25);
    }



    /*! \brief Zoom out by 25 %.
     */
    void RayTracerWindow::zoomOut()
    {
        scaleImage(0.8);
    }



    /*! \brief Reset image to normal size.
     */
    void RayTracerWindow::normalSize()
    {
        imageViewerWidgets_[tabWidget_->currentIndex()]->resetTransform();
    }



    /*! \brief Sets image size to fit window size.
     */
    void RayTracerWindow::fitToWindow()
    {
        bool fitToWindow = fitToWindowAction_->isChecked();
        if (fitToWindow)
        {
            QGraphicsItem* imageItem = imageViewerWidgets_[tabWidget_->currentIndex()]->items().at(0);
            imageViewerWidgets_[tabWidget_->currentIndex()]->fitInView(imageItem, Qt::KeepAspectRatio);
        }
        else
        {
            normalSize();
        }
        updateViewActions();
    }



    ///*! \brief Loads scene description file.
    // *  \return Returns TRUE if loading was successful, else FALSE.
    // */
    //bool RayTracerWindow::loadSceneDescription()
    //{
    //    if (frontend_->initScene(sceneDescriptionLine_->text().toStdString()))
    //    {
    //        //setRenderGuiDisabled(false);
    //        return true;
    //    }
    //    return false;
    //}



    /*! \brief Blocks GUI components while rendering.
     */
    void RayTracerWindow::start()
    {
        // disable GUI components
        setSceneGuiDisabled(true);
        //setRenderGuiDisabled(true);
        setMultithreadingGuiDisabled(true);
        setLoggingGuiDisabled(true);

        // set control buttons
        startButton_->setDisabled(true);
        if (getMultiThreading())
        {
            abortButton_->setDisabled(false);
            pauseButton_->setDisabled(false);
            resumeButton_->setDisabled(true);
        }

        // set progress bar
        progressBar_->setValue(0);
        progressBar_->setVisible(true);

#ifdef win_OS
        taskbarButton_ = new QWinTaskbarButton(this);
        taskbarButton_->setWindow(this->windowHandle());
        taskbarProgress_ = taskbarButton_->progress();
        taskbarProgress_->setMinimum(0);
        taskbarProgress_->setMaximum(0);
        taskbarProgress_->setVisible(true);
        taskbarProgress_->setValue(0);
#endif
    }



    /*! \brief Sets GUI components when rendering is aborted.
     */
    void RayTracerWindow::abort()
    {
        // enable GUI components
        setSceneGuiDisabled(false);
        //setRenderGuiDisabled(false);
        setMultithreadingGuiDisabled(false);
        setLoggingGuiDisabled(false);

        // set control buttons
        startButton_->setDisabled(false);
        if (getMultiThreading())
        {
            abortButton_->setDisabled(true);
            pauseButton_->setDisabled(true);
            resumeButton_->setDisabled(true);
        }

        statusBar()->showMessage(tr("Ready"));
        progressBar_->setVisible(false);

#ifdef win_OS
        taskbarProgress_->setVisible(false);
#endif
    }



    /*! \brief Sets GUI components while pausing.
     */
    void RayTracerWindow::pause()
    {
        // set control buttons
        startButton_->setDisabled(true);
        if (getMultiThreading())
        {
            abortButton_->setDisabled(false);
            pauseButton_->setDisabled(true);
            resumeButton_->setDisabled(false);
#ifdef win_OS
            taskbarProgress_->pause();
#endif
        }
    }



    /*! \brief Sets GUI components when resuming.
     */
    void RayTracerWindow::resume()
    {
        start();
    }



    /*! \brief Displays message in message area.
     *  \param[in] message Message to display.
     */
    void RayTracerWindow::showMessage(QString message)
    {
        std::cout << message.toStdString() << std::endl;
    }



    /*! \brief Sets the number of image chunks to be rendered.
     *  \param[in] numberOfChunks Message to display.
     */
    void RayTracerWindow::setNumberOfChunks(int numberOfChunks)
    {
        progressBar_->setMaximum(numberOfChunks);

#ifdef win_OS
        taskbarProgress_->setMaximum(numberOfChunks);
#endif
    }



    /*! \brief Event handler for close events.
     *  \param[in] event Close event.
     *  
     *  Tries to close all opened tabs and prompts save dialogs for unsaved data. Ignores close event,
     *  if user selects to abort closing the application or when an error occurs during saving.
     */
    void RayTracerWindow::closeEvent(QCloseEvent* event)
    {
        bool closeFlag = closeAllTabs();

        if (closeFlag)
        {
            // images have been saved / shall not be saved
            close();
        }
        else
        {
            // user pressed cancel / error during saving
            event->ignore();
        }
    }



    /*! \brief Open load file dialog to browse scene description files.
     */
    void RayTracerWindow::browseSceneDescription()
    {
        QString fileName = QFileDialog::getOpenFileName(this, tr("Open file"), "", tr("Files (*.*)"));
        if (!fileName.isNull())
        {
            sceneDescriptionLine_->setText(fileName);
        }
    }



    /*! \brief Open load file dialog to browse session files and loads session.
     */
    void RayTracerWindow::loadSession()
    {
        QString fileName = QFileDialog::getOpenFileName(this, tr("Open file"), "", tr("Files (*.*)"));
        if (!fileName.isNull())
        {
            // load session
            if (frontend_->initSession(fileName.toStdString()))
            {
                // set GUI components
                setSceneGuiDisabled(true);
                //setRenderGuiDisabled(true);
                setMultithreadingGuiDisabled(true);
                numberOfThreadsLine_->setDisabled(false);

                abort();
                abortButton_->setDisabled(false);
            }
        }
    }



    /*! \brief Enables Gui for changing session data.
     */
    void RayTracerWindow::resetSession()
    {
        // set GUI components
        abort();

        frontend_->previousSession_ = false;
    }



    /*! \brief Toggle multithreading properties.
     *  \param[in] checked Boolean variable indicating whether to enable multithreading properties.
     */
    void RayTracerWindow::toggleMultiThreading(bool checked)
    {
        chunkWidthLine_->setEnabled(checked);
        chunkHeightLine_->setEnabled(checked);
        numberOfThreadsLine_->setEnabled(checked);
    }



    /*! \brief Enables and disables File menu bar actions.
     */
    void RayTracerWindow::updateFileActions()
    {
        if (tabWidget_->count() == 0)
        {
            saveAction_->setEnabled(false);
            zoomInAction_->setEnabled(false);
            zoomOutAction_->setEnabled(false);
            normalSizeAction_->setEnabled(false);
            fitToWindowAction_->setEnabled(false);
        }
        else
        {
            saveAction_->setEnabled(true);
            zoomInAction_->setEnabled(true);
            zoomOutAction_->setEnabled(true);
            normalSizeAction_->setEnabled(true);
            fitToWindowAction_->setEnabled(true);
        }
    }



    /*! \brief Enables and disables View menu bar actions.
     */
    void RayTracerWindow::updateViewActions()
    {
        zoomInAction_->setEnabled(!fitToWindowAction_->isChecked());
        zoomOutAction_->setEnabled(!fitToWindowAction_->isChecked());
        normalSizeAction_->setEnabled(!fitToWindowAction_->isChecked());
    }



    /*! \brief Creates central tab widget.
     */
    void RayTracerWindow::createTabWidget()
    {
        tabWidget_ = new QTabWidget;
        tabWidget_->setTabsClosable(true);
        connect(tabWidget_, &QTabWidget::tabCloseRequested, this, &RayTracerWindow::closeTab);
        imageViewerLayout_->addWidget(tabWidget_);
    }



    /*! \brief Creates menu bar actions.
     */
    void RayTracerWindow::createActions()
    {
        openAction_ = new QAction(tr("&Open"), this);
        openAction_->setShortcut(tr("Ctrl+O"));
        connect(openAction_, &QAction::triggered, this, &RayTracerWindow::open);

        saveAction_ = new QAction(tr("&Save"), this);
        saveAction_->setShortcut(tr("Ctrl+S"));
        saveAction_->setEnabled(false);
        connect(saveAction_, &QAction::triggered, this, &RayTracerWindow::save);

        exitAction_ = new QAction(tr("E&xit"), this);
        exitAction_->setShortcut(tr("Ctrl+Q"));
        connect(exitAction_, &QAction::triggered, this, &RayTracerWindow::close);

        zoomInAction_ = new QAction(tr("Zoom &In (25%)"), this);
        zoomInAction_->setShortcut(tr("Ctrl++"));
        zoomInAction_->setEnabled(false);
        connect(zoomInAction_, &QAction::triggered, this, &RayTracerWindow::zoomIn);

        zoomOutAction_ = new QAction(tr("Zoom &Out (25%)"), this);
        zoomOutAction_->setShortcut(tr("Ctrl+-"));
        zoomOutAction_->setEnabled(false);
        connect(zoomOutAction_, &QAction::triggered, this, &RayTracerWindow::zoomOut);

        normalSizeAction_ = new QAction(tr("&Normal Size"), this);
        normalSizeAction_->setShortcut(tr("Ctrl+N"));
        normalSizeAction_->setEnabled(false);
        connect(normalSizeAction_, &QAction::triggered, this, &RayTracerWindow::normalSize);

        fitToWindowAction_ = new QAction(tr("&Fit to Window"), this);
        fitToWindowAction_->setEnabled(false);
        fitToWindowAction_->setShortcut(tr("Ctrl+F"));
        connect(fitToWindowAction_, &QAction::triggered, this, &RayTracerWindow::fitToWindow);

        loadSessionAction_ = new QAction(tr("Load Session"), this);
        loadSessionAction_->setShortcut(tr("Ctrl+L"));
        connect(loadSessionAction_, &QAction::triggered, this, &RayTracerWindow::loadSession);

        resetSessionAction_ = new QAction(tr("Reset Session"), this);
        resetSessionAction_->setShortcut(tr("Ctrl+R"));
        connect(resetSessionAction_, &QAction::triggered, this, &RayTracerWindow::resetSession);
    }



    /*! \brief Creates menu bar.
     */
    void RayTracerWindow::createMenus()
    {
        fileMenu_ = menuBar()->addMenu(tr("&File"));
        fileMenu_->addAction(openAction_);
        fileMenu_->addAction(saveAction_);
        fileMenu_->addSeparator();
        fileMenu_->addAction(exitAction_);

        viewMenu_ = menuBar()->addMenu(tr("&View"));
        viewMenu_->addAction(zoomInAction_);
        viewMenu_->addAction(zoomOutAction_);
        viewMenu_->addSeparator();
        viewMenu_->addAction(normalSizeAction_);
        viewMenu_->addAction(fitToWindowAction_);

        sessionMenu_ = menuBar()->addMenu(tr("Session"));
        sessionMenu_->addAction(loadSessionAction_);
        sessionMenu_->addAction(resetSessionAction_);
    }



    /*! \brief Returns size of the Pixmap of specified tab.
     *  \param[in] tabIndex Index of tab.
     *  \return Returns the size of the drawn pixmap.
     */
    QSize RayTracerWindow::getSize(int tabIndex) const
    {
        if (tabWidget_->count() <= tabIndex)
        {
            return QSize(0, 0);
        }
        const QGraphicsPixmapItem* imageItem = qgraphicsitem_cast<const QGraphicsPixmapItem*>(imageViewerWidgets_[tabIndex]->items().at(0));
        return imageItem->pixmap().size();
    }



    /*! \brief Scale image by given factor.
     *  \param[in] factor Scaling factor.
     */
    void RayTracerWindow::scaleImage(double factor)
    {
        int currentIndex = tabWidget_->currentIndex();
        imageViewerWidgets_[currentIndex]->scale(factor, factor);
    }



    /*! \brief Adjust image tab label.
     *  \param[in] tabIndex Index of tab to update.
     *  \param[in] imageIndex Unique image index.
     *  \param[in] saveFlag Flag indicating whether image has been saved yet.
     */
    void RayTracerWindow::adjustTabLabel(int tabIndex, int imageIndex, bool saveFlag)
    {
        QString tabLabel = QString(tr("Image %1")).arg(imageIndex + 1);
        if (!saveFlag)
        {
            tabLabel.append("*");
        }
        tabWidget_->setTabText(tabIndex, tabLabel);
    }



    /*! \brief Set disabled state of scene controls.
     *  \param[in] disabled Flag indicating whether to disable scene controls.
     */
    void RayTracerWindow::setSceneGuiDisabled(bool disabled)
    {
        browseSceneButton_->setDisabled(disabled);
        sceneDescriptionLine_->setDisabled(disabled);
        //loadSceneButton_->setDisabled(disabled);
    }



    ///*! \brief Set disabled state of render property controls.
    // *  \param[in] disabled Flag indicating whether to disable render property controls.
    // */
    //void RayTracerWindow::setRenderGuiDisabled(bool disabled)
    //{
    //    uResLine_->setDisabled(disabled);
    //    vResLine_->setDisabled(disabled);
    //    samplerComboBox_->setDisabled(disabled);
    //    numberOfSamplesLine_->setDisabled(disabled);
    //}



    /*! \brief Set disabled state of multithreading controls.
     *  \param[in] disabled Flag indicating whether to disable multithreading controls.
     */
    void RayTracerWindow::setMultithreadingGuiDisabled(bool disabled)
    {
        multiThreadingCheckBox_->setDisabled(disabled);
        chunkWidthLine_->setDisabled(disabled);
        chunkHeightLine_->setDisabled(disabled);
        numberOfThreadsLine_->setDisabled(disabled);
    }



    /*! \brief Set disabled state of logging controls.
     *  \param[in] disabled Flag indicating whether to disable logging controls.
     */
    void RayTracerWindow::setLoggingGuiDisabled(bool disabled)
    {
        logLevelDropDown_->setDisabled(disabled);
    }

} // end of namespace iiit
