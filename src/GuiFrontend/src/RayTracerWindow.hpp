// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file RayTracerWindow.hpp
 *  \author Thomas Nuernberg
 *  \date 21.11.2014
 *  \brief Definition of class RayTracerWindow.
 *
 *  Definition of class RayTracerWindow, the GUI of the ray tracer.
 */

#ifndef __RAYTRACERWINDOW_HPP__
#define __RAYTRACERWINDOW_HPP__



#if defined(_MSC_VER) || defined(WIN32)  || defined(_WIN32) || defined(__WIN32__) \
    || defined(WIN64)    || defined(_WIN64) || defined(__WIN64__)
#define win_OS
#endif



#include <string>
#include <memory>
#include <iostream>
#include <streambuf>

#include <QtWidgets>
#ifdef win_OS
#include <QtWinExtras>
#endif

#include "CImg.h"

#include "StdRedirector.hpp"
#include "Viewer/ImageViewerWidget.hpp"



namespace iiit
{

    class GuiFrontend; // forward declaration



    /*! \brief Slimmer QLineEdit.
     */
    class SlimLineEdit : public QLineEdit
    {
    public:
        /*! \brief Reimplemented sizeHint to use minimumSizeHint.
         *  \return Returns sizeHint.
         */
        virtual QSize sizeHint() const
        {
            return minimumSizeHint();
        }
    };



    /*! \brief Graphical User Interface for the RayTracer class.
     *
     *  This class provides a Graphical User Interface for the RayTracer class based in QMainWindow. The
     *  left hand side shows the different control properties of the RayTracer while the resulting
     *  images are displayed on the right hand side.
     *  
     *  Images can be displayed, loaded, and saved. The class offers a tabbed interface to display
     *  multiple images. Each image can be zoomed independently. A dialog is opened, when an unsaved
     *  image tab shall be closed.
     *
     *  Use addImage() to pass an image object to the viewer to display. The initImage() function adds a
     *  new image tab to the viewer and displays a default black image. Images that are already
     *  displayed can either be updated as a whole with the updateImage() function or in parts by
     *  specifying the according region to update with the updateChunk() function.
     */
    class RayTracerWindow : public QMainWindow
    {
        Q_OBJECT

    public:
        RayTracerWindow(GuiFrontend* frontend, QWidget *parent = 0);
        ~RayTracerWindow();

        std::string getSceneDescriptionFileName();
        void setSceneDescriptionFileName(std::string sceneDescription);
        bool getMultiThreading() const;
        void setMultiThreading(bool multiThreading);
        int getChunkWidth() const;
        void setChunkWidth(int chunkWidth);
        int getChunkHeight() const;
        void setChunkHeight(int chunkHeight);
        size_t getNumberOfThreads() const;
        void setNumberOfThreads(size_t numberOfThreads);
        int getLogLevel() const;
        //Sampler::Type getSamplerType() const;

        //int getURes() const;
        //void setURes(int uRes);
        //int getVRes() const;
        //void setVRes(int vRes);
        
    public slots:
        int addImage(std::shared_ptr<const cimg_library::CImg<float> > image, bool saveFlag = false);
        int initImage(int width, int height);
        int initImage(const QSize& size);
        void updateImage(std::shared_ptr<const cimg_library::CImg<float> > image, int imageIndex);
        void updateChunk(std::shared_ptr<const cimg_library::CImg<float> > image, QRect region, int imageIndex);

        void open();
        bool closeTab(int index);
        bool closeAllTabs();
        bool save();
        void zoomIn();
        void zoomOut();
        void normalSize();
        void fitToWindow();
        //bool loadSceneDescription();

        void start();
        void abort();
        void pause();
        void resume();

        void showMessage(QString message);

        void setNumberOfChunks(int numberOfChunks);

    protected:
        void closeEvent(QCloseEvent* event);

    private slots:
        void showImage(QGraphicsScene* scene, std::shared_ptr<const cimg_library::CImg<float> > image);
        void browseSceneDescription();
        void loadSession();
        void resetSession();
        void toggleMultiThreading(bool checked);
        void updateFileActions();
        void updateViewActions();

    private:
        void createTabWidget();
        void createActions();
        void createMenus();
        QSize getSize(int tabIndex) const;
        void scaleImage(double factor);
        void adjustTabLabel(int tabIndex, int imageIndex, bool saveFlag);
        void setSceneGuiDisabled(bool disabled);
        //void setRenderGuiDisabled(bool disabled);
        void setMultithreadingGuiDisabled(bool disabled);
        void setLoggingGuiDisabled(bool disabled);

        // pointer to frontend
        GuiFrontend* frontend_; ///< Pointer to frontend.

        // central tab widget
        QHBoxLayout* imageViewerLayout_; ///< Layout for tabbed image viewer.
        QTabWidget* tabWidget_; ///< Tab widget for tabbed image viewer.

        // per tab data
        std::vector<ImageViewerWidget*> imageViewerWidgets_; ///< Widgets displaying images in tab.
        std::vector<int> imageIndices_; ///< Unique IDs of currently opened images.
        std::vector<bool> saveFlags_; ///< Flags indicating whether images have been saved.
        std::vector<std::shared_ptr<cimg_library::CImg<float> > > images_; ///< Images in tab widget.
        int imageCounter_; ///< Counter of displayed images, used for generating image IDs.

        // menu bar actions
        QAction* openAction_; ///< QAction to open an image.
        QAction* saveAction_; ///< QAction to save an image.
        QAction* exitAction_; ///< QAction to exit the application.
        QAction* normalSizeAction_; ///< QAction to reset view to 100%.
        QAction* zoomInAction_; ///< QAction to zoom in.
        QAction* zoomOutAction_; ///< QAction to zoom out.
        QAction* fitToWindowAction_; ///< QAction to fit the image to the current window size.
        QAction* loadSessionAction_; ///< QAction to load a previous rendering session.
        QAction* resetSessionAction_; ///< QAction to reset current rendering session.

        // menu bar
        QMenu* fileMenu_; ///< Menu containing open, save and exit actions.
        QMenu* viewMenu_; ///< Menu containing viewer controls.
        QMenu* sessionMenu_; ///< Menu containing session actions.

        // ray tracer properties
        QPushButton* browseSceneButton_; ///< Button to browse scene description files.
        QLineEdit* sceneDescriptionLine_; ///< Line holding the path of the scene description file.
        //QPushButton* loadSceneButton_; ///< Button to load scene description file.
        QPushButton* startButton_; ///< Button to start rendering.
        QPushButton* abortButton_; ///< Button to abort rendering.
        QPushButton* pauseButton_; ///< Button to pause rendering.
        QPushButton* resumeButton_; ///< Button to resume current rendering session.
        //SlimLineEdit* uResLine_; ///< Line holding horizontal image resolution.
        //SlimLineEdit* vResLine_; ///< Line holding vertical image resolution.
        //QComboBox* samplerComboBox_; ///< Drop down menu for sample selection.
        //SlimLineEdit* numberOfSamplesLine_; ///< Line holding the number of samples to use per pixel.
        QCheckBox* multiThreadingCheckBox_; ///< Checkbox to toggle multithreading.
        SlimLineEdit* chunkWidthLine_; ///< Line holding desired width of image chunks.
        SlimLineEdit* chunkHeightLine_; ///< Line holding desired height of image chunks.
        SlimLineEdit* numberOfThreadsLine_; ///< Line holding the number of threads.
        QComboBox* logLevelDropDown_; ///< Drop-Down menu to select logging level.

        // console
        std::shared_ptr<StdRedirector> stdRedirector_; ///< Object for redirecting std::cout.

        // status
        QProgressBar* progressBar_; ///< Progress bar to indicate render progress.

#ifdef win_OS
        QWinTaskbarButton* taskbarButton_; ///< Windows taskbar button.
        QWinTaskbarProgress* taskbarProgress_; ///< Progress bar in windows taskbar to indicate render progress.
#endif
    };

} // end of namespace iiit

#endif // __RAYTRACERWINDOW_HPP__
