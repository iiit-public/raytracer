// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file GuiFrontend.cpp
 *  \author Thomas Nuernberg
 *  \date 19.12.2014
 *  \brief Implementation of class RayTracerFrontend.
 *
 *  Implementation of class GuiFrontend, which offers a graphical user interface for the RayTracer
 *  class.
 */

#include "GuiFrontend.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <functional>
#include <algorithm>
#include <chrono>
#include <new> // std::bad_alloc

#include "Logger.hpp"
#include "FileLogTarget.hpp"

#include "SessionParser.hpp"
#include "SceneLogger.hpp"



namespace iiit
{

    /*! \brief Default constructor.
     */ 
    GuiFrontend::GuiFrontend()
        : previousSession_(false)
        , imageIndex_(0)
    {
        rayTracerWindow_ = new RayTracerWindow(this);
        rayTracerWindow_->show();
        qRegisterMetaType<std::shared_ptr<const cimg_library::CImg<float> > >(); // register shared pointer to CImg as signal argument
        qRegisterMetaType<QTextCursor>(); // register QTextCursor as signal argument
        qRegisterMetaType<QTextBlock>(); // register QTextBlock as signal argument
        connect(this, &GuiFrontend::chunkFinished, rayTracerWindow_, &RayTracerWindow::updateChunk, Qt::QueuedConnection);
        connect(this, &GuiFrontend::renderingFinished, rayTracerWindow_, &RayTracerWindow::abort);
        connect(this, &GuiFrontend::message, rayTracerWindow_, &RayTracerWindow::showMessage, Qt::QueuedConnection);
    }



    /*! \brief Default destructor.
     */ 
    GuiFrontend::~GuiFrontend()
    {
        // delete GUI
        delete rayTracerWindow_;
    }



    /*! \brief Converts the given image chunk and sends it to the viewer widget.
     *  \param[in] viewPlane ViewPlane corresponding to the image.
     *  \param[in] image Updated image.
     *
     *  Method takes an updated image of type CImg and copies and converts it to type QImage.
     */ 
    void GuiFrontend::updateChunk(const ViewPlane& viewPlane, std::shared_ptr<const cimg_library::CImg<float> > image)
    {
        // send image to GUI
        updateChunkMutex_.lock();
        emit chunkFinished(
            image,
            QRect(viewPlane.uOffset_, viewPlane.vOffset_, viewPlane.uRes_, viewPlane.vRes_),
            imageIndex_);
        //sessionLogger_.logProgress(getChunkIndex(viewPlane));
        //sessionLogger_.logChunk(viewPlane, image);
        updateChunkMutex_.unlock();
    }



    /*! \brief Builds RayTracer objects and invokes the rendering of a scene.
     */ 
    void GuiFrontend::run()
    {
        // set GUI
        rayTracerWindow_->start();

        rayTracerWindow_->statusBar()->showMessage(tr("Parsing Scene Description File..."));

        // get data from GUI
        if (!initSession())
        {
            rayTracerWindow_->statusBar()->showMessage(tr("Parsing failed!"));
            rayTracerWindow_->abort();
            return;
        }

        // log scene
        std::ifstream  src(rayTracerWindow_->getSceneDescriptionFileName().c_str(), std::ios::binary);
        std::ofstream  dst("scene.json", std::ios::binary);
        dst << src.rdbuf();
        //SceneLogger sceneLogger;
        //std::ofstream sceneFile;
        //sceneFile.open("scene.json", std::ios::out);
        //sceneLogger.log(scene_, sceneFile);
        //sceneFile.close();

        // log session
        std::ofstream sessionFile;
        sessionFile.open("session.json", std::ios::out | std::ios::trunc);
        sessionLogger_.logSession(chunkWidth_, chunkHeight_, sessionFile);

        sessionFile.close();

        // setup logging
        if (LOG_MAX_LEVEL && rayTracerWindow_->getLogLevel())
        {
            FILE* pFile = fopen("raytracer.log", "w"); // write mode -> existing log file gets replaced
            FileLogTarget::stream() = pFile;
            Logger<FileLogTarget>::reportingLevel() = LogLevel(rayTracerWindow_->getLogLevel());
        }
        else
        {
            Logger<FileLogTarget>::reportingLevel() = LogLevel::LogNone;
        }

        if (multithreading_)
        {
            if (!metaData_.supportsMultiThreading_)
            {
                std::cout << "Current scene doesn't support multithreading." << std::endl;

                // reset GUI
                rayTracerWindow_->abort();

                return;
            }

            // partition ViewPlane defining several render tasks
            std::vector<ViewPlane> viewPlanes = createChunks(
                scene_->viewPlane_,
                chunkWidth_,
                chunkHeight_,
                metaData_.uMin_,
                metaData_.uMax_,
                metaData_.vMin_,
                metaData_.vMax_);

            // continue previous session?
            if (previousSession_)
            {
                // open progress file
                std::ifstream progressFile;
                progressFile.open(progressFileName_.c_str(), std::ifstream::in | std::ifstream::binary);

                // remove viewPlanes that were already rendered
                int chunkIndex;
                while (progressFile)
                {
                    // read chunk index from file
                    progressFile.read((char*)(&chunkIndex), sizeof(chunkIndex));
                    for (std::vector<ViewPlane>::iterator it = viewPlanes.begin(); it != viewPlanes.end(); ++it)
                    {
                        // compare chunk index with current index
                        if (getChunkIndex(*it) == chunkIndex)
                        {
                            // remove ViewPlane
                            viewPlanes.erase(it);
                            break;
                        }
                    }
                }
                progressFile.close();
            }

            // delete RayTracers
            rayTracers_.clear();

            // create RayTracer for each ViewPlane
            try
            {
                for (std::vector<ViewPlane>::const_iterator it = viewPlanes.begin(); it != viewPlanes.end(); ++it)
                {
                    std::shared_ptr<AbstractScene> scene(scene_->clone());
                    scene->viewPlane_ = *it;
                    rayTracers_.push_back(std::shared_ptr<RayTracer>(new RayTracer(scene, std::bind(&GuiFrontend::updateChunk, this, std::placeholders::_1, std::placeholders::_2))));
                }
            }
            catch (std::bad_alloc& ba)
            {
                std::cout << "bad_alloc exception caught: " << ba.what() << std::endl;
                std::cout << "Possible reasons:" << std::endl;
                std::cout << "  - There is not enough memory." << std::endl;
                std::cout << "  - The memory is fragmented." << std::endl;
                std::cout << "  - In case of a 32 bit build: More than 4 GB memory are required." << std::endl;
                rayTracerWindow_->statusBar()->showMessage(tr("Rendering failed!"));
                abort();
                return;
            }

            // create worker pool
            pool_ = std::unique_ptr<ThreadPool>(new ThreadPool(rayTracerWindow_->getNumberOfThreads()));

            // start logging
            if (previousSession_)
            {
                sessionLogger_.resumeProgressLogging();
            }
            else
            {
                sessionLogger_.startProgressLogging();
            }

            // start time
            start_ = std::chrono::system_clock::now();

            // set progress bar
            rayTracerWindow_->setNumberOfChunks(static_cast<int>(rayTracers_.size()));

            // update status
            rayTracerWindow_->statusBar()->showMessage(tr("Rendering..."));
            LOG(LogLevel::LogInfo, "Starting to render scene " << rayTracerWindow_->getSceneDescriptionFileName().c_str() << ".");

            // fill task queue
            for (std::vector<std::shared_ptr<RayTracer> >::iterator it = rayTracers_.begin(); it != rayTracers_.end(); ++it)
            {
                std::function<void()> task = std::bind(&RayTracer::run, *it);
                pool_->enqueue(task);

                //// use this code for debugging multithreaded rendering
                //it->get()->run();
            }

            // wait for queue to empty
            std::thread t(std::bind(&GuiFrontend::waitForPool, this));
            t.detach();
        }
        else
        {
            // delete RayTracers
            rayTracers_.clear();

            // select area to render
            scene_->viewPlane_.uRes_ = metaData_.uMax_ - metaData_.uMin_ + 1;
            scene_->viewPlane_.uOffset_ = metaData_.uMin_;
            scene_->viewPlane_.vRes_ = metaData_.vMax_ - metaData_.vMin_ + 1;
            scene_->viewPlane_.vOffset_ = metaData_.vMin_;

            // create single RayTracer object
            rayTracers_.push_back(std::shared_ptr<RayTracer>(new RayTracer(scene_, std::bind(&GuiFrontend::updateChunk, this, std::placeholders::_1, std::placeholders::_2))));

            // update status
            rayTracerWindow_->statusBar()->showMessage(tr("Rendering..."));
            LOG(LogLevel::LogInfo, "Starting to render scene " << rayTracerWindow_->getSceneDescriptionFileName().c_str() << ".");

            // start time
            start_ = std::chrono::system_clock::now();

            // render
            rayTracers_.front()->run();

            // duration
            auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start_);
            std::cout << "Elapsed Time: " << duration.count() << " ms" << std::endl;
            LOG(LogLevel::LogInfo, "Rendering finished. Elapsed Time: " << duration.count() << " ms");

            // reset GUI
            rayTracerWindow_->abort();

            rayTracerWindow_->statusBar()->showMessage(tr("Ready"));

            // delete RayTracers
            rayTracers_.clear();

            // remove last scene reference
            scene_.reset();
        }
    }



    /*! \brief Abort rendering.
     */
    void GuiFrontend::abort()
    {
        // delete worker pool, which signals the threads to terminate.
        pool_.reset();

        // reset GUI
        rayTracerWindow_->abort();

        // reset session
        previousSession_ = false;
    }



    /*! \brief Pause rendering.
     */
    void GuiFrontend::pause()
    {
        pool_->pause();
        rayTracerWindow_->pause();
    }



    /*! \brief Resume rendering.
     */
    void GuiFrontend::resume()
    {
        pool_->resume();
        rayTracerWindow_->resume();
    }



    /*! \brief Builds Scene object from scene description file.
     *  \param[in] sceneDescriptionFileName Name of scene description file.
     *  \return Return TRUE if building was successful, else FALSE.
     */
    bool GuiFrontend::initScene(const std::string& sceneDescriptionFileName)
    {
        // create new Scene pointer
        std::shared_ptr<AbstractScene> scene;
        
        // parse data
        SceneParser sceneParser;
        if (!sceneParser.parse(sceneDescriptionFileName.c_str(), scene, metaData_))
        {
            // parsing failed
            return false;
        }

        // keep new scene data
        scene_ = scene;
        return true;
    }



    /*! \brief Initialize new session.
     */
    bool GuiFrontend::initSession()
    {
        if (!initScene(rayTracerWindow_->getSceneDescriptionFileName()))
        {
            return false;
        }

        //// get resolution
        //scene_.viewPlane_.uRes_ = rayTracerWindow_->getURes();
        //scene_.viewPlane_.vRes_ = rayTracerWindow_->getVRes();
        //scene_.viewPlane_.uAbsRes_ = rayTracerWindow_->getURes();
        //scene_.viewPlane_.vAbsRes_ = rayTracerWindow_->getVRes();

        // init frontend properties
        multithreading_ = rayTracerWindow_->getMultiThreading();
        if (multithreading_)
        {
            chunkWidth_ = rayTracerWindow_->getChunkWidth();
            chunkHeight_ = rayTracerWindow_->getChunkHeight();
        }

        // init image
        if (!previousSession_)
        {
            imageIndex_ = rayTracerWindow_->initImage(scene_->viewPlane_.uRes_, scene_->viewPlane_.vRes_);
            std::shared_ptr<cimg_library::CImg<float> > image = std::make_shared<cimg_library::CImg<float> >(scene_->viewPlane_.uAbsRes_, scene_->viewPlane_.vAbsRes_, 1, 3);
            cimg_forXYC(*image, x, y, v)
            {
                // this ensures, that the initial image is stored as color image
                image->operator()(x, y, v) = static_cast<float>(((x + y) * (v + 1)) / (std::max(1,(scene_->viewPlane_.uAbsRes_ + scene_->viewPlane_.vAbsRes_ - 2)) * 3));
            }
            sessionLogger_.logImage(image);
        }
        
        return true;
    }



    /*! \brief Initialize previous session.
     *  \param[in] sessionFileName Name of session file.
     */
    bool GuiFrontend::initSession(const std::string& sessionFileName)
    {
        // open progress file
        std::ifstream stream(sessionFileName.c_str(), std::ifstream::in);
        if (!stream.is_open())
        {
            std::cout << "Cannot open file """ << sessionFileName << """." <<std::endl;
            return false;
        }

        // parse contents of progress file
        SessionParser sessionParser;
        if (!sessionParser.parse(stream))
        {
            return false;
        }

        // get paths relative to current directory
        QFileInfo sessionFile(sessionFileName.c_str());
        std::string sceneFileName  = sessionFile.absolutePath().append("/").append(sessionParser.getSceneDescriptionFileName().c_str()).toStdString();
        progressFileName_ = sessionFile.absolutePath().append("/").append(sessionParser.getProgressFileName().c_str()).toStdString();
        std::string imageFileName = sessionFile.absolutePath().append("/").append(sessionParser.getImageFileName().c_str()).toStdString();

        // init world data
        if (!initScene(sceneFileName))
        {
            return false;
        }

        // init frontend properties
        previousSession_ = true;
        multithreading_ = true;
        chunkWidth_ = sessionParser.getChunkWidth();
        chunkHeight_ = sessionParser.getChunkHeight();

        // update GUI
        imageIndex_ = rayTracerWindow_->addImage(std::shared_ptr<const cimg_library::CImg<float> >(new cimg_library::CImg<float>(imageFileName.c_str())));
        rayTracerWindow_->setSceneDescriptionFileName(sceneFileName);
        rayTracerWindow_->setMultiThreading(true);
        rayTracerWindow_->setChunkWidth(chunkWidth_);
        rayTracerWindow_->setChunkHeight(chunkHeight_);
        return true;
    }



    /*! \brief Partitions a ViewPlane to chunks.
     *  \param[in] viewPlane ViewPlane to partition.
     *  \param[in] width Desired width of partitions.
     *  \param[in] height Desired height of partitions.
     *  \param[in] uMin Minimum u-coordinate to render.
     *  \param[in] uMax Maximum u-coordinate to render.
     *  \param[in] vMin Minimum v-coordinate to render.
     *  \param[in] vMax Maximum v-coordinate to render.
     *  \return Return vector of ViewPlane objects of partitions.
     *
     *  Partitions the given ViewPlane object according to the desired chunk width and height. The
     *  resulting ViewPlanes are either of the specified size or smaller. The offset coordinates of the
     *  ViewPlanes are updated enabling the calculation of absolute image coordinates.
     */
    std::vector<ViewPlane> GuiFrontend::createChunks(const ViewPlane& viewPlane, int width, int height, int uMin, int uMax, int vMin, int vMax)
    {
        std::vector<ViewPlane> viewPlanePartitions;
        if (width == 0 || height == 0)
        {
            ViewPlane viewPlanePartition = viewPlane;
            viewPlanePartition.uRes_ = uMax - uMin + 1;
            viewPlanePartition.uAbsRes_ = viewPlane.uAbsRes_;
            viewPlanePartition.uOffset_ = uMin;
            viewPlanePartition.vRes_ = vMax - vMin + 1;
            viewPlanePartition.vAbsRes_ = viewPlane.vAbsRes_;
            viewPlanePartition.vOffset_ = vMin;

            viewPlanePartitions.push_back(viewPlanePartition);
        }
        else
        {
            // partition ViewPlane
            for (int v = vMin; v <= vMax; v += height)
            {
                for (int u = uMin; u <= uMax; u += width)
                {
                    ViewPlane viewPlanePartition = viewPlane;
                    viewPlanePartition.uRes_ = std::min<int>(width, uMax - u + 1);
                    viewPlanePartition.uAbsRes_ = viewPlane.uAbsRes_;
                    viewPlanePartition.uOffset_ = viewPlane.uOffset_ + u;
                    viewPlanePartition.vRes_ = std::min<int>(height, vMax - v + 1);
                    viewPlanePartition.vAbsRes_ = viewPlane.vAbsRes_;
                    viewPlanePartition.vOffset_ = viewPlane.vOffset_ + v;

                    viewPlanePartitions.push_back(viewPlanePartition);
                }
            }
        }
        return viewPlanePartitions;
    }



    /*! \brief Resets GUI and logger.
     *
     *  Helper function suited to run in separate thread waiting for the completion of the render tasks.
     *  Resets GUI and logger when worker pool has finished work.
     */
    void GuiFrontend::waitForPool()
    {
        // wait for worker pool to finish
        pool_->waitForWorkers();

        // duration
        auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - start_);
        emit message(QString("Elapsed Time: %1 ms").arg(duration.count()));
        LOG(LogLevel::LogInfo, "Rendering finished. Elapsed Time: " << duration.count() << " ms");

        // reset frontend
        sessionLogger_.endProgressLogging();
        previousSession_ = false;
        emit renderingFinished();

        // delete RayTracers
        rayTracers_.clear();

        // remove last scene reference
        scene_.reset();

        // reset ThreadPool
        pool_.reset();
    }



    /*! \brief Get chunk index of ViewPlane.
     *  \param[in] viewPlane ViewPlane object.
     *  \return Return chunk index of ViewPlane.
     */
    int GuiFrontend::getChunkIndex(const ViewPlane& viewPlane)
    {
        int uIndex = viewPlane.uOffset_ / rayTracerWindow_->getChunkWidth();
        int vIndex = viewPlane.vOffset_ / rayTracerWindow_->getChunkHeight();

        // uses ceiling of integer division: q = ceil(x / y) = (x + y - 1) / y
        int chunksPerRow = (scene_->viewPlane_.uRes_ + rayTracerWindow_->getChunkWidth() - 1) / rayTracerWindow_->getChunkWidth();

        return uIndex + vIndex * chunksPerRow;
    }

} // end of namespace iiit
