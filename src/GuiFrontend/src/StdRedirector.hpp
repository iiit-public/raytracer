// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file StdRedirector.hpp
 *  \author Thomas Nuernberg
 *  \date 29.05.2015
 *  \brief Implementation of class StdRedirector.
 *
 *  Implementation of class StdRedirector for redirecting std streams.
 */

#ifndef __STDREDIRECTOR_HPP__
#define __STDREDIRECTOR_HPP__

#include <iostream>
#include <streambuf>

#include <QTextEdit>



namespace iiit
{

    /*! \brief Class for redirecting std streams.
     */
    class StdRedirector : public std::basic_streambuf<char>
    {

    public:
        /*! \brief Constructor with initialization.
         *  \param[in] stream Stream to redirect.
         *  \param[in] console Pointer to QTextEdit to redirect stream to.
         */
        StdRedirector(std::ostream& stream, QTextEdit* console)
            : stream_(stream)
            , originalbuffer_(stream.rdbuf())
            , console_(console)
            , append_(console_->metaObject()->method(console_->metaObject()->indexOfSlot("append(QString)")))
        {
            // set read buffer of stream
            stream.rdbuf(this);
        }

        /*! Destructor resets the redirected stream.
         */
        ~StdRedirector()
        {
            // reset read buffer of stream
            stream_.rdbuf(originalbuffer_);
        }

    protected:
        /*! \brief Overloaded method to ensure space in put area of console buffer.
         *  \param[in] v Character to store in put area.
         *  \return Character stored in put are.
         */
        virtual int_type overflow(int_type v)
        {
            //if (v == '\n')
            //{
            //    console_->append("");
            //}
            //return v;
            if (v == '\n')
            {
                //console_->append(string_.c_str());
                append_.invoke(console_, Qt::QueuedConnection, Q_ARG(QString, string_.c_str()));
                string_.erase(string_.begin(), string_.end());
            }
            else
            {
                string_ += v;
            }
            return v;
        }

        /*! \brief Overloaded method to write characters to the console.
         *  \param[in] p Pointer to sequence of characters to be written.
         *  \param[in] n Number of characters to write.
         *  \return THe number of characters written.
         */
        virtual std::streamsize xsputn(const char* p, std::streamsize n)
        {
            string_.append(p, p + n);

            int pos = 0;
            while (pos != std::string::npos)
            {
                pos = static_cast<int>(string_.find('\n'));
                if (pos != std::string::npos)
                {
                    std::string tmp(string_.begin(), string_.begin() + pos);
                    //console_->append(tmp.c_str());
                    append_.invoke(console_, Qt::QueuedConnection, Q_ARG(QString, tmp.c_str()));
                    string_.erase(string_.begin(), string_.begin() + pos + 1);
                }
            }
            return n;


            //QString str(p);
            //console_->moveCursor(QTextCursor::End);
            //if (str.contains("\n"))
            //{
            //    QStringList splittedStr = str.split("\n");
            //    console_->insertPlainText(splittedStr.at(0));

            //    for (int i = 1; i < splittedStr.size(); ++i)
            //    {
            //        console_->append(splittedStr.at(i));
            //    }
            //}
            //else
            //{
            //    console_->insertPlainText(str);
            //}
            //return n;
        }

    private:
        std::ostream& stream_; ///< Stream to redirect.
        std::streambuf* originalbuffer_; ///< Pointer to store original stream buffer .
        QTextEdit* console_; ///< Text field for to redirect stream.
        QMetaMethod append_; ///< Cache of append() method.
        std::string string_; ///< String to redirect.
    };

} // end of namespace iiit

#endif // __STDREDIRECTOR_HPP__