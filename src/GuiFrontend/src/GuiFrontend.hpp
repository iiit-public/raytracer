// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file GuiFrontend.hpp
 *  \author Thomas Nuernberg
 *  \date 19.12.2014
 *  \brief Definition of class GuiFrontend.
 *
 *  Definition of class GuiFrontend, which offers a graphical user interface for the RayTracer
 *  class.
 */

#ifndef __GUIFRONTEND_HPP__
#define __GUIFRONTEND_HPP__

#include <vector>
#include <mutex>
#include <string>
#include <memory>
#include <chrono>

#include <QtWidgets>

#include "CImg.h"

#include "RayTracer.hpp"
#include "AbstractScene.hpp"

#include "ThreadPool.hpp"
#include "SessionLogger.hpp"
#include "SceneParser.hpp"

#include "RayTracerWindow.hpp"



namespace iiit
{

    /*! \brief Derived class for a frontend with graphical user interface.
     *  
     *  This class owns the RayTracer object and manages the data exchange with the graphical user
     *  interface.
     */
    class GuiFrontend : public QObject
    {
        Q_OBJECT

    public:
        GuiFrontend();
        ~GuiFrontend();
        
        bool initScene(const std::string& sceneDescriptionFileName);
        //bool initGeometry(const std::string& sceneDescriptionFileName);
        //bool initCamera(const std::string& sceneDescriptionFileName);
        bool initSession();
        bool initSession(const std::string& sessionFileName);

        void updateChunk(const ViewPlane& viewPlane, std::shared_ptr<const cimg_library::CImg<float> > image);

    public slots:
        void run();
        void abort();
        void pause();
        void resume();

    signals:
        /*! \brief Signals the finished computation of an image chunk.
         */
        void chunkFinished(std::shared_ptr<const cimg_library::CImg<float> > image, QRect region, int imageIndex);

        /*! \brief Signals that the rendering has finished.
         */
        void renderingFinished();

        /*! \brief Signals message to be displayed.
         *  \param[in] message Message to be displayed.
         */
        void message(QString message);

    private:
        friend class RayTracerWindow;

        // methods
        std::vector<ViewPlane> createChunks(const ViewPlane& viewPlane, int width, int height, int uMin, int uMax, int vMin, int vMax);
        void waitForPool();
        int getChunkIndex(const ViewPlane& viewPlane);
        
        // frontend variables and properties
        bool multithreading_; ///< Flag indicating whether to use multithreading
        bool previousSession_; ///< Flag indicating whether to continue rendering a previously aborted session.
        int chunkWidth_; ///< Pixel width of image chunks.
        int chunkHeight_; ///< Pixel height of image chunks
        std::string progressFileName_; ///< File name of progress file.
        std::unique_ptr<ThreadPool> pool_; ///< ThreadPool for multithreading support.
        std::mutex updateChunkMutex_; ///< Guard mutex for GUI access.
        SessionLogger sessionLogger_; ///< SessionLogger object.

        // render variables
        std::vector<std::shared_ptr<RayTracer> > rayTracers_; ///< Vector holding RayTracer objects.
        std::shared_ptr<AbstractScene> scene_; ///< Scene object describing scene to render.
        SceneMetaData metaData_; ///< Scene meta data.

        // GUI variables
        RayTracerWindow* rayTracerWindow_; ///< GUI object.
        int imageIndex_; ///< GUI image index of current image.

        // time variable
        std::chrono::system_clock::time_point start_; ///< Start time of rendering process.
    };

} // end of namespace iiit

Q_DECLARE_METATYPE(std::shared_ptr<const cimg_library::CImg<float> >); ///< register shared pointer to CImg as signal argument
Q_DECLARE_METATYPE(QTextCursor); ///< register QTextCursor as signal argument
Q_DECLARE_METATYPE(QTextBlock); ///< register QTextBlock as signal argument

#endif // __GUIFRONTEND_HPP__
