# Project
project(raytracer)

# Add source files
set(SOURCE_GUIFRONTEND
    ${CMAKE_CURRENT_SOURCE_DIR}/GuiFrontend.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/GuiFrontend.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/RayTracerWindow.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/RayTracerWindow.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/StdRedirector.hpp
    PARENT_SCOPE
)

set(SOURCE_GUIFRONTEND_EXE
    ${CMAKE_CURRENT_SOURCE_DIR}/Icon.h
    ${CMAKE_CURRENT_SOURCE_DIR}/main.cpp
    PARENT_SCOPE
)