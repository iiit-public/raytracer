// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file SceneParserTest.cpp
 *  \author Christian Zimmermann
 *  \date 05.06.2015
 *  \brief Unit test of class SceneParser.
 */

#include <iostream>
#include <fstream>
#include <string>

#include "gtest/gtest.h"

#include "SceneParser.hpp"

#include "AbstractScene.hpp"
#include "Scene.hpp"

#include "Objects/GeometricObject.hpp"
#include "Objects/Sphere.hpp"
#include "Objects/Plane.hpp"
#include "Objects/Triangle.hpp"
#include "Objects/Box.hpp"
#include "Objects/Disk.hpp"
#include "Objects/Square.hpp"
#include "Objects/OpenCylinder.hpp"
#include "Objects/OpenCone.hpp"
#include "Objects/Torus.hpp"
#include "Objects/Compound.hpp"
#include "Objects/TriangleMesh.hpp"

#include "Cameras/Orthographic.hpp"
#include "Cameras/Pinhole.hpp"
#include "Cameras/ThinLens.hpp"
#include "Cameras/Sensor.hpp"


#include "Spectrum/RgbSpectrum.hpp"

#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/Normal.hpp"



using namespace iiit;



// specify path base for json test scene files
// path may differ for different build systems, default: "../src/Utilities/test/"
const std::string fileBase = "../src/Utilities/test/";



/*! \brief Unit test of parsing a sample scene description file.
 */
TEST(SceneParserTest, ParseGeometryTest)
{
    const std::string file = fileBase + "ParseGeometryTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;
    
    // parse data
    SceneParser sceneParser;

    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created objects
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    auto objects = scene->objects_;

    EXPECT_EQ(21, objects.size());
}



/*! \brief Unit test of parsing a sphere object.
 */
TEST(SceneParserTest, ParseSphereTest)
{
    const std::string file = fileBase + "ParseGeometryTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;
    
    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created objects
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    auto objects = scene->objects_;

    ASSERT_EQ(21, objects.size());

    ASSERT_EQ(GeometricObject<RgbSpectrum>::Type::Sphere, objects.at(0)->getType());
    std::shared_ptr<Sphere<RgbSpectrum> > sphere(std::dynamic_pointer_cast<Sphere<RgbSpectrum> >(objects.at(0)));
    EXPECT_EQ(0.5, sphere->getRadius());
    EXPECT_EQ(Point3D(0.0, 0.0, 0.0), sphere->getCenter());

    ASSERT_EQ(GeometricObject<RgbSpectrum>::Type::Sphere, objects.at(1)->getType());
    std::shared_ptr<Sphere<RgbSpectrum> > sphere2(std::dynamic_pointer_cast<Sphere<RgbSpectrum> >(objects.at(1)));
    EXPECT_EQ(0.5, sphere2->getRadius());
    EXPECT_EQ(Point3D(0.0, 0.0, 0.0), sphere2->getCenter());
}



/*! \brief Unit test of parsing a plane object.
 */
TEST(SceneParserTest, ParsePlaneTest)
{
    const std::string file = fileBase + "ParseGeometryTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;
    
    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created objects
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    auto objects = scene->objects_;

    ASSERT_EQ(21, objects.size());

    ASSERT_EQ(GeometricObject<RgbSpectrum>::Type::Plane, objects.at(2)->getType());
    std::shared_ptr<Plane<RgbSpectrum> > plane(std::dynamic_pointer_cast<Plane<RgbSpectrum> >(objects.at(2)));
    EXPECT_EQ(Point3D(1.0, 0.0, 0.0), plane->getPoint());
    EXPECT_EQ(Normal(1.0, 0.0, 0.0), plane->getNormal());

    ASSERT_EQ(GeometricObject<RgbSpectrum>::Type::Plane, objects.at(3)->getType());
    std::shared_ptr<Plane<RgbSpectrum> > plane2(std::dynamic_pointer_cast<Plane<RgbSpectrum> >(objects.at(3)));
    EXPECT_EQ(Point3D(1.0, 0.0, 0.0), plane2->getPoint());
    EXPECT_EQ(Normal(1.0, 0.0, 0.0), plane2->getNormal());
}



/*! \brief Unit test of parsing a triangle object.
 */
TEST(SceneParserTest, ParseTriangleTest)
{
    const std::string file = fileBase + "ParseGeometryTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;
    
    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created objects
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    auto objects = scene->objects_;

    ASSERT_EQ(21, objects.size());

    ASSERT_EQ(GeometricObject<RgbSpectrum>::Type::Triangle, objects.at(4)->getType());
    std::shared_ptr<Triangle<RgbSpectrum> > triangle(std::dynamic_pointer_cast<Triangle<RgbSpectrum> >(objects.at(4)));
    EXPECT_EQ(Point3D(-1.0, -1.0, 0.0), triangle->vertex0_);
    EXPECT_EQ(Point3D(1.0, 1.0, 1.0), triangle->vertex1_);
    EXPECT_EQ(Point3D(1.0, -1.0, 1.0), triangle->vertex2_);

    ASSERT_EQ(GeometricObject<RgbSpectrum>::Type::Triangle, objects.at(5)->getType());
    std::shared_ptr<Triangle<RgbSpectrum> > triangle2(std::dynamic_pointer_cast<Triangle<RgbSpectrum> >(objects.at(5)));
    EXPECT_EQ(Point3D(-1.0, -1.0, 0.0), triangle2->vertex0_);
    EXPECT_EQ(Point3D(1.0, 1.0, 1.0), triangle2->vertex1_);
    EXPECT_EQ(Point3D(1.0, -1.0, 1.0), triangle2->vertex2_);
}



/*! \brief Unit test of parsing a box object.
 */
TEST(SceneParserTest, ParseBoxTest)
{
    const std::string file = fileBase + "ParseGeometryTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;
    
    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created objects
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    auto objects = scene->objects_;

    ASSERT_EQ(21, objects.size());

    ASSERT_EQ(GeometricObject<RgbSpectrum>::Type::Box, objects.at(6)->getType());
    std::shared_ptr<Box<RgbSpectrum> > box(std::dynamic_pointer_cast<Box<RgbSpectrum> >(objects.at(6)));
    EXPECT_EQ(Point3D(-1.0, -1.0, 0.0), box->getVertex0());
    EXPECT_EQ(Point3D(1.0, 1.0, 1.0), box->getVertex1());

    ASSERT_EQ(GeometricObject<RgbSpectrum>::Type::Box, objects.at(7)->getType());
    std::shared_ptr<Box<RgbSpectrum> > box2(std::dynamic_pointer_cast<Box<RgbSpectrum> >(objects.at(7)));
    EXPECT_EQ(Point3D(-1.0, -1.0, 0.0), box2->getVertex0());
    EXPECT_EQ(Point3D(1.0, 1.0, 1.0), box2->getVertex1());
}



/*! \brief Unit test of parsing a disk object.
 */
TEST(SceneParserTest, ParseDiskTest)
{
    const std::string file = fileBase + "ParseGeometryTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;
    
    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created objects
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    auto objects = scene->objects_;

    ASSERT_EQ(21, objects.size());

    ASSERT_EQ(GeometricObject<RgbSpectrum>::Type::Disk, objects.at(8)->getType());
    std::shared_ptr<Disk<RgbSpectrum> > disk(std::dynamic_pointer_cast<Disk<RgbSpectrum> >(objects.at(8)));
    EXPECT_EQ(Point3D(0.0, 0.0, 0.0), disk->getPoint());
    EXPECT_EQ(Normal(0.0, 0.0, 1.0), disk->getNormal());
    EXPECT_EQ(1.0, disk->getRadius());

    ASSERT_EQ(GeometricObject<RgbSpectrum>::Type::Disk, objects.at(9)->getType());
    std::shared_ptr<Disk<RgbSpectrum> > disk2(std::dynamic_pointer_cast<Disk<RgbSpectrum> >(objects.at(9)));
    EXPECT_EQ(Point3D(0.0, 0.0, 0.0), disk2->getPoint());
    EXPECT_EQ(Normal(0.0, 0.0, 1.0), disk2->getNormal());
    EXPECT_EQ(1.0, disk->getRadius());
}



/*! \brief Unit test of parsing a square object.
 */
TEST(SceneParserTest, ParseSquareTest)
{
    const std::string file = fileBase + "ParseGeometryTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;
    
    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created objects
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    auto objects = scene->objects_;

    ASSERT_EQ(21, objects.size());

    ASSERT_EQ(GeometricObject<RgbSpectrum>::Type::Square, objects.at(10)->getType());
    std::shared_ptr<Square<RgbSpectrum> > square(std::dynamic_pointer_cast<Square<RgbSpectrum> >(objects.at(10)));
    EXPECT_EQ(Point3D(0.0, 0.0, 0.0), square->getPoint());
    EXPECT_EQ(Vector3D(2.0, 0.0, 0.0), square->getA());
    EXPECT_EQ(Vector3D(0.0, 0.0, 1.0), square->getB());

    ASSERT_EQ(GeometricObject<RgbSpectrum>::Type::Square, objects.at(11)->getType());
    std::shared_ptr<Square<RgbSpectrum> > square2(std::dynamic_pointer_cast<Square<RgbSpectrum> >(objects.at(11)));
    EXPECT_EQ(Point3D(0.0, 0.0, 0.0), square2->getPoint());
    EXPECT_EQ(Vector3D(2.0, 0.0, 0.0), square2->getA());
    EXPECT_EQ(Vector3D(0.0, 0.0, 1.0), square2->getB());

    ASSERT_EQ(GeometricObject<RgbSpectrum>::Type::Square, objects.at(12)->getType());
    std::shared_ptr<Square<RgbSpectrum> > square3(std::dynamic_pointer_cast<Square<RgbSpectrum> >(objects.at(12)));
    EXPECT_EQ(Point3D(0.0, 0.0, 0.0), square3->getPoint());
    EXPECT_EQ(Vector3D(2.0, 0.0, 0.0), square3->getA());
    EXPECT_EQ(Vector3D(0.0, 0.0, 1.0), square3->getB());

    ASSERT_EQ(GeometricObject<RgbSpectrum>::Type::Square, objects.at(13)->getType());
    std::shared_ptr<Square<RgbSpectrum> > square4(std::dynamic_pointer_cast<Square<RgbSpectrum> >(objects.at(13)));
    EXPECT_EQ(Point3D(0.0, 0.0, 0.0), square4->getPoint());
    EXPECT_EQ(Vector3D(2.0, 0.0, 0.0), square4->getA());
    EXPECT_EQ(Vector3D(0.0, 0.0, 1.0), square4->getB());
}



/*! \brief Unit test of parsing a cylinder object.
 */
TEST(SceneParserTest, ParseCylinderTest)
{
    const std::string file = fileBase + "ParseGeometryTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;
    
    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created objects
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    auto objects = scene->objects_;

    ASSERT_EQ(21, objects.size());

    ASSERT_EQ(GeometricObject<RgbSpectrum>::Type::OpenCylinder, objects.at(14)->getType());
    std::shared_ptr<OpenCylinder<RgbSpectrum> > cylinder(std::dynamic_pointer_cast<OpenCylinder<RgbSpectrum> >(objects.at(14)));
    EXPECT_EQ(Point3D(0.0, 0.0, 0.0), cylinder->getCenter());
    EXPECT_EQ(0.5, cylinder->getRadius());
    EXPECT_EQ(0.0, cylinder->getZMin());
    EXPECT_EQ(1.0, cylinder->getZMax());

    ASSERT_EQ(GeometricObject<RgbSpectrum>::Type::OpenCylinder, objects.at(15)->getType());
    std::shared_ptr<OpenCylinder<RgbSpectrum> > cylinder2(std::dynamic_pointer_cast<OpenCylinder<RgbSpectrum> >(objects.at(15)));
    EXPECT_EQ(Point3D(0.0, 0.0, 0.0), cylinder2->getCenter());
    EXPECT_EQ(0.5, cylinder2->getRadius());
    EXPECT_EQ(0.0, cylinder2->getZMin());
    EXPECT_EQ(1.0, cylinder2->getZMax());
}



/*! \brief Unit test of parsing a cone object.
 */
TEST(SceneParserTest, ParseConeTest)
{
    const std::string file = fileBase + "ParseGeometryTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;
    
    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created objects
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    auto objects = scene->objects_;

    ASSERT_EQ(21, objects.size());

    ASSERT_EQ(GeometricObject<RgbSpectrum>::Type::OpenCone, objects.at(16)->getType());
    std::shared_ptr<OpenCone<RgbSpectrum> > cone(std::dynamic_pointer_cast<OpenCone<RgbSpectrum> >(objects.at(16)));
    EXPECT_EQ(Point3D(0.0, 0.5, 0.15), cone->getCenter());
    EXPECT_EQ(0.1, cone->getMinRadius());
    EXPECT_EQ(0.0, cone->getMaxRadius());
    EXPECT_EQ(0.0, cone->getZMin());
    EXPECT_EQ(0.2, cone->getZMax());

    ASSERT_EQ(GeometricObject<RgbSpectrum>::Type::OpenCone, objects.at(17)->getType());
    std::shared_ptr<OpenCone<RgbSpectrum> > cone2(std::dynamic_pointer_cast<OpenCone<RgbSpectrum> >(objects.at(17)));
    EXPECT_EQ(Point3D(0.0, 0.5, 0.15), cone2->getCenter());
    EXPECT_EQ(0.1, cone2->getMinRadius());
    EXPECT_EQ(0.0, cone2->getMaxRadius());
    EXPECT_EQ(0.0, cone2->getZMin());
    EXPECT_EQ(0.2, cone2->getZMax());
}



/*! \brief Unit test of parsing a torus object.
 */
TEST(SceneParserTest, ParseTorusTest)
{
    const std::string file = fileBase + "ParseGeometryTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;
    
    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created objects
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    auto objects = scene->objects_;

    ASSERT_EQ(21, objects.size());

    ASSERT_EQ(GeometricObject<RgbSpectrum>::Type::Torus, objects.at(18)->getType());
    std::shared_ptr<Torus<RgbSpectrum> > torus(std::dynamic_pointer_cast<Torus<RgbSpectrum> >(objects.at(18)));
    EXPECT_EQ(0.25, torus->getInnerRadius());
    EXPECT_EQ(1.0, torus->getOuterRadius());

    ASSERT_EQ(GeometricObject<RgbSpectrum>::Type::Torus, objects.at(19)->getType());
    std::shared_ptr<Torus<RgbSpectrum> > torus2(std::dynamic_pointer_cast<Torus<RgbSpectrum> >(objects.at(19)));
    EXPECT_EQ(0.25, torus2->getInnerRadius());
    EXPECT_EQ(1.0, torus2->getOuterRadius());
}



/*! \brief Unit test of parsing a compound object.
 */
TEST(SceneParserTest, ParseCompoundTest)
{
    const std::string file = fileBase + "ParseGeometryTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;
    
    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created objects
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    auto objects = scene->objects_;

    ASSERT_EQ(21, objects.size());
    ASSERT_EQ(GeometricObject<RgbSpectrum>::Type::Compound, objects.at(20)->getType());

    // nothing else to test
}



/*! \brief Unit test of parsing an Instance object.
 */
TEST(SceneParserTest, ParseInstanceTest)
{
    const std::string file = fileBase + "ParseInstanceTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;

    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created objects
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum>>(abstractScene);
    auto objects = scene->objects_;

    ASSERT_EQ(7, objects.size());
    for (unsigned int i = 0; i < 7; ++i)
    {
        EXPECT_EQ(GeometricObject<RgbSpectrum>::Type::Sphere, objects.at(i)->getType());
    }
}



/*! \brief Unit test of parsing Instances of the same object.
 */
TEST(SceneParserTest, ParseInstancesTest)
{
    const std::string file = fileBase + "ParseInstancesTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;

    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created objects
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum>>(abstractScene);
    auto objects = scene->objects_;

    ASSERT_EQ(7, objects.size());
    for (unsigned int i = 0; i < 7; ++i)
    {
        EXPECT_EQ(GeometricObject<RgbSpectrum>::Type::Sphere, objects.at(i)->getType());
    }
}



/*! \brief Unit test of parsing a Grid object.
 */
TEST(SceneParserTest, ParseGridTest)
{
    const std::string file = fileBase + "ParseGridTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;

    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created objects
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum>>(abstractScene);
    auto objects = scene->objects_;

    EXPECT_EQ(1, objects.size());
}



/*! \brief Unit test of parsing a texture.
 */
TEST(SceneParserTest, ParseTextureTest)
{
    const std::string file = fileBase + "ParseTextureTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;
    
    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check textures of created objects
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    auto objects = scene->objects_;
    
    EXPECT_EQ(4, objects.size());
    ///< todo: Man kann hier leider nicht auf die zugrunde liegenden Texturen zugreifen.
}



/*! \brief Unit test of parsing an orthographic camera.
 */
TEST(SceneParserTest, ParseOrthographicTest)
{
    const std::string file = fileBase + "ParseOrthographicTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;
    
    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created camera
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
//    std::shared_ptr<Camera<RgbSpectrum> > camera(scene->getCamera().lock());
//    EXPECT_EQ(Point3D(0.0, 0.0, 0.25), camera->getEye());
//    EXPECT_EQ(Point3D(5.0, 0.0, 0.25), camera->getLookAt());
//    EXPECT_EQ(Vector3D(0.0, 0.0, 1.0), camera->getUp());
    EXPECT_EQ(512, scene->viewPlane_.uAbsRes_);
    EXPECT_EQ(512, scene->viewPlane_.vAbsRes_);
    EXPECT_EQ(0.0004, scene->viewPlane_.s_);
//    EXPECT_EQ(CameraType::Type::Orthographic, camera->getType());
}



/*! \brief Unit test of parsing a pinhole camera.
 */
TEST(SceneParserTest, ParsePinholeTest)
{
    const std::string file = fileBase + "ParsePinholeTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;
    
    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created camera
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
//    std::shared_ptr<Camera<RgbSpectrum> > camera(scene->getCamera().lock());
//    EXPECT_EQ(Point3D(0.0, 0.0, 0.25), camera->getEye());
//    EXPECT_EQ(Point3D(5.0, 0.0, 0.25), camera->getLookAt());
//    EXPECT_EQ(Vector3D(0.0, 0.0, 1.0), camera->getUp());
    EXPECT_EQ(512, scene->viewPlane_.uAbsRes_);
    EXPECT_EQ(512, scene->viewPlane_.vAbsRes_);
    EXPECT_EQ(0.0004, scene->viewPlane_.s_);
//    ASSERT_EQ(CameraType::Type::Pinhole, camera->getType());
//    std::shared_ptr<Pinhole<RgbSpectrum> > pinhole(std::dynamic_pointer_cast<Pinhole<RgbSpectrum> >(camera));
//    EXPECT_EQ(0.1, pinhole->getFocalLength());
}



/*! \brief Unit test of parsing a thin lens camera.
 */
TEST(SceneParserTest, ParseThinLensTest)
{
    const std::string file = fileBase + "ParseThinLensTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;
    
    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created camera
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
//    std::shared_ptr<Camera<RgbSpectrum> > camera(scene->getCamera().lock());
//    EXPECT_EQ(Point3D(0.0, 0.0, 0.25), camera->getEye());
//    EXPECT_EQ(Point3D(5.0, 0.0, 0.25), camera->getLookAt());
//    EXPECT_EQ(Vector3D(0.0, 0.0, 1.0), camera->getUp());
    EXPECT_EQ(512, scene->viewPlane_.uAbsRes_);
    EXPECT_EQ(512, scene->viewPlane_.vAbsRes_);
    EXPECT_EQ(0.0004, scene->viewPlane_.s_);
//    ASSERT_EQ(CameraType::Type::ThinLens, camera->getType());
//    std::shared_ptr<ThinLens<RgbSpectrum> > thinLens(std::dynamic_pointer_cast<ThinLens<RgbSpectrum> >(camera));
//    EXPECT_FLOAT_EQ(0.02f, thinLens->getLensRadius());
//    EXPECT_FLOAT_EQ(0.03f, thinLens->getImageDistance());
//    EXPECT_EQ(2.0, thinLens->getFocusDistance());
}



/*! \brief Unit test of parsing a light field camera.
 */
TEST(SceneParserTest, ParseLightFieldCameraTest)
{
    const std::string file = fileBase + "ParseLightFieldCameraTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;

    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created camera
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    //    std::shared_ptr<Camera<RgbSpectrum> > camera(scene->getCamera().lock());
    //    EXPECT_EQ(Point3D(0.0, 0.0, 0.25), camera->getEye());
    //    EXPECT_EQ(Point3D(5.0, 0.0, 0.25), camera->getLookAt());
    //    EXPECT_EQ(Vector3D(0.0, 0.0, 1.0), camera->getUp());
    EXPECT_EQ(512, scene->viewPlane_.uAbsRes_);
    EXPECT_EQ(512, scene->viewPlane_.vAbsRes_);
    EXPECT_EQ(0.0004, scene->viewPlane_.s_);
    //    ASSERT_EQ(CameraType::Type::LightFieldCamera, camera->getType());
    //    std::shared_ptr<LightFieldCamera<RgbSpectrum> > lightFieldCamera(std::dynamic_pointer_cast<LightFieldCamera<RgbSpectrum> >(camera));
    //    EXPECT_FLOAT_EQ(0.02f, lightFieldCamera->getLensRadius());
    //    EXPECT_FLOAT_EQ(0.03f, lightFieldCamera->getImageDistance());
    //    EXPECT_EQ(2.0, lightFieldCamera->getFocusDistance());
}



/*! \brief Unit test of parsing a real lens camera camera.
 */
TEST(SceneParserTest, ParseRealLensTest)
{
    const std::string file = fileBase + "ParseRealLensTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;

    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created camera
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    //    std::shared_ptr<Camera<RgbSpectrum> > camera(scene->getCamera().lock());
    //    EXPECT_EQ(Point3D(0.0, 0.0, 0.25), camera->getEye());
    //    EXPECT_EQ(Point3D(5.0, 0.0, 0.25), camera->getLookAt());
    //    EXPECT_EQ(Vector3D(0.0, 0.0, 1.0), camera->getUp());
    EXPECT_EQ(512, scene->viewPlane_.uAbsRes_);
    EXPECT_EQ(512, scene->viewPlane_.vAbsRes_);
    EXPECT_EQ(0.0004, scene->viewPlane_.s_);
    //    ASSERT_EQ(CameraType::Type::RealLensCamera, camera->getType());
    //    std::shared_ptr<RealLensCamera<RgbSpectrum> > realLens(std::dynamic_pointer_cast<RealLensCamera<RgbSpectrum> >(camera));
    //    EXPECT_FLOAT_EQ(0.02f, realLens->getLensRadius());
    //    EXPECT_FLOAT_EQ(0.03f, realLens->getImageDistance());
    //    EXPECT_EQ(2.0, realLens->getFocusDistance());


    // alternative factory method
    const std::string file2 = fileBase + "ParseRealLensTest2.json";

    // parse data
    EXPECT_EQ(sceneParser.parse(file2.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created camera
    scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    //    std::shared_ptr<Camera<RgbSpectrum> > camera(scene->getCamera().lock());
    //    EXPECT_EQ(Point3D(0.0, 0.0, 0.25), camera->getEye());
    //    EXPECT_EQ(Point3D(5.0, 0.0, 0.25), camera->getLookAt());
    //    EXPECT_EQ(Vector3D(0.0, 0.0, 1.0), camera->getUp());
    EXPECT_EQ(512, scene->viewPlane_.uAbsRes_);
    EXPECT_EQ(512, scene->viewPlane_.vAbsRes_);
    EXPECT_EQ(0.0004, scene->viewPlane_.s_);
    //    ASSERT_EQ(CameraType::Type::RealLensCamera, camera->getType());
    //    std::shared_ptr<RealLensCamera<RgbSpectrum> > realLens(std::dynamic_pointer_cast<RealLensCamera<RgbSpectrum> >(camera));
    //    EXPECT_FLOAT_EQ(0.02f, realLens->getLensRadius());
    //    EXPECT_FLOAT_EQ(0.03f, realLens->getImageDistance());
    //    EXPECT_EQ(2.0, realLens->getFocusDistance());
}



/*! \brief Unit test of parsing a chromatic real lens camera.
 */
TEST(SceneParserTest, ParseChromaticRealLensTest)
{
    const std::string file = fileBase + "ParseChromaticRealLensTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;

    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created camera
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    //    std::shared_ptr<Camera<RgbSpectrum> > camera(scene->getCamera().lock());
    //    EXPECT_EQ(Point3D(0.0, 0.0, 0.25), camera->getEye());
    //    EXPECT_EQ(Point3D(5.0, 0.0, 0.25), camera->getLookAt());
    //    EXPECT_EQ(Vector3D(0.0, 0.0, 1.0), camera->getUp());
    EXPECT_EQ(512, scene->viewPlane_.uAbsRes_);
    EXPECT_EQ(512, scene->viewPlane_.vAbsRes_);
    EXPECT_EQ(0.0004, scene->viewPlane_.s_);
    //    ASSERT_EQ(CameraType::Type::ChromaticRealLensCamera, camera->getType());
    //    std::shared_ptr<ChromaticRealLensCamera<RgbSpectrum> > chromaticRealLens(std::dynamic_pointer_cast<ChromaticRealLensCamera<RgbSpectrum> >(camera));
    //    EXPECT_FLOAT_EQ(0.02f, chromaticRealLens->getLensRadius());
    //    EXPECT_FLOAT_EQ(0.03f, chromaticRealLens->getImageDistance());
    //    EXPECT_EQ(2.0, chromaticRealLens->getFocusDistance());


    // alternative factory method
    const std::string file2 = fileBase + "ParseChromaticRealLensTest2.json";

    // parse data
    EXPECT_EQ(sceneParser.parse(file2.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created camera
    scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    //    std::shared_ptr<Camera<RgbSpectrum> > camera(scene->getCamera().lock());
    //    EXPECT_EQ(Point3D(0.0, 0.0, 0.25), camera->getEye());
    //    EXPECT_EQ(Point3D(5.0, 0.0, 0.25), camera->getLookAt());
    //    EXPECT_EQ(Vector3D(0.0, 0.0, 1.0), camera->getUp());
    EXPECT_EQ(512, scene->viewPlane_.uAbsRes_);
    EXPECT_EQ(512, scene->viewPlane_.vAbsRes_);
    EXPECT_EQ(0.0004, scene->viewPlane_.s_);
    //    ASSERT_EQ(CameraType::Type::ChromaticRealLensCamera, camera->getType());
    //    std::shared_ptr<ChromaticRealLensCamera<RgbSpectrum> > realLens(std::dynamic_pointer_cast<ChromaticRealLensCamera<RgbSpectrum> >(camera));
    //    EXPECT_FLOAT_EQ(0.02f, chromaticRealLens->getLensRadius());
    //    EXPECT_FLOAT_EQ(0.03f, chromaticRealLens->getImageDistance());
    //    EXPECT_EQ(2.0, chromaticRealLens->getFocusDistance());

    // alternative factory method
    const std::string file3 = fileBase + "ParseChromaticRealLensTest3.json";

    // parse data
    EXPECT_EQ(sceneParser.parse(file3.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created camera
    scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    //    std::shared_ptr<Camera<RgbSpectrum> > camera(scene->getCamera().lock());
    //    EXPECT_EQ(Point3D(0.0, 0.0, 0.25), camera->getEye());
    //    EXPECT_EQ(Point3D(5.0, 0.0, 0.25), camera->getLookAt());
    //    EXPECT_EQ(Vector3D(0.0, 0.0, 1.0), camera->getUp());
    EXPECT_EQ(512, scene->viewPlane_.uAbsRes_);
    EXPECT_EQ(512, scene->viewPlane_.vAbsRes_);
    EXPECT_EQ(0.0004, scene->viewPlane_.s_);
    //    ASSERT_EQ(CameraType::Type::ChromaticRealLensCamera, camera->getType());
    //    std::shared_ptr<ChromaticRealLensCamera<RgbSpectrum> > realLens(std::dynamic_pointer_cast<ChromaticRealLensCamera<RgbSpectrum> >(camera));
    //    EXPECT_FLOAT_EQ(0.02f, chromaticRealLens->getLensRadius());
    //    EXPECT_FLOAT_EQ(0.03f, chromaticRealLens->getImageDistance());
    //    EXPECT_EQ(2.0, chromaticRealLens->getFocusDistance());

    // alternative factory method
    const std::string file4 = fileBase + "ParseChromaticRealLensTest4.json";

    // parse data
    EXPECT_EQ(sceneParser.parse(file4.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created camera
    scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    //    std::shared_ptr<Camera<RgbSpectrum> > camera(scene->getCamera().lock());
    //    EXPECT_EQ(Point3D(0.0, 0.0, 0.25), camera->getEye());
    //    EXPECT_EQ(Point3D(5.0, 0.0, 0.25), camera->getLookAt());
    //    EXPECT_EQ(Vector3D(0.0, 0.0, 1.0), camera->getUp());
    EXPECT_EQ(512, scene->viewPlane_.uAbsRes_);
    EXPECT_EQ(512, scene->viewPlane_.vAbsRes_);
    EXPECT_EQ(0.0004, scene->viewPlane_.s_);
    //    ASSERT_EQ(CameraType::Type::ChromaticRealLensCamera, camera->getType());
    //    std::shared_ptr<ChromaticRealLensCamera<RgbSpectrum> > realLens(std::dynamic_pointer_cast<ChromaticRealLensCamera<RgbSpectrum> >(camera));
    //    EXPECT_FLOAT_EQ(0.02f, chromaticRealLens->getLensRadius());
    //    EXPECT_FLOAT_EQ(0.03f, chromaticRealLens->getImageDistance());
    //    EXPECT_EQ(2.0, chromaticRealLens->getFocusDistance());

    // alternative factory method
    const std::string file5 = fileBase + "ParseChromaticRealLensTest5.json";

    // parse data
    EXPECT_EQ(sceneParser.parse(file5.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created camera
    scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    //    std::shared_ptr<Camera<RgbSpectrum> > camera(scene->getCamera().lock());
    //    EXPECT_EQ(Point3D(0.0, 0.0, 0.25), camera->getEye());
    //    EXPECT_EQ(Point3D(5.0, 0.0, 0.25), camera->getLookAt());
    //    EXPECT_EQ(Vector3D(0.0, 0.0, 1.0), camera->getUp());
    EXPECT_EQ(512, scene->viewPlane_.uAbsRes_);
    EXPECT_EQ(512, scene->viewPlane_.vAbsRes_);
    EXPECT_EQ(0.0004, scene->viewPlane_.s_);
    //    ASSERT_EQ(CameraType::Type::ChromaticRealLensCamera, camera->getType());
    //    std::shared_ptr<ChromaticRealLensCamera<RgbSpectrum> > realLens(std::dynamic_pointer_cast<ChromaticRealLensCamera<RgbSpectrum> >(camera));
    //    EXPECT_FLOAT_EQ(0.02f, chromaticRealLens->getLensRadius());
    //    EXPECT_FLOAT_EQ(0.03f, chromaticRealLens->getImageDistance());
    //    EXPECT_EQ(2.0, chromaticRealLens->getFocusDistance());
}



/*! \brief Unit test of parsing image noise parameters camera.
 */
TEST(SceneParserTest, ParseImageNoiseTest)
{
    const std::string file = fileBase + "ParseImageNoiseTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;

    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created camera
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    EXPECT_EQ(512, scene->viewPlane_.uAbsRes_);
    EXPECT_EQ(512, scene->viewPlane_.vAbsRes_);
    EXPECT_EQ(0.0004, scene->viewPlane_.s_);
}



/*! \brief Unit test of parsing a ply model.
 */
TEST(SceneParserTest, PlyTest)
{
    const std::string file = fileBase + "PlyTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;
    
    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created objects
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    auto objects = scene->objects_;

    EXPECT_EQ(3, objects.size());    
    ASSERT_EQ(GeometricObject<RgbSpectrum>::Type::TriangleMesh, objects.at(0)->getType());
    std::shared_ptr<TriangleMesh<RgbSpectrum> > triangleMesh(std::static_pointer_cast<TriangleMesh<RgbSpectrum> >(objects[0]));
    EXPECT_EQ(Point3D(-1.0, -1.0, 1.0), triangleMesh->mesh_->vertices_[0]);
    EXPECT_EQ(Point3D(1.0, -1.0, 0.0), triangleMesh->mesh_->vertices_[1]);
    EXPECT_EQ(Point3D(1.0, 1.0, 1.0), triangleMesh->mesh_->vertices_[2]);
    EXPECT_EQ(Point3D(-1.0, 1.0, 0.0), triangleMesh->mesh_->vertices_[3]);
}



/*! \brief Unit test of including JSON files.
 */
TEST(SceneParserTest, InlcudeTest)
{
    const std::string file = fileBase + "IncludeTest1.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;
    
    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created objects
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    auto objects = scene->objects_;

    EXPECT_EQ(1, objects.size());    
    EXPECT_EQ(GeometricObject<RgbSpectrum>::Type::TriangleMesh, objects.at(0)->getType());
}



/*! \brief Unit test of parsing an aperture.
 */
TEST(SceneParserTest, ParseApertureTest)
{
    const std::string file = fileBase + "ParseApertureTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;

    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";


    // check created objects
    std::shared_ptr<iiit::Scene<RgbSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<RgbSpectrum> >(abstractScene);
    auto objects = scene->objects_;
//    std::shared_ptr<Camera<RgbSpectrum> > camera(scene->getCamera().lock());
//    EXPECT_EQ(Point3D(0.0, 0.0, 0.25), camera->getEye());
//    EXPECT_EQ(Point3D(5.0, 0.0, 0.25), camera->getLookAt());
//    EXPECT_EQ(Vector3D(0.0, 0.0, 1.0), camera->getUp());
    EXPECT_EQ(512, scene->viewPlane_.uAbsRes_);
    EXPECT_EQ(512, scene->viewPlane_.vAbsRes_);
    EXPECT_EQ(0.0004, scene->viewPlane_.s_);
//    ASSERT_EQ(CameraType::Type::ApertureCamera, camera->getType());
//    std::shared_ptr<ApertureCamera<RgbSpectrum> > aperture_camera(std::dynamic_pointer_cast<ApertureCamera<RgbSpectrum> >(camera));
//    EXPECT_FLOAT_EQ(0.02f, aperture_camera->getLensRadius());
//    EXPECT_FLOAT_EQ(0.03f, aperture_camera->getImageDistance());
//    EXPECT_NE(aperture_camera->getAperture(), std::nullptr_t());

    //for(int n=1; n<100; n++)
    //{
    //    Ray ray(Point3D(sin(n*2*PI/100)*0.99, cos(n*2*PI/100), 0)*0.99, Vector3D(0,0,1));
    //    EXPECT_EQ(true, aperture_camera->getAperture()->checkRay(ray, 1));
    //}
}



/*! \brief Unit test of parsing different lights.
 */
TEST(SceneParserTest, ParseLightsTest)
{
    const std::string file = fileBase + "ParseLightsTest.json";
    
    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;
    
    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(true, sceneParser.parse(file.c_str(), abstractScene, metaData));

    const std::string file2 = fileBase + "ParseLightsTest2.json";
    EXPECT_EQ(true, sceneParser.parse(file2.c_str(), abstractScene, metaData));
}



/*! \brief Unit test of parsing different materials.
 */
TEST(SceneParserTest, ParseMaterialsTest)
{
    const std::string file = fileBase + "ParseMaterialsTest.json";
    
    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;
    
    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(true, sceneParser.parse(file.c_str(), abstractScene, metaData));
}



/*! \brief Unit test of parsing different textures.
 */
TEST(SceneParserTest, ParseTexturesTest)
{
    const std::string file = fileBase + "ParseTexturesTest.json";
    
    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;
    
    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(true, sceneParser.parse(file.c_str(), abstractScene, metaData));
}



/*! \brief Unit test of parsing different samplers.
 */
TEST(SceneParserTest, ParseSamplersTest)
{
    const std::string file = fileBase + "ParseSamplersTest.json"; ///< \todo Vary seed specification
    
    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;
    
    // parse data
    SceneParser sceneParser;
    EXPECT_EQ(true, sceneParser.parse(file.c_str(), abstractScene, metaData));
}


/*! \brief Unit test of parsing ideal sensor.
 */
TEST(SceneParserTest, ParseSensorIdealTest)
{
    const std::string file = fileBase + "ParseSensorIdealTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;

    // parse data
    // first ideal Sensor
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";


    // check created camera
    std::shared_ptr<iiit::Scene<SampledSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<SampledSpectrum> >(abstractScene);


    const SampledSpectrum referenceResponsivity = SampledSpectrum::fromSampled(iiit::idealResponsivityLambdaSamples, iiit::idealResponsivityValueSamples);


    EXPECT_EQ("Ideal", abstractScene->getSensorName());
    EXPECT_EQ( *(scene->getCamera()->getSensor().getResponsivity())[0], referenceResponsivity);


}


/*! \brief Unit test of parsing Sony ICX618 sensor.
 */
TEST(SceneParserTest, ParseSensorICX618Test)
{
    const std::string file = fileBase + "ParseSensorICX618Test.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;

    // parse data
    // first ideal Sensor
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";


    // check created camera
    std::shared_ptr<iiit::Scene<SampledSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<SampledSpectrum> >(abstractScene);


    const SampledSpectrum referenceResponsivity = SampledSpectrum::fromSampled(iiit::ccdSonyICX618ResponsivityLambdaSamples, iiit::ccdSonyICX618ResponsivityValueSamples);


    EXPECT_EQ("CCD SONY ICX618", abstractScene->getSensorName());
    EXPECT_EQ( *(scene->getCamera()->getSensor().getResponsivity())[0], referenceResponsivity);

}



/*! \brief Unit test of parsing Sony ICX814 sensor.
 */
TEST(SceneParserTest, ParseSensorICX814Test)
{
    const std::string file = fileBase + "ParseSensorICX814Test.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;

    // parse data
    // first ideal Sensor
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";


    // check created camera
    std::shared_ptr<iiit::Scene<SampledSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<SampledSpectrum> >(abstractScene);


    const SampledSpectrum referenceResponsivity = SampledSpectrum::fromSampled(iiit::ccdSonyICX814ResponsivityLambdaSamples, iiit::ccdSonyICX814ResponsivityValueSamples);


    EXPECT_EQ("CCD SONY ICX814", abstractScene->getSensorName());
    EXPECT_EQ( *(scene->getCamera()->getSensor().getResponsivity())[0], referenceResponsivity);

}



/*! \brief Unit test of parsing ON-Semiconductor KAI 4050 sensor.
 */
TEST(SceneParserTest, ParseSensorKAI4050Test)
{
    const std::string file = fileBase + "ParseSensorKAI4050Test.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;

    // parse data
    // first ideal Sensor
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";


    // check created camera
    std::shared_ptr<iiit::Scene<SampledSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<SampledSpectrum> >(abstractScene);


    const SampledSpectrum referenceResponsivity = SampledSpectrum::fromSampled(iiit::ccdOnKAI4050ResponsivityLambdaSamples, iiit::ccdOnKAI4050ResponsivityValueSamples);


    EXPECT_EQ("CCD ON-Semiconductor KAI 4050", abstractScene->getSensorName());
    EXPECT_EQ( *(scene->getCamera()->getSensor().getResponsivity())[0], referenceResponsivity);

}



/*! \brief Unit test of parsing ON-Semiconductor KAI 16070 sensor.
 */
TEST(SceneParserTest, ParseSensorKAI16070Test)
{
    const std::string file = fileBase + "ParseSensorKAI16070Test.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;

    // parse data
    // first ideal Sensor
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";


    // check created camera
    std::shared_ptr<iiit::Scene<SampledSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<SampledSpectrum> >(abstractScene);


    const SampledSpectrum referenceResponsivity = SampledSpectrum::fromSampled(iiit::ccdOnKAI16070ResponsivityLambdaSamples, iiit::ccdOnKAI16070ResponsivityValueSamples);


    EXPECT_EQ("CCD ON-Semiconductor KAI 16070", abstractScene->getSensorName());
    EXPECT_EQ( *(scene->getCamera()->getSensor().getResponsivity())[0], referenceResponsivity);

}



/*! \brief Unit test of parsing Sony IMX174 sensor.
 */
TEST(SceneParserTest, ParseSensorIMX174Test)
{
    const std::string file = fileBase + "ParseSensorIMX174Test.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;

    // parse data
    // first ideal Sensor
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";


    // check created camera
    std::shared_ptr<iiit::Scene<SampledSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<SampledSpectrum> >(abstractScene);


    const SampledSpectrum referenceResponsivity = SampledSpectrum::fromSampled(iiit::cmosSonyIMX174ResponsivityLambdaSamples, iiit::cmosSonyIMX174ResponsivityValueSamples);


    EXPECT_EQ("CMOS SONY IMX174", abstractScene->getSensorName());
    EXPECT_EQ( *(scene->getCamera()->getSensor().getResponsivity())[0], referenceResponsivity);

}




/*! \brief Unit test of parsing ON-Semiconductor KAI 16070 Color sensor.
 */
TEST(SceneParserTest, ParseSensorKAI16070CTest)
{
    const std::string file = fileBase + "ParseSensorKAI16070CTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;

    // parse data
    // first ideal Sensor
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";


    // check created camera
    std::shared_ptr<iiit::Scene<SampledSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<SampledSpectrum> >(abstractScene);


    const SampledSpectrum referenceRedResponsivity = SampledSpectrum::fromSampled(iiit::ccdOnKAI16070RedResponsivityLambdaSamples, iiit::ccdOnKAI16070RedResponsivityValueSamples);
    const SampledSpectrum referenceGreenResponsivity = SampledSpectrum::fromSampled(iiit::ccdOnKAI16070GreenResponsivityLambdaSamples, iiit::ccdOnKAI16070GreenResponsivityValueSamples);
    const SampledSpectrum referenceBlueResponsivity = SampledSpectrum::fromSampled(iiit::ccdOnKAI16070BlueResponsivityLambdaSamples, iiit::ccdOnKAI16070BlueResponsivityValueSamples);


    EXPECT_EQ("CCD ON-Semiconductor KAI 16070 Color", abstractScene->getSensorName());
    EXPECT_EQ( *(scene->getCamera()->getSensor().getResponsivity())[0], referenceRedResponsivity);
    EXPECT_EQ( *(scene->getCamera()->getSensor().getResponsivity())[1], referenceGreenResponsivity);
    EXPECT_EQ( *(scene->getCamera()->getSensor().getResponsivity())[2], referenceBlueResponsivity);

}




/*! \brief Unit test of parsing User specified sensor.
 */
TEST(SceneParserTest, ParseSensorOwnTest)
{
    const std::string file = fileBase + "ParseSensorOwnTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;

    // parse data
    // first ideal Sensor
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created camera
    std::shared_ptr<iiit::Scene<SampledSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<SampledSpectrum> >(abstractScene);

    const std::vector<float> tmpLambda = {400.f, 300.f, 600.f, 700.f, 444.f, 541.1f, 645.11f};
    const std::vector<float> tmpValue  = {1.f, 2.5f, 2.0f, 0.00156423f, 0.01f, 999.643f, 4.f};

    const SampledSpectrum referenceResponsivity = SampledSpectrum::fromSampled(tmpLambda, tmpValue);

    EXPECT_EQ("Own (User specified)", abstractScene->getSensorName());
    EXPECT_EQ( *(scene->getCamera()->getSensor().getResponsivity())[0], referenceResponsivity);


    const std::string file2 = fileBase + "ParseSensorOwnTest2.json";

    // parse data
    // first ideal Sensor
    EXPECT_EQ(sceneParser.parse(file2.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    // check created camera
    std::shared_ptr<iiit::Scene<SampledSpectrum> > scene2 = std::dynamic_pointer_cast<iiit::Scene<SampledSpectrum> >(abstractScene);

    EXPECT_EQ("Own (User specified)", abstractScene->getSensorName());
    EXPECT_EQ(*(scene2->getCamera()->getSensor().getResponsivity())[0], referenceResponsivity);
    EXPECT_EQ(*(scene2->getCamera()->getSensor().getResponsivity())[1], referenceResponsivity);
    EXPECT_EQ(*(scene2->getCamera()->getSensor().getResponsivity())[2], referenceResponsivity);
}



/*! \brief Unit test of parsing sensor with `Raw` image type.
 */
TEST(SceneParserTest, ParseSensorRawTest)
{
    const std::string file = fileBase + "ParseSensorRawTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;

    // parse data
    // first ideal Sensor
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";


    // check created camera
    std::shared_ptr<iiit::Scene<SampledSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<SampledSpectrum> >(abstractScene);




    EXPECT_EQ(1, abstractScene->getNumChannels());
    EXPECT_EQ(ImageType::Type::Raw, scene->getCamera()->getSensor().getImageType());

}


/*! \brief Unit test of parsing sensor with `Rgb` image type.
 */
TEST(SceneParserTest, ParseSensorRgbTest)
{
    const std::string file = fileBase + "ParseSensorRgbTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;

    // parse data
    // first ideal Sensor
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";


    // check created camera
    std::shared_ptr<iiit::Scene<SampledSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<SampledSpectrum> >(abstractScene);




    EXPECT_EQ(3, abstractScene->getNumChannels());
    EXPECT_EQ(ImageType::Type::Rgb, scene->getCamera()->getSensor().getImageType());

}



/*! \brief Unit test of parsing sensor with `IdealRgb` image type.
 */
TEST(SceneParserTest, ParseSensorIdealRgbTest)
{
    const std::string file = fileBase + "ParseSensorIdealRgbTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;

    // parse data
    // first ideal Sensor
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";


    // check created camera
    std::shared_ptr<iiit::Scene<SampledSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<SampledSpectrum> >(abstractScene);




    EXPECT_EQ(3, abstractScene->getNumChannels());
    EXPECT_EQ(ImageType::Type::IdealRgb, scene->getCamera()->getSensor().getImageType());

}


/*! \brief Unit test of parsing sensor with `MultiSpectral` image type.
 */
TEST(SceneParserTest, ParseSensorMultiSpectralTest)
{
    const std::string file = fileBase + "ParseSensorMultiSpectralTest.json";

    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;

    // parse data
    // first ideal Sensor
    SceneParser sceneParser;
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";


    // check created camera
    std::shared_ptr<iiit::Scene<SampledSpectrum> > scene = std::dynamic_pointer_cast<iiit::Scene<SampledSpectrum> >(abstractScene);




    EXPECT_EQ(iiit::numSpectralSamples, abstractScene->getNumChannels());
    EXPECT_EQ(ImageType::Type::MultiSpectral, scene->getCamera()->getSensor().getImageType());

}


/*! \brief Unit test of parsing different tracers.
 */
TEST(SceneParserTest, ParseTracerTest)
{
    // create new Scene pointer
    std::shared_ptr<AbstractScene> abstractScene;

    // scene meta data
    SceneMetaData metaData;

    // parser object
    SceneParser sceneParser;

    // parse data
    const std::string file = fileBase + "ParseTracerTest.json";
    EXPECT_EQ(sceneParser.parse(file.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    const std::string file2 = fileBase + "ParseTracerTest2.json";
    EXPECT_EQ(sceneParser.parse(file2.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    const std::string file3 = fileBase + "ParseTracerTest3.json";
    EXPECT_EQ(sceneParser.parse(file3.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    const std::string file4 = fileBase + "ParseTracerTest4.json";
    EXPECT_EQ(sceneParser.parse(file4.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    const std::string file5 = fileBase + "ParseTracerTest5.json";
    EXPECT_EQ(sceneParser.parse(file5.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";

    const std::string file6 = fileBase + "ParseTracerTest6.json";
    EXPECT_EQ(sceneParser.parse(file6.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";
    
    const std::string file7 = fileBase + "ParseTracerTest7.json";
    EXPECT_EQ(sceneParser.parse(file7.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";
    
    const std::string file8 = fileBase + "ParseTracerTest8.json";
    EXPECT_EQ(sceneParser.parse(file8.c_str(), abstractScene, metaData), true) << "Scene Parsing failed.";
}
