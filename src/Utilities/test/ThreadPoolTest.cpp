// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file ThreadPoolTest.cpp
*  \author Thomas Nuernberg
*  \date 21.04.2016
*  \brief Unit test of classes ThreadPool and Worker.
*/

#include <chrono>
#include <thread>
#include <mutex>

#include "gtest/gtest.h"

#include "ThreadPool.hpp"



using namespace iiit;



static int counter = 0; ///< Variable for counting the number of completed jobs.
std::mutex counterMutex; ///< Mutex guarding the counter access.



/*! \brief Example job for ThreadPool.
 */
void job()
{
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    counterMutex.lock();
    counter++;
    counterMutex.unlock();
}



/*! \brief Unit test for testing completion of ThreadPool.
 */
TEST(ThreadPoolTest, MultiThreadingTest)
{
    unsigned int numJobs = 20;
    counter = 0;

    int numWorkers = std::thread::hardware_concurrency();
    if (numWorkers == 1)
    {
        std::cout << "Warning: Current Hardware doesn't support multiple threads. Test results may be without significance" << std::endl;
    }

    // create ThreadPool
    std::shared_ptr<ThreadPool> pool = std::unique_ptr<ThreadPool>(new ThreadPool(numWorkers));

    for (unsigned int i = 0; i < numJobs; ++i)
    {
        pool->enqueue(&job);
    }

    pool->waitForWorkers();

    EXPECT_EQ(numJobs, counter);
}



/*! \brief Unit test for testing abortion of ThreadPool.
 */
TEST(ThreadPoolTest, MultiThreadingAbortTest)
{
    unsigned int numJobs = 200;
    counter = 0;

    int numWorkers = std::thread::hardware_concurrency();
    if (numWorkers == 1)
    {
        std::cout << "Warning: Current Hardware doesn't support multiple threads. Test results may be without significance" << std::endl;
    }

    // create ThreadPool
    std::shared_ptr<ThreadPool> pool = std::unique_ptr<ThreadPool>(new ThreadPool(numWorkers));

    for (unsigned int i = 0; i < numJobs; ++i)
    {
        pool->enqueue(&job);
    }

    pool.reset(); // destroy ThreadPool before jobs are finished

    EXPECT_NE(numJobs, counter);
}