# Project
project(raytracer)

# Add source files
set(SOURCE_JSONLOGGER
    ${CMAKE_CURRENT_SOURCE_DIR}/SceneLogger.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/SceneLogger.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/SessionLogger.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/SessionLogger.hpp
    PARENT_SCOPE
)

set(SOURCE_JSONPARSER
    ${CMAKE_CURRENT_SOURCE_DIR}/SceneParser.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/SceneParser.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/SessionParser.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/SessionParser.hpp
    PARENT_SCOPE
)

set(SOURCE_THREADPOOL
    ${CMAKE_CURRENT_SOURCE_DIR}/ThreadPool.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/ThreadPool.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Worker.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Worker.hpp
    PARENT_SCOPE
)