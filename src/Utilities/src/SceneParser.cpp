// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file SceneParser.cpp
 *  \author Thomas Nuernberg
 *  \date 13.01.2015
 *  \brief Implementation of class SceneParser.
 *
 *  Implementation of class SceneParser used to extract world data from a
 *  <a HREF="http://www.json.org">JSON</a> description.
 */

#include "SceneParser.hpp"

#include <new> // std::bad_alloc
#include <chrono>

// apertures
#include "Cameras/Aperture.hpp"
#include "Cameras/CircularAperture.hpp"
#include "Cameras/ImageAperture.hpp"

// samplers
#include "Sampler/PureRandom.hpp"
#include "Sampler/Jittered.hpp"
#include "Sampler/NRooks.hpp"
#include "Sampler/MultiJittered.hpp"
#include "Sampler/Hammersley.hpp"

// spectrum types
#include "Spectrum/Monochromatic.hpp"
#include "Spectrum/RgbSpectrum.hpp"
#include "Spectrum/SampledSpectrum.hpp"
#include "Spectrum/CoefficientSpectrum.hpp"



namespace iiit
{

    /*! \brief Default constructor.
     */
    SceneParser::SceneParser()
        : lightType_(SpectrumRepresentationType::IlluminationSpectrum)
        , materialType_(SpectrumRepresentationType::ReflectanceSpectrum)
    {
    }


    /*! \brief Default destructor.
     */
    SceneParser::~SceneParser()
    {
    }



    /*! \brief Parse scene description for geometric data.
     *  \param[in] sceneDescriptionFileName C-string of scene description file name.
     *  \param[out] scene Scene object to store scene data.
     *  \param[out] metaData Object to store scene meta data.
     *  \return Returns TRUE if parsing was successful, else FALSE.
     *
     *  Parses scene data from scene description in <a HREF="http://www.json.org">JSON</a> format.
     *  The data is not stored in the given Scene object until it is assured, that no errors occurred.
     */
    bool SceneParser::parse(const char* sceneDescriptionFileName, std::shared_ptr<AbstractScene>& scene, SceneMetaData& metaData)
    {
        // open scene description file
        std::ifstream stream(sceneDescriptionFileName, std::ifstream::in);
        if (!stream.is_open())
        {
            std::cout << "Cannot open file """ << sceneDescriptionFileName << """." <<std::endl;
            return false;
        }

        Json::Value root;
        Json::Reader reader;

        // read scene description
        if (!reader.parse(stream, root))
        {
            std::cout << "Failed to parse configuration" << std::endl << reader.getFormattedErrorMessages();
            stream.close();
            return false;
        }

        // close scene description file
        stream.close();

        // parse included files
        filePath_ = sceneDescriptionFileName;
        int pos = static_cast<int>(filePath_.find_last_of('/'));
        if (pos != std::string::npos)
        {
            filePath_.erase(pos + 1);
        }
        else
        {
            filePath_.clear();
        }
        
        if (!parseIncludedFiles(root))
        {
            return false;
        }
        
        // check geometry
        if (!checkKey(root, "geometry", PropertyType::ObjectValue, true) ||
            !checkKey(root["geometry"], "objects", PropertyType::ArrayValue, true))
        {
            return false;
        }

        // check camera
        if (!checkKey(root, "camera", PropertyType::ObjectValue, true))
        {
            return false;
        }

        // check lights
        if (!checkKey(root, "lights", PropertyType::ArrayValue, true))
        {
            return false;
        }

        // initialize SampledSpectrum 
        SampledSpectrum::init();

        try
        {
            if (checkKey(root, "spectrumType", PropertyType::StringValue, false) &&
                root["spectrumType"].asString().compare("SampledSpectrum") == 0)
            {
                // set global spectral types
                if (checkKey(root, "lightSpectralType", PropertyType::StringValue, false))
                {
                    if (root["lightSpectralType"].asString().compare("Reflectance") == 0)
                    {
                        lightType_ = SpectrumRepresentationType::ReflectanceSpectrum;
                    }
                    else if (root["lightSpectralType"].asString().compare("Illumination") == 0)
                    {
                        lightType_ = SpectrumRepresentationType::IlluminationSpectrum;
                    }
                    else
                    {
                        std::cout << "Unknown spectral type: " << root["lightsSpectralType"].asString() << std::endl;
                        return false;
                    }
                }
                if (checkKey(root, "materialSpectralType", PropertyType::StringValue, false))
                {
                    if (root["materialSpectralType"].asString().compare("Reflectance") == 0)
                    {
                        materialType_ = SpectrumRepresentationType::ReflectanceSpectrum;
                    }
                    else if (root["materialSpectralType"].asString().compare("Illumination") == 0)
                    {
                        materialType_ = SpectrumRepresentationType::IlluminationSpectrum;
                    }
                    else
                    {
                        std::cout << "Unknown spectral type: " << root["materialSpectralType"].asString() << std::endl;
                        return false;
                    }
                }


                std::shared_ptr<Scene<SampledSpectrum> > parsedScene(new Scene<SampledSpectrum>);
                if (!buildScene(root, parsedScene, metaData))
                {
                    return false;
                }
                scene = parsedScene;
            }
            else if (checkKey(root, "spectrumType", PropertyType::StringValue, false) &&
                root["spectrumType"].asString().compare("Monochromatic") == 0)
            {
                std::shared_ptr<Scene<Monochromatic> > parsedScene(new Scene<Monochromatic>);
                if (!buildScene(root, parsedScene, metaData))
                {
                    return false;
                }
                scene = parsedScene;
            }
            else
            {
                std::shared_ptr<Scene<RgbSpectrum> > parsedScene(new Scene<RgbSpectrum>);
                if (!buildScene(root, parsedScene, metaData))
                {
                    return false;
                }
                scene = parsedScene;
            }
        }
        catch (std::bad_alloc& ba)
        {
            std::cout << "bad_alloc exception caught: " << ba.what() << std::endl;
            std::cout << "Possible reasons:" << std::endl;
            std::cout << "  - There is not enough memory." << std::endl;
            std::cout << "  - The memory is fragmented." << std::endl;
            std::cout << "  - In case of a 32 bit build: More than 2 GB memory are required." << std::endl;
            return false;
        }
        
        // parsing successful -> keep scene data
        return true;
    }



    /*! \brief Extracts camera position and orientation of camera.
     *  \param[in] cameraDescription <a HREF="http://www.json.org">JSON</a> value of camera description.
     *  \param[out] eye Point in world coordinates defining the origin of the camera coordinate system.
     *  \param[out] lookAt Point in world coordinates that the camera looks at.
     *  \param[out] up Vector in world coordinates pointing in the up direction of the camera.
     *  \return Returns TRUE if building was successful, else FALSE.
     */
    bool SceneParser::buildCameraState(const Json::Value& cameraDescription, Point3D& eye, Point3D& lookAt, Vector3D& up)
    {
        // ensure required properties are defined and of correct type
        if (!checkKey(cameraDescription, "eye", PropertyType::VectorValue, true) ||
            !checkKey(cameraDescription, "lookAt", PropertyType::VectorValue, true) ||
            !checkKey(cameraDescription, "up", PropertyType::VectorValue, true) ||
            !checkDimension(cameraDescription["eye"], 3, true) ||
            !checkDimension(cameraDescription["lookAt"], 3, true) ||
            !checkDimension(cameraDescription["up"], 3, true))
        {
            return false;
        }

        // extract data
        eye[0] = cameraDescription["eye"][0].asDouble();
        eye[1] = cameraDescription["eye"][1].asDouble();
        eye[2] = cameraDescription["eye"][2].asDouble();
        lookAt[0] = cameraDescription["lookAt"][0].asDouble();
        lookAt[1] = cameraDescription["lookAt"][1].asDouble();
        lookAt[2] = cameraDescription["lookAt"][2].asDouble();
        up[0] = cameraDescription["up"][0].asDouble();
        up[1] = cameraDescription["up"][1].asDouble();
        up[2] = cameraDescription["up"][2].asDouble();

        // successfully extracted data.
        return true;
    }



    /*! \brief Calculates optical parameters of a camera.
     *  \param[in] cameraDescription <a HREF="http://www.json.org">JSON</a> value of camera description.
     *  \param[out] imageDistance Distance between sensor and optical center.
     *  \param[out] focusDistance Distance of objects in focus.
     *  \return Returns TRUE if calculation was successful, else FALSE.
     *
     *  Method takes either two of image distance, focus distance and focal length and calculates the
     *  image distance an the focus distance according to the paraxial approximation.
     */
    bool SceneParser::calcOptics(const Json::Value& cameraDescription, float& imageDistance, float& focusDistance)
    {
        if (checkKey(cameraDescription, "imageDistance", PropertyType::NumberValue, false) &&
            checkKey(cameraDescription, "focusDistance", PropertyType::NumberValue, false))
        {
            if (checkKey(cameraDescription, "focalLength", PropertyType::NumberValue, false))
            {
                std::cout << "Keys ""imageDistance"", ""focusDistance"" and ""focalLength"" defined. Ignoring ""focalLength""." << std::endl;
            }
            imageDistance = cameraDescription["imageDistance"].asFloat();
            focusDistance = cameraDescription["focusDistance"].asFloat();
        }
        else if (checkKey(cameraDescription, "imageDistance", PropertyType::NumberValue, false) &&
            checkKey(cameraDescription, "focalLength", PropertyType::NumberValue, false))
        {
            imageDistance = cameraDescription["imageDistance"].asFloat();
            focusDistance = static_cast<float>(1.0 / ((1.0 / cameraDescription["focalLength"].asFloat()) - (1.0 / imageDistance)));
        }
        else if (checkKey(cameraDescription, "focusDistance", PropertyType::NumberValue, false) &&
            checkKey(cameraDescription, "focalLength", PropertyType::NumberValue, false))
        {
            focusDistance = cameraDescription["focusDistance"].asFloat();
            imageDistance = static_cast<float>(1.0 / ((1.0 / cameraDescription["focalLength"].asFloat()) - (1.0 / focusDistance)));
        }
        else
        {
            std::cout << "Either two of the keys ""imageDistance"", ""focusDistance"" and ""focalLength"" have to be specified." << std::endl;
            return false;
        }
        if (focusDistance < 0.0)
        {
            std::cout << "Warning: Image distance smaller than focal length. Setting focus distance to infinity." << std::endl;
            focusDistance = HUGE_VALUE;
        }
        if (focusDistance > HUGE_VALUE)
        {
            focusDistance = HUGE_VALUE;
        }
        return true;
    }



    /*! \brief Build a sampler object from a <a HREF="http://www.json.org">JSON</a> description.
     *  \param[in] samplerDescription <a HREF="http://www.json.org">JSON</a> value of sampler description.
     *  \param[out] sampler Built sampler object.
     *  \return Returns TRUE if building was successful, else FALSE.
     */
    bool SceneParser::buildSampler(const Json::Value& samplerDescription, std::shared_ptr<Sampler>& sampler)
    {
        // ensure required properties are defined and of correct type
        if (!checkKey(samplerDescription, "type", PropertyType::StringValue, true) ||
            !checkKey(samplerDescription, "numSamples", PropertyType::NumberValue, true))
        {
            return false;
        }

        // build sampler
        if (samplerDescription["type"].asString() == "Regular")
        {
            sampler.reset(new Regular(samplerDescription["numSamples"].asInt()));
        }
        else if (samplerDescription["type"].asString() == "PureRandom")
        {
            if (!checkKey(samplerDescription, "numSets", PropertyType::NumberValue, false))
            {
                std::cout << "Sampler property ""numSets"" not defined. Using default." << std::endl;
                sampler.reset(new PureRandom(samplerDescription["numSamples"].asInt()));
            }
            else
            {
                sampler.reset(new PureRandom(samplerDescription["numSamples"].asInt(), samplerDescription["numSets"].asInt()));
            }
            if (checkKey(samplerDescription, "seed", PropertyType::NumberValue, false))
            {
                sampler->setSeed(samplerDescription["seed"].asUInt());
            }
            else if (checkKey(samplerDescription, "seed", PropertyType::StringValue, false) &&
                samplerDescription["seed"].asString() == "Random")
            {
                sampler->setSeed(randomSeed());
            }
        }
        else if (samplerDescription["type"].asString() == "Jittered")
        {
            if (!checkKey(samplerDescription, "numSets", PropertyType::NumberValue, false))
            {
                std::cout << "Sampler property ""numSets"" not defined. Using default." << std::endl;
                sampler.reset(new Jittered(samplerDescription["numSamples"].asInt()));
            }
            else
            {
                sampler.reset(new Jittered(samplerDescription["numSamples"].asInt(), samplerDescription["numSets"].asInt()));
            }
            if (checkKey(samplerDescription, "seed", PropertyType::NumberValue, false))
            {
                sampler->setSeed(samplerDescription["seed"].asUInt());
            }
            else if (checkKey(samplerDescription, "seed", PropertyType::StringValue, false) &&
                samplerDescription["seed"].asString() == "Random")
            {
                sampler->setSeed(randomSeed());
            }
        }
        else if (samplerDescription["type"].asString() == "NRooks")
        {
            if (!checkKey(samplerDescription, "numSets", PropertyType::NumberValue, false))
            {
                std::cout << "Sampler property ""numSets"" not defined. Using default." << std::endl;
                sampler.reset(new NRooks(samplerDescription["numSamples"].asInt()));
            }
            else
            {
                sampler.reset(new NRooks(samplerDescription["numSamples"].asInt(), samplerDescription["numSets"].asInt()));
            }
            if (checkKey(samplerDescription, "seed", PropertyType::NumberValue, false))
            {
                sampler->setSeed(samplerDescription["seed"].asUInt());
            }
            else if (checkKey(samplerDescription, "seed", PropertyType::StringValue, false) &&
                samplerDescription["seed"].asString() == "Random")
            {
                sampler->setSeed(randomSeed());
            }
        }
        else if (samplerDescription["type"].asString() == "MultiJittered")
        {
            if (!checkKey(samplerDescription, "numSets", PropertyType::NumberValue, false))
            {
                std::cout << "Sampler property ""numSets"" not defined. Using default." << std::endl;
                sampler.reset(new MultiJittered(samplerDescription["numSamples"].asInt()));
            }
            else
            {
                sampler.reset(new MultiJittered(samplerDescription["numSamples"].asInt(), samplerDescription["numSets"].asInt()));
            }
            if (checkKey(samplerDescription, "seed", PropertyType::NumberValue, false))
            {
                sampler->setSeed(samplerDescription["seed"].asUInt());
            }
            else if (checkKey(samplerDescription, "seed", PropertyType::StringValue, false) &&
                samplerDescription["seed"].asString() == "Random")
            {
                sampler->setSeed(randomSeed());
            }
        }
        else if (samplerDescription["type"].asString() == "Hammersley")
        {
            sampler.reset(new Hammersley(samplerDescription["numSamples"].asInt()));
        }
        else
        {
            // "type" unknown
            std::cout << "Unknown type of sampler object: " << samplerDescription["type"].asString() << std::endl;
            return false;
        }

        // set optional jump seed
        if (checkKey(samplerDescription, "jumpSeed", PropertyType::NumberValue, false))
        {
            sampler->setJumpSeed(samplerDescription["jumpSeed"].asUInt());
        }
        else if (checkKey(samplerDescription, "jumpSeed", PropertyType::StringValue, false) &&
            samplerDescription["jumpSeed"].asString() == "Random")
        {
            sampler->setJumpSeed(randomSeed());
        }

        // set optional shuffle seed
        if (checkKey(samplerDescription, "shuffleSeed", PropertyType::NumberValue, false))
        {
            sampler->setShuffleSeed(samplerDescription["shuffleSeed"].asUInt());
        }
        else if (checkKey(samplerDescription, "shuffleSeed", PropertyType::StringValue, false) &&
            samplerDescription["shuffleSeed"].asString() == "Random")
        {
            sampler->setShuffleSeed(randomSeed());
        }

        // generate samples
        sampler->generateSamples();
        
        return true;
    }



    /*! \brief Generate random seed.
     *  \return Returns random seed.
     *
     *  Method uses std::random_device to generate a random seed. If random_device is not available, the
     *  system clock is used instead.
     */
    unsigned int SceneParser::randomSeed()
    {
        unsigned int res;
        try
        {
            res = std::random_device()();
        }
        catch (std::exception&)
        {
            std::cout << "std::random_device not available on your system. Using system clock for seed generation instead." << std::endl;
            res = static_cast<unsigned int>(std::chrono::system_clock::now().time_since_epoch().count());
        }
        return res;
    }
    
    
    
    /*! \brief Build Aperture from aperture description.
     *  \param[in] apertureDescription <a HREF="http://www.json.org">JSON</a> value of aperture description.
     *  \param[out] aperture Aperture object to store aperture data.
     *  \return Returns TRUE if parsing was successful, else FALSE.
     */
    bool SceneParser::buildAperture(const Json::Value& apertureDescription, std::shared_ptr<Aperture>& aperture)
    {
        // ensure required properties are defined
        if (!checkKey(apertureDescription, "type", PropertyType::StringValue, true) ||
            !checkKey(apertureDescription, "radius", PropertyType::NumberValue, true))
        {
            return false;
        }

        // check if aperturePosition is defined. If not, set a default of 0 and give a message
        float aperturePosition = 0.f;
        if (!checkKey(apertureDescription, "aperturePosition", PropertyType::NumberValue, false))
        {
            std::cout << "No ""aperturePosition"" found. Using default z = 0." << std::endl;
        }
        else
        {
            aperturePosition = apertureDescription["aperturePosition"].asFloat();
        }

        if (apertureDescription["type"].asString() == "CircularAperture")
        {
            aperture = std::shared_ptr<Aperture>(new CircularAperture(aperturePosition, apertureDescription["radius"].asFloat()));
        }
        else if (apertureDescription["type"].asString() == "ImageAperture")
        {
            // ensure required properties are defined
            if (!checkKey(apertureDescription, "imageType", PropertyType::StringValue, true))
            {
                return false;
            }

            std::shared_ptr<ImageAperture> imageAperture(new ImageAperture(aperturePosition, apertureDescription["radius"].asFloat()));

            if (apertureDescription["imageType"].asString() == "Circular")
            {
                // ensure required properties are defined
                if (!checkKey(apertureDescription, "width", PropertyType::IntValue, true) ||
                    !checkKey(apertureDescription, "height", PropertyType::IntValue, true) ||
                    !checkKey(apertureDescription, "apertureRadius", PropertyType::IntValue, true))
                {
                    return false;
                }

                imageAperture->setCircularAperture(
                    apertureDescription["width"].asInt(),
                    apertureDescription["height"].asInt(),
                    apertureDescription["apertureRadius"].asInt());
            }
            else if (apertureDescription["imageType"].asString() == "CircularRing")
            {
                if (!checkKey(apertureDescription, "width", PropertyType::IntValue, true) ||
                    !checkKey(apertureDescription, "height", PropertyType::IntValue, true))
                {
                    return false;
                }
                if (!checkKey(apertureDescription, "innerRadius", PropertyType::IntValue, true) ||
                    !checkKey(apertureDescription, "outerRadius", PropertyType::IntValue, true))
                {
                    return false;
                }
                imageAperture->setCircularRingAperture(
                    apertureDescription["width"].asInt(),
                    apertureDescription["height"].asInt(),
                    apertureDescription["innerRadius"].asInt(),
                    apertureDescription["outerRadius"].asInt());
            }
            else if (apertureDescription["imageType"].asString() == "Rectangular")
            {
                if (!checkKey(apertureDescription, "width", PropertyType::IntValue, true) ||
                    !checkKey(apertureDescription, "height", PropertyType::IntValue, true))
                {
                    return false;
                }

                if (checkKey(apertureDescription, "rectHalfWidth", PropertyType::IntValue, false) &&
                    checkKey(apertureDescription, "rectHalfHeight", PropertyType::IntValue, false))
                {
                    imageAperture->setRectangularAperture(
                        apertureDescription["width"].asInt(),
                        apertureDescription["height"].asInt(),
                        apertureDescription["rectHalfWidth"].asInt(),
                        apertureDescription["rectHalfHeight"].asInt());
                }
                else
                {
                    std::cout << "Keys 'rectHalfWidth' and 'rectHalfHeight' not specified. Assuming rectangle all white!." << std::endl;
                    imageAperture->setRectangularAperture(
                        apertureDescription["width"].asInt(),
                        apertureDescription["height"].asInt(),
                        static_cast<int>(apertureDescription["width"].asInt() / 2),
                        static_cast<int>(apertureDescription["height"].asInt() / 2));
                }
            }
            else if (apertureDescription["imageType"].asString() == "Levin")
            {
                imageAperture->setLevinAperture();
            }
            else if (apertureDescription["imageType"].asString() == "Image")
            {
                if (!checkKey(apertureDescription, "source", PropertyType::StringValue, true))
                {
                    return false;
                }
                if (!imageAperture->setCustomImageAperture(filePath_ + apertureDescription["source"].asString()))
                {
                    std::cout << "Failed to build aperture from file " << filePath_ + apertureDescription["source"].asString() << "." << std::endl;
                    return false;
                }
            }
            else
            {
                // "type" unknown
                std::cout << "Unknown type of aperture: " << apertureDescription["type"].asString() << std::endl;
                return false;
            }

            if (checkKey(apertureDescription, "apertureScaling", PropertyType::StringValue, false))
            {
                std::string scalingMethod = apertureDescription["apertureScaling"].asString();
                if (scalingMethod == "ApertureInsideLens")
                {
                    imageAperture->setApertureInsideLens();
                }
                else if (scalingMethod == "ApertureOutsideLens")
                {
                    imageAperture->setApertureOutsideLens();
                }
                else if (scalingMethod == "CustomPixelSize")
                {
                    if (!checkKey(apertureDescription, "customPixelWidth", PropertyType::NumberValue, false))
                    {
                        std::cout << "Key 'customPixelWidth' missing for selected option 'CustomPixelSize'" << std::endl;
                        return false;
                    }
                    double customPixelSize = apertureDescription["customPixelWidth"].asDouble();
                    imageAperture->setCustomPixelSize(customPixelSize);
                }
                else
                {
                    std::cout << "Unknown value for key 'apertureScaling': " << apertureDescription["apertureScaling"].asString() << std::endl;
                    return false;
                }
            }

            if (checkKey(apertureDescription, "offset", PropertyType::VectorValue, false) &&
                checkDimension(apertureDescription["offset"], 2, true))
            {
                imageAperture->setApertureTranslation(apertureDescription["offset"][0].asInt(),
                    apertureDescription["offset"][1].asInt());
            }

            if (checkKey(apertureDescription, "inPlaneRotation", PropertyType::NumberValue, false))
            {
                imageAperture->setInPlaneRotation(apertureDescription["inPlaneRotation"].asDouble());
            }

            if (checkKey(apertureDescription, "pixelAspectRatio", PropertyType::NumberValue, false))
            {
                imageAperture->setPixelAspectRatio(apertureDescription["pixelAspectRatio"].asFloat());
            }

            if (checkKey(apertureDescription, "isPeriodic", PropertyType::BoolValue, false))
            {
                if (apertureDescription["isPeriodic"].asBool())
                {
                    if (!checkKey(apertureDescription, "numberOfUPatterns", PropertyType::IntValue, true) ||
                        !checkKey(apertureDescription, "numberOfVPatterns", PropertyType::IntValue, true))
                    {
                        return false;
                    }
                    imageAperture->setPeriodic(
                        apertureDescription["numberOfUPatterns"].asInt(),
                        apertureDescription["numberOfVPatterns"].asInt());
                }
            }
            
            aperture = imageAperture;
        }
        else
        {
            // "type" unknown
            std::cout << "Unknown type of aperture: " << apertureDescription["type"].asString() << std::endl;
            return false;
        }
        
        return true;
    }



    /*! \brief Parses all included <a HREF="http://www.json.org">JSON</a> files.
     *  \param[in,out] value <a HREF="http://www.json.org">JSON</a> to check and replace if necessary.
     *  \return Returns TRUE if parsing was successful, else FALSE.
     *
     *  Iterates over all objects in the given <a HREF="http://www.json.org">JSON</a> value and looks
     *  for objects with a single "include" key, that specifies a file to include. This file then gets
     *  parsed and the respective <a HREF="http://www.json.org">JSON</a> object is replaced by the
     *  parsed object.
     *
     *  The path of the file to include has to be specified relative to the main scene description file.
     *
     *  Calls itself iteratively for all <a HREF="http://www.json.org">JSON</a> objects in the given
     *  object.
     */
    bool SceneParser::parseIncludedFiles(Json::Value& value)
    {
        // check for include key
        if (checkKey(value, "include", PropertyType::StringValue, false))
        {
            // ensure "include" key is the only key
            if (value.getMemberNames().size() != 1)
            {
                std::cout << """include"" key is not the sole key of the JSON object." << std::endl;
                return false;
            }

            // get path of included file
            std::string includeFileName(value["include"].asString());

            // open included file
            std::ifstream stream(std::string(filePath_).append(includeFileName), std::ifstream::in);
            if (!stream.is_open())
            {
                std::cout << "Cannot open file """ << includeFileName << """." <<std::endl;
                return false;
            }

            Json::Value root;
            Json::Reader reader;

            // read scene description
            if (!reader.parse(stream, root))
            {
                std::cout << "Failed to parse configuration" << std::endl << reader.getFormattedErrorMessages();
                return false;
            }

            // adjust paths
            std::string includePath(includeFileName);
            int pos = static_cast<int>(includePath.find_last_of('/'));
            if (pos != std::string::npos)
            {
                includePath.erase(pos + 1);
            }
            else
            {
                includePath.clear();
            }
            adjustPaths(includePath, root);

            // replace JSON object
            value = root;

            if (!parseIncludedFiles(value))
            {
                return false;
            }
        }
        else
        {
            // check remaining json objects
            std::vector<std::string> keys = value.getMemberNames();
            for (unsigned int i = 0; i < keys.size(); ++i)
            {
                if (value[keys[i]].type() == Json::ValueType::arrayValue)
                {
                    for (unsigned int j = 0; j < value[keys[i]].size(); ++j)
                    {
                        if (value[keys[i]][j].type() == Json::ValueType::objectValue)
                        {
                            if (!parseIncludedFiles(value[keys[i]][j]))
                            {
                                return false;
                            }
                        }
                    }
                }
                if (value[keys[i]].type() == Json::ValueType::objectValue)
                {
                    if (!parseIncludedFiles(value[keys[i]]))
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    }



    /*! \brief Adjust file paths to paths relative to the main scene description file.
     *  \param[in] includePath Relative path of the file that included the given
     *  <a HREF="http://www.json.org">JSON</a> object.
     *  \param[in,out] value <a HREF="http://www.json.org">JSON</a> to check and adjust if necessary.
     *
     *  Iterates over all objects in the given <a HREF="http://www.json.org">JSON</a> value and looks
     *  for keys that specify a file path. These paths are adjusted to paths relative to the main scene
     *  description file.
     *
     *  Calls itself iteratively for all <a HREF="http://www.json.org">JSON</a> objects in the given
     *  object.
     */
    void SceneParser::adjustPaths(const std::string& includePath, Json::Value& value)
    {
        // check for include key
        if (value.isMember("include"))
        {
            std::string newIncludePath(includePath);
            newIncludePath.append(value["include"].asString());
            value["include"] = newIncludePath;
        }

        // check for plyFile key
        if (value.isMember("plyFile"))
        {
            std::string newIncludePath(includePath);
            newIncludePath.append(value["plyFile"].asString());
            value["plyFile"] = newIncludePath;
        }

        // adjust remaining paths
        std::vector<std::string> keys = value.getMemberNames();
        for (unsigned int i = 0; i < keys.size(); ++i)
        {
            if (value[keys[i]].type() == Json::ValueType::arrayValue)
            {
                for (unsigned int j = 0; j < value[keys[i]].size(); ++j)
                {
                    if (value[keys[i]][j].type() == Json::ValueType::objectValue)
                    {
                        adjustPaths(includePath, value[keys[i]][j]);
                    }
                }
            }
            if (value[keys[i]].type() == Json::ValueType::objectValue)
            {
                adjustPaths(includePath, value[keys[i]]);
            }
        }
    }



    /*! \brief Checks type of value.
     *  \param[in] value <a HREF="http://www.json.org">JSON</a> value to check.
     *  \param[in] type Type of the value.
     *  \param[in] required Flag indicating whether to print out an error message, when conditions are
     *  not met.
     *  \return Returns TRUE if all went fine, else FALSE.
     */
    bool SceneParser::checkValue(const Json::Value& value, SceneParser::PropertyType type, bool required)
    {
        // check if value has correct type
        if (type & PropertyType::BoolValue)
        {
            if (value.type() != Json::ValueType::booleanValue)
            {
                if (required)
                {
                    std::cout << "Expected a boolean." << std::endl;
                }
                return false;
            }
        }
        else if (type & PropertyType::IntValue)
        {
            if (value.type() != Json::ValueType::intValue)
            {
                if (required)
                {
                    std::cout << "Expected an integer number." << std::endl;
                }
                return false;
            }
        }
        else if (type & PropertyType::UintValue)
        {
            if (value.type() != Json::ValueType::uintValue)
            {
                if (required)
                {
                    std::cout << "Expected an unsigned integer number." << std::endl;
                }
                return false;
            }
        }
        else if (type & PropertyType::RealValue)
        {
            if (value.type() != Json::ValueType::realValue)
            {
                if (required)
                {
                    std::cout << "Expected a real number." << std::endl;
                }
                return false;
            }
        }
        else if (type & PropertyType::NumberValue)
        {
            if (value.type() != Json::ValueType::intValue &&
                value.type() != Json::ValueType::uintValue &&
                value.type() != Json::ValueType::realValue)
            {
                if (required)
                {
                    std::cout << "Expected a number." << std::endl;
                }
                return false;
            }
        }
        else if (type & PropertyType::VectorValue)
        {
            if (value.type() != Json::ValueType::arrayValue)
            {
                if (required)
                {
                    std::cout << "Expected a vector." << std::endl;
                }
                return false;
            }

            for (unsigned int i = 0; i < value.size(); ++i)
            {
                if (value[i].type() != Json::ValueType::intValue &&
                    value[i].type() != Json::ValueType::uintValue &&
                    value[i].type() != Json::ValueType::realValue)
                {
                    if (required)
                    {
                        std::cout << "Expected a numeric vector." << std::endl;
                    }
                    return false;
                }
            }
        }
        else if (type & PropertyType::MatrixValue)
        {
            if (value.type() != Json::ValueType::arrayValue)
            {
                if (required)
                {
                    std::cout << "Expected a matrix." << std::endl;
                }
                return false;
            }

            for (unsigned int i = 0; i < value.size(); ++i)
            {
                if (value[i].type() != Json::ValueType::arrayValue)
                {
                    if (required)
                    {
                        std::cout << "Expected a matrix." << std::endl;
                    }
                    return false;
                }

                for (unsigned int j = 0; j < value[i].size(); ++j)
                {
                    if (value[i][j].type() != Json::ValueType::intValue &&
                        value[i][j].type() != Json::ValueType::uintValue &&
                        value[i][j].type() != Json::ValueType::realValue)
                    {
                        if (required)
                        {
                            std::cout << "Expected a numeric matrix." << std::endl;
                        }
                        return false;
                    }
                }
            }
        }
        else if (type & PropertyType::StringValue)
        {
            if (value.type() != Json::ValueType::stringValue)
            {
                if (required)
                {
                    std::cout << "Expected a string." << std::endl;
                }
                return false;
            }
        }
        else if (type & PropertyType::ArrayValue)
        {
            if (value.type() != Json::ValueType::arrayValue)
            {
                if (required)
                {
                    std::cout << "Expected an array." << std::endl;
                }
                return false;
            }
        }
        else if (type & PropertyType::ObjectValue)
        {
            if (value.type() != Json::ValueType::objectValue)
            {
                if (required)
                {
                    std::cout << "Expected an object." << std::endl;
                }
                return false;
            }
        }
        return true;
    }
    
    
    
    /*! \brief Checks type of key.
     *  \param[in] value <a HREF="http://www.json.org">JSON</a> value to check.
     *  \param[in] key Key to check.
     *  \param[in] type Type of the key.
     *  \param[in] required Flag indicating whether to print out an error message, when conditions are
     *  not met.
     *  \return Returns TRUE if all went fine, else FALSE.
     */
    bool SceneParser::checkKey(const Json::Value& value, const std::string& key, SceneParser::PropertyType type, bool required)
    {
        // check if key is defined
        if (!value.isMember(key))
        {
            if (required)
            {
                std::cout << "No """ << key << """ key found." << std::endl;
            }
            return false;
        }
        
        // check if value has correct type
        if (type & PropertyType::BoolValue)
        {
            if (value[key].type() != Json::ValueType::booleanValue)
            {
                if (required)
                {
                    std::cout << "Expected """ << key << """ to be a boolean." << std::endl;
                }
                return false;
            }
        }
        else if (type & PropertyType::IntValue)
        {
            if (value[key].type() != Json::ValueType::intValue)
            {
                if (required)
                {
                    std::cout << "Expected """ << key << """ to be an integer number." << std::endl;
                }
                return false;
            }
        }
        else if (type & PropertyType::UintValue)
        {
            if (value[key].type() != Json::ValueType::uintValue)
            {
                if (required)
                {
                    std::cout << "Expected """ << key << """ to be an unsigned integer number." << std::endl;
                }
                return false;
            }
        }
        else if (type & PropertyType::RealValue)
        {
            if (value[key].type() != Json::ValueType::realValue)
            {
                if (required)
                {
                    std::cout << "Expected """ << key << """ to be a real number." << std::endl;
                }
                return false;
            }
        }
        else if (type & PropertyType::NumberValue)
        {
            if (value[key].type() != Json::ValueType::intValue &&
                value[key].type() != Json::ValueType::uintValue &&
                value[key].type() != Json::ValueType::realValue)
            {
                if (required)
                {
                    std::cout << "Expected """ << key << """ to be a number." << std::endl;
                }
                return false;
            }
        }
        else if (type & PropertyType::VectorValue)
        {
            if (value[key].type() != Json::ValueType::arrayValue)
            {
                if (required)
                {
                    std::cout << "Expected """ << key << """ to be a vector." << std::endl;
                }
                return false;
            }
            
            for (unsigned int i = 0; i < value[key].size(); ++i)
            {
                if (value[key][i].type() != Json::ValueType::intValue &&
                    value[key][i].type() != Json::ValueType::uintValue &&
                    value[key][i].type() != Json::ValueType::realValue)
                {
                    if (required)
                    {
                        std::cout << "Expected """ << key << """ to be a numeric vector." << std::endl;
                    }
                    return false;
                }
            }
        }
        else if (type & PropertyType::MatrixValue)
        {
            if (value[key].type() != Json::ValueType::arrayValue)
            {
                if (required)
                {
                    std::cout << "Expected """ << key << """ to be a matrix." << std::endl;
                }
                return false;
            }
            
            for (unsigned int i = 0; i < value[key].size(); ++i)
            {
                if (value[key][i].type() != Json::ValueType::arrayValue)
                {
                    if (required)
                    {
                        std::cout << "Expected """ << key << """ to be a matrix." << std::endl;
                    }
                    return false;
                }
                
                for (unsigned int j = 0; j < value[key][i].size(); ++j)
                {
                    if (value[key][i][j].type() != Json::ValueType::intValue &&
                        value[key][i][j].type() != Json::ValueType::uintValue &&
                        value[key][i][j].type() != Json::ValueType::realValue)
                    {
                        if (required)
                        {
                            std::cout << "Expected """ << key << """ to be a numeric matrix." << std::endl;
                        }
                        return false;
                    }
                }
            }
        }
        else if (type & PropertyType::StringValue)
        {
            if (value[key].type() != Json::ValueType::stringValue)
            {
                if (required)
                {
                    std::cout << "Expected """ << key << """ to be a string." << std::endl;
                }
                return false;
            }
        }
        else if (type & PropertyType::ArrayValue)
        {
            if (value[key].type() != Json::ValueType::arrayValue)
            {
                if (required)
                {
                    std::cout << "Expected """ << key << """ to be an array." << std::endl;
                }
                return false;
            }
        }
        else if (type & PropertyType::ObjectValue)
        {
            if (value[key].type() != Json::ValueType::objectValue)
            {
                if (required)
                {
                    std::cout << "Expected """ << key << """ to be an object." << std::endl;
                }
                return false;
            }
        }
        else if (type & PropertyType::ColorValue)
        {
            if (value[key].type() != Json::ValueType::realValue &&
                value[key].type() != Json::ValueType::arrayValue &&
                value[key].type() != Json::ValueType::objectValue)
            {
                if (required)
                {
                    std::cout << "Expected """ << key << """ to be convertible to a color." << std::endl;
                }
                return false;
            }
        }
        return true;
    }



    /*! \brief Checks if a value has the correct dimension.
     *  \param[in] value <a HREF="http://www.json.org">JSON</a> value to check.
     *  \param[in] dimension Expected dimension.
     *  \param[in] required Flag indicating whether to print out an error message, when conditions are
     *  not met.
     */
    bool SceneParser::checkDimension(const Json::Value& value, int dimension, bool required)
    {
        if (value.size() != dimension)
        {
            if (required)
            {
                std::cout << "Expected a dimension of " << dimension << "." << std::endl;
            }
            return false;
        }
        return true;
    }



    /*! \brief Checks if a value has the correct matrix dimension.
     *  \param[in] value <a HREF="http://www.json.org">JSON</a> value to check.
     *  \param[in] uDimension Expected u-dimension.
     *  \param[in] vDimension Expected v-dimension.
     *  \param[in] required Flag indicating whether to print out an error message, when conditions are
     *  not met.
     */
    bool SceneParser::checkMatrixDimension(const Json::Value& value, int uDimension, int vDimension, bool required)
    {
        if (value.size() != uDimension)
        {
            if (required)
            {
                std::cout << "Expected a horizontal dimension of " << uDimension << "." << std::endl;
            }
            return false;
        }
        else
        {
            for (unsigned int i = 0; i < value.size(); ++i)
            {
                if (value[i].size() != vDimension)
                {
                    if (required)
                    {
                        std::cout << "Expected a vertical dimension of " << vDimension << "." << std::endl;
                    }
                    return false;
                }
            }
        }
        return true;
    }



    /*! \brief Extracts properties of the micro lens array and builds the grid.
     *  \return Returns ´true´ if building was successful, else ´false´.
     */
    template <class SpectrumType>
    bool SceneParser::buildMicroLensArray(const Json::Value& gridDescription,
                                          std::shared_ptr<AbstractGrid<SpectrumType> > microLensArray,
                                          double microLensRadius, double imageDistance,
                                          double x_max, double y_max,
                                          SceneMetaData metaData)
    {
        std::string gridType;
        bool is_coded;
        double offset_x = 0.0;
        double offset_y = 0.0;
        double rotation = 0.0;
        double spacing = 2*microLensRadius;
        double z = -imageDistance;

        // check grid description keys
        if (!checkKey(gridDescription, "offset", PropertyType::ArrayValue, true))
        {
            std::cout << "Grid property ""offset"" not defined. Using default [0, 0]." << std::endl;
        }
        else
        {
            offset_x = gridDescription["offset"][0].asDouble();
            offset_y = gridDescription["offset"][1].asDouble();
        }

        if (!checkKey(gridDescription, "rotation", PropertyType::NumberValue, true))
        {
            std::cout << "Grid property ""rotation"" not defined. Using default 0.0." << std::endl;
        }
        else
        {
            rotation = gridDescription["rotation"].asDouble();
        }

        // check grid description keys
        if (!checkKey(gridDescription, "coded", PropertyType::BoolValue, true))
        {
            std::cout << "Grid property ""coded"" not defined. Using default ""false""." << std::endl;
            is_coded = false;
        }
        else is_coded = gridDescription["coded"].asBool();

        if (!checkKey(gridDescription, "type", PropertyType::StringValue, true))
        {
            std::cout << "Grid property ""type"" not defined. Using default ""Hex""." << std::endl;
            gridType = "Hex";
        }
        else gridType = gridDescription["type"].asString();

        if (gridType == "Hex")
        {
            *microLensArray = HexGrid<SpectrumType>(x_max, y_max, offset_x, offset_y, spacing, rotation, 0.0, 0.0, z, is_coded);
        }
        else if (gridType == "Rect")
        {
            *microLensArray = RectGrid<SpectrumType>(x_max, y_max, offset_x, offset_y, spacing, spacing, rotation, 0.0, 0.0, z, is_coded);
        }
        else
        {
            std::cout << "Unknown grid type." << std::endl;
            return false;
        }

        // check noise description keys
        if (checkKey(gridDescription, "noise", PropertyType::NumberValue, true))
        {
            if (gridDescription["noise"].asDouble() != 0.0)
            {
                double sigma = gridDescription["noise"].asDouble() * 0.01 * spacing;
                microLensArray->add_noise(0.0, sigma);  // zero mean
            }
        }

        // check save description keys
        if (checkKey(gridDescription, "save", PropertyType::BoolValue, true))
        {
            if (gridDescription["save"].asBool())
            {
                std::string csvFileName = metaData.fileName_ + "_gridPoints.csv";
                microLensArray->save(csvFileName);
            }
        }

        // successfully generated microlens array
        return true;
    }

} // end of namespace iiit
