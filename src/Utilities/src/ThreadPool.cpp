// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file ThreadPool.cpp
 *  \author Thomas Nuernberg
 *  \date 12.12.2014
 *  \brief Implementation of class ThreadPool.
 *
 *  Implementation of class ThreadPool, using Worker objects.
 */

#include "ThreadPool.hpp"



namespace iiit
{

    /*! \brief Default constructor.
     *  \param[in] size Number of workers in pool.
     */
    ThreadPool::ThreadPool(size_t size)
        : shuttingDown_(false)
        , idleWorkers_(0)
        , size_(size)
    {
        LOG(LogLevel::LogInfo, "Constructing workers.");
        resume();
    }



    /*! \brief Default destructor.
     *  
     *  Wakes all threads from sleep and joins them. Worker threads currently in execution will first
     *  finish their current tasks. Tasks still waiting wont be executed. Threads currently executing
     *  waitForWorkers() are notified and exit the function to be able to safely destroy the object.
     */
    ThreadPool::~ThreadPool()
    {
        // notify all threads waiting for the pool to finish
        LOG(LogLevel::LogInfo, "Shutting down.");
        shuttingDown_.store(true);
        finishCondition_.notify_all();

        // join all worker threads
        pause();
        std::unique_lock<std::mutex> waitLock(waitMutex_);
        LOG(LogLevel::LogInfo, "Pool destroyed.");
    }



    /*! \brief Notifies all workers and joins threads.
     */
    void ThreadPool::pause()
    {
        // stop all threads
        pause_.store(true);
        workCondition_.notify_all();

        // join them
        for (size_t i = 0; i < threads_.size(); ++i)
        {
            threads_[i].join();
        }
        threads_.clear();
    }



    /*! \brief Creates worker threads, which will start working immediately if there are any tasks.
     */
    void ThreadPool::resume()
    {
        pause_.store(false);

        // create workers
        for (size_t i = 0; i < size_; ++i)
        {
            threads_.push_back(std::thread(Worker(*this)));
        }
    }



    /*! \brief Indicates whether the ThreadPool is idle.
     *  \return Returns TRUE if idle, FALSE otherwise.
     *
     *  Indicates whether the ThreadPool is idle, which means that there are no work items and all
     *  workers are idle.
     */
    bool ThreadPool::isIdle()
    {
        // acquire lock
        std::unique_lock<std::mutex> lock(queueMutex_);
        if (!tasks_.empty())
        {
            return false;
        }
        else if (idleWorkers_.load() != size_)
        {
            return false;
        }
        return true;
    }



    /*! \brief Indicates whether the task queue is empty.
     *  \return Returns TRUE if task queue is empty, FALSE otherwise.
     */
    bool ThreadPool::isEmpty()
    {
        // acquire lock
        std::unique_lock<std::mutex> lock(queueMutex_);
        return tasks_.empty();
    }



    /*! \brief Wait until all tasks have been finished.
     *
     *  Suspends the calling thread until all tasks in queue have been finished or until the ThreadPool
     *  is to be destroyed. After leaving this method the ThreadPool object can be destroyed safely.
     *
     *  If the main program is suited to handle events, it is suggested to call this method from another
     *  thread in order to not block the whole program. Finish events can be generated in the other
     *  thread.
     *
     *  \b Example:
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
     *  #include <thread>
     *  #include "TreadPool.hpp"
     *
     *  // function to be executed in threads
     *  void foo()
     *  {
     *      ...
     *  }
     *
     *  // helper function
     *  void waitFunction(ThreadPool& pool)
     *  {
     *      pool.waitForWorkers();
     *
     *      // Do stuff when pool has finished.
     *  }
     *
     *  int main()
     *  {
     *      // create thread pool
     *      ThreadPool pool(2);
     *
     *      // push task(s) in queue
     *      std::function<void()> task(&foo);
     *      pool.enqueue(task);
     *      ...
     *
     *      std::thread t(&waitFunction, pool);
     *  }
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     */
    void ThreadPool::waitForWorkers()
    {
        // acquire lock preventing the object to be destroyed
        std::unique_lock<std::mutex> waitLock(waitMutex_);
        while (true)
        {
            // acquire lock
            std::unique_lock<std::mutex> queueLock(queueMutex_);

            // look for a work item in queue
            while (!tasks_.empty())
            {
                // still work to do, wait for tasks to finish
                LOG(LogLevel::LogInfo, "Queue not empty. Main thread going to sleep.");
                finishCondition_.wait(queueLock);
                LOG(LogLevel::LogInfo, "Main thread waking up.");
                if (shuttingDown_)
                {
                    LOG(LogLevel::LogInfo, "ThreadPool shutting down. Leaving wait function.");
                    return;
                }
            }

            // check if threads are sleeping
            LOG(LogLevel::LogInfo, "Queue empty. Checking workers.");
            if (idleWorkers_.load() != threads_.size())
            {
                LOG(LogLevel::LogInfo, "Threads still working (" << threads_.size() - idleWorkers_.load() << "/" << threads_.size() << "). Main thread going to sleep.");
                finishCondition_.wait(queueLock);
                LOG(LogLevel::LogInfo, "Main thread waking up.");
            }
            else
            {
                LOG(LogLevel::LogInfo, "Work done. Leaving waitForWorkers() function.");
                return;
            }
        } // release lock
    } // release lock

} // end of namespace iiit