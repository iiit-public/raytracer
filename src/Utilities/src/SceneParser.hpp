// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file SceneParser.hpp
 *  \author Thomas Nuernberg
 *  \date 13.01.2015
 *  \brief Definition of class SceneParser.
 *
 *  Definition of class SceneParser used to extract world data from a
 *  <a HREF="http://www.json.org">JSON</a> description.
 */

#ifndef __SCENEPARSER_HPP__
#define __SCENEPARSER_HPP__

#include<complex>
#include <string>
#include <memory>
#include <vector>
#include <array>
#include <fstream>
#include <random>

#include "json/json.h"
#include "rply.h"
#include "CImg.h"
#include "cnpy.h"

#include "AbstractScene.hpp"

// geometric objects
#include "Objects/GeometricObject.hpp"
#include "Objects/AreaLightObject.hpp"
#include "Objects/Sphere.hpp"
#include "Objects/Plane.hpp"
#include "Objects/Triangle.hpp"
#include "Objects/Box.hpp"
#include "Objects/Disk.hpp"
#include "Objects/Square.hpp"
#include "Objects/OpenCylinder.hpp"
#include "Objects/OpenCone.hpp"
#include "Objects/Torus.hpp"
#include "Objects/Compound.hpp"
#include "Objects/Instance.hpp"
#include "Objects/Grid.hpp"
#include "Objects/FlatMeshTriangle.hpp"
#include "Objects/SmoothMeshTriangle.hpp"
#include "Objects/TriangleMesh.hpp"

// cameras
#include "Cameras/Camera.hpp"
#include "Cameras/ChromaticRealLensCamera.hpp"
#include "Cameras/LightFieldCamera.hpp"
#include "Cameras/LightFieldCameraCalibration.hpp"
#include "Cameras/LightFieldReferenceCamera.hpp"
#include "Cameras/Orthographic.hpp"
#include "Cameras/Pinhole.hpp"
#include "Cameras/RealLensCamera.hpp"
#include "Cameras/ThinLens.hpp"

// sensor class
#include "Cameras/Sensor.hpp"

// aperture class
#include "Cameras/Aperture.hpp"

// aberration classes
#include "Aberration/ImageNoise.hpp"

// basic sampler
#include "Sampler/Regular.hpp"

// transformation class
#include "Utilities/Transformation.hpp"
#include "Utilities/Matrix4x4.hpp"

// sampler base class
#include "Sampler/Sampler.hpp"

// materials
#include "Materials/Material.hpp"
#include "Materials/Matte.hpp"
#include "Materials/Phong.hpp"
#include "Materials/Reflective.hpp"
#include "Materials/GlossyReflective.hpp"
#include "Materials/Transparent.hpp"
#include "Materials/Dielectric.hpp"
#include "Materials/Emissive.hpp"

// textures
#include "Textures/Texture.hpp"
#include "Textures/ConstantColor.hpp"
#include "Textures/Checker2D.hpp"
#include "Textures/Checker3D.hpp"
#include "Textures/Grid2D.hpp"
#include "Textures/Grid3D.hpp"
#include "Textures/Sine2D.hpp"
#include "Textures/Sine3D.hpp"
#include "Textures/NextNeighborNoiseTexture.hpp"
#include "Textures/LinearNoiseTexture.hpp"
#include "Textures/CubicNoiseTexture.hpp"
#include "Textures/ImageTexture.hpp"
#include "Textures/SpectralImageTexture.hpp"

// mappings
#include "Textures/Mappings/Mapping.hpp"
#include "Textures/Mappings/SphereMap.hpp"
#include "Textures/Mappings/LightProbeMap.hpp"
#include "Textures/Mappings/SquareMap.hpp"
#include "Textures/Mappings/DiskMap.hpp"
#include "Textures/Mappings/CylinderMap.hpp"

// tracers
#include "Tracers/Tracer.hpp"
#include "Tracers/RayCast.hpp"
#include "Tracers/AreaLighting.hpp"
#include "Tracers/Whitted.hpp"
#include "Tracers/WhittedAreaLighting.hpp"
#include "Tracers/PathTracer.hpp"
#include "Tracers/DepthTracer.hpp"
#include "Tracers/SurfaceTracer.hpp"
#include "Tracers/NormalTracer.hpp"
#include "Tracers/CoordinateTracer.hpp"
#include "Tracers/ObjectTracer.hpp"
#include "Tracers/RayParameterTracer.hpp"

// lights
#include "Lights/Ambient.hpp"
#include "Lights/AmbientOccluder.hpp"
#include "Lights/DirectionalLight.hpp"
#include "Lights/PointLight.hpp"
#include "Lights/AreaLight.hpp"
#include "Lights/EnvironmentLight.hpp"

// spectrum base class
#include "Spectrum/CoefficientSpectrum.hpp"

// utility classes
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/Constants.h"
#include "Utilities/AbstractGrid.hpp"
#include "Utilities/HexGrid.hpp"
#include "Utilities/RectGrid.hpp"



namespace iiit
{

    /*! \brief Struct to store meta data of the parsing process.
     */
    struct SceneMetaData
    {
        /*! \brief Default constructor.
         */
        SceneMetaData() : supportsMultiThreading_(true) {}

        bool supportsMultiThreading_; ///< Flag indicating whether the scene supports multithreading.
        int uMin_; ///< Minimum u-coordinate to render.
        int uMax_; ///< Maximum u-coordinate to render.
        int vMin_; ///< Minimum v-coordinate to render.
        int vMax_; ///< Maximum v-coordinate to render.
        std::string fileName_; ///< Filename of image file
    };



    /*! \brief Class to build world.
     *
     *  Class for extracting world data from a <a HREF="http://www.json.org">JSON</a> description of the
     *  scene.
     *
     *  The basic structure of a scene is to define a <a HREF="http://www.json.org">JSON</a> object that
     *  defines the keys "geometry," "camera" and "lights".
     *  
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
     *  {
     *      "geometry" : <geometric object information>,
     *      "camera" : <camera information>,
     *      "lights" : <lights information>
     *  }
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *
     *  The "geometry" contains data of all geometric objects in the scene and is therefore usually an
     *  array of different geometric objects. The "camera" value holds data of the camera to simulate.
     *
     *  An geometric object is defined by its type and its properties as follows:
     *
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
     *  {
     *      "type" : <type identifier>,
     *      "<property>" : <object specific property>,
     *      "material" : <material specification>,
     *      "brdf" : <brdf specification>,
     *      "texture" : <texture specification>
     *  }
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *
     *  The required properties depend on the type of the object to build.
     *
     *  Other <a HREF="http://www.json.org">JSON</a> files can be included by using an object with a
     *  single "incldue" key, followed by a string with the file path. The file path must be relative
     *  from the file with the "include" key.
     *
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
     *   "camera" : {
     *      "include" : "../relative/path/to/camera.json"
     *  }
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     */
    class SceneParser
    {
        /*! This enum type defines types of properties.
         */
        enum PropertyType
        {
            BoolValue = 1, ///< Boolean value.
            IntValue = 2, ///< Integer value.
            UintValue = 4, ///< Unsigned integer value.
            RealValue = 8, ///< Real value.
            NumberValue = 16, ///< Arbitrary number .
            VectorValue = 32, ///< Vector of numbers.
            MatrixValue = 64, ///< Matrix of numbers.
            StringValue = 128, ///< String value.
            ArrayValue = 256, ///< <a HREF="http://www.json.org">JSON</a> array.
            ObjectValue = 512, ///< <a HREF="http://www.json.org">JSON</a> object.
            ColorValue = 1024, ///< Value convertible to a color.
        };



    public:
        SceneParser();
        ~SceneParser();

        bool parse(const char* sceneDescriptionFileName, std::shared_ptr<AbstractScene>& scene, SceneMetaData& metaData);

    private:
        
        template <class SpectrumType>
        bool buildScene(const Json::Value sceneDescription, std::shared_ptr<Scene<SpectrumType> >& scene, SceneMetaData& metaData);
        
        // scene geometry
        template <class SpectrumType>
        bool buildGeometry(const Json::Value& geometryDescription, std::shared_ptr<Scene<SpectrumType> >& scene);

        template <class SpectrumType>
        bool buildGeometricObject(const Json::Value& geometryDescription, std::shared_ptr<GeometricObject<SpectrumType> >& geometricObject);

        template <class SpectrumType>
        bool buildAreaLightObject(const Json::Value& geometryDescription, std::shared_ptr<AreaLightObject<SpectrumType> >& geometricObject);

        template <class SpectrumType>
        bool buildSphere(const Json::Value& objectDescription, std::shared_ptr<Sphere<SpectrumType> >& geometricObject);

        template <class SpectrumType>
        bool buildPlane(const Json::Value& objectDescription, std::shared_ptr<Plane<SpectrumType> >& geometricObject);

        template <class SpectrumType>
        bool buildTriangle(const Json::Value& objectDescription, std::shared_ptr<Triangle<SpectrumType> >& geometricObject);

        template <class SpectrumType>
        bool buildBox(const Json::Value& objectDescription, std::shared_ptr<Box<SpectrumType> >& geometricObject);

        template <class SpectrumType>
        bool buildDisk(const Json::Value& objectDescription, std::shared_ptr<Disk<SpectrumType> >& geometricObject);

        template <class SpectrumType>
        bool buildSquare(const Json::Value& objectDescription, std::shared_ptr<Square<SpectrumType> >& geometricObject);

        template <class SpectrumType>
        bool buildCylinder(const Json::Value& objectDescription, std::shared_ptr<OpenCylinder<SpectrumType> >& geometricObject);

        template <class SpectrumType>
        bool buildCone(const Json::Value& objectDescription, std::shared_ptr<OpenCone<SpectrumType> >& geometricObject);

        template <class SpectrumType>
        bool buildTorus(const Json::Value& objectDescription, std::shared_ptr<Torus<SpectrumType> >& geometricObject);

        template <class SpectrumType>
        bool buildCompound(const Json::Value& objectDescription, std::shared_ptr<Compound<SpectrumType> >& geometricObject);

        template <class SpectrumType>
        bool buildGrid(const Json::Value& objectDescription, std::shared_ptr<Grid<SpectrumType> >& geometricObject);

        template <class SpectrumType>
        bool buildTriangleMesh(const Json::Value& objectDescription, std::shared_ptr<TriangleMesh<SpectrumType> >& geometricObject);

        template <class SpectrumType>
        bool buildInstance(const Json::Value& transformationDescription, std::shared_ptr<GeometricObject<SpectrumType> > object, std::shared_ptr<GeometricObject<SpectrumType> >& instance);

        template <class SpectrumType>
        bool transformInstance(const Json::Value& transformationDescription, std::shared_ptr<Instance<SpectrumType> >& instanceObject);

        template <class SpectrumType>
        bool buildInstances(const Json::Value& objectDescription, std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >& geometricObjects);
        
        // camera
        template <class SpectrumType>
        bool buildCamera(const Json::Value& cameraDescription, std::shared_ptr<Scene<SpectrumType> > scene, SceneMetaData& metaData);

        bool buildCameraState(const Json::Value& cameraDescription, Point3D& eye, Point3D& lookAt, Vector3D& up);

        template <class SpectrumType>
        bool buildSensor(const Json::Value& cameraDescription, Sensor<SpectrumType>& sensor);

        template <class SpectrumType>
        bool buildMicroLensArray(const Json::Value& gridDescription, std::shared_ptr<AbstractGrid<SpectrumType> > microLensArray, double microLensRadius, double imageDistance, double x_max, double y_max, SceneMetaData metaData);

        bool calcOptics(const Json::Value& cameraDescription, float& imageDistance, float& focusDistance);

        // sampler
        bool buildSampler(const Json::Value& samplerDescription, std::shared_ptr<Sampler>& sampler);
        unsigned int randomSeed();

        // aperture
        bool buildAperture(const Json::Value& apertureDescription, std::shared_ptr<Aperture>& aperture);

        // material
        template <class SpectrumType>
        bool buildMaterial(const Json::Value& materialDescription, std::shared_ptr<Material<SpectrumType> >& material);
        template <class SpectrumType>
        bool buildTexture(const Json::Value& textureDescription, std::shared_ptr<Texture<SpectrumType> >& texture);

        // light
        template <class SpectrumType>
        bool buildLights(const Json::Value& lightDescription, std::shared_ptr<Scene<SpectrumType> > scene);
        
        // color representation
        template <class SpectrumType>
        bool buildColor(const Json::Value& colorDescription, SpectrumType& color, SpectrumRepresentationType type);

        // tracer
        template <class SpectrumType>
        bool buildTracer(const Json::Value& tracerDescription, std::shared_ptr<Scene<SpectrumType> > scene);

        // utility methods
        bool checkValue(const Json::Value& value, SceneParser::PropertyType type, bool required = true);
        bool checkKey(const Json::Value& value, const std::string& key, SceneParser::PropertyType type, bool required = true);
        bool checkDimension(const Json::Value& value, int dimension, bool required = true);
        bool checkMatrixDimension(const Json::Value& value, int uDdimension, int vDdimension, bool required = true);
        bool parseIncludedFiles(Json::Value& value);
        void adjustPaths(const std::string& includePath, Json::Value& value);

        // ply callbacks
        template <class SpectrumType>
        static int vertexCallback(p_ply_argument argument);

        template <class SpectrumType>
        static int faceCallback(p_ply_argument argument);

        std::string filePath_; ///< Path to main json scene description file.

        SpectrumRepresentationType lightType_;
        SpectrumRepresentationType materialType_;
    };



    /*! \brief Builds scene object.
     *  \param[in] sceneDescription <a HREF="http://www.json.org">JSON</a> value of scene description.
     *  \param[out] scene Scene object to store scene data.
     *  \param[out] metaData Object to store scene meta data.
     *  \return Returns ´true´ if building was successful, else ´false´.
     */
    template <class SpectrumType>
    bool SceneParser::buildScene(const Json::Value sceneDescription, std::shared_ptr<Scene<SpectrumType> >& scene, SceneMetaData& metaData)
    {
        // scene object to store parsed data
        std::shared_ptr<Scene<SpectrumType> > parsedScene(new Scene<SpectrumType>);
        
        // build geometric objects
        if (!buildGeometry<SpectrumType>(sceneDescription["geometry"]["objects"], parsedScene))
        {
            return false;
        }
        
        // background color
        if (!sceneDescription["geometry"].isMember("backgroundColor"))
        {
            std::cout << "No valid background color specified. Using black." << std::endl;
            parsedScene->backgroundColor_ = SpectrumType(0.f);
        }
        else
        {
            SpectrumType color;
            if (!buildColor(sceneDescription["geometry"]["backgroundColor"], color, lightType_))
            {
                return false;
            }
            parsedScene->backgroundColor_ = color;
        }
        
        // build camera
        if (!buildCamera(sceneDescription["camera"], parsedScene, metaData))
        {
            return false;
        }
        
        // build lights
        if (!buildLights(sceneDescription["lights"], parsedScene))
        {
            return false;
        }
        
        // check tracer
        if (!checkKey(sceneDescription, "tracer", PropertyType::ObjectValue, false))
        {
            // use RayCast as default tracer
            std::shared_ptr<Tracer<SpectrumType> > tracer(new RayCast<SpectrumType>(parsedScene));
            parsedScene->setTracer(tracer);
        }
        else
        {
            if (!buildTracer<SpectrumType>(sceneDescription["tracer"], parsedScene))
            {
                return false;
            }
        }
        
        // parsing successful -> keep scene data
        *scene = *parsedScene;
        return true;
    }



    /*! \brief Build geometric data.
     *  \param[in] geometryDescription <a HREF="http://www.json.org">JSON</a> value of scene geometry
     *  description.
     *  \param[out] scene Scene object to store geometric data.
     *  \return Returns ´true´ if building was successful, else ´false´.
     */
    template <class SpectrumType>
    bool SceneParser::buildGeometry(const Json::Value& geometryDescription, std::shared_ptr<Scene<SpectrumType> >& scene)
    {
        // build geometric objects
        for (unsigned int i = 0; i < geometryDescription.size(); ++i)
        {
            // check type key
            if (!checkKey(geometryDescription[i], "type", PropertyType::StringValue, true))
            {
                return false;
            }
            
            if (geometryDescription[i]["type"].asString() == "Instance")
            {
                std::vector<std::shared_ptr<GeometricObject<SpectrumType> > > instances;
                if (!buildInstances<SpectrumType>(geometryDescription[i], instances))
                {
                    return false;
                }
                
                for (unsigned int j = 0; j < instances.size(); ++j)
                {
                    scene->addObject(instances[j]);
                }
            }
            else
            {
                std::shared_ptr<GeometricObject<SpectrumType> > geometricObject;
                if (buildGeometricObject<SpectrumType>(geometryDescription[i], geometricObject))
                {
                    scene->addObject(geometricObject);
                }
                else
                {
                    return false;
                }
            }
        }
        
        // parsing successful
        return true;
    }



    /*! \brief Factory method for geometric objects.
     *  \param[in] geometryDescription <a HREF="http://www.json.org">JSON</a> value of scene geometry
     *  description.
     *  \param[out] geometricObject Object to store geometric data.
     *  \return Returns ´true´ if building was successful, else ´false´.
     *
     *  Builds and initializes objects derived from GeometricObject specified by a
     *  <a HREF="http://www.json.org">JSON</a> description. The description must specify a valid type
     *  and the objects properties, according to the type.
     */
    template <class SpectrumType>
    bool SceneParser::buildGeometricObject(const Json::Value& geometryDescription, std::shared_ptr<GeometricObject<SpectrumType> >& geometricObject)
    {
        // build object
        std::shared_ptr<GeometricObject<SpectrumType> > object;
        if (geometryDescription["type"].asString() == "Sphere")
        {
            std::shared_ptr<Sphere<SpectrumType> > sphere;
            if (!buildSphere<SpectrumType>(geometryDescription, sphere))
            {
                return false;
            }
            object = sphere;
        }
        else if (geometryDescription["type"].asString() == "Plane")
        {
            std::shared_ptr<Plane<SpectrumType> > plane;
            if (!buildPlane<SpectrumType>(geometryDescription, plane))
            {
                return false;
            }
            object = plane;
        }
        else if (geometryDescription["type"].asString() == "Triangle")
        {
            std::shared_ptr<Triangle<SpectrumType> > triangle;
            if (!buildTriangle<SpectrumType>(geometryDescription, triangle))
            {
                return false;
            }
            object = triangle;
        }
        else if (geometryDescription["type"].asString() == "Box")
        {
            std::shared_ptr<Box<SpectrumType> > box;
            if (!buildBox<SpectrumType>(geometryDescription, box))
            {
                return false;
            }
            object = box;
        }
        else if (geometryDescription["type"].asString() == "Disk")
        {
            std::shared_ptr<Disk<SpectrumType> > disk;
            if (!buildDisk<SpectrumType>(geometryDescription, disk))
            {
                return false;
            }
            object = disk;
        }
        else if (geometryDescription["type"].asString() == "Square")
        {
            std::shared_ptr<Square<SpectrumType> > square;
            if (!buildSquare<SpectrumType>(geometryDescription, square))
            {
                return false;
            }
            object = square;
        }
        else if (geometryDescription["type"].asString() == "Cylinder")
        {
            std::shared_ptr<OpenCylinder<SpectrumType> > cylinder;
            if (!buildCylinder<SpectrumType>(geometryDescription, cylinder))
            {
                return false;
            }
            object = cylinder;
        }
        else if (geometryDescription["type"].asString() == "Cone")
        {
            std::shared_ptr<OpenCone<SpectrumType> > cone;
            if (!buildCone<SpectrumType>(geometryDescription, cone))
            {
                return false;
            }
            object = cone;
        }
        else if (geometryDescription["type"].asString() == "Torus")
        {
            std::shared_ptr<Torus<SpectrumType> > torus;
            if (!buildTorus<SpectrumType>(geometryDescription, torus))
            {
                return false;
            }
            object = torus;
        }
        else if (geometryDescription["type"].asString() == "Compound")
        {
            std::shared_ptr<Compound<SpectrumType> > compound;
            if (!buildCompound<SpectrumType>(geometryDescription, compound))
            {
                return false;
            }
            object = compound;
        }
        else if (geometryDescription["type"].asString() == "Grid")
        {
            std::shared_ptr<Grid<SpectrumType> > grid;
            if (!buildGrid<SpectrumType>(geometryDescription, grid))
            {
                return false;
            }
            object = grid;
        }
        else if (geometryDescription["type"].asString() == "TriangleMesh")
        {
            std::shared_ptr<TriangleMesh<SpectrumType> > triangleMesh;
            if (!buildTriangleMesh<SpectrumType>(geometryDescription, triangleMesh))
            {
                return false;
            }
            object = triangleMesh;
        }
        else
        {
            // "type" unknown
            std::cout << "Unknown type of geometric object: " << geometryDescription["type"].asString() << std::endl;
            return false;
        }
        
        // check for transformation key
        if (geometryDescription.isMember("transformation"))
        {
            std::shared_ptr<GeometricObject<SpectrumType>> instance;
            if (!buildInstance<SpectrumType>(geometryDescription["transformation"], object, instance))
            {
                return false;
            }
            object = instance;
        }

        if (checkKey(geometryDescription, "castsShadows", PropertyType::BoolValue, false))
        {
            object->setCastsShadows(geometryDescription["castsShadows"].asBool());
        }
        
        // set object
        geometricObject = object;
        
        // building successful
        return true;
    }



    /*! \brief Factory method for geometric objects that can serve as area lights.
     *  \param[in] geometryDescription <a HREF="http://www.json.org">JSON</a> value of scene geometry
     *  description.
     *  \param[out] geometricObject Object to store geometric data.
     *  \return Returns ´true´ if building was successful, else ´false´.
     */
    template <class SpectrumType>
    bool SceneParser::buildAreaLightObject(const Json::Value& geometryDescription, std::shared_ptr<AreaLightObject<SpectrumType> >& geometricObject)
    {
        // build object
        std::shared_ptr<AreaLightObject<SpectrumType> > object;
        if (geometryDescription["type"].asString() == "Square")
        {
            std::shared_ptr<Square<SpectrumType> > square;
            if (!buildSquare<SpectrumType>(geometryDescription, square))
            {
                return false;
            }
            object = square;
        }
        else if (geometryDescription["type"].asString() == "Disk")
        {
            std::shared_ptr<Disk<SpectrumType> > disk;
            if (!buildDisk<SpectrumType>(geometryDescription, disk))
            {
                return false;
            }
            object = disk;
        }
        else if (geometryDescription["type"].asString() == "Sphere")
        {
            std::shared_ptr<Sphere<SpectrumType> > sphere;
            if (!buildSphere<SpectrumType>(geometryDescription, sphere))
            {
                return false;
            }
            object = sphere;
        }
        else
        {
            // "type" unknown
            std::cout << "Unknown type of area light object: " << geometryDescription["type"].asString() << std::endl;
            return false;
        }

        // disable shadows for area lights
        object->setCastsShadows(false);
        
        // set object
        geometricObject = object;
        
        // building successful
        return true;
    }



    /*! \brief Factory method for sphere objects.
     *  \param[in] objectDescription <a HREF="http://www.json.org">JSON</a> object description.
     *  \param[out] geometricObject Reference to built sphere.
     *  \return Returns ´true´ if building was successful, else ´false´.
     */
    template <class SpectrumType>
    bool SceneParser::buildSphere(const Json::Value& objectDescription, std::shared_ptr<Sphere<SpectrumType> >& geometricObject)
    {
        // check properties
        if (!checkKey(objectDescription, "radius", PropertyType::NumberValue, true) ||
            !checkKey(objectDescription, "position", PropertyType::VectorValue, true) ||
            !checkDimension(objectDescription["position"], 3, true))
        {
            return false;
        }
        
        std::shared_ptr<Material<SpectrumType> > material;
        if (checkKey(objectDescription, "material", PropertyType::ObjectValue, false))
        {
            if (!buildMaterial<SpectrumType>(objectDescription["material"], material))
            {
                return false;
            }

            // build object with material
            geometricObject.reset(new Sphere<SpectrumType>(
                objectDescription["position"][0].asDouble(),
                objectDescription["position"][1].asDouble(),
                objectDescription["position"][2].asDouble(),
                objectDescription["radius"].asDouble(),
                material));
        }
        else
        {
            // build object with default material
            geometricObject.reset(new Sphere<SpectrumType>(
                objectDescription["position"][0].asDouble(),
                objectDescription["position"][1].asDouble(),
                objectDescription["position"][2].asDouble(),
                objectDescription["radius"].asDouble()));
        }
        
        // building successful
        return true;
    }



    /*! \brief Factory method for plane objects.
     *  \param[in] objectDescription <a HREF="http://www.json.org">JSON</a> object description.
     *  \param[out] geometricObject Reference to built plane.
     *  \return Returns ´true´ if building was successful, else ´false´.
     */
    template <class SpectrumType>
    bool SceneParser::buildPlane(const Json::Value& objectDescription, std::shared_ptr<Plane<SpectrumType> >& geometricObject)
    {
        // check properties
        if (!checkKey(objectDescription, "point", PropertyType::VectorValue, true) ||
            !checkKey(objectDescription, "normal", PropertyType::VectorValue, true) ||
            !checkDimension(objectDescription["point"], 3, true) ||
            !checkDimension(objectDescription["normal"], 3, true))
        {
            return false;
        }
        
        std::shared_ptr<Material<SpectrumType> > material;
        if (checkKey(objectDescription, "material", PropertyType::ObjectValue, false))
        {
            if (!buildMaterial<SpectrumType>(objectDescription["material"], material))
            {
                return false;
            }

            // build object with material
            geometricObject.reset(new Plane<SpectrumType>(
                objectDescription["point"][0].asDouble(),
                objectDescription["point"][1].asDouble(),
                objectDescription["point"][2].asDouble(),
                objectDescription["normal"][0].asDouble(),
                objectDescription["normal"][1].asDouble(),
                objectDescription["normal"][2].asDouble(),
                material));
        }
        else
        {
            // build object with default material
            geometricObject.reset(new Plane<SpectrumType>(
                objectDescription["point"][0].asDouble(),
                objectDescription["point"][1].asDouble(),
                objectDescription["point"][2].asDouble(),
                objectDescription["normal"][0].asDouble(),
                objectDescription["normal"][1].asDouble(),
                objectDescription["normal"][2].asDouble()));
        }
        
        // building successful
        return true;
    }



    /*! \brief Factory method for triangle objects.
     *  \param[in] objectDescription <a HREF="http://www.json.org">JSON</a> object description.
     *  \param[out] geometricObject Reference to built triangle.
     *  \return Returns ´true´ if building was successful, else ´false´.
     */
    template <class SpectrumType>
    bool SceneParser::buildTriangle(const Json::Value& objectDescription, std::shared_ptr<Triangle<SpectrumType> >& geometricObject)
    {
        // check properties
        if (!checkKey(objectDescription, "v0", PropertyType::VectorValue, true) ||
            !checkKey(objectDescription, "v1", PropertyType::VectorValue, true) ||
            !checkKey(objectDescription, "v2", PropertyType::VectorValue, true) ||
            !checkDimension(objectDescription["v0"], 3, true) ||
            !checkDimension(objectDescription["v1"], 3, true) ||
            !checkDimension(objectDescription["v2"], 3, true))
        {
            return false;
        }
        
        std::shared_ptr<Material<SpectrumType> > material;
        if (checkKey(objectDescription, "material", PropertyType::ObjectValue, false))
        {
            if (!buildMaterial<SpectrumType>(objectDescription["material"], material))
            {
                return false;
            }

            // build object with material
            geometricObject.reset(new Triangle<SpectrumType>(
                Point3D(objectDescription["v0"][0].asDouble(), objectDescription["v0"][1].asDouble(), objectDescription["v0"][2].asDouble()),
                Point3D(objectDescription["v1"][0].asDouble(), objectDescription["v1"][1].asDouble(), objectDescription["v1"][2].asDouble()),
                Point3D(objectDescription["v2"][0].asDouble(), objectDescription["v2"][1].asDouble(), objectDescription["v2"][2].asDouble()),
                material));
        }
        else
        {
            // build object with default material
            geometricObject.reset(new Triangle<SpectrumType>(
                Point3D(objectDescription["v0"][0].asDouble(), objectDescription["v0"][1].asDouble(), objectDescription["v0"][2].asDouble()),
                Point3D(objectDescription["v1"][0].asDouble(), objectDescription["v1"][1].asDouble(), objectDescription["v1"][2].asDouble()),
                Point3D(objectDescription["v2"][0].asDouble(), objectDescription["v2"][1].asDouble(), objectDescription["v2"][2].asDouble())));
        }
        
        // building successful
        return true;
    }



    /*! \brief Factory method for box objects.
     *  \param[in] objectDescription <a HREF="http://www.json.org">JSON</a> object description.
     *  \param[out] geometricObject Reference to built box.
     *  \return Returns ´true´ if building was successful, else ´false´.
     */
    template <class SpectrumType>
    bool SceneParser::buildBox(const Json::Value& objectDescription, std::shared_ptr<Box<SpectrumType> >& geometricObject)
    {
        // check properties
        if (!checkKey(objectDescription, "v0", PropertyType::VectorValue, true) ||
            !checkKey(objectDescription, "v1", PropertyType::VectorValue, true) ||
            !checkDimension(objectDescription["v0"], 3, true) ||
            !checkDimension(objectDescription["v1"], 3, true))
        {
            return false;
        }
        
        std::shared_ptr<Material<SpectrumType> > material;
        if (checkKey(objectDescription, "material", PropertyType::ObjectValue, false))
        {
            if (!buildMaterial<SpectrumType>(objectDescription["material"], material))
            {
                return false;
            }

            // build object with material
            geometricObject.reset(new Box<SpectrumType>(
                Point3D(objectDescription["v0"][0].asDouble(), objectDescription["v0"][1].asDouble(), objectDescription["v0"][2].asDouble()),
                Point3D(objectDescription["v1"][0].asDouble(), objectDescription["v1"][1].asDouble(), objectDescription["v1"][2].asDouble()),
                material));
        }
        else
        {
            // build object with default material
            geometricObject.reset(new Box<SpectrumType>(
                Point3D(objectDescription["v0"][0].asDouble(), objectDescription["v0"][1].asDouble(), objectDescription["v0"][2].asDouble()),
                Point3D(objectDescription["v1"][0].asDouble(), objectDescription["v1"][1].asDouble(), objectDescription["v1"][2].asDouble())));
        }
        
        // building successful
        return true;
    }



    /*! \brief Factory method for disk objects.
     *  \param[in] objectDescription <a HREF="http://www.json.org">JSON</a> object description.
     *  \param[out] geometricObject Reference to built disk.
     *  \return Returns ´true´ if building was successful, else ´false´.
     */
    template <class SpectrumType>
    bool SceneParser::buildDisk(const Json::Value& objectDescription, std::shared_ptr<Disk<SpectrumType> >& geometricObject)
    {
        // check properties
        if (!checkKey(objectDescription, "point", PropertyType::VectorValue, true) ||
            !checkKey(objectDescription, "normal", PropertyType::VectorValue, true) ||
            !checkKey(objectDescription, "radius", PropertyType::NumberValue, true) ||
            !checkDimension(objectDescription["point"], 3, true) ||
            !checkDimension(objectDescription["normal"], 3, true))
        {
            return false;
        }
        
        std::shared_ptr<Material<SpectrumType> > material;
        if (checkKey(objectDescription, "material", PropertyType::ObjectValue, false))
        {
            if (!buildMaterial<SpectrumType>(objectDescription["material"], material))
            {
                return false;
            }

            // build object with material
            geometricObject.reset(new Disk<SpectrumType>(
                objectDescription["point"][0].asDouble(),
                objectDescription["point"][1].asDouble(),
                objectDescription["point"][2].asDouble(),
                objectDescription["normal"][0].asDouble(),
                objectDescription["normal"][1].asDouble(),
                objectDescription["normal"][2].asDouble(),
                objectDescription["radius"].asDouble(),
                material));
        }
        else
        {
            // build object with default material
            geometricObject.reset(new Disk<SpectrumType>(
                objectDescription["point"][0].asDouble(),
                objectDescription["point"][1].asDouble(),
                objectDescription["point"][2].asDouble(),
                objectDescription["normal"][0].asDouble(),
                objectDescription["normal"][1].asDouble(),
                objectDescription["normal"][2].asDouble(),
                objectDescription["radius"].asDouble()));
        }
        
        // building successful
        return true;
    }



    /*! \brief Factory method for square objects.
     *  \param[in] objectDescription <a HREF="http://www.json.org">JSON</a> object description.
     *  \param[out] geometricObject Reference to built square.
     *  \return Returns ´true´ if building was successful, else ´false´.
     */
    template <class SpectrumType>
    bool SceneParser::buildSquare(const Json::Value& objectDescription, std::shared_ptr<Square<SpectrumType> >& geometricObject)
    {
        // check properties
        if (!checkKey(objectDescription, "point", PropertyType::VectorValue, true) ||
            !checkKey(objectDescription, "a", PropertyType::VectorValue, true) ||
            !checkKey(objectDescription, "b", PropertyType::VectorValue, true) ||
            !checkDimension(objectDescription["point"], 3, true) ||
            !checkDimension(objectDescription["a"], 3, true) ||
            !checkDimension(objectDescription["b"], 3, true))
        {
            return false;
        }
        
        // check optional parameters
        bool normal = (checkKey(objectDescription, "normal", PropertyType::VectorValue, false) && checkDimension(objectDescription["normal"], 3, false));
        
        std::shared_ptr<Material<SpectrumType> > material;
        if (checkKey(objectDescription, "material", PropertyType::ObjectValue, false))
        {
            if (!buildMaterial<SpectrumType>(objectDescription["material"], material))
            {
                return false;
            }

            // build object with material
            if (normal)
            {
                geometricObject.reset(new Square<SpectrumType>(
                    Point3D(objectDescription["point"][0].asDouble(),
                    objectDescription["point"][1].asDouble(),
                    objectDescription["point"][2].asDouble()),
                    Vector3D(objectDescription["a"][0].asDouble(),
                    objectDescription["a"][1].asDouble(),
                    objectDescription["a"][2].asDouble()),
                    Vector3D(objectDescription["b"][0].asDouble(),
                    objectDescription["b"][1].asDouble(),
                    objectDescription["b"][2].asDouble()),
                    Normal(objectDescription["normal"][0].asDouble(),
                    objectDescription["normal"][1].asDouble(),
                    objectDescription["normal"][2].asDouble()),
                    material));
            }
            else
            {
                geometricObject.reset(new Square<SpectrumType>(
                    Point3D(objectDescription["point"][0].asDouble(),
                    objectDescription["point"][1].asDouble(),
                    objectDescription["point"][2].asDouble()),
                    Vector3D(objectDescription["a"][0].asDouble(),
                    objectDescription["a"][1].asDouble(),
                    objectDescription["a"][2].asDouble()),
                    Vector3D(objectDescription["b"][0].asDouble(),
                    objectDescription["b"][1].asDouble(),
                    objectDescription["b"][2].asDouble()),
                    material));
            }
        }
        else
        {
            // build object with default material
            if (normal)
            {
                geometricObject.reset(new Square<SpectrumType>(
                    Point3D(objectDescription["point"][0].asDouble(),
                    objectDescription["point"][1].asDouble(),
                    objectDescription["point"][2].asDouble()),
                    Vector3D(objectDescription["a"][0].asDouble(),
                    objectDescription["a"][1].asDouble(),
                    objectDescription["a"][2].asDouble()),
                    Vector3D(objectDescription["b"][0].asDouble(),
                    objectDescription["b"][1].asDouble(),
                    objectDescription["b"][2].asDouble()),
                    Normal(objectDescription["normal"][0].asDouble(),
                    objectDescription["normal"][1].asDouble(),
                    objectDescription["normal"][2].asDouble())));
            }
            else
            {
                geometricObject.reset(new Square<SpectrumType>(
                    Point3D(objectDescription["point"][0].asDouble(),
                    objectDescription["point"][1].asDouble(),
                    objectDescription["point"][2].asDouble()),
                    Vector3D(objectDescription["a"][0].asDouble(),
                    objectDescription["a"][1].asDouble(),
                    objectDescription["a"][2].asDouble()),
                    Vector3D(objectDescription["b"][0].asDouble(),
                    objectDescription["b"][1].asDouble(),
                    objectDescription["b"][2].asDouble())));
            }
        }
        
        // building successful
        return true;
    }



    /*! \brief Factory method for cylinder objects.
     *  \param[in] objectDescription <a HREF="http://www.json.org">JSON</a> object description.
     *  \param[out] geometricObject Reference to built cylinder.
     *  \return Returns ´true´ if building was successful, else ´false´.
     */
    template <class SpectrumType>
    bool SceneParser::buildCylinder(const Json::Value& objectDescription, std::shared_ptr<OpenCylinder<SpectrumType> >& geometricObject)
    {
        // check properties
        if (!checkKey(objectDescription, "position", PropertyType::VectorValue, true) ||
            !checkKey(objectDescription, "radius", PropertyType::NumberValue, true) ||
            !checkKey(objectDescription, "zMin", PropertyType::NumberValue, true) ||
            !checkKey(objectDescription, "zMax", PropertyType::NumberValue, true) ||
            !checkDimension(objectDescription["position"], 3, true))
        {
            return false;
        }
        
        std::shared_ptr<Material<SpectrumType>> material;
        if (checkKey(objectDescription, "material", PropertyType::ObjectValue, false))
        {
            if (!buildMaterial<SpectrumType>(objectDescription["material"], material))
            {
                return false;
            }

            // build object with material
            geometricObject.reset(new OpenCylinder<SpectrumType>(
                Point3D(objectDescription["position"][0].asDouble(),
                objectDescription["position"][1].asDouble(),
                objectDescription["position"][2].asDouble()),
                objectDescription["radius"].asDouble(),
                objectDescription["zMin"].asDouble(),
                objectDescription["zMax"].asDouble(),
                material));
        }
        else
        {
            // build object with default material
            geometricObject.reset(new OpenCylinder<SpectrumType>(
                Point3D(objectDescription["position"][0].asDouble(),
                objectDescription["position"][1].asDouble(),
                objectDescription["position"][2].asDouble()),
                objectDescription["radius"].asDouble(),
                objectDescription["zMin"].asDouble(),
                objectDescription["zMax"].asDouble()));
        }
        
        // building successful
        return true;
    }



    /*! \brief Factory method for cone objects.
     *  \param[in] objectDescription <a HREF="http://www.json.org">JSON</a> object description.
     *  \param[out] geometricObject Reference to built cone.
     *  \return Returns ´true´ if building was successful, else ´false´.
     */
    template <class SpectrumType>
    bool SceneParser::buildCone(const Json::Value& objectDescription, std::shared_ptr<OpenCone<SpectrumType> >& geometricObject)
    {
        // check properties
        if (!checkKey(objectDescription, "position", PropertyType::VectorValue, true) ||
            !checkKey(objectDescription, "minRadius", PropertyType::NumberValue, true) ||
            !checkKey(objectDescription, "maxRadius", PropertyType::NumberValue, true) ||
            !checkKey(objectDescription, "zMin", PropertyType::NumberValue, true) ||
            !checkKey(objectDescription, "zMax", PropertyType::NumberValue, true) ||
            !checkDimension(objectDescription["position"], 3, true))
        {
            return false;
        }
        
        std::shared_ptr<Material<SpectrumType> > material;
        if (checkKey(objectDescription, "material", PropertyType::ObjectValue, false))
        {
            if (!buildMaterial<SpectrumType>(objectDescription["material"], material))
            {
                return false;
            }

            // build object with material
            geometricObject.reset(new OpenCone<SpectrumType>(
                Point3D(objectDescription["position"][0].asDouble(),
                objectDescription["position"][1].asDouble(),
                objectDescription["position"][2].asDouble()),
                objectDescription["minRadius"].asDouble(),
                objectDescription["maxRadius"].asDouble(),
                objectDescription["zMin"].asDouble(),
                objectDescription["zMax"].asDouble(),
                material));
        }
        else
        {
            // build object with default material
            geometricObject.reset(new OpenCone<SpectrumType>(
                Point3D(objectDescription["position"][0].asDouble(),
                objectDescription["position"][1].asDouble(),
                objectDescription["position"][2].asDouble()),
                objectDescription["minRadius"].asDouble(),
                objectDescription["maxRadius"].asDouble(),
                objectDescription["zMin"].asDouble(),
                objectDescription["zMax"].asDouble()));
        }
        
        // building successful
        return true;
    }



    /*! \brief Factory method for torus objects.
     *  \param[in] objectDescription <a HREF="http://www.json.org">JSON</a> object description.
     *  \param[out] geometricObject Reference to built torus.
     *  \return Returns ´true´ if building was successful, else ´false´.
     */
    template <class SpectrumType>
    bool SceneParser::buildTorus(const Json::Value& objectDescription, std::shared_ptr<Torus<SpectrumType> >& geometricObject)
    {
        // check properties
        if (!checkKey(objectDescription, "innerRadius", PropertyType::NumberValue, true) ||
            !checkKey(objectDescription, "outerRadius", PropertyType::NumberValue, true))
        {
            return false;
        }
        
        std::shared_ptr<Material<SpectrumType> > material;
        if (checkKey(objectDescription, "material", PropertyType::ObjectValue, false))
        {
            if (!buildMaterial<SpectrumType>(objectDescription["material"], material))
            {
                return false;
            }

            // build object with material
            geometricObject.reset(new Torus<SpectrumType>(
                objectDescription["innerRadius"].asDouble(),
                objectDescription["outerRadius"].asDouble(),
                material));
        }
        else
        {
            // build object with default material
            geometricObject.reset(new Torus<SpectrumType>(
                objectDescription["innerRadius"].asDouble(),
                objectDescription["outerRadius"].asDouble()));
        }
        
        // building successful
        return true;
    }



    /*! \brief Factory method for compound objects.
     *  \param[in] objectDescription <a HREF="http://www.json.org">JSON</a> object description.
     *  \param[out] geometricObject Reference to built compound.
     *  \return Returns ´true´ if building was successful, else ´false´.
     */
    template <class SpectrumType>
    bool SceneParser::buildCompound(const Json::Value& objectDescription, std::shared_ptr<Compound<SpectrumType> >& geometricObject)
    {
        // check properties
        if (!checkKey(objectDescription, "objects", PropertyType::ArrayValue, true))
        {
            return false;
        }
        
        std::shared_ptr<Compound<SpectrumType> > compound(new Compound<SpectrumType>);
        for (unsigned int i = 0; i < objectDescription["objects"].size(); ++i)
        {
            // check type key
            if (!checkKey(objectDescription["objects"][i], "type", PropertyType::StringValue, true))
            {
                return false;
            }
            
            if (objectDescription["objects"][i]["type"].asString() == "Instance")
            {
                // try to build object instances
                std::vector<std::shared_ptr<GeometricObject<SpectrumType> > > instances;
                if (!buildInstances<SpectrumType>(objectDescription["objects"][i], instances))
                {
                    return false;
                }
                
                for (unsigned int j = 0; j < instances.size(); ++j)
                {
                    compound->addObject(instances[j]);
                }
            }
            else
            {
                // try to build object
                std::shared_ptr<GeometricObject<SpectrumType> > object;
                if (buildGeometricObject<SpectrumType>(objectDescription["objects"][i], object))
                {
                    // add object to compound
                    compound->addObject(object);
                }
                else
                {
                    return false;
                }
            }
        }
        
        // assign common material
        std::shared_ptr<Material<SpectrumType> > material;
        if (checkKey(objectDescription, "material", PropertyType::ObjectValue, false))
        {
            if (!buildMaterial<SpectrumType>(objectDescription["material"], material))
            {
                return false;
            }

            compound->setMaterial(material);
        }
        
        // building successful
        geometricObject = compound;
        return true;
    }



    /*! \brief Factory method for grid objects.
     *  \param[in] objectDescription <a HREF="http://www.json.org">JSON</a> object description.
     *  \param[out] geometricObject Reference to built grid.
     *  \return Returns ´true´ if building was successful, else ´false´.
     */
    template <class SpectrumType>
    bool SceneParser::buildGrid(const Json::Value& objectDescription, std::shared_ptr<Grid<SpectrumType> >& geometricObject)
    {
        // check properties
        if (!checkKey(objectDescription, "objects", PropertyType::ArrayValue, true))
        {
            return false;
        }
        
        std::shared_ptr<Grid<SpectrumType> > grid(new Grid<SpectrumType>);
        for (unsigned int i = 0; i < objectDescription["objects"].size(); ++i)
        {
            // check type key
            if (!checkKey(objectDescription["objects"][i], "type", PropertyType::StringValue, true))
            {
                return false;
            }
            
            if (objectDescription["objects"][i]["type"].asString() == "Instance")
            {
                // try to build object instances
                std::vector<std::shared_ptr<GeometricObject<SpectrumType> > > instances;
                if (!buildInstances<SpectrumType>(objectDescription["objects"][i], instances))
                {
                    return false;
                }
                
                for (unsigned int j = 0; j < instances.size(); ++j)
                {
                    grid->addObject(instances[j]);
                }
            }
            else
            {
                // try to build object
                std::shared_ptr<GeometricObject<SpectrumType> > object;
                if (buildGeometricObject<SpectrumType>(objectDescription["objects"][i], object))
                {
                    // add object to compound
                    grid->addObject(object);
                }
                else
                {
                    return false;
                }
            }
        }
        
        // assign common material
        std::shared_ptr<Material<SpectrumType> > material;
        if (checkKey(objectDescription, "material", PropertyType::ObjectValue, false))
        {
            if (!buildMaterial<SpectrumType>(objectDescription["material"], material))
            {
                return false;
            }

            grid->setMaterial(material);
        }
        
        // building successful
        grid->initGrid();
        geometricObject = grid;
        return true;
    }



    /*! \brief Factory method for triangle meshes.
     *  \param[in] objectDescription <a HREF="http://www.json.org">JSON</a> object description.
     *  \param[out] geometricObject Reference to built triangle mesh.
     *  \return Returns ´true´ if building was successful, else ´false´.
     */
    template <class SpectrumType>
    bool SceneParser::buildTriangleMesh(const Json::Value& objectDescription, std::shared_ptr<TriangleMesh<SpectrumType> >& geometricObject)
    {
        // check properties
        if (!checkKey(objectDescription, "plyFile", PropertyType::StringValue, true))
        {
            return false;
        }
        
        // check optional parameters
        typename TriangleMesh<SpectrumType>::Type type = TriangleMesh<SpectrumType>::Type::Flat;
        if (checkKey(objectDescription, "shading", PropertyType::StringValue, false))
        {
            if (objectDescription["shading"].asString() == "Flat" || objectDescription["shading"].asString() == "flat")
            {
                type = TriangleMesh<SpectrumType>::Type::Flat;
            }
            else if (objectDescription["shading"].asString() == "Smooth" || objectDescription["shading"].asString() == "smooth")
            {
                type = TriangleMesh<SpectrumType>::Type::Smooth;
            }
            else
            {
                std::cout << "Shading """ << objectDescription["shading"].asString() << """ unknown." << std::endl;
                return false;
            }
        }
        else
        {
            std::cout << "No ""shading"" defined. Using default." << std::endl;
        }
        
        std::shared_ptr<TriangleMesh<SpectrumType> > triangleMesh(new TriangleMesh<SpectrumType>());
        
        // open ply file
        p_ply plyFile = ply_open(std::string(filePath_).append(objectDescription["plyFile"].asCString()).c_str(), NULL, 0, NULL);
        if (!plyFile)
        {
            std::cout << "PLY file '" << objectDescription["plyFile"].asCString() << "' could not be opened." << std::endl;
            return false;
        }

        // parse header
        if (!ply_read_header(plyFile))
        {
            std::cout << "Header of PLY file '" << objectDescription["plyFile"].asCString() << "' could not be parsed." << std::endl;
            return false;
        }

        // setup callback functions to extract ply-data
        long numVertices = ply_set_read_cb(plyFile, "vertex", "x", vertexCallback<SpectrumType>, &triangleMesh, 0);
        ply_set_read_cb(plyFile, "vertex", "y", vertexCallback<SpectrumType>, &triangleMesh, 1);
        ply_set_read_cb(plyFile, "vertex", "z", vertexCallback<SpectrumType>, &triangleMesh, 2);
        long numFaces = ply_set_read_cb(plyFile, "face", "vertex_indices", faceCallback<SpectrumType>, &triangleMesh, type);

        // reserve memory
        triangleMesh->mesh_->vertices_.reserve(numVertices);
        triangleMesh->reserve(numFaces);

        triangleMesh->mesh_->numVertices_ = numVertices;
        triangleMesh->mesh_->numTriangles_ = numFaces;

        if (type == TriangleMesh<SpectrumType>::Type::Smooth)
        {
            for (int j = 0; j < triangleMesh->mesh_->numVertices_; ++j)
            {
                triangleMesh->mesh_->vertexFaces_.push_back(std::vector<int>());
            }
        }

        // actually parse the file body
        if (!ply_read(plyFile))
        {
            return false;
        }

        // close file
        ply_close(plyFile);

        // post-process triangle data
        if (type == TriangleMesh<SpectrumType>::Type::Smooth)
        {
            triangleMesh->computeMeshNormals();
        }
        
        // assign common material
        std::shared_ptr<Material<SpectrumType> > material;
        if (checkKey(objectDescription, "material", PropertyType::ObjectValue, false))
        {
            if (!buildMaterial<SpectrumType>(objectDescription["material"], material))
            {
                return false;
            }

            triangleMesh->setMaterial(material);
        }
        
        // building successful
        triangleMesh->initGrid();
        geometricObject = triangleMesh;
        return true;
    }



    /*! \brief Callback function for extracting TriangleMesh vertec data.
     *  \param[in] argument Data to extract.
     *  \return Returns 1, which signals to proceed with the parsing.
     */
    template <class SpectrumType>
    int SceneParser::vertexCallback(p_ply_argument argument)
    {
        long index;
        std::shared_ptr<TriangleMesh<SpectrumType> >* triangleMesh;
        ply_get_argument_user_data(argument, (void**)&triangleMesh, &index);
        if (index == 0)
        {
             (*triangleMesh)->mesh_->vertices_.push_back(Point3D(ply_get_argument_value(argument), 0.0, 0.0));
        }
        else if (index == 1)
        {
            (*triangleMesh)->mesh_->vertices_.back().y_ = ply_get_argument_value(argument);
        }
        else
        {
            (*triangleMesh)->mesh_->vertices_.back().z_ = ply_get_argument_value(argument);
        }
        return 1;
    }



    /*! \brief Callback function for extracting TriangleMesh face data.
     *  \param[in] argument Data to extract.
     *  \return Returns 1, which signals to proceed with the parsing.
     */
    template <class SpectrumType>
    int SceneParser::faceCallback(p_ply_argument argument)
    {
        long length;
        long valueIndex;
        long type;
        std::shared_ptr<TriangleMesh<SpectrumType> >* triangleMesh;
        ply_get_argument_user_data(argument, (void**)&triangleMesh, &type);
        ply_get_argument_property(argument, NULL, &length, &valueIndex);
        switch (valueIndex)
        {
            case -1:
            {
                if (static_cast<typename TriangleMesh<SpectrumType>::Type>(type) == TriangleMesh<SpectrumType>::Type::Flat)
                {
                    std::shared_ptr<FlatMeshTriangle<SpectrumType> > triangle(new FlatMeshTriangle<SpectrumType>(
                        std::weak_ptr<Mesh>((*triangleMesh)->mesh_), 0, 0, 0));
                    (*triangleMesh)->addObject(triangle);
                }
                else
                {
                    std::shared_ptr<SmoothMeshTriangle<SpectrumType> > triangle(new SmoothMeshTriangle<SpectrumType>(
                        std::weak_ptr<Mesh>((*triangleMesh)->mesh_), 0, 0, 0));
                    (*triangleMesh)->addObject(triangle);
                }
            }
            break;
            case 0:
            {
                if (static_cast<typename TriangleMesh<SpectrumType>::Type>(type) == TriangleMesh<SpectrumType>::Type::Flat)
                {
                    std::shared_ptr<FlatMeshTriangle<SpectrumType> > triangle = std::dynamic_pointer_cast<FlatMeshTriangle<SpectrumType> >(
                        (*triangleMesh)->objects_.back());
                    triangle->vertexIndices_[0] = static_cast<int>(ply_get_argument_value(argument));
                }
                else
                {
                    std::shared_ptr<SmoothMeshTriangle<SpectrumType> > triangle = std::dynamic_pointer_cast<SmoothMeshTriangle<SpectrumType> >(
                        (*triangleMesh)->objects_.back());
                    int index = static_cast<int>(ply_get_argument_value(argument));
                    triangle->vertexIndices_[0] = index;
                    (*triangleMesh)->mesh_->vertexFaces_[index].push_back(static_cast<int>((*triangleMesh)->objects_.size() - 1));
                }
            }
            break;
            case 1:
            {
                if (static_cast<typename TriangleMesh<SpectrumType>::Type>(type) == TriangleMesh<SpectrumType>::Type::Flat)
                {
                    std::shared_ptr<FlatMeshTriangle<SpectrumType> > triangle = std::dynamic_pointer_cast<FlatMeshTriangle<SpectrumType> >(
                        (*triangleMesh)->objects_.back());
                    triangle->vertexIndices_[1] = static_cast<int>(ply_get_argument_value(argument));
                }
                else
                {
                    std::shared_ptr<SmoothMeshTriangle<SpectrumType> > triangle = std::dynamic_pointer_cast<SmoothMeshTriangle<SpectrumType> >(
                        (*triangleMesh)->objects_.back());
                    int index = static_cast<int>(ply_get_argument_value(argument));
                    triangle->vertexIndices_[1] = index;
                    (*triangleMesh)->mesh_->vertexFaces_[index].push_back(static_cast<int>((*triangleMesh)->objects_.size() - 1));
                }
            }
            break;
            case 2:
            {
                if (static_cast<typename TriangleMesh<SpectrumType>::Type>(type) == TriangleMesh<SpectrumType>::Type::Flat)
                {
                    std::shared_ptr<FlatMeshTriangle<SpectrumType> > triangle = std::dynamic_pointer_cast<FlatMeshTriangle<SpectrumType> >(
                        (*triangleMesh)->objects_.back());
                    triangle->vertexIndices_[2] = static_cast<int>(ply_get_argument_value(argument));
                    triangle->computeNormal(false);
                }
                else
                {
                    std::shared_ptr<SmoothMeshTriangle<SpectrumType> > triangle = std::dynamic_pointer_cast<SmoothMeshTriangle<SpectrumType> >(
                        (*triangleMesh)->objects_.back());
                    int index = static_cast<int>(ply_get_argument_value(argument));
                    triangle->vertexIndices_[2] = index;
                    (*triangleMesh)->mesh_->vertexFaces_[index].push_back(static_cast<int>((*triangleMesh)->objects_.size() - 1));
                    triangle->computeNormal(false);
                }
            }
            break;
            default:
            break;
        }
        return 1;
    }



    /*! \brief Builds instances of one object.
     *  \param[in] objectDescription <a HREF="http://www.json.org">JSON</a> object description.
     *  \param[out] geometricObjects Vector of build instances.
     */
    template <class SpectrumType>
    bool SceneParser::buildInstances(const Json::Value& objectDescription, std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >& geometricObjects)
    {
        // check properties
        if (!checkKey(objectDescription, "object", PropertyType::ObjectValue, true) ||
            !checkKey(objectDescription, "transformations", PropertyType::ArrayValue, true))
        {
            return false;
        }
        
        // check forbidden properties
        if (objectDescription.isMember("transformation"))
        {
            std::cout << """transformation"" forbidden in instance description. Use ""transformations""" << std::endl;
            return false;
        }
        
        // check optional parameters
        bool buildMaterials = false;
        if (checkKey(objectDescription, "materials", PropertyType::ArrayValue, false))
        {
            if (!checkDimension(objectDescription["materials"], objectDescription["transformations"].size(), true))
            {
                return false;
            }
            
            buildMaterials = true;
        }
        
        // build object
        std::shared_ptr<GeometricObject<SpectrumType> > object;
        if (!buildGeometricObject<SpectrumType>(objectDescription["object"], object))
        {
            return false;
        }
        
        // build instances
        for (unsigned int i = 0; i < objectDescription["transformations"].size(); ++i)
        {
            // build instance from object
            std::shared_ptr<Instance<SpectrumType> > instanceObject(new Instance<SpectrumType>(object));
            
            // transform instance
            if (!checkValue(objectDescription["transformations"][i], PropertyType::ArrayValue, true) ||
                !transformInstance(objectDescription["transformations"][i], instanceObject))
            {
                return false;
            }
            
            // set material of instance
            if (buildMaterials)
            {
                std::shared_ptr<Material<SpectrumType> > material;
                if (!buildMaterial<SpectrumType>(objectDescription["materials"][i], material))
                {
                    return false;
                }
                instanceObject->setMaterial(material);
            }
            
            // add instance to objects
            geometricObjects.push_back(instanceObject);
        }
        
        // building successful
        return true;
    }



    /*! \brief Puts object in Instance class and applies transformations.
     *  \param[in] transformationDescription <a HREF="http://www.json.org">JSON</a> value of
     *  transformation description.
     *  \param[in] object Object to transform.
     *  \param[out] instance Instance object.
     *
     *  To put an object in an instance, assign an empty array as value of key "transformation".
     */
    template <class SpectrumType>
    bool SceneParser::buildInstance(const Json::Value& transformationDescription, std::shared_ptr<GeometricObject<SpectrumType> > object, std::shared_ptr<GeometricObject<SpectrumType> >& instance)
    {
        std::shared_ptr<Instance<SpectrumType> > instanceObject(new Instance<SpectrumType>(object));
        if (!transformInstance<SpectrumType>(transformationDescription, instanceObject))
        {
            return false;
        }
        
        instance = instanceObject;
        return true;
    }



    /*! \brief Parses and applies transformation to object instance.
     *  \param[in] transformationDescription <a HREF="http://www.json.org">JSON</a> value of
     *  transformation description.
     *  \param[in,out] instanceObject Object instance.
     */
    template <class SpectrumType>
    bool SceneParser::transformInstance(const Json::Value& transformationDescription, std::shared_ptr<Instance<SpectrumType> >& instanceObject)
    {
        for (unsigned int i = 0; i < transformationDescription.size(); ++i)
        {
            if (transformationDescription[i].size() != 1)
            {
                std::cout << "Only one transformation key per transformation object allowed." << std::endl;
                return false;
            }
            
            if (transformationDescription[i].getMemberNames()[0] == "translation")
            {
                if (!checkKey(transformationDescription[i], "translation", PropertyType::VectorValue, true) ||
                    !checkDimension(transformationDescription[i]["translation"], 3, true))
                {
                    return false;
                }
                instanceObject->translate(transformationDescription[i]["translation"][0].asFloat(),
                    transformationDescription[i]["translation"][1].asFloat(),
                    transformationDescription[i]["translation"][2].asFloat());
            }
            else if (transformationDescription[i].getMemberNames()[0] == "xRotation")
            {
                if (!checkKey(transformationDescription[i], "xRotation", PropertyType::NumberValue, true))
                {
                    return false;
                }
                instanceObject->rotateX(transformationDescription[i]["xRotation"].asFloat());
            }
            else if (transformationDescription[i].getMemberNames()[0] == "yRotation")
            {
                if (!checkKey(transformationDescription[i], "yRotation", PropertyType::NumberValue, true))
                {
                    return false;
                }
                instanceObject->rotateY(transformationDescription[i]["yRotation"].asFloat());
            }
            else if (transformationDescription[i].getMemberNames()[0] == "zRotation")
            {
                if (!checkKey(transformationDescription[i], "zRotation", PropertyType::NumberValue, true))
                {
                    return false;
                }
                instanceObject->rotateZ(transformationDescription[i]["zRotation"].asFloat());
            }
            else if (transformationDescription[i].getMemberNames()[0] == "scale")
            {
                if (!checkKey(transformationDescription[i], "scale", PropertyType::VectorValue, true) ||
                    !checkDimension(transformationDescription[i]["scale"], 3, true))
                {
                    return false;
                }
                instanceObject->scale(transformationDescription[i]["scale"][0].asFloat(),
                    transformationDescription[i]["scale"][1].asFloat(),
                    transformationDescription[i]["scale"][2].asFloat());
            }
            else if (transformationDescription[i].getMemberNames()[0] == "shear")
            {
                if (!checkKey(transformationDescription[i], "shear", PropertyType::VectorValue, true) ||
                    !checkDimension(transformationDescription[i]["shear"], 6, true))
                {
                    return false;
                }
                instanceObject->shear(transformationDescription[i]["shear"][0].asFloat(),
                    transformationDescription[i]["shear"][1].asFloat(),
                    transformationDescription[i]["shear"][2].asFloat(),
                    transformationDescription[i]["shear"][3].asFloat(),
                    transformationDescription[i]["shear"][4].asFloat(),
                    transformationDescription[i]["shear"][5].asFloat());
            }
            else if (transformationDescription[i].getMemberNames()[0] == "matrix")
            {
                if (!checkKey(transformationDescription[i], "matrix", PropertyType::MatrixValue, true) ||
                    !checkMatrixDimension(transformationDescription[i]["matrix"], 4, 4, true))
                {
                    return false;
                }
                Matrix4x4 matrix(transformationDescription[i]["matrix"][0][0].asDouble(),
                    transformationDescription[i]["matrix"][0][1].asDouble(),
                    transformationDescription[i]["matrix"][0][2].asDouble(),
                    transformationDescription[i]["matrix"][0][3].asDouble(),
                    transformationDescription[i]["matrix"][1][0].asDouble(),
                    transformationDescription[i]["matrix"][1][1].asDouble(),
                    transformationDescription[i]["matrix"][1][2].asDouble(),
                    transformationDescription[i]["matrix"][1][3].asDouble(),
                    transformationDescription[i]["matrix"][2][0].asDouble(),
                    transformationDescription[i]["matrix"][2][1].asDouble(),
                    transformationDescription[i]["matrix"][2][2].asDouble(),
                    transformationDescription[i]["matrix"][2][3].asDouble(),
                    transformationDescription[i]["matrix"][3][0].asDouble(),
                    transformationDescription[i]["matrix"][3][1].asDouble(),
                    transformationDescription[i]["matrix"][3][2].asDouble(),
                    transformationDescription[i]["matrix"][3][3].asDouble());
                instanceObject->addTransformation(matrix);
            }
        }
        return true;
    }



    /*! \brief Build camera from scene description.
     *  \param[in] cameraDescription <a HREF="http://www.json.org">JSON</a> value of camera description.
     *  \param[out] scene Scene object to store camera data.
     *  \param[out] metaData Object to store scene meta data.
     *  \return Returns TRUE if parsing was successful, else FALSE.
     */
    template <class SpectrumType>
    bool SceneParser::buildCamera(const Json::Value& cameraDescription, std::shared_ptr<Scene<SpectrumType> > scene, SceneMetaData& metaData)
    {
        std::shared_ptr<Camera<SpectrumType> > camera;
        
        // ensure that "type" is defined
        if (!checkKey(cameraDescription, "type", PropertyType::StringValue, true))
        {
            return false;
        }
        
        // build camera state
        Point3D eye;
        Point3D lookAt;
        Vector3D up;
        if (!buildCameraState(cameraDescription, eye, lookAt, up))
        {
            return false;
        }

        // build sensor
        Sensor<SpectrumType> sensor;
        if (!buildSensor(cameraDescription, sensor))
        {
            return false;
        }



        try
        {
            // build camera
            if (cameraDescription["type"].asString() == "Orthographic")
            {
                camera.reset(new Orthographic<SpectrumType>(scene, eye, lookAt, up, sensor));
            }
            else if (cameraDescription["type"].asString() == "Pinhole")
            {
                if (!checkKey(cameraDescription, "imageDistance", PropertyType::NumberValue, true))
                {
                    return false;
                }
                camera.reset(new Pinhole<SpectrumType>(scene, eye, lookAt, up, sensor, cameraDescription["imageDistance"].asDouble()));
            }
            else if (cameraDescription["type"].asString() == "ThinLens")
            {
                if (!checkKey(cameraDescription, "lensRadius", PropertyType::NumberValue, true))
                {
                    return false;
                }

                float imageDistance;
                float focusDistance;

                calcOptics(cameraDescription, imageDistance, focusDistance);

                // build camera
                ThinLens<SpectrumType>* cameraPtr = new ThinLens<SpectrumType>(
                    scene,
                    eye,
                    lookAt,
                    up,
                    sensor,
                    cameraDescription["lensRadius"].asFloat(),
                    imageDistance,
                    focusDistance);

                // check existence of "sampler" key
                std::shared_ptr<Sampler> sampler;
                if (!checkKey(cameraDescription, "sampler", PropertyType::ObjectValue, false))
                {
                    std::cout << "No ""sampler"" found. Assuming regular sampling with one sample." << std::endl;
                    sampler = std::shared_ptr<Sampler>(new Regular());
                    sampler->generateSamples();
                }
                else
                {
                    if (!buildSampler(cameraDescription["sampler"], sampler))
                    {
                        return false;
                    }
                }

                // map samples to unit disk
                sampler->mapSamplesToUnitDisk();
            
                // set sampler
                cameraPtr->setSampler(std::shared_ptr<Sampler>(sampler));

                // set camera
                camera.reset(cameraPtr);
            }
            else if (cameraDescription["type"].asString() == "LightFieldReference")
            {
                if (!checkKey(cameraDescription, "lensRadius", PropertyType::NumberValue, true) ||
                    !checkKey(cameraDescription, "microLensRadius", PropertyType::NumberValue, true) ||
                    !checkKey(cameraDescription, "numULenses", PropertyType::NumberValue, true) ||
                    !checkKey(cameraDescription, "numVLenses", PropertyType::NumberValue, true) ||
                    !checkKey(cameraDescription, "numAngSteps", PropertyType::NumberValue, true))
                {
                    return false;
                }

                float lensRadius = cameraDescription["lensRadius"].asFloat();
                float microlensRadius = cameraDescription["microLensRadius"].asFloat();

                int numULenses = cameraDescription["numULenses"].asInt();
                int numVLenses = cameraDescription["numVLenses"].asInt();
                int numAngSteps = cameraDescription["numAngSteps"].asInt();

                float subapertureRadius = lensRadius/float(numAngSteps);
                if (checkKey(cameraDescription, "subapertureRadius", PropertyType::NumberValue, true))
                {
                    subapertureRadius = cameraDescription["subapertureRadius"].asFloat();
                }

                float imageDistance;
                float focusDistance;

                calcOptics(cameraDescription, imageDistance, focusDistance);

                // build camera
                iiit::LightFieldReference<SpectrumType>* cameraPtr = new LightFieldReference<SpectrumType>(
                    scene,
                    eye,
                    lookAt,
                    up,
                    sensor,
                    lensRadius,
                    microlensRadius,
                    subapertureRadius,
                    imageDistance,
                    focusDistance,
                    numULenses,
                    numVLenses,
                    numAngSteps);

                // Initialize full viewplane
                scene->viewPlane_.uRes_ = numULenses * numAngSteps;
                scene->viewPlane_.vRes_ = numVLenses * numAngSteps;
                scene->viewPlane_.uAbsRes_ = scene->viewPlane_.uRes_;
                scene->viewPlane_.vAbsRes_ = scene->viewPlane_.vRes_;
                scene->viewPlane_.s_ = 2*cameraDescription["microLensRadius"].asDouble();

                // check existence of "sampler" key
                std::shared_ptr<Sampler> lensSampler;
                std::shared_ptr<Sampler> virtualSensorSampler;

                if (!checkKey(cameraDescription, "sampler", PropertyType::ObjectValue, false))
                {
                    std::cout << "No ""sampler"" found. Assuming regular sampling with one sample." << std::endl;
                    lensSampler = std::shared_ptr<Sampler>(new Regular());
                    virtualSensorSampler = std::shared_ptr<Sampler>(new Regular());
                    lensSampler->generateSamples();
                    virtualSensorSampler->generateSamples();
                }
                else
                {
                    if (!buildSampler(cameraDescription["sampler"], lensSampler) ||
                        !buildSampler(cameraDescription["sampler"], virtualSensorSampler) )
                    {
                        return false;
                    }
                }

                // set sampler
                cameraPtr->setLensSampler(std::shared_ptr<Sampler>(lensSampler));
                cameraPtr->setVirtualSensorSampler(std::shared_ptr<Sampler>(virtualSensorSampler));

                // set camera
                camera.reset(cameraPtr);
            }
            else if (cameraDescription["type"].asString() == "LightFieldCamera")
            {
                if (!checkKey(cameraDescription, "lensRadius", PropertyType::NumberValue, true) ||
                    !checkKey(cameraDescription, "microLensRadius", PropertyType::NumberValue, true))
                {
                    return false;
                }

                float imageDistance;
                float focusDistance;
                double mlRadius = cameraDescription["microLensRadius"].asDouble();

                calcOptics(cameraDescription, imageDistance, focusDistance);

                // Build microlens array (and save coordinates to csv)
                std::shared_ptr<AbstractGrid<SpectrumType> > microLensArray(new AbstractGrid<SpectrumType> );

                // check grid description
                if (!checkKey(cameraDescription, "grid", PropertyType::ObjectValue, true))
                {
                    std::cout << "No grid description found." << std::endl;
                    return false;
                }

                double s_size_x = cameraDescription["viewPlane"]["resolution"][0].asDouble();
                double s_size_y = cameraDescription["viewPlane"]["resolution"][1].asDouble();

                double size_x = s_size_x * cameraDescription["viewPlane"]["pixelSize"].asDouble();
                double size_y = s_size_y * cameraDescription["viewPlane"]["pixelSize"].asDouble();

                double x_max = 0.5 * size_x + 3 * mlRadius;
                double y_max = 0.5 * size_y + 3 * mlRadius;

                buildMicroLensArray(cameraDescription["grid"], microLensArray, mlRadius, imageDistance, x_max, y_max, metaData);

                // build camera
                LightFieldCamera<SpectrumType>* cameraPtr = new LightFieldCamera<SpectrumType>(
                    scene,
                    eye,
                    lookAt,
                    up,
                    sensor,
                    cameraDescription["lensRadius"].asFloat(),
                    imageDistance,
                    focusDistance,
                    cameraDescription["microLensRadius"].asFloat(),
                    microLensArray);

                // check existence of "sampler" key
                std::shared_ptr<Sampler> sampler;
                if (!checkKey(cameraDescription, "sampler", PropertyType::ObjectValue, false))
                {
                    std::cout << "No ""sampler"" found. Assuming regular sampling with one sample." << std::endl;
                    sampler = std::shared_ptr<Sampler>(new Regular());
                    sampler->generateSamples();
                }
                else
                {
                    if (!buildSampler(cameraDescription["sampler"], sampler))
                    {
                        return false;
                    }
                }

                // map samples to unit disk
                sampler->mapSamplesToUnitDisk();

                // set sampler
                cameraPtr->setSampler(std::shared_ptr<Sampler>(sampler));

                // set camera
                camera.reset(cameraPtr);

                // set scene meta data
                metaData.supportsMultiThreading_ = false;
            }
            else if (cameraDescription["type"].asString() == "LightFieldCameraCalibration")
            {
                if (!checkKey(cameraDescription, "lensRadius", PropertyType::NumberValue, true) ||
                    !checkKey(cameraDescription, "microLensRadius", PropertyType::NumberValue, true))
                {
                    return false;
                }

                float imageDistance;
                float focusDistance;
                double mlRadius = cameraDescription["microLensRadius"].asDouble();

                calcOptics(cameraDescription, imageDistance, focusDistance);

                // Build microlens array (and save coordinates to csv)
                std::shared_ptr<AbstractGrid<SpectrumType> > microLensArray(new AbstractGrid<SpectrumType> );

                // check grid description
                if (!checkKey(cameraDescription, "grid", PropertyType::ObjectValue, true))
                {
                    std::cout << "No grid description found." << std::endl;
                    return false;
                }

                double s_size_x = cameraDescription["viewPlane"]["resolution"][0].asDouble();
                double s_size_y = cameraDescription["viewPlane"]["resolution"][1].asDouble();

                double size_x = s_size_x * cameraDescription["viewPlane"]["pixelSize"].asDouble();
                double size_y = s_size_y * cameraDescription["viewPlane"]["pixelSize"].asDouble();

                double x_max = 0.5 * size_x + 3 * mlRadius;
                double y_max = 0.5 * size_y + 3 * mlRadius;

                buildMicroLensArray(cameraDescription["grid"], microLensArray, mlRadius, imageDistance, x_max, y_max, metaData);

                // build camera
                LightFieldCameraCalibration<SpectrumType>* cameraPtr = new LightFieldCameraCalibration<SpectrumType>(
                    scene,
                    eye,
                    lookAt,
                    up,
                    sensor,
                    cameraDescription["lensRadius"].asFloat(),
                    imageDistance,
                    focusDistance,
                    cameraDescription["microLensRadius"].asFloat(),
                    microLensArray);

                // check existence of "sampler" key
                std::shared_ptr<Sampler> sampler;
                if (!checkKey(cameraDescription, "sampler", PropertyType::ObjectValue, false))
                {
                    std::cout << "No ""sampler"" found. Assuming regular sampling with one sample." << std::endl;
                    sampler = std::shared_ptr<Sampler>(new Regular());
                    sampler->generateSamples();
                }
                else
                {
                    if (!buildSampler(cameraDescription["sampler"], sampler))
                    {
                        return false;
                    }
                }

                // map samples to unit disk
                sampler->mapSamplesToUnitDisk();

                // set sampler
                cameraPtr->setSampler(std::shared_ptr<Sampler>(sampler));

                // set camera
                camera.reset(cameraPtr);

                // set scene meta data
                metaData.supportsMultiThreading_ = false;
            }
            else if (cameraDescription["type"].asString() == "RealLensCamera")
            {
                if (!checkKey(cameraDescription, "lensRadius", PropertyType::NumberValue, true) ||
                    !checkKey(cameraDescription, "imageDistance", PropertyType::NumberValue, true) ||
                    !checkKey(cameraDescription, "radius1", PropertyType::NumberValue, true) ||
                    !checkKey(cameraDescription, "radius2", PropertyType::NumberValue, true) ||
                    !checkKey(cameraDescription, "lensThickness", PropertyType::NumberValue, true))
                {
                    return false;
                }

                float refractionIndex;
                if (checkKey(cameraDescription, "focusDistance", PropertyType::NumberValue, false))
                {
                    if (checkKey(cameraDescription, "refractionIndex", PropertyType::NumberValue, false))
                    {
                        std::cout << "Keys ""focusDistance"" and ""refractionIndex"" defined. Ignoring ""refractionIndex""." << std::endl;
                    }

                    // calculate refraction index from desired focus distance
                    float imageDistance = cameraDescription["imageDistance"].asFloat();
                    float focusDistance = cameraDescription["focusDistance"].asFloat();
                    float radius1 = cameraDescription["radius1"].asFloat();
                    float radius2 = cameraDescription["radius2"].asFloat();
                    float lensThickness = cameraDescription["lensThickness"].asFloat();

                    // calculation for thick lens without paraxial approximation
                    double a = (1.0 / radius1) + (1.0 / radius2) + (lensThickness / radius1 / radius2);
                    double b = -1.0 * ((1.0 / radius1) + (1.0 / radius2) + (2.0 * lensThickness / radius1 / radius2) + (1.0 / imageDistance) + (1.0 / focusDistance));
                    double c = lensThickness / radius1 / radius2;
                    refractionIndex = static_cast<float>((-1.0 * b + sqrt(b * b - 4.0 * a * c) ) / 2.0 / a);

                    //// calculation for thin lens without paraxial approximation
                    //refractionIndex = static_cast<float>(((1.0 / imageDistance) + (1.0 / focusDistance)) / ((1.0 / radius1) + (1.0 / radius2)) + 1.0);

                    //std::cout << "n: " << refractionIndex << std::endl;
                }
                else if (checkKey(cameraDescription, "refractionIndex", PropertyType::NumberValue, false))
                {
                    refractionIndex = cameraDescription["refractionIndex"].asFloat();
                }
                else
                {
                    std::cout << "Either one of the keys ""focusDistance"" and ""refractionIndex"" have to be specified." << std::endl;
                    return false;
                }
            
                // build camera
                RealLensCamera<SpectrumType>* cameraPtr = new RealLensCamera<SpectrumType>(
                    scene,
                    eye,
                    lookAt,
                    up,
                    sensor,
                    cameraDescription["lensRadius"].asFloat(),
                    cameraDescription["imageDistance"].asFloat(),
                    cameraDescription["radius1"].asFloat(),
                    cameraDescription["radius2"].asFloat(),
                    cameraDescription["lensThickness"].asFloat(),
                    refractionIndex);
            
                // check existence of "sampler" key
                std::shared_ptr<Sampler> sampler;
                if (!checkKey(cameraDescription, "sampler", PropertyType::ObjectValue, false))
                {
                    std::cout << "No ""sampler"" found. Assuming regular sampling with one sample." << std::endl;
                    sampler = std::shared_ptr<Sampler>(new Regular());
                    sampler->generateSamples();
                }
                else
                {
                    if (!buildSampler(cameraDescription["sampler"], sampler))
                    {
                        return false;
                    }
                }
            
                // map samples to unit disk
                sampler->mapSamplesToUnitDisk();
            
                // set sampler
                cameraPtr->setSampler(std::shared_ptr<Sampler>(sampler));
            
                // set camera
                camera.reset(cameraPtr);
            }
            else if (cameraDescription["type"].asString() == "ChromaticRealLensCamera")
            {
                if (!checkKey(cameraDescription, "lensRadius", PropertyType::NumberValue, true) ||
                    !checkKey(cameraDescription, "radius1", PropertyType::NumberValue, true) ||
                    !checkKey(cameraDescription, "radius2", PropertyType::NumberValue, true) ||
                    !checkKey(cameraDescription, "lensThickness", PropertyType::NumberValue, true))
                {
                    return false;
                }

                // Sellmeier coefficients can either be entered in form "sellmeierCoefficients" : [B1,B2,B3,C1,C2,C3] with B1 to C3 as doubles
                // or with identifier of known glasses e.g. "sellmeierCoefficients" : "BK7"
                std::shared_ptr<SellmeierCoefficients> usedGlass;

                if(checkKey(cameraDescription, "sellmeierCoefficients", PropertyType::VectorValue, false) &&
                   checkDimension(cameraDescription["sellmeierCoefficients"], 6, false))
                {
                    usedGlass.reset(new SellmeierCoefficients);
                    usedGlass->B1 = cameraDescription["sellmeierCoefficients"][0].asDouble();
                    usedGlass->B2 = cameraDescription["sellmeierCoefficients"][1].asDouble();
                    usedGlass->B3 = cameraDescription["sellmeierCoefficients"][2].asDouble();
                    usedGlass->C1 = cameraDescription["sellmeierCoefficients"][3].asDouble();
                    usedGlass->C2 = cameraDescription["sellmeierCoefficients"][4].asDouble();
                    usedGlass->C3 = cameraDescription["sellmeierCoefficients"][5].asDouble();
                }
                else if (checkKey(cameraDescription, "sellmeierCoefficients", PropertyType::StringValue, false))
                {
                    if (cameraDescription["sellmeierCoefficients"].asString() == "BK7")
                    {
                        // BK7 taken from https://de.wikipedia.org/wiki/Sellmeier-Gleichung (22.06.16)
                        usedGlass.reset(new SellmeierCoefficients);
                        usedGlass->B1 = 1.03961212;
                        usedGlass->B2 = 0.231792344;
                        usedGlass->B3 = 1.01046945;
                        usedGlass->C1 = 6.00069867e-3;
                        usedGlass->C2 = 2.00179144e-2;
                        usedGlass->C3 = 103.560653;
                    }
                    else if (cameraDescription["sellmeierCoefficients"].asString() == "Quartz")
                    {
                        // Specifications taken from http://www.diss.fu-berlin.de/diss/servlets/MCRFileNodeServlet/FUDISS_derivate_000000000247/08_kap8.pdf
                        usedGlass.reset(new SellmeierCoefficients);
                        usedGlass->B1 = 0.6961663;
                        usedGlass->B2 = 0.4079426;
                        usedGlass->B3 = 0.8974794;
                        usedGlass->C1 = 4.6791e-3;
                        usedGlass->C2 = 1.35121e-2;
                        usedGlass->C3 = 97.934003;
                    }
                    else if (cameraDescription["sellmeierCoefficients"].asString() == "Sapphire")
                    {
                        // Specifications taken from http://www.diss.fu-berlin.de/diss/servlets/MCRFileNodeServlet/FUDISS_derivate_000000000247/08_kap8.pdf
                        usedGlass.reset(new SellmeierCoefficients);
                        usedGlass->B1 = 1.0237980;
                        usedGlass->B2 = 1.0582640;
                        usedGlass->B3 = 5.2807920;
                        usedGlass->C1 = 3.7759e-3;
                        usedGlass->C2 = 1.22544e-2;
                        usedGlass->C3 = 321.36160;
                    }
                    else if (cameraDescription["sellmeierCoefficients"].asString() == "SF11")
                    {
                        // Specifications taken from http://www.diss.fu-berlin.de/diss/servlets/MCRFileNodeServlet/FUDISS_derivate_000000000247/08_kap8.pdf
                        usedGlass.reset(new SellmeierCoefficients);
                        usedGlass->B1 = 1.73848403;
                        usedGlass->B2 = 0.31116897;
                        usedGlass->B3 = 1.17490871;
                        usedGlass->C1 = 1.360686e-2;
                        usedGlass->C2 = 6.159605e-2;
                        usedGlass->C3 = 121.922711;
                    }
                    else
                    {
                        std::cout << "Unknown type of optical material: " << cameraDescription["sellmeierCoefficients"].asString() << std::endl;
                        return false;
                    }
                }
                else
                {
                    std::cout << """sellmeierCoefficients"" key invalid. It has to be specified correctly." << std::endl;
                    return false;
                }

                float imageDistance = 0.f;
                float radius1 = cameraDescription["radius1"].asFloat();
                float radius2 = cameraDescription["radius2"].asFloat();
                float lensThickness = cameraDescription["lensThickness"].asFloat();

                // Either read in focusDistance or imageDistance
                if (checkKey(cameraDescription, "focusDistance", PropertyType::NumberValue, false))
                {
                    float focusDistance = cameraDescription["focusDistance"].asFloat();

                    // calculate mean wavelength
                    float meanWaveLength; // needed to calculate mean refraction index
                    SpectrumType temp;
                    if (temp.getNumOfCoeffs() % 2 == 0)
                    {
                        meanWaveLength = static_cast<float>((temp.getWaveLengthNm(temp.getNumOfCoeffs() / 2 - 1) +
                            temp.getWaveLengthNm(temp.getNumOfCoeffs() / 2))) / 2.f;
                    }
                    else
                    {
                        meanWaveLength = static_cast<float>(temp.getWaveLengthNm(temp.getNumOfCoeffs() / 2)); // = floor(numCoeffs / 2)
                    }

                    // calculate refraction index
                    //double lambda = static_cast<double>(waveLength) * 1e-3;
                    double meanWaveLengthSqr = static_cast<double>(meanWaveLength * meanWaveLength); // in micro meter^2
                    double refractionSqr = 1.0 + (usedGlass->B1 * meanWaveLengthSqr / (meanWaveLengthSqr - usedGlass->C1))
                        + (usedGlass->B2 * meanWaveLengthSqr / (meanWaveLengthSqr - usedGlass->C2))
                        + (usedGlass->B3 * meanWaveLengthSqr / (meanWaveLengthSqr - usedGlass->C3));
                    float meanRefrectionIndex = static_cast<float>(sqrt(refractionSqr));

                    // calculate image distance
                    imageDistance = 1.f / (((meanRefrectionIndex - 1.f) * (1.f / radius1 + 1.f / radius2))
                        + (lensThickness * (meanRefrectionIndex - 1.f) * (meanRefrectionIndex - 1.f) / (radius1 * radius2 * meanRefrectionIndex)
                        - (1.f / focusDistance)));
                }
                else if (checkKey(cameraDescription, "imageDistance", PropertyType::NumberValue, false))
                {
                    imageDistance = cameraDescription["imageDistance"].asFloat();
                }
                else
                {
                    std::cout << "Either one of the keys ""imageDistance"" or ""focusDistance"" has to be specified." << std::endl;
                    return false;
                }

                // build camera
                ChromaticRealLensCamera<SpectrumType>* cameraPtr = new ChromaticRealLensCamera<SpectrumType>(
                    scene,
                    eye,
                    lookAt,
                    up,
                    sensor,
                    cameraDescription["lensRadius"].asFloat(),
                    imageDistance,
                    radius1,
                    radius2,
                    lensThickness,
                    usedGlass);

                // check existence of "sampler" key
                std::shared_ptr<Sampler> sampler;
                if (!checkKey(cameraDescription, "sampler", PropertyType::ObjectValue, false))
                {
                    std::cout << "No ""sampler"" found. Assuming regular sampling with one sample." << std::endl;
                    sampler = std::shared_ptr<Sampler>(new Regular());
                    sampler->generateSamples();
                }
                else
                {
                    if (!buildSampler(cameraDescription["sampler"], sampler))
                    {
                        return false;
                    }
                }

                // map samples to unit disk
                sampler->mapSamplesToUnitDisk();

                // set sampler
                cameraPtr->setSampler(std::shared_ptr<Sampler>(sampler));

                // set camera
                camera.reset(cameraPtr);
            }
            else
            {
                // "type" unknown
                std::cout << "Unknown type of camera object: " << cameraDescription["type"].asString() << std::endl;
                return false;
            }
        }
        catch (std::runtime_error& e)
        {
            std::cout << e.what() << std::endl;
            return false;
        }
        
        scene->setCamera(camera);
        
        if (!checkKey(cameraDescription, "viewPlane", PropertyType::ObjectValue, true) ||
            !((cameraDescription["type"].asString() == "LightFieldReference") || checkKey(cameraDescription["viewPlane"], "resolution", PropertyType::VectorValue, true)) ||
            !((cameraDescription["type"].asString() == "LightFieldReference") || checkKey(cameraDescription["viewPlane"], "pixelSize", PropertyType::NumberValue, true) ) ||
            !((cameraDescription["type"].asString() == "LightFieldReference") || checkDimension(cameraDescription["viewPlane"]["resolution"], 2, true)))
        {
            return false;
        }
        // If the camera is the LightFieldReference Camera , the full viewplane will be calculated
        // from the cameraDiscriptions
        if (cameraDescription["type"].asString() != "LightFieldReference"){
            scene->viewPlane_.uRes_ = cameraDescription["viewPlane"]["resolution"][0].asInt();
            scene->viewPlane_.vRes_ = cameraDescription["viewPlane"]["resolution"][1].asInt();
            scene->viewPlane_.uAbsRes_ = scene->viewPlane_.uRes_;
            scene->viewPlane_.vAbsRes_ = scene->viewPlane_.vRes_;
            scene->viewPlane_.s_ = cameraDescription["viewPlane"]["pixelSize"].asDouble();
        }
        
        // check existence of "sampler" key
        if (!checkKey(cameraDescription["viewPlane"], "sampler", PropertyType::ObjectValue, false))
        {
            std::cout << "No ""sampler"" found. Assuming regular sampling with one sample." << std::endl;
            scene->viewPlane_.setSamples(1);
            
        }
        else
        {
            std::shared_ptr<Sampler> sampler;
            if (!buildSampler(cameraDescription["viewPlane"]["sampler"], sampler))
            {
                return false;
            }
            scene->viewPlane_.setSampler(sampler);
        }

        // check existence of "renderOnly" key
        if (!checkKey(cameraDescription["viewPlane"], "renderOnly", PropertyType::VectorValue, false))
        {
            // set scene meta data
            metaData.uMin_ = 0;
            metaData.uMax_ = scene->viewPlane_.uRes_ - 1;
            metaData.vMin_ = 0;
            metaData.vMax_ = scene->viewPlane_.vRes_ - 1;
        }
        else
        {
            if (!checkDimension(cameraDescription["viewPlane"]["renderOnly"], 4, true))
            {
                return false;
            }

            int uMin = cameraDescription["viewPlane"]["renderOnly"][0].asInt();
            int uMax = cameraDescription["viewPlane"]["renderOnly"][1].asInt();
            int vMin = cameraDescription["viewPlane"]["renderOnly"][2].asInt();
            int vMax = cameraDescription["viewPlane"]["renderOnly"][3].asInt();

            if (uMin > uMax || uMax < 0 || uMin > scene->viewPlane_.uRes_ - 1 || vMin > vMax || vMax < 0 || vMin > scene->viewPlane_.vRes_ - 1)
            {
                std::cout << "Nothing to render. Check your ""renderOnly"" definition." << std::endl;
                return false;
            }

            // set scene meta data
            metaData.uMin_ = std::max<int>(0, uMin);
            metaData.uMax_ = std::min<int>(scene->viewPlane_.uRes_ - 1, uMax);
            metaData.vMin_ = std::max<int>(0, vMin);
            metaData.vMax_ = std::min<int>(scene->viewPlane_.vRes_ - 1, vMax);
        }

        // check existence of imageNoise key
        if (checkKey(cameraDescription, "imageNoise", PropertyType::ObjectValue, false))
        {
            if (!checkKey(cameraDescription["imageNoise"], "readNoise", PropertyType::NumberValue, true) ||
                !checkKey(cameraDescription["imageNoise"], "darkCurrent", PropertyType::NumberValue, true) ||
                !checkKey(cameraDescription["imageNoise"], "exposure", PropertyType::NumberValue, true) ||
                !checkKey(cameraDescription["imageNoise"], "gain", PropertyType::NumberValue, false))
            {
                return false;
            }

            camera->setImageNoise(ImageNoise<SpectrumType>(
                cameraDescription["imageNoise"]["readNoise"].asFloat(),
                cameraDescription["imageNoise"]["darkCurrent"].asFloat(),
                cameraDescription["imageNoise"]["exposure"].asFloat(),
                cameraDescription["imageNoise"]["gain"].asFloat()));
        }

        // check existence of apertures key
        if (checkKey(cameraDescription, "apertures", PropertyType::ArrayValue, false))
        {
            for (unsigned int i = 0; i < cameraDescription["apertures"].size(); ++i)
            {
                std::shared_ptr<Aperture> aperture;

                // build aperture
                if (!buildAperture(cameraDescription["apertures"][i], aperture))
                {
                    return false;
                }

                // add aperture
                camera->addAperture(aperture);
            }
        }

        // light field camera samples micro lenses, not pixels -> map samples to unit disk
        if (camera->getType() == CameraType::Type::LightFieldCamera || camera->getType() == CameraType::Type::LightFieldCameraCalibration)
        {
            scene->viewPlane_.sampler_->mapSamplesToUnitDisk();
        }

        // parsing successful
        return true;
    }



    /*! \brief Extracts sensor properties an builds sensor object.
     *  \param[in] cameraDescription <a HREF="http://www.json.org">JSON</a> value of camera description.
     *  \param[out] sensor Sensor object.
     *  \return Returns `true` if building was successful, else ´false`.
     */
    template<class SpectrumType>
    bool SceneParser::buildSensor(const Json::Value& cameraDescription, Sensor<SpectrumType>& sensor)
    {
        //SampledSpectrum sensorResponsivity;
        std::vector<std::shared_ptr<const SampledSpectrum> > responsivities;
        std::string sensorName;
        if (!cameraDescription.isMember("sensorType"))
        {
            // if sensorType is not explicitly defined, set default to Ideal
            sensorName = "Ideal";
            responsivities.push_back(std::shared_ptr<const SampledSpectrum>(new SampledSpectrum(
                SampledSpectrum::fromSampled(iiit::idealResponsivityLambdaSamples, iiit::idealResponsivityValueSamples))));
        }
        else if (cameraDescription["sensorType"].isObject()) // Sensor is parsed as pair(s) of arrays specifying the responsivity of the sensor
        {
            // sampled data
            if (checkKey(cameraDescription["sensorType"], "lambda", PropertyType::VectorValue, false) &&
                checkKey(cameraDescription["sensorType"], "value", PropertyType::VectorValue, false) &&
                checkDimension(cameraDescription["sensorType"]["value"], cameraDescription["sensorType"]["lambda"].size(), true))
            {
                // Construct sensor from specified data
                std::vector<float> lambda;
                lambda.reserve(cameraDescription["sensorType"]["lambda"].size());
                std::vector<float> value;
                value.reserve(cameraDescription["sensorType"]["lambda"].size());

                for (unsigned int i = 0; i < cameraDescription["sensorType"]["lambda"].size(); ++i)
                {
                    lambda.push_back(cameraDescription["sensorType"]["lambda"][i].asFloat());
                    value.push_back(cameraDescription["sensorType"]["value"][i].asFloat());
                }

                if (checkKey(cameraDescription["sensorType"], "name", PropertyType::StringValue, false))
                {
                    sensorName = cameraDescription["sensorType"]["name"].asString();
                }
                else
                {
                    sensorName = "Own (User specified)";
                }
                responsivities.push_back(std::shared_ptr<const SampledSpectrum>(new SampledSpectrum(SampledSpectrum::fromSampled(lambda, value))));
            }
            else if (checkKey(cameraDescription["sensorType"], "red", PropertyType::ObjectValue, true) &&
                checkKey(cameraDescription["sensorType"]["red"], "lambda", PropertyType::VectorValue, true) &&
                checkKey(cameraDescription["sensorType"]["red"], "value", PropertyType::VectorValue, true) &&
                checkDimension(cameraDescription["sensorType"]["red"]["value"], cameraDescription["sensorType"]["red"]["lambda"].size(), true) &&
                checkKey(cameraDescription["sensorType"], "green", PropertyType::ObjectValue, true) &&
                checkKey(cameraDescription["sensorType"]["green"], "lambda", PropertyType::VectorValue, true) &&
                checkKey(cameraDescription["sensorType"]["green"], "value", PropertyType::VectorValue, true) &&
                checkDimension(cameraDescription["sensorType"]["green"]["value"], cameraDescription["sensorType"]["green"]["lambda"].size(), true) &&
                checkKey(cameraDescription["sensorType"], "blue", PropertyType::ObjectValue, true) &&
                checkKey(cameraDescription["sensorType"]["blue"], "lambda", PropertyType::VectorValue, true) &&
                checkKey(cameraDescription["sensorType"]["blue"], "value", PropertyType::VectorValue, true) &&
                checkDimension(cameraDescription["sensorType"]["blue"]["value"], cameraDescription["sensorType"]["blue"]["lambda"].size(), true))
            {
                std::vector<std::string> channels = { {"red", "green", "blue"} };
                for (std::vector<std::string>::const_iterator it = channels.begin(); it != channels.end(); ++it)
                {
                    // Construct sensor from specified data
                    std::vector<float> lambda;
                    lambda.reserve(cameraDescription["sensorType"][*it]["lambda"].size());
                    std::vector<float> value;
                    value.reserve(cameraDescription["sensorType"][*it]["lambda"].size());

                    for (unsigned int i = 0; i < cameraDescription["sensorType"][*it]["lambda"].size(); ++i)
                    {
                        lambda.push_back(cameraDescription["sensorType"][*it]["lambda"][i].asFloat());
                        value.push_back(cameraDescription["sensorType"][*it]["value"][i].asFloat());
                    }
                    responsivities.push_back(std::shared_ptr<const SampledSpectrum>(new SampledSpectrum(SampledSpectrum::fromSampled(lambda, value))));
                }

                if (checkKey(cameraDescription["sensorType"], "name", PropertyType::StringValue, false))
                {
                    sensorName = cameraDescription["sensorType"]["name"].asString();
                }
                else
                {
                    sensorName = "Own (User specified)";
                }
            }
            else
            {
                std::cout << "Specified sensor responsivity data is inconsistent, using default." << std::endl;
                sensorName = "Ideal";
                responsivities.push_back(std::shared_ptr<const SampledSpectrum>(new SampledSpectrum(
                    SampledSpectrum::fromSampled(iiit::idealResponsivityLambdaSamples, iiit::idealResponsivityValueSamples))));
            }

        }
        else if (cameraDescription["sensorType"].isString()) // Sensor is parsed as a string value
        {
            // check if specified sensorType coincides with allowed ones and set sensorType
            if (cameraDescription["sensorType"].asString() == "Ideal")
            {
                sensorName = "Ideal";
                responsivities.push_back(std::shared_ptr<const SampledSpectrum>(new SampledSpectrum(
                    SampledSpectrum::fromSampled(iiit::idealResponsivityLambdaSamples, iiit::idealResponsivityValueSamples))));
            }
            // Use SONY ICX618 as default CCD sensor
            else if (cameraDescription["sensorType"].asString() == "CCD" ||
                cameraDescription["sensorType"].asString() == "ICX618" ||
                cameraDescription["sensorType"].asString() == "Sony ICX618")
            {
                sensorName = "CCD SONY ICX618";
                responsivities.push_back(std::shared_ptr<const SampledSpectrum>(new SampledSpectrum(
                    SampledSpectrum::fromSampled(iiit::ccdSonyICX618ResponsivityLambdaSamples, iiit::ccdSonyICX618ResponsivityValueSamples))));

            }
            else if (cameraDescription["sensorType"].asString() == "ICX814" ||
                cameraDescription["sensorType"].asString() == "Sony ICX814")
            {
                sensorName = "CCD SONY ICX814";
                responsivities.push_back(std::shared_ptr<const SampledSpectrum>(new SampledSpectrum(
                    SampledSpectrum::fromSampled(iiit::ccdSonyICX814ResponsivityLambdaSamples, iiit::ccdSonyICX814ResponsivityValueSamples))));
            }
            else if (cameraDescription["sensorType"].asString() == "KAI4050" ||
                cameraDescription["sensorType"].asString() == "ON KAI4050")
            {
                sensorName = "CCD ON-Semiconductor KAI 4050";
                responsivities.push_back(std::shared_ptr<const SampledSpectrum>(new SampledSpectrum(
                    SampledSpectrum::fromSampled(iiit::ccdOnKAI4050ResponsivityLambdaSamples, iiit::ccdOnKAI4050ResponsivityValueSamples))));
            }
            else if (cameraDescription["sensorType"].asString() == "KAI16070" ||
                cameraDescription["sensorType"].asString() == "ON KAI16070" ||
                cameraDescription["sensorType"].asString() == "KAI16070M" ||
                cameraDescription["sensorType"].asString() == "ON KAI16070 Mono")
            {
                sensorName = "CCD ON-Semiconductor KAI 16070";
                responsivities.push_back(std::shared_ptr<const SampledSpectrum>(new SampledSpectrum(
                    SampledSpectrum::fromSampled(iiit::ccdOnKAI16070ResponsivityLambdaSamples, iiit::ccdOnKAI16070ResponsivityValueSamples))));
            }
            else if (cameraDescription["sensorType"].asString() == "KAI16070C" ||
                cameraDescription["sensorType"].asString() == "ON KAI16070 Color")
            {
                sensorName = "CCD ON-Semiconductor KAI 16070 Color";
                responsivities.push_back(std::shared_ptr<const SampledSpectrum>(new SampledSpectrum(
                    SampledSpectrum::fromSampled(iiit::ccdOnKAI16070RedResponsivityLambdaSamples, iiit::ccdOnKAI16070RedResponsivityValueSamples))));
                responsivities.push_back(std::shared_ptr<const SampledSpectrum>(new SampledSpectrum(
                    SampledSpectrum::fromSampled(iiit::ccdOnKAI16070GreenResponsivityLambdaSamples, iiit::ccdOnKAI16070GreenResponsivityValueSamples))));
                responsivities.push_back(std::shared_ptr<const SampledSpectrum>(new SampledSpectrum(
                    SampledSpectrum::fromSampled(iiit::ccdOnKAI16070BlueResponsivityLambdaSamples, iiit::ccdOnKAI16070BlueResponsivityValueSamples))));
            }
            else if (cameraDescription["sensorType"].asString() == "CMOS" ||
                cameraDescription["sensorType"].asString() == "IMX174" ||
                cameraDescription["sensorType"].asString() == "Sony IMX174")
            {
                sensorName = "CMOS SONY IMX174";
                responsivities.push_back(std::shared_ptr<const SampledSpectrum>(new SampledSpectrum(
                    SampledSpectrum::fromSampled(iiit::cmosSonyIMX174ResponsivityLambdaSamples, iiit::cmosSonyIMX174ResponsivityValueSamples))));
            }
            else
            {
                std::cout << "Specified camera sensorType \'"
                    << cameraDescription["sensorType"].asString()
                    << "\' is invalid. Setting sensorType to \'Ideal\'."
                    << std::endl;
                sensorName = "Ideal";
                responsivities.push_back(std::shared_ptr<const SampledSpectrum>(new SampledSpectrum(
                    SampledSpectrum::fromSampled(iiit::idealResponsivityLambdaSamples, iiit::idealResponsivityValueSamples))));
            }
        }

        // set imageType data
        ImageType::Type imageType;
        // if imageType is not explicitly defined, set default to IdealRgb
        if (!checkKey(cameraDescription, "imageType", PropertyType::StringValue, false))
        {
            imageType = ImageType::Type::IdealRgb;
        }
        // check if specified imageType coincides with allowed ones and set imageType
        else if (cameraDescription["imageType"].asString() == "Raw")
        {
            imageType = ImageType::Type::Raw;
        }
        else if (cameraDescription["imageType"].asString() == "Rgb" ||
            cameraDescription["imageType"].asString() == "RGB")
        {
            imageType = ImageType::Type::Rgb;
        }
        else if (cameraDescription["imageType"].asString() == "IdealRgb" ||
            cameraDescription["imageType"].asString() == "IdealRGB")
        {
            imageType = ImageType::Type::IdealRgb;
        }
        else if (cameraDescription["imageType"].asString() == "Multispectral" ||
            cameraDescription["imageType"].asString() == "MultiSpectral")
        {
            imageType = ImageType::Type::MultiSpectral;
        }
        else
        {
            std::cout << "Specified camera imageType \'"
                << cameraDescription["imageType"].asString()
                << "\' is invalid. Setting imageType to \'IdealRgb\'."
                << std::endl;
            imageType = ImageType::Type::IdealRgb;
        }

        // build sensor
        Sensor<SpectrumType> tmpSensor(imageType, responsivities, sensorName);
        sensor = tmpSensor;

        // successfully extracted data.
        return true;
    }  

    /*! \brief Factory method for objects material.
     *  \param[in] materialDescription <a HREF="http://www.json.org">JSON</a> value of material
     *  description.
     *  \param[out] material Pointer to built material object.
     *  \return Returns ´true´ if building was successful, else ´false´.
     *
     *  Builds and initializes objects derived from Material specified by a
     *  <a HREF="http://www.json.org">JSON</a> description. The description must specify a valid type
     *  and the materials's properties, according to the type.
     */
    template <class SpectrumType>
    bool SceneParser::buildMaterial(const Json::Value& materialDescription, std::shared_ptr<Material<SpectrumType> >& material)
    {
        // ensure required properties are defined and of correct type
        if (!checkKey(materialDescription, "type", PropertyType::StringValue, true))
        {
            return false;
        }
        
        if (materialDescription["type"].asString() == "Matte")
        {
            // build material
            std::shared_ptr<Matte<SpectrumType> > matte(new Matte<SpectrumType>());
            
            // optional properties
            if (checkKey(materialDescription, "ka", PropertyType::NumberValue, false))
            {
                matte->setAmbientReflectionFactor(materialDescription["ka"].asFloat());
            }
            
            if (checkKey(materialDescription, "kd", PropertyType::NumberValue, false))
            {
                matte->setDiffuseReflectionFactor(materialDescription["kd"].asFloat());
            }
            
            if (checkKey(materialDescription, "cd", PropertyType::ColorValue, false))
            {
                SpectrumType color;
                if (buildColor(materialDescription["cd"], color, materialType_))
                {
                    matte->setDiffuseColor(color);
                }
            }
            
            // texture overwrites cd
            if (checkKey(materialDescription, "texture", PropertyType::ObjectValue, false))
            {
                // build texture
                std::shared_ptr<Texture<SpectrumType> > texture;
                if (!buildTexture<SpectrumType>(materialDescription["texture"], texture))
                {
                    return false;
                }
                matte->setDiffuseTexture(texture);
            }
            
            // check existence of "sampler" key
            if (checkKey(materialDescription, "sampler", PropertyType::ObjectValue, false))
            {
                std::shared_ptr<Sampler> sampler;
                if (!buildSampler(materialDescription["sampler"], sampler))
                {
                    return false;
                }
                float exp = 1.f;
                if (checkKey(materialDescription["sampler"], "exp", PropertyType::RealValue, false))
                {
                    exp = materialDescription["sampler"]["exp"].asFloat();
                }
                sampler->mapSamplesToHemisphere(exp);
                matte->setSampler(sampler);
            }
 
            // return material
            material = matte;
        }
        else if (materialDescription["type"].asString() == "Phong")
        {
            // build material
            std::shared_ptr<Phong<SpectrumType>> phong(new Phong<SpectrumType>());
            
            // optional properties
            if (checkKey(materialDescription, "ka", PropertyType::NumberValue, false))
            {
                phong->setAmbientReflectionFactor(materialDescription["ka"].asFloat());
            }
            
            if (checkKey(materialDescription, "kd", PropertyType::NumberValue, false))
            {
                phong->setDiffuseReflectionFactor(materialDescription["kd"].asFloat());
            }
            
            if (checkKey(materialDescription, "cd", PropertyType::ColorValue, false))
            {
                SpectrumType color;
                if (buildColor(materialDescription["cd"], color, materialType_))
                {
                    phong->setDiffuseColor(color);
                }
            }
            
            // texture overwrites cd
            if (checkKey(materialDescription, "texture", PropertyType::ObjectValue, false))
            {
                // build texture
                std::shared_ptr<Texture<SpectrumType> > texture;
                if (!buildTexture<SpectrumType>(materialDescription["texture"], texture))
                {
                    return false;
                }
                phong->setDiffuseTexture(texture);
            }
            
            if (checkKey(materialDescription, "ks", PropertyType::NumberValue, false))
            {
                phong->setSpecularReflectionFactor(materialDescription["ks"].asFloat());
            }
            
            if (checkKey(materialDescription, "exp", PropertyType::NumberValue, false))
            {
                phong->setSpecularExponent(materialDescription["exp"].asFloat());
            }
            
            if (checkKey(materialDescription, "cs", PropertyType::ColorValue, false))
            {
                SpectrumType color;
                if (buildColor(materialDescription["cs"], color, materialType_))
                {
                    phong->setSpecularColor(color);
                }
            }
            
            // texture overwrites cs
            if (checkKey(materialDescription, "specularTexture", PropertyType::ObjectValue, false))
            {
                // build texture
                std::shared_ptr<Texture<SpectrumType> > texture;
                if (!buildTexture<SpectrumType>(materialDescription["specularTexture"], texture))
                {
                    return false;
                }
                phong->setSpecularTexture(texture);
            }
            
            // return material
            material = phong;
        }
        else if (materialDescription["type"].asString() == "Reflective")
        {
            // build material
            std::shared_ptr<Reflective<SpectrumType>> reflective(new Reflective<SpectrumType>());
            
            // optional properties
            // ambient brdf
            if (checkKey(materialDescription, "ka", PropertyType::NumberValue, false))
            {
                reflective->setAmbientReflectionFactor(materialDescription["ka"].asFloat());
            }
            
            // diffuse brdf
            if (checkKey(materialDescription, "kd", PropertyType::NumberValue, false))
            {
                reflective->setDiffuseReflectionFactor(materialDescription["kd"].asFloat());
            }
            
            if (checkKey(materialDescription, "cd", PropertyType::ColorValue, false))
            {
                SpectrumType color;
                if (buildColor(materialDescription["cd"], color, materialType_))
                {
                    reflective->setDiffuseColor(color);
                }
            }
            
            // texture overwrites cd
            if (checkKey(materialDescription, "texture", PropertyType::ObjectValue, false))
            {
                // build texture
                std::shared_ptr<Texture<SpectrumType> > texture;
                if (!buildTexture<SpectrumType>(materialDescription["texture"], texture))
                {
                    return false;
                }
                reflective->setDiffuseTexture(texture);
            }
            
            // glossy specular brdf
            if (checkKey(materialDescription, "ks", PropertyType::NumberValue, false))
            {
                reflective->setSpecularReflectionFactor(materialDescription["ks"].asFloat());
            }
            
            if (checkKey(materialDescription, "exp", PropertyType::NumberValue, false))
            {
                reflective->setSpecularExponent(materialDescription["exp"].asFloat());
            }
            
            if (checkKey(materialDescription, "cs", PropertyType::ColorValue, false))
            {
                SpectrumType color;
                if (buildColor(materialDescription["cs"], color, materialType_))
                {
                    reflective->setSpecularColor(color);
                }
            }
            
            // texture overwrites cs
            if (checkKey(materialDescription, "specularTexture", PropertyType::ObjectValue, false))
            {
                // build texture
                std::shared_ptr<Texture<SpectrumType> > texture;
                if (!buildTexture<SpectrumType>(materialDescription["specularTexture"], texture))
                {
                    return false;
                }
                reflective->setSpecularTexture(texture);
            }
            
            
            // perfect specular brdf
            if (checkKey(materialDescription, "kr", PropertyType::NumberValue, false))
            {
                reflective->setReflectiveFactor(materialDescription["kr"].asFloat());
            }
            
            if (checkKey(materialDescription, "cr", PropertyType::ColorValue, false))
            {
                SpectrumType color;
                if (buildColor(materialDescription["cr"], color, materialType_))
                {
                    reflective->setReflectiveColor(color);
                }
            }
            
            // return material
            material = reflective;
        }
        else if (materialDescription["type"].asString() == "GlossyReflective")
        {
            // build material
            std::shared_ptr<GlossyReflective<SpectrumType>> glossyReflective(new GlossyReflective<SpectrumType>());
            
            // optional properties
            // ambient brdf
            if (checkKey(materialDescription, "ka", PropertyType::NumberValue, false))
            {
                glossyReflective->setAmbientReflectionFactor(materialDescription["ka"].asFloat());
            }
            
            // diffuse brdf
            if (checkKey(materialDescription, "kd", PropertyType::NumberValue, false))
            {
                glossyReflective->setDiffuseReflectionFactor(materialDescription["kd"].asFloat());
            }
            
            if (checkKey(materialDescription, "cd", PropertyType::ColorValue, false))
            {
                SpectrumType color;
                if (buildColor(materialDescription["cd"], color, materialType_))
                {
                    glossyReflective->setDiffuseColor(color);
                }
            }
            
            // texture overwrites cd
            if (checkKey(materialDescription, "texture", PropertyType::ObjectValue, false))
            {
                // build texture
                std::shared_ptr<Texture<SpectrumType> > texture;
                if (!buildTexture<SpectrumType>(materialDescription["texture"], texture))
                {
                    return false;
                }
                glossyReflective->setDiffuseTexture(texture);
            }
            
            // glossy specular brdf
            if (checkKey(materialDescription, "ks", PropertyType::NumberValue, false))
            {
                glossyReflective->setSpecularReflectionFactor(materialDescription["ks"].asFloat());
            }
            
            if (checkKey(materialDescription, "exp", PropertyType::NumberValue, false))
            {
                glossyReflective->setSpecularExponent(materialDescription["exp"].asFloat());
            }
            
            if (checkKey(materialDescription, "cs", PropertyType::ColorValue, false))
            {
                SpectrumType color;
                if (buildColor(materialDescription["cs"], color, materialType_))
                {
                    glossyReflective->setSpecularColor(color);
                }
            }
            
            // texture overwrites cs
            if (checkKey(materialDescription, "specularTexture", PropertyType::ObjectValue, false))
            {
                // build texture
                std::shared_ptr<Texture<SpectrumType> > texture;
                if (!buildTexture<SpectrumType>(materialDescription["specularTexture"], texture))
                {
                    return false;
                }
                glossyReflective->setSpecularTexture(texture);
            }
            
            
            // glossy reflective brdf
            if (checkKey(materialDescription, "kr", PropertyType::NumberValue, false))
            {
                glossyReflective->setReflectionFactor(materialDescription["kr"].asFloat());
            }
            
            float expr;
            if (checkKey(materialDescription, "expr", PropertyType::NumberValue, false))
            {
                expr = materialDescription["expr"].asFloat();
                glossyReflective->setReflectionExponent(expr);
            }
            else
            {
                expr = 1.f;
            }
            
            if (checkKey(materialDescription, "cr", PropertyType::ColorValue, false))
            {
                SpectrumType color;
                if (buildColor(materialDescription["cr"], color, materialType_))
                {
                    glossyReflective->setReflectionColor(color);
                }
            }
            
            std::shared_ptr<Sampler> sampler;
            if (!checkKey(materialDescription, "sampler", PropertyType::ObjectValue, false))
            {
                std::cout << "No ""sampler"" found. Assuming regular sampling with one sample." << std::endl;
                sampler = std::shared_ptr<Sampler>(new Regular());
                sampler->generateSamples();
            }
            else
            {
                if (!buildSampler(materialDescription["sampler"], sampler))
                {
                    return false;
                }
            }
            sampler->mapSamplesToHemisphere(expr);
            glossyReflective->setSampler(sampler);
            
            // return material
            material = glossyReflective;
        }
        else if (materialDescription["type"].asString() == "Transparent")
        {
            // build material
            std::shared_ptr<Transparent<SpectrumType>> transparent(new Transparent<SpectrumType>());
            
            // optional properties
            // ambient brdf
            if (checkKey(materialDescription, "ka", PropertyType::NumberValue, false))
            {
                transparent->setAmbientReflectionFactor(materialDescription["ka"].asFloat());
            }
            
            // diffuse brdf
            if (checkKey(materialDescription, "kd", PropertyType::NumberValue, false))
            {
                transparent->setDiffuseReflectionFactor(materialDescription["kd"].asFloat());
            }
            
            if (checkKey(materialDescription, "cd", PropertyType::ColorValue, false))
            {
                SpectrumType color;
                if (buildColor(materialDescription["cd"], color, materialType_))
                {
                    transparent->setDiffuseColor(color);
                }
            }
            
            // texture overwrites cd
            if (checkKey(materialDescription, "texture", PropertyType::ObjectValue, false))
            {
                // build texture
                std::shared_ptr<Texture<SpectrumType> > texture;
                if (!buildTexture<SpectrumType>(materialDescription["texture"], texture))
                {
                    return false;
                }
                transparent->setDiffuseTexture(texture);
            }
            
            // glossy specular brdf
            if (checkKey(materialDescription, "ks", PropertyType::NumberValue, false))
            {
                transparent->setSpecularReflectionFactor(materialDescription["ks"].asFloat());
            }
            
            if (checkKey(materialDescription, "exp", PropertyType::NumberValue, false))
            {
                transparent->setSpecularExponent(materialDescription["exp"].asFloat());
            }
            
            if (checkKey(materialDescription, "cs", PropertyType::ColorValue, false))
            {
                SpectrumType color;
                if (buildColor(materialDescription["cs"], color, materialType_))
                {
                    transparent->setSpecularColor(color);
                }
            }
            
            // texture overwrites cs
            if (checkKey(materialDescription, "specularTexture", PropertyType::ObjectValue, false))
            {
                // build texture
                std::shared_ptr<Texture<SpectrumType> > texture;
                if (!buildTexture<SpectrumType>(materialDescription["specularTexture"], texture))
                {
                    return false;
                }
                transparent->setSpecularTexture(texture);
            }
            
            
            // perfect specular brdf
            if (checkKey(materialDescription, "kr", PropertyType::NumberValue, false))
            {
                transparent->setReflectiveFactor(materialDescription["kr"].asFloat());
            }
            
            if (checkKey(materialDescription, "cr", PropertyType::ColorValue, false))
            {
                SpectrumType color;
                if (buildColor(materialDescription["cr"], color, materialType_))
                {
                    transparent->setReflectiveColor(color);
                }
            }
            
            // perfect transparent btdf
            if (checkKey(materialDescription, "kt", PropertyType::NumberValue, false))
            {
                transparent->setTransmittanceFactor(materialDescription["kt"].asFloat());
            }
            
            if (checkKey(materialDescription, "eta", PropertyType::NumberValue, false))
            {
                transparent->setIndexOfRefraction(materialDescription["eta"].asFloat());
            }
            
            // return material
            material = transparent;
        }
        else if (materialDescription["type"].asString() == "Dielectric")
        {
            // build material
            std::shared_ptr<Dielectric<SpectrumType> > dielectric(new Dielectric<SpectrumType>());
            
            // optional properties
            // ambient brdf
            if (checkKey(materialDescription, "ka", PropertyType::NumberValue, false))
            {
                dielectric->setAmbientReflectionFactor(materialDescription["ka"].asFloat());
            }
            
            // diffuse brdf
            if (checkKey(materialDescription, "kd", PropertyType::NumberValue, false))
            {
                dielectric->setDiffuseReflectionFactor(materialDescription["kd"].asFloat());
            }
            
            if (checkKey(materialDescription, "cd", PropertyType::ColorValue, false))
            {
                SpectrumType color;
                if (buildColor(materialDescription["cd"], color, materialType_))
                {
                    dielectric->setDiffuseColor(color);
                }
            }
            
            // texture overwrites cd
            if (checkKey(materialDescription, "texture", PropertyType::ObjectValue, false))
            {
                // build texture
                std::shared_ptr<Texture<SpectrumType> > texture;
                if (!buildTexture<SpectrumType>(materialDescription["texture"], texture))
                {
                    return false;
                }
                dielectric->setDiffuseTexture(texture);
            }
            
            // glossy specular brdf
            if (checkKey(materialDescription, "ks", PropertyType::NumberValue, false))
            {
                dielectric->setSpecularReflectionFactor(materialDescription["ks"].asFloat());
            }
            
            if (checkKey(materialDescription, "exp", PropertyType::NumberValue, false))
            {
                dielectric->setSpecularExponent(materialDescription["exp"].asFloat());
            }
            
            if (checkKey(materialDescription, "cs", PropertyType::ColorValue, false))
            {
                SpectrumType color;
                if (buildColor(materialDescription["cs"], color, materialType_))
                {
                    dielectric->setSpecularColor(color);
                }
            }
            
            // texture overwrites cs
            if (checkKey(materialDescription, "specularTexture", PropertyType::ObjectValue, false))
            {
                // build texture
                std::shared_ptr<Texture<SpectrumType> > texture;
                if (!buildTexture<SpectrumType>(materialDescription["specularTexture"], texture))
                {
                    return false;
                }
                dielectric->setSpecularTexture(texture);
            }
            
            
            // fresnel brdf & fresnel btdf
            if (checkKey(materialDescription, "etaIn", PropertyType::NumberValue, false))
            {
                dielectric->setEtaIn(materialDescription["etaIn"].asFloat());
            }
            
            if (checkKey(materialDescription, "etaOut", PropertyType::NumberValue, false))
            {
                dielectric->setEtaOut(materialDescription["etaOut"].asFloat());
            }

            
            // colot filtering
            if (checkKey(materialDescription, "cfIn", PropertyType::ColorValue, false))
            {
                SpectrumType color;
                if (buildColor(materialDescription["cfIn"], color, materialType_))
                {
                    dielectric->setCfIn(color);
                }
            }
            
            if (checkKey(materialDescription, "cfOut", PropertyType::ColorValue, false))
            {
                SpectrumType color;
                if (buildColor(materialDescription["cfOut"], color, materialType_))
                {
                    dielectric->setCfOut(color);
                }
            }
            
            // return material
            material = dielectric;
        }
        else if (materialDescription["type"].asString() == "Emissive")
        {
            // build material
            std::shared_ptr<Emissive<SpectrumType> > emissive(new Emissive<SpectrumType>());
            
            // optional properties
            if (checkKey(materialDescription, "ls", PropertyType::NumberValue, false))
            {
                emissive->setRadianceScaling(materialDescription["ls"].asFloat());
            }
            
            if (checkKey(materialDescription, "color", PropertyType::ColorValue, false))
            {
                SpectrumType color;
                if (buildColor(materialDescription["color"], color, materialType_))
                {
                    emissive->setColor(color);
                }
            }

            // return material
            material = emissive;
        }
        else
        {
            // "material" unknown
            std::cout << "Unknown type of material: " << materialDescription["type"].asString() << std::endl;
            return false;
        }
        
        return true;
    }
    
    
    
    /*! \brief Build a texture objects from a <a HREF="http://www.json.org">JSON</a> description.
     *  \param[in] textureDescription <a HREF="http://www.json.org">JSON</a> value of texture description.
     *  \param[out] texture Built texture object.
     *  \return Returns ´true´ if building was successful, else ´false´.
     */
    template <class SpectrumType>
    bool SceneParser::buildTexture(const Json::Value& textureDescription, std::shared_ptr<Texture<SpectrumType> >& texture)
    {
        // ensure required properties are defined
        if (!checkKey(textureDescription, "type", PropertyType::StringValue, true))
        {
            return false;
        }
        
        if (textureDescription["type"].asString() == "ConstantColor")
        {
            // ensure required properties are defined
            SpectrumType color;
            if (!checkKey(textureDescription, "color", PropertyType::ColorValue, true) ||
                !buildColor(textureDescription["color"], color, materialType_))
            {
                return false;
            }
    
            texture = std::shared_ptr<Texture<SpectrumType> >(new ConstantColor<SpectrumType>(color));
        }
        else if (textureDescription["type"].asString() == "Checker2D")
        {
            // ensure required properties are defined
            if (!checkKey(textureDescription, "sizeU", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "sizeV", PropertyType::NumberValue, true))
            {
                return false;
            }
            
            SpectrumType color1;
            SpectrumType color2;
            if (!checkKey(textureDescription, "color1", PropertyType::ColorValue, true) ||
                !checkKey(textureDescription, "color2", PropertyType::ColorValue, true) ||
                !buildColor(textureDescription["color1"], color1, materialType_) ||
                !buildColor(textureDescription["color2"], color2, materialType_))
            {
                return false;
            }
            
            texture = std::shared_ptr<Texture<SpectrumType> >(new Checker2D<SpectrumType>(
                textureDescription["sizeU"].asFloat(), textureDescription["sizeV"].asFloat(), color1, color2));
        }
        else if (textureDescription["type"].asString() == "Checker3D")
        {
            // ensure required properties are defined
            if (!checkKey(textureDescription, "sizeX", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "sizeY", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "sizeZ", PropertyType::NumberValue, true))
            {
                return false;
            }
            
            SpectrumType color1;
            SpectrumType color2;
            if (!checkKey(textureDescription, "color1", PropertyType::ColorValue, true) ||
                !checkKey(textureDescription, "color2", PropertyType::ColorValue, true) ||
                !buildColor(textureDescription["color1"], color1, materialType_) ||
                !buildColor(textureDescription["color2"], color2, materialType_))
            {
                return false;
            }
            
            texture = std::shared_ptr<Texture<SpectrumType> >(new Checker3D<SpectrumType> (
                textureDescription["sizeX"].asFloat(), textureDescription["sizeY"].asFloat(), textureDescription["sizeZ"].asFloat(), color1, color2 ));
        }
        else if (textureDescription["type"].asString() == "Grid2D")
        {
            // ensure required properties are defined
            if (!checkKey(textureDescription, "faceSizeU", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "edgeSizeU", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "faceSizeV", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "edgeSizeV", PropertyType::NumberValue, true))
            {
                return false;
            }
            
            SpectrumType faceColor;
            SpectrumType edgeColor;
            if (!checkKey(textureDescription, "faceColor", PropertyType::ColorValue, true) ||
                !checkKey(textureDescription, "edgeColor", PropertyType::ColorValue, true) ||
                !buildColor(textureDescription["faceColor"], faceColor, materialType_) ||
                !buildColor(textureDescription["edgeColor"], edgeColor, materialType_))
            {
                return false;
            }
            
            texture = std::shared_ptr<Texture<SpectrumType> >(new Grid2D<SpectrumType>(
                textureDescription["faceSizeU"].asFloat(),
                textureDescription["edgeSizeU"].asFloat(),
                textureDescription["faceSizeV"].asFloat(),
                textureDescription["edgeSizeV"].asFloat(),
                faceColor,
                edgeColor));
        }
        else if (textureDescription["type"].asString() == "Grid3D")
        {
            // ensure required properties are defined
            if (!checkKey(textureDescription, "faceSizeX", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "edgeSizeX", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "faceSizeY", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "edgeSizeY", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "faceSizeZ", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "edgeSizeZ", PropertyType::NumberValue, true))
            {
                return false;
            }
            
            SpectrumType faceColor;
            SpectrumType edgeColor;
            if (!checkKey(textureDescription, "faceColor", PropertyType::ColorValue, true) ||
                !checkKey(textureDescription, "edgeColor", PropertyType::ColorValue, true) ||
                !buildColor(textureDescription["faceColor"], faceColor, materialType_) ||
                !buildColor(textureDescription["edgeColor"], edgeColor, materialType_))
            {
                return false;
            }
            
            texture = std::shared_ptr<Texture<SpectrumType> >(new Grid3D<SpectrumType>(
                textureDescription["faceSizeX"].asFloat(),
                textureDescription["edgeSizeX"].asFloat(),
                textureDescription["faceSizeZ"].asFloat(),
                textureDescription["edgeSizeZ"].asFloat(),
                textureDescription["faceSizeZ"].asFloat(),
                textureDescription["edgeSizeZ"].asFloat(),
                faceColor,
                edgeColor));
        }
        else if (textureDescription["type"].asString() == "Sine2D")
        {
            // ensure required properties are defined
            if (!checkKey(textureDescription, "uFrequency", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "vFrequency", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "uPhase", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "vPhase", PropertyType::NumberValue, true))
            {
                return false;
            }
            
            SpectrumType color1;
            SpectrumType color2;
            if (!checkKey(textureDescription, "color1", PropertyType::ColorValue, true) ||
                !checkKey(textureDescription, "color2", PropertyType::ColorValue, true) ||
                !buildColor(textureDescription["color1"], color1, materialType_) ||
                !buildColor(textureDescription["color2"], color2, materialType_))
            {
                return false;
            }
            
            texture = std::shared_ptr<Texture<SpectrumType> >(new Sine2D<SpectrumType>(
                textureDescription["uFrequency"].asFloat(),
                textureDescription["vFrequency"].asFloat(),
                textureDescription["uPhase"].asFloat(),
                textureDescription["vPhase"].asFloat(),
                color1,
                color2));
        }
        else if (textureDescription["type"].asString() == "Sine3D")
        {
            // ensure required properties are defined
            if (!checkKey(textureDescription, "xFrequency", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "yFrequency", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "zFrequency", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "xPhase", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "yPhase", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "zPhase", PropertyType::NumberValue, true))
            {
                return false;
            }
            
            SpectrumType color1;
            SpectrumType color2;
            if (!checkKey(textureDescription, "color1", PropertyType::ColorValue, true) ||
                !checkKey(textureDescription, "color2", PropertyType::ColorValue, true) ||
                !buildColor(textureDescription["color1"], color1, materialType_) ||
                !buildColor(textureDescription["color2"], color2, materialType_))
            {
                return false;
            }
            
            texture = std::shared_ptr<Texture<SpectrumType> >(new Sine3D<SpectrumType>(
                textureDescription["xFrequency"].asFloat(),
                textureDescription["yFrequency"].asFloat(),
                textureDescription["zFrequency"].asFloat(),
                textureDescription["xPhase"].asFloat(),
                textureDescription["yPhase"].asFloat(),
                textureDescription["zPhase"].asFloat(),
                color1,
                color2));
        }
        else if (textureDescription["type"].asString() == "NextNeighborNoiseTexture")
        {
            // ensure required properties are defined
            if (!checkKey(textureDescription, "amplitudeFactor", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "frequencyFactor", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "iterations", PropertyType::IntValue, true) ||
                !checkKey(textureDescription, "gridSize", PropertyType::NumberValue, true))
            {
                return false;
            }
            
            SpectrumType color;
            if (!checkKey(textureDescription, "color", PropertyType::ColorValue, true) ||
                !buildColor(textureDescription["color"], color, materialType_))
            {
                return false;
            }

            if (checkKey(textureDescription, "seed", PropertyType::NumberValue, false))
            {
                texture = std::shared_ptr<Texture<SpectrumType> >(new NextNeighborNoiseTexture<SpectrumType>(
                    color,
                    textureDescription["amplitudeFactor"].asFloat(),
                    textureDescription["frequencyFactor"].asFloat(),
                    textureDescription["iterations"].asInt(),
                    textureDescription["gridSize"].asFloat(),
                    textureDescription["seed"].asInt()));
            }
            else if (checkKey(textureDescription, "seed", PropertyType::StringValue, false) &&
                textureDescription["seed"].asString() == "Random")
            {
                texture = std::shared_ptr<Texture<SpectrumType> >(new NextNeighborNoiseTexture<SpectrumType>(
                    color,
                    textureDescription["amplitudeFactor"].asFloat(),
                    textureDescription["frequencyFactor"].asFloat(),
                    textureDescription["iterations"].asInt(),
                    textureDescription["gridSize"].asFloat(),
                   randomSeed()));
            }
            else
            {
                texture = std::shared_ptr<Texture<SpectrumType> >(new NextNeighborNoiseTexture<SpectrumType>(
                    color,
                    textureDescription["amplitudeFactor"].asFloat(),
                    textureDescription["frequencyFactor"].asFloat(),
                    textureDescription["iterations"].asInt(),
                    textureDescription["gridSize"].asFloat()));
            }
        }
        else if (textureDescription["type"].asString() == "LinearNoiseTexture")
        {
            // ensure required properties are defined
            if (!checkKey(textureDescription, "amplitudeFactor", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "frequencyFactor", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "iterations", PropertyType::IntValue, true) ||
                !checkKey(textureDescription, "gridSize", PropertyType::NumberValue, true))
            {
                return false;
            }
            
            SpectrumType color;
            if (!checkKey(textureDescription, "color", PropertyType::ColorValue, true) ||
                !buildColor(textureDescription["color"], color, materialType_))
            {
                return false;
            }
            
            if (checkKey(textureDescription, "seed", PropertyType::NumberValue, false))
            {
                texture = std::shared_ptr<Texture<SpectrumType> >(new LinearNoiseTexture<SpectrumType>(
                    color,
                    textureDescription["amplitudeFactor"].asFloat(),
                    textureDescription["frequencyFactor"].asFloat(),
                    textureDescription["iterations"].asInt(),
                    textureDescription["gridSize"].asFloat(),
                    textureDescription["seed"].asInt()));
            }
            else if (checkKey(textureDescription, "seed", PropertyType::StringValue, false) &&
                textureDescription["seed"].asString() == "Random")
            {
                texture = std::shared_ptr<Texture<SpectrumType> >(new LinearNoiseTexture<SpectrumType>(
                    color,
                    textureDescription["amplitudeFactor"].asFloat(),
                    textureDescription["frequencyFactor"].asFloat(),
                    textureDescription["iterations"].asInt(),
                    textureDescription["gridSize"].asFloat(),
                    randomSeed()));
            }
            else
            {
                texture = std::shared_ptr<Texture<SpectrumType> >(new LinearNoiseTexture<SpectrumType>(
                    color,
                    textureDescription["amplitudeFactor"].asFloat(),
                    textureDescription["frequencyFactor"].asFloat(),
                    textureDescription["iterations"].asInt(),
                    textureDescription["gridSize"].asFloat()));
            }
        }
        else if (textureDescription["type"].asString() == "CubicNoiseTexture")
        {
            // ensure required properties are defined
            if (!checkKey(textureDescription, "amplitudeFactor", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "frequencyFactor", PropertyType::NumberValue, true) ||
                !checkKey(textureDescription, "iterations", PropertyType::IntValue, true) ||
                !checkKey(textureDescription, "gridSize", PropertyType::NumberValue, true))
            {
                return false;
            }
            
            SpectrumType color;
            if (!checkKey(textureDescription, "color", PropertyType::ColorValue, true) ||
                !buildColor(textureDescription["color"], color, materialType_))
            {
                return false;
            }
            
            if (checkKey(textureDescription, "seed", PropertyType::NumberValue, false))
            {
                texture = std::shared_ptr<Texture<SpectrumType> >(new CubicNoiseTexture<SpectrumType>(
                    color,
                    textureDescription["amplitudeFactor"].asFloat(),
                    textureDescription["frequencyFactor"].asFloat(),
                    textureDescription["iterations"].asInt(),
                    textureDescription["gridSize"].asFloat(),
                    textureDescription["seed"].asInt()));
            }
            else if (checkKey(textureDescription, "seed", PropertyType::StringValue, false) &&
                textureDescription["seed"].asString() == "Random")
            {
                texture = std::shared_ptr<Texture<SpectrumType> >(new CubicNoiseTexture<SpectrumType>(
                    color,
                    textureDescription["amplitudeFactor"].asFloat(),
                    textureDescription["frequencyFactor"].asFloat(),
                    textureDescription["iterations"].asInt(),
                    textureDescription["gridSize"].asFloat(),
                    randomSeed()));
            }
            else
            {
                texture = std::shared_ptr<Texture<SpectrumType> >(new CubicNoiseTexture<SpectrumType>(
                    color,
                    textureDescription["amplitudeFactor"].asFloat(),
                    textureDescription["frequencyFactor"].asFloat(),
                    textureDescription["iterations"].asInt(),
                    textureDescription["gridSize"].asFloat()));
            }
        }
        else if (textureDescription["type"].asString() == "ImageTexture")
        {
            // ensure required properties are defined
            if (!checkKey(textureDescription, "image", PropertyType::StringValue, true) ||
                !checkKey(textureDescription, "mapping", PropertyType::StringValue, true))
            {
                return false;
            }

            // build texture
            std::shared_ptr<ImageTexture<SpectrumType> > imageTexture(new ImageTexture<SpectrumType>());

            // build image
            std::shared_ptr<cimg_library::CImg<unsigned char> > image;
            // Image path relative to json file path
            std::string imageFileName = std::string(filePath_).append(textureDescription["image"].asCString());

            // open file to check for existence
            std::FILE* f = std::fopen(imageFileName.c_str(), "r");

            // open texture image if existent
            if (f != NULL)
            {
                std::fclose(f);

                image = std::shared_ptr<cimg_library::CImg<unsigned char> >(new cimg_library::CImg<unsigned char>(imageFileName.c_str()));
            }
            else
            {
                std::cout << "Can't find the image " << imageFileName << "." << std::endl;
                return false;
            }

            if (image->is_empty())
            {
                std::cout << "Failed to load image " << textureDescription["image"].asCString() << "." << std::endl;
                return false;
            }

            if (image->spectrum() == 1)
            {
                image->resize(image->width(), image->height(), 1, 3);
            }

            imageTexture->setImage(image);

            // build mapping
            if (textureDescription["mapping"].asString() == "SphereMap")
            {
                imageTexture->setMapping(std::shared_ptr<SphereMap>(new SphereMap()));
            }
            else if (textureDescription["mapping"].asString() == "LightProbeMap")
            {
                imageTexture->setMapping(std::shared_ptr<LightProbeMap>(new LightProbeMap()));
            }
            else if (textureDescription["mapping"].asString() == "SquareMap")
			{
                imageTexture->setMapping(std::shared_ptr<SquareMap>(new SquareMap()));
			}
            else if (textureDescription["mapping"].asString() == "DiskMap")
            {
                imageTexture->setMapping(std::shared_ptr<DiskMap>(new DiskMap()));
            }
            else if (textureDescription["mapping"].asString() == "CylinderMap")
            {
                imageTexture->setMapping(std::shared_ptr<CylinderMap>(new CylinderMap()));
            }
            else
            {
                std::cout << "Unknown type of image mapping: " << textureDescription["mapping"].asString() << std::endl;
                return false;
            }

            // set texture
            texture = imageTexture;
        }
        else if (textureDescription["type"].asString() == "SpectralImageTexture")
        {
            // ensure required properties are defined
            if (!checkKey(textureDescription, "lambda", PropertyType::VectorValue, true) ||
                !checkKey(textureDescription, "data", PropertyType::StringValue, true) ||
                !checkKey(textureDescription, "mapping", PropertyType::StringValue, true))
            {
                return false;
            }
            // Image path relative to json file path
            std::string imageFileName = std::string(filePath_).append(textureDescription["data"].asCString());

            // Read image data array
            cnpy::NpyArray data_in_arr = cnpy::npy_load(imageFileName);
            unsigned long num_ch_in = data_in_arr.shape[2];

            // Check data
            unsigned long word_size = data_in_arr.word_size;

            if (word_size != 8) {
                std::cout << "Given Texture data " << imageFileName << " seems not the be a double." << std::endl;
                std::cout << "Please pass data with 64bit double precision." << std::endl;
                return false;
            }

            // Get lambda values
            std::vector<float> lambda;
            lambda.reserve(textureDescription["lambda"].size());
            for (unsigned int i = 0; i < textureDescription["lambda"].size(); ++i)
            {
                lambda.push_back(textureDescription["lambda"][i].asFloat());
            }

            // Check size
            unsigned long num_ch_lambdas = lambda.size();
            std::vector<float> lambda_arr;

             if (num_ch_lambdas != num_ch_in){
                if(num_ch_lambdas == 2){

                    float lambda_start = lambda[0];
                    float lambda_end = lambda[1];

                    // Check if order is correct
                    if(lambda_end <= lambda_start){
                        std::cout << "Given lambda values are not in correct order." << std::endl;
                        std::cout << "Please specify data and lambda, such that lambda values are ascending." << std::endl;
                        return false;
                    }
                    // Calculate values (equidistant)
                    for (unsigned int i = 0; i < num_ch_in; ++i)
                    {
                        lambda_arr.push_back(i*(lambda_end - lambda_start)/(num_ch_in - 1) + lambda_start);
                    }


                }
                else{
                    std::cout << "Failed to create texture: Number of image channels and lambda values do not agree" << std::endl;
                    std::cout << "Either pass lambda value range, or lambda values for every channel." << std::endl;
                    return false;
                }
             } // end: check size
             else{
                 lambda_arr = lambda;
             }

            // Create spectrum data matrix (multi spectral image) with right number of spectral samples (numSpectralSamples)
            // This gets passed to the SpectralImageTexture

            unsigned long nRows = data_in_arr.shape[0];
            unsigned long nCols = data_in_arr.shape[1];

            std::shared_ptr<std::vector<SpectrumType> > data(new std::vector<SpectrumType>());

            // Copy and convert data to SpectrumType
            double* loaded_data = data_in_arr.data<double>();

            for(unsigned int i = 0; i < nRows*nCols; i++){
                std::vector<float> value;
                for(unsigned int k = 0; k < num_ch_in; k++){
                    value.push_back(float(loaded_data[num_ch_in*i + k]));
                }

                // Copy data
                data->push_back(SpectrumType::fromSampled(lambda_arr, value));
            }


            // build texture
            std::shared_ptr<SpectralImageTexture<SpectrumType> > spectralImageTexture(new SpectralImageTexture<SpectrumType>());

            spectralImageTexture->setData(data);
            spectralImageTexture->setShape(nCols, nRows);

            // build mapping
            if (textureDescription["mapping"].asString() == "SphereMap")
            {
                spectralImageTexture->setMapping(std::shared_ptr<SphereMap>(new SphereMap()));
            }
            else if (textureDescription["mapping"].asString() == "LightProbeMap")
            {
                spectralImageTexture->setMapping(std::shared_ptr<LightProbeMap>(new LightProbeMap()));
            }
            else if (textureDescription["mapping"].asString() == "SquareMap")
            {
                spectralImageTexture->setMapping(std::shared_ptr<SquareMap>(new SquareMap()));
            }
            else if (textureDescription["mapping"].asString() == "DiskMap")
            {
                spectralImageTexture->setMapping(std::shared_ptr<DiskMap>(new DiskMap()));
            }
            else if (textureDescription["mapping"].asString() == "CylinderMap")
            {
                spectralImageTexture->setMapping(std::shared_ptr<CylinderMap>(new CylinderMap()));
            }
            else
            {
                std::cout << "Unknown type of image mapping: " << textureDescription["mapping"].asString() << std::endl;
                return false;
            }

            // set texture
            texture = spectralImageTexture;
        }
        else
        {
            // "texture" unknown
            std::cout << "Unknown type of texture: " << textureDescription["type"].asString() << std::endl;
            return false;
        }
        return true;
    }
    
    
    
    /*! \brief Build light objects from a <a HREF="http://www.json.org">JSON</a> description.
     *  \param[in] lightDescription <a HREF="http://www.json.org">JSON</a> value of lights description.
     *  \param[out] scene Scene object to store scene data.
     *  \return Returns ´true´ if building was successful, else ´false´.
     */
    template <class SpectrumType>
    bool SceneParser::buildLights(const Json::Value& lightDescription, std::shared_ptr<Scene<SpectrumType> > scene)
    {
        for (unsigned int i = 0; i < lightDescription.size(); ++i)
        {
            // ensure required properties are defined
            if (!checkKey(lightDescription[i], "type", PropertyType::StringValue, true))
            {
                return false;
            }
            
            if (lightDescription[i]["type"].asString() == "AmbientLight")
            {
                if (!scene->getAmbientLight().expired())
                {
                    std::cout << "Only one ambient light per scene allowed. Ignoring additional ambient lights." << std::endl;
                }
                else
                {
                    std::shared_ptr<Ambient<SpectrumType> > ambientLight(new Ambient<SpectrumType>);
                    
                    // check optional properties
                    SpectrumType color;
                    if (checkKey(lightDescription[i], "color", PropertyType::ColorValue, false) &&
                        buildColor(lightDescription[i]["color"], color, lightType_))
                    {
                        ambientLight->setColor(color);
                    }
  
                    if (checkKey(lightDescription[i], "radianceScaling", PropertyType::NumberValue, false))
                    {
                        ambientLight->setRadianceScaling(lightDescription[i]["radianceScaling"].asFloat());
                    }
                    
                    // building successful
                    scene->setAmbientLight(ambientLight);
                }
            }
            else if (lightDescription[i]["type"].asString() == "AmbientOccluder")
            {
                if (!scene->getAmbientLight().expired())
                {
                    std::cout << "Only one ambient light per scene allowed. Ignoring additional ambient lights." << std::endl;
                }
                else
                {
                    std::shared_ptr<AmbientOccluder<SpectrumType> > ambientOccluder(new AmbientOccluder<SpectrumType>);
                    
                    // check existence of "sampler" key
                    std::shared_ptr<Sampler> sampler;
                    float exp = 1.f;
                    if (!checkKey(lightDescription[i], "sampler", PropertyType::ObjectValue, false))
                    {
                        std::cout << "No ""sampler"" found. Assuming regular sampling with one sample." << std::endl;
                        sampler = std::shared_ptr<Sampler>(new Regular());
                        sampler->generateSamples();
                    }
                    else
                    {
                        if (!buildSampler(lightDescription[i]["sampler"], sampler))
                        {
                            return false;
                        }
                        if (!checkKey(lightDescription[i]["sampler"], "exp", PropertyType::NumberValue, false))
                        {
                            exp = 1.f;
                        }
                        else
                        {
                            exp = lightDescription[i]["sampler"].asFloat();
                        }
                    }

                    // map samples to hemisphere
                    sampler->mapSamplesToHemisphere(exp);
            
                    // set sampler
                    ambientOccluder->setSampler(std::shared_ptr<Sampler>(sampler));
                    
                    // check optional properties
                    SpectrumType color;
                    if (checkKey(lightDescription[i], "color", PropertyType::ColorValue, false) &&
                        buildColor(lightDescription[i]["color"], color, lightType_))
                    {
                        ambientOccluder->setColor(color);
                    }
  
                    if (checkKey(lightDescription[i], "radianceScaling", PropertyType::NumberValue, false))
                    {
                        ambientOccluder->setRadianceScaling(lightDescription[i]["radianceScaling"].asFloat());
                    }

                    SpectrumType minColor;
                    if (lightDescription[i].isMember("minAmount"))
                    {
                        ambientOccluder->setMinAmount(lightDescription[i]["minAmount"].asFloat());
                    }
                    
                    // building successful
                    scene->setAmbientLight(ambientOccluder);
                }
            }
            else if (lightDescription[i]["type"].asString() == "DirectionalLight")
            {
                // ensure required properties are defined
                if (!checkKey(lightDescription[i], "direction", PropertyType::VectorValue, true) ||
                    !checkDimension(lightDescription[i]["direction"], 3, true))
                {
                    return false;
                }
                std::shared_ptr<DirectionalLight<SpectrumType> > directionalLight(new DirectionalLight<SpectrumType>(Vector3D(
                    lightDescription[i]["direction"][0].asDouble(),
                    lightDescription[i]["direction"][1].asDouble(),
                    lightDescription[i]["direction"][2].asDouble())));
                
                // check optional properties
                SpectrumType color;
                if (checkKey(lightDescription[i], "color", PropertyType::ColorValue, false) &&
                    buildColor(lightDescription[i]["color"], color, lightType_))
                {
                    directionalLight->setColor(color);
                }

                if (checkKey(lightDescription[i], "radianceScaling", PropertyType::NumberValue, false))
                {
                    directionalLight->setRadianceScaling(lightDescription[i]["radianceScaling"].asFloat());
                }
                
                // building successful
                scene->addLight(directionalLight);
            }
            else if (lightDescription[i]["type"].asString() == "PointLight")
            {
                // ensure required properties are defined
                if (!checkKey(lightDescription[i], "position", PropertyType::VectorValue, true) ||
                    !checkDimension(lightDescription[i]["position"], 3, true))
                {
                    return false;
                }
                std::shared_ptr<PointLight<SpectrumType> > pointLight(new PointLight<SpectrumType>(Point3D(
                    lightDescription[i]["position"][0].asDouble(),
                    lightDescription[i]["position"][1].asDouble(),
                    lightDescription[i]["position"][2].asDouble())));
                
                // check optional properties
                SpectrumType color;
                if (checkKey(lightDescription[i], "color", PropertyType::ColorValue, false) &&
                    buildColor(lightDescription[i]["color"], color, lightType_))
                {
                    pointLight->setColor(color);
                }
                
                if (checkKey(lightDescription[i], "radianceScaling", PropertyType::NumberValue, false))
                {
                    pointLight->setRadianceScaling(lightDescription[i]["radianceScaling"].asFloat());
                }
                
                // building successful
                scene->addLight(pointLight);
            }
            else if (lightDescription[i]["type"].asString() == "AreaLight")
            {
                // ensure required properties are defined
                if (!checkKey(lightDescription[i], "object", PropertyType::ObjectValue, true) ||
                    !checkKey(lightDescription[i]["object"], "type", PropertyType::StringValue, true) ||
                    !checkKey(lightDescription[i]["object"], "material", PropertyType::ObjectValue, true) ||
                    !checkKey(lightDescription[i]["object"]["material"], "type", PropertyType::StringValue, true))
                {
                    return false;
                }
                if (lightDescription[i]["object"]["type"].asString() != "Square" &&
                    lightDescription[i]["object"]["type"].asString() != "Disk" &&
                    lightDescription[i]["object"]["type"].asString() != "Sphere")
                {
                    std::cout << "Only Square, Disk and Sphere objects allowed as area lights." << std::endl;
                    return false;
                }
                if (lightDescription[i]["object"]["material"]["type"].asString() != "Emissive")
                {
                    std::cout << "Area lights have to use the Emissive material." << std::endl;
                    return false;
                }

                // build object
                std::shared_ptr<AreaLightObject<SpectrumType> > areaLightObject;
                if (!buildAreaLightObject(lightDescription[i]["object"], areaLightObject))
                {
                    return false;
                }

                // build sampler
                std::shared_ptr<Sampler> sampler;
                if (!checkKey(lightDescription[i]["object"], "sampler", PropertyType::ObjectValue, false))
                {
                    std::cout << "No ""sampler"" found. Assuming regular sampling with one sample." << std::endl;
                    sampler = std::shared_ptr<Sampler>(new Regular());
                    sampler->generateSamples();
                }
                else
                {
                    if (!buildSampler(lightDescription[i]["object"]["sampler"], sampler))
                    {
                        return false;
                    }
                }
                if (lightDescription[i]["object"]["type"].asString() == "Disk")
                {
                    sampler->mapSamplesToUnitDisk();
                }
                else if (lightDescription[i]["object"]["type"].asString() == "Sphere")
                {
                    sampler->mapSamplesToSphere();
                }
                areaLightObject->setSampler(sampler);
                
                // build light
                std::shared_ptr<AreaLight<SpectrumType> > areaLight(new AreaLight<SpectrumType>(areaLightObject));

                // building successful
                scene->addLight(areaLight);
                scene->addObject(areaLightObject);
            }
            else if (lightDescription[i]["type"].asString() == "EnvironmentLight")
            {
                // ensure required properties are defined
                if (!checkKey(lightDescription[i], "material", PropertyType::ObjectValue, true) ||
                    !checkKey(lightDescription[i]["material"], "type", PropertyType::StringValue, true))
                {
                    return false;
                }
                if (lightDescription[i]["material"]["type"].asString() != "Emissive")
                {
                    std::cout << "Environment lights have to use the Emissive material." << std::endl;
                    return false;
                }

                // build sampler
                std::shared_ptr<Sampler> sampler;
                float exp = 1.f;
                if (!checkKey(lightDescription[i], "sampler", PropertyType::ObjectValue, false))
                {
                    std::cout << "No ""sampler"" found. Assuming regular sampling with one sample." << std::endl;
                    sampler = std::shared_ptr<Sampler>(new Regular());
                    sampler->generateSamples();
                }
                else
                {
                    if (!buildSampler(lightDescription[i]["sampler"], sampler))
                    {
                        return false;
                    }
                    if (checkKey(lightDescription[i]["sampler"], "exp", PropertyType::NumberValue, false))
                    {
                        exp = lightDescription[i]["sampler"].asFloat();
                    }
                }
                sampler->mapSamplesToHemisphere(exp);

                // build material
                std::shared_ptr<Material<SpectrumType> > material;
                if (!buildMaterial<SpectrumType>(lightDescription[i]["material"], material))
                {
                    return false;
                }

                // build light
                std::shared_ptr<EnvironmentLight<SpectrumType> > environmentLight(new EnvironmentLight<SpectrumType>(material));
                environmentLight->setSampler(sampler);

                // build object
                std::shared_ptr<GeometricObject<SpectrumType> > sphere(new Sphere<SpectrumType>(0.0, 0.0, 0.0, 1e6, material));
                sphere->setCastsShadows(false);

                // building successful
                scene->addLight(environmentLight);
                scene->addObject(sphere);
            }
            else
            {
                // "type" unknown
                std::cout << "Unknown type of light: " << lightDescription[i]["type"].asString() << std::endl;
                return false;
            }
            
            // set optional shadow flag
            if (checkKey(lightDescription[i], "castsShadows", PropertyType::BoolValue, false) && lightDescription[i]["type"].asString() != "AmbientLight")
            {
                scene->lights_.back()->setShadows(lightDescription[i]["castsShadows"].asBool());
            }
        }
        return true;
    }
    
    
    
    /*! \brief Build a color representation object from a <a HREF="http://www.json.org">JSON</a> description.
     *  \param[in] colorDescription <a HREF="http://www.json.org">JSON</a> value of color description.
     *  \param[out] color Color representation object to store data.
     *  \param[in] type Type of spectral representation base functions.
     *  \return Returns ´true´ if building was successful, else ´false´.
     *
     *  Function parses a color representation object. There are three basic methods to define a color:
     *  1. Definition of a gray-scale value:
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
     *  {
     *      "<color key>" : 0.5
     *  }
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *  2. Definition of an RGB color:
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
     *  {
     *      "<color key>" : [1.0, 0.5, 0.0]
     *  }
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *  3. Definition of a sampled spectrum:
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
     *  {
     *      "<color key>" : {
     *          "lambda" : [<array of wavelength values>],
     *          "value" : [<array of spectral power values>]
     *      }
     *  }
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     */
    template <class SpectrumType>
    bool SceneParser::buildColor(const Json::Value& colorDescription, SpectrumType& color, SpectrumRepresentationType type)
    {
        if (colorDescription.isObject())
        {
            // sampled data
            if (!checkKey(colorDescription, "lambda", PropertyType::VectorValue, true) ||
                !checkKey(colorDescription, "value", PropertyType::VectorValue, true) ||
                !checkDimension(colorDescription["value"], colorDescription["lambda"].size(), true))
            {
                return false;
            }
            
            std::vector<float> lambda;
            lambda.reserve(colorDescription["lambda"].size());
            std::vector<float> value;
            value.reserve(colorDescription["lambda"].size());
            
            for (unsigned int i = 0; i < colorDescription["lambda"].size(); ++i)
            {
                lambda.push_back(colorDescription["lambda"][i].asFloat());
                value.push_back(colorDescription["value"][i].asFloat());
            }
            
            color = SpectrumType::fromSampled(lambda, value);
            return true;
        }
        else if (colorDescription.isArray())
        {
            // rgb values
            if (!checkValue(colorDescription, SceneParser::PropertyType::VectorValue, true) ||
                !checkDimension(colorDescription, 3, true))
            {
                return false;
            }
            std::array<float, 3> rgb = {{0.f, 0.f, 0.f}};
            rgb[0] = colorDescription[0].asFloat() / TRUE_COLOR_MAX;
            rgb[1] = colorDescription[1].asFloat() / TRUE_COLOR_MAX;
            rgb[2] = colorDescription[2].asFloat() / TRUE_COLOR_MAX;
            color = SpectrumType::fromRgb(rgb, type);
            return true;
        }
        else if (colorDescription.isNumeric())
        {
            // gray-scale value
            if (!checkValue(colorDescription, SceneParser::PropertyType::NumberValue, true))
            {
                return false;
            }
            color = SpectrumType(colorDescription.asFloat());
            return true;
        }
        else
        {
            std::cout << "Failed to parse color: " << colorDescription.toStyledString() << std::endl;
            std::cout << "Consult the documentation of method buildColor()." << std::endl;
            return false;
        }
    }
    
    
    
    /*! \brief Build a tracer object from a <a HREF="http://www.json.org">JSON</a> description.
     *  \param[in] tracerDescription <a HREF="http://www.json.org">JSON</a> value of tracer description.
     *  \param[out] scene Scene object to store scene data.
     *  \return Returns ´true´ if building was successful, else ´false´.
     */
    template <class SpectrumType>
    bool SceneParser::buildTracer(const Json::Value& tracerDescription, std::shared_ptr<Scene<SpectrumType> > scene)
    {
        // ensure required properties are defined
        if (!checkKey(tracerDescription, "type", PropertyType::StringValue, true))
        {
            return false;
        }

        // Default natural vignetting tracing to false, otherwise load from tracerDescription
        bool vignetting = false;
        if (checkKey(tracerDescription, "vignetting", PropertyType::BoolValue, true))
        {
            vignetting = tracerDescription["vignetting"].asBool();
        }

        if (tracerDescription["type"].asString() == "RayCast")
        {
            std::shared_ptr<Tracer<SpectrumType> > tracer(new RayCast<SpectrumType>(scene, vignetting));
            scene->setTracer(tracer);
        }
        else if (tracerDescription["type"].asString() == "AreaLighting")
        {
            std::shared_ptr<Tracer<SpectrumType> > tracer(new AreaLighting<SpectrumType>(scene, vignetting));
            scene->setTracer(tracer);
        }
        else if (tracerDescription["type"].asString() == "Whitted")
        {
            std::shared_ptr<Whitted<SpectrumType> > tracer(new Whitted<SpectrumType>(scene, vignetting));
            
            // check optional parameters
            if (checkKey(tracerDescription, "maxDepth", PropertyType::NumberValue, false))
            {
                tracer->setMaxDepth(tracerDescription["maxDepth"].asInt());
            }
       
            scene->setTracer(tracer);
        }
        else if (tracerDescription["type"].asString() == "WhittedAreaLighting")
        {
            std::shared_ptr<WhittedAreaLighting<SpectrumType> > tracer(new WhittedAreaLighting<SpectrumType>(scene, vignetting));
            
            // check optional parameters
            if (checkKey(tracerDescription, "maxDepth", PropertyType::NumberValue, false))
            {
                tracer->setMaxDepth(tracerDescription["maxDepth"].asInt());
            }
       
            scene->setTracer(tracer);
        }
        else if (tracerDescription["type"].asString() == "PathTracer")
        {
            std::shared_ptr<PathTracer<SpectrumType> > tracer(new PathTracer<SpectrumType>(scene, vignetting));
            
            // check optional parameters
            if (checkKey(tracerDescription, "maxDepth", PropertyType::NumberValue, false))
            {
                tracer->setMaxDepth(tracerDescription["maxDepth"].asInt());
            }
            
            scene->setTracer(tracer);
        }
        else if (tracerDescription["type"].asString() == "DepthTracer")
        {
            std::shared_ptr<Tracer<SpectrumType> > tracer;

            std::cout << "You are using the DepthTracer. Make sure to specify the output file with PFM ending to get correct 32bit results. For this, please use the camera's 'imageType' = 'Raw' option. ";
    
            // check optional parameters
            if (checkKey(tracerDescription, "minDistance", PropertyType::NumberValue, false))
            {
                tracer.reset(new DepthTracer<SpectrumType>(scene, tracerDescription["minDistance"].asFloat()));
            }
            else
            {
                tracer.reset(new DepthTracer<SpectrumType>(scene));
            }
    
            scene->setTracer(tracer);
        }
        else if (tracerDescription["type"].asString() == "CoordinateTracer")
        {
            std::shared_ptr<Tracer<SpectrumType> > tracer;

            std::cout << "You are using the CoordinateTracer. Make sure to specify the output file with PFM ending to get correct 32bit results. For this, please use the camera's 'imageType' = 'IdealRgb' option. ";

            int coordinateType = 2;
            int reflectionDepth = 0;

            // check optional parameters
            if (checkKey(tracerDescription, "coordinateType", PropertyType::NumberValue, false))
            {
                coordinateType = tracerDescription["coordinateType"].asInt();
            }
            if (checkKey(tracerDescription, "reflectionDepth", PropertyType::NumberValue, false))
            {
                reflectionDepth = tracerDescription["reflectionDepth"].asInt();
            }

            tracer.reset(new CoordinateTracer<SpectrumType>(scene, coordinateType, reflectionDepth));

            scene->setTracer(tracer);
        }
        else if (tracerDescription["type"].asString() == "SurfaceTracer")
        {
            std::shared_ptr<Tracer<SpectrumType> > tracer;
            tracer.reset(new SurfaceTracer<SpectrumType>(scene));
            scene->setTracer(tracer);
        }
        else if (tracerDescription["type"].asString() == "NormalTracer")
        {
            std::shared_ptr<Tracer<SpectrumType> > tracer;

            // check optional parameters
            if (checkKey(tracerDescription, "encodeNormals", PropertyType::BoolValue, false))
            {
                tracer.reset(new NormalTracer<SpectrumType>(scene, tracerDescription["encodeNormals"].asBool()));
            }
            else
            {
                tracer.reset(new NormalTracer<SpectrumType>(scene));
            }
            scene->setTracer(tracer);
        }
        else if (tracerDescription["type"].asString() == "ObjectTracer")
        {
            std::shared_ptr<Tracer<SpectrumType> > tracer;
            tracer.reset(new ObjectTracer<SpectrumType>(scene));
            scene->setTracer(tracer);
        }
        else if (tracerDescription["type"].asString() == "RayParameterTracer")
        {
            std::cout << "You are using the RayParameterTracer. ";
            // check optional parameters
            if (numSpectralSamples != 6)
            {   
                std::cout << "'numSpectralSamples' is set to """ << numSpectralSamples << """ but it needs to be 6." << std::endl;
                std::cout << "Please compile the project with the correct number of samples. Specify the value of 'NUM_SPEC_SAMPLES' in CMAKE." << std::endl;
                return false;
            }
            else
            {   
                std::cout << "Make sure to specify 'spectrumType' = 'SampledSpectrum' and please use the camera's 'imageType' : 'MultiSpectral' option" << std::endl;
                std::shared_ptr<Tracer<SpectrumType> > tracer;
                tracer.reset(new RayParameterTracer<SpectrumType>(scene));
                scene->setTracer(tracer);
            }
        }
        else
        {
            // "type" unknown
            std::cout << "Unknown type of tracer: " << tracerDescription["type"].asString() << std::endl;
            return false;
        }
        return true;
    }

} // end of namespace iiit

#endif // __SCENEPARSER_HPP__
