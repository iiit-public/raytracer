// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file SessionParser.hpp
 *  \author Thomas Nuernberg
 *  \date 23.01.2015
 *  \brief Definition of class SessionParser.
 *
 *  Definition of class SessionParser used to extract session data from a
 *  <a HREF="http://www.json.org">JSON</a> description.
 */

#ifndef __SESSIONPARSER_HPP__
#define __SESSIONPARSER_HPP__

#include <iostream>
#include <string>

#include "json/json.h"



namespace iiit
{

    /*! \brief Class parse session data.
     *
     *  Class for extracting data from a <a HREF="http://www.json.org">JSON</a> description to continue
     *  rendering of a previously aborted session.
     *
     *  The basic structure of a session is to define a <a HREF="http://www.json.org">JSON</a> object
     *  that defines the keys "image," "progress," "sceneDescription," and "chunkSize".
     *  
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.java}
     *  {
     *      "image" : <unfinished image to complete>,
     *      "progress" : <hex file containing finished image chunks>,
     *      "sceneDescription" : <description file of scene to complete>
     *      "chunkSize": <chunk size used for rendering>
     *  }
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     */
    class SessionParser
    {
    public:
        SessionParser();
        ~SessionParser();
        
        bool parse(const std::string& sessionFile);
        bool parse(std::istream& sessionFile);

        std::string getImageFileName() const;
        std::string getProgressFileName() const;
        std::string getSceneDescriptionFileName() const;
        int getChunkWidth() const;
        int getChunkHeight() const;

    private:
        std::string imageFileName_; ///< String containing file name of image.
        std::string progressFileName_; ///< String containing file name of progress file.
        std::string sceneDescriptionFileName_; ///< String containing file name of scene description.
        int chunkWidth_; ///< Width of image chunks.
        int chunkHeight_; ///< Height of image chunks.
    };

} // end of namespace iiit

#endif // __SESSIONPARSER_HPP__