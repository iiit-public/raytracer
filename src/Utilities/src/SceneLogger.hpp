// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file SceneLogger.hpp
 *  \author Thomas Nuernberg
 *  \date 22.01.2015
 *  \brief Definition of class SceneLogger.
 *
 *  Definition of class SceneLogger used to log scene data in <a HREF="http://www.json.org">JSON</a>
 *  syntax.
 *
 *  \deprecated This class is not up to date and no longer in use.
 */

//#ifndef __SCENELOGGER_HPP__
//#define __SCENELOGGER_HPP__
//
//#include <iostream>
//#include <memory>
//
//#include "json/json.h"
//
//#include "Scene.hpp"
//#include "Objects/GeometricObject.hpp"
//#include "Sampler/Sampler.hpp"
//
//
//
///*! \brief Class to log world data describing the current scene in
// *  <a HREF="http://www.json.org">JSON</a> syntax.
// *
// *  \deprecated This class is not up to date and no longer in use.
// */
//class SceneLogger
//{
//public:
//    SceneLogger();
//    ~SceneLogger();
//
//    void log(std::shared_ptr<const Scene> scene, std::ostream& logTarget) const;
//
//private:
//    void logSampler(std::shared_ptr<const Sampler> sampler, Json::Value& samplerDescription) const;
//};
//
//#endif // __SCENELOGGER_HPP__