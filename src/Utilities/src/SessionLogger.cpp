// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file SessionLogger.cpp
 *  \author Thomas Nuernberg
 *  \date 29.01.2015
 *  \brief Implementation of class SessionLogger.
 *
 *  Implementation of class SessionLogger used to log session data.
 */

#include "SessionLogger.hpp"

#include "Utilities/Constants.h"



namespace iiit
{

    const char* SessionLogger::sceneFileName_ = "scene.json";
    const char* SessionLogger::progressFileName_ = "progress.hex";
    const char* SessionLogger::imageFileName_ = "image_in_progress";



    /*! \brief Default constructor.
     */
    SessionLogger::SessionLogger()
    {
        // check for file i/o capabilities
        std::FILE *file = 0;
        if ((file = std::fopen(cimg_library::cimg::imagemagick_path(), "r")) != 0)
        {
            std::fclose(file);
            pngFileFormat_ = true;
        }
        else
        {
            pngFileFormat_ = false;
        }
    }



    /*! \brief Default destructor.
     */
    SessionLogger::~SessionLogger()
    {
    }



    /*! \brief Log session data.
     *  \param[in] chunkWidth Pixel width of image chunks.
     *  \param[in] chunkHeight Pixel height of image chunks.
     *  \param[in] logTarget Output stream of session file.
     *
     *  Log session data in <a HREF="http://www.json.org">JSON</a> syntax.
     */
    void SessionLogger::logSession(int chunkWidth, int chunkHeight, std::ostream& logTarget)
    {
        Json::Value root;
        root["sceneDescription"] = sceneFileName_;
        root["image"] = imageFileName_;
        root["chunkSize"].append(chunkWidth);
        root["chunkSize"].append(chunkHeight);
        root["progress"] = progressFileName_;
        Json::StyledStreamWriter writer;
        writer.write(logTarget, root);
    }



    /*! \brief Start logging of the current render progress.
     *
     *  Opens stream of progress file and deletes the content of the progress file if existent.
     */
    void SessionLogger::startProgressLogging()
    {
        progressLogTarget_.open(progressFileName_, std::ios::binary | std::ios::trunc);
    }



    /*! \brief Resume logging of the current render progress.
     *
     *  Opens stream of progress file and sets the file pointer to the end of the file.
     */
    void SessionLogger::resumeProgressLogging()
    {
        progressLogTarget_.open(progressFileName_, std::ios::binary | std::ios::ate);
    }



    /*! \brief Logs current render progress.
     *  \param[in] chunkIndex Index of rendered image chunk.
     *
     *  Writes the given chunk index to the end of the progress file.
     */
    void SessionLogger::logProgress(int chunkIndex)
    {
        progressLogTarget_.write(reinterpret_cast<const char*>(&chunkIndex), sizeof(chunkIndex));
        progressLogTarget_.flush();
    }



    /*! \brief Terminate the logging of the current progress.
     */
    void SessionLogger::endProgressLogging()
    {
        progressLogTarget_.close();
    }



    /*! \brief Writes the given file to the hard drive.
     *  \param[in] floatImage Image to log.
     */
    void SessionLogger::logImage(std::shared_ptr<const cimg_library::CImg<float> > floatImage)
    {
        if (pngFileFormat_)
        {
            // save image as png
            std::shared_ptr<cimg_library::CImg<unsigned short> > image = std::make_shared<cimg_library::CImg<unsigned short> >(
                floatImage->width(), floatImage->height(), floatImage->depth(), floatImage->spectrum());

            // iterate pixels
            for (int u = 0; u < image->width(); ++u)
            {
                for (int v = 0; v < image->height(); ++v)
                {
                    // copy pixel
                    image->operator()(u, v, 0) = static_cast<unsigned short>(floatImage->operator()(u, v, 0) * HIGH_COLOR_MAX);
                    image->operator()(u, v, 1) = static_cast<unsigned short>(floatImage->operator()(u, v, 1) * HIGH_COLOR_MAX);
                    image->operator()(u, v, 2) = static_cast<unsigned short>(floatImage->operator()(u, v, 2) * HIGH_COLOR_MAX);
                }
            }
            char fullImageFileName_[22];
            strcpy(fullImageFileName_, imageFileName_);
            strcat(fullImageFileName_, ".png");
            floatImage->save_png(fullImageFileName_);
        }
        else
        {
            // save image as bmp
            std::shared_ptr<cimg_library::CImg<unsigned char> > image = std::make_shared<cimg_library::CImg<unsigned char> >(
                floatImage->width(), floatImage->height(), floatImage->depth(), floatImage->spectrum());

            // iterate pixels
            for (int u = 0; u < image->width(); ++u)
            {
                for (int v = 0; v < image->height(); ++v)
                {
                    // copy pixel
                    image->operator()(u, v, 0) = static_cast<unsigned char>(floatImage->operator()(u, v, 0) * TRUE_COLOR_MAX);
                    image->operator()(u, v, 1) = static_cast<unsigned char>(floatImage->operator()(u, v, 1) * TRUE_COLOR_MAX);
                    image->operator()(u, v, 2) = static_cast<unsigned char>(floatImage->operator()(u, v, 2) * TRUE_COLOR_MAX);
                }
            }

            char fullImageFileName_[22];
            strcpy(fullImageFileName_, imageFileName_);
            strcat(fullImageFileName_, ".bmp");
            floatImage->save_bmp(fullImageFileName_);
        }
    }



    /*! \brief Updates the given image chunk in the logged image.
     *  \param[in] viewPlane ViewPlane object defining the image chunk.
     *  \param[in] chunk Image chunk to log.
     */
    void SessionLogger::logChunk(const ViewPlane& viewPlane, std::shared_ptr<const cimg_library::CImg<float> > chunk)
    {
        char fullImageFileName_[22];
        strcpy(fullImageFileName_, imageFileName_);
        if (pngFileFormat_)
        {
            strcat(fullImageFileName_, ".png");
            try
            {
                cimg_library::CImg<unsigned short> target(fullImageFileName_);

                // ensure image is loaded as color image
                if (target.spectrum() == 1)
                {
                    target.resize(target.width(), target.height(), target.depth(), 3, 1);
                }

                // iterate pixels
                for (int u = 0; u < chunk->width(); ++u)
                {
                    for (int v = 0; v < chunk->height(); ++v)
                    {
                        // copy pixel
                        target(u + viewPlane.uOffset_, v + viewPlane.vOffset_, 0) = static_cast<unsigned short>(chunk->operator()(u, v, 0) * HIGH_COLOR_MAX);
                        target(u + viewPlane.uOffset_, v + viewPlane.vOffset_, 1) = static_cast<unsigned short>(chunk->operator()(u, v, 1) * HIGH_COLOR_MAX);
                        target(u + viewPlane.uOffset_, v + viewPlane.vOffset_, 2) = static_cast<unsigned short>(chunk->operator()(u, v, 2) * HIGH_COLOR_MAX);
                    }
                }

                target.save_png(fullImageFileName_);
            }
            catch (cimg_library::CImgIOException)
            {
                return;
            }  
        }
        else
        {
            strcat(fullImageFileName_, ".bmp");
            try
            {
                cimg_library::CImg<unsigned char> target(fullImageFileName_);

                // ensure image is loaded as color image
                if (target.spectrum() == 1)
                {
                    target.resize(target.width(), target.height(), target.depth(), 3, 1);
                }

                // iterate pixels
                for (int u = 0; u < chunk->width(); ++u)
                {
                    for (int v = 0; v < chunk->height(); ++v)
                    {
                        // copy pixel
                        target(u + viewPlane.uOffset_, v + viewPlane.vOffset_, 0) = static_cast<unsigned char>(chunk->operator()(u, v, 0) * TRUE_COLOR_MAX);
                        target(u + viewPlane.uOffset_, v + viewPlane.vOffset_, 1) = static_cast<unsigned char>(chunk->operator()(u, v, 1) * TRUE_COLOR_MAX);
                        target(u + viewPlane.uOffset_, v + viewPlane.vOffset_, 2) = static_cast<unsigned char>(chunk->operator()(u, v, 2) * TRUE_COLOR_MAX);
                    }
                }

                target.save_bmp(fullImageFileName_);
            }
            catch (cimg_library::CImgIOException)
            {
                return;
            }  
        }
    }

} // end of namespace iiit