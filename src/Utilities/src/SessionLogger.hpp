// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file SessionLogger.hpp
 *  \author Thomas Nuernberg
 *  \date 29.01.2015
 *  \brief Definition of class SessionLogger.
 *
 *  Definition of class SessionLogger used to log session data.
 */

#ifndef __SESSIONLOGGER_HPP__
#define __SESSIONLOGGER_HPP__

#include <iostream>
#include <fstream>
#include <memory>

#include "CImg.h"
#include "json/json.h"

#include "Cameras/ViewPlane.hpp"



namespace iiit
{

    /*! \brief Class to log current session.
     *
     *  Class logs current session, consisting of render properties and the current progress of the
     *  image rendering. The logged data and the scene description file are sufficient to continue a
     *  previously aborted render session.
     */
    class SessionLogger
    {
    public:
        SessionLogger();
        ~SessionLogger();

        // session logging
        void logSession(int chunkWidth, int chunkHeight, std::ostream& logTarget);

        // progress logging
        void startProgressLogging();
        void resumeProgressLogging();
        void logProgress(int chunkIndex);
        void endProgressLogging();

        // image logging
        void logImage(std::shared_ptr<const cimg_library::CImg<float> > image);
        void logChunk(const ViewPlane& viewPlane, std::shared_ptr<const cimg_library::CImg<float> > chunk);

    private:
        std::ofstream progressLogTarget_; ///< File stream to log current render progress.

        static const char* sceneFileName_; ///< Default file name of scene description file.
        static const char* progressFileName_; ///< Default file name of progress file.
        static const char* imageFileName_; ///< Default file name of image file.

        bool pngFileFormat_; ///< Flag indicating whether files are to be stored as png image.
    };

} // end of namespace iiit

#endif // __SESSIONLOGGER_HPP__