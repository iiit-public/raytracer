// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file SessionParser.cpp
 *  \author Thomas Nuernberg
 *  \date 23.01.2015
 *  \brief Implementation of class SessionParser.
 *
 *  Implementation of class SessionParser used to extract session data from a
 *  <a HREF="http://www.json.org">JSON</a> description.
 */

#include "SessionParser.hpp"

#include <sstream>



namespace iiit
{

    /*! \brief Default constructor.
     */
    SessionParser::SessionParser()
        : chunkWidth_(0)
        , chunkHeight_(0)
    {
    }



    /*! \brief Default destructor.
     */
    SessionParser::~SessionParser()
    {
    }



    /*! \brief Parse session description.
     *  \param[in] sessionFile String containing session description.
     *  \return Returns TRUE if parsing was successful, else FALSE.
     *
     *  Parses session data from session description in <a HREF="http://www.json.org">JSON</a> format.
     */
    bool SessionParser::parse(const std::string& sessionFile)
    {
        std::istringstream stream(sessionFile, std::ios_base::in);
        return parse(stream);
    }



    /*! \brief Parse session description.
     *  \param[in] sessionFile Input stream of session description.
     *  \return Returns TRUE if parsing was successful, else FALSE.
     *
     *  Parses session data from session description in <a HREF="http://www.json.org">JSON</a> format.
     */
    bool SessionParser::parse(std::istream& sessionFile)
    {
        Json::Value root;
        Json::Reader reader;

        // read scene description
        bool parsingSuccessful = reader.parse(sessionFile, root);
        if (!parsingSuccessful)
        {
            std::cout << "Failed to parse configuration\n" << reader.getFormattedErrorMessages();
            return parsingSuccessful;
        }

        // set image file name
        imageFileName_ = root["image"].asString();

        // set progress file name
        progressFileName_ = root["progress"].asString();

        // set scene description file name
        sceneDescriptionFileName_ = root["sceneDescription"].asString();

        // set chunk size
        chunkWidth_ = root["chunkSize"][0].asInt();
        chunkHeight_ = root["chunkSize"][1].asInt();
        return true;
    }



    /*! \brief Get file name of image.
     *  \return Image file name.
     */
    std::string SessionParser::getImageFileName() const
    {
        return imageFileName_;
    }



    /*! \brief Get file name of progress file.
     *  \return Progress file name.
     */
    std::string SessionParser::getProgressFileName() const
    {
        return progressFileName_;
    }



    /*! \brief Get file name of scene file.
     *  \return Scene file name.
     */
    std::string SessionParser::getSceneDescriptionFileName() const
    {
        return sceneDescriptionFileName_;
    }



    /*! \brief Get pixel width of image chunks.
     *  \return Pixel width of image chunks.
     */
    int SessionParser::getChunkWidth() const
    {
        return chunkWidth_;
    }



    /*! \brief Get pixel height of image chunks.
     *  \return Pixel height of image chunks.
     */
    int SessionParser::getChunkHeight() const
    {
        return chunkHeight_;
    }

} // end of namespace iiit