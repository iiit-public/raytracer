// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Worker.hpp
 *  \author Thomas Nuernberg
 *  \date 12.12.2014
 *  \brief Definition of class Worker.
 *
 *  Definition of worker class Worker, to be used in a ThreadPool.
 */

#ifndef __WORKER_HPP__
#define __WORKER_HPP__

#include <atomic>



namespace iiit
{

    class ThreadPool; // forward declaration



    /*! \brief Class for a worker of a ThreadPool.
     */
    class Worker
    {
    public:
        Worker(ThreadPool& pool);
        ~Worker();

        void operator()();

    private:
        friend class ThreadPool;

        ThreadPool& pool_; ///< Pointer to managing ThreadPool
    };

} // end of namespace iiit

#endif // __WORKER_HPP__