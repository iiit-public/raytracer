// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Worker.cpp
 *  \author Thomas Nuernberg
 *  \date 12.12.2014
 *  \brief Implementation of class Worker.
 *
 *  Implementation of worker class Worker, to be used in a ThreadPool.
 */

#include "Worker.hpp"

#include <mutex>

#include "ThreadPool.hpp"
#include "Logger.hpp"



namespace iiit
{

    /*! \brief Default constructor.
     *  \param[in] pool
     */
    Worker::Worker(ThreadPool& pool)
        : pool_(pool)
    {

    }



    /*! \brief Default destructor.
     */
    Worker::~Worker()
    {

    }



    /*! \brief Main function to be executed in thread.
     *
     *  Method implements an infinite loop that checks for tasks in the queue of the ThreadPool and
     *  executes them. Function is only left, if the ThreadPool object is destroyed. 
     */
    void Worker::operator()()
    {
        std::function<void()> task;
        while (true)
        {
            {
                // aquire lock
                std::unique_lock<std::mutex> lock(pool_.queueMutex_);

                // look for a work item
                while (!pool_.pause_.load() && pool_.tasks_.empty())
                {
                    // if there are none notify main thread and wait for notification
                    ++(pool_.idleWorkers_);
                    LOG(LogLevel::LogInfo, "Thread " << std::this_thread::get_id() << " going to sleep. " << pool_.idleWorkers_.load() << " idle workers");
                    pool_.finishCondition_.notify_all();
                    pool_.workCondition_.wait(lock);
                    LOG(LogLevel::LogInfo, "Thread " << std::this_thread::get_id() << " waking up.");
                    --(pool_.idleWorkers_);
                }

                if (pool_.pause_.load())
                {
                    // exit if pool is stopped
                    return;
                }

                // get the task from the queue
                LOG(LogLevel::LogInfo, "Thread " << std::this_thread::get_id() << " doing work. " << pool_.idleWorkers_.load() << " idle workers");
                task = pool_.tasks_.front();
                pool_.tasks_.pop();

            } // release lock

            // execute task
            task();
        }
    }

} // end of namespace iiit