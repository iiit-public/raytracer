// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file ThreadPool.hpp
 *  \author Thomas Nuernberg
 *  \date 12.12.2014
 *  \brief Definition of class ThreadPool.
 *
 *  Definition of class ThreadPool, using Worker objects.
 */

#ifndef __THREADPOOL_HPP__
#define __THREADPOOL_HPP__

#include <thread>
#include <mutex>
#include <condition_variable>
#include <vector>
#include <queue>
#include <atomic>
#include <functional>

#include "Worker.hpp"
#include "Logger.hpp"



namespace iiit
{

    /*! \brief Class to manage a pool of workers executing a pool of tasks.
     * 
     *  Class manages a pool of workers of type Worker to run in individual threads and provides a
     *  queue of tasks to be executed by the workers. Tasks can be pushed into the queue with the
     *  enqueue() method. If there is an idle worker, the task will be executed immediately, otherwise
     *  the tasks are executed in a FIFO order.
     *
     *  If the ThreadPool object is to be destroyed, the threads are joined once the tasks in execution
     *  at that time are finished. This has to be considered when a ThreadPool object runs out of scope.
     *
     *  \b Example:
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
     *  #include "TreadPool.hpp"
     *
     *  // function to be executed in threads
     *  void foo()
     *  {
     *      ...
     *  }
     *
     *  int main()
     *  {
     *      // create thread pool
     *      ThreadPool pool(2);
     *
     *      // push task(s) in queue
     *      std::function<void()> task(&foo);
     *      pool.enqueue(task);
     *      ...
     *  }
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *
     *  If the task to be performed is a class method (e. g. bar::foo()), the std::bind function has to
     *  be used to mask the implicit object pointer argument.
     *
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
     *      // push task(s) in queue
     *      std::function<void()> task = std::bind(&bar::foo, this);
     *      pool.enqueue(task);
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *
     *  Use waitForWorkers() to prevent a ThreadPool object to run out of scope. This will suspend the
     *  main thread until all tasks in the queue are finished.
     */
    class ThreadPool
    {

    public:
        ThreadPool(size_t size);
        ~ThreadPool();

        template<class F> void enqueue(F f);
        void pause();
        void resume();
        bool isIdle();
        bool isEmpty();
        void waitForWorkers();

    private:
        friend class Worker;

        std::vector<std::thread> threads_; ///< Vector of threads.
        std::queue<std::function<void()> > tasks_; ///< Queue of tasks.

        std::mutex queueMutex_; ///< Mutex guarding queue access.
        std::mutex waitMutex_; ///< Mutex guarding the deletion of the ThreadPool while other threads are in waitForWorkers().
        std::condition_variable workCondition_; ///< Condition variable to signal new work items.
        std::condition_variable finishCondition_; ///< Condition variable to signal idle workers.

        std::atomic<bool> pause_; ///< Guarded boolean variable indicating the interruption of the ThreadPool.
        std::atomic<bool> shuttingDown_; ///< Guarded boolean variable indication the deletion of the ThreadPool.
        std::atomic<size_t> idleWorkers_; ///< Guarded variable of current number of idle workers.

        size_t size_; ///< Number of workers in pool. 
    };



    /*! \brief Push task into queue.
     *  \tparam F Type of task. Must be convertible to std::function<void()>.
     *  \param[in] f Function object to task.
     */
    template<class F> void ThreadPool::enqueue(F f)
    {
        {
            // acquire lock
            std::unique_lock<std::mutex> lock(queueMutex_);

            // add the task
            tasks_.push(std::function<void()>(f));
            LOG(LogLevel::LogInfo, "Putting work item in queue. Currently " << tasks_.size() << " items in queue.");

        } // release lock

        // wake up one thread
        workCondition_.notify_one();
    }

} // end of namespace iiit

#endif // __THREADPOOL_HPP__
