// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file SceneLogger.cpp
 *  \author Thomas Nuernberg
 *  \date 22.01.2015
 *  \brief Implementation of class SceneLogger.
 *
 *  Implementation of class SceneLogger used to log scene data in
 *  <a HREF="http://www.json.org">JSON</a> syntax.
 */

//#include "SceneLogger.hpp"
//
//// utilities
//#include "Utilities/Point3D.hpp"
//#include "Utilities/Vector3D.hpp"
//
//// geometric objects
//#include "Objects/Sphere.hpp"
//#include "Objects/Plane.hpp"
//#include "Objects/Triangle.hpp"
//
//// cameras
//#include "Cameras/Orthographic.hpp"
//#include "Cameras/Pinhole.hpp"
//#include "Cameras/ThinLens.hpp"
//#include "Cameras/ApertureCamera.hpp"
//
//// samplers
//#include "Sampler/Regular.hpp"
//#include "Sampler/PureRandom.hpp"
//#include "Sampler/Jittered.hpp"
//
//
//
///*! \brief Default constructor.
// */
//SceneLogger::SceneLogger()
//{
//}
//
//
//
///*! \brief Default destructor.
// */
//SceneLogger::~SceneLogger()
//{
//}
//
//
//
///*! \brief Write world data to <a HREF="http://www.json.org">JSON</a> file.
// *  \param[in] scene Scene data to log.
// *  \param[in] logTarget Output stream to log file.
// */
//void SceneLogger::log(std::shared_ptr<const Scene> scene, std::ostream& logTarget) const
//{
//    Json::Value root;
//
//    // build JSON object of geometry
//    root["geometry"]["objects"];
//    for (std::vector<std::shared_ptr<GeometricObject> >::const_iterator it = scene->objects_.begin(); it  != scene->objects_.end(); ++it)
//    {
//        Json::Value object;
//
//        // build specific object
//        switch ((*it)->getType())
//        {
//        case GeometricObject::Sphere:
//        {
//            // build sphere
//            std::shared_ptr<Sphere> sphere = std::static_pointer_cast<Sphere>(*it);
//            object["type"] = "sphere";
//            Json::Value properties;
//            properties["radius"] = sphere->getRadius();
//            properties["position"].append(sphere->getCenter().x_);
//            properties["position"].append(sphere->getCenter().y_);
//            properties["position"].append(sphere->getCenter().z_);
//            object["properties"] = properties;
//            break;
//        }
//        case GeometricObject::Plane:
//        {
//            // build plane
//            std::shared_ptr<Plane> plane = std::static_pointer_cast<Plane>(*it);
//            object["type"] = "plane";
//            Json::Value properties;
//            properties["point"].append(plane->getPoint().x_);
//            properties["point"].append(plane->getPoint().y_);
//            properties["point"].append(plane->getPoint().z_);
//            properties["normal"].append(plane->getNormal().x_);
//            properties["normal"].append(plane->getNormal().y_);
//            properties["normal"].append(plane->getNormal().z_);
//            object["properties"] = properties;
//            break;
//        }
//        case GeometricObject::Triangle:
//        {
//            // build triangle
//            std::shared_ptr<Triangle> triangle = std::static_pointer_cast<Triangle>(*it);
//            object["type"] = "triangle";
//            Json::Value properties;
//            properties["v0"].append(triangle->vertex0_.x_);
//            properties["v0"].append(triangle->vertex0_.y_);
//            properties["v0"].append(triangle->vertex0_.z_);
//            properties["v1"].append(triangle->vertex1_.x_);
//            properties["v1"].append(triangle->vertex1_.y_);
//            properties["v1"].append(triangle->vertex1_.z_);
//            properties["v2"].append(triangle->vertex2_.x_);
//            properties["v2"].append(triangle->vertex2_.y_);
//            properties["v2"].append(triangle->vertex2_.z_);
//            object["properties"] = properties;
//            break;
//        }
//        default:
//            break;
//        }
//        root["geometry"]["objects"].append(object);
//    }
//    root["geometry"]["backgroundColor"].append(scene->backgroundColor_.r_);
//    root["geometry"]["backgroundColor"].append(scene->backgroundColor_.g_);
//    root["geometry"]["backgroundColor"].append(scene->backgroundColor_.b_);
//
//
//    // build json object of camera
//    Json::Value camera;
//    camera["eye"].append(scene->getCamera().lock()->getEye().x_);
//    camera["eye"].append(scene->getCamera().lock()->getEye().y_);
//    camera["eye"].append(scene->getCamera().lock()->getEye().z_);
//    camera["lookAt"].append(scene->getCamera().lock()->getLookAt().x_);
//    camera["lookAt"].append(scene->getCamera().lock()->getLookAt().y_);
//    camera["lookAt"].append(scene->getCamera().lock()->getLookAt().z_);
//    camera["up"].append(scene->getCamera().lock()->getUp().x_);
//    camera["up"].append(scene->getCamera().lock()->getUp().y_);
//    camera["up"].append(scene->getCamera().lock()->getUp().z_);
//    switch (scene->getCamera().lock()->getType())
//    {
//    case Camera::Orthographic:
//    {
//        camera["type"] = "Orthographic";
//        break;
//    }
//    case Camera::Pinhole:
//    {
//        std::weak_ptr<Pinhole> pinhole = std::static_pointer_cast<Pinhole>(scene->getCamera().lock());
//        camera["type"] = "Pinhole";
//        camera["focalLength"] = pinhole.lock()->getFocalLength();
//        break;
//    }
//    case Camera::ThinLens:
//    {
//        std::shared_ptr<ThinLens> thinLens = std::static_pointer_cast<ThinLens>(scene->getCamera().lock());
//        camera["type"] = "ThinLens";
//        camera["lensRadius"] = thinLens->getLensRadius();
//        camera["imageDistance"] = thinLens->getImageDistance();
//        camera["focusDistance"] = thinLens->getFocusDistance();
//        camera["zoom"] = thinLens->getZoom();
//
//        // log sampler
//        Json::Value sampler;
//        logSampler(thinLens->getSampler(), sampler);
//        camera["sampler"] = sampler;
//    }
//    case Camera::ApertureCamera:
//    {
//        std::shared_ptr<ApertureCamera> thinLens = std::static_pointer_cast<ApertureCamera>(scene->getCamera().lock());
//        camera["type"] = "ApertureCamera";
//        camera["lensRadius"] = thinLens->getLensRadius();
//        camera["imageDistance"] = thinLens->getImageDistance();
//        camera["focusDistance"] = thinLens->getFocusDistance();
//        camera["zoom"] = thinLens->getZoom();
//
//        // log sampler
//        Json::Value sampler;
//        logSampler(thinLens->getSampler(), sampler);
//        camera["sampler"] = sampler;
//    }
//    default:
//        break;
//    }
//    Json::Value viewPlane;
//    viewPlane["resolution"].append(scene->viewPlane_.uRes_);
//    viewPlane["resolution"].append(scene->viewPlane_.vRes_);
//    viewPlane["pixelSize"] = scene->viewPlane_.s_;
//
//    // log sampler
//    Json::Value sampler;
//    logSampler(scene->viewPlane_.sampler_, sampler);
//    viewPlane["sampler"] = sampler;
//    camera["sampler"] = sampler;
//    //viewPlane["gamma"] = world.viewPlane_.gamma_;
//    camera["viewPlane"] = viewPlane;
//    root["camera"] = camera;
//
//    // write JSON file
//    Json::StyledStreamWriter writer;
//
//    writer.write(logTarget, root);
//}
//
//
//
///*! \brief Log data of sampling object.
// *  \param[in] sampler Sampling object.
// *  \param[out] samplerDescription <a HREF="http://www.json.org">JSON</a> description of sampler
// *  object.
// */
//void SceneLogger::logSampler(std::shared_ptr<const Sampler> sampler, Json::Value& samplerDescription) const
//{
//    switch (sampler->getType())
//    {
//    case Sampler::Regular:
//    {
//        samplerDescription["type"] = "Regular";
//        break;
//    }
//    case Sampler::PureRandom:
//    {
//        samplerDescription["type"] = "PureRandom";
//        break;
//    }
//    case Sampler::Jittered:
//    {
//        samplerDescription["type"] = "Jittered";
//        break;
//    }
//    default:
//        break;
//    }
//    samplerDescription["numSamples"] = sampler->getNumSamples();
//    samplerDescription["numSets"] = sampler->getNumSets();
//}