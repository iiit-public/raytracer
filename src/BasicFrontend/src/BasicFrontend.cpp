// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file BasicFrontend.cpp
 *  \author Thomas Nuernberg
 *  \date 06.01.2015
 *  \brief Implementation of class BasicFrontend.
 *
 *  Implementation of class BasicFrontend to run the ray tracer from console.
 */

#include "BasicFrontend.hpp"

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <functional>
#include <new> // std::bad_alloc
#include <ctime>
#include <cstdlib>

#include "ThreadPool.hpp"
#include "Logger.hpp"
#include "FileLogTarget.hpp"



namespace iiit
{

    /*! \brief Default constructor.
     */ 
    BasicFrontend::BasicFrontend()
        : imageMagick_(false), pngFormat_(false), pfmFormat_(false)
    {
        cimg_library::cimg::exception_mode(0); // enable quiet exception mode
    }



    /*! \brief Default destructor.
     */ 
    BasicFrontend::~BasicFrontend()
    {
    }


    /*! \brief Function to get imageMagick member value.
     */
    bool BasicFrontend::getImageMagick()
    {
        return imageMagick_;
    }


    /*! \brief Function to get PfmFormat member value.
     */
    bool BasicFrontend::getPfmFormat()
    {
        return pfmFormat_;
    }


    /*! \brief Function to get PngFormat member value.
     */
    bool BasicFrontend::getPngFormat()
    {
        return pngFormat_;
    }



    /*! \brief Builds a RayTracer object and invokes the rendering of a scene.
     */ 
    bool BasicFrontend::run(int argc, char *argv[])
    {
        // get scene description file
        if (argc < 2)
        {
            std::cout << "Usage" << std::endl << std::endl << "  basicfrontend <path-to-scene-description> [-p #] [-w #] [-h #] [-o <filename>] [-i <ENVI interleave mode>] [-d #] [-n]" << std::endl << std::endl;
            std::cout << "The parameters in [] are optional:" << std::endl;
            std::cout << "    -p  Specify number of parallel worker threads. Default: 1." << std::endl;
            std::cout << "    -w  Specify width of chunks in pixels. Default: 128." << std::endl;
            std::cout << "    -h  Specify height of chunks in pixels. Default: 128." << std::endl;
            std::cout << "    -o  Specify output file name. A 4 digit number is appended automatically. Default: RayTracer_Image_####.png." << std::endl;
            std::cout << "    -i  Optional. Specify the interleave modus when saving hyperspectral ENVI files. Either bil, bip or bsq. Default: bil." << std::endl;
            std::cout << "    -d  Optional. Specify the data type when saving hyperspectral ENVI files. Either uint8, uint16 or float32. Default: float32." << std::endl;
            std::cout << "    -n  Don't append four digit number at the end of the file name of the output image." << std::endl;
            return true;
        }
        std::string sceneDescriptionFileName(argv[1]);

        // default parameters
        bool multithreading = false;
        int numberOfThreads = 1;
        int chunkWidth = 128;
        int chunkHeight = 128;
        omitNumber_ = false;
        fileName_ = std::string("RayTracer_Image.png"); // default file name
        enviInterleave_ = std::string("bil"); // default ENVI interleave modus
        enviDataformat_ = 4; // default: float32

        if (argc > 2)
        {
            for (int i = 2; i < argc; ++i)
            {
                std::string option = argv[i];
                if (option == "-p")
                {
                    multithreading = true;
                    if (argc > i + 1)
                    {
                        try
                        {
                            numberOfThreads = std::min(static_cast<unsigned int>(std::stoi(std::string(argv[++i]))), std::thread::hardware_concurrency());
                        }
                        catch (std::invalid_argument&)
                        {
                            --i;
                            numberOfThreads = std::thread::hardware_concurrency();
                        }
                    }
                    else
                    {
                        numberOfThreads = std::thread::hardware_concurrency();
                    }
                }
                else if (option == "-w" && argc > i + 1)
                {
                    try
                    {
                        chunkWidth = std::max(std::stoi(std::string(argv[++i])), 1);
                    }
                    catch (std::invalid_argument&)
                    {
                        std::cout << "Could not parse arguments!" << std::endl;
                        return false;
                    }
                }
                else if (option == "-h" && argc > i + 1)
                {
                    try
                    {
                        chunkHeight = std::max(std::stoi(std::string(argv[++i])), 1);
                    }
                    catch (std::invalid_argument&)
                    {
                        std::cout << "Could not parse arguments!" << std::endl;
                        return false;
                    }                
                }
                else if (option == "-o" && argc > i + 1)
                {
                    fileName_ = std::string(argv[++i]);
                }
                else if (option == "-i" && argc > i + 1)
                {
                    std::string tmpString = std::string(argv[++i]);
                    if (tmpString == "bil" || tmpString == "bsq" || tmpString == "bip")
                    {
                        enviInterleave_ = tmpString;
                    }
                    else
                    {
                        std::cout << "Could not parse  -i argument. Specified ENVI interleave is not allowed. Use bil, bip or bsq. Exiting." << std::endl;
                        return false;
                    }
                }
                else if (option == "-d" && argc > i + 1)
                {
                    // For ENVI data format number, see https://www.harrisgeospatial.com/docs/enviheaderfiles.html

                    std::string tmpString = std::string(argv[++i]);
                    if (tmpString == "uint8")
                    {

                        enviDataformat_ = 1;
                    }

                    else if (tmpString == "uint16")
                    {
                        enviDataformat_ = 12;
                    }

                    else if (tmpString == "float32")
                    {
                        enviDataformat_ = 4;
                    }

                    else
                    {
                        std::cout << "Could not parse  -d argument. Specified ENVI data format is not allowed. Use uint8, uint16 or float32. Exiting." << std::endl;
                        return false;
                    }
                }
                else if (option == "-n")
                {
                    omitNumber_ = true;
                }
                else
                {
                    std::cout << "Could not parse arguments!" << std::endl;
                    return false;
                }
            }
        }


        // check for file i/o capabilities
        std::FILE *file = 0;

        if ((file = std::fopen(cimg_library::cimg::imagemagick_path(), "r")) != 0)
        {
            std::fclose(file);
            imageMagick_ = true;
        }
        else if ((file = std::fopen("/usr/bin/convert", "r")) != 0 ||
                 (file = std::fopen("/usr/local/bin/convert", "r")) != 0) // check for standard linux locations explicitly
        {
             std::fclose(file);
             imageMagick_ = true;
        }
        else // no imagemagick found
        {
            imageMagick_ = false;
        }


        // setup logging
        if (LOG_MAX_LEVEL)
        {
            FILE* textFile = fopen("raytracer.log", "w"); // write mode -> existing log file gets replaced
            FileLogTarget::stream() = textFile;
        }
        else
        {
            Logger<FileLogTarget>::reportingLevel() = LogLevel::LogNone;
        }


        // append index to file name
        int idx0 = static_cast<int>(std::max(fileName_.rfind("/"), fileName_.rfind("\\")));
        int idx1 = static_cast<int>(fileName_.rfind("."));

        std::string path = fileName_.substr(0, idx0 + 1);
        std::string name = fileName_.substr(idx0 + 1, idx1 - idx0 - 1);
        std::string extension("");
        if (idx1 != -1)
        {
            extension = fileName_.substr(idx1);
            if(imageMagick_)
            {
                if(extension == ".pfm")
                {
                    pfmFormat_ = true;
                }
                else if(extension == ".png")
                {
                    pngFormat_ = true;
                }
            }
            // ELSE: Use default.

        }

        fileName_ = path + name;


        if (isAvailable(fileName_))
        {
            // Save filename to metadata to be accessable by SceneParser
            metaData_.fileName_ = fileName_;

            // Init Scene, calls SceneParser
            if (!initScene(sceneDescriptionFileName))
            {
                return false;
            }

            if (multithreading)
            {
                if (!metaData_.supportsMultiThreading_)
                {
                    std::cout << "Current scene doesn't support multithreading." << std::endl;
                    return false;
                }

                // Create baseImage that the threads load and write to. Depends on number of color channels
                int numChannels = this->scene_->getNumChannels();

                if (numChannels == 1 || numChannels == 3)
                {
                    if (pfmFormat_)
                    {
                        std::string pfmName = fileName_ + ".pfm";
                        std::shared_ptr<const cimg_library::CImg<float> > baseImage (new const cimg_library::CImg<float>(scene_->viewPlane_.uAbsRes_, scene_->viewPlane_.vAbsRes_, 1, 1));

                        savePfm(pfmName.c_str(), baseImage);
                    }

                    else if (pngFormat_)
                    {
                        std::string pngName = fileName_ + ".png";
                        cimg_library::CImg<unsigned short> baseImage(scene_->viewPlane_.uAbsRes_, scene_->viewPlane_.vAbsRes_, 1, 3);

                        cimg_forXYC(baseImage, x, y, v)
                        {
                            // this ensures, that the initial image is stored as color image
                            baseImage(x, y, v) = ((x + y) * (v + 1) * HIGH_COLOR_MAX) / ((scene_->viewPlane_.uAbsRes_ + scene_->viewPlane_.vAbsRes_ - 2) * 3);
                        }
                        baseImage.save_png(pngName.c_str(),4);
                    }
                    else
                    {
                        std::string bmpName = fileName_ + ".bmp";
                        cimg_library::CImg<unsigned char> baseImage(scene_->viewPlane_.uAbsRes_, scene_->viewPlane_.vAbsRes_, 1, 3);

                        cimg_forXYC(baseImage, x, y, v)
                        {
                            // this ensures, that the initial image is stored as color image
                            baseImage(x, y, v) = ((x + y) * (v + 1) * TRUE_COLOR_MAX) / ((scene_->viewPlane_.uAbsRes_ + scene_->viewPlane_.vAbsRes_ - 2) * 3);
                        }
                        baseImage.save_bmp(bmpName.c_str());
                    }
                }
                else if (numChannels == iiit::numSpectralSamples)
                {
                    std::string imgName = fileName_ + ".img";
                    std::string hdrName = fileName_ + ".hdr";

                    //initialize base image and header file, init image with zeros
                    std::shared_ptr<const cimg_library::CImg<float> > baseImage (new const cimg_library::CImg<float>(scene_->viewPlane_.uAbsRes_, scene_->viewPlane_.vAbsRes_, 1, iiit::numSpectralSamples, 0.f));

                    saveEnviHdr(hdrName,baseImage);
                    saveEnviImg(imgName,baseImage);
                }

                // partition ViewPlane defining several render tasks
                std::vector<ViewPlane> viewPlanes = createChunks(
                    scene_->viewPlane_,
                    chunkWidth,
                    chunkHeight,
                    metaData_.uMin_,
                    metaData_.uMax_,
                    metaData_.vMin_,
                    metaData_.vMax_);

                std::vector<std::shared_ptr<RayTracer> > rayTracers_;

                // create RayTracer for each ViewPlane
                try
                {
                    for (std::vector<ViewPlane>::const_iterator it = viewPlanes.begin(); it != viewPlanes.end(); ++it)
                    {
                        std::shared_ptr<AbstractScene> scene(scene_->clone());
                        scene->viewPlane_ = *it;
                        rayTracers_.push_back(std::shared_ptr<RayTracer>(new RayTracer(scene, std::bind(&BasicFrontend::updateChunk, this, std::placeholders::_1, std::placeholders::_2))));
                    }
                }
                catch (std::bad_alloc& ba)
                {
                    std::cout << "bad_alloc exception caught: " << ba.what() << std::endl;
                    std::cout << "Possible reasons:" << std::endl;
                    std::cout << "  - There is not enough memory." << std::endl;
                    std::cout << "  - The memory is fragmented." << std::endl;
                    std::cout << "  - In case of a 32 bit build: More than 4 GB memory are required." << std::endl;
                    return false;
                }

                // create worker pool
                pool_ = std::unique_ptr<ThreadPool>(new ThreadPool(numberOfThreads));

                // fill task queue
                for (std::vector<std::shared_ptr<RayTracer> >::iterator it = rayTracers_.begin(); it != rayTracers_.end(); ++it)
                {
                    std::function<void()> task = std::bind(&RayTracer::run, *it);
                    pool_->enqueue(task);

                    // use this code for debugging multithreaded rendering
                    //it->get()->run();
                }

                // wait for queue to empty
                pool_->waitForWorkers();

                // delete RayTracers
                rayTracers_.clear();

                // remove last scene reference
                scene_.reset();

                // reset ThreadPool
                pool_.reset();

            } // ENDIF multithreading
            else
            {
                // select area to render
                scene_->viewPlane_.uRes_ = metaData_.uMax_ - metaData_.uMin_ + 1;
                scene_->viewPlane_.uOffset_ = metaData_.uMin_;
                scene_->viewPlane_.vRes_ = metaData_.vMax_ - metaData_.vMin_ + 1;
                scene_->viewPlane_.vOffset_ = metaData_.vMin_;

                // create RayTracer object
                rayTracer_.push_back(std::shared_ptr<RayTracer>(new RayTracer(scene_, std::bind(&BasicFrontend::updateImage, this, std::placeholders::_1, std::placeholders::_2))));

                // render
                rayTracer_[0]->run();

                // delete RayTracers
                rayTracer_.clear();

                // remove last scene reference
                scene_.reset();

                // reset ThreadPool
                pool_.reset();
            }

        } // ENDIF isAvailable(fileName_)
        else
        {
            std::cout << "ERROR: File could not be saved. Move/Delete previous results." << std::endl;
            return false;
        }

        return true;
    }



    /*! \brief Builds Scene object from scene description file.
     *  \param[in] sceneDescriptionFileName Name of scene description file.
     *  \return Return \c true if building was successful, else \c false.
     */
    bool BasicFrontend::initScene(const std::string& sceneDescriptionFileName)
    {
        // create new Scene pointer
        std::shared_ptr<AbstractScene> scene;
        
        // parse data
        SceneParser sceneParser;
        if (!sceneParser.parse(sceneDescriptionFileName.c_str(), scene, metaData_))
        {
            return false;
        }

        // keep new scene data
        scene_ = scene;

        return true;
    }



    /*! \brief Partitions a ViewPlane to chunks.
     *  \param[in] viewPlane ViewPlane to partition.
     *  \param[in] width Desired width of partitions.
     *  \param[in] height Desired height of partitions.
     *  \param[in] uMin Minimum u-coordinate to render.
     *  \param[in] uMax Maximum u-coordinate to render.
     *  \param[in] vMin Minimum v-coordinate to render.
     *  \param[in] vMax Maximum v-coordinate to render.
     *  \return Return vector of ViewPlane objects of partitions.
     *
     *  Partitions the given ViewPlane object according to the desired chunk width and height. The
     *  resulting ViewPlanes are either of the specified size or smaller. The offset coordinates of the
     *  ViewPlanes are updated enabling the calculation of absolute image coordinates.
     */
    std::vector<ViewPlane> BasicFrontend::createChunks(const ViewPlane& viewPlane, int width, int height, int uMin, int uMax, int vMin, int vMax)
    {
        std::vector<ViewPlane> viewPlanePartitions;
        if (width == 0 || height == 0)
        {
            ViewPlane viewPlanePartition = viewPlane;
            viewPlanePartition.uRes_ = uMax - uMin + 1;
            viewPlanePartition.uAbsRes_ = viewPlane.uAbsRes_;
            viewPlanePartition.uOffset_ = uMin;
            viewPlanePartition.vRes_ = vMax - vMin + 1;
            viewPlanePartition.vAbsRes_ = viewPlane.vAbsRes_;
            viewPlanePartition.vOffset_ = vMin;

            viewPlanePartitions.push_back(viewPlanePartition);
        }
        else
        {
            // partition ViewPlane
            for (int v = vMin; v <= vMax; v += height)
            {
                for (int u = uMin; u <= uMax; u += width)
                {
                    ViewPlane viewPlanePartition = viewPlane;
                    viewPlanePartition.uRes_ = std::min<int>(width, uMax - u + 1);
                    viewPlanePartition.uAbsRes_ = viewPlane.uAbsRes_;
                    viewPlanePartition.uOffset_ = viewPlane.uOffset_ + u;
                    viewPlanePartition.vRes_ = std::min<int>(height, vMax - v + 1);
                    viewPlanePartition.vAbsRes_ = viewPlane.vAbsRes_;
                    viewPlanePartition.vOffset_ = viewPlane.vOffset_ + v;

                    viewPlanePartitions.push_back(viewPlanePartition);
                }
            }
        }
        return viewPlanePartitions;
    }



    /*! \brief Saves updated image.
     *  \param[in] viewPlane ViewPlane corresponding to the image.
     *  \param[in] floatImage Updated image.
     *
     *  Saves the updated image to the current directory in a format corresponding to the \c ImageType
     *  of the sensor. \c Raw and \c RGB images are saved in \c .bmp or \c .png format, depending on
     *  whether \c ImageMagick is installed or not. \c MultiSpectral images are stored in the ENVI file
     *  format, consisting of a \c .hdr file and a \c .img data file.
     */
    void BasicFrontend::updateImage(const ViewPlane& viewPlane, std::shared_ptr<const cimg_library::CImg<float> > floatImage)
    {
        // Distinguish save procedure by number of color channels
        int numChannels = this->scene_->getNumChannels();

        if (numChannels == 1)
        {
            if (pfmFormat_)
            {
                std::string pfmName = fileName_ + ".pfm";
                this->savePfm(pfmName, floatImage);
            }
            else if (pngFormat_)
            {
                std::string pngName = fileName_ + ".png";
                this->saveMonoPng(pngName,floatImage);
            }
            else
            {
                std::string bmpName = fileName_ + ".bmp";
                this->saveMonoBmp(bmpName,floatImage);
            }
        }
        if (numChannels == 3)
        {
            if (pfmFormat_)
            {
                std::string pfmName = fileName_ + ".pfm";
                this->savePfm(pfmName, floatImage);
            }
            else if (pngFormat_)
            {
                std::string pngName = fileName_ + ".png";
                this->savePng(pngName,floatImage);
            }
            else
            {
                std::string bmpName = fileName_ + ".bmp";
                this->saveBmp(bmpName,floatImage);
            }
        }
        if (numChannels == iiit::numSpectralSamples)
        {
            std::string imgName = fileName_ + ".img";
            std::string hdrName = fileName_ + ".hdr";
            this->saveEnviHdr(hdrName, floatImage);
            this->saveEnviImg(imgName, floatImage);
        }

        return;
    }



    /*! \brief Saves updated image chunk.
     *  \param[in] viewPlane ViewPlane corresponding to the image.
     *  \param[in] image Updated image.
     *
     *  Loads the base image and includes the rendered image chunk.
     */
    void BasicFrontend:: updateChunk(const ViewPlane& viewPlane, std::shared_ptr<const cimg_library::CImg<float> > floatImage)
    {

        // lock mutex
        imageMutex_.lock();


        // Distinguish save procedure by number of color channels
        int numChannels = this->scene_->getNumChannels();

        if (numChannels == 1)
        {
            if (pfmFormat_)
            {
                std::string pfmName = fileName_ + ".pfm";
                cimg_library::CImg<float> baseImage(pfmName.c_str()); //construct from pfm base image file.

                // iterate pixels, copy from floatImage to viewplane pixels of the baseImage
                for (int u = 0; u < floatImage->width(); ++u)
                {
                    for (int v = 0; v < floatImage->height(); ++v)
                    {
                        // copy pixel
                        baseImage(u + viewPlane.uOffset_, v + viewPlane.vOffset_, 0, 0) = floatImage->operator()(u, v, 0, 0);
                    }
                }

                //save updated image
                baseImage.save_pfm(pfmName.c_str());
            }
            else if (pngFormat_)
            {
                std::string pngName = fileName_ + ".png";
                cimg_library::CImg<unsigned short> baseImage(pngName.c_str()); //construct from PNG base image file.

                // ensure image is loaded as color image
                if (baseImage.spectrum() == 1)
                {
                    baseImage.resize(baseImage.width(), baseImage.height(), baseImage.depth(), 3, 1);
                }

                // iterate pixels, copy from floatImage to viewplane pixels of the baseImage
                for (int u = 0; u < floatImage->width(); ++u)
                {
                    for (int v = 0; v < floatImage->height(); ++v)
                    {
                        // copy pixel
                        baseImage(u + viewPlane.uOffset_, v + viewPlane.vOffset_, 0, 0) = static_cast<unsigned short>(floatImage->operator()(u, v, 0, 0) * HIGH_COLOR_MAX);
                        baseImage(u + viewPlane.uOffset_, v + viewPlane.vOffset_, 0, 1) = static_cast<unsigned short>(floatImage->operator()(u, v, 0, 0) * HIGH_COLOR_MAX);
                        baseImage(u + viewPlane.uOffset_, v + viewPlane.vOffset_, 0, 2) = static_cast<unsigned short>(floatImage->operator()(u, v, 0, 0) * HIGH_COLOR_MAX);
                    }
                }

                //save updated image
                baseImage.save_png(pngName.c_str(),4);
            }
            else
            {
                std::string bmpName = fileName_ + ".bmp";
                cimg_library::CImg<unsigned char> baseImage(bmpName.c_str()); //construct from BMP base image file

                // ensure image is loaded as color image
                if (baseImage.spectrum() == 1)
                {
                    baseImage.resize(baseImage.width(), baseImage.height(), baseImage.depth(), 3, 1);
                }

                // iterate pixels
                for (int u = 0; u < floatImage->width(); ++u)
                {
                    for (int v = 0; v < floatImage->height(); ++v)
                    {
                        // copy pixel
                        baseImage(u + viewPlane.uOffset_, v + viewPlane.vOffset_, 0, 0) = static_cast<unsigned char>(floatImage->operator()(u, v, 0, 0) * TRUE_COLOR_MAX);
                        baseImage(u + viewPlane.uOffset_, v + viewPlane.vOffset_, 0, 1) = static_cast<unsigned char>(floatImage->operator()(u, v, 0, 0) * TRUE_COLOR_MAX);
                        baseImage(u + viewPlane.uOffset_, v + viewPlane.vOffset_, 0, 2) = static_cast<unsigned char>(floatImage->operator()(u, v, 0, 0) * TRUE_COLOR_MAX);
                    }
                }
                baseImage.save_bmp(bmpName.c_str());
            }
        }
        else if (numChannels == 3)
        {
            if (pfmFormat_)
            {
                std::string pfmName = fileName_ + ".pfm";
                cimg_library::CImg<float> baseImage(pfmName.c_str()); //construct from pfm base image file.

                // ensure image is loaded as color image
                if (baseImage.spectrum() == 1)
                {
                    baseImage.resize(baseImage.width(), baseImage.height(), baseImage.depth(), 3, 1);
                }

                // iterate pixels, copy from floatImage to viewplane pixels of the baseImage
                for (int u = 0; u < floatImage->width(); ++u)
                {
                    for (int v = 0; v < floatImage->height(); ++v)
                    {
                        // copy pixel
                        baseImage(u + viewPlane.uOffset_, v + viewPlane.vOffset_, 0, 0) = floatImage->operator()(u, v, 0, 0);
                        baseImage(u + viewPlane.uOffset_, v + viewPlane.vOffset_, 0, 1) = floatImage->operator()(u, v, 0, 1);
                        baseImage(u + viewPlane.uOffset_, v + viewPlane.vOffset_, 0, 2) = floatImage->operator()(u, v, 0, 2);
                    }
                }

                //save updated image
                baseImage.save_pfm(pfmName.c_str());
            }

            if (pngFormat_)
            {
                std::string pngName = fileName_ + ".png";
                cimg_library::CImg<unsigned short> baseImage(pngName.c_str()); //construct from PNG base image file.

                // ensure image is loaded as color image
                if (baseImage.spectrum() == 1)
                {
                    baseImage.resize(baseImage.width(), baseImage.height(), baseImage.depth(), 3, 1);
                }

                // iterate pixels, copy from floatImage to viewplane pixels of the baseImage
                for (int u = 0; u < floatImage->width(); ++u)
                {
                    for (int v = 0; v < floatImage->height(); ++v)
                    {
                        // copy pixel
                        baseImage(u + viewPlane.uOffset_, v + viewPlane.vOffset_, 0, 0) = static_cast<unsigned short>(floatImage->operator()(u, v, 0, 0) * HIGH_COLOR_MAX);
                        baseImage(u + viewPlane.uOffset_, v + viewPlane.vOffset_, 0, 1) = static_cast<unsigned short>(floatImage->operator()(u, v, 0, 1) * HIGH_COLOR_MAX);
                        baseImage(u + viewPlane.uOffset_, v + viewPlane.vOffset_, 0, 2) = static_cast<unsigned short>(floatImage->operator()(u, v, 0, 2) * HIGH_COLOR_MAX);
                    }
                }

                //save updated image
                baseImage.save_png(pngName.c_str(),4);
            }
            else
            {
                std::string bmpName = fileName_ + ".bmp";
                cimg_library::CImg<unsigned char> baseImage(bmpName.c_str()); //construct from BMP base image file

                // ensure image is loaded as color image
                if (baseImage.spectrum() == 1)
                {
                    baseImage.resize(baseImage.width(), baseImage.height(), baseImage.depth(), 3, 1);
                }

                // iterate pixels
                for (int u = 0; u < floatImage->width(); ++u)
                {
                    for (int v = 0; v < floatImage->height(); ++v)
                    {
                        // copy pixel
                        baseImage(u + viewPlane.uOffset_, v + viewPlane.vOffset_, 0, 0) = static_cast<unsigned char>(floatImage->operator()(u, v, 0, 0) * TRUE_COLOR_MAX);
                        baseImage(u + viewPlane.uOffset_, v + viewPlane.vOffset_, 0, 1) = static_cast<unsigned char>(floatImage->operator()(u, v, 0, 1) * TRUE_COLOR_MAX);
                        baseImage(u + viewPlane.uOffset_, v + viewPlane.vOffset_, 0, 2) = static_cast<unsigned char>(floatImage->operator()(u, v, 0, 2) * TRUE_COLOR_MAX);
                    }
                }
                baseImage.save_bmp(bmpName.c_str());
            }
        }
        else if (numChannels == iiit::numSpectralSamples)
        {
            std::string imgName = fileName_ + ".img";

            std::fstream imgFile (imgName, std::ios::in | std::ios::out |std::ios::binary | std::ios::ate);
            std::streampos size;
            float* imgData;

            if (imgFile.is_open())
            {
                // File opened OK. Read data from file
                size = imgFile.tellg();                                         // size of the file (in bytes)
                long imgDataLength = 0;
                if(enviDataformat_ == 1) // uint8
                {
                    imgDataLength = static_cast<long>(size) / sizeof(uint8_t);  // number of stored uint8
                }
                else if(enviDataformat_ == 12) // uint16
                {
                    imgDataLength = static_cast<long>(size) / sizeof(uint16_t); // number of stored uint16
                }
                else if(enviDataformat_ == 4) // float32
                {
                    imgDataLength = static_cast<long>(size) / sizeof(float);    // number of stored floats
                }

                imgData = new float[imgDataLength];                             // allocate memory for float array

                imgFile.seekg(0, std::ios::beg);                                // set get pos to beginning

                if(enviDataformat_ == 1) // uint8
                {
                    // Read uint8, copy to float
                    uint8_t* imgDataTmp = new uint8_t[imgDataLength];
                    imgFile.read(reinterpret_cast<char *> (imgDataTmp), size) ;
                    for(int i = 0; i < imgDataLength; i++)
                    {
                        imgData[i] = float(imgDataTmp[i]) / float(UINT8_MAX);
                    }
                    delete[] imgDataTmp; //free allocated space
                }

                else if(enviDataformat_ == 12) // uint16
                {
                    // Read uint16, copy to float
                    uint16_t* imgDataTmp = new uint16_t[imgDataLength];
                    imgFile.read(reinterpret_cast<char *> (imgDataTmp), size) ;
                    for(int i = 0; i < imgDataLength; i++)
                    {
                        imgData[i] = float(imgDataTmp[i]) / float(UINT16_MAX);
                    }
                    delete[] imgDataTmp; //free allocated space
                }

                else if(enviDataformat_ == 4) // float32
                {
                    // Read float directly
                    imgFile.read(reinterpret_cast<char *> (imgData), size) ;
                }



                // Depending on interleave type, write to data
                if (enviInterleave_ == "bil")
                {
                    for (int v = 0; v < (floatImage->height()); ++v)
                    {
                        // iterate over spectral bands
                        for (int i = 0; i < (floatImage->spectrum()); ++i)
                        {
                            // iterate over line pixels
                            for(int u = 0; u < (floatImage->width()); ++u)
                            {
                                // convert 3D matrix to 1D array
                               imgData[(u + viewPlane.uOffset_) + (i * scene_->viewPlane_.uAbsRes_) + ((v + viewPlane.vOffset_) * scene_->viewPlane_.uAbsRes_ * iiit::numSpectralSamples)] = floatImage->operator()(u, v, i);
                            }
                        }
                    }
                }
                else if (enviInterleave_ == "bsq")
                {
                    for (int i = 0; i < (floatImage->spectrum()); ++i)
                    {
                        // iterate over spectral bands
                        for (int v = 0; v < (floatImage->height()); ++v)
                        {
                            // iterate over line pixels
                            for(int u = 0; u < (floatImage->width()); ++u)
                            {
                                // convert 3D matrix to 1D array
                               imgData[(u + viewPlane.uOffset_) + ((v + viewPlane.vOffset_) * scene_->viewPlane_.uAbsRes_) + (i * scene_->viewPlane_.uAbsRes_ * scene_->viewPlane_.vAbsRes_)] = floatImage->operator()(u, v, i);
                            }
                        }
                    }
                }
                else if (enviInterleave_ == "bip")
                {
                    // iterate over pixels
                    for (int v = 0; v < (floatImage->height()); ++v)
                    {
                        // iterate over line pixels
                        for(int u = 0; u < (floatImage->width()); ++u)
                        {
                            // iterate over spectrum
                            for (int i = 0; i < (floatImage->spectrum()); ++i)
                            {
                                // convert 3D matrix to 1D array
                               imgData[i + ((u + viewPlane.uOffset_) * iiit::numSpectralSamples) + ((v + viewPlane.vOffset_) * scene_->viewPlane_.uAbsRes_ * iiit::numSpectralSamples)] = floatImage->operator()(u, v, i);
                            }
                        }
                    }
                }

                imgFile.seekg (0, std::ios::beg);
                if(enviDataformat_ == 1) // uint8
                {
                    // Read uint8, copy to float
                    uint8_t* imgDataTmp = new uint8_t[imgDataLength];

                    // iterate over pixels, copy data
                    for (int i = 0; i < imgDataLength; i++)
                    {
                        imgDataTmp[i] = uint8_t(float(UINT8_MAX)*imgData[i]);
                    }
                    imgFile.write(reinterpret_cast<char *>(imgDataTmp), size);
                    delete[] imgDataTmp; //free allocated space
                }

                else if(enviDataformat_ == 12) // uint16
                {
                    // Read uint8, copy to float
                    uint16_t* imgDataTmp = new uint16_t[imgDataLength];

                    // iterate over pixels, copy data
                    for (int i = 0; i < imgDataLength; i++)
                    {
                        imgDataTmp[i] = uint16_t(float(UINT16_MAX)*imgData[i]);
                    }
                    imgFile.write(reinterpret_cast<char *>(imgDataTmp), size);
                    delete[] imgDataTmp; //free allocated space
                }

                else if(enviDataformat_ == 4) // float32
                {
                    // Read float directly
                    imgFile.write(reinterpret_cast<char *>(imgData), size);
                }
                imgFile.close();
                delete[] imgData; //free allocated space
            }
            else
            {
                std::cout << "ERROR: Unable to open ENVI file." << std::endl;
                return;
            }
        }

        // release mutex
        imageMutex_.unlock();

        return;
    }


    /*! \brief Saves ENVI data file
     *  \param[in] imgName Name of the image data file.
     *  \param[in] floatImage Hyperspectral image to be saved.
     *
     *  Creates and saves the image data file \c .img in the ENVI file format.
     *
     *  The ENVI image format is a flat-binary raster file with an accompanying ASCII header file.
     *  The data is stored as a binary stream of bytes in one of the following formats, referred to as
     *  the interleave type:
     *  \li Band Sequential: BSQ format is the simplest format, where each line of the data is followed
     *  immediately by the next line in the same spectral band. Hence, the image is saved one channel
     *  after another. This format is optimal for spatial access of any part of a single spectral band.
     *  \li Band-interleaved-by-pixel: BIP format stores the first pixel for all bands in sequential
     *  order, followed by the second pixel for all bands, followed by the third pixel for all bands,
     *  and so forth, interleaved up to the number of pixels. This format provides optimum performance
     *  for spectral access of the image data.
     *  \li Band-interleaved-by-line: BIL format stores the first line of the first band, followed by
     *  the first line of the second band, followed by the first line of the third band, interleaved up
     *  to the number of bands. Subsequent lines for each band are interleaved in similar fashion. This
     *  format provides a compromise in performance between spatial and spectral processing and is the
     *  recommended file format for most ENVI processing tasks.\n
     *
     *  For details on the specified header properties, see
     *  <a href="https://www.harrisgeospatial.com/docs/enviimagefiles.html" target="_blank">the official link</a>.
     */
    void BasicFrontend::saveEnviImg(const std::string &imgName, std::shared_ptr<const cimg_library::CImg<float> > floatImage)
    {
        // open data files for writing
        FILE* imgFile = fopen(imgName.c_str(), "wb"); //binary image data file


        // write data to .img file
        // default BIL format
        // first, collect data in a float array
        int imageDataSize = floatImage->width() * floatImage->height() * floatImage->spectrum();


        float* imageData = (float *) malloc(imageDataSize * sizeof(float));

        // check whether malloc was successful
        if (imageData == NULL)
        {
            printf("Error allocating memory!\n"); // print an error message
            return; // return with failure
        }


        // sort data according to enviInterleave_ specification

        if (enviInterleave_ == "bil")
        {
            for (int v = 0; v < (floatImage->height()); ++v)
            {
                // iterate over spectral bands
                for (int i = 0; i < (floatImage->spectrum()); ++i)
                {
                    // iterate over line pixels
                    for (int u = 0; u < (floatImage->width()); ++u)
                    {
                        // convert 3D matrix to 1D array
                       imageData[u + (i * floatImage->width()) + (v * floatImage->width() * floatImage->spectrum())] = floatImage->operator()(u, v, i);
                    }
                }
            }

        }

        else if (enviInterleave_ == "bsq")
        {
            for (int i = 0; i < (floatImage->spectrum()); ++i)
            {
                // iterate over spectral bands
                for (int v = 0; v < (floatImage->height()); ++v)
                {
                    // iterate over line pixels
                    for(int u = 0; u < (floatImage->width()); ++u)
                    {
                        // convert 3D matrix to 1D array
                       imageData[u + (v * floatImage->width()) + (i * floatImage->width() * floatImage->height())] = floatImage->operator()(u, v, i);
                    }
                }
            }
        }

        else if (enviInterleave_ == "bip")
        {
            // iterate over pixels
            for (int v = 0; v < (floatImage->height()); ++v)
            {
                // iterate over line pixels
                for(int u = 0; u < (floatImage->width()); ++u)
                {
                    // iterate over spectrum
                    for (int i = 0; i < (floatImage->spectrum()); ++i)
                    {
                        // convert 3D matrix to 1D array
                       imageData[i + (u * floatImage->spectrum()) + (v * floatImage->width() * floatImage->spectrum())] = floatImage->operator()(u, v, i);
                    }
                }
            }
        }

        // write data, close file, and free the allocated memory

        if(enviDataformat_ == 1) // uint8
        {
            std::cout << "Saving as uint8. Maximum value: " << UINT8_MAX << std::endl;
            uint8_t* array = (uint8_t *) malloc(imageDataSize * sizeof(uint8_t));

            // iterate over pixels, copy data
            for (int i = 0; i < imageDataSize; i++)
            {
                array[i] = uint8_t(float(UINT8_MAX)*imageData[i]);
            }
            fwrite(array, sizeof(uint8_t), imageDataSize, imgFile);
        }

        else if(enviDataformat_ == 12) // uint16
        {
            std::cout << "Saving as uint16. Maximum value: " << UINT16_MAX << std::endl;
            uint16_t* array = (uint16_t *) malloc(imageDataSize * sizeof(int16_t));

            // iterate over pixels, copy data
            for (int i = 0; i < imageDataSize; i++)
            {
                array[i] = uint16_t(float(UINT16_MAX)*imageData[i]);
            }
            fwrite(array, sizeof(uint16_t), imageDataSize, imgFile);
        }

        else if(enviDataformat_ == 4) // float32
        {
            std::cout << "Saving as float32." << std::endl;
            fwrite(imageData, sizeof(float), imageDataSize, imgFile);
        }

        //fwrite(imageData, sizeof(float), imageDataSize, imgFile);
        free(imageData);
        fclose(imgFile);

        return;
    }



    /*! \brief Saves ENVI header file
     *  \param[in] hdrName Name of the header file.
     *  \param[in] floatImage Hyperspectral image to be saved.
     *
     *  Creates and saves the header file \c .hdr for ENVI file format.
     *
     *  The ENVI image format is a flat-binary raster file with an accompanying ASCII header file.\n
     *  For details on the specified header properties, see the 
     *  <a href="https://www.harrisgeospatial.com/docs/ENVIHeaderFiles.html" target="_blank"> official link</a>.
     */
    void BasicFrontend::saveEnviHdr(const std::string &hdrName, std::shared_ptr<const cimg_library::CImg<float> > floatImage)
    {
        // open data files for writing
        FILE* hdrFile = fopen(hdrName.c_str(), "w");  // ascii header file


        // first write .hdr file

        // current date/time based on current system, in seconds since Jan 1, 1970
        std::time_t timeNow = time(0);

        // convert to string form
        std::string dateNow = ctime(&timeNow);
        dateNow.pop_back(); // get rid of newline \n

        std::string headerString;
        headerString  = "ENVI\nDescription = {\n\tRaytraced Image, Rendered " + dateNow + ".}\n";
        headerString += "samples = " + std::to_string( floatImage->width() )+ "\n";
        headerString += "lines = " + std::to_string( floatImage->height() )+ "\n";
        headerString += "bands = " + std::to_string(floatImage->spectrum()) + "\n";
        headerString += "header offset = 0\n";
        headerString += "file type = ENVI Standard\n";
        //headerString += "data type = 4\n"; // "4" for 32 bit float
        headerString += "data type = " + std::to_string(enviDataformat_) + "\n"; // "1" for 8bit unsigned int
        headerString += "interleave = " + enviInterleave_ + "\n"; // Refers to whether the data interleave is BSQ, BIL, or BIP.
        headerString += "sensor type = ";
        headerString += this->scene_->getSensorName();
        headerString += "\n";
        headerString += "byte order = 0\n"; // "0" for little endian (intel), "1" for big endian
        headerString += "wavelength = {\n\t";
        for (int i = 0; i < floatImage->spectrum(); ++i)
        {
            headerString += std::to_string( (float)iiit::sampledLambdaStart + (i + 0.5f) * (float)(iiit::sampledLambdaEnd - iiit::sampledLambdaStart)/(float)floatImage->spectrum() ) + ", ";
        }
        headerString.pop_back(); headerString.pop_back(); // delete last space and comma;
        headerString += "}\n";
        headerString += "wavelength units = Nanometers\n";

        // Calculating the FWHM:
        // This is somewhat arbitrary as the raytracer renders with idealized delta-spectra.
        // For FWHM value, we assume the spectrum to be normally distributed with a sigma,
        // such that the spectra bands only overlap by less then 0.01%, that is, we assume,
        // that the spectrum lies with 4sigma interval in the idealized bandwidth of the spectrum,
        // specified by the lambda interval and the number of spectral samples.

        float sigma = 0.25f*(iiit::sampledLambdaEnd - iiit::sampledLambdaStart)/(floatImage->spectrum());
        sigma /= 4.f; // here specify the wanted certainty, default: 4sigma
        float FWHM = 2.354820045f * sigma; //calculate FWHM for standard deviation, FWHM = 2 * sqrt(2 * ln 2) * sigma

        headerString += "fwhm = {\n\t";
        for (int i = 0; i < floatImage->spectrum(); ++i)
        {
            headerString += std::to_string( FWHM ) + ", ";
        }
        headerString.pop_back(); headerString.pop_back();//delete last space and comma;
        headerString += "}\n";


        //write header to file and close .hdr file
        fprintf(hdrFile, "%s", headerString.c_str());
        fclose(hdrFile);

        return;
    }



    /*! \brief Saves bmp image file
     *  \param[in] bmpName Name of the image file.
     *  \param[in] floatImage Image to be saved.
     *
     *  Saves the image in the \c .bmp file format.
     */
    void BasicFrontend::saveBmp(const std::string &bmpName, std::shared_ptr<const cimg_library::CImg<float> > floatImage)
    {
        std::shared_ptr<cimg_library::CImg<unsigned char> > image = std::make_shared<cimg_library::CImg<unsigned char> >(
            floatImage->width(), floatImage->height(), floatImage->depth(), 3);

        // iterate pixels
        for (int u = 0; u < image->width(); ++u)
        {
            for (int v = 0; v < image->height(); ++v)
            {
                // copy pixel
                image->operator()(u, v, 0) = static_cast<unsigned char>(floatImage->operator()(u, v, 0) * TRUE_COLOR_MAX);
                image->operator()(u, v, 1) = static_cast<unsigned char>(floatImage->operator()(u, v, 1) * TRUE_COLOR_MAX);
                image->operator()(u, v, 2) = static_cast<unsigned char>(floatImage->operator()(u, v, 2) * TRUE_COLOR_MAX);
            }
        }

        image->save_bmp(bmpName.c_str());
        // no need to close file explicitly, as this is done in save_bmp()

        return;
    }



    /*! \brief Saves monochromatic bmp image file
     *  \param[in] bmpName Name of the image file.
     *  \param[in] floatImage Image to be saved.
     *
     *  Saves the image in black and white in the \c .bmp file format.
     */
    void BasicFrontend::saveMonoBmp(const std::string &bmpName, std::shared_ptr<const cimg_library::CImg<float> > floatImage)
    {
        std::shared_ptr<cimg_library::CImg<unsigned char> > image = std::make_shared<cimg_library::CImg<unsigned char> >(
            floatImage->width(), floatImage->height(), floatImage->depth(), 3);

        // iterate pixels
        for (int u = 0; u < image->width(); ++u)
        {
            for (int v = 0; v < image->height(); ++v)
            {
                // copy pixel to every channel
                image->operator()(u, v, 0) = static_cast<unsigned char>(floatImage->operator()(u, v, 0) * TRUE_COLOR_MAX);
                image->operator()(u, v, 1) = static_cast<unsigned char>(floatImage->operator()(u, v, 0) * TRUE_COLOR_MAX);
                image->operator()(u, v, 2) = static_cast<unsigned char>(floatImage->operator()(u, v, 0) * TRUE_COLOR_MAX);
            }
        }
        image->save_bmp(bmpName.c_str());
        // no need to close file explicitly, as this is done in save_bmp()

        return;
    }



    /*! \brief Saves png image file
     *  \param[in] pngName Name of the image file.
     *  \param[in] floatImage Image to be saved.
     *
     *  Saves the image in the \c .png file format. Uses \c ImageMagick.
     */
    void BasicFrontend::savePng(const std::string &pngName, std::shared_ptr<const cimg_library::CImg<float> > floatImage)
    {
        std::shared_ptr<cimg_library::CImg<unsigned short> > image = std::make_shared<cimg_library::CImg<unsigned short> >(
            floatImage->width(), floatImage->height(), floatImage->depth(), 3);

        // iterate pixels
        for (int u = 0; u < image->width(); ++u)
        {
            for (int v = 0; v < image->height(); ++v)
            {
                // copy pixel
                image->operator()(u, v, 0) = static_cast<unsigned short>(floatImage->operator()(u, v, 0) * HIGH_COLOR_MAX);
                image->operator()(u, v, 1) = static_cast<unsigned short>(floatImage->operator()(u, v, 1) * HIGH_COLOR_MAX);
                image->operator()(u, v, 2) = static_cast<unsigned short>(floatImage->operator()(u, v, 2) * HIGH_COLOR_MAX);
            }
        }


        image->save_png(pngName.c_str(),4);
        // no need to close file explicitly, as this is done in save_bmp()

        return;
    }



    /*! \brief Saves monochromatic png image file
     *  \param[in] pngName Name of the image file.
     *  \param[in] floatImage Image to be saved.
     *
     *  Saves the image in black and white the \c .png file format. Uses \c ImageMagick.
     */
    void BasicFrontend::saveMonoPng(const std::string &pngName, std::shared_ptr<const cimg_library::CImg<float> > floatImage)
    {
        std::shared_ptr<cimg_library::CImg<unsigned short> > image = std::make_shared<cimg_library::CImg<unsigned short> >(
            floatImage->width(), floatImage->height(), floatImage->depth(), 3);

        // iterate pixels
        for (int u = 0; u < image->width(); ++u)
        {
            for (int v = 0; v < image->height(); ++v)
            {
                // copy pixel
                image->operator()(u, v, 0) = static_cast<unsigned short>(floatImage->operator()(u, v, 0) * HIGH_COLOR_MAX);
                image->operator()(u, v, 1) = static_cast<unsigned short>(floatImage->operator()(u, v, 0) * HIGH_COLOR_MAX);
                image->operator()(u, v, 2) = static_cast<unsigned short>(floatImage->operator()(u, v, 0) * HIGH_COLOR_MAX);
            }
        }


        image->save_png(pngName.c_str(),4);
        // no need to close file explicitly, as this is done in save_bmp()

        return;
    }



    /*! \brief Saves pfm image file (used to save properties like depth or normal)
     *  \param[in] pfmName Name of the image file.
     *  \param[in] floatImage Image to be saved.
     *
     *  Saves the image \c .pf, file format in 32bit float. Uses \c ImageMagick. Only works for Mono or 3-channel images.
     */
    void BasicFrontend::savePfm(const std::string &pfmName, std::shared_ptr<const cimg_library::CImg<float> > floatImage)
    {
        floatImage->save_pfm(pfmName.c_str());

        return;
    }



    /*! \brief Check if fileName exists.
     *  \param[in] fileName Filename to check.
     *
     *  Checks if Filename \c fileName exists, if necessary iterating over appended numbers from
     *  \c _0000 to \c _9999 if \c -n is not explicitly parsed as an argument to the \c BasicFrontend.
     *  If a free number is found, update \c fileName accordingly and return \c true. If all filenames
     *  are unavailable, return \c false.
     */
    bool BasicFrontend::isAvailable(std::string &fileName)
    {
        if (!omitNumber_)
        {
            fileName.append("_0000");
        }

        std::string tmpFileName=fileName;

        std::string pngName = tmpFileName + ".png";
        FILE* pngFile = fopen(pngName.c_str(), "r");

        std::string pfmName = tmpFileName + ".pfm";
        FILE* pfmFile = fopen(pfmName.c_str(), "r");

        std::string bmpName = tmpFileName + ".bmp";
        FILE* bmpFile = fopen(bmpName.c_str(), "r");

        // For ENVI file format
        std::string imgName = tmpFileName + ".img";
        FILE* imgFile = fopen(imgName.c_str(), "rb"); //binary image file
        std::string hdrName = tmpFileName + ".hdr";
        FILE* hdrFile = fopen(hdrName.c_str(), "r");

        // check if file(s) already exists
        char numStr[8];
        int num = 0;

        while ( ((pngFile || pfmFile || bmpFile || imgFile || hdrFile) && num < 10000) && !omitNumber_)
        {
            if (pngFile)
            {
                fclose(pngFile);
            }
            if (pfmFile)
            {
                fclose(pfmFile);
            }
            if (bmpFile)
            {
                fclose(bmpFile);
            }
            if (imgFile)
            {
                fclose(imgFile);
            }
            if (hdrFile)
            {
                fclose(hdrFile);
            }
            // replace digits in filename by next higher one
            sprintf(numStr, "%04u", ++num);
            // delete last four numerical characters of fileName_ and append new ones.
            tmpFileName.replace(tmpFileName.size() - 4, 4, numStr);

            pngName = tmpFileName + ".png";
            pngName = tmpFileName + ".pfm";
            bmpName = tmpFileName + ".bmp";
            imgName = tmpFileName + ".img";
            hdrName = tmpFileName + ".hdr";

            // open files with next numbered filename and check again
            pngFile = fopen(pngName.c_str(), "r");
            pfmFile = fopen(pfmName.c_str(), "r");
            bmpFile = fopen(bmpName.c_str(), "r");
            imgFile = fopen(imgName.c_str(), "r");
            hdrFile = fopen(hdrName.c_str(), "r");
        }


        if ((num >= 10000 && !omitNumber_) || (omitNumber_ && (pngFile || pfmFile|| bmpFile || imgFile || hdrFile)))
        {
            if (pngFile)
            {
                fclose(pngFile);
            }
            if (pfmFile)
            {
                fclose(pfmFile);
            }
            if (bmpFile)
            {
                fclose(bmpFile);
            }
            if (imgFile)
            {
                fclose(imgFile);
            }
            if (hdrFile)
            {
                fclose(hdrFile);
            }

            return false;
        }

        // success, filename is available, set and return 1
        fileName = tmpFileName;
        // images already existing
        if (pngFile)
        {
            fclose(pngFile);
        }
        if (pfmFile)
        {
            fclose(pfmFile);
        }
        if (bmpFile)
        {
            fclose(bmpFile);
        }
        if (imgFile)
        {
            fclose(imgFile);
        }
        if (hdrFile)
        {
            fclose(hdrFile);
        }
        return true;
      }

} // end of namespace iiit
