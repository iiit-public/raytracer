// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*!
 *  \file BasicFrontend.hpp
 *  \author Thomas Nuernberg
 *  \date 06.01.2015
 *  \brief Definition of class BasicFrontend.
 *
 *  Definition of class BasicFrontend to run the ray tracer from console.
 */

#ifndef __BASICFRONTEND_HPP__
#define __BASICFRONTEND_HPP__

#include <memory>
#include <vector>
#include <string>

#include "CImg.h"

#include "ThreadPool.hpp"
#include "SceneParser.hpp"

#include "RayTracer.hpp"
#include "AbstractScene.hpp"
#include "Cameras/ViewPlane.hpp"



namespace iiit
{

    /*! \brief Derived class for a basic frontend.
     *  
     *  This class owns the RayTracer object and invokes the actual ray tracing. After the computation,
     *  the result is saved as png or bmp file in the current directory.
     *
     *  \b Usage:
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *  basicfrontend <path-to-scene-description> [-p #] [-w #] [-h #] [-o <filename>] [-i <interleave mode>] [-d #] [-n]
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *
     *  The parameters in `[]` are optional: \n
     *  `-p`  Specify number of parallel worker threads. Default: 1. \n
     *  `-w`  Specify width of chunks in pixels. Default: 128. \n
     *  `-h`  Specify height of chunks in pixels. Default: 128. \n
     *  `-o`  Specify output file name. A 4 digit number is appended automatically. Default: RayTracer_Image_####.png. \n
     *  `-i`  Optional. Specify the interleave modus when saving hyperspectral ENVI files. Either bil, bip or bsq. Default: bil.\n
     *  `-d`  Optional. Specify the data type when saving hyperspectral ENVI files. Either uint8, uint16 or float32. Default: float32.\n
     *  `-n`  Don't append four digit number at the end of the file name of the output image.
     */
    class BasicFrontend
    {
        
    public:
        BasicFrontend();
        ~BasicFrontend();

        void updateImage(const ViewPlane& viewPlane, std::shared_ptr<const cimg_library::CImg<float> > floatImage);
        void updateChunk(const ViewPlane& viewPlane, std::shared_ptr<const cimg_library::CImg<float> > floatImage);

        bool run(int argc, char *argv[]);

        bool getPngFormat();
        bool getImageMagick();
        bool getPfmFormat();

    private:
        bool initScene(const std::string& sceneDescriptionFileName);
        bool isAvailable(std::string &fileName);

        void saveEnviHdr(const std::string &hdrName, std::shared_ptr<const cimg_library::CImg<float> > floatImage);
        void saveEnviImg(const std::string &imgName, std::shared_ptr<const cimg_library::CImg<float> > floatImage);
        void saveBmp    (const std::string &bmpName, std::shared_ptr<const cimg_library::CImg<float> > floatImage);
        void saveMonoBmp(const std::string &bmpName, std::shared_ptr<const cimg_library::CImg<float> > floatImage);
        void savePng    (const std::string &pngName, std::shared_ptr<const cimg_library::CImg<float> > floatImage);
        void saveMonoPng(const std::string &pngName, std::shared_ptr<const cimg_library::CImg<float> > floatImage);
        void savePfm(const std::string &pfmName, std::shared_ptr<const cimg_library::CImg<float> > floatImage);

        std::vector<ViewPlane> createChunks(const ViewPlane& viewPlane, int width, int height, int uMin, int uMax, int vMin, int vMax);

        std::shared_ptr<AbstractScene> scene_; ///< Scene object describing scene to render.
        SceneMetaData metaData_; ///< Scene meta data.
        std::vector<std::shared_ptr<RayTracer>> rayTracer_; ///< RayTracer object that does the rendering.

        std::shared_ptr<ThreadPool> pool_; ///< ThreadPool for multithreading support.
        std::string enviInterleave_; ///< Specifies interleave modus for saving hyperspactral images in ENVI format
        unsigned int enviDataformat_; ///< Specifies data format (uint8, int16, float etc.) for saving hyperspactral images in ENVI format
        std::mutex imageMutex_; ///< Mutex guarding the access of the image on the hard drive.

        bool imageMagick_; ///< Flag indicating whether system support Image Magic.
        bool pngFormat_; ///< Flag indicating whether filename has png suffix.
        bool pfmFormat_; ///< Flag indicating whether filename has pfm suffix
        bool omitNumber_;  ///< Flag indicating whether to append numberstring to output filename.
        std::string fileName_; ///< File name of the rendered image.

    };

} // end of namespace iiit

#endif // __BASICFRONTEND_HPP__
