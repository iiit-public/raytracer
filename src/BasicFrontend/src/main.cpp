// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file BasicFrontend/src/main.cpp
 *  \author Thomas Nuernberg
 *  \date 07.01.2015
 *  \brief Main file of BasicFrontend application.
 */

#include "BasicFrontend.hpp"



/*! \brief  Main function of basic frontend
 *  \param  argc An integer argument count of the command line arguments
 *  \param  argv An argument vector of the command line arguments
 *  \return Returns 0 on success.
 */
int main(int argc, char *argv[])
{
    iiit::BasicFrontend rayTracer;
    rayTracer.run(argc, argv);
    return 0;
}