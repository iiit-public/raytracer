// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file FileBase.hpp
 *  \author Maximilian Schambach
 *  \date 19.04.2017
 *  \brief Define FileBase for basicfrontend unit tests
 */


#ifndef __FILEBASE_HPP__
#define __FILEBASE_HPP__

#include <string>
extern std::string fileBase; ///< Base directory for basic frontend test data.

#endif
