// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file ImageTypeTest.cpp
 *  \author Maximilian Schambach
 *  \date 19.04.2017
 *  \brief Unit tests of basicfrontend for Singlethreaded imageType tests.
 */

#include "gtest/gtest.h"
#include <cstdio>
#include <fstream>


#include "BasicFrontend.hpp"
#include "include/CImg/CImg.h"
#include "FileBase.hpp"


/*! \brief Unit test for a scene pixel rendered as `Raw` image type  and saved as .bmp.
 */
TEST(BasicFrontendTest, RawBmpTest)
{
    // initialize arguments for raytracer
    std::string fileName = "basicfrontend_RawBmpTest";
    std::string argument = fileBase +  "RawTest.json";
    std::string optionFileName = fileName + ".bmp";

    char* tmp_args[] = { (char*)"basicfrontend"
                         ,(char*)argument.c_str()
                         ,(char*)"-o", (char*)optionFileName.c_str()
                         ,(char*)"-n"
                         ,NULL };
    char **argv;
    argv= tmp_args;
    int argc = sizeof(tmp_args)/sizeof(tmp_args[0]) - 1;

    // check that numSpectralSamples is set to 30, otherwise test will not be valid, as the spectrum to be rendered is specified by 30 lambda/value pairs. If that differs from 30, interpolating is done and the result cannot be tested straight forwadly

    EXPECT_EQ(iiit::numSpectralSamples, 30) << "Number of Samples has to be 30 for the test, or specify according sampledSpectrum color in test json file. Otherwise, interpolating will be done and the result is not straightforwad to test.";


    // calculate rawValue from color in json file:
    // even though the internal color of the material will be interpolated,
    // the integral (i.e. the sum here) will be the same for both
    float rawFloatValue = 1.25f + 0.25f + 0.75f + 0.0f + 1.0f + 0.325f + 0.125f + 0.253f + 0.333f + 0.876f + 0.5f + 0.55f + 0.555f + 0.123f + 0.789f + 0.493f + 0.2f + 0.3f + 0.4f + 0.5f + 0.789f + 0.6f + 0.41f + 0.03f + 0.01f + 0.7f + 0.111f + 0.99f + 1.f + 1.5f;

    rawFloatValue /= 30.f;

    //run raytracer, image is saved to current path with standard name, check that raytracer runs without error
    iiit::BasicFrontend rayTracer;
    EXPECT_EQ(rayTracer.run(argc, argv), true);


    std::string bmpName = fileName + ".bmp";
    cimg_library::CImg<float> rawImg(bmpName.c_str());

    rawFloatValue *= 255.f;
    int rawValue = (int) rawFloatValue;
    rawValue = (rawValue > 255 ? 255 : rawValue);

    // check the rendered Raw value and compare with input from values as specified in json file.
    EXPECT_EQ(rawImg.operator()(0,0,0) , rawValue);
    EXPECT_EQ(rawImg.operator()(0,0,1) , rawValue);
    EXPECT_EQ(rawImg.operator()(0,0,2) , rawValue);

    // delete image from disk
    remove(bmpName.c_str());

}


/*! \brief Unit test for a scene pixel rendered as `Raw` image type and saved as .png.
 */
TEST(BasicFrontendTest, RawPngTest)
{
    // initialize arguments for raytracer
    std::string fileName = "basicfrontend_RawPngTest";
    std::string argument = fileBase +  "RawTest.json";
    std::string optionFileName = fileName + ".png";

    char* tmp_args[] = { (char*)"basicfrontend"
                         ,(char*)argument.c_str()
                         ,(char*)"-o", (char*)optionFileName.c_str()
                         ,(char*)"-n"
                         ,NULL };
    char **argv;
    argv= tmp_args;
    int argc = sizeof(tmp_args)/sizeof(tmp_args[0]) - 1;



    // check that numSpectralSamples is set to 30, otherwise test will not be valid, as the spectrum to be rendered is specified by 30 lambda/value pairs. If that differs from 30, interpolating is done and the result cannot be tested straight forwadly
    EXPECT_EQ(iiit::numSpectralSamples, 30) << "Number of Samples has to be 30 for the test, or specify according sampledSpectrum color in test json file. Otherwise, interpolating will be done and the result is not straightforwad to test.";

    // calculate rawValue from color in json file:
    // even though the internal color of the material will be interpolated,
    // the integral (i.e. the sum here) will be the same for both
    float rawFloatValue = 1.25f + 0.25f + 0.75f + 0.0f + 1.0f + 0.325f + 0.125f + 0.253f + 0.333f + 0.876f + 0.5f + 0.55f + 0.555f + 0.123f + 0.789f + 0.493f + 0.2f + 0.3f + 0.4f + 0.5f + 0.789f + 0.6f + 0.41f + 0.03f + 0.01f + 0.7f + 0.111f + 0.99f + 1.f + 1.5f;

    rawFloatValue /= 30.f;

    //run raytracer, image is saved to current path with standard name, check that raytracer runs without error
    iiit::BasicFrontend rayTracer;
    EXPECT_EQ(rayTracer.run(argc, argv), true);
    // check that saving .png is supported by system
    EXPECT_EQ(rayTracer.getPngFormat(), true) << "For this test to run OK, saving PNG has to be supported by the system.";

    std::string pngName = fileName + ".png";
    cimg_library::CImg<unsigned short> rawImg(pngName.c_str());

    int rawValue = (int) (rawFloatValue * iiit::HIGH_COLOR_MAX);


    // check the rendered RGB value and compare with input from values as specified in json file.
    EXPECT_EQ(rawImg(0,0,0,0) , rawValue);


    // delete image from disk
    remove(pngName.c_str());

}


/*! \brief Unit test for a scene pixel rendered as `IdealRgb` image type saved as .bmp.
 */
TEST(BasicFrontendTest, IdealRgbBmpTest)
{
    // initialize arguments for raytracer
    std::string fileName = "basicfrontend_IdealRgbBmpTest";
    std::string argument = fileBase +  "IdealRgbTest.json";
    std::string optionFileName = fileName + ".bmp";

    char* tmp_args[] = { (char*)"basicfrontend"
                         ,(char*)argument.c_str()
                         ,(char*)"-o", (char*)optionFileName.c_str()
                         ,(char*)"-n"
                         ,NULL };
    char **argv;
    argv= tmp_args;
    int argc = sizeof(tmp_args)/sizeof(tmp_args[0]) - 1;


    //run raytracer, image is saved to current path with standard name, check that it runs without error
    iiit::BasicFrontend rayTracer;
    EXPECT_EQ(rayTracer.run(argc, argv), true);

    std::string bmpName = fileName + ".bmp";
    cimg_library::CImg<float> rgbImg(bmpName.c_str());

    // check the rendered RGB value and compare with input from values as specified in json file.
    EXPECT_EQ(rgbImg.operator()(0,0,0) , 135);
    EXPECT_EQ(rgbImg.operator()(0,0,1) , 201);
    EXPECT_EQ(rgbImg.operator()(0,0,2) , 52);

    // delete image from disk
    remove(bmpName.c_str());
}


/*! \brief Unit test for a scene pixel rendered as `IdealRgb` image type saved as .png.
 */
TEST(BasicFrontendTest, IdealRgbPngTest)
{
    // initialize arguments for raytracer
    std::string fileName = "basicfrontend_IdealRgbBmpTest";
    std::string argument = fileBase +  "IdealRgbTest.json";
    std::string optionFileName = fileName + ".png";

    char* tmp_args[] = { (char*)"basicfrontend"
                         ,(char*)argument.c_str()
                         ,(char*)"-o", (char*)optionFileName.c_str()
                         ,(char*)"-n"
                         ,NULL };
    char **argv;
    argv= tmp_args;
    int argc = sizeof(tmp_args)/sizeof(tmp_args[0]) - 1;

    //run raytracer, image is saved to current path with standard name, check that it runs without error
    iiit::BasicFrontend rayTracer;
    EXPECT_EQ(rayTracer.run(argc, argv), true);
    // check that saving .png is supported by system
    EXPECT_EQ(rayTracer.getPngFormat(), true) << "For this test to run OK, saving PNG has to be supported by the system.";



    std::string pngName = fileName + ".png";
    cimg_library::CImg<int> rgbImg(pngName.c_str());

    // check the rendered RGB value and compare with input from values as specified in json file.
    // the multiplication with 257 is necessary because png files are always stored with 16 bit per color per pixel
    EXPECT_EQ(rgbImg.operator()(0,0,0), 135 * 257);
    EXPECT_EQ(rgbImg.operator()(0,0,1), 201 * 257);
    EXPECT_EQ(rgbImg.operator()(0,0,2), 52 * 257);

    // delete image from disk
    remove(pngName.c_str());
}



/*! \brief Unit test for a scene pixel rendered as `MultiSpectral` image type.
 */
TEST(BasicFrontendTest, MultiSpectralFloat32Test)
{
    // initialize arguments for raytracer
    std::string fileName = "basicfrontend_MultiSpectralTest";
    std::string argument = fileBase +  "MultiSpectralTest.json";
    std::string optionFileName = fileName;

    char* tmp_args[] = { (char*)"basicfrontend"
                         ,(char*)argument.c_str()
                         ,(char*)"-o", (char*)optionFileName.c_str()
                         ,(char*)"-d", (char*)"float32"
                         ,(char*)"-n"
                         ,NULL };
    char **argv;
    argv= tmp_args;
    int argc = sizeof(tmp_args)/sizeof(tmp_args[0]) - 1;


    //run raytracer, image is saved to current path with standard name
    iiit::BasicFrontend rayTracer;
    EXPECT_EQ(rayTracer.run(argc, argv), true);

    std::string hdrName = fileName + ".hdr";
    std::string imgName = fileName + ".img";

    std::fstream imgFile (imgName, std::ios::in | std::ios::binary | std::ios::ate);
    std::streampos size;


    EXPECT_EQ(iiit::numSpectralSamples,  30) << "Test only works for 30 Spectral bands.";
    EXPECT_EQ(iiit::sampledLambdaStart, 400) << "Test only works when lambda sampled from 400.";
    EXPECT_EQ(iiit::sampledLambdaEnd,   700) << "Test only works when lambda sampled to 700.";

    // expected data is mostly 0, and 1, but 0.5 for the point the reference jumps from 0 to 1 due to interpolation
    float expectedData[iiit::numSpectralSamples] = {0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.5f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f, 1.f};

    // check that file is open
    EXPECT_EQ(imgFile.is_open(), true);

    // File opened OK. Read data from file
    size = imgFile.tellg();                                         // size of the file (in bytes)
    long imgDataLength = static_cast<long>(size) / sizeof(float);   // number of stored floats
    float *imgData;
    imgData = new float[imgDataLength];

    // allocate memory for float array
    imgFile.seekg(0, std::ios::beg);                                // set get pos to beginning
    imgFile.read(reinterpret_cast<char*> (imgData), size);        // read floats from binary file

    for (int i = 0; i < iiit::numSpectralSamples; ++i)
    {
        EXPECT_FLOAT_EQ(imgData[i], expectedData[i]);
    }

    imgFile.close();
    delete[] imgData; //free allocated space

    // remove files from disk
    remove(hdrName.c_str());
    remove(imgName.c_str());
}

/*! \brief Unit test for a scene pixel rendered as `MultiSpectral` image type.
 */
TEST(BasicFrontendTest, MultiSpectralUint8Test)
{
    // initialize arguments for raytracer
    std::string fileName = "basicfrontend_MultiSpectralTest";
    std::string argument = fileBase +  "MultiSpectralTest.json";
    std::string optionFileName = fileName;

    char* tmp_args[] = { (char*)"basicfrontend"
                         ,(char*)argument.c_str()
                         ,(char*)"-o", (char*)optionFileName.c_str()
                         ,(char*)"-d", (char*)"uint8"
                         ,(char*)"-n"
                         ,NULL };
    char **argv;
    argv= tmp_args;
    int argc = sizeof(tmp_args)/sizeof(tmp_args[0]) - 1;


    //run raytracer, image is saved to current path with standard name
    iiit::BasicFrontend rayTracer;
    EXPECT_EQ(rayTracer.run(argc, argv), true);

    std::string hdrName = fileName + ".hdr";
    std::string imgName = fileName + ".img";

    std::fstream imgFile (imgName, std::ios::in | std::ios::binary | std::ios::ate);
    std::streampos size;


    EXPECT_EQ(iiit::numSpectralSamples,  30) << "Test only works for 30 Spectral bands.";
    EXPECT_EQ(iiit::sampledLambdaStart, 400) << "Test only works when lambda sampled from 400.";
    EXPECT_EQ(iiit::sampledLambdaEnd,   700) << "Test only works when lambda sampled to 700.";

    // expected data is mostly 0, and 1, but 0.5 for the point the reference jumps from 0 to 1 due to interpolation
    uint8_t max_val = UINT8_MAX;
    uint8_t mid = uint8_t(0.5f*float(max_val));
    uint8_t expectedData[iiit::numSpectralSamples] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, mid, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val};

    // check that file is open
    EXPECT_EQ(imgFile.is_open(), true);

    // File opened OK. Read data from file
    size = imgFile.tellg();                                         // size of the file (in bytes)
    long imgDataLength = static_cast<long>(size) / sizeof(uint8_t); // number of stored floats
    uint8_t *imgData;
    imgData = new uint8_t[imgDataLength];

    // allocate memory for float array
    imgFile.seekg(0, std::ios::beg);                                // set get pos to beginning
    imgFile.read(reinterpret_cast<char*> (imgData), size);          // read floats from binary file

    for (int i = 0; i < iiit::numSpectralSamples; ++i)
    {
        EXPECT_EQ(imgData[i], expectedData[i]);
    }

    imgFile.close();
    delete[] imgData; //free allocated space

    // remove files from disk
    remove(hdrName.c_str());
    remove(imgName.c_str());
}

/*! \brief Unit test for a scene pixel rendered as `MultiSpectral` image type.
 */
TEST(BasicFrontendTest, MultiSpectralUint16Test)
{
    // initialize arguments for raytracer
    std::string fileName = "basicfrontend_MultiSpectralTest";
    std::string argument = fileBase +  "MultiSpectralTest.json";
    std::string optionFileName = fileName;

    char* tmp_args[] = { (char*)"basicfrontend"
                         ,(char*)argument.c_str()
                         ,(char*)"-o", (char*)optionFileName.c_str()
                         ,(char*)"-d", (char*)"uint16"
                         ,(char*)"-n"
                         ,NULL };
    char **argv;
    argv= tmp_args;
    int argc = sizeof(tmp_args)/sizeof(tmp_args[0]) - 1;


    //run raytracer, image is saved to current path with standard name
    iiit::BasicFrontend rayTracer;
    EXPECT_EQ(rayTracer.run(argc, argv), true);

    std::string hdrName = fileName + ".hdr";
    std::string imgName = fileName + ".img";

    std::fstream imgFile (imgName, std::ios::in | std::ios::binary | std::ios::ate);
    std::streampos size;


    EXPECT_EQ(iiit::numSpectralSamples,  30) << "Test only works for 30 Spectral bands.";
    EXPECT_EQ(iiit::sampledLambdaStart, 400) << "Test only works when lambda sampled from 400.";
    EXPECT_EQ(iiit::sampledLambdaEnd,   700) << "Test only works when lambda sampled to 700.";

    // expected data is mostly 0, and 1, but 0.5 for the point the reference jumps from 0 to 1 due to interpolation
    uint16_t max_val = UINT16_MAX;
    uint16_t mid = uint16_t(0.5f*float(max_val));
    uint16_t expectedData[iiit::numSpectralSamples] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, mid, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val, max_val};

    // check that file is open
    EXPECT_EQ(imgFile.is_open(), true);

    // File opened OK. Read data from file
    size = imgFile.tellg();                                         // size of the file (in bytes)
    long imgDataLength = static_cast<long>(size) / sizeof(uint16_t); // number of stored floats
    uint16_t *imgData;
    imgData = new uint16_t[imgDataLength];

    // allocate memory for float array
    imgFile.seekg(0, std::ios::beg);                                // set get pos to beginning
    imgFile.read(reinterpret_cast<char*> (imgData), size);          // read floats from binary file

    for (int i = 0; i < iiit::numSpectralSamples; ++i)
    {
        EXPECT_EQ(imgData[i], expectedData[i]);
    }

    imgFile.close();
    delete[] imgData; //free allocated space

    // remove files from disk
    remove(hdrName.c_str());
    remove(imgName.c_str());
}
