// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file FileAndErrorHandlingTest.cpp
 *  \author Maximilian Schambach
 *  \date 19.04.2017
 *  \brief Unit tests of basicfrontend for file and error handling.
 */

#include "gtest/gtest.h"
#include <cstdio>
#include <fstream>


#include "BasicFrontend.hpp"
#include "include/CImg/CImg.h"
#include "FileBase.hpp"




/*! \brief Unit test for usage handling of raytracer.
 */
TEST(BasicFrontendTest, UsageTest)
{
    // initialize arguments for raytracer

    char* tmp_args[] = { (char*)"basicfrontend"
                         ,NULL };
    char **argv;
    argv= tmp_args;
    int argc = sizeof(tmp_args)/sizeof(tmp_args[0]) - 1;

    //run raytracer, image is saved to current path with standard name, check that raytracer runs without error
    iiit::BasicFrontend rayTracer;
    testing::internal::CaptureStdout(); //capture std::cout
    EXPECT_EQ(rayTracer.run(argc, argv), true);
    std::string output = testing::internal::GetCapturedStdout();
    std::string expectedOutput = "Usage\n\n  "
                                 "basicfrontend <path-to-scene-description> [-p #] [-w #] [-h #] [-o <filename>] [-i <ENVI interleave mode>] [-d #] [-n]\n\n"
                                 "The parameters in [] are optional:\n    "
                                 "-p  Specify number of parallel worker threads. Default: 1.\n    "
                                 "-w  Specify width of chunks in pixels. Default: 128.\n    "
                                 "-h  Specify height of chunks in pixels. Default: 128.\n    "
                                 "-o  Specify output file name. A 4 digit number is appended automatically. Default: RayTracer_Image_####.png.\n    "
                                 "-i  Optional. Specify the interleave modus when saving hyperspectral ENVI files. Either bil, bip or bsq. Default: bil.\n    "
                                 "-d  Optional. Specify the data type when saving hyperspectral ENVI files. Either uint8, uint16 or float32. Default: float32.\n    "
                                 "-n  Don't append four digit number at the end of the file name of the output image.\n";
    EXPECT_EQ(output, expectedOutput);
}




// test argument handling


/*! \brief Unit test for argument i handling of raytracer.
 */
TEST(BasicFrontendTest, ParseIargTest)
{
    // initialize arguments for raytracer with arbitrary file
    std::string argument = fileBase +  "RawTest.json";

    char* tmp_args[] = { (char*)"basicfrontend"
                         ,(char*)argument.c_str()
                         ,(char*)"-i", (char*)"NONSENSE"
                         ,NULL };
    char **argv;
    argv= tmp_args;
    int argc = sizeof(tmp_args)/sizeof(tmp_args[0]) - 1;



    //run raytracer, image is saved to current path with standard name, check that raytracer runs without error
    iiit::BasicFrontend rayTracer;
    testing::internal::CaptureStdout(); //capture std::cout
    EXPECT_EQ(rayTracer.run(argc, argv), false);
    std::string output = testing::internal::GetCapturedStdout();
    std::string expectedOutput = "Could not parse  -i argument. Specified ENVI interleave is not allowed. Use bil, bip or bsq. Exiting.\n";
    EXPECT_EQ(output, expectedOutput);
}

/*! \brief Unit test for argument chunkwidth handling of raytracer.
 */
TEST(BasicFrontendTest, ParseChunkWidthTest)
{
    // initialize arguments for raytracer with arbitrary file
    std::string argument = fileBase +  "RawTest.json";

    char* tmp_args[] = { (char*)"basicfrontend"
                         ,(char*)argument.c_str()
                         ,(char*)"-w", (char*)"NONSENSE"
                         ,NULL };
    char **argv;
    argv= tmp_args;
    int argc = sizeof(tmp_args)/sizeof(tmp_args[0]) - 1;



    //run raytracer, image is saved to current path with standard name, check that raytracer runs without error
    iiit::BasicFrontend rayTracer;
    testing::internal::CaptureStdout(); //capture std::cout
    EXPECT_EQ(rayTracer.run(argc, argv), false);
    std::string output = testing::internal::GetCapturedStdout();
    std::string expectedOutput = "Could not parse arguments!\n";
    EXPECT_EQ(output, expectedOutput);
}



/*! \brief Unit test for argument file handling of raytracer.
 */
TEST(BasicFrontendTest, ParseJsonTest)
{
    // initialize arguments for raytracer with arbitrary file
    std::string argument = fileBase +  "NONSENSE.json";

    char* tmp_args[] = { (char*)"basicfrontend"
                         ,(char*)argument.c_str()
                         ,NULL };
    char **argv;
    argv= tmp_args;
    int argc = sizeof(tmp_args)/sizeof(tmp_args[0]) - 1;



    //run raytracer, image is saved to current path with standard name, check that raytracer runs without error
    iiit::BasicFrontend rayTracer;
    testing::internal::CaptureStdout(); //capture std::cout
    EXPECT_EQ(rayTracer.run(argc, argv), false);
    std::string output = testing::internal::GetCapturedStdout();
    std::string expectedOutput = "Cannot open file " + argument +".\n";
    EXPECT_EQ(output, expectedOutput);
}


/*! \brief Unit test for argument chunkheight handling of raytracer.
 */
TEST(BasicFrontendTest, ParseChunkHeightTest)
{
    // initialize arguments for raytracer with arbitrary file
    std::string argument = fileBase +  "RawTest.json";

    char* tmp_args[] = { (char*)"basicfrontend"
                         ,(char*)argument.c_str()
                         ,(char*)"-h", (char*)"NONSENSE"
                         ,NULL };
    char **argv;
    argv= tmp_args;
    int argc = sizeof(tmp_args)/sizeof(tmp_args[0]) - 1;



    //run raytracer, image is saved to current path with standard name, check that raytracer runs without error
    iiit::BasicFrontend rayTracer;
    testing::internal::CaptureStdout(); //capture std::cout
    EXPECT_EQ(rayTracer.run(argc, argv), false);
    std::string output = testing::internal::GetCapturedStdout();
    std::string expectedOutput = "Could not parse arguments!\n";
    EXPECT_EQ(output, expectedOutput);
}



/*! \brief Unit test for nonsense argument handling of raytracer.
 */
TEST(BasicFrontendTest, ParseNonsenseTest)
{
    // initialize arguments for raytracer with arbitrary file
    std::string argument = fileBase +  "RawTest.json";

    char* tmp_args[] = { (char*)"basicfrontend"
                         ,(char*)argument.c_str()
                         ,(char*)"-z", (char*)"NONSENSE"
                         ,NULL };
    char **argv;
    argv= tmp_args;
    int argc = sizeof(tmp_args)/sizeof(tmp_args[0]) - 1;



    //run raytracer, image is saved to current path with standard name, check that raytracer runs without error
    iiit::BasicFrontend rayTracer;
    testing::internal::CaptureStdout(); //capture std::cout
    EXPECT_EQ(rayTracer.run(argc, argv), false);
    std::string output = testing::internal::GetCapturedStdout();
    std::string expectedOutput = "Could not parse arguments!\n";
    EXPECT_EQ(output, expectedOutput);
}

/*! \brief Unit test for more nonsense argument handling of raytracer.
 */
TEST(BasicFrontendTest, ParseMoreNonsenseTest)
{
    // initialize arguments for raytracer with arbitrary file
    std::string argument = fileBase +  "RawTest.json";

    char* tmp_args[] = { (char*)"basicfrontend"
                         ,(char*)argument.c_str()
                         ,(char*)"-nonsense"
                         ,NULL };
    char **argv;
    argv= tmp_args;
    int argc = sizeof(tmp_args)/sizeof(tmp_args[0]) - 1;



    //run raytracer, image is saved to current path with standard name, check that raytracer runs without error
    iiit::BasicFrontend rayTracer;
    testing::internal::CaptureStdout(); //capture std::cout
    EXPECT_EQ(rayTracer.run(argc, argv), false);
    std::string output = testing::internal::GetCapturedStdout();
    std::string expectedOutput = "Could not parse arguments!\n";
    EXPECT_EQ(output, expectedOutput);
}


/*! \brief Unit test for filename number handling
 */
TEST(BasicFrontendTest, FileNameHandlingTest)
{
    // initialize arguments for raytracer
    std::string fileName = "FileNameHandlingTest";
    std::string argument = fileBase +  "RawTest.json";
    std::string optionFileName = fileName + ".bmp";

    char* tmp_args[] = { (char*)"basicfrontend"
                         ,(char*)argument.c_str()
                         ,(char*)"-o", (char*)optionFileName.c_str()
                         ,NULL };
    char **argv;
    argv= tmp_args;
    int argc = sizeof(tmp_args)/sizeof(tmp_args[0]) - 1;


    // create dummy file with same filename
    std::string dummyFileName = fileName + "_0000.bmp";
    std::ofstream dummyFile;
    dummyFile.open (dummyFileName);
    dummyFile << "DUMMY.\n";
    dummyFile.close();


    //run raytracer, image is saved to current path with standard name, check that raytracer runs without error
    iiit::BasicFrontend rayTracer;
    EXPECT_EQ(rayTracer.run(argc, argv), true);

    // check that raytracer appended 0001 to filename
    std::string bmpName = fileName + "_0001.bmp";
    cimg_library::CImg<float> rawImg(bmpName.c_str());

    float rawFloatValue = 1.25f + 0.25f + 0.75f + 0.0f + 1.0f + 0.325f + 0.125f + 0.253f + 0.333f + 0.876f + 0.5f + 0.55f + 0.555f + 0.123f + 0.789f + 0.493f + 0.2f + 0.3f + 0.4f + 0.5f + 0.789f + 0.6f + 0.41f + 0.03f + 0.01f + 0.7f + 0.111f + 0.99f + 1.f + 1.5f;
    rawFloatValue /= 30.f;
    rawFloatValue *= 255.f;
    int rawValue = (int) rawFloatValue;
    rawValue = (rawValue > 255 ? 255 : rawValue);

    // check the rendered Raw value and compare with input from values as specified in json file.
    EXPECT_EQ(rawImg.operator()(0,0,0) , rawValue);
    EXPECT_EQ(rawImg.operator()(0,0,1) , rawValue);
    EXPECT_EQ(rawImg.operator()(0,0,2) , rawValue);

    // delete image from disk
    remove(bmpName.c_str());
    remove(dummyFileName.c_str());
}


/*! \brief Unit test for existing bmp filename without number handling
 */
TEST(BasicFrontendTest, ExistingBmpFileNameHandlingTest)
{
    // initialize arguments for raytracer
    std::string fileName = "ExistingBmpFileNameHandlingTest";
    std::string argument = fileBase +  "RawTest.json";
    std::string optionFileName = fileName + ".bmp";

    char* tmp_args[] = { (char*)"basicfrontend"
                         ,(char*)argument.c_str()
                         ,(char*)"-o", (char*)optionFileName.c_str()
                         ,(char*)"-n"
                         ,NULL };
    char **argv;
    argv= tmp_args;
    int argc = sizeof(tmp_args)/sizeof(tmp_args[0]) - 1;


    // create dummy file with same filename
    std::string dummyFileName = fileName + ".bmp";
    std::ofstream dummyFile;
    dummyFile.open (dummyFileName);
    dummyFile << "DUMMY.\n";
    dummyFile.close();


    //run raytracer, image is saved to current path with standard name, check that raytracer runs with error
    iiit::BasicFrontend rayTracer;
    testing::internal::CaptureStdout(); //capture std::cout
    EXPECT_EQ(rayTracer.run(argc, argv), false);
    std::string output = testing::internal::GetCapturedStdout();
    std::string expectedOutput = "ERROR: File could not be saved. Move/Delete previous results.\n";
    EXPECT_EQ(output, expectedOutput);

    remove(dummyFileName.c_str());
}

/*! \brief Unit test for existing png filename without number handling
 */
TEST(BasicFrontendTest, ExistingPngFileNameHandlingTest)
{
    // initialize arguments for raytracer
    std::string fileName = "ExistingPngFileNameHandlingTest";
    std::string argument = fileBase +  "RawTest.json";
    std::string optionFileName = fileName + ".bmp";

    char* tmp_args[] = { (char*)"basicfrontend"
                         ,(char*)argument.c_str()
                         ,(char*)"-o", (char*)optionFileName.c_str()
                         ,(char*)"-n"
                         ,NULL };
    char **argv;
    argv= tmp_args;
    int argc = sizeof(tmp_args)/sizeof(tmp_args[0]) - 1;


    // create dummy file with same filename
    std::string dummyFileName = fileName + ".png";
    std::ofstream dummyFile;
    dummyFile.open (dummyFileName);
    dummyFile << "DUMMY.\n";
    dummyFile.close();


    //run raytracer, image is saved to current path with standard name, check that raytracer runs with error
    iiit::BasicFrontend rayTracer;
    testing::internal::CaptureStdout(); //capture std::cout
    EXPECT_EQ(rayTracer.run(argc, argv), false);
    std::string output = testing::internal::GetCapturedStdout();
    std::string expectedOutput = "ERROR: File could not be saved. Move/Delete previous results.\n";
    EXPECT_EQ(output, expectedOutput);

    remove(dummyFileName.c_str());
}


/*! \brief Unit test for existing img filename without number handling
 */
TEST(BasicFrontendTest, ExistingImgFileNameHandlingTest)
{
    // initialize arguments for raytracer
    std::string fileName = "ExistingImgFileNameHandlingTest";
    std::string argument = fileBase +  "RawTest.json";
    std::string optionFileName = fileName + ".bmp";

    char* tmp_args[] = { (char*)"basicfrontend"
                         ,(char*)argument.c_str()
                         ,(char*)"-o", (char*)optionFileName.c_str()
                         ,(char*)"-n"
                         ,NULL };
    char **argv;
    argv= tmp_args;
    int argc = sizeof(tmp_args)/sizeof(tmp_args[0]) - 1;


    // create dummy file with same filename
    std::string dummyFileName = fileName + ".img";
    std::ofstream dummyFile;
    dummyFile.open (dummyFileName);
    dummyFile << "DUMMY.\n";
    dummyFile.close();


    //run raytracer, image is saved to current path with standard name, check that raytracer runs with error
    iiit::BasicFrontend rayTracer;
    testing::internal::CaptureStdout(); //capture std::cout
    EXPECT_EQ(rayTracer.run(argc, argv), false);
    std::string output = testing::internal::GetCapturedStdout();
    std::string expectedOutput = "ERROR: File could not be saved. Move/Delete previous results.\n";
    EXPECT_EQ(output, expectedOutput);

    remove(dummyFileName.c_str());
}



/*! \brief Unit test for existing hdr filename without number handling
 */
TEST(BasicFrontendTest, ExistingHdrFileNameHandlingTest)
{
    // initialize arguments for raytracer
    std::string fileName = "ExistingHdeFileNameHandlingTest";
    std::string argument = fileBase +  "RawTest.json";
    std::string optionFileName = fileName + ".bmp";

    char* tmp_args[] = { (char*)"basicfrontend"
                         ,(char*)argument.c_str()
                         ,(char*)"-o", (char*)optionFileName.c_str()
                         ,(char*)"-n"
                         ,NULL };
    char **argv;
    argv= tmp_args;
    int argc = sizeof(tmp_args)/sizeof(tmp_args[0]) - 1;


    // create dummy file with same filename
    std::string dummyFileName = fileName + ".hdr";
    std::ofstream dummyFile;
    dummyFile.open (dummyFileName);
    dummyFile << "DUMMY.\n";
    dummyFile.close();


    //run raytracer, image is saved to current path with standard name, check that raytracer runs with error
    iiit::BasicFrontend rayTracer;
    testing::internal::CaptureStdout(); //capture std::cout
    EXPECT_EQ(rayTracer.run(argc, argv), false);
    std::string output = testing::internal::GetCapturedStdout();
    std::string expectedOutput = "ERROR: File could not be saved. Move/Delete previous results.\n";
    EXPECT_EQ(output, expectedOutput);

    remove(dummyFileName.c_str());
}




/*! \brief Unit test for too many existing filename number handling
 */
TEST(BasicFrontendTest, TooManyExistingFileNameHandlingTest)
{
    // initialize arguments for raytracer
    std::string fileName = "TooManyExistingFileNameHandlingTest";
    std::string argument = fileBase +  "RawTest.json";
    std::string optionFileName = fileName + ".bmp";

    char* tmp_args[] = { (char*)"basicfrontend"
                         ,(char*)argument.c_str()
                         ,(char*)"-o", (char*)optionFileName.c_str()
                         ,NULL };
    char **argv;
    argv= tmp_args;
    int argc = sizeof(tmp_args)/sizeof(tmp_args[0]) - 1;


    // create dummy files with same filename and appended number
    std::string dummyFileName = fileName + ".bmp";
    std::ofstream dummyFile;

    // create files from _0000 to _9999
    for (int i = 0; i < 10000; i++)
    {
        std::stringstream a;
        a << std::setfill ('0') << std::setw (4) << i;
        dummyFileName = fileName + "_" + a.str() + ".bmp";
        dummyFile.open(dummyFileName.c_str(), std::ios::out);
        dummyFile.close();
    }

    //run raytracer, image is saved to current path with standard name, check that raytracer runs with error
    iiit::BasicFrontend rayTracer;
    testing::internal::CaptureStdout(); //capture std::cout
    EXPECT_EQ(rayTracer.run(argc, argv), false);
    std::string output = testing::internal::GetCapturedStdout();
    std::string expectedOutput = "ERROR: File could not be saved. Move/Delete previous results.\n";
    EXPECT_EQ(output, expectedOutput);

    // remove files from _0000 to _9999
    for (int i = 0; i < 10000; i++)
    {
        std::stringstream a;
        a << std::setfill ('0') << std::setw (4) << i;
        dummyFileName = fileName + "_" + a.str() + ".bmp";
        remove(dummyFileName.c_str());
    }
}
