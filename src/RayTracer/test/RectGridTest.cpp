// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file HexGridTest.cpp
 *  \author Maximilian Schambach
 *  \date 27.02.2019
 *  \brief Unit test of class HexGrid.
 */

#include "gtest/gtest.h"

#include "Utilities/Constants.h"
#include "Utilities/RectGrid.hpp"
#include "Utilities/Point3D.hpp"
#include "Spectrum/RgbSpectrum.hpp"

using namespace iiit;



///*! \brief Unit test of constructor.
TEST(RectGridTest, RectGridConstructor)
{
    double x_max = 1.01;
    double y_max = 2.01;
    double offset_x = 0.0;
    double offset_y = 0.0;
    double spacing_x = 0.5;
    double spacing_y = 1.0;
    double rotation = 0.0;
    double phi = 0.0;
    double theta = 0.0;
    double z = 0.0;

    RectGrid<RgbSpectrum> grid(x_max, y_max, offset_x, offset_y, spacing_x, spacing_y, rotation, phi, theta, z, false);
    EXPECT_EQ(grid.get_num_points(), 5*5);

    // Flip spacing and rotate
    spacing_x = 1.0;
    spacing_y = 0.5;
    rotation = PI/2;
    RectGrid<RgbSpectrum> grid2(x_max, y_max, offset_x, offset_y, spacing_x, spacing_y, rotation, phi, theta, z, false);

    EXPECT_NEAR(grid2.get_a_x(), grid.get_b_x(), 0.001);
    EXPECT_NEAR(grid2.get_a_y(), grid.get_b_y(), 0.001);
    EXPECT_NEAR(grid2.get_b_x(), -grid.get_a_x(), 0.001);
    EXPECT_NEAR(grid2.get_b_y(), -grid.get_a_y(), 0.001);

    EXPECT_EQ(grid.get_num_points(), grid2.get_num_points());
}
