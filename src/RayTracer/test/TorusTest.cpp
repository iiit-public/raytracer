// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file TorusTest.cpp
*  \author Thomas Nuernberg
*  \date 20.04.2017
*  \brief Unit test of class Torus.
*/

#include "gtest/gtest.h"

#include "Scene.hpp"
#include "Spectrum/RgbSpectrum.hpp"
#include "Objects/Torus.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/Normal.hpp"
#include "Utilities/Ray.hpp"
#include "Utilities/ShadingData.hpp"

using namespace iiit;



/*! \brief Unit of torus hit() function.
*
*  Test of various rays hitting or missing a torus.
*/
TEST(TorusTest, HitTorusTest)
{
    double tmin;
    std::shared_ptr<Scene<RgbSpectrum> > scene(new Scene<RgbSpectrum>);
    ShadingData<RgbSpectrum> shadingData(scene);

    // torus parameters
    double innerRadius = 0.25;
    double outerRadius = 1.0;
    Torus<RgbSpectrum> torus(innerRadius, outerRadius);

    // missing rays in z-direction
    Ray ray(Point3D(0.0, 0.0, -2.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_FALSE(torus.hit(ray, tmin, shadingData));
    EXPECT_FALSE(torus.shadowHit(ray, tmin));

    ray.set(Point3D(1.5, 0.0, -2.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_FALSE(torus.hit(ray, tmin, shadingData));
    EXPECT_FALSE(torus.shadowHit(ray, tmin));

    ray.set(Point3D(0.9, 0.9, -2.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_FALSE(torus.hit(ray, tmin, shadingData));
    EXPECT_FALSE(torus.shadowHit(ray, tmin));

    // hitting rays in z-direction (-> 2 intersections)
    //ray.set(Point3D(1.0, 0.0, -2.0), Vector3D(0.0, 0.0, 1.0)); // borderline case of QuarticSolver yields no hitpoint
    ray.set(Point3D(1.1, 0.0, -2.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_TRUE(torus.hit(ray, tmin, shadingData));
    EXPECT_TRUE(torus.shadowHit(ray, tmin));
    //EXPECT_DOUBLE_EQ(1.75, tmin);
    //EXPECT_EQ(Normal(0.0, 0.0, -1.0), shadingData.normal_);
    //EXPECT_EQ(Point3D(1.0, 0.0, -0.25), shadingData.hitPoint_);

    //ray.set(Point3D(0.0, 1.0, -2.0), Vector3D(0.0, 0.0, 1.0)); // borderline case of QuarticSolver yields no hitpoint
    ray.set(Point3D(0.0, 1.1, -2.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_TRUE(torus.hit(ray, tmin, shadingData));
    EXPECT_TRUE(torus.shadowHit(ray, tmin));
    //EXPECT_DOUBLE_EQ(1.75, tmin);
    //EXPECT_EQ(Normal(0.0, 0.0, -1.0), shadingData.normal_);
    //EXPECT_EQ(Point3D(0.0, 1.0, -0.25), shadingData.hitPoint_);

    // missing rays in x-direction
    ray.set(Point3D(-2.0, 0.0, 1.5), Vector3D(1.0, 0.0, 0.0));
    EXPECT_FALSE(torus.hit(ray, tmin, shadingData));
    EXPECT_FALSE(torus.shadowHit(ray, tmin));

    ray.set(Point3D(-2.0, 1.5, 0.0), Vector3D(1.0, 0.0, 0.0));
    EXPECT_FALSE(torus.hit(ray, tmin, shadingData));
    EXPECT_FALSE(torus.shadowHit(ray, tmin));

    // hitting rays in x-direction (-> up to four intersections)
    //ray.set(Point3D(-2.0, 1.0, 0.0), Vector3D(1.0, 0.0, 0.0)); // borderline case of QuarticSolver yields no hitpoint
    ray.set(Point3D(-2.0, 1.1, 0.0), Vector3D(1.0, 0.0, 0.0));
    EXPECT_TRUE(torus.hit(ray, tmin, shadingData));
    EXPECT_TRUE(torus.shadowHit(ray, tmin));

    ray.set(Point3D(-2.0, 0.0, 0.0), Vector3D(1.0, 0.0, 0.0));
    EXPECT_TRUE(torus.hit(ray, tmin, shadingData));
    EXPECT_TRUE(torus.shadowHit(ray, tmin));

    ray.set(Point3D(-2.0, 0.0, 0.125), Vector3D(1.0, 0.0, 0.0));
    EXPECT_TRUE(torus.hit(ray, tmin, shadingData));
    EXPECT_TRUE(torus.shadowHit(ray, tmin));
}



/*! \brief Unit of torus hit() function.
 *  \attention Results of rays never actually traverse through the torus volume are irrelevant.
 *
 *  Test of various rays touching a torus.
 */
TEST(TorusTest, TouchTorusTest)
{
    double tmin;
    std::shared_ptr<Scene<RgbSpectrum> > scene(new Scene<RgbSpectrum>);
    ShadingData<RgbSpectrum> shadingData(scene);

    // torus parameters
    double innerRadius = 0.25;
    double outerRadius = 1.0;
    Torus<RgbSpectrum> torus(innerRadius, outerRadius);

    // touching rays in z-direction
    Ray ray(Point3D(1.25, 0.0, -2.0), Vector3D(0.0, 0.0, 1.0));
    bool hit = torus.hit(ray, tmin, shadingData);
    hit = torus.shadowHit(ray, tmin);

    ray.set(Point3D(0.75, 0.0, -2.0), Vector3D(0.0, 0.0, 1.0));
    hit = torus.hit(ray, tmin, shadingData);
    hit = torus.shadowHit(ray, tmin);

    // touching rays in x-direction
    ray.set(Point3D(-2.0, 1.25, 0.0), Vector3D(1.0, 0.0, 0.0));
    hit = torus.hit(ray, tmin, shadingData);
    hit = torus.shadowHit(ray, tmin);

    ray.set(Point3D(-2.0, 0.75, 0.0), Vector3D(1.0, 0.0, 0.0));
    EXPECT_TRUE(torus.hit(ray, tmin, shadingData));
    EXPECT_TRUE(torus.shadowHit(ray, tmin));

    ray.set(Point3D(-2.0, 0.0, 0.25), Vector3D(1.0, 0.0, 0.0));
    hit = torus.hit(ray, tmin, shadingData);
    hit = torus.shadowHit(ray, tmin);
}
