// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file CameraTestComparison.cpp
 *  \author Sandro Braun
 *  \date   10.11.2015
 *  \brief  a test that should give three identical output pictures although three different
 *  Cameras are used
 *
 *  We create a scene that is regarded with a ThinLens, an ApertureCamera and a DualApertureCamera.
 *  The cameras are configured in a way that they should give the absolute identical output picture.
 *  If you want to see your faulty pixels, scroll to the end of this file and modify the test by
 *  removing the comment block. You will then see your rendered images as well as the difference
 *  images between them.
 *
 *
 */

#include "gtest/gtest.h"

#include <memory>
#include <random>
#include <chrono>

#include "RayTracer.hpp"
#include "Spectrum/RgbSpectrum.hpp"
#include "Tracers/RayCast.hpp"
#include "Objects/Box.hpp"
#include "Objects/Instance.hpp"
#include "Textures/Checker3D.hpp"
#include "Sampler/Regular.hpp"
#include "Cameras/ThinLens.hpp"
#include "Cameras/ImageAperture.hpp"
#include "Cameras/Sensor.hpp"
#include "Cameras/Sensor.cpp"
#include "Lights/PointLight.hpp"
#include "Utilities/Constants.h"

using namespace iiit;



// general parameters
#define NUMSAMPLES  16

// define camera parameters
// parameters for all 3 cameras
#define EYEPOINT        0.0, 0.0, 0.0
#define LOOKAT          1.0, 0.0, 0.0
#define UP              0.0, 1.0, 0.0
#define SENSORNAME      std::string("Ideal")
#define SENSORRESPONSIVITY iiit::responsivity
#define IMAGETYPE       ImageType::Type::IdealRgb
#define LENSRADIUS      25.0
#define IMAGEDISTANCE   45.0
#define FOCUSDISTANCE   90.0
//apertureCamera class parameters
#define APERTURE_POSITION   0.0

#define FRONT_APERTURE_POSITION 0.0
#define REAR_APERTURE_POSITION  0.0


/*! \brief Function builds the scene for this unit test.
 */
std::shared_ptr<Scene<RgbSpectrum> > createScene(){

    std::cout << "Creating scene...";

    std::shared_ptr<Scene<RgbSpectrum> > scene(new Scene<RgbSpectrum>());
    std::array<float, 3> rgb = {{0.f, 0.f, 0.f}};
    rgb[0] = 100.f / TRUE_COLOR_MAX;
    scene->backgroundColor_ = RgbSpectrum::fromRgb(rgb, SpectrumRepresentationType::IlluminationSpectrum);
    
    //set sensor resolution
    scene->viewPlane_.uRes_ = 512;  //horizontal pixel amount
    scene->viewPlane_.vRes_ = 512;  //vertical pixel amount
    scene->viewPlane_.uAbsRes_ = scene->viewPlane_.uRes_;   //this is done for multithreading
    scene->viewPlane_.vAbsRes_ = scene->viewPlane_.vRes_;   //this is done for multithreading
    scene->viewPlane_.s_ = 0.025;   //pixel size

    std:: cout << "finished!" << std::endl << "creating sampler...";

    //set regular sampler on viewPlane. This is important to be able to compare the images in the test
    scene->viewPlane_.setSampler( std::shared_ptr<Regular>( new Regular(NUMSAMPLES) ) );    //128 samples
    scene->viewPlane_.sampler_->generateSamples();

    std:: cout << "finished!" << std::endl << "Test Object...";

    //create a checked matte texture for the box
    std::array<float, 3> rgb1 = {{1.f, 1.f, 1.f}};
    std::array<float, 3> rgb2 = {{0.f, 0.f, 0.f}};
    std::shared_ptr<Texture<RgbSpectrum> > checkedTexture(
        new Checker3D<RgbSpectrum>(1.0, 1.0, 1.0,
        RgbSpectrum::fromRgb(rgb1, SpectrumRepresentationType::ReflectanceSpectrum),
        RgbSpectrum::fromRgb(rgb2, SpectrumRepresentationType::ReflectanceSpectrum)));
    std::shared_ptr<Matte<RgbSpectrum>> matte( new Matte<RgbSpectrum>() );
    matte->setDiffuseTexture(checkedTexture);
    matte->setDiffuseReflectionFactor(1.f);

    //build the box with the matte material
    //this is very complicated because the box is transformed later on
    std::shared_ptr<Instance<RgbSpectrum>> box(new Instance<RgbSpectrum>(
            std::shared_ptr<Box<RgbSpectrum>>(new Box<RgbSpectrum>(
                    Point3D(-1.0, -1.0, -1.0), /*v_0*/
                    Point3D(1.0, 1.0, 1.0), /*v_1*/
                    matte
            ) )
    ) );   /*material*/


    //transform it
    box->rotateZ(-1.1f);
    box->scale(5, 3, 3);
    box->translate(90, 0, 0);

    //and add it to the scene
    scene->addObject(box);

    std:: cout << "finished!" << std::endl << "creating PointLight...";

    //create a Light
    std::shared_ptr<PointLight<RgbSpectrum> > pointLight( new PointLight<RgbSpectrum>(Point3D(2.0, 2.0, 2.0) ) /*Location*/);
    pointLight->setColor(RgbSpectrum::fromRgb(rgb1, SpectrumRepresentationType::ReflectanceSpectrum));  //white Light
    pointLight->setRadianceScaling(8.0);

    //and add it to the scene
    scene->addLight(pointLight);

    std:: cout << "finished!" << std::endl << "creating tracer...";

    //set the raytracer of the scene
    std::shared_ptr<Tracer<RgbSpectrum>> tracer(new RayCast<RgbSpectrum>(scene));
    scene->setTracer(tracer);

    std:: cout << "finished!" << std::endl;

    //return the scene
    return scene;
}


/*! \brief Unit tests renders a scene with three different cameras.
 *
 *  Unit test renders the same scene with the the ThinLens, the ApertureCamera and the
 *  DualApertureCamera. The aperture cameras are configured to produce the same image as the
 *  ThinLens.
 */
TEST(CameraComparisonTest, pictureTest){


    std::shared_ptr<Scene<RgbSpectrum> > scene = createScene();

    std:: cout << "Creating ThinLens Camera...";

    //**// first we test the ThinLens class
    std::shared_ptr<ThinLens<RgbSpectrum> > thinLensCamera( new ThinLens<RgbSpectrum>(
        scene,
        Point3D(EYEPOINT), /*eye Point*/
        Point3D(LOOKAT), /*look at Point*/
        Vector3D(UP), /*up direction*/
        Sensor<RgbSpectrum>(IMAGETYPE, SENSORRESPONSIVITY, SENSORNAME), /*sensor*/
        LENSRADIUS, /* lens Radius */
        IMAGEDISTANCE, /* image Distance */
        FOCUSDISTANCE) ); /* focus Distance */
    // give the camera a sampler
    std::shared_ptr<Sampler> cameraSampler(new Regular(NUMSAMPLES));
    cameraSampler->generateSamples();
    cameraSampler->mapSamplesToUnitDisk();
    thinLensCamera->setSampler(cameraSampler);

    // set the scenes Camera to our thinLensCamera
    scene->setCamera(thinLensCamera);

    // initialize image
    std::shared_ptr<cimg_library::CImg<float> >thinLensImage =
            std::make_shared<cimg_library::CImg<float> >(scene->viewPlane_.uRes_, scene->viewPlane_.vRes_, 1, 3);

    // render image
    std:: cout << "finished!" << std::endl << "Rendering scene with ThinLensCamera...";
    scene->renderScene(thinLensImage);


    //**// then we test the ApertureCamera class
    std:: cout << "finished!" << std::endl << "Creating apertureCamera class...";
    std::shared_ptr<ImageAperture> aperture ( new ImageAperture(APERTURE_POSITION, LENSRADIUS) );
    aperture->setRectangularAperture(10,10,5,5);
    aperture->setApertureOutsideLens();     //this means that the lens is inside the aperture

    std::shared_ptr<ThinLens<RgbSpectrum> > apertureCamera( new ThinLens<RgbSpectrum>(
        scene,
        Point3D(EYEPOINT), /*eye Point*/
        Point3D(LOOKAT), /*look at Point*/
        Vector3D(UP), /*up direction*/
        Sensor<RgbSpectrum>(IMAGETYPE, SENSORRESPONSIVITY, SENSORNAME), /*sensor*/
        LENSRADIUS, /* lens Radius */
        IMAGEDISTANCE, /* image Distance */
        FOCUSDISTANCE /* focus Distance */
        ) );
    // add aperture
    apertureCamera->addAperture(aperture);
    // give the camera a sampler
    apertureCamera->setSampler(cameraSampler);
    // set the scenes Camera to our thinLensCamera
    scene->setCamera(apertureCamera);

    // initialize image
    std::shared_ptr<cimg_library::CImg<float> > apertureCameraImage =
            std::make_shared<cimg_library::CImg<float> >(scene->viewPlane_.uRes_, scene->viewPlane_.vRes_, 1, 3);

    // render image
    std:: cout << "finished!" << std::endl << "Rendering image with apertureCameraClass...";
    scene->renderScene(apertureCameraImage);


    std:: cout << "finished!" << std::endl << "Creating DualApertureClass...";

    EXPECT_TRUE(*thinLensImage == *apertureCameraImage);


    //you can use this to see your bugs. DO NOT DELETE THIS
/*
    cimg_library::CImgDisplay   disp1(*thinLensImage, "thinLensImage"),
            disp2(*apertureCameraImage, "apertureCameraImage"),
            disp3(*dualApertureCameraImage, "dualApertureCameraImage");

    std::cout << "check if each picture looks the same. you should see a "
                 "black and white checked box on a reddish background. "
                 "Close one picture to continue" << std::endl;

    while(!disp1.is_closed() && !disp2.is_closed() && !disp3.is_closed()){
    }

    std::cout << "if you see three black pictures now, everything is fine" << std::endl;

    disp1 = cimg_library::CImgDisplay(*thinLensImage - *apertureCameraImage, "difference Picture: thinLens - apertureCamera");
    disp2 = cimg_library::CImgDisplay(*thinLensImage - *dualApertureCameraImage, "difference Picture: thinsLens - dualApertureCamera");
    disp3 = cimg_library::CImgDisplay(*dualApertureCameraImage - *apertureCameraImage, "difference Picture: dualApertureCamera - apertureCamera");

    while(!disp1.is_closed() && !disp2.is_closed() && !disp3.is_closed()){
    }

    */

//do not delete this 

}

