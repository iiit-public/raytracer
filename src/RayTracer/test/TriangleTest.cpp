// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file TriangleTest.cpp
 *  \author Thomas Nuernberg
 *  \date 21.04.2017
 *  \brief Unit test of class Triangle.
 */

#include "gtest/gtest.h"

#include "Scene.hpp"
#include "Objects/GeometricObject.hpp"
#include "Objects/Triangle.hpp"
#include "Spectrum/Monochromatic.hpp"
#include "Materials/Material.hpp"
#include "Materials/Matte.hpp"
#include "Utilities/Ray.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"

using namespace iiit;



/*! \brief Unit test of Triangle constructors.
 */
TEST(TriangleTest, ContstuctorTest)
{
    double tmin;
    std::shared_ptr<Scene<Monochromatic> > scene(new Scene<Monochromatic>);
    ShadingData<Monochromatic> shadingData(scene);

    // default constructor
    Triangle<Monochromatic> triangle1;
    Point3D origin(0.5, 0.5, 1.0);
    Vector3D direction(0.0, 0.0, -1.0);
    Ray ray(origin, direction);
    EXPECT_FALSE(triangle1.hit(ray, tmin, shadingData));
    EXPECT_FALSE(triangle1.shadowHit(ray, tmin));

    Triangle<Monochromatic> triangle2(Point3D(0.0, 0.0, 0.0), Point3D(2.0, 0.0, 0.0), Point3D(0.0, 2.0, 0.0));
    EXPECT_TRUE(triangle2.hit(ray, tmin, shadingData));
    EXPECT_TRUE(triangle2.shadowHit(ray, tmin));
    EXPECT_DOUBLE_EQ(1.0, tmin);
    EXPECT_EQ(Normal(0.0, 0.0, 1.0), shadingData.normal_);
    EXPECT_EQ(Point3D(0.5, 0.5, 0.0), shadingData.hitPoint_);

    Triangle<Monochromatic> triangle3(Point3D(0.0, 0.0, 0.0), Point3D(2.0, 0.0, 0.0), Point3D(0.0, 2.0, 0.0),
        std::shared_ptr<Material<Monochromatic> >(new Matte<Monochromatic>(Monochromatic(1.f))));
    EXPECT_TRUE(triangle3.hit(ray, tmin, shadingData));
    EXPECT_TRUE(triangle3.shadowHit(ray, tmin));

    Triangle<Monochromatic> triangle4(triangle3);
    EXPECT_TRUE(triangle4.hit(ray, tmin, shadingData));
    EXPECT_TRUE(triangle4.shadowHit(ray, tmin));

    Triangle<Monochromatic> triangle5(*(triangle3.clone()));
    EXPECT_TRUE(triangle5.hit(ray, tmin, shadingData));
    EXPECT_TRUE(triangle5.shadowHit(ray, tmin));
}



/*! \brief Unit test of Triangle get() and set() methods.
 */
TEST(TriangleTest, GetterSetterTest)
{
    Triangle<Monochromatic> disk;
    EXPECT_EQ(GeometricObject<Monochromatic>::Type::Triangle, disk.getType());
}



/*! \brief Unit test of Triangle hit() function.
 */
TEST(TriangleTest, HitTest)
{
    double tmin;
    std::shared_ptr<Scene<Monochromatic> > scene(new Scene<Monochromatic>);
    ShadingData<Monochromatic> shadingData(scene);

    Triangle<Monochromatic> triangle(Point3D(0.0, 0.0, 0.0), Point3D(1.0, 0.0, 0.0), Point3D(0.0, 1.0, 0.0));

    // ray passes through Triangle
    Point3D origin(0.25, 0.25, -0.5);
    Vector3D direction(0.0, 0.0, 1.0);
    Ray ray(origin, direction);
    EXPECT_TRUE(triangle.hit(ray, tmin, shadingData));
    EXPECT_TRUE(triangle.shadowHit(ray, tmin));

    // ray starts on face of Triangle
    origin.set(0.25, 0.25, 0.0);
    ray.set(origin, direction);
    EXPECT_FALSE(triangle.hit(ray, tmin, shadingData));
    EXPECT_FALSE(triangle.shadowHit(ray, tmin));

    // ray misses Triangle
    origin.set(1.5, 0.5, 0.0);
    ray.set(origin, direction);
    EXPECT_FALSE(triangle.hit(ray, tmin, shadingData));
    EXPECT_FALSE(triangle.shadowHit(ray, tmin));

    // ray tangential to Triangle
    origin.set(-1.5, -1.5, 0.0);
    direction.set(1.0, 1.0, 0.0);
    direction.normalize();
    ray.set(origin, direction);
    triangle.hit(ray, tmin, shadingData); // actual result irrelevant
    triangle.shadowHit(ray, tmin); // actual result irrelevant
}