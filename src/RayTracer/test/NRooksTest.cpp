// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file NRooksTest.cpp
 *  \author Thomas Nuernberg
 *  \date 21.06.2016
 *  \brief Unit test of class NRooks.
 */

#include "gtest/gtest.h"

#include "Sampler/NRooks.hpp"
#include "Utilities/Point2D.hpp"
#include "Utilities/Point3D.hpp"

using namespace iiit;



/*! \brief Unit test of NRooks sampler value range in unit square.
 */
TEST(NRooksTest, SampleTest)
{
    int numSamples = 256;
    NRooks nRooks(numSamples);
    nRooks.generateSamples();

    EXPECT_EQ(numSamples, nRooks.getNumSamples());
    for (int i = 0; i < nRooks.getNumSamples(); ++i)
    {
        Point2D samplePoint = nRooks.sampleUnitSquare();
        EXPECT_LE(0.0, samplePoint.x_);
        EXPECT_LE(0.0, samplePoint.y_);
        EXPECT_GE(1.0, samplePoint.x_);
        EXPECT_GE(1.0, samplePoint.y_);
    }
}



/*! \brief Unit test of NRooks sampler value range in unit disk.
 */
TEST(NRooksTest, DiskTest)
{
    int numSamples = 256;
    NRooks nRooks(numSamples);
    nRooks.generateSamples();
    nRooks.mapSamplesToUnitDisk();

    EXPECT_EQ(256, nRooks.getNumSamples());
    for (int i = 0; i < nRooks.getNumSamples(); ++i)
    {
        Point2D samplePoint = nRooks.sampleUnitDisk();
        EXPECT_LE(0.0, samplePoint.distance(Point2D(0.0, 0.0)));
        EXPECT_GE(1.0, samplePoint.distance(Point2D(0.0, 0.0)));
    }
}



/*! \brief Unit test of NRooks sampler value range in hemisphere.
 */
TEST(NRooksTest, HemisphereTest)
{
    int numSamples = 256;
    NRooks nRooks(numSamples);
    nRooks.generateSamples();
    nRooks.mapSamplesToHemisphere(1.f);

    EXPECT_EQ(256, nRooks.getNumSamples());
    for (int i = 0; i < nRooks.getNumSamples(); ++i)
    {
        Point3D samplePoint = nRooks.sampleHemisphere();
        EXPECT_LE(0.0, samplePoint.z_);
        EXPECT_DOUBLE_EQ(1.0, samplePoint.distance(Point3D(0.0, 0.0, 0.0)));
    }
}



/*! \brief Unit test of NRooks sampler value range in hemisphere.
 */
TEST(NRooksTest, SphereTest)
{
    int numSamples = 256;
    NRooks nRooks(numSamples);
    nRooks.generateSamples();
    nRooks.mapSamplesToSphere();

    EXPECT_EQ(256, nRooks.getNumSamples());
    for (int i = 0; i < nRooks.getNumSamples(); ++i)
    {
        Point3D samplePoint = nRooks.sampleUnitSphere();
        EXPECT_DOUBLE_EQ(1.0, samplePoint.distance(Point3D(0.0, 0.0, 0.0)));
    }
}