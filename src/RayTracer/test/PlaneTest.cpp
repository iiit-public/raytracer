// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file PlaneTest.cpp
 *  \author Thomas Nuernberg
 *  \date 24.04.2015
 *  \brief Unit test of class Plane.
 */

#include "gtest/gtest.h"

#include "Scene.hpp"
#include "Spectrum/RgbSpectrum.hpp"
#include "Objects/Plane.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/Normal.hpp"
#include "Utilities/Ray.hpp"
#include "Utilities/ShadingData.hpp"

using namespace iiit;



/*! \brief Unit of plane hit() function.
 *
 *  Test of a ray hitting a plane.
 */
TEST(PlaneTest, HitPlaneTest)
{
    Point3D planePoint(0.0, 0.0, 0.0);
    Normal planeNormal(0.0, 0.0, 1.0);
    Plane<RgbSpectrum> plane(planePoint, planeNormal);

    Point3D rayPoint(1.0, 1.0, 1.0);
    Vector3D rayVector(-1.0, -1.0, -1.0);
    double length = rayVector.length();
    rayVector.normalize();
    Ray ray(rayPoint, rayVector);

    double tmin;
    std::shared_ptr<Scene<RgbSpectrum> > scene(new Scene<RgbSpectrum>);
    ShadingData<RgbSpectrum> shadingData(scene);
    bool hit = plane.hit(ray, tmin, shadingData);
    EXPECT_TRUE(hit);
    EXPECT_DOUBLE_EQ(length, tmin);
    EXPECT_EQ(planeNormal, shadingData.normal_);
    EXPECT_EQ(planePoint, shadingData.hitPoint_);
    hit = plane.shadowHit(ray, tmin);
    EXPECT_TRUE(hit);
}



/*! \brief Unit of plane hit() function.
 *
 *  Test of a ray missing a plane.
 */
TEST(PlaneTest, MissPlaneTest)
{
    Point3D planePoint(0.0, 0.0, 0.0);
    Normal planeNormal(0.0, 0.0, 1.0);
    Plane<RgbSpectrum> plane(planePoint, planeNormal);

    Point3D rayPoint(1.0, 1.0, 1.0);
    Vector3D rayVector(-1.0, -1.0, 0.0);
    rayVector.normalize();
    Ray ray(rayPoint, rayVector);

    double tmin;
    std::shared_ptr<Scene<RgbSpectrum> > scene(new Scene<RgbSpectrum>);
    ShadingData<RgbSpectrum> shadingData(scene);
    bool hit = plane.hit(ray, tmin, shadingData);
    EXPECT_FALSE(hit);
    hit = plane.shadowHit(ray, tmin);
}



/*! \brief Unit of plane hit() function.
 *
 *  Test of a ray in a plane.
 */
TEST(PlaneTest, InPlaneTest)
{
    Point3D planePoint(0.0, 0.0, 0.0);
    Normal planeNormal(0.0, 0.0, 1.0);
    Plane<RgbSpectrum> plane(planePoint, planeNormal);

    Point3D rayPoint(1.0, 1.0, 0.0);
    Vector3D rayVector(-1.0, -1.0, 0.0);
    rayVector.normalize();
    Ray ray(rayPoint, rayVector);

    double tmin;
    std::shared_ptr<Scene<RgbSpectrum> > scene(new Scene<RgbSpectrum>);
    ShadingData<RgbSpectrum> shadingData(scene);
    bool hit = plane.hit(ray, tmin, shadingData);
    EXPECT_FALSE(hit); // design choice
    hit = plane.shadowHit(ray, tmin);
    EXPECT_FALSE(hit); // design choice
}



/*! \brief Unit of sphere calculateLocalHitPoint() function.
 */
TEST(PlaneTest, LocalHitPointTest1)
{
    //Define Plane
    Point3D planePoint(0.0, 0.0, 0.0);
    Normal planeNormal(0.0, 0.0, 1.0);
    Plane<RgbSpectrum> plane(planePoint, planeNormal);

    //Define Ray
    Point3D rayPoint(1.0, 2.0, 1.0);
    Vector3D rayVector(0.0, 0.0, -1.0);
    rayVector.normalize();
    Ray ray(rayPoint, rayVector);
    
    //Perform hit to calculate local hitpoint
    double tmin;
    std::shared_ptr<Scene<RgbSpectrum> > scene(new Scene<RgbSpectrum>);
    ShadingData<RgbSpectrum> shadingData(scene);
    plane.hit(ray, tmin, shadingData);
    EXPECT_EQ(Point2D(1.0, 2.0), shadingData.localHitPoint2D_); 
}



/*! \brief Unit of sphere calculateLocalHitPoint() function.
 */
TEST(PlaneTest, LocalHitPointTest2)
{
    //Define Plane
    Point3D planePoint(0.0, 0.0, 0.0);
    Normal planeNormal(0.0, 1.0, 0.0);
    Plane<RgbSpectrum> plane(planePoint, planeNormal);

    //Define Ray
    Point3D rayPoint(1.0, 1.0, 2.0);
    Vector3D rayVector(0.0, -1.0, 0.0);
    rayVector.normalize();
    Ray ray(rayPoint, rayVector);
    
    //Perform hit to calculate local hitpoint
    double tmin;
    std::shared_ptr<Scene<RgbSpectrum> > scene(new Scene<RgbSpectrum>);
    ShadingData<RgbSpectrum> shadingData(scene);
    plane.hit(ray, tmin, shadingData);
    EXPECT_EQ(Point2D(1.0, -2.0), shadingData.localHitPoint2D_); 
}



/*! \brief Unit of sphere calculateLocalHitPoint() function.
 */
TEST(PlaneTest, LocalHitPointTest3)
{
    //Define Plane
    Point3D planePoint(0.0, 0.0, 0.0);
    Normal planeNormal(1.0, 0.0, 0.0);
    Plane<RgbSpectrum> plane(planePoint, planeNormal);

    //Define Ray
    Point3D rayPoint(1.0, 1.0, 2.0);
    Vector3D rayVector(-1.0, 0.0, 0.0);
    rayVector.normalize();
    Ray ray(rayPoint, rayVector);
    
    //Perform hit to calculate local hitpoint
    double tmin;
    std::shared_ptr<Scene<RgbSpectrum> > scene(new Scene<RgbSpectrum>);
    ShadingData<RgbSpectrum> shadingData(scene);
    plane.hit(ray, tmin, shadingData);
    EXPECT_EQ(Point2D(1.0, 2.0), shadingData.localHitPoint2D_); 
}