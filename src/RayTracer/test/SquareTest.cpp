// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file SquareTest.cpp
 *  \author Thomas Nuernberg
 *  \date 21.04.2017
 *  \brief Unit test of class Square.
 */

#include "gtest/gtest.h"

#include "Scene.hpp"
#include "Objects/GeometricObject.hpp"
#include "Objects/Square.hpp"
#include "Spectrum/Monochromatic.hpp"
#include "Materials/Material.hpp"
#include "Materials/Matte.hpp"
#include "Utilities/Ray.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"

using namespace iiit;



/*! \brief Unit test of Square constructors.
 */
TEST(SquareTest, ContstuctorTest)
{
    double tmin;
    std::shared_ptr<Scene<Monochromatic> > scene(new Scene<Monochromatic>);
    ShadingData<Monochromatic> shadingData(scene);

    // default constructor
    Square<Monochromatic> square1;
    Point3D origin(0.5, 0.5, 1.0);
    Vector3D direction(0.0, 0.0, -1.0);
    Ray ray(origin, direction);
    EXPECT_FALSE(square1.hit(ray, tmin, shadingData));
    EXPECT_FALSE(square1.shadowHit(ray, tmin));

    Square<Monochromatic> square2(Point3D(0.0, 0.0, 0.0), Vector3D(1.0, 0.0, 0.0), Vector3D(0.0, 1.0, 0.0));
    EXPECT_TRUE(square2.hit(ray, tmin, shadingData));
    EXPECT_TRUE(square2.shadowHit(ray, tmin));
    EXPECT_DOUBLE_EQ(1.0, tmin);
    EXPECT_EQ(Normal(0.0, 0.0, 1.0), shadingData.normal_);
    EXPECT_EQ(Point3D(0.5, 0.5, 0.0), shadingData.hitPoint_);

    Square<Monochromatic> square3(Point3D(0.0, 0.0, 0.0), Vector3D(1.0, 0.0, 0.0), Vector3D(0.0, 1.0, 0.0),
        std::shared_ptr<Material<Monochromatic> >(new Matte<Monochromatic>(Monochromatic(1.f))));
    EXPECT_TRUE(square3.hit(ray, tmin, shadingData));
    EXPECT_TRUE(square3.shadowHit(ray, tmin));

    Square<Monochromatic> square4(Point3D(0.0, 0.0, 0.0), Vector3D(1.0, 0.0, 0.0), Vector3D(0.0, 1.0, 0.0), Normal(0.0, 0.0, 1.0));
    EXPECT_TRUE(square4.hit(ray, tmin, shadingData));
    EXPECT_TRUE(square4.shadowHit(ray, tmin));

    Square<Monochromatic> square5(Point3D(0.0, 0.0, 0.0), Vector3D(1.0, 0.0, 0.0), Vector3D(0.0, 1.0, 0.0), Normal(0.0, 0.0, 1.0),
        std::shared_ptr<Material<Monochromatic> >(new Matte<Monochromatic>(Monochromatic(1.f))));
    EXPECT_TRUE(square5.hit(ray, tmin, shadingData));
    EXPECT_TRUE(square5.shadowHit(ray, tmin));

    Square<Monochromatic> square6(square5);
    EXPECT_TRUE(square6.hit(ray, tmin, shadingData));
    EXPECT_TRUE(square6.shadowHit(ray, tmin));

    Square<Monochromatic> square7(*(square5.clone()));
    EXPECT_TRUE(square7.hit(ray, tmin, shadingData));
    EXPECT_TRUE(square7.shadowHit(ray, tmin));
}



/*! \brief Unit test of Square get() and set() methods.
 */
TEST(SquareTest, GetterSetterTest)
{
    Square<Monochromatic> square;
    EXPECT_EQ(GeometricObject<Monochromatic>::Type::Square, square.getType());

    Point3D point(1.0, 1.0, 1.0);
    square.setPoint(point);
    EXPECT_EQ(point, square.getPoint());

    square.setPoint(point.x_, point.y_, point.z_);
    EXPECT_EQ(point, square.getPoint());

    Vector3D a(1.0, 0.0, 0.0);
    square.setA(a);
    EXPECT_EQ(a, square.getA());

    Vector3D b(0.0, 1.0, 0.0);
    square.setB(b);
    EXPECT_EQ(b, square.getB());
    
    Normal normal(0.0, 0.0, 1.0);
    square.setNormal(normal);
    EXPECT_EQ(normal, square.getNormal());

    square.setNormal(normal.x_, normal.y_, normal.z_);
    EXPECT_EQ(normal, square.getNormal());
}



/*! \brief Unit test of Square hit() function.
 */
TEST(SquareTest, HitTest)
{
    double tmin;
    std::shared_ptr<Scene<Monochromatic> > scene(new Scene<Monochromatic>);
    ShadingData<Monochromatic> shadingData(scene);

    Square<Monochromatic> square(Point3D(0.0, 0.0, 0.0), Vector3D(1.0, 0.0, 0.0), Vector3D(0.0, 1.0, 0.0));

    // ray passes through Square
    Point3D origin(0.5, 0.5, -0.5);
    Vector3D direction(0.1, 0.25, 1.0);
    direction.normalize();
    Ray ray(origin, direction);
    EXPECT_TRUE(square.hit(ray, tmin, shadingData));
    EXPECT_TRUE(square.shadowHit(ray, tmin));

    // ray starts on face of Square
    origin.set(0.5, 0.5, 0.0);
    ray.set(origin, direction);
    EXPECT_FALSE(square.hit(ray, tmin, shadingData));
    EXPECT_FALSE(square.shadowHit(ray, tmin));

    // ray misses Square
    origin.set(1.5, 0.5, 0.0);
    ray.set(origin, direction);
    EXPECT_FALSE(square.hit(ray, tmin, shadingData));
    EXPECT_FALSE(square.shadowHit(ray, tmin));

    // ray tangential to Square
    origin.set(-0.5, -0.5, 0.0);
    direction.set(1.0, 1.0, 0.0);
    direction.normalize();
    ray.set(origin, direction);
    square.hit(ray, tmin, shadingData); // actual result irrelevant
    square.shadowHit(ray, tmin); // actual result irrelevant
}