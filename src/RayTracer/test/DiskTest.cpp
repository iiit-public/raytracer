// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file DiskTest.cpp
 *  \author Thomas Nuernberg
 *  \date 21.04.2017
 *  \brief Unit test of class Disk.
 */

#include "gtest/gtest.h"

#include "Scene.hpp"
#include "Objects/GeometricObject.hpp"
#include "Objects/Disk.hpp"
#include "Spectrum/Monochromatic.hpp"
#include "Materials/Material.hpp"
#include "Materials/Matte.hpp"
#include "Utilities/Ray.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"

using namespace iiit;



/*! \brief Unit test of Disk constructors.
 */
TEST(DiskTest, ContstuctorTest)
{
    double tmin;
    std::shared_ptr<Scene<Monochromatic> > scene(new Scene<Monochromatic>);
    ShadingData<Monochromatic> shadingData(scene);

    // default constructor
    Disk<Monochromatic> disk1;
    Point3D origin(0.5, 0.5, 1.0);
    Vector3D direction(0.0, 0.0, -1.0);
    Ray ray(origin, direction);
    EXPECT_FALSE(disk1.hit(ray, tmin, shadingData));
    EXPECT_FALSE(disk1.shadowHit(ray, tmin));

    Disk<Monochromatic> disk2(Point3D(0.0, 0.0, 0.0), Normal(0.0, 0.0, 1.0), 1.0);
    EXPECT_TRUE(disk2.hit(ray, tmin, shadingData));
    EXPECT_TRUE(disk2.shadowHit(ray, tmin));
    EXPECT_DOUBLE_EQ(1.0, tmin);
    EXPECT_EQ(Normal(0.0, 0.0, 1.0), shadingData.normal_);
    EXPECT_EQ(Point3D(0.5, 0.5, 0.0), shadingData.hitPoint_);

    Disk<Monochromatic> disk3(Point3D(0.0, 0.0, 0.0), Normal(0.0, 0.0, 1.0), 1.0,
        std::shared_ptr<Material<Monochromatic> >(new Matte<Monochromatic>(Monochromatic(1.f))));
    EXPECT_TRUE(disk3.hit(ray, tmin, shadingData));
    EXPECT_TRUE(disk3.shadowHit(ray, tmin));

    Disk<Monochromatic> disk4(0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0);
    EXPECT_TRUE(disk4.hit(ray, tmin, shadingData));
    EXPECT_TRUE(disk4.shadowHit(ray, tmin));

    Disk<Monochromatic> disk5(0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0,
        std::shared_ptr<Material<Monochromatic> >(new Matte<Monochromatic>(Monochromatic(1.f))));
    EXPECT_TRUE(disk5.hit(ray, tmin, shadingData));
    EXPECT_TRUE(disk5.shadowHit(ray, tmin));

    Disk<Monochromatic> disk6(disk5);
    EXPECT_TRUE(disk6.hit(ray, tmin, shadingData));
    EXPECT_TRUE(disk6.shadowHit(ray, tmin));

    Disk<Monochromatic> disk7(*(disk5.clone()));
    EXPECT_TRUE(disk7.hit(ray, tmin, shadingData));
    EXPECT_TRUE(disk7.shadowHit(ray, tmin));
}



/*! \brief Unit test of Disk get() and set() methods.
 */
TEST(DiskTest, GetterSetterTest)
{
    Disk<Monochromatic> disk;
    EXPECT_EQ(GeometricObject<Monochromatic>::Type::Disk, disk.getType());

    Point3D point(1.0, 1.0, 1.0);
    disk.setPoint(point);
    EXPECT_EQ(point, disk.getPoint());

    disk.setPoint(point.x_, point.y_, point.z_);
    EXPECT_EQ(point, disk.getPoint());

    Normal normal(0.0, 0.0, 1.0);
    disk.setNormal(normal);
    EXPECT_EQ(normal, disk.getNormal());

    disk.setNormal(normal.x_, normal.y_, normal.z_);
    EXPECT_EQ(normal, disk.getNormal());

    disk.setRadius(1.0);
    EXPECT_EQ(1.0, disk.getRadius());
}



/*! \brief Unit test of Disk hit() function.
 */
TEST(DiskTest, HitTest)
{
    double tmin;
    std::shared_ptr<Scene<Monochromatic> > scene(new Scene<Monochromatic>);
    ShadingData<Monochromatic> shadingData(scene);

    Disk<Monochromatic> disk(Point3D(0.0, 0.0, 0.0), Normal(0.0, 0.0, 1.0), 1.0);

    // ray passes through Disk
    Point3D origin(0.0, 0.0, -0.5);
    Vector3D direction(0.1, 0.25, 1.0);
    direction.normalize();
    Ray ray(origin, direction);
    EXPECT_TRUE(disk.hit(ray, tmin, shadingData));
    EXPECT_TRUE(disk.shadowHit(ray, tmin));

    // ray starts on face of Disk
    origin.set(0.5, 0.5, 0.0);
    ray.set(origin, direction);
    EXPECT_FALSE(disk.hit(ray, tmin, shadingData));
    EXPECT_FALSE(disk.shadowHit(ray, tmin));

    // ray misses Disk
    origin.set(1.5, 0.5, 0.0);
    ray.set(origin, direction);
    EXPECT_FALSE(disk.hit(ray, tmin, shadingData));
    EXPECT_FALSE(disk.shadowHit(ray, tmin));

    // ray tangential to Disk
    origin.set(-1.5, -1.5, 0.0);
    direction.set(1.0, 1.0, 0.0);
    direction.normalize();
    ray.set(origin, direction);
    disk.hit(ray, tmin, shadingData); // actual result irrelevant
    disk.shadowHit(ray, tmin); // actual result irrelevant
}