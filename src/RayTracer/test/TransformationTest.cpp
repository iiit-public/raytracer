// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file TransformationTest.cpp
 *  \author Thomas Nuernberg
 *  \date 19.03.2015
 *  \brief Unit test of class Transformation.
 */

#include "gtest/gtest.h"

#include "Utilities/Transformation.hpp"
#include "Utilities/Matrix4x4.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/Point3D.hpp"

using namespace iiit;



/*! \brief Unit test of look at transformation.
 */
TEST(TransformationTest, LookAtTransformation)
{
    // identity transformation
    Point3D eye(0.0, 0.0, 0.0);
    Point3D lookAt(0.0, 0.0, 1.0);
    Vector3D up(0.0, -1.0, 0.0);
    ASSERT_NO_THROW(Transformation(eye, lookAt, up));
    Transformation identityTransformation(eye, lookAt, up);
    Matrix4x4 identityMatrix;
    EXPECT_EQ(identityMatrix, identityTransformation.getMatrix());
    EXPECT_EQ(identityMatrix, identityTransformation.getInvMatrix());


    // translation
    eye.set(1.0, 1.0, 1.0);
    lookAt.set(1.0, 1.0, 2.0);
    ASSERT_NO_THROW(Transformation(eye, lookAt, up));
    Transformation translation(eye, lookAt, up);
    Matrix4x4 translationMatrix;
    translationMatrix(1,4) = 1.0;
    translationMatrix(2,4) = 1.0;
    translationMatrix(3,4) = 1.0;
    EXPECT_EQ(translationMatrix, translation.getMatrix());
}



/*! \brief Unit test of look at transformation with linear dependent vectors.
 */
TEST(TransformationTest, LinearDependency)
{
    Point3D eye(0.0, 0.0, 0.0);
    Point3D lookAt(0.0, 0.0, 1.0);
    Vector3D up(0.0, 0.0, -1.0);
    EXPECT_THROW(Transformation(eye, lookAt, up), std::runtime_error);
}