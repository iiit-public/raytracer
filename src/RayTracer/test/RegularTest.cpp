// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file RegularTest.cpp
 *  \author Thomas Nuernberg
 *  \date 13.11.2015
 *  \brief Unit test of class Regular.
 */

#include "gtest/gtest.h"

#include "Sampler/Regular.hpp"
#include "Utilities/Point2D.hpp"
#include "Utilities/Point3D.hpp"

using namespace iiit;



/*! \brief Unit test of regular sampler value range in unit square.
 */
TEST(RegularTest, SampleTest)
{
    int numSamples = 256;
    Regular regular(numSamples);
    regular.generateSamples();

    EXPECT_EQ(numSamples, regular.getNumSamples());
    for (int i = 0; i < regular.getNumSamples(); ++i)
    {
        Point2D samplePoint = regular.sampleUnitSquare();
        EXPECT_LE(0.0, samplePoint.x_);
        EXPECT_LE(0.0, samplePoint.y_);
        EXPECT_GE(1.0, samplePoint.x_);
        EXPECT_GE(1.0, samplePoint.y_);
    }
}



/*! \brief Unit test of non-square number of samples.
 */
TEST(RegularTest, SquareRootTest)
{
    int numSamples = 255;
    Regular regular(numSamples);
    regular.generateSamples();

    EXPECT_EQ(256, regular.getNumSamples());
    for (int i = 0; i < regular.getNumSamples(); ++i)
    {
        Point2D samplePoint = regular.sampleUnitSquare();
        EXPECT_LE(0.0, samplePoint.x_);
        EXPECT_LE(0.0, samplePoint.y_);
        EXPECT_GE(1.0, samplePoint.x_);
        EXPECT_GE(1.0, samplePoint.y_);
    }
}



/*! \brief Unit test of regular sampler value range in unit disk.
 */
TEST(RegularTest, DiskTest)
{
    int numSamples = 256;
    Regular regular(numSamples);
    regular.generateSamples();
    regular.mapSamplesToUnitDisk();

    EXPECT_EQ(256, regular.getNumSamples());
    for (int i = 0; i < regular.getNumSamples(); ++i)
    {
        Point2D samplePoint = regular.sampleUnitDisk();
        EXPECT_LE(0.0, samplePoint.distance(Point2D(0.0, 0.0)));
        EXPECT_GE(1.0, samplePoint.distance(Point2D(0.0, 0.0)));
    }
}



/*! \brief Unit test of regular sampler value range in hemisphere.
 */
TEST(RegularTest, HemisphereTest)
{
    int numSamples = 256;
    Regular regular(numSamples);
    regular.generateSamples();
    regular.mapSamplesToHemisphere(1.f);

    EXPECT_EQ(256, regular.getNumSamples());
    for (int i = 0; i < regular.getNumSamples(); ++i)
    {
        Point3D samplePoint = regular.sampleHemisphere();
        EXPECT_LE(0.0, samplePoint.z_);
        EXPECT_DOUBLE_EQ(1.0, samplePoint.distance(Point3D(0.0, 0.0, 0.0)));
    }
}



/*! \brief Unit test of regular sampler value range in hemisphere.
 */
TEST(RegularTest, SphereTest)
{
    int numSamples = 256;
    Regular regular(numSamples);
    regular.generateSamples();
    regular.mapSamplesToSphere();

    EXPECT_EQ(256, regular.getNumSamples());
    for (int i = 0; i < regular.getNumSamples(); ++i)
    {
        Point3D samplePoint = regular.sampleUnitSphere();
        EXPECT_DOUBLE_EQ(1.0, samplePoint.distance(Point3D(0.0, 0.0, 0.0)));
    }
}