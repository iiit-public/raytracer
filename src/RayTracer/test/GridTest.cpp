// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file GridTest.cpp
 *  \author Thomas Nuernberg
 *  \date 26.05.2015
 *  \brief Unit test of class Grid.
 */

#include "gtest/gtest.h"

#include <memory>
#include <math.h>
#include <random>
#include <chrono>

#include "Scene.hpp"
#include "Spectrum/RgbSpectrum.hpp"
#include "Objects/Grid.hpp"
#include "Objects/Compound.hpp"
#include "Objects/BoundingBox.hpp"
#include "Objects/Sphere.hpp"
#include "Utilities/Ray.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Constants.h"

using namespace iiit;



/*! \brief Unit test of grid initGrid() function.
 */
TEST(GridTest, BuildTest)
{
    double t;
    std::shared_ptr<Scene<RgbSpectrum> > scene(new Scene<RgbSpectrum>);
    ShadingData<RgbSpectrum> shadingData(scene);

    Grid<RgbSpectrum> grid;
    std::shared_ptr<GeometricObject<RgbSpectrum> > sphere(new Sphere<RgbSpectrum>(Point3D(0.0, 0.0, 0.0), 1.0));
    grid.addObject(std::shared_ptr<GeometricObject<RgbSpectrum> >(sphere));
    grid.initGrid();

    std::cout << "num_cells = " << grid.getNumXCells() * grid.getNumYCells() * grid.getNumZCells() <<
        ": " << grid.getNumXCells() << " x " << grid.getNumYCells() << " x " << grid.getNumZCells() << std::endl;

    Ray hittingRay(Point3D(0.0, 2.0, 0.0), Vector3D(0.0, -1.0, 0.0));
    BoundingBox boundingBox = grid.getBoundingBox();
    EXPECT_TRUE(boundingBox.hit(hittingRay));
    EXPECT_TRUE(grid.hit(hittingRay, t, shadingData));
    Ray missingRay(Point3D(2.5, 2.0, 0.0), Vector3D(0.0, -1.0, 0.0));
    EXPECT_FALSE(boundingBox.hit(missingRay));
    EXPECT_FALSE(grid.hit(missingRay, t, shadingData));


    Grid<RgbSpectrum> grid2;
    std::shared_ptr<GeometricObject<RgbSpectrum> > sphere2(new Sphere<RgbSpectrum>(Point3D(5.0, 0.0, 0.0), 1.0));
    grid2.addObject(std::shared_ptr<GeometricObject<RgbSpectrum> >(sphere));
    grid2.addObject(std::shared_ptr<GeometricObject<RgbSpectrum> >(sphere2));
    grid2.initGrid();

    std::cout << "num_cells = " << grid2.getNumXCells() * grid2.getNumYCells() * grid2.getNumZCells() <<
        ": " << grid2.getNumXCells() << " x " << grid2.getNumYCells() << " x " << grid2.getNumZCells() << std::endl;

    missingRay.set(Point3D(2.5, 2.0, 0.0), Vector3D(0.0, -1.0, 0.0));
    boundingBox = grid2.getBoundingBox();
    EXPECT_TRUE(boundingBox.hit(hittingRay));
    EXPECT_TRUE(grid2.hit(hittingRay, t, shadingData));
    EXPECT_TRUE(boundingBox.hit(missingRay));
    EXPECT_FALSE(grid2.hit(missingRay, t, shadingData));

    Grid<RgbSpectrum> grid3;
    std::shared_ptr<GeometricObject<RgbSpectrum> > sphere3(new Sphere<RgbSpectrum>(Point3D(5.0, 0.0, 5.0), 1.0));
    grid3.addObject(std::shared_ptr<GeometricObject<RgbSpectrum> >(sphere));
    grid3.addObject(std::shared_ptr<GeometricObject<RgbSpectrum> >(sphere2));
    grid3.addObject(std::shared_ptr<GeometricObject<RgbSpectrum> >(sphere3));
    grid3.initGrid();

    std::cout << "num_cells = " << grid3.getNumXCells() * grid3.getNumYCells() * grid3.getNumZCells() <<
        ": " << grid3.getNumXCells() << " x " << grid3.getNumYCells() << " x " << grid3.getNumZCells() << std::endl;

    boundingBox = grid3.getBoundingBox();
    EXPECT_TRUE(boundingBox.hit(hittingRay));
    EXPECT_TRUE(grid3.hit(hittingRay, t, shadingData));
    EXPECT_TRUE(boundingBox.hit(missingRay));
    EXPECT_FALSE(grid3.hit(missingRay, t, shadingData));

    Grid<RgbSpectrum> grid4;
    std::shared_ptr<GeometricObject<RgbSpectrum> > sphere4(new Sphere<RgbSpectrum>(Point3D(5.0, 5.0, 5.0), 1.0));
    grid4.addObject(std::shared_ptr<GeometricObject<RgbSpectrum> >(sphere));
    grid4.addObject(std::shared_ptr<GeometricObject<RgbSpectrum> >(sphere2));
    grid4.addObject(std::shared_ptr<GeometricObject<RgbSpectrum> >(sphere3));
    grid4.addObject(std::shared_ptr<GeometricObject<RgbSpectrum> >(sphere4));
    grid4.initGrid();

    missingRay.set(Point3D(2.5, 2.0, 2.5), Vector3D(0.0, -1.0, 0.0));
    boundingBox = grid4.getBoundingBox();
    EXPECT_TRUE(boundingBox.hit(hittingRay));
    EXPECT_TRUE(grid4.hit(hittingRay, t, shadingData));
    EXPECT_TRUE(boundingBox.hit(missingRay));
    EXPECT_FALSE(grid4.hit(missingRay, t, shadingData));

    std::cout << "num_cells = " << grid4.getNumXCells() * grid4.getNumYCells() * grid4.getNumZCells() <<
        ": " << grid4.getNumXCells() << " x " << grid4.getNumYCells() << " x " << grid4.getNumZCells() << std::endl;
}



 /*! \brief Unit test of grid hit() function.
  */
TEST(GridTest, RayOriginTest)
{
    double t;
    std::shared_ptr<Scene<RgbSpectrum> > scene(new Scene<RgbSpectrum>);
    ShadingData<RgbSpectrum> shadingData(scene);

    Grid<RgbSpectrum> grid;
    std::shared_ptr<GeometricObject<RgbSpectrum> > sphere1(new Sphere<RgbSpectrum>(Point3D(0.0, 0.0, 0.0), 1.0));
    grid.addObject(std::shared_ptr<GeometricObject<RgbSpectrum> >(sphere1));
    std::shared_ptr<GeometricObject<RgbSpectrum> > sphere2(new Sphere<RgbSpectrum>(Point3D(5.0, 0.0, 0.0), 1.0));
    grid.addObject(std::shared_ptr<GeometricObject<RgbSpectrum> >(sphere2));
    grid.initGrid();

    // rays starting outside grid
    Ray hittingRay(Point3D(0.0, 2.0, 0.0), Vector3D(0.0, -1.0, 0.0));
    EXPECT_TRUE(grid.hit(hittingRay, t, shadingData));
    Ray missingRay(Point3D(2.5, 2.0, 0.0), Vector3D(0.0, -1.0, 0.0));
    EXPECT_FALSE(grid.hit(missingRay, t, shadingData));

    // rays starting on grid edge
    hittingRay.set(Point3D(0.0, 1.0, 0.0), Vector3D(0.0, -1.0, 0.0));
    EXPECT_TRUE(grid.hit(hittingRay, t, shadingData));
    missingRay.set(Point3D(2.5, 1.0, 0.0), Vector3D(0.0, -1.0, 0.0));
    EXPECT_FALSE(grid.hit(missingRay, t, shadingData));
    hittingRay.set(Point3D(0.0, -1.0, 0.0), Vector3D(0.0, -1.0, 0.0));
    EXPECT_FALSE(grid.hit(hittingRay, t, shadingData)); // ray points away from object
    missingRay.set(Point3D(2.5, -1.0, 0.0), Vector3D(0.0, -1.0, 0.0));
    EXPECT_FALSE(grid.hit(missingRay, t, shadingData));

    // rays starting inside grid
    hittingRay.set(Point3D(0.0, 0.0, 0.0), Vector3D(0.0, -1.0, 0.0));
    EXPECT_TRUE(grid.hit(hittingRay, t, shadingData));
    missingRay.set(Point3D(2.5, 0.0, 0.0), Vector3D(0.0, -1.0, 0.0));
    EXPECT_FALSE(grid.hit(missingRay, t, shadingData));
}



/*! \brief Unit test of grid performance.
 *
 *  Test constructs a scene with 100000 randomly placed spheres. The performance gain of calculating
 *  ray-object intersections with a grid is compared to the additional computational cost of
 *  building the grid.
 */
TEST(GridTest, RandomSpheresTest)
{
    double t;
    std::shared_ptr<Scene<RgbSpectrum> > scene(new Scene<RgbSpectrum>);
    ShadingData<RgbSpectrum> shadingData(scene);

    // build random spheres
    int numSpheres = 100000;
    double sphereVolume = 0.1 / numSpheres; // all spheres combined occupy 10% of the unit cube
    double radius = pow(sphereVolume * 3.0 / 4.0 * PI, 1.0 / 3.0);

    std::mt19937 generator; // default seed
    std::uniform_real_distribution<double> distribution(0.0, 1.0);

    Grid<RgbSpectrum> grid;
    Compound<RgbSpectrum> compound;

    for (int i = 0; i < numSpheres; ++i)
    {
        std::shared_ptr<Sphere<RgbSpectrum>> sphere(new Sphere<RgbSpectrum>(Point3D(distribution(generator), distribution(generator), distribution(generator)), radius));
        grid.addObject(sphere);
        compound.addObject(sphere);
    }

    // grid initialization

    // start time
    std::chrono::system_clock::time_point startTime = std::chrono::system_clock::now();

    // init grid
    grid.initGrid();

    // duration
    auto initDuration = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - startTime);
    std::cout << "Grid initialization time: " << initDuration.count() << " ms" << std::endl;

    std::cout << "num_cells = " << grid.getNumXCells() * grid.getNumYCells() * grid.getNumZCells() <<
        ": " << grid.getNumXCells() << " x " << grid.getNumYCells() << " x " << grid.getNumZCells() << std::endl;


    // grid ray intersections

    // generate random rays
    int gridN = 100000; // varying intersection time expected
    std::vector<Ray> gridRays;
    gridRays.resize(gridN);
    for (std::vector<Ray>::iterator it = gridRays.begin(); it != gridRays.end(); ++it)
    {
        it->set(Point3D(-1.0, distribution(generator), distribution(generator)), Vector3D(1.0, 0.0, 0.0));
    }

    // start time
    startTime = std::chrono::system_clock::now();

    // intersect with grid
    for (std::vector<Ray>::iterator it = gridRays.begin(); it != gridRays.end(); ++it)
    {
        grid.hit(*it, t, shadingData);
    }

    // duration
    auto gridDuration = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - startTime);
    std::cout << "Mean time for grid-ray-intersection: " << gridDuration.count() / static_cast<float>(gridN) << " ms" << std::endl;


    // compound ray intersections

    // generate random rays
    int compoundN = 10; // constant intersection time expected
    std::vector<Ray> compoundRays;
    compoundRays.resize(compoundN);
    for (std::vector<Ray>::iterator it = compoundRays.begin(); it != compoundRays.end(); ++it)
    {
        it->set(Point3D(-1.0, distribution(generator), distribution(generator)), Vector3D(1.0, 0.0, 0.0));
    }

    // start time
    startTime = std::chrono::system_clock::now();

    // intersect with grid
    for (std::vector<Ray>::iterator it = compoundRays.begin(); it != compoundRays.end(); ++it)
    {
        compound.hit(*it, t, shadingData);
    }

    // duration
    auto compoundDuration = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - startTime);
    std::cout << "Mean time for compound-ray-intersection: " << compoundDuration.count() / static_cast<float>(compoundN) << " ms" << std::endl;


    // comparison
    EXPECT_LT(gridDuration / gridN, compoundDuration / compoundN);

    std::cout << "Speed-up factor: " << static_cast<float>(compoundDuration.count()) / compoundN / static_cast<float>(gridDuration.count()) * gridN  << std::endl;
    int gainLimit = static_cast<int>(static_cast<float>(initDuration.count()) /
        (static_cast<float>(compoundDuration.count()) / compoundN - static_cast<float>(gridDuration.count()) / gridN));
    std::cout << "Performance gain using grid after " << gainLimit << " ray intersections." << std::endl;
}