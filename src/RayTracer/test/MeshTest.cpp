// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file MeshTest.cpp
*  \author Thomas Nuernberg
*  \date 21.04.2017
*  \brief Unit tests for triangle meshes.
*/

#include "gtest/gtest.h"

#include "Scene.hpp"
#include "Objects/GeometricObject.hpp"
#include "Objects/TriangleMesh.hpp"
#include "Objects/Mesh.hpp"
#include "Objects/MeshTriangle.hpp"
#include "Objects/FlatMeshTriangle.hpp"
#include "Objects/SmoothMeshTriangle.hpp"
#include "Spectrum/Monochromatic.hpp"
#include "Materials/Material.hpp"
#include "Materials/Matte.hpp"
#include "Utilities/Ray.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"

using namespace iiit;



/*! \brief Unit test of TriangleMesh with FlatMeshTriangle faces.
 */
TEST(MeshTest, FlatMeshTest)
{
    int numVertices = 4;
    int numFaces = 2;
    TriangleMesh<Monochromatic> triangleMesh;

    // reserve memory
    triangleMesh.mesh_->vertices_.reserve(numVertices);
    triangleMesh.reserve(numFaces);

    triangleMesh.mesh_->numVertices_ = numVertices;
    triangleMesh.mesh_->numTriangles_ = numFaces;

    // add vertices
    triangleMesh.mesh_->vertices_.push_back(Point3D(-1.0, -1.0, 1.0));
    triangleMesh.mesh_->vertices_.push_back(Point3D(1.0, -1.0, 0.0));
    triangleMesh.mesh_->vertices_.push_back(Point3D(1.0, 1.0, 1.0));
    triangleMesh.mesh_->vertices_.push_back(Point3D(-1.0, 1.0, 0.0));

    // add faces
    std::shared_ptr<FlatMeshTriangle<Monochromatic> > triangle(new FlatMeshTriangle<Monochromatic>(
        std::weak_ptr<Mesh>(triangleMesh.mesh_), 0, 1, 2));
    triangleMesh.addObject(triangle);

    triangle.reset(new FlatMeshTriangle<Monochromatic>(
        std::weak_ptr<Mesh>(triangleMesh.mesh_), 0, 2, 3));
    triangleMesh.addObject(triangle);

    triangleMesh.setMaterial(std::shared_ptr<Material<Monochromatic> >(new Matte<Monochromatic>(Monochromatic(1.f))));
    triangleMesh.initGrid();

    // test object intersections
    double tmin;
    std::shared_ptr<Scene<Monochromatic> > scene(new Scene<Monochromatic>);
    ShadingData<Monochromatic> shadingData(scene);

    // hitting rays
    Point3D origin(0.5, 0.5, 0.0);
    Vector3D direction(0.0, 0.0, 1.0);
    Ray ray(origin, direction);
    EXPECT_TRUE(triangleMesh.hit(ray, tmin, shadingData));
    EXPECT_TRUE(triangleMesh.shadowHit(ray, tmin));
    EXPECT_DOUBLE_EQ(1.0, tmin);
    EXPECT_EQ(Point3D(0.5, 0.5, 1.0), shadingData.hitPoint_);

    origin.set(0.5, -0.5, 0.0);
    ray.set(origin, direction);
    EXPECT_TRUE(triangleMesh.hit(ray, tmin, shadingData));
    EXPECT_TRUE(triangleMesh.shadowHit(ray, tmin));
    EXPECT_DOUBLE_EQ(0.5, tmin);
    EXPECT_EQ(Point3D(0.5, -0.5, 0.5), shadingData.hitPoint_);

    // missing rays
    origin.set(1.5, -1.5, 0.0);
    ray.set(origin, direction);
    EXPECT_FALSE(triangleMesh.hit(ray, tmin, shadingData));
    EXPECT_FALSE(triangleMesh.shadowHit(ray, tmin));

    origin.set(2.0, 2.0, 0.5);
    direction.set(-1.0, -1.0, 0.0);
    direction.normalize();
    ray.set(origin, direction);
    EXPECT_FALSE(triangleMesh.hit(ray, tmin, shadingData));
    EXPECT_FALSE(triangleMesh.shadowHit(ray, tmin));

    // tangential rays
    origin.set(2.0, 2.0, 1.0);
    ray.set(origin, direction);
    triangleMesh.hit(ray, tmin, shadingData); // actual result irrelevant
    triangleMesh.shadowHit(ray, tmin); // actual result irrelevant

    origin.set(2.0, -2.0, 1.0);
    direction.set(-1.0, 1.0, 0.0);
    direction.normalize();
    ray.set(origin, direction);
    triangleMesh.hit(ray, tmin, shadingData); // actual result irrelevant
    triangleMesh.shadowHit(ray, tmin); // actual result irrelevant
}



/*! \brief Unit test of TriangleMesh with SmoothMeshTriangle faces.
*/
TEST(MeshTest, SmoothMeshTest)
{
    int numVertices = 4;
    int numFaces = 2;
    TriangleMesh<Monochromatic> triangleMesh;

    // reserve memory
    triangleMesh.mesh_->vertices_.reserve(numVertices);
    triangleMesh.reserve(numFaces);

    triangleMesh.mesh_->numVertices_ = numVertices;
    triangleMesh.mesh_->numTriangles_ = numFaces;

    for (int j = 0; j < triangleMesh.mesh_->numVertices_; ++j)
    {
        triangleMesh.mesh_->vertexFaces_.push_back(std::vector<int>());
    }

    // add vertices
    triangleMesh.mesh_->vertices_.push_back(Point3D(-1.0, -1.0, 1.0));
    triangleMesh.mesh_->vertices_.push_back(Point3D(1.0, -1.0, 0.0));
    triangleMesh.mesh_->vertices_.push_back(Point3D(1.0, 1.0, 1.0));
    triangleMesh.mesh_->vertices_.push_back(Point3D(-1.0, 1.0, 0.0));

    // add faces
    std::shared_ptr<SmoothMeshTriangle<Monochromatic> > triangle(new SmoothMeshTriangle<Monochromatic>(
        std::weak_ptr<Mesh>(triangleMesh.mesh_), 0, 1, 2));
    triangleMesh.addObject(triangle);
    triangleMesh.mesh_->vertexFaces_[0].push_back(static_cast<int>(triangleMesh.objects_.size() - 1));
    triangleMesh.mesh_->vertexFaces_[1].push_back(static_cast<int>(triangleMesh.objects_.size() - 1));
    triangleMesh.mesh_->vertexFaces_[2].push_back(static_cast<int>(triangleMesh.objects_.size() - 1));
    triangle->computeNormal(false);

    triangle.reset(new SmoothMeshTriangle<Monochromatic>(
        std::weak_ptr<Mesh>(triangleMesh.mesh_), 0, 2, 3));
    triangleMesh.addObject(triangle);
    triangleMesh.mesh_->vertexFaces_[0].push_back(static_cast<int>(triangleMesh.objects_.size() - 1));
    triangleMesh.mesh_->vertexFaces_[2].push_back(static_cast<int>(triangleMesh.objects_.size() - 1));
    triangleMesh.mesh_->vertexFaces_[3].push_back(static_cast<int>(triangleMesh.objects_.size() - 1));
    triangle->computeNormal(false);

    triangleMesh.computeMeshNormals();

    triangleMesh.setMaterial(std::shared_ptr<Material<Monochromatic> >(new Matte<Monochromatic>(Monochromatic(1.f))));
    triangleMesh.initGrid();

    // test object intersections
    double tmin;
    std::shared_ptr<Scene<Monochromatic> > scene(new Scene<Monochromatic>);
    ShadingData<Monochromatic> shadingData(scene);

    // hitting rays
    Point3D origin(0.5, 0.5, 0.0);
    Vector3D direction(0.0, 0.0, 1.0);
    Ray ray(origin, direction);
    EXPECT_TRUE(triangleMesh.hit(ray, tmin, shadingData));
    EXPECT_TRUE(triangleMesh.shadowHit(ray, tmin));
    EXPECT_DOUBLE_EQ(1.0, tmin);
    EXPECT_EQ(Point3D(0.5, 0.5, 1.0), shadingData.hitPoint_);

    origin.set(0.5, -0.5, 0.0);
    ray.set(origin, direction);
    EXPECT_TRUE(triangleMesh.hit(ray, tmin, shadingData));
    EXPECT_TRUE(triangleMesh.shadowHit(ray, tmin));
    EXPECT_DOUBLE_EQ(0.5, tmin);
    EXPECT_EQ(Point3D(0.5, -0.5, 0.5), shadingData.hitPoint_);

    // missing rays
    origin.set(1.5, -1.5, 0.0);
    ray.set(origin, direction);
    EXPECT_FALSE(triangleMesh.hit(ray, tmin, shadingData));
    EXPECT_FALSE(triangleMesh.shadowHit(ray, tmin));

    origin.set(2.0, 2.0, 0.5);
    direction.set(-1.0, -1.0, 0.0);
    direction.normalize();
    ray.set(origin, direction);
    EXPECT_FALSE(triangleMesh.hit(ray, tmin, shadingData));
    EXPECT_FALSE(triangleMesh.shadowHit(ray, tmin));

    // tangential rays
    origin.set(2.0, 2.0, 1.0);
    ray.set(origin, direction);
    triangleMesh.hit(ray, tmin, shadingData); // actual result irrelevant
    triangleMesh.shadowHit(ray, tmin); // actual result irrelevant

    origin.set(2.0, -2.0, 1.0);
    direction.set(-1.0, 1.0, 0.0);
    direction.normalize();
    ray.set(origin, direction);
    triangleMesh.hit(ray, tmin, shadingData); // actual result irrelevant
    triangleMesh.shadowHit(ray, tmin); // actual result irrelevant
}