// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file VectorTest.cpp
 *  \author Thomas Nuernberg
 *  \date 16.03.2015
 *  \brief Unit test of class Vector3D.
 */

#include "gtest/gtest.h"

#include "Utilities/Constants.h"
#include "Utilities/Vector3D.hpp"
#include "Utilities/Point3D.hpp"

using namespace iiit;



/*! \brief Unit test of vector comparison.
 */
TEST(VectorTest, VectorComparison)
{
    Vector3D v1(1.0, 0.5, 2.2);
    EXPECT_EQ(v1, v1);

    Vector3D v2(-4.0, 1.45e4, 86.22);
    EXPECT_NE(v1, v2);
}



/*! \brief Unit test of different vector constructors.
 */
TEST(VectorTest, VectorConstructor)
{
    Vector3D v1(1.0, 0.5, 2.2);

    Vector3D v2(v1);
    EXPECT_EQ(v1, v2);

    Point3D p1(1.0, 0.5, 2.2);
    Vector3D v3(p1);
    EXPECT_EQ(v1, v3);
}



/*! \brief Unit test of element access methods.
 *
 *  Data validity as well as out of range access are tested.
 */
TEST(VectorTest, ElementAccess)
{
    const Vector3D v1(1.0, 0.5, 2.2);
    ASSERT_NO_THROW(v1(1));
    EXPECT_EQ(1.0, v1(1));
    ASSERT_NO_THROW(v1[0]);
    EXPECT_EQ(1.0, v1[0]);
    ASSERT_NO_THROW(v1(2));
    EXPECT_EQ(0.5, v1(2));
    ASSERT_NO_THROW(v1[1]);
    EXPECT_EQ(0.5, v1[1]);
    ASSERT_NO_THROW(v1(3));
    EXPECT_EQ(2.2, v1(3));
    ASSERT_NO_THROW(v1[2]);
    EXPECT_EQ(2.2, v1[2]);

    Vector3D v2(1.0, 0.5, 2.2);
    EXPECT_NO_THROW(v2(1) = 2.0);
    EXPECT_NO_THROW(v2(2) = 2.0);
    EXPECT_NO_THROW(v2(3) = 2.0);
    EXPECT_EQ(2.0, v2(1));
    EXPECT_EQ(2.0, v2(2));
    EXPECT_EQ(2.0, v2(3));

    EXPECT_NO_THROW(v2[0] = 3.0);
    EXPECT_NO_THROW(v2[1] = 3.0);
    EXPECT_NO_THROW(v2[2] = 3.0);
    EXPECT_EQ(3.0, v2(1));
    EXPECT_EQ(3.0, v2(2));
    EXPECT_EQ(3.0, v2(3));

    double a;
    EXPECT_THROW(a = v1(0), std::out_of_range);
    EXPECT_THROW(a = v1(4), std::out_of_range);
    EXPECT_THROW(v2(0) = 0.0, std::out_of_range);
    EXPECT_THROW(v2(4) = 0.0, std::out_of_range);
    EXPECT_THROW(a = v1[3], std::out_of_range);
    EXPECT_THROW(v2[3] = 0.0, std::out_of_range);
}



/*! \brief Unit test of vector assignment operator.
 */
TEST(VectorTest, VectorAssignment)
{
    Vector3D v1(1.0, 0.5, 2.2);
    Vector3D v2;
    EXPECT_NE(v1, v2);

    v2 = v1;
    EXPECT_EQ(v1, v2);

    Vector3D v3;
    v2 = v3;
    EXPECT_NE(v1, v2);
    EXPECT_EQ(v2, v3);
}



/*! \brief Unit test of default vector constructor.
 */
TEST(VectorTest, ZeroVector)
{
    Vector3D v1(0.0, 0.0, 0.0);
    EXPECT_EQ(v1, v1);

    Vector3D v2;
    EXPECT_EQ(v1, v2);
}



/*! \brief Unit test of unary minus operator.
 */
TEST(VectorTest, UnaryMinus)
{
    Vector3D v1(1.0, 0.5, 2.2);
    Vector3D v2(-1.0, -0.5, -2.2);
    EXPECT_NE(v1, v2);
    EXPECT_EQ(v1, -v2);
    EXPECT_EQ(v2, -v1);
}



/*! \brief Unit test of add operator.
 */
TEST(VectorTest, VectorAddition)
{
    Vector3D v1(1.0, 0.5, 2.2);
    Vector3D v2;
    EXPECT_EQ(v1, v1 + v2);
    EXPECT_EQ(v1, v1 - v2);

    Vector3D v3(v1);
    v1 -= v2;
    EXPECT_EQ(v3, v1);
    v1 += v2;
    EXPECT_EQ(v3, v1);


    Vector3D v4(8.0, -2.0, 2.2 - 4.3);
    // numerical problem of double precision
    // EXPECT_EQ(-2.1, 2.2 - 4.3); fails
    Vector3D v5(-6.0, 3.0, 6.5);
    Vector3D v6(-7.0, 2.5, 4.3);
    EXPECT_EQ(v4, v1 - v6);
    EXPECT_EQ(v5, v1 + v6);

    Vector3D v7(v1);
    v7 -= v6;
    EXPECT_EQ(v4, v7);
    Vector3D v8(v1);
    v8 += v6;
    EXPECT_EQ(v5, v8);
}



/*! \brief Unit test of multiplication of a vector with a scalar.
 */
TEST(VectorTest, ScalarMultiplication)
{
    Vector3D v1(1.0, 0.5, 2.2);
    Vector3D v2(2.0, 1.0, 4.4);
    Vector3D v3(0.5, 0.25, 1.1);
    double a = 2.0;
    EXPECT_EQ(v2, v1 * a);
    EXPECT_EQ(v3, v1 / a);
}



/*! \brief Unit test of vector dot product.
 */
TEST(VectorTest, DotProduct)
{
    Vector3D v1(1.0, 0.5, 2.0);
    EXPECT_DOUBLE_EQ(v1.length() * v1.length(), v1 * v1);

    Vector3D v2(5.0, -2.0, -2.0);
    EXPECT_DOUBLE_EQ(0.0, v1 * v2);

    Vector3D v3(2.0, -4.0, 1.0);
    EXPECT_DOUBLE_EQ(2.0, v1 * v3);
}



/*! \brief Unit test of vector length calculation and normalization.
 */
TEST(VectorTest, VectorLength)
{
    Vector3D v1(2.0, 1.0, -2.0);
    EXPECT_DOUBLE_EQ(3.0, v1.length());

    v1.normalize();
    EXPECT_DOUBLE_EQ(1.0, v1.length());
}



/*! \brief Unit test of vector cross product.
 */
TEST(VectorTest, CrossProduct)
{
    Vector3D v1(1.0, 0.5, 2.0);
    Vector3D v2;
    EXPECT_EQ(v2, v1.crossProduct(v1));
    EXPECT_EQ(v2, v1.crossProduct(-v1));
    EXPECT_EQ(v2, v1.crossProduct(v1 * 5.0));

    Vector3D v3(5.0, -2.0, -2.0);
    Vector3D v4(3.0, 12.0, -4.5);
    EXPECT_EQ(v4, v1.crossProduct(v3));
    EXPECT_EQ(v4, -v3.crossProduct(v1));
}


/*! \brief Unit test of vector angle calculation
 */
TEST(VectorTest, Angle)
{
    Vector3D v1(1.0, 0.0, 0.0);
    Vector3D v2(0.0, 1.0, 0.0);
    Vector3D v3(0.0, 0.0, 1.0);
    Vector3D v4(-1.0, 0.0, 0.0);
    Vector3D v5(0.0, 0.0, -1.0);

    EXPECT_DOUBLE_EQ(0.0,   v1.angle(v1));
    EXPECT_DOUBLE_EQ(PI_2,  v1.angle(v2));
    EXPECT_DOUBLE_EQ(PI_2,  v1.angle(v3));
    EXPECT_DOUBLE_EQ(PI,    v1.angle(v4));
    EXPECT_DOUBLE_EQ(PI_2,v1.angle(v5));

}
