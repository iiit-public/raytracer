// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file ColorTest.cpp
 *  \author Thomas Nuernberg
 *  \date 09.09.2016
 *  \brief Test of various spectral representation classes.
 */

#include "gtest/gtest.h"

#include <array>
#include <vector>

#include "Spectrum/CoefficientSpectrum.hpp"
#include "Spectrum/Monochromatic.hpp"
#include "Spectrum/RgbSpectrum.hpp"
#include "Spectrum/SampledSpectrum.hpp"

using namespace iiit;



/*! \brief Unit test of Monochromatic constructors.
 *
 *  Test of Monochromatic constructor with grayscale values.
 */
TEST(MonochromaticTest, GrayscaleConstructorTest)
{
    // constructor with single argument
    Monochromatic singleGrayscale(1.0);
    std::array<float, 3> rgb;
    EXPECT_EQ(1, singleGrayscale.getNumOfCoeffs());
    EXPECT_EQ(1.f, singleGrayscale.getCoeff(0));
    singleGrayscale.toRgb(rgb);
    EXPECT_EQ(1.f, rgb[0]);
    EXPECT_EQ(1.f, rgb[1]);
    EXPECT_EQ(1.f, rgb[2]);

    // constructor with three arguments
    rgb[0] = 1.f;
    rgb[1] = 1.f;
    rgb[2] = 1.f;
    Monochromatic rgbGrayscale = Monochromatic::fromRgb(rgb, SpectrumRepresentationType::IlluminationSpectrum);
    EXPECT_EQ(1, rgbGrayscale.getNumOfCoeffs());
    EXPECT_EQ(1.f, rgbGrayscale.getCoeff(0));
    rgbGrayscale.toRgb(rgb);
    EXPECT_EQ(1.f, rgb[0]);
    EXPECT_EQ(1.f, rgb[1]);
    EXPECT_EQ(1.f, rgb[2]);
}



/*! \brief Unit test of Monochromatic factory methods.
 *
 *  Test of RgbSpectrum factory methods with various input values.
 */
TEST(MonochromaticTest, FactoryTest)
{
    // factory method with three rgb arguments
    std::array<float, 3> rgb;
    rgb[0] = 1.f;
    rgb[1] = 1.f;
    rgb[2] = 1.f;
    Monochromatic rgbIlluminationGrayscale = Monochromatic::fromRgb(rgb, SpectrumRepresentationType::IlluminationSpectrum);
    EXPECT_EQ(1, rgbIlluminationGrayscale.getNumOfCoeffs());
    EXPECT_EQ(1.f, rgbIlluminationGrayscale.getCoeff(0));
    rgbIlluminationGrayscale.toRgb(rgb);
    EXPECT_EQ(1.f, rgb[0]);
    EXPECT_EQ(1.f, rgb[1]);
    EXPECT_EQ(1.f, rgb[2]);
    rgb[0] = 1.f;
    rgb[1] = 1.f;
    rgb[2] = 1.f;
    Monochromatic rgbReflectanceGrayscale = Monochromatic::fromRgb(rgb, SpectrumRepresentationType::ReflectanceSpectrum);
    EXPECT_EQ(1, rgbReflectanceGrayscale.getNumOfCoeffs());
    EXPECT_EQ(1.f, rgbReflectanceGrayscale.getCoeff(0));
    rgbReflectanceGrayscale.toRgb(rgb);
    EXPECT_EQ(1.f, rgb[0]);
    EXPECT_EQ(1.f, rgb[1]);
    EXPECT_EQ(1.f, rgb[2]);

    // factory method with sampled spectrum
    std::vector<float> waveLenghts;
    for (int i = 0; i < 30; ++i)
    {
        waveLenghts.push_back(static_cast<float>(405 + i * 10));
    }
    std::vector<float> white(30, 1.f);
    Monochromatic sampledGrayscale = Monochromatic::fromSampled(waveLenghts, white);
    sampledGrayscale.toRgb(rgb);
    // in general, a homogeneous spectrum will not result in the same grayscale value
    // it should only work with black
    std::vector<float> black(30, 0.f);
    Monochromatic sampledBlack = Monochromatic::fromSampled(waveLenghts, black);
    sampledBlack.toRgb(rgb);
    EXPECT_EQ(0.f, rgb[0]);
    EXPECT_EQ(0.f, rgb[1]);
    EXPECT_EQ(0.f, rgb[2]);
}



/*! \brief Unit test of Monochromatic operators.
 */
TEST(MonochromaticTest, OperatorTest)
{
    float value = 0.5f;
    Monochromatic color(value);

    // operator=    
    Monochromatic result = color;
    EXPECT_EQ(0.5f, result.getCoeff(0));

    // operator+=
    result += color;
    EXPECT_EQ(1.f, result.getCoeff(0));

    // operator+
    result = color + color;
    EXPECT_EQ(1.f, result.getCoeff(0));

    // operator-
    result = Monochromatic(1.f);
    result = result - color;
    EXPECT_EQ(0.5f, result.getCoeff(0));

    // operator-=
    result = Monochromatic(1.f);
    result -= color;
    EXPECT_EQ(0.5f, result.getCoeff(0));

    // operator*=
    result = Monochromatic(1.f);
    result *= value;
    EXPECT_EQ(0.5f, result.getCoeff(0));

    // operator*
    result = Monochromatic(1.f);
    result = result * value;
    EXPECT_EQ(0.5f, result.getCoeff(0));

    // operator*=
    result = Monochromatic(1.f);
    result *= color;
    EXPECT_EQ(0.5f, result.getCoeff(0));

    // operator*
    result = Monochromatic(1.f);
    result = result * color;
    EXPECT_EQ(0.5f, result.getCoeff(0));

    // operator/=
    result = Monochromatic(1.f);
    result /= value;
    EXPECT_EQ(2.f, result.getCoeff(0));

    // operator/
    result = Monochromatic(1.f);
    result = result / value;
    EXPECT_EQ(2.f, result.getCoeff(0));

    // operator/=
    result = Monochromatic(1.f);
    result /= color;
    EXPECT_EQ(2.f, result.getCoeff(0));

    // operator/
    result = Monochromatic(1.f);
    result = result / color;
    EXPECT_EQ(2.f, result.getCoeff(0));
    
    // power operator
    result = Monochromatic(0.5f);
    result = result.powc(2.f);
    EXPECT_EQ(0.25f, result.getCoeff(0));
}



/*! \brief Unit test of RgbSpectrum constructors.
 *
 *  Test of RgbSpectrum constructors with grayscale values.
 */
TEST(RgbTest, GrayscaleConstructorTest)
{
    // constructor with single argument
    RgbSpectrum singleGrayscale(1.0);
    EXPECT_EQ(3, singleGrayscale.getNumOfCoeffs());
    EXPECT_EQ(1.f, singleGrayscale.getCoeff(0));
    EXPECT_EQ(1.f, singleGrayscale.getCoeff(1));
    EXPECT_EQ(1.f, singleGrayscale.getCoeff(2));
    std::array<float, 3> rgb;
    singleGrayscale.toRgb(rgb);
    EXPECT_EQ(1.f, rgb[0]);
    EXPECT_EQ(1.f, rgb[1]);
    EXPECT_EQ(1.f, rgb[2]);

    // constructor with three arguments
    rgb[0] = 1.f;
    rgb[1] = 1.f;
    rgb[2] = 1.f;
    RgbSpectrum rgbGrayscale(rgb);
    EXPECT_EQ(3, rgbGrayscale.getNumOfCoeffs());
    EXPECT_EQ(1.f, rgbGrayscale.getCoeff(0));
    EXPECT_EQ(1.f, rgbGrayscale.getCoeff(1));
    EXPECT_EQ(1.f, rgbGrayscale.getCoeff(2));
    rgbGrayscale.toRgb(rgb);
    EXPECT_EQ(1.f, rgb[0]);
    EXPECT_EQ(1.f, rgb[1]);
    EXPECT_EQ(1.f, rgb[2]);
}



/*! \brief Unit test of RgbSpectrum factory methods.
 *
 *  Test of RgbSpectrum factory methods with various input values.
 */
TEST(RgbTest, RgbFactoryTest)
{
    // factory method with three rgb arguments
    std::array<float, 3> rgb;
    rgb[0] = 1.f;
    rgb[1] = 1.f;
    rgb[2] = 1.f;
    RgbSpectrum rgbIlluminationGrayscale = RgbSpectrum::fromRgb(rgb, SpectrumRepresentationType::IlluminationSpectrum);
    EXPECT_EQ(3, rgbIlluminationGrayscale.getNumOfCoeffs());
    EXPECT_EQ(1.f, rgbIlluminationGrayscale.getCoeff(0));
    EXPECT_EQ(1.f, rgbIlluminationGrayscale.getCoeff(1));
    EXPECT_EQ(1.f, rgbIlluminationGrayscale.getCoeff(2));
    rgbIlluminationGrayscale.toRgb(rgb);
    EXPECT_EQ(1.f, rgb[0]);
    EXPECT_EQ(1.f, rgb[1]);
    EXPECT_EQ(1.f, rgb[2]);
    rgb[0] = 1.f;
    rgb[1] = 1.f;
    rgb[2] = 1.f;
    RgbSpectrum rgbReflectanceGrayscale = RgbSpectrum::fromRgb(rgb, SpectrumRepresentationType::ReflectanceSpectrum);
    EXPECT_EQ(3, rgbReflectanceGrayscale.getNumOfCoeffs());
    EXPECT_EQ(1.f, rgbReflectanceGrayscale.getCoeff(0));
    EXPECT_EQ(1.f, rgbReflectanceGrayscale.getCoeff(1));
    EXPECT_EQ(1.f, rgbReflectanceGrayscale.getCoeff(2));
    rgbReflectanceGrayscale.toRgb(rgb);
    EXPECT_EQ(1.f, rgb[0]);
    EXPECT_EQ(1.f, rgb[1]);
    EXPECT_EQ(1.f, rgb[2]);

    // factory method with sampled spectrum
    std::vector<float> waveLenghts;
    for (int i = 0; i < 30; ++i)
    {
        waveLenghts.push_back(static_cast<float>(405 + i * 10));
    }
    std::vector<float> white(30, 1.f);
    RgbSpectrum sampledGrayscale = RgbSpectrum::fromSampled(waveLenghts, white);
    sampledGrayscale.toRgb(rgb);
    // in general, a homogeneous spectrum will not result in a gray color in rgb space
    // it should only work with black
    std::vector<float> black(30, 0.f);
    RgbSpectrum sampledBlack = RgbSpectrum::fromSampled(waveLenghts, black);
    sampledBlack.toRgb(rgb);
    EXPECT_EQ(0.f, rgb[0]);
    EXPECT_EQ(0.f, rgb[1]);
    EXPECT_EQ(0.f, rgb[2]);
}



/*! \brief Unit test of CoefficientSpectrum conversion functions.
 *
 *  Test of CoefficientSpectrum rgb to xyz color space conversion capabilities.
 */
TEST(RgbTest, ConversionTest)
{
    std::array<float, 3> rgb;
    rgb[0] = 1.f;
    rgb[1] = 1.f;
    rgb[2] = 1.f;

    std::array<float, 3> xyz;
    xyz[0] = 0.f;
    xyz[1] = 0.f;
    xyz[2] = 0.f;

    CoefficientSpectrum<30>::rgbToXyz(rgb, xyz);
    std::array<float, 3> rgb2;
    CoefficientSpectrum<30>::xyzToRgb(xyz, rgb2);

    EXPECT_FLOAT_EQ(rgb[0], rgb2[0]);
    EXPECT_FLOAT_EQ(rgb[1], rgb2[1]);
    EXPECT_FLOAT_EQ(rgb[2], rgb2[2]);

    xyz[0] = 1.f;
    xyz[1] = 1.f;
    xyz[2] = 1.f;

    CoefficientSpectrum<30>::xyzToRgb(xyz, rgb);
    std::array<float, 3> xyz2;
    CoefficientSpectrum<30>::rgbToXyz(rgb, xyz2);

    EXPECT_FLOAT_EQ(xyz[0], xyz2[0]);
    EXPECT_FLOAT_EQ(xyz[1], xyz2[1]);
    EXPECT_FLOAT_EQ(xyz[2], xyz2[2]);
}



/*! \brief Unit test of RgbSpectrum operators.
*/
TEST(RgbTest, OperatorTest)
{
    float value = 0.5f;
    RgbSpectrum color(value);

    // operator=    
    RgbSpectrum result = color;
    EXPECT_EQ(0.5f, result.getCoeff(0));
    EXPECT_EQ(0.5f, result.getCoeff(1));
    EXPECT_EQ(0.5f, result.getCoeff(2));

    // operator+=
    result += color;
    EXPECT_EQ(1.f, result.getCoeff(0));
    EXPECT_EQ(1.f, result.getCoeff(1));
    EXPECT_EQ(1.f, result.getCoeff(2));

    // operator+
    result = color + color;
    EXPECT_EQ(1.f, result.getCoeff(0));
    EXPECT_EQ(1.f, result.getCoeff(1));
    EXPECT_EQ(1.f, result.getCoeff(2));

    // operator-
    result = RgbSpectrum(1.f);
    result = result - color;
    EXPECT_EQ(0.5f, result.getCoeff(0));
    EXPECT_EQ(0.5f, result.getCoeff(1));
    EXPECT_EQ(0.5f, result.getCoeff(2));

    // operator-=
    result = RgbSpectrum(1.f);
    result -= color;
    EXPECT_EQ(0.5f, result.getCoeff(0));
    EXPECT_EQ(0.5f, result.getCoeff(1));
    EXPECT_EQ(0.5f, result.getCoeff(2));

    // operator*=
    result = RgbSpectrum(1.f);
    result *= value;
    EXPECT_EQ(0.5f, result.getCoeff(0));
    EXPECT_EQ(0.5f, result.getCoeff(1));
    EXPECT_EQ(0.5f, result.getCoeff(2));

    // operator*
    result = RgbSpectrum(1.f);
    result = result * value;
    EXPECT_EQ(0.5f, result.getCoeff(0));
    EXPECT_EQ(0.5f, result.getCoeff(1));
    EXPECT_EQ(0.5f, result.getCoeff(2));

    // operator*=
    result = RgbSpectrum(1.f);
    result *= color;
    EXPECT_EQ(0.5f, result.getCoeff(0));
    EXPECT_EQ(0.5f, result.getCoeff(1));
    EXPECT_EQ(0.5f, result.getCoeff(2));

    // operator*
    result = RgbSpectrum(1.f);
    result = result * color;
    EXPECT_EQ(0.5f, result.getCoeff(0));
    EXPECT_EQ(0.5f, result.getCoeff(1));
    EXPECT_EQ(0.5f, result.getCoeff(2));

    // operator/=
    result = RgbSpectrum(1.f);
    result /= value;
    EXPECT_EQ(2.f, result.getCoeff(0));
    EXPECT_EQ(2.f, result.getCoeff(1));
    EXPECT_EQ(2.f, result.getCoeff(2));

    // operator/
    result = RgbSpectrum(1.f);
    result = result / value;
    EXPECT_EQ(2.f, result.getCoeff(0));
    EXPECT_EQ(2.f, result.getCoeff(1));
    EXPECT_EQ(2.f, result.getCoeff(2));

    // operator/=
    result = RgbSpectrum(1.f);
    result /= color;
    EXPECT_EQ(2.f, result.getCoeff(0));
    EXPECT_EQ(2.f, result.getCoeff(1));
    EXPECT_EQ(2.f, result.getCoeff(2));

    // operator/
    result = RgbSpectrum(1.f);
    result = result / color;
    EXPECT_EQ(2.f, result.getCoeff(0));
    EXPECT_EQ(2.f, result.getCoeff(1));
    EXPECT_EQ(2.f, result.getCoeff(2));
    
    // power operator
    result = RgbSpectrum(0.5f);
    result = result.powc(2.f);
    EXPECT_EQ(0.25f, result.getCoeff(0));
    EXPECT_EQ(0.25f, result.getCoeff(1));
    EXPECT_EQ(0.25f, result.getCoeff(2));
}



/*! \brief Unit test of SampledSpectrum factory method with sparse spectral data.
 *
 *  Test of SampledSpectrum factory method fromSampled() with sparse spectral data.
 */
TEST(SampledTest, FromSampledSparseTest)
{
    std::vector<float> lambdaSamples;
    lambdaSamples.push_back(450);
    lambdaSamples.push_back(500);
    lambdaSamples.push_back(550);
    lambdaSamples.push_back(600);
    std::vector<float> valueSamples;
    valueSamples.push_back(0);
    valueSamples.push_back(1);
    valueSamples.push_back(1);
    valueSamples.push_back(0);

    SampledSpectrum color = SampledSpectrum::fromSampled(lambdaSamples, valueSamples);

    EXPECT_FLOAT_EQ(0.f, color.getCoeff(0));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(1));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(2));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(3));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(4));
    EXPECT_FLOAT_EQ(0.1f, color.getCoeff(5));
    EXPECT_FLOAT_EQ(0.3f, color.getCoeff(6));
    EXPECT_FLOAT_EQ(0.5f, color.getCoeff(7));
    EXPECT_FLOAT_EQ(0.7f, color.getCoeff(8));
    EXPECT_FLOAT_EQ(0.9f, color.getCoeff(9));
    EXPECT_FLOAT_EQ(1.f, color.getCoeff(10));
    EXPECT_FLOAT_EQ(1.f, color.getCoeff(11));
    EXPECT_FLOAT_EQ(1.f, color.getCoeff(12));
    EXPECT_FLOAT_EQ(1.f, color.getCoeff(13));
    EXPECT_FLOAT_EQ(1.f, color.getCoeff(14));
    EXPECT_FLOAT_EQ(0.9f, color.getCoeff(15));
    EXPECT_FLOAT_EQ(0.7f, color.getCoeff(16));
    EXPECT_FLOAT_EQ(0.5f, color.getCoeff(17));
    EXPECT_FLOAT_EQ(0.3f, color.getCoeff(18));
    EXPECT_FLOAT_EQ(0.1f, color.getCoeff(19));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(20));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(21));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(22));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(23));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(24));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(25));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(26));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(27));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(28));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(29));
}



/*! \brief Unit test of SampledSpectrum factory method with dense spectral data.
 *
 *  Test of SampledSpectrum factory method fromSampled() with dense spectral data.
 */
TEST(SampledTest, FromSampledDenseTest)
{
    std::vector<float> lambdaSamples;
    lambdaSamples.push_back(520);
    lambdaSamples.push_back(522);
    lambdaSamples.push_back(524);
    lambdaSamples.push_back(526);
    std::vector<float> valueSamples;
    valueSamples.push_back(0);
    valueSamples.push_back(1);
    valueSamples.push_back(1);
    valueSamples.push_back(0);

    SampledSpectrum color = SampledSpectrum::fromSampled(lambdaSamples, valueSamples);
    EXPECT_EQ(numSpectralSamples, color.getNumOfCoeffs());

    EXPECT_FLOAT_EQ(0.f, color.getCoeff(0));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(1));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(2));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(3));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(4));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(5));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(6));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(7));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(8));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(9));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(10));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(11));
    EXPECT_FLOAT_EQ(0.4f, color.getCoeff(12));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(13));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(14));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(15));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(16));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(17));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(18));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(19));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(20));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(21));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(22));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(23));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(24));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(25));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(26));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(27));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(28));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(29));
}



/*! \brief Unit test of SampledSpectrum factory method with dense unequally spaced spectral data.
 *
 *  Test of SampledSpectrum factory method fromSampled() with dense unequally spaced spectral data.
 */
TEST(SampledTest, FromSampledDenseUnequalTest)
{
    std::vector<float> lambdaSamples;
    lambdaSamples.push_back(518);
    lambdaSamples.push_back(522);
    lambdaSamples.push_back(524);
    lambdaSamples.push_back(526);
    std::vector<float> valueSamples;
    valueSamples.push_back(0);
    valueSamples.push_back(1);
    valueSamples.push_back(1);
    valueSamples.push_back(0);

    SampledSpectrum color = SampledSpectrum::fromSampled(lambdaSamples, valueSamples);
    EXPECT_EQ(numSpectralSamples, color.getNumOfCoeffs());

    EXPECT_FLOAT_EQ(0.f, color.getCoeff(0));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(1));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(2));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(3));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(4));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(5));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(6));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(7));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(8));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(9));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(10));
    EXPECT_FLOAT_EQ(0.05f, color.getCoeff(11));
    EXPECT_FLOAT_EQ(0.45f, color.getCoeff(12));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(13));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(14));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(15));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(16));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(17));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(18));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(19));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(20));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(21));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(22));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(23));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(24));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(25));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(26));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(27));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(28));
    EXPECT_FLOAT_EQ(0.f, color.getCoeff(29));
}




/*! \brief Unit test of SampledSpectrum grayscale constructor.
 *
 *  Test of SampledSpectrum monochromatic initialization.
 */
TEST(SampledTest, GrayscaleConstructorTest)
{
    SampledSpectrum gray(0.5f);
    for (int i = 0; i < gray.getNumOfCoeffs(); ++i)
    {
        EXPECT_EQ(gray.getCoeff(i), 0.5f);
    }
}



/*! \brief Unit test of SampledSpectrum rgb conversion functions.
 *  \attention Test only used to show results. Thus it will always pass.
 *
 *  Test of SampledSpectrum rgb to sampled color space conversion capabilities.
 */
TEST(SampledTest, ConversionTest)
{
    SampledSpectrum::init();

    std::array<float, 3> rgbWhite;
    rgbWhite[0] = 1.f;
    rgbWhite[1] = 1.f;
    rgbWhite[2] = 1.f;

    // reflectance spectrum
    SampledSpectrum sampledReflectanceWhite = SampledSpectrum::fromRgb(rgbWhite, SpectrumRepresentationType::ReflectanceSpectrum);
    std::array<float, 3> rgbReflectanceWhite;
    sampledReflectanceWhite.toRgb(rgbReflectanceWhite);
    std::cout << "Reflectance white rgb: [" << rgbReflectanceWhite[0] << ", " << rgbReflectanceWhite[1] << ", " << rgbReflectanceWhite[2] << "]" << std::endl;

    // illumination spectrum
    SampledSpectrum sampledIlluminationWhite = SampledSpectrum::fromRgb(rgbWhite, SpectrumRepresentationType::IlluminationSpectrum);
    std::array<float, 3> rgbIlluminationWhite;
    sampledIlluminationWhite.toRgb(rgbIlluminationWhite);
    std::cout << "Illumination white rgb: [" << rgbIlluminationWhite[0] << ", " << rgbIlluminationWhite[1] << ", " << rgbIlluminationWhite[2] << "]" << std::endl;
}



/*! \brief Unit test of SampledSpectrum operators.
*/
TEST(SampledTest, OperatorTest)
{
    float value = 0.5f;
    SampledSpectrum color(value);

    // operator=    
    SampledSpectrum result = color;
    for (int i = 0; i < result.getNumOfCoeffs(); ++i)
    {
        EXPECT_FLOAT_EQ(0.5f, result.getCoeff(i));
    }

    // operator+=
    result += color;
    for (int i = 0; i < result.getNumOfCoeffs(); ++i)
    {
        EXPECT_FLOAT_EQ(1.f, result.getCoeff(i));
    }

    // operator+
    result = color + color;
    for (int i = 0; i < result.getNumOfCoeffs(); ++i)
    {
        EXPECT_FLOAT_EQ(1.f, result.getCoeff(i));
    }

    // operator-
    result = SampledSpectrum(1.f);
    result = result - color;
    for (int i = 0; i < result.getNumOfCoeffs(); ++i)
    {
        EXPECT_FLOAT_EQ(0.5f, result.getCoeff(i));
    }

    // operator-=
    result = SampledSpectrum(1.f);
    result -= color;
    for (int i = 0; i < result.getNumOfCoeffs(); ++i)
    {
        EXPECT_FLOAT_EQ(0.5f, result.getCoeff(i));
    }

    // operator*=
    result = SampledSpectrum(1.f);
    result *= value;
    for (int i = 0; i < result.getNumOfCoeffs(); ++i)
    {
        EXPECT_FLOAT_EQ(0.5f, result.getCoeff(i));
    }

    // operator*
    result = SampledSpectrum(1.f);
    result = result * value;
    for (int i = 0; i < result.getNumOfCoeffs(); ++i)
    {
        EXPECT_FLOAT_EQ(0.5f, result.getCoeff(i));
    }

    // operator*=
    result = SampledSpectrum(1.f);
    result *= color;
    for (int i = 0; i < result.getNumOfCoeffs(); ++i)
    {
        EXPECT_FLOAT_EQ(0.5f, result.getCoeff(i));
    }

    // operator*
    result = SampledSpectrum(1.f);
    result = result * color;
    for (int i = 0; i < result.getNumOfCoeffs(); ++i)
    {
        EXPECT_FLOAT_EQ(0.5f, result.getCoeff(i));
    }

    // operator/=
    result = SampledSpectrum(1.f);
    result /= value;
    for (int i = 0; i < result.getNumOfCoeffs(); ++i)
    {
        EXPECT_FLOAT_EQ(2.f, result.getCoeff(i));
    }

    // operator/
    result = SampledSpectrum(1.f);
    result = result / value;
    for (int i = 0; i < result.getNumOfCoeffs(); ++i)
    {
        EXPECT_FLOAT_EQ(2.f, result.getCoeff(i));
    }

    // operator/=
    result = SampledSpectrum(1.f);
    result /= color;
    for (int i = 0; i < result.getNumOfCoeffs(); ++i)
    {
        EXPECT_FLOAT_EQ(2.f, result.getCoeff(i));
    }

    // operator/
    result = SampledSpectrum(1.f);
    result = result / color;
    for (int i = 0; i < result.getNumOfCoeffs(); ++i)
    {
        EXPECT_FLOAT_EQ(2.f, result.getCoeff(i));
    }
    
    // power operator
    result = SampledSpectrum(0.5f);
    result = result.powc(2.f);
    for (int i = 0; i < result.getNumOfCoeffs(); ++i)
    {
        EXPECT_FLOAT_EQ(0.25f, result.getCoeff(i));
    }
}
