// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file HexGridTest.cpp
 *  \author Maximilian Schambach
 *  \date 27.02.2019
 *  \brief Unit test of class HexGrid.
 */

#include "gtest/gtest.h"

#include "Utilities/HexGrid.hpp"
#include "Utilities/Point3D.hpp"
#include "Spectrum/RgbSpectrum.hpp"

using namespace iiit;


///*! \brief Unit test of constructor.
TEST(HexGridTest, HexGridConstructor)
{
    double x_max = 1.02;
    double y_max = 1.02;
    double offset_x = 0.0;
    double offset_y = 0.0;
    double spacing = 0.5;
    double rotation = 0.0;
    double phi = 0.0;
    double theta = 0.0;
    double z = 0.0;

    HexGrid<RgbSpectrum> grid(x_max, y_max, offset_x, offset_y, spacing, rotation, phi, theta, z, false);
    EXPECT_EQ(grid.get_num_points(), 5*3 + 4*2);


}


/////*! \brief Unit test of constructor.
//TEST(HexGridTest, HexGridSave)
//{
//    double x_max = 0.0101;
//    double y_max = 0.0201;
//    double offset_x = 0.0;
//    double offset_y = 0.0;
//    double spacing = 0.0005;
//    double rotation = 0.0;
//    double phi = 0.0;
//    double theta = 0.0;
//    double z = 0.0;

//    HexGrid grid(x_max, y_max, offset_x, offset_y, spacing, rotation, phi, theta, z);
//    grid.save("test_hex_grid.csv");


//}
