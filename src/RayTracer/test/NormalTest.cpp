// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file NormalTest.cpp
 *  \author Thomas Nuernberg
 *  \date 16.03.2015
 *  \brief Unit test of class Normal.
 */

#include "gtest/gtest.h"

#include "Utilities/Normal.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/Point3D.hpp"

using namespace iiit;



/*! \brief Unit test of normal comparison.
 */
TEST(NormalTest, NormalComparison)
{
    Normal n1(1.0, 0.5, 2.2);
    EXPECT_EQ(n1, n1);

    Normal n2(-4.0, 1.45e4, 86.22);
    EXPECT_NE(n1, n2);
}



/*! \brief Unit test of different normal constructors.
 */
TEST(NormalTest, NormalConstructor)
{
    Normal n1(1.0, 0.5, 2.2);

    Normal n2(n1);
    EXPECT_EQ(n1, n2);

    Vector3D v1(1.0, 0.5, 2.2);
    Normal n3(v1);
    EXPECT_EQ(n1, n3);
}



/*! \brief Unit test of element access methods.
 *
 *  Data validity as well as out of range access are tested.
 */
TEST(NormalTest, ElementAccess)
{
    const Normal n1(1.0, 0.5, 2.2);
    ASSERT_NO_THROW(n1(1));
    EXPECT_EQ(1.0, n1(1));
    ASSERT_NO_THROW(n1[0]);
    EXPECT_EQ(1.0, n1[0]);
    ASSERT_NO_THROW(n1(2));
    EXPECT_EQ(0.5, n1(2));
    ASSERT_NO_THROW(n1[1]);
    EXPECT_EQ(0.5, n1[1]);
    ASSERT_NO_THROW(n1(3));
    EXPECT_EQ(2.2, n1(3));
    ASSERT_NO_THROW(n1[2]);
    EXPECT_EQ(2.2, n1[2]);

    Normal n2(1.0, 0.5, 2.2);
    EXPECT_NO_THROW(n2(1) = 2.0);
    EXPECT_NO_THROW(n2(2) = 2.0);
    EXPECT_NO_THROW(n2(3) = 2.0);
    EXPECT_EQ(2.0, n2(1));
    EXPECT_EQ(2.0, n2(2));
    EXPECT_EQ(2.0, n2(3));

    EXPECT_NO_THROW(n2[0] = 3.0);
    EXPECT_NO_THROW(n2[1] = 3.0);
    EXPECT_NO_THROW(n2[2] = 3.0);
    EXPECT_EQ(3.0, n2(1));
    EXPECT_EQ(3.0, n2(2));
    EXPECT_EQ(3.0, n2(3));

    double a;
    EXPECT_THROW(a = n1(0), std::out_of_range);
    EXPECT_THROW(a = n1(4), std::out_of_range);
    EXPECT_THROW(n2(0) = 0.0, std::out_of_range);
    EXPECT_THROW(n2(4) = 0.0, std::out_of_range);
    EXPECT_THROW(a = n1[3], std::out_of_range);
    EXPECT_THROW(n2[3] = 0.0, std::out_of_range);
}



/*! \brief Unit test of normal assignment operator.
 */
TEST(NormalTest, NormalAssignment)
{
    Normal n1(1.0, 0.5, 2.2);
    Normal n2;
    EXPECT_NE(n1, n2);

    n2 = n1;
    EXPECT_EQ(n1, n2);

    Normal n3;
    n2 = n3;
    EXPECT_NE(n1, n2);
    EXPECT_EQ(n2, n3);

    Vector3D v1(1.0, 0.5, 2.2);
    Normal n4 = v1;
    EXPECT_EQ(n1, n4);

    Point3D p1(1.0, 0.5, 2.2);
    Normal n5;
    n5 = p1;
    EXPECT_EQ(n1, n5);
}



/*! \brief Unit test of default vector constructor.
 */
TEST(NormalTest, ZeroVector)
{
    Normal n1(0.0, 0.0, 0.0);
    EXPECT_EQ(n1, n1);

    Normal n2;
    EXPECT_EQ(n1, n2);
}



/*! \brief Unit test of unary minus operator.
 */
TEST(NormalTest, UnaryMinus)
{
    Normal n1(1.0, 0.5, 2.2);
    Normal n2(-1.0, -0.5, -2.2);
    EXPECT_NE(n1, n2);
    EXPECT_EQ(n1, -n2);
    EXPECT_EQ(n2, -n1);
}



/*! \brief Unit test of add operator.
 */
TEST(NormalTest, VectorAddition)
{
    Normal n1(1.0, 0.5, 2.2);
    Normal n2;
    EXPECT_EQ(n1, n1 + n2);
    EXPECT_EQ(n1, n1 - n2);

    Normal n3(n1);
    n1 -= n2;
    EXPECT_EQ(n3, n1);
    n1 += n2;
    EXPECT_EQ(n3, n1);


    Normal n4(8.0, -2.0, 2.2 - 4.3);
    // numerical problem of double precision
    // EXPECT_EQ(-2.1, 2.2 - 4.3); fails
    Normal n5(-6.0, 3.0, 6.5);
    Normal n6(-7.0, 2.5, 4.3);
    EXPECT_EQ(n4, n1 - n6);
    EXPECT_EQ(n5, n1 + n6);

    Normal n7(n1);
    n7 -= n6;
    EXPECT_EQ(n4, n7);
    Normal n8(n1);
    n8 += n6;
    EXPECT_EQ(n5, n8);
}



/*! \brief Unit test of multiplication of a normal with a scalar.
 */
TEST(NormalTest, ScalarMultiplication)
{
    Normal n1(1.0, 0.5, 2.2);
    Normal n2(2.0, 1.0, 4.4);
    Normal n3(0.5, 0.25, 1.1);
    double a = 2.0;
    EXPECT_EQ(n2, n1 * a);
    EXPECT_EQ(n3, n1 / a);
}



/*! \brief Unit test of normal dot product.
 */
TEST(NormalTest, DotProduct)
{
    Normal n1(1.0, 0.5, 2.0);

    Vector3D v1(5.0, -2.0, -2.0);
    EXPECT_EQ(0.0, n1 * v1);

    Vector3D v2(2.0, -4.0, 1.0);
    EXPECT_EQ(2.0, n1 * v2);
}



/*! \brief Unit test of normalization.
 */
TEST(NormalTest, Normalization)
{
    Normal n1(5.0, 0.0, 0.0);
    Normal n2(1.0, 0.0, 0.0);
    n1.normalize();
    EXPECT_EQ(n2, n1);
}