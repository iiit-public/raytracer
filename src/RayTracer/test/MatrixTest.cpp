// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file MatrixTest.cpp
 *  \author Thomas Nuernberg
 *  \date 03.03.2015
 *  \brief Unit test of class Matrix4x4.
 */

#include "gtest/gtest.h"

#include <stdexcept>

#include "Utilities/Matrix4x4.hpp"

using namespace iiit;



/*! \brief Unit test of matrix comparison.
 */
TEST(MatrixTest, MatrixComparison)
{
    Matrix4x4 m1(4.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 2.0, 0.0,
        0.0, 1.0, 2.0, 0.0,
        1.0, 0.0, 0.0, 1.0);
    EXPECT_EQ(m1, m1);

    Matrix4x4 m2(0.25, 0.0, 0.0, 0.0,
        0.0, -1.0, 1.0, 0.0,
        0.0, 0.5, 0.0, 0.0,
        -0.25, 0.0, 0.0, 1.0);
    EXPECT_NE(m1, m2);
}



/*! \brief Unit test of different vector constructors.
 */
TEST(MatrixTest, MatrixConstructor)
{
    Matrix4x4 m1(4.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 2.0, 0.0,
        0.0, 1.0, 2.0, 0.0,
        1.0, 0.0, 0.0, 1.0);
    double m2[4][4];
    m2[0][0] = 4.0;
    m2[0][1] = 0.0;
    m2[0][2] = 0.0;
    m2[0][3] = 0.0;
    m2[1][0] = 0.0;
    m2[1][1] = 0.0;
    m2[1][2] = 2.0;
    m2[1][3] = 0.0;
    m2[2][0] = 0.0;
    m2[2][1] = 1.0;
    m2[2][2] = 2.0;
    m2[2][3] = 0.0;
    m2[3][0] = 1.0;
    m2[3][1] = 0.0;
    m2[3][2] = 0.0;
    m2[3][3] = 1.0;
    Matrix4x4 m3(m2);
    EXPECT_EQ(m1, m3);

    Matrix4x4 m4(1.0, 0.0, 0.0, 0.0,
        0.0, 1.0, 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        0.0, 0.0, 0.0, 1.0);
    Matrix4x4 m5;
    EXPECT_EQ(m4, m5);

    Matrix4x4 m6(m1);
    EXPECT_EQ(m1, m6);
}



/*! \brief Unit test of element access methods.
 */
TEST(MatrixTest, ElementAccess)
{
    Matrix4x4 m1(4.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 2.0, 0.0,
        0.0, 1.0, 2.0, 0.0,
        1.0, 0.0, 0.0, 1.0);
    EXPECT_EQ(4.0, m1[0][0]);
    EXPECT_EQ(4.0, m1(1, 1));
    EXPECT_EQ(1.0, m1[2][1]);
    EXPECT_EQ(1.0, m1(3, 2));

    const Matrix4x4 m2(4.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 2.0, 0.0,
        0.0, 1.0, 2.0, 0.0,
        1.0, 0.0, 0.0, 1.0);
    EXPECT_EQ(4.0, m2[0][0]);
    EXPECT_EQ(4.0, m2(1, 1));
    EXPECT_EQ(1.0, m2[2][1]);
    EXPECT_EQ(1.0, m2(3, 2));
}



/*! \brief Unit test of matrix assignment operator.
 */
TEST(MatrixTest, MatrixAssignment)
{
    Matrix4x4 m1(4.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 2.0, 0.0,
        0.0, 1.0, 2.0, 0.0,
        1.0, 0.0, 0.0, 1.0);
    Matrix4x4 m2;
    EXPECT_NE(m1, m2);

    m2 = m1;
    EXPECT_EQ(m1, m2);

    Matrix4x4 m3;
    m2 = m3;
    EXPECT_NE(m1, m2);
    EXPECT_EQ(m2, m3);
}



/*! \brief Unit test of default matrix constructor.
 */
TEST(MatrixTest, IdentityMatrix)
{
    Matrix4x4 m1(1.0, 0.0, 0.0, 0.0,
        0.0, 1.0, 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        0.0, 0.0, 0.0, 1.0);
    EXPECT_EQ(m1, m1);

    Matrix4x4 m2;
    EXPECT_EQ(m1, m2);
    EXPECT_EQ(m1, m1.inverse());
    EXPECT_EQ(m1, m2.inverse());
}



/*! \brief Unit test of matrix transposition.
 */
TEST(MatrixTest, MarixTransposition)
{
    Matrix4x4 m1;
    EXPECT_EQ(m1, m1.transpose());

    Matrix4x4 m2(4.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 2.0, 0.0,
        0.0, 1.0, 2.0, 0.0,
        1.0, 0.0, 0.0, 1.0);
    Matrix4x4 m3(4.0, 0.0, 0.0, 1.0,
        0.0, 0.0, 1.0, 0.0,
        0.0, 2.0, 2.0, 0.0,
        0.0, 0.0, 0.0, 1.0);
    EXPECT_EQ(m2, m3.transpose());
    EXPECT_EQ(m3, m2.transpose());
}



/*! \brief Unit test of matrix multiplication.
 */
TEST(MatrixTest, MatrixMultiplication)
{
    Matrix4x4 m1(4.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 2.0, 0.0,
        0.0, 1.0, 2.0, 0.0,
        1.0, 0.0, 0.0, 1.0);
    Matrix4x4 m2(16.0, 0.0, 0.0, 0.0,
        0.0, 2.0, 4.0, 0.0,
        0.0, 2.0, 6.0, 0.0,
        5.0, 0.0, 0.0, 1.0);
    Matrix4x4 m3 = m1 * m1;
    EXPECT_EQ(m3, m2);
}



/*! \brief Unit test of multiplication of a matrix with a scalar.
 */
TEST(MatrixTest, ScalarMultiplication)
{
    Matrix4x4 m1(4.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 2.0, 0.0,
        0.0, 1.0, 2.0, 0.0,
        1.0, 0.0, 0.0, 1.0);
    Matrix4x4 m2(16.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 8.0, 0.0,
        0.0, 4.0, 8.0, 0.0,
        4.0, 0.0, 0.0, 4.0);
    Matrix4x4 m3 = m1 * 4.0;
    EXPECT_EQ(m3, m2);
}



/*! \brief Unit test of matrix inversion.
 */
TEST(MatrixTest, MatrixInversion)
{
    Matrix4x4 m1(4.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 2.0, 0.0,
        0.0, 1.0, 2.0, 0.0,
        1.0, 0.0, 0.0, 1.0);

    Matrix4x4 m2(0.25, 0.0, 0.0, 0.0,
        0.0, -1.0, 1.0, 0.0,
        0.0, 0.5, 0.0, 0.0,
        -0.25, 0.0, 0.0, 1.0);
    ASSERT_NO_THROW(m1.inverse());
    ASSERT_NO_THROW(m2.inverse());
    EXPECT_EQ(m2, m1.inverse());
    EXPECT_EQ(m1, m2.inverse());

    Matrix4x4 m3(4.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 2.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        1.0, 0.0, 0.0, 1.0);
    Matrix4x4 m4;
    EXPECT_THROW(m4 = m3.inverse(), std::runtime_error);
}