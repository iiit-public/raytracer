// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file InstanceTest.cpp
 *  \author Thomas Nuernberg
 *  \date 14.05.2015
 *  \brief Unit test of class Instance.
 */

#include "gtest/gtest.h"

#include "Scene.hpp"
#include "Spectrum/RgbSpectrum.hpp"
#include "Objects/Instance.hpp"
#include "Objects/Sphere.hpp"
#include "Utilities/Constants.h"
#include "Utilities/Ray.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/Normal.hpp"
#include "Utilities/ShadingData.hpp"

using namespace iiit;



/*! \brief Unit test of instance translation.
 */
TEST(InstanceTest, TranslationTest)
{
    Point3D center(0.0, 0.0, 0.0);
    double radius = 1.0;
    std::shared_ptr<GeometricObject<RgbSpectrum> > sphere(new Sphere<RgbSpectrum>(center, radius));

    Instance<RgbSpectrum> instance(sphere);

    Point3D origin(-2.0, 0.0, 0.0);
    Vector3D direction(1.0, 0.0, 0.0);
    Ray ray(origin, direction);

    double t;
    std::shared_ptr<Scene<RgbSpectrum> > scene(new Scene<RgbSpectrum>);
    ShadingData<RgbSpectrum> shadingData(scene);
    EXPECT_TRUE(instance.hit(ray, t, shadingData));

    instance.translate(2.0, 0.0, 0.0);
    EXPECT_TRUE(instance.hit(ray, t, shadingData));

    instance.translate(0.0, 2.0, 0.0);
    EXPECT_FALSE(instance.hit(ray, t, shadingData));
}



/*! \brief Unit test of instance rotation.
 */
TEST(InstanceTest, RotationTest)
{
    Point3D center(0.0, 0.0, 0.0);
    double radius = 1.0;
    std::shared_ptr<GeometricObject<RgbSpectrum>> sphere(new Sphere<RgbSpectrum>(center, radius));

    Instance<RgbSpectrum> instance(sphere);

    Point3D origin(-2.0, 0.0, 0.0);
    Vector3D direction(1.0, 0.0, 0.0);
    Ray ray(origin, direction);

    double tolerance = 1e-10; // tolerance use for floating point comparison, when expected value is 0

    double t;
    std::shared_ptr<Scene<RgbSpectrum>> scene(new Scene<RgbSpectrum>);
    ShadingData<RgbSpectrum> shadingData(scene);
    Point3D expectedhitPoint(-1.0, 0.0, 0.0);
    Normal expectedNormal(-1.0, 0.0, 0.0);
    EXPECT_TRUE(instance.hit(ray, t, shadingData));
    EXPECT_EQ(expectedhitPoint, shadingData.hitPoint_);
    EXPECT_EQ(expectedNormal, shadingData.normal_);

    instance.rotateX(static_cast<float>(PI / 4.0));
    EXPECT_TRUE(instance.hit(ray, t, shadingData));
    EXPECT_FLOAT_EQ(static_cast<float>(expectedhitPoint.x_), static_cast<float>(shadingData.hitPoint_.x_)); // float precision sufficient
    EXPECT_NEAR(expectedhitPoint.y_, shadingData.hitPoint_.y_, tolerance);
    EXPECT_NEAR(expectedhitPoint.z_, shadingData.hitPoint_.z_, tolerance);
    EXPECT_FLOAT_EQ(static_cast<float>(expectedNormal.x_), static_cast<float>(shadingData.normal_.x_));
    EXPECT_NEAR(expectedNormal.y_, shadingData.normal_.y_, tolerance);
    EXPECT_NEAR(expectedNormal.z_, shadingData.normal_.z_, tolerance);

    instance.rotateX(static_cast<float>(PI / 4.0));
    instance.rotateY(static_cast<float>(3.0 * PI / 4.0));
    instance.rotateZ(static_cast<float>(3.0 * PI / 2.0));
    EXPECT_TRUE(instance.hit(ray, t, shadingData));
    EXPECT_FLOAT_EQ(static_cast<float>(expectedhitPoint.x_), static_cast<float>(shadingData.hitPoint_.x_));
    EXPECT_NEAR(expectedhitPoint.y_, shadingData.hitPoint_.y_, tolerance);
    EXPECT_NEAR(expectedhitPoint.z_, shadingData.hitPoint_.z_, tolerance);
    EXPECT_FLOAT_EQ(static_cast<float>(expectedNormal.x_), static_cast<float>(shadingData.normal_.x_));
    EXPECT_NEAR(expectedNormal.y_, shadingData.normal_.y_, tolerance);
    EXPECT_NEAR(expectedNormal.z_, shadingData.normal_.z_, tolerance);
}



/*! \brief Unit test of instance scaling.
 */
TEST(InstanceTest, ScaleTest)
{
    Point3D center(0.0, 0.0, 0.0);
    double radius = 1.0;
    std::shared_ptr<GeometricObject<RgbSpectrum>> sphere(new Sphere<RgbSpectrum>(center, radius));

    Instance<RgbSpectrum> instance(sphere);

    Point3D origin(-2.0, 0.0, 0.0);
    Vector3D direction(1.0, 0.0, 0.0);
    Ray hittingRay(origin, direction);

    origin.set(-2.0, 2.0, 0.0);
    Ray missingRay(origin, direction);

    double t;
    std::shared_ptr<Scene<RgbSpectrum>> scene(new Scene<RgbSpectrum>);
    ShadingData<RgbSpectrum> shadingData(scene);
    EXPECT_TRUE(instance.hit(hittingRay, t, shadingData));
    EXPECT_FALSE(instance.hit(missingRay, t, shadingData));

    instance.scale(1.5f, 1.1f, 3.2f);
    EXPECT_TRUE(instance.hit(hittingRay, t, shadingData));
    EXPECT_FALSE(instance.hit(missingRay, t, shadingData));

    instance.scale(1.f, 1.9f, 1.f);
    EXPECT_TRUE(instance.hit(hittingRay, t, shadingData));
    EXPECT_TRUE(instance.hit(missingRay, t, shadingData));
}
