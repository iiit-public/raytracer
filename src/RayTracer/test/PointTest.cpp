// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file PointTest.cpp
 *  \author Thomas Nuernberg
 *  \date 13.03.2015
 *  \brief Unit test of class Point3D.
 */

#include "gtest/gtest.h"

#include "Utilities/Constants.h"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"

using namespace iiit;



/*! \brief Unit test of point comparison.
 */
TEST(PointTest, PointComparison)
{
    Point3D p1(1.0, 0.5, 2.2);
    EXPECT_EQ(p1, p1);

    Point3D p2(-4.0, 1.45e4, 86.22);
    EXPECT_NE(p1, p2);
}



/*! \brief Unit test of different point constructors.
 */
TEST(PointTest, PointConstructor)
{
    Point3D p1(1.0, 0.5, 2.2);

    Point3D p2(p1);
    EXPECT_EQ(p1, p2);

    Vector3D v1(1.0, 0.5, 2.2);
    Point3D p3(v1);
    EXPECT_EQ(p1, p3);
}



/*! \brief Unit test of element access methods.
 *
 *  Data validity as well as out of range access are tested.
 */
TEST(PointTest, ElementAccess)
{
    const Point3D p1(1.0, 0.5, 2.2);
    ASSERT_NO_THROW(p1(1));
    EXPECT_EQ(1.0, p1(1));
    ASSERT_NO_THROW(p1[0]);
    EXPECT_EQ(1.0, p1[0]);
    ASSERT_NO_THROW(p1(2));
    EXPECT_EQ(0.5, p1(2));
    ASSERT_NO_THROW(p1[1]);
    EXPECT_EQ(0.5, p1[1]);
    ASSERT_NO_THROW(p1(3));
    EXPECT_EQ(2.2, p1(3));
    ASSERT_NO_THROW(p1[2]);
    EXPECT_EQ(2.2, p1[2]);

    Point3D p2(1.0, 0.5, 2.2);
    EXPECT_NO_THROW(p2(1) = 2.0);
    EXPECT_NO_THROW(p2(2) = 2.0);
    EXPECT_NO_THROW(p2(3) = 2.0);
    EXPECT_EQ(2.0, p2(1));
    EXPECT_EQ(2.0, p2(2));
    EXPECT_EQ(2.0, p2(3));

    EXPECT_NO_THROW(p2[0] = 3.0);
    EXPECT_NO_THROW(p2[1] = 3.0);
    EXPECT_NO_THROW(p2[2] = 3.0);
    EXPECT_EQ(3.0, p2(1));
    EXPECT_EQ(3.0, p2(2));
    EXPECT_EQ(3.0, p2(3));

    double a;
    EXPECT_THROW(a = p1(0), std::out_of_range);
    EXPECT_THROW(a = p1(4), std::out_of_range);
    EXPECT_THROW(p2(0) = 0.0, std::out_of_range);
    EXPECT_THROW(p2(4) = 0.0, std::out_of_range);
    EXPECT_THROW(a = p1[3], std::out_of_range);
    EXPECT_THROW(p2[3] = 0.0, std::out_of_range);
}



/*! \brief Unit test of point assignment operator.
 */
TEST(PointTest, PointAssignment)
{
    Point3D p1(1.0, 0.5, 2.2);
    Point3D p2;
    EXPECT_NE(p1, p2);

    p2 = p1;
    EXPECT_EQ(p1, p2);

    Point3D p3;
    p2 = p3;
    EXPECT_NE(p1, p2);
    EXPECT_EQ(p2, p3);
}



/*! \brief Unit test of default point constructor.
 */
TEST(PointTest, ZeroVector)
{
    Point3D p1(0.0, 0.0, 0.0);
    EXPECT_EQ(p1, p1);

    Point3D p2;
    EXPECT_EQ(p1, p2);
}



/*! \brief Unit test of unary minus operator.
 */
TEST(PointTest, UnaryMinus)
{
    Point3D p1(1.0, 0.5, 2.2);
    Point3D p2(-1.0, -0.5, -2.2);
    EXPECT_NE(p1, p2);
    EXPECT_EQ(p1, -p2);
    EXPECT_EQ(p2, -p1);
}



/*! \brief Unit test of minus operator.
 */
TEST(PointTest, PointDifference)
{
    Point3D p1(1.0, 0.5, 2.2);
    Point3D p2;
    Vector3D v1(p1);
    EXPECT_EQ(v1, p1 - p2);
    EXPECT_EQ(v1, -(p2 - p1));

    Point3D p3(8.0, -2.0, 6.5);
    Vector3D v2(-7.0, 2.5, -4.3);
    EXPECT_EQ(v2, p1 - p3);
    EXPECT_EQ(v2, -(p3 - p1));
}



/*! \brief Unit test of add operator.
 */
TEST(PointTest, VectorAddition)
{
    Point3D p1(1.0, 0.5, 2.2);
    Vector3D v1;
    EXPECT_EQ(p1, p1 + v1);
    EXPECT_EQ(p1, p1 - v1);

    Point3D p3(8.0, -2.0, 2.2 - 4.3);
    // numerical problem of double precision
    // EXPECT_EQ(-2.1, 2.2 - 4.3); // fails
    Point3D p4(-6.0, 3.0, 6.5);
    Vector3D v2(-7.0, 2.5, 4.3);
    EXPECT_EQ(p3, p1 - v2);
    EXPECT_EQ(p4, p1 + v2);
}



/*! \brief Unit test of multiplication of a vector with a scalar.
 */
TEST(PointTest, ScalarMultiplication)
{
    Point3D p1(1.0, 0.5, 2.2);
    Point3D p2(2.0, 1.0, 4.4);
    double a = 2.0;
    EXPECT_EQ(p2, p1 * a);
}



/*! \brief Unit test of point distance calculation.
 */
TEST(PointTest, PointDistance)
{
    Point3D p1(1.0, 0.5, 2.2);
    Point3D p2(3.0, -0.5, 4.2);
    EXPECT_EQ(0.0, p1.distance(p1));
    EXPECT_EQ(3.0, p1.distance(p2));
    EXPECT_EQ(3.0, p2.distance(p1));
}


/*! \brief Unit test of x rotations
 */
TEST(PointTest, PointRotX)
{
    Point3D p1(0.0, 0.0, 1.0);
    Point3D p1ref(p1);
    Point3D p2(0.0, 1.0, 0.0);
    Point3D p3(0.0, 0.0, -1.0);
    Point3D p4(0.0, -1.0, 0.0);

    // rotate and rotate back
    p1.rotate_x(0.12);
    p1.rotate_x(-0.12);
    EXPECT_NEAR(p1.x_, p1ref.x_, 0.001);
    EXPECT_NEAR(p1.y_, p1ref.y_, 0.001);
    EXPECT_NEAR(p1.z_, p1ref.z_, 0.001);

    // rotate 90deg
    p1.rotate_x(-PI/2);
    EXPECT_NEAR(p1.x_, p2.x_, 0.001);
    EXPECT_NEAR(p1.y_, p2.y_, 0.001);
    EXPECT_NEAR(p1.z_, p2.z_, 0.001);

    p1.rotate_x(-PI/2);
    EXPECT_NEAR(p1.x_, p3.x_, 0.001);
    EXPECT_NEAR(p1.y_, p3.y_, 0.001);
    EXPECT_NEAR(p1.z_, p3.z_, 0.001);

    p1.rotate_x(-PI/2);
    EXPECT_NEAR(p1.x_, p4.x_, 0.001);
    EXPECT_NEAR(p1.y_, p4.y_, 0.001);
    EXPECT_NEAR(p1.z_, p4.z_, 0.001);

    p1.rotate_x(-PI/2);
    EXPECT_NEAR(p1.x_, p1ref.x_, 0.001);
    EXPECT_NEAR(p1.y_, p1ref.y_, 0.001);
    EXPECT_NEAR(p1.z_, p1ref.z_, 0.001);

    p1.rotate_x(-PI/4);
    EXPECT_NEAR(p1.x_, p1ref.x_, 0.001);
    EXPECT_NEAR(p1.y_, 1.0/SQRT_TWO, 0.001);
    EXPECT_NEAR(p1.z_, 1.0/SQRT_TWO, 0.001);
}


/*! \brief Unit test of y rotations
 */
TEST(PointTest, PointRotY)
{
    Point3D p1(0.0, 0.0, 1.0);
    Point3D p1ref(p1);
    Point3D p2(1.0, 0.0, 0.0);
    Point3D p3(0.0, 0.0, -1.0);
    Point3D p4(-1.0, 0.0, 0.0);

    // rotate and rotate back
    p1.rotate_y(0.12);
    p1.rotate_y(-0.12);
    EXPECT_NEAR(p1.x_, p1ref.x_, 0.001);
    EXPECT_NEAR(p1.y_, p1ref.y_, 0.001);
    EXPECT_NEAR(p1.z_, p1ref.z_, 0.001);

    // rotate 90deg
    p1.rotate_y(PI/2);
    EXPECT_NEAR(p1.x_, p2.x_, 0.001);
    EXPECT_NEAR(p1.y_, p2.y_, 0.001);
    EXPECT_NEAR(p1.z_, p2.z_, 0.001);

    p1.rotate_y(PI/2);
    EXPECT_NEAR(p1.x_, p3.x_, 0.001);
    EXPECT_NEAR(p1.y_, p3.y_, 0.001);
    EXPECT_NEAR(p1.z_, p3.z_, 0.001);

    p1.rotate_y(PI/2);
    EXPECT_NEAR(p1.x_, p4.x_, 0.001);
    EXPECT_NEAR(p1.y_, p4.y_, 0.001);
    EXPECT_NEAR(p1.z_, p4.z_, 0.001);

    p1.rotate_y(PI/2);
    EXPECT_NEAR(p1.x_, p1ref.x_, 0.001);
    EXPECT_NEAR(p1.y_, p1ref.y_, 0.001);
    EXPECT_NEAR(p1.z_, p1ref.z_, 0.001);

    p1.rotate_y(PI/4);
    EXPECT_NEAR(p1.x_, 1.0/SQRT_TWO, 0.001);
    EXPECT_NEAR(p1.y_, p1ref.y_, 0.001);
    EXPECT_NEAR(p1.z_, 1.0/SQRT_TWO, 0.001);
}


/*! \brief Unit test of z rotations
 */
TEST(PointTest, PointRotZ)
{
    Point3D p1(1.0, 0.0, 0.0);
    Point3D p1ref(p1);
    Point3D p2(0.0, 1.0, 0.0);
    Point3D p3(-1.0, 0.0, 0.0);
    Point3D p4(0.0, -1.0, 0.0);

    // rotate and rotate back
    p1.rotate_z(0.12);
    p1.rotate_z(-0.12);
    EXPECT_NEAR(p1.x_, p1ref.x_, 0.001);
    EXPECT_NEAR(p1.y_, p1ref.y_, 0.001);
    EXPECT_NEAR(p1.z_, p1ref.z_, 0.001);

    // rotate 90deg
    p1.rotate_z(PI/2);
    EXPECT_NEAR(p1.x_, p2.x_, 0.001);
    EXPECT_NEAR(p1.y_, p2.y_, 0.001);
    EXPECT_NEAR(p1.z_, p2.z_, 0.001);

    p1.rotate_z(PI/2);
    EXPECT_NEAR(p1.x_, p3.x_, 0.001);
    EXPECT_NEAR(p1.y_, p3.y_, 0.001);
    EXPECT_NEAR(p1.z_, p3.z_, 0.001);

    p1.rotate_z(PI/2);
    EXPECT_NEAR(p1.x_, p4.x_, 0.001);
    EXPECT_NEAR(p1.y_, p4.y_, 0.001);
    EXPECT_NEAR(p1.z_, p4.z_, 0.001);

    p1.rotate_z(PI/2);
    EXPECT_NEAR(p1.x_, p1ref.x_, 0.001);
    EXPECT_NEAR(p1.y_, p1ref.y_, 0.001);
    EXPECT_NEAR(p1.z_, p1ref.z_, 0.001);

    p1.rotate_z(PI/4);
    EXPECT_NEAR(p1.x_, 1.0/SQRT_TWO, 0.001);
    EXPECT_NEAR(p1.y_, 1.0/SQRT_TWO, 0.001);
    EXPECT_NEAR(p1.z_, p1ref.z_, 0.001);
}
