// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file ImageApertureTest.cpp
 *  \author Stefano Blum
 *  \date 25.10.2015
 *  \author Sandro Braun
 *  \date 2.8.2016
 *  \brief Unit test of class ImageAperture.
 */

#include "gtest/gtest.h"

#include "Cameras/ImageAperture.hpp"
#include "Utilities/Ray.hpp"
#include "Utilities/Point2D.hpp"
#include "Utilities/Constants.h"

#include <math.h>

using namespace iiit;



/*! \brief Unit test of a ray hitting a circular aperture.
 */
TEST(ImageApertureTest, CircularApertureTest)
{
    float aperturePosition = 0.f;
    float lensRadius = 1.f;
    ImageAperture aperture(aperturePosition, lensRadius);
    aperture.setCircularAperture(3, 3, 1);
    aperture.setApertureOutsideLens();

    // center ray should not get attenuated
    Ray ray(Point3D(0.0, 0.0, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));

    // off-lens ray should be attenuated completely
    ray.set(Point3D(0.9 * lensRadius, 0.9 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));

    // off-aperture ray should be attenuated completely
    ray.set(Point3D(1.1 * lensRadius, 1.1 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));
}



/*! \brief Unit test of a ray hitting a ring aperture.
 */
TEST(ImageApertureTest, RingApertureTest)
{
    float aperturePosition = 0.f;
    float lensRadius = 1.f;
    ImageAperture aperture(aperturePosition, lensRadius);
    aperture.setCircularRingAperture(100, 100, 25, 50);
    aperture.setApertureOutsideLens();

    // center ray should get attenuated completely
    Ray ray(Point3D(0.0, 0.0, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));

    // border rays should not get attenuated
    ray.set(Point3D(0.0 * lensRadius, 0.75 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));
    ray.set(Point3D(0.75 * lensRadius, 0.0 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));
    ray.set(Point3D(0.0 * lensRadius, -0.75 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));
    ray.set(Point3D(-0.75 * lensRadius, 0.0 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));

    // off-lens ray should be attenuated completely
    ray.set(Point3D(0.9 * lensRadius, 0.9 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));

    // off-aperture ray should be attenuated completely
    ray.set(Point3D(1.1 * lensRadius, 1.1 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));
}



/*! \brief Unit test of a ray hitting a rectangular aperture.
 */
TEST(ImageApertureTest, RectangularApertureTest)
{
    float aperturePosition = 0.f;
    float lensRadius = 1.f;
    ImageAperture aperture(aperturePosition, lensRadius);
    aperture.setRectangularAperture(200, 200, 75, 75);
    aperture.setApertureOutsideLens();

    // center ray should not get attenuated
    Ray ray(Point3D(0.0, 0.0, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));

    // border rays should get attenuated completely
    ray.set(Point3D(0.0 * lensRadius, 0.85 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));
    ray.set(Point3D(0.85 * lensRadius, 0.0 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));
    ray.set(Point3D(0.0 * lensRadius, -0.85 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));
    ray.set(Point3D(-0.85 * lensRadius, 0.0 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));

    // off-lens ray should not be attenuated as the aperture does not check the lens dimensions
    ray.set(Point3D(0.73 * lensRadius, 0.73 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));

    // off-aperture ray should be attenuated completely
    ray.set(Point3D(1.1 * lensRadius, 1.1 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));

    aperture.setRectangularAperture(200, 200);
    aperture.setApertureOutsideLens();

    // center ray should not get attenuated
    ray.set(Point3D(0.0, 0.0, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));

    // border rays not get attenuated
    ray.set(Point3D(0.0 * lensRadius, 0.85 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));
    ray.set(Point3D(0.85 * lensRadius, 0.0 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));
    ray.set(Point3D(0.0 * lensRadius, -0.85 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));
    ray.set(Point3D(-0.85 * lensRadius, 0.0 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));

    // off-lens ray should not be attenuated as the aperture does not check the lens dimensions
    ray.set(Point3D(0.73 * lensRadius, 0.73 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));

    // off-aperture ray should be attenuated completely
    ray.set(Point3D(1.1 * lensRadius, 1.1 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));
}



/*! \brief Unit test of a ray hitting a levin aperture.
 */
TEST(ImageApertureTest, LevinApertureTest)
{
    float aperturePosition = 0.f;
    float lensRadius = 1.f;
    ImageAperture aperture(aperturePosition, lensRadius);
    aperture.setLevinAperture();
    aperture.setApertureInsideLens();

    // center ray should not get attenuated
    Ray ray(Point3D(0.0, 0.0, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));

    double sqrt2Half = sqrt(2.0) / 2.0;

    // half horizontal rays should get attenuated completely
    ray.set(Point3D(0.5 * lensRadius * sqrt2Half, 0.0, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));
    ray.set(Point3D(-0.5 * lensRadius * sqrt2Half, 0.0, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));

    // half vertical rays should not get attenuated
    ray.set(Point3D(0.0, 0.5 * lensRadius * sqrt2Half, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));
    ray.set(Point3D(0.0, -0.5 * lensRadius * sqrt2Half, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));
}



/*! \brief Unit test of aperture scaling.
 */
TEST(ImageApertureTest, ScaleTest)
{

    //1 . aperture outside lens
    float aperturePosition = 0.f;
    float lensRadius = 5.f;
    ImageAperture aperture(aperturePosition, lensRadius);
    aperture.setRectangularAperture(8, 6);
    aperture.setApertureOutsideLens();
    
    // center ray should not get attenuated
    Ray ray(Point3D(0.0, 0.0, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));

    // on-lens ray should not get attenuated
    ray.set(Point3D(0.0, 3.1, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));

    // on-aperture should not get attenuated
    ray.set(Point3D(4.5, 0.0, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));

    //2. aperture inside lens
    aperture.setApertureInsideLens();
    // center ray should still not get attenuated
    ray.set(Point3D(0.0, 0.0, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));

    // previous on-lens ray should now be off-lens
    ray.set(Point3D(0.0, 3.1, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));

    // previous on-lens ray should now be off-lens
    ray.set(Point3D(4.5, 0.0, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));
}



/*! \brief Unit test of aperture pixel aspect ratio.
 */
TEST(ImageApertureTest, AspectRatioTest)
{
    float aperturePosition = 0.f;
    float lensRadius = 5.f;
    ImageAperture aperture(aperturePosition, lensRadius);
    aperture.setPixelAspectRatio(2.f);
    aperture.setRectangularAperture(4, 6);
    aperture.setApertureOutsideLens();
    
    // center ray should not get attenuated
    Ray ray(Point3D(0.0, 0.0, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));

    // on-lens ray should not get attenuated
    ray.set(Point3D(0.0, 3.1, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));

    // on-aperture should not get attenuated
    ray.set(Point3D(4.5, 0.0, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));

    aperture.setApertureInsideLens();

    // center ray should still not get attenuated
    ray.set(Point3D(0.0, 0.0, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));

    // previous on-lens ray should now be off-lens
    ray.set(Point3D(0.0, 3.1, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));

    // previous on-lens ray should now be off-lens
    ray.set(Point3D(4.5, 0.0, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));
}



/*! \brief Unit test of aperture pixel offset.
 */
TEST(ImageApertureTest, OffsetTest)
{
    float aperturePosition = 0.f;
    float lensRadius = 1.f;
    ImageAperture aperture(aperturePosition, lensRadius);
    aperture.setCircularAperture(3, 3, 1);
    aperture.setApertureOutsideLens();
    

    // center ray should not get attenuated
    Ray ray(Point3D(0.0, 0.0, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));

    // off center ray should be attenuated completely
    ray.set(Point3D(0.75, 0.75, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));

    // shift aperture
    aperture.setApertureTranslation(1, 1);

    // center ray now should be attenuated completely
    ray.set(Point3D(0.0, 0.0, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));

    // off center ray now should not get attenuated
    ray.set(Point3D(0.75, 0.75, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));
}



/*! \brief Unit test of aperture rotation.
 */
TEST(ImageApertureTest, RotationTest)
{
    float aperturePosition = 0.f;
    float lensRadius = 1.f;
    ImageAperture aperture(aperturePosition, lensRadius);
    aperture.setRectangularAperture(100, 100);
    aperture.setApertureInsideLens();

    double sqrt2Half = sqrt(2.0) / 2.0;

    // before rotation
    Ray ray(Point3D(0.0, 0.0, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));
    ray.set(Point3D(-0.8 * lensRadius, 0.0, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));
    ray.set(Point3D(-0.8 * lensRadius * sqrt2Half, 0.8 * lensRadius * sqrt2Half, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));
    ray.set(Point3D(0.0, 0.8 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));
    ray.set(Point3D(0.8 * lensRadius * sqrt2Half, 0.8 * lensRadius * sqrt2Half, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));
    ray.set(Point3D(0.8 * lensRadius, 0.0, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));
    ray.set(Point3D(0.8 * lensRadius * sqrt2Half, -0.8 * lensRadius * sqrt2Half, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));
    ray.set(Point3D(0.0, -0.8 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));
    ray.set(Point3D(-0.8 * lensRadius * sqrt2Half, -0.8 * lensRadius * sqrt2Half, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));

    aperture.setInPlaneRotation(PI_4);

    // after rotation
    ray.set(Point3D(0.0, 0.0, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));
    ray.set(Point3D(-0.8 * lensRadius, 0.0, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));
    ray.set(Point3D(-0.8 * lensRadius * sqrt2Half, 0.8 * lensRadius * sqrt2Half, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));
    ray.set(Point3D(0.0, 0.8 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));
    ray.set(Point3D(0.8 * lensRadius * sqrt2Half, 0.8 * lensRadius * sqrt2Half, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));
    ray.set(Point3D(0.8 * lensRadius, 0.0, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));
    ray.set(Point3D(0.8 * lensRadius * sqrt2Half, -0.8 * lensRadius * sqrt2Half, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));
    ray.set(Point3D(0.0, -0.8 * lensRadius, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(1.f, aperture.attenuateRay(ray));
    ray.set(Point3D(-0.8 * lensRadius * sqrt2Half, -0.8 * lensRadius * sqrt2Half, -1.0), Vector3D(0.0, 0.0, 1.0));
    EXPECT_EQ(0.f, aperture.attenuateRay(ray));
}