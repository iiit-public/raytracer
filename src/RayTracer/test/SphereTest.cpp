// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file SphereTest.cpp
 *  \author Thomas Nuernberg
 *  \date 24.04.2015
 *  \brief Unit test of class Sphere.
 */

#include "gtest/gtest.h"

#include "Scene.hpp"
#include "Spectrum/RgbSpectrum.hpp"
#include "Objects/Sphere.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/Normal.hpp"
#include "Utilities/Ray.hpp"
#include "Utilities/ShadingData.hpp"

using namespace iiit;



/*! \brief Unit of sphere hit() function.
 *
 *  Test of a ray hitting a sphere centrally.
 */
TEST(SphereTest, HitSphereTest)
{
    Point3D spherePoint(0.0, 0.0, 0.0);
    double sphereRadius = 1.0;
    Sphere<RgbSpectrum> sphere(spherePoint, sphereRadius);

    Point3D rayPoint(-2.0, 0.0, 0.0);
    Vector3D rayVector(1.0, 0.0, 0.0);
    Ray ray(rayPoint, rayVector);

    double tmin;
    std::shared_ptr<Scene<RgbSpectrum> > scene(new Scene<RgbSpectrum>);
    ShadingData<RgbSpectrum> shadingData(scene);
    bool hit = sphere.hit(ray, tmin, shadingData);
    EXPECT_TRUE(hit);
    EXPECT_DOUBLE_EQ(1.0, tmin);
    EXPECT_EQ(Normal(-1.0, 0.0, 0.0), shadingData.normal_);
    EXPECT_EQ(Point3D(-1.0, 0.0, 0.0), shadingData.hitPoint_);
    hit = sphere.shadowHit(ray, tmin);
    EXPECT_TRUE(hit);
}



/*! \brief Unit of sphere hit() function.
 *
 *  Test of a ray missing a sphere.
 */
TEST(PlaneTest, MissSphereTest)
{
    Point3D spherePoint(0.0, 0.0, 0.0);
    double sphereRadius = 1.0;
    Sphere<RgbSpectrum> sphere(spherePoint, sphereRadius);

    Point3D rayPoint(-2.0, 0.0, 0.0);
    Vector3D rayVector(1.0, 1.0, 0.0);
    Ray ray(rayPoint, rayVector);

    double tmin;
    std::shared_ptr<Scene<RgbSpectrum> > scene(new Scene<RgbSpectrum>);
    ShadingData<RgbSpectrum> shadingData(scene);
    bool hit = sphere.hit(ray, tmin, shadingData);
    EXPECT_FALSE(hit);
    hit = sphere.shadowHit(ray, tmin);
    EXPECT_FALSE(hit);
}



/*! \brief Unit of sphere hit() function.
 *
 *  Test of a ray hitting a sphere tangentially.
 */
TEST(PlaneTest, TangentSphereTest)
{
    Point3D spherePoint(0.0, 0.0, 0.0);
    double sphereRadius = 1.0;
    Sphere<RgbSpectrum> sphere(spherePoint, sphereRadius);

    Point3D rayPoint(-2.0, 1.0, 0.0);
    Vector3D rayVector(1.0, 0.0, 0.0);
    Ray ray(rayPoint, rayVector);

    double tmin;
    std::shared_ptr<Scene<RgbSpectrum> > scene(new Scene<RgbSpectrum>);
    ShadingData<RgbSpectrum> shadingData(scene);
    bool hit = sphere.hit(ray, tmin, shadingData);
    EXPECT_TRUE(hit);
    EXPECT_DOUBLE_EQ(2.0, tmin);
    EXPECT_EQ(Normal(0.0, 1.0, 0.0), shadingData.normal_);
    EXPECT_EQ(Point3D(0.0, 1.0, 0.0), shadingData.hitPoint_);
    hit = sphere.shadowHit(ray, tmin);
    EXPECT_TRUE(hit);
}



/*! \brief Unit of sphere calculateLocalHitPoint() function.
 */
TEST(SphereTest, LocalHitPointTest1)
{
    Point3D spherePoint(0.0, 0.0, 0.0);
    double sphereRadius = 1.0;
    Sphere<RgbSpectrum> sphere(spherePoint, sphereRadius);

    Point3D rayPoint(2.0, 2.0, 0.0);
    Vector3D rayVector(-1.0, -1.0, 0.0);
    Ray ray(rayPoint, rayVector);

    double tmin;
    std::shared_ptr<Scene<RgbSpectrum> > scene(new Scene<RgbSpectrum>);
    ShadingData<RgbSpectrum> shadingData(scene);
    sphere.hit(ray, tmin, shadingData);    
    EXPECT_EQ(Point2D(90.0, 45.0), shadingData.localHitPoint2D_); 
}



/*! \brief Unit of sphere calculateLocalHitPoint() function.
 */
TEST(SphereTest, LocalHitPointTest2)
{
    Point3D spherePoint(0.0, 0.0, 0.0);
    double sphereRadius = 1.0;
    Sphere<RgbSpectrum> sphere(spherePoint, sphereRadius);

    Point3D rayPoint(-2.0, 2.0, 0.0);
    Vector3D rayVector(1.0, -1.0, 0.0);
    Ray ray(rayPoint, rayVector);

    double tmin;
    std::shared_ptr<Scene<RgbSpectrum> > scene(new Scene<RgbSpectrum>);
    ShadingData<RgbSpectrum> shadingData(scene);
    sphere.hit(ray, tmin, shadingData);    
    EXPECT_EQ(Point2D(90.0, 135.0), shadingData.localHitPoint2D_); 
}



/*! \brief Unit of sphere calculateLocalHitPoint() function.
 */
TEST(SphereTest, LocalHitPointTest3)
{
    Point3D spherePoint(0.0, 0.0, 0.0);
    double sphereRadius = 1.0;
    Sphere<RgbSpectrum> sphere(spherePoint, sphereRadius);

    Point3D rayPoint(-2.0, -2.0, 0.0);
    Vector3D rayVector(1.0, 1.0, 0.0);
    Ray ray(rayPoint, rayVector);

    double tmin;
    std::shared_ptr<Scene<RgbSpectrum> > scene(new Scene<RgbSpectrum>);
    ShadingData<RgbSpectrum> shadingData(scene);
    sphere.hit(ray, tmin, shadingData);    
    EXPECT_EQ(Point2D(90.0, 225.0), shadingData.localHitPoint2D_); 
}



/*! \brief Unit of sphere calculateLocalHitPoint() function.
 */
TEST(SphereTest, LocalHitPointTest4)
{
    Point3D spherePoint(0.0, 0.0, 0.0);
    double sphereRadius = 1.0;
    Sphere<RgbSpectrum> sphere(spherePoint, sphereRadius);

    Point3D rayPoint(2.0, -2.0, 0.0);
    Vector3D rayVector(-1.0, 1.0, 0.0);
    Ray ray(rayPoint, rayVector);

    double tmin;
    std::shared_ptr<Scene<RgbSpectrum> > scene(new Scene<RgbSpectrum>);
    ShadingData<RgbSpectrum> shadingData(scene);
    sphere.hit(ray, tmin, shadingData);    
    EXPECT_EQ(Point2D(90.0, 315.0), shadingData.localHitPoint2D_); 
}



/*! \brief Unit of sphere calculateLocalHitPoint() function.
 */
TEST(SphereTest, LocalHitPointTest5)
{
    Point3D spherePoint(0.0, 0.0, 0.0);
    double sphereRadius = 1.0;
    Sphere<RgbSpectrum> sphere(spherePoint, sphereRadius);

    Point3D rayPoint(2.0, 0.0, 2.0);
    Vector3D rayVector(-1.0, 0.0, -1.0);
    Ray ray(rayPoint, rayVector);

    double tmin;
    std::shared_ptr<Scene<RgbSpectrum> > scene(new Scene<RgbSpectrum>);
    ShadingData<RgbSpectrum> shadingData(scene);
    sphere.hit(ray, tmin, shadingData);    
    EXPECT_EQ(Point2D(45.0, 0.0), shadingData.localHitPoint2D_); 
}



/*! \brief Unit of sphere calculateLocalHitPoint() function.
 */
TEST(SphereTest, LocalHitPointTest6)
{
    Point3D spherePoint(0.0, 0.0, 0.0);
    double sphereRadius = 1.0;
    Sphere<RgbSpectrum> sphere(spherePoint, sphereRadius);

    Point3D rayPoint(2.0, 0.0, -2.0);
    Vector3D rayVector(-1.0, 0.0, 1.0);
    Ray ray(rayPoint, rayVector);

    double tmin;
    std::shared_ptr<Scene<RgbSpectrum> > scene(new Scene<RgbSpectrum>);
    ShadingData<RgbSpectrum> shadingData(scene);
    sphere.hit(ray, tmin, shadingData);    
    EXPECT_EQ(Point2D(135.0, 0.0), shadingData.localHitPoint2D_); 
}