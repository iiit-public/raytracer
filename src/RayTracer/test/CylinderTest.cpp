// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file CylinderTest.cpp
*  \author Thomas Nuernberg
*  \date 21.04.2017
*  \brief Unit test of class OpenCylinder.
*/

#include "gtest/gtest.h"

#include "Scene.hpp"
#include "Objects/GeometricObject.hpp"
#include "Objects/OpenCylinder.hpp"
#include "Spectrum/Monochromatic.hpp"
#include "Materials/Material.hpp"
#include "Materials/Matte.hpp"
#include "Utilities/Ray.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"

using namespace iiit;



/*! \brief Unit test of OpenCylinder constructors.
*/
TEST(CylinderTest, ContstuctorTest)
{
    double tmin;
    std::shared_ptr<Scene<Monochromatic> > scene(new Scene<Monochromatic>);
    ShadingData<Monochromatic> shadingData(scene);

    // default constructor
    OpenCylinder<Monochromatic> cylinder1;
    Point3D origin(-2.0, 0.1, 0.5);
    Vector3D direction(1.0, 0.0, 0.0);
    Ray ray(origin, direction);
    EXPECT_FALSE(cylinder1.hit(ray, tmin, shadingData));
    EXPECT_FALSE(cylinder1.shadowHit(ray, tmin));

    OpenCylinder<Monochromatic> cylinder2(Point3D(0.0, 0.0, 0.0), 1.0, 0.0, 1.0);
    origin.set(-2.0, 0.0, 0.5);
    ray.set(origin, direction);
    EXPECT_TRUE(cylinder2.hit(ray, tmin, shadingData));
    EXPECT_TRUE(cylinder2.shadowHit(ray, tmin));
    EXPECT_DOUBLE_EQ(1.0, tmin);
    EXPECT_EQ(Normal(-1.0, 0.0, 0.0), shadingData.normal_);
    EXPECT_EQ(Point3D(-1.0, 0.0, 0.5), shadingData.hitPoint_);

    OpenCylinder<Monochromatic> cylinder3(Point3D(0.0, 0.0, 0.0), 1.0, 0.0, 1.0,
        std::shared_ptr<Material<Monochromatic> >(new Matte<Monochromatic>(Monochromatic(1.f))));
    EXPECT_TRUE(cylinder3.hit(ray, tmin, shadingData));
    EXPECT_TRUE(cylinder3.shadowHit(ray, tmin));

    OpenCylinder<Monochromatic> cylinder4(cylinder3);
    EXPECT_TRUE(cylinder4.hit(ray, tmin, shadingData));
    EXPECT_TRUE(cylinder4.shadowHit(ray, tmin));

    OpenCylinder<Monochromatic> cylinder5(*(cylinder3.clone()));
    EXPECT_TRUE(cylinder5.hit(ray, tmin, shadingData));
    EXPECT_TRUE(cylinder5.shadowHit(ray, tmin));
}



/*! \brief Unit test of OpenCylinder get() and set() methods.
*/
TEST(CylinderTest, GetterSetterTest)
{
    OpenCylinder<Monochromatic> cylinder;
    EXPECT_EQ(GeometricObject<Monochromatic>::Type::OpenCylinder, cylinder.getType());

    Point3D point(1.0, 1.0, 1.0);
    cylinder.setCenter(point);
    EXPECT_EQ(point, cylinder.getCenter());

    cylinder.setCenter(point.x_, point.y_, point.z_);
    EXPECT_EQ(point, cylinder.getCenter());

    cylinder.setRadius(1.0);
    EXPECT_EQ(1.0, cylinder.getRadius());

    cylinder.setZMin(0.0);
    EXPECT_EQ(0.0, cylinder.getZMin());

    cylinder.setZMax(1.0);
    EXPECT_EQ(1.0, cylinder.getZMax());
}



/*! \brief Unit test of OpenCylinder hit() function.
*/
TEST(CylinderTest, HitTest)
{
    double tmin;
    std::shared_ptr<Scene<Monochromatic> > scene(new Scene<Monochromatic>);
    ShadingData<Monochromatic> shadingData(scene);

    OpenCylinder<Monochromatic> cylinder(Point3D(0.0, 0.0, 0.0), 1.0, 0.0, 1.0);

    // ray passes through OpenCylinder
    Point3D origin(-2.0, 0.0, 0.5);
    Vector3D direction(1.0, 0.0, 0.0);
    Ray ray(origin, direction);
    EXPECT_TRUE(cylinder.hit(ray, tmin, shadingData));
    EXPECT_TRUE(cylinder.shadowHit(ray, tmin));
    EXPECT_DOUBLE_EQ(1.0, tmin);
    EXPECT_EQ(Normal(-1.0, 0.0, 0.0), shadingData.normal_);
    EXPECT_EQ(Point3D(-1.0, 0.0, 0.5), shadingData.hitPoint_);

    // ray starting inside OpenCylinder
    origin.set(0.0, 0.0, 0.5);
    ray.set(origin, direction);
    EXPECT_TRUE(cylinder.hit(ray, tmin, shadingData));
    EXPECT_TRUE(cylinder.shadowHit(ray, tmin));
    EXPECT_DOUBLE_EQ(1.0, tmin);
    EXPECT_EQ(Normal(-1.0, 0.0, 0.0), shadingData.normal_);
    EXPECT_EQ(Point3D(1.0, 0.0, 0.5), shadingData.hitPoint_);

    // ray starts on face of OpenCylinder
    origin.set(1.0, 0.0, 0.5);
    ray.set(origin, direction);
    EXPECT_FALSE(cylinder.hit(ray, tmin, shadingData));
    EXPECT_FALSE(cylinder.shadowHit(ray, tmin));

    // ray misses OpenCylinder
    origin.set(-2.0, 1.5, 0.5);
    ray.set(origin, direction);
    EXPECT_FALSE(cylinder.hit(ray, tmin, shadingData));
    EXPECT_FALSE(cylinder.shadowHit(ray, tmin));

    origin.set(0.0, 0.0, -0.5);
    direction.set(0.0, 0.0, 1.0);
    ray.set(origin, direction);
    EXPECT_FALSE(cylinder.hit(ray, tmin, shadingData));
    EXPECT_FALSE(cylinder.shadowHit(ray, tmin));

    // ray tangential to OpenCylinder
    origin.set(1.0, 0.0, -0.5);
    ray.set(origin, direction);
    cylinder.hit(ray, tmin, shadingData); // actual result irrelevant
    cylinder.shadowHit(ray, tmin); // actual result irrelevant
}