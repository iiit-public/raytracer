// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file BoundingBoxTest.cpp
 *  \author Thomas Nuernberg
 *  \date 14.05.2015
 *  \brief Unit test of class BoundingBox.
 */

#include "gtest/gtest.h"

#include "Objects/BoundingBox.hpp"
#include "Utilities/Ray.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"

using namespace iiit;



/*! \brief Unit test of bounding box default constructor.
 */
TEST(BoundingBoxTest, DefaultContstuctorTest)
{
    BoundingBox box;
    Point3D origin(0.0, 0.0, 0.0);
    Vector3D direction(1.0, 1.0, 0.0);
    direction.normalize();
    Ray ray(origin, direction);
    EXPECT_TRUE(box.hit(ray));

    origin.set(-1e6, 0.0, 0.5);
    ray.set(origin, direction);
    EXPECT_TRUE(box.hit(ray));

    origin.set(1e6, 0.0, 0.5);
    ray.set(origin, direction);
    EXPECT_TRUE(box.hit(ray));
}



/*! \brief Unit test of bounding box hit() function with sloped ray.
 */
TEST(BoundingBoxTest, HitTest)
{
    Point3D p1(0.0, 0.0, 0.0);
    Point3D p2(1.0, 1.0, 1.0);
    BoundingBox box(p1, p2);

    // ray passes through bounding box (positive t values)
    Point3D origin(-0.5, 0.5, 0.5);
    Vector3D direction(1.0, 0.25, 0.0);
    direction.normalize();
    Ray ray(origin, direction);
    EXPECT_TRUE(box.hit(ray));

    // ray starts on face of bounding box (facing inwards)
    origin.set(0.0, 0.5, 0.5);
    ray.set(origin, direction);
    EXPECT_TRUE(box.hit(ray));

    // ray starts inside box
    origin.set(0.5, 0.5, 0.5);
    ray.set(origin, direction);
    EXPECT_TRUE(box.hit(ray));

    // ray starts on face of bounding box (facing outwards), --> t < EPS
    origin.set(1.0, 0.5, 0.5);
    ray.set(origin, direction);
    EXPECT_FALSE(box.hit(ray));

    // ray passes through bounding box (negative t values)
    origin.set(1.5, 0.5, 0.5);
    ray.set(origin, direction);
    EXPECT_FALSE(box.hit(ray));
}



/*! \brief Unit test of bounding box hit() function with axis-aligned ray.
 */
TEST(BoundingBoxTest, AxisAlignedHitTest)
{
    Point3D p1(0.0, 0.0, 0.0);
    Point3D p2(1.0, 1.0, 1.0);
    BoundingBox box(p1, p2);

    // ray passes through bounding box (positive t values)
    Point3D origin(-0.5, 0.5, 0.5);
    Vector3D direction(1.0, 0.0, 0.0);
    Ray ray(origin, direction);
    EXPECT_TRUE(box.hit(ray));

    // ray starts on face of bounding box (facing inwards)
    origin.set(0.0, 0.5, 0.5);
    ray.set(origin, direction);
    EXPECT_TRUE(box.hit(ray));

    // ray starts inside ; box
    origin.set(0.5, 0.5, 0.5);
    ray.set(origin, direction);
    EXPECT_TRUE(box.hit(ray));

    // ray starts on face of bounding box (facing outwards), --> t < EPS
    origin.set(1.0, 0.5, 0.5);
    ray.set(origin, direction);
    EXPECT_FALSE(box.hit(ray));

    // ray passes through bounding box (negative t values)
    origin.set(1.5, 0.5, 0.5);
    ray.set(origin, direction);
    EXPECT_FALSE(box.hit(ray));
}



/*! \brief Unit test of bounding box hit() function with missing ray.
 */
TEST(BoundingBoxTest, MissTest)
{
    Point3D p1(0.0, 0.0, 0.0);
    Point3D p2(1.0, 1.0, 1.0);
    BoundingBox box(p1, p2);
    Ray ray(Point3D(-0.5, 0.5, 1.5), Vector3D(1.0, 0.0, 0.0));
    EXPECT_FALSE(box.hit(ray));

    // ray hits, but with negative t value
    ray.set(Point3D(1.5, 0.5, 0.5), Vector3D(1.0, 0.0, 0.0));
    EXPECT_FALSE(box.hit(ray));

    ray.set(Point3D(1.5, 0.5, -0.5), Vector3D(1.0, 0.0, 0.0));
    EXPECT_FALSE(box.hit(ray));

    Vector3D direction(0.25, 0.0, 1.0);
    direction.normalize();
    ray.set(Point3D(-1.0, 0.0, 0.0), direction);
    EXPECT_FALSE(box.hit(ray));
}



/*! \brief Unit test of bounding box hit() function with tangential ray.
 */
TEST(BoundingBoxTest, EdgeTest)
{
    Point3D p1(0.0, 0.0, 0.0);
    Point3D p2(1.0, 1.0, 1.0);
    BoundingBox box(p1, p2);
    Ray ray(Point3D(-0.5, 0.5, 0.0), Vector3D(1.0, 0.0, 0.0));
    EXPECT_TRUE(box.hit(ray));

    // ray starts on edge of bounding box (facing outwards), --> t < EPS
    ray.set(Point3D(1.0, 0.5, 0.0), Vector3D(1.0, 0.0, 0.0));
    EXPECT_FALSE(box.hit(ray));

    // ray hits vertex, --> t0 == t1
    Vector3D direction(1.0, 1.0, 1.0);
    direction.normalize();
    ray.set(Point3D(-1.0, 0.0, 0.0), direction);
    EXPECT_FALSE(box.hit(ray));
}