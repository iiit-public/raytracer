// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file AbstractGridTest.cpp
 *  \author Maximilian Schambach
 *  \date 26.02.2019
 *  \brief Unit test of class AbstractGrid.
 */

#include "gtest/gtest.h"

#include <iostream>
#include <fstream>
#include <string>

#include "Utilities/AbstractGrid.hpp"
#include "Utilities/Point3D.hpp"
#include "Spectrum/RgbSpectrum.hpp"

using namespace iiit;


/*! \brief Unit test of AbstractGrid comparison.
 */
TEST(AbstractGridTest, AbstractGridComparison)
{
    double x_max = 1.1;
    double y_max = 0.17777;
    double offset_x = 0.3;
    double offset_y = 0.66;
    double a_x = 1.1;
    double a_y = 1.2;
    double b_x = 2.5;
    double b_y = 3.33;
    double phi = 0.12;
    double theta = 0.01;
    double z = -0.1;

    AbstractGrid<RgbSpectrum> grid1(x_max, y_max, offset_x, offset_y, a_x, a_y, b_x, b_y, phi, theta, z, false);
    EXPECT_EQ(grid1, grid1);

    AbstractGrid<RgbSpectrum> grid2(1.5f, y_max, offset_x, offset_y, a_x, a_y, b_x, b_y, phi, theta, z, false);
    EXPECT_NE(grid1, grid2);
}


/*! \brief Unit test of different constructors.
 */
TEST(AbstractGridTest, AbstractGridConstructor)
{
    AbstractGrid<RgbSpectrum> grid;

    double x_max = 0.1;
    double y_max = 0.17;
    double offset_x = 0.5;
    double offset_y = 0.55;
    double a_x = 1.1;
    double a_y = 1.2;
    double b_x = 2.5;
    double b_y = 3.33;
    double phi = 0.12;
    double theta = 0.01;
    double z = -0.1;

    AbstractGrid<RgbSpectrum> grid1(x_max, y_max, offset_x, offset_y, a_x, a_y, b_x, b_y, phi, theta, z, false);

    EXPECT_EQ(grid1.get_x_max(), x_max);
    EXPECT_EQ(grid1.get_y_max(), y_max);
    EXPECT_EQ(grid1.get_offset_x(), offset_x);
    EXPECT_EQ(grid1.get_offset_y(), offset_y);
    EXPECT_EQ(grid1.get_a_x(), a_x);
    EXPECT_EQ(grid1.get_a_y(), a_y);
    EXPECT_EQ(grid1.get_b_x(), b_x);
    EXPECT_EQ(grid1.get_b_y(), b_y);
    EXPECT_EQ(grid1.get_phi(), phi);
    EXPECT_EQ(grid1.get_theta(), theta);
    EXPECT_EQ(grid1.get_z(), z);

    // copy constructor
    AbstractGrid<RgbSpectrum> grid2(grid1);
    EXPECT_EQ(grid1, grid2);
}


/*! \brief Unit test for adding points
 */
TEST(AbstractGridTest, AbstractGridAddPoint)
{
    double x_max = 1.1;
    double y_max = 0.17777;
    double offset_x = 0.3;
    double offset_y = 0.66;
    double a_x = 1.1;
    double a_y = 1.2;
    double b_x = 2.5;
    double b_y = 3.33;
    double phi = 0.12;
    double theta = 0.01;
    double z = -0.1;

    AbstractGrid<RgbSpectrum> grid1(x_max, y_max, offset_x, offset_y, a_x, a_y, b_x, b_y, phi, theta, z, false);
    int n_points = grid1.get_num_points();

    Point3D p1(0.5, 1.1, -2.0);
    Point3D p2(4.3, 2.1, 0.11);
    Point3D p3(-1., 3.1, 2.22);

    grid1.add_point(p1);
    EXPECT_EQ(grid1.get_num_points(), n_points + 1);
    EXPECT_EQ(grid1.get_point(n_points), p1);

    grid1.add_point(p2);
    EXPECT_EQ(grid1.get_num_points(), n_points + 2);
    EXPECT_EQ(grid1.get_point(n_points), p1);
    EXPECT_EQ(grid1.get_point(n_points + 1), p2);

    grid1.add_point(p3);
    EXPECT_EQ(grid1.get_num_points(), n_points + 3);
    EXPECT_EQ(grid1.get_point(n_points), p1);
    EXPECT_EQ(grid1.get_point(n_points + 1), p2);
    EXPECT_EQ(grid1.get_point(n_points + 2), p3);

    // Test get method safeness
    EXPECT_NO_THROW(grid1.get_point(n_points + 4));
    EXPECT_THROW(grid1.get_point_safe(n_points + 4), std::out_of_range);
}

/*! \brief Unit test of point calculation
 */
TEST(AbstractGridTest, AbstractGridCalcPoints)
{
    double x_max = 3.1;
    double y_max = 2.1;
    double offset_x = 0.0;
    double offset_y = 0.0;
    double a_x = 1.0;
    double a_y = 0.0;
    double b_x = 0.0;
    double b_y = 0.5;
    double phi = 0.0;
    double theta = 0.0;
    double z = -0.1;
    AbstractGrid<RgbSpectrum> grid1(x_max, y_max, offset_x, offset_y, a_x, a_y, b_x, b_y, phi, theta, z, false);
    EXPECT_EQ(grid1.get_num_points(), 63);

    x_max = 3.1;
    y_max = 2.1;
    offset_x = 0.0;
    offset_y = 0.0;
    a_x = 1.0;
    a_y = 0.0;
    b_x = 0.0;
    b_y = 0.5;
    phi = 0.0;
    theta = 0.0;
    z = -0.1;
    AbstractGrid<RgbSpectrum> grid2(x_max, y_max, offset_x, offset_y, a_x, a_y, b_x, b_y, phi, theta, z, false);
    EXPECT_EQ(grid2.get_num_points(), 63);

    x_max = 3.1;
    y_max = 2.1;
    offset_x = 0.5;
    offset_y = 0.5;
    a_x = 0.5;
    a_y = 0.5;
    b_x = 0.5;
    b_y = -0.5;
    phi = 0.0;
    theta = 0.0;
    z = -0.1;
    AbstractGrid<RgbSpectrum> grid3(x_max, y_max, offset_x, offset_y, a_x, a_y, b_x, b_y, phi, theta, z, false);
    EXPECT_EQ(grid3.get_num_points(), 5*7+4*6);
    grid3.save("test_hex_grid.csv");

}


/*! \brief Unit test of saving
 */
TEST(AbstractGridTest, AbstractGridSave)
{
    double x_max = 0.0;
    double y_max = 0.0;
    double offset_x = 0.3;
    double offset_y = 0.66;
    double a_x = 1.1;
    double a_y = 1.2;
    double b_x = 2.5;
    double b_y = 3.33;
    double phi = 0.12;
    double theta = 0.01;
    double z = -0.1;

    AbstractGrid<RgbSpectrum> grid1(x_max, y_max, offset_x, offset_y, a_x, a_y, b_x, b_y, phi, theta, z, false);

    // add test points
    Point3D p1(0.5, 1.1, -2.0);
    Point3D p2(0.1234567890123456789, 2555.1, 0.101);
    Point3D p3(-1.0, 3.1, 0.1234567890123456789);
    grid1.add_point(p1);
    grid1.add_point(p2);
    grid1.add_point(p3);

    // create dummy file
    std::string dummyFileName = "GridSaveTest.csv";

    // save grid
    grid1.save(dummyFileName);

    // read from csv
    std::ifstream infile(dummyFileName);

    std::string str;

    // get first line
    std::getline(infile, str);
    std::string exp = "# x; y; z";
    EXPECT_EQ(exp, str);

    // read data point lines
    std::getline(infile, str);
    exp = "0.5; 1.1; -2";
    EXPECT_EQ(exp, str);

    std::getline(infile, str);
    exp = "0.1234567890123457; 2555.1; 0.101";
    EXPECT_EQ(exp, str);

    std::getline(infile, str);
    exp = "-1; 3.1; 0.1234567890123457";
    EXPECT_EQ(exp, str);

    remove(dummyFileName.c_str());
}

/*! \brief Unit test for adding points
 */
TEST(AbstractGridTest, AbstractGridColor)
{
    double x_max = 1.1;
    double y_max = 0.17777;
    double offset_x = 0.3;
    double offset_y = 0.66;
    double a_x = 1.1;
    double a_y = 1.2;
    double b_x = 2.5;
    double b_y = 3.33;
    double phi = 0.12;
    double theta = 0.01;
    double z = -0.1;

    // For non-coded grid, all colors are constant 1.0
    bool is_coded = false;
    AbstractGrid<RgbSpectrum> grid1(x_max, y_max, offset_x, offset_y, a_x, a_y, b_x, b_y, phi, theta, z, is_coded);
    int n_points = grid1.get_num_points();

    RgbSpectrum color(1.0);
    for (unsigned int i = 0; i < n_points; i++) {
        EXPECT_EQ(grid1.get_color(i), color);
    }


    is_coded = true;
    AbstractGrid<RgbSpectrum> grid2(x_max, y_max, offset_x, offset_y, a_x, a_y, b_x, b_y, phi, theta, z, is_coded);
    n_points = grid2.get_num_points();


    for (unsigned int i = 0; i < n_points; i++) {
        int sum = 0;
        for (unsigned int j = 0; j < color.getNumOfCoeffs(); j++) {
            sum += grid2.get_color(i).getCoeff(j);
        }
        // Expect only one non zero coefficient
        EXPECT_EQ(sum, 1.0);
    }

}
