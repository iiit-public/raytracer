// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file ViewPlaneTest.cpp
 *  \author Thomas Nuernberg
 *  \date 27.03.2015
 *  \brief Unit test of class ViewPlane.
 */

#include "gtest/gtest.h"

#include "Cameras/ViewPlane.hpp"

using namespace iiit;



/*! \brief Unit test of pixel indices of a single viewplane
 */
TEST(ViewPlaneTest, SinglePlaneAbsRel)
{
    ViewPlane viewPlane;
    viewPlane.uRes_ = 4;
    viewPlane.vRes_ = 4;

    EXPECT_EQ(0, viewPlane.uAbs(0));
    EXPECT_EQ(0, viewPlane.vAbs(0));
    EXPECT_EQ(3, viewPlane.uAbs(3));
    EXPECT_EQ(3, viewPlane.vAbs(3));

    EXPECT_EQ(0, viewPlane.uRel(0));
    EXPECT_EQ(0, viewPlane.vRel(0));
    EXPECT_EQ(3, viewPlane.uRel(3));
    EXPECT_EQ(3, viewPlane.vRel(3));

    EXPECT_EQ(0, viewPlane.uAbsBegin());
    EXPECT_EQ(0, viewPlane.vAbsBegin());
    EXPECT_EQ(4, viewPlane.uAbsEnd());
    EXPECT_EQ(4, viewPlane.vAbsEnd());

    EXPECT_EQ(0, viewPlane.uRelBegin());
    EXPECT_EQ(0, viewPlane.vRelBegin());
    EXPECT_EQ(4, viewPlane.uRelEnd());
    EXPECT_EQ(4, viewPlane.vRelEnd());
}



/*! \brief Unit test of pixel indices of a viewplane chunk
 */
TEST(ViewPlaneTest, MultiPlaneAbsRel)
{
    ViewPlane viewPlane;
    viewPlane.uRes_ = 4;
    viewPlane.uOffset_ = 4;
    viewPlane.vRes_ = 4;
    viewPlane.vOffset_ = 4;

    EXPECT_EQ(4, viewPlane.uAbs(0));
    EXPECT_EQ(4, viewPlane.vAbs(0));
    EXPECT_EQ(7, viewPlane.uAbs(3));
    EXPECT_EQ(7, viewPlane.vAbs(3));

    EXPECT_EQ(0, viewPlane.uRel(4));
    EXPECT_EQ(0, viewPlane.vRel(4));
    EXPECT_EQ(3, viewPlane.uRel(7));
    EXPECT_EQ(3, viewPlane.vRel(7));

    EXPECT_EQ(4, viewPlane.uAbsBegin());
    EXPECT_EQ(4, viewPlane.vAbsBegin());
    EXPECT_EQ(8, viewPlane.uAbsEnd());
    EXPECT_EQ(8, viewPlane.vAbsEnd());

    EXPECT_EQ(0, viewPlane.uRelBegin());
    EXPECT_EQ(0, viewPlane.vRelBegin());
    EXPECT_EQ(4, viewPlane.uRelEnd());
    EXPECT_EQ(4, viewPlane.vRelEnd());
}



/*! \brief Unit test of a single viewplane coordinate transformation.
 */
TEST(ViewPlaneTest, SinglePlaneCoordinateTest)
{
    ViewPlane viewPlane;
    viewPlane.uRes_ = 4;
    viewPlane.uOffset_ = 0;
    viewPlane.uAbsRes_ = 4;
    viewPlane.vRes_ = 4;
    viewPlane.vOffset_ = 0;
    viewPlane.vAbsRes_ = 4;
    viewPlane.s_ = 1.0;

    EXPECT_EQ(-1.5, viewPlane.uAbsToXc(0));
    EXPECT_EQ(-1.5, viewPlane.uRelToXc(0));
    EXPECT_EQ(0.5, viewPlane.vAbsToYc(2));
    EXPECT_EQ(0.5, viewPlane.vRelToYc(2));

    viewPlane.uRes_ = 5;
    viewPlane.uOffset_ = 0;
    viewPlane.uAbsRes_ = 5;
    viewPlane.vRes_ = 5;
    viewPlane.vOffset_ = 0;
    viewPlane.vAbsRes_ = 5;

    EXPECT_EQ(-2.0, viewPlane.uAbsToXc(0));
    EXPECT_EQ(-2.0, viewPlane.uRelToXc(0));
    EXPECT_EQ(0.0, viewPlane.vAbsToYc(2));
    EXPECT_EQ(0.0, viewPlane.vRelToYc(2));
}



/*! \brief Unit test of a viewplane chunk coordinate transformation.
 */
TEST(ViewPlaneTest, MultiPlaneCoordinateTest)
{
    ViewPlane viewPlane;
    viewPlane.uRes_ = 2;
    viewPlane.uOffset_ = 2;
    viewPlane.uAbsRes_ = 4;
    viewPlane.vRes_ = 2;
    viewPlane.vOffset_ = 2;
    viewPlane.vAbsRes_ = 4;
    viewPlane.s_ = 1.0;

    EXPECT_EQ(-1.5, viewPlane.uAbsToXc(0));
    EXPECT_EQ(0.5, viewPlane.uRelToXc(0));
    EXPECT_EQ(0.5, viewPlane.vAbsToYc(2));
    EXPECT_EQ(1.5, viewPlane.vRelToYc(1));

    viewPlane.uRes_ = 2;
    viewPlane.uOffset_ = 2;
    viewPlane.uAbsRes_ = 5;
    viewPlane.vRes_ = 2;
    viewPlane.vOffset_ = 2;
    viewPlane.vAbsRes_ = 5;

    EXPECT_EQ(-2.0, viewPlane.uAbsToXc(0));
    EXPECT_EQ(0.0, viewPlane.uRelToXc(0));
    EXPECT_EQ(0.0, viewPlane.vAbsToYc(2));
    EXPECT_EQ(1.0, viewPlane.vRelToYc(1));
}