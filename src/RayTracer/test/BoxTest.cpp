// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file BoxTest.cpp
 *  \author Thomas Nuernberg
 *  \date 21.04.2017
 *  \brief Unit test of class Box.
 */

#include "gtest/gtest.h"

#include "Scene.hpp"
#include "Objects/GeometricObject.hpp"
#include "Objects/Box.hpp"
#include "Spectrum/Monochromatic.hpp"
#include "Materials/Material.hpp"
#include "Materials/Matte.hpp"
#include "Utilities/Ray.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"

using namespace iiit;



/*! \brief Unit test of Box constructors.
 */
TEST(BoxTest, ContstuctorTest)
{
    double tmin;
    std::shared_ptr<Scene<Monochromatic> > scene(new Scene<Monochromatic>);
    ShadingData<Monochromatic> shadingData(scene);

    Box<Monochromatic> box1;
    Point3D origin(0.5, 0.5, 0.0);
    Vector3D direction(0.0, 0.0, 1.0);
    Ray ray(origin, direction);
    EXPECT_TRUE(box1.hit(ray, tmin, shadingData));
    EXPECT_TRUE(box1.shadowHit(ray, tmin));

    Box<Monochromatic> box2(Point3D(0.0, 0.0, 0.0), Point3D(1.0, 1.0, 1.0));
    EXPECT_TRUE(box2.hit(ray, tmin, shadingData));
    EXPECT_TRUE(box2.shadowHit(ray, tmin));

    Box<Monochromatic> box3(Point3D(0.0, 0.0, 0.0), Point3D(1.0, 1.0, 1.0),
        std::shared_ptr<Material<Monochromatic> >(new Matte<Monochromatic>(Monochromatic(1.f))));
    EXPECT_TRUE(box3.hit(ray, tmin, shadingData));
    EXPECT_TRUE(box3.shadowHit(ray, tmin));

    Box<Monochromatic> box4(box3);
    EXPECT_TRUE(box4.hit(ray, tmin, shadingData));
    EXPECT_TRUE(box4.shadowHit(ray, tmin));

    Box<Monochromatic> box5(*(box3.clone()));
    EXPECT_TRUE(box5.hit(ray, tmin, shadingData));
    EXPECT_TRUE(box5.shadowHit(ray, tmin));
}



/*! \brief Unit test of Box get() and set() methods.
*/
TEST(BoxTest, GetterSetterTest)
{
    Box<Monochromatic> box;
    EXPECT_EQ(GeometricObject<Monochromatic>::Type::Box, box.getType());

    Point3D vertex0(5.0, 3.2, 94.2);
    Point3D vertex1(46.63, 3523.22, 486.8);
    box.setVertex0(vertex0);
    box.setVertex1(vertex1);

    EXPECT_EQ(vertex0, box.getVertex0());
    EXPECT_EQ(vertex1, box.getVertex1());
}



/*! \brief Unit test of Box hit() function.
 */
TEST(BoxTest, HitTest)
{
    double tmin;
    std::shared_ptr<Scene<Monochromatic> > scene(new Scene<Monochromatic>);
    ShadingData<Monochromatic> shadingData(scene);

    Point3D p1(0.0, 0.0, 0.0);
    Point3D p2(1.0, 1.0, 1.0);
    Box<Monochromatic> box(p1, p2);

    // ray passes through Box (positive t values)
    Point3D origin(-0.5, 0.5, 0.5);
    Vector3D direction(1.0, 0.25, 0.0);
    direction.normalize();
    Ray ray(origin, direction);
    EXPECT_TRUE(box.hit(ray, tmin, shadingData));
    EXPECT_TRUE(box.shadowHit(ray, tmin));

    // ray starts on face of Box (facing inwards)
    origin.set(0.0, 0.5, 0.5);
    ray.set(origin, direction);
    EXPECT_TRUE(box.hit(ray, tmin, shadingData));
    EXPECT_TRUE(box.shadowHit(ray, tmin));

    // ray starts inside Box
    origin.set(0.5, 0.5, 0.5);
    ray.set(origin, direction);
    EXPECT_TRUE(box.hit(ray, tmin, shadingData));
    EXPECT_TRUE(box.shadowHit(ray, tmin));

    // ray starts on face of Box (facing outwards), --> t < EPS
    origin.set(1.0, 0.5, 0.5);
    ray.set(origin, direction);
    EXPECT_FALSE(box.hit(ray, tmin, shadingData));
    EXPECT_FALSE(box.shadowHit(ray, tmin));

    // ray passes through Box (negative t values)
    origin.set(1.5, 0.5, 0.5);
    ray.set(origin, direction);
    EXPECT_FALSE(box.hit(ray, tmin, shadingData));
    EXPECT_FALSE(box.shadowHit(ray, tmin));
}



/*! \brief Unit test of Box hit() function with axis-aligned ray.
 */
TEST(BoxTest, AxisAlignedHitTest)
{
    double tmin;
    std::shared_ptr<Scene<Monochromatic> > scene(new Scene<Monochromatic>);
    ShadingData<Monochromatic> shadingData(scene);

    Point3D p1(0.0, 0.0, 0.0);
    Point3D p2(1.0, 1.0, 1.0);
    Box<Monochromatic> box(p1, p2);

    // ray passes through Box (positive t values)
    Point3D origin(-0.5, 0.5, 0.5);
    Vector3D direction(1.0, 0.0, 0.0);
    Ray ray(origin, direction);
    EXPECT_TRUE(box.hit(ray, tmin, shadingData));
    EXPECT_TRUE(box.shadowHit(ray, tmin));

    // ray starts on face of Box (facing inwards)
    origin.set(0.0, 0.5, 0.5);
    ray.set(origin, direction);
    EXPECT_TRUE(box.hit(ray, tmin, shadingData));
    EXPECT_TRUE(box.shadowHit(ray, tmin));

    // ray starts inside Box
    origin.set(0.5, 0.5, 0.5);
    ray.set(origin, direction);
    EXPECT_TRUE(box.hit(ray, tmin, shadingData));
    EXPECT_TRUE(box.shadowHit(ray, tmin));

    // ray starts on face of Box (facing outwards), --> t < EPS
    origin.set(1.0, 0.5, 0.5);
    ray.set(origin, direction);
    EXPECT_FALSE(box.hit(ray, tmin, shadingData));
    EXPECT_FALSE(box.shadowHit(ray, tmin));

    // ray passes through Box (negative t values)
    origin.set(1.5, 0.5, 0.5);
    ray.set(origin, direction);
    EXPECT_FALSE(box.hit(ray, tmin, shadingData));
    EXPECT_FALSE(box.shadowHit(ray, tmin));
}



/*! \brief Unit test of Box hit() function with missing ray.
 */
TEST(BoxTest, MissTest)
{
    double tmin;
    std::shared_ptr<Scene<Monochromatic> > scene(new Scene<Monochromatic>);
    ShadingData<Monochromatic> shadingData(scene);

    Point3D p1(0.0, 0.0, 0.0);
    Point3D p2(1.0, 1.0, 1.0);
    Box<Monochromatic> box(p1, p2);
    Ray ray(Point3D(-0.5, 0.5, 1.5), Vector3D(1.0, 0.0, 0.0));
    EXPECT_FALSE(box.hit(ray, tmin, shadingData));
    EXPECT_FALSE(box.shadowHit(ray, tmin));

    // ray hits, but with negative t value
    ray.set(Point3D(1.5, 0.5, 0.5), Vector3D(1.0, 0.0, 0.0));
    EXPECT_FALSE(box.hit(ray, tmin, shadingData));
    EXPECT_FALSE(box.shadowHit(ray, tmin));

    ray.set(Point3D(1.5, 0.5, -0.5), Vector3D(1.0, 0.0, 0.0));
    EXPECT_FALSE(box.hit(ray, tmin, shadingData));
    EXPECT_FALSE(box.shadowHit(ray, tmin));

    Vector3D direction(0.25, 0.0, 1.0);
    direction.normalize();
    ray.set(Point3D(-1.0, 0.0, 0.0), direction);
    EXPECT_FALSE(box.hit(ray, tmin, shadingData));
    EXPECT_FALSE(box.shadowHit(ray, tmin));
}



/*! \brief Unit test of bounding Box hit() function with tangential ray.
 */
TEST(BoxTest, EdgeTest)
{
    double tmin;
    std::shared_ptr<Scene<Monochromatic> > scene(new Scene<Monochromatic>);
    ShadingData<Monochromatic> shadingData(scene);

    Point3D p1(0.0, 0.0, 0.0);
    Point3D p2(1.0, 1.0, 1.0);
    Box<Monochromatic> box(p1, p2);
    Ray ray(Point3D(-0.5, 0.5, 0.0), Vector3D(1.0, 0.0, 0.0));
    EXPECT_TRUE(box.hit(ray, tmin, shadingData));
    EXPECT_TRUE(box.shadowHit(ray, tmin));

    // ray starts on edge of bounding box (facing outwards), --> t < EPS
    ray.set(Point3D(1.0, 0.5, 0.0), Vector3D(1.0, 0.0, 0.0));
    EXPECT_FALSE(box.hit(ray, tmin, shadingData));
    EXPECT_FALSE(box.shadowHit(ray, tmin));

    // ray hits vertex, --> t0 == t1
    Vector3D direction(1.0, 1.0, 1.0);
    direction.normalize();
    ray.set(Point3D(-1.0, 0.0, 0.0), direction);
    EXPECT_FALSE(box.hit(ray, tmin, shadingData));
    EXPECT_FALSE(box.shadowHit(ray, tmin));
}