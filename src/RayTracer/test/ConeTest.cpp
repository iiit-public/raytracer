// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file ConeTest.cpp
*  \author Thomas Nuernberg
*  \date 21.04.2017
*  \brief Unit test of class OpenCone.
*/

#include "gtest/gtest.h"

#include "Scene.hpp"
#include "Objects/GeometricObject.hpp"
#include "Objects/OpenCone.hpp"
#include "Spectrum/Monochromatic.hpp"
#include "Materials/Material.hpp"
#include "Materials/Matte.hpp"
#include "Utilities/Ray.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"

using namespace iiit;



/*! \brief Unit test of OpenCone constructors.
*/
TEST(ConeTest, ContstuctorTest)
{
    double tmin;
    std::shared_ptr<Scene<Monochromatic> > scene(new Scene<Monochromatic>);
    ShadingData<Monochromatic> shadingData(scene);

    // default constructor
    OpenCone<Monochromatic> cone1;
    Point3D origin(-2.0, 0.1, 0.5);
    Vector3D direction(1.0, 0.0, 0.0);
    Ray ray(origin, direction);
    EXPECT_FALSE(cone1.hit(ray, tmin, shadingData));
    EXPECT_FALSE(cone1.shadowHit(ray, tmin));

    OpenCone<Monochromatic> cone2(Point3D(0.0, 0.0, 0.0), -1.0, 1.0, -1.0, 1.0);
    origin.set(-2.0, 0.0, 0.5);
    ray.set(origin, direction);
    EXPECT_TRUE(cone2.hit(ray, tmin, shadingData));
    EXPECT_TRUE(cone2.shadowHit(ray, tmin));
    EXPECT_DOUBLE_EQ(1.5, tmin);
    EXPECT_EQ(Point3D(-0.5, 0.0, 0.5), shadingData.hitPoint_);

    OpenCone<Monochromatic> cone3(Point3D(0.0, 0.0, 0.0), -1.0, 1.0, -1.0, 1.0,
        std::shared_ptr<Material<Monochromatic> >(new Matte<Monochromatic>(Monochromatic(1.f))));
    EXPECT_TRUE(cone3.hit(ray, tmin, shadingData));
    EXPECT_TRUE(cone3.shadowHit(ray, tmin));

    OpenCone<Monochromatic> cone4(cone3);
    EXPECT_TRUE(cone4.hit(ray, tmin, shadingData));
    EXPECT_TRUE(cone4.shadowHit(ray, tmin));

    OpenCone<Monochromatic> cone5(*(cone3.clone()));
    EXPECT_TRUE(cone5.hit(ray, tmin, shadingData));
    EXPECT_TRUE(cone5.shadowHit(ray, tmin));
}



/*! \brief Unit test of OpenCone get() and set() methods.
*/
TEST(ConeTest, GetterSetterTest)
{
    OpenCone<Monochromatic> cone;
    EXPECT_EQ(GeometricObject<Monochromatic>::Type::OpenCone, cone.getType());

    Point3D point(1.0, 1.0, 1.0);
    cone.setCenter(point);
    EXPECT_EQ(point, cone.getCenter());

    cone.setCenter(point.x_, point.y_, point.z_);
    EXPECT_EQ(point, cone.getCenter());

    cone.setMinRadius(-1.0);
    EXPECT_EQ(-1.0, cone.getMinRadius());

    cone.setMaxRadius(1.0);
    EXPECT_EQ(1.0, cone.getMaxRadius());

    cone.setZMin(0.0);
    EXPECT_EQ(0.0, cone.getZMin());

    cone.setZMax(1.0);
    EXPECT_EQ(1.0, cone.getZMax());
}



/*! \brief Unit test of OpenCone hit() function.
*/
TEST(ConeTest, HitTest)
{
    double tmin;
    std::shared_ptr<Scene<Monochromatic> > scene(new Scene<Monochromatic>);
    ShadingData<Monochromatic> shadingData(scene);

    OpenCone<Monochromatic> cone(Point3D(0.0, 0.0, 0.0), -1.0, 1.0, -1.0, 1.0);

    // ray passes through OpenCylinder
    Point3D origin(-2.0, 0.0, 0.5);
    Vector3D direction(1.0, 0.0, 0.0);
    Ray ray(origin, direction);
    EXPECT_TRUE(cone.hit(ray, tmin, shadingData));
    EXPECT_TRUE(cone.shadowHit(ray, tmin));
    EXPECT_DOUBLE_EQ(1.5, tmin);
    EXPECT_EQ(Point3D(-0.5, 0.0, 0.5), shadingData.hitPoint_);

    // ray starting inside OpenCylinder
    origin.set(0.0, 0.0, 0.5);
    ray.set(origin, direction);
    EXPECT_TRUE(cone.hit(ray, tmin, shadingData));
    EXPECT_TRUE(cone.shadowHit(ray, tmin));
    EXPECT_DOUBLE_EQ(0.5, tmin);
    EXPECT_EQ(Point3D(0.5, 0.0, 0.5), shadingData.hitPoint_);

    // ray starts on face of OpenCylinder
    origin.set(0.5, 0.0, 0.5);
    ray.set(origin, direction);
    EXPECT_FALSE(cone.hit(ray, tmin, shadingData));
    EXPECT_FALSE(cone.shadowHit(ray, tmin));

    // ray misses OpenCylinder
    origin.set(-2.0, 1.5, 0.5);
    ray.set(origin, direction);
    EXPECT_FALSE(cone.hit(ray, tmin, shadingData));
    EXPECT_FALSE(cone.shadowHit(ray, tmin));

    // ray tangential to OpenCylinder
    origin.set(-2.0, 0.0, -2.0);
    direction.set(1.0, 0.0, 1.0);
    direction.normalize();
    ray.set(origin, direction);
    cone.hit(ray, tmin, shadingData); // actual result irrelevant
    cone.shadowHit(ray, tmin); // actual result irrelevant

    // ray through Cone center
    origin.set(0.0, 0.0, -0.5);
    direction.set(0.0, 0.0, 1.0);
    ray.set(origin, direction);
    cone.hit(ray, tmin, shadingData); // actual result irrelevant
    cone.shadowHit(ray, tmin); // actual result irrelevant
}