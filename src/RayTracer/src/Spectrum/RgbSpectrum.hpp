// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file RgbSpectrum.hpp
 *  \author Thomas Nuernberg
 *  \date 14.12.2015
 *  \brief Definition of RgbSpectrum.hpp.
 *
 *  Definition of RgbSpectrum class, that represents a color by three components.
 */

#ifndef __RGBSPECTRUM_HPP__
#define __RGBSPECTRUM_HPP__

#include <vector>

#include "CoefficientSpectrum.hpp"
#include "SampledSpectrum.hpp"



namespace iiit
{
    
    /*! \brief Spectrum represented by color channels red, green and blue.
     */
    class RgbSpectrum : public CoefficientSpectrum<3>
    {
    public:
        RgbSpectrum(float value = 0.f);
        RgbSpectrum(const float* values);
        RgbSpectrum(const std::array<float, 3>& values);
        
        // factory methods
        static RgbSpectrum fromSampled(const std::vector<float>& lambdaSamples, const std::vector<float>& valueSamples);
        static RgbSpectrum fromRgb(const std::array<float, 3>& rgb, SpectrumRepresentationType type);
        static RgbSpectrum fromXyz(const std::array<float, 3>& xyz, SpectrumRepresentationType type = ReflectanceSpectrum);
        
        // conversion methods
        void toXyz(std::array<float, 3>& xyz) const;
        void toRgb(std::array<float, 3>& rgb) const;
        void toSampled(SampledSpectrum& color) const;

        int getWaveLengthNm(int index);
        
        // operators
        RgbSpectrum& operator=(const RgbSpectrum& rhs);
        RgbSpectrum& operator+=(const RgbSpectrum& rhs);
        RgbSpectrum operator+(const RgbSpectrum& rhs) const;
        RgbSpectrum operator-(void) const;
        RgbSpectrum& operator-=(const RgbSpectrum& rhs);
        RgbSpectrum operator-(const RgbSpectrum& rhs) const;
        RgbSpectrum& operator*=(float factor);
        RgbSpectrum operator*(float factor) const;
        RgbSpectrum& operator*=(const RgbSpectrum& rhs);
        RgbSpectrum operator*(const RgbSpectrum& rhs) const;
        RgbSpectrum& operator/=(float factor);
        RgbSpectrum operator/(float factor) const;
        RgbSpectrum& operator/=(const RgbSpectrum& rhs);
        RgbSpectrum operator/(const RgbSpectrum& rhs) const;
        RgbSpectrum powc(float p) const;
        
    private:
        static float interpolateSpectrumSamples(const std::vector<float>& lambdas, const std::vector<float>& values, float lambda);
        static const std::array<int, 3> rgbWaveLengthsNm;
        
    };

} // end of namespace iiit

#endif // __RGBSPECTRUM_HPP__
