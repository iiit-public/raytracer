// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file CoefficientSpectrum.hpp
 *  \author Thomas Nuernberg
 *  \date 11.12.2015
 *  \brief Definition of CoefficientSpectrum.
 *
 *  Definition of CoefficientSpectrum class, that defines the basic operation for any spectrum/color
 *  class represented by coefficients of base functions.
 */

#ifndef __COEFFICIENTSPECTRUM_HPP__
#define __COEFFICIENTSPECTRUM_HPP__

#include <algorithm>
#include <limits>
#include <math.h>
#include <vector>
#include <array>



namespace iiit
{
    
    /*! \brief Enumeration to denote the spectral type for rgb to spectrum conversion.
     */
    enum SpectrumRepresentationType
    {
        ReflectanceSpectrum, ///< Spectral representation for surface reflections.
        IlluminationSpectrum ///< Spectral representation for illuminations.
    };
    
    
    
    // spectral data declarations
    static const int rawNumSamples = 441; ///< Number of samples of CIE reference data.
    extern const std::array<float, rawNumSamples> rawX; ///< Reference X base function according to CIE standard observer.
    extern const std::array<float, rawNumSamples> rawY; ///< Reference Y base function according to CIE standard observer.
    extern const std::array<float, rawNumSamples> rawZ; ///< Reference Z base function according to CIE standard observer.
    extern const std::array<float, rawNumSamples> rawLambdaNm; ///< Wavelengths of CIE reference data in nano meters.



    /*! \brief Base class of color representation classes.
     *  \tparam nCoeffs Number of base function coefficients representing the spectrum.
     */
    template <int nCoeffs>
    class CoefficientSpectrum
    {
    public:
        CoefficientSpectrum(float value = 0.f);
        
        // operators
        bool operator==(const CoefficientSpectrum<nCoeffs>& rhs) const;
        bool operator!=(const CoefficientSpectrum<nCoeffs>& rhs) const;
        
        bool isBlack() const;
        bool isNan() const;
        CoefficientSpectrum& linearInterpolate(float t, const CoefficientSpectrum& other) const;
        CoefficientSpectrum& clamp(float minLimit = 0.f, float maxLimit = std::numeric_limits<float>::infinity()) const;

        int getNumOfCoeffs() const;
        float getCoeff(int index) const;
        void setCoeff(int index, float value);
        
        static void xyzToRgb(const std::array<float, 3>& xyz, std::array<float, 3>& rgb);
        static void rgbToXyz(const std::array<float, 3>& rgb, std::array<float, 3>& xyz);
        
        static void sortSpectrum(std::vector<float>& lambdaSamples, std::vector<float>& valueSamples);
        static float averageSpectrumSamples(const std::vector<float>& lambdaSamples, const std::vector<float> valueSamples, float lambda0, float lambda1);
        static float linearInterpolate(float t, float v1, float v2);

//        virtual void toRgb(float& value) const = 0;
//        virtual void toMonochromatic(std::array<float, 3>& rgb) const = 0;
//        virtual void toSampled(std::array<float, 30>& color) const = 0;
        
    protected:
        float coeffs[nCoeffs]; ///< Coefficients of spectrum.

    };



    /*! \brief Default constructor with uniform initialization.
     *  \param[in] value Default coefficient value.
     */
    template <int nCoeffs>
    CoefficientSpectrum<nCoeffs>::CoefficientSpectrum(float value)
    {
        for (int i = 0; i < nCoeffs; ++i)
        {
            coeffs[i] = value;
        }
    }



    /*! \brief Equality operator.
     *  \param[in] rhs Spectrum to compare.
     *  \return Returns TRUE if spectra are equal, else FALSE.
     */
    template <int nCoeffs>
    bool CoefficientSpectrum<nCoeffs>::operator==(const CoefficientSpectrum& rhs) const
    {
        for (int i = 0; i < nCoeffs; ++i)
        {
            if (coeffs[i] != rhs.coeffs[i])
            {
                return false;
            }
        }
        
        return true;
    }



    /*! \brief Inequality operator.
     *  \param[in] rhs Spectrum to compare.
     *  \return Returns TRUE if spectra are unequal, else FALSE.
     */
    template <int nCoeffs>
    bool CoefficientSpectrum<nCoeffs>::operator!=(const CoefficientSpectrum& rhs) const
    {
        return !(this->operator==(rhs));
    }



    /*! \brief Checks if spectrum is all black.
     *  \return Return TRUE if spectrum is all black, else FALSE.
     */
    template <int nCoeffs>
    bool CoefficientSpectrum<nCoeffs>::isBlack() const
    {
        for (int i = 0; i < nCoeffs; ++i)
        {
            if (coeffs[i] == 0.f)
            {
                return false;
            }
        }
        return true;
    }



    /*! \brief Checks if coefficients contain NaN values.
     *  \return Returns TRUE if at least one coefficients is not a number, else FALSE.
     */
    template <int nCoeffs>
    bool CoefficientSpectrum<nCoeffs>::isNan() const
    {
        for (int i = 0; i < nCoeffs; ++i)
        {
            if (coeffs[i] != coeffs[i])
            {
                return true;
            }
        }
        return false;
    }



    /*! \brief Interpolates linearly between two spectra.
     *  \param[in] t Weighting factor between 0 and 1.
     *  \param[in] other Other spectrum,
     */
    template <int nCoeffs>
    CoefficientSpectrum<nCoeffs>& CoefficientSpectrum<nCoeffs>::linearInterpolate(float t, const CoefficientSpectrum<nCoeffs>& other) const
    {
        return (1.f - t) * *this + t * other;
    }



    /*! \brief Clamp spectrum coefficients.
     *  \param[in] minLimit Lower limit.
     *  \param[in] maxLimit Upper limit.
     */
    template <int nCoeffs>
    CoefficientSpectrum<nCoeffs>& CoefficientSpectrum<nCoeffs>::clamp(float minLimit, float maxLimit) const
    {
        for (int i = 0; i < nCoeffs; ++i)
        {
            coeffs[i] = std::max<float>(coeffs[i], minLimit);
            coeffs[i] = std::min<float>(coeffs[i], maxLimit);
        }
        return *this;
    }



    /*! \brief Get number of coefficients.
     *  \return Returns number of coefficients.
     */
    template <int nCoeffs>
    int CoefficientSpectrum<nCoeffs>::getNumOfCoeffs() const
    {
        return nCoeffs;
    }



    /*! \brief Get coefficients.
     *  \param[in] index Index of coefficient.
     *  \return Returns specified coefficient.
     */
    template <int nCoeffs>
    float CoefficientSpectrum<nCoeffs>::getCoeff(int index) const
    {
        if (index >= 0 && index < nCoeffs)
        {
            return coeffs[index];
        }
        return 0.f;
    }



    /*! \brief Set coefficients.
     *  \param[in] index Index of coefficient.
     *  \param[in] value Value of specified coefficient.
     */
    template <int nCoeffs>
    void CoefficientSpectrum<nCoeffs>::setCoeff(int index, float value)
    {
        if (index >= 0 && index < nCoeffs)
        {
            coeffs[index] = value;
        }
    }



    /*! \brief Convert color from XYZ to sRGB representation.
     *  \param[in] xyz Color in XYZ representation.
     *  \param[out] rgb Color in rgb representation according to sRGB standard.
     */
    template <int nCoeffs>
    void CoefficientSpectrum<nCoeffs>::xyzToRgb(const std::array<float ,3>& xyz, std::array<float, 3>& rgb)
    {
        rgb[0] =  3.2404542f * xyz[0] - 1.5371385f * xyz[1] - 0.4985314f * xyz[2];
        rgb[1] = -0.9692660f * xyz[0] + 1.8760108f * xyz[1] + 0.0415560f * xyz[2];
        rgb[2] =  0.0556434f * xyz[0] - 0.2040259f * xyz[1] + 1.0572252f * xyz[2];
    }



    /*! \brief Convert color from sRGB to XYZ representation.
     *  \param[in] rgb Color in rgb representation according to sRGB standard.
     *  \param[out] xyz Color in XYZ representation.
     */
    template <int nCoeffs>
    void CoefficientSpectrum<nCoeffs>::rgbToXyz(const std::array<float, 3>& rgb, std::array<float, 3>& xyz)
    {
        xyz[0] = 0.4124564f * rgb[0] + 0.3575761f * rgb[1] + 0.1804375f * rgb[2];
        xyz[1] = 0.2126729f * rgb[0] + 0.7151522f * rgb[1] + 0.0721750f * rgb[2];
        xyz[2] = 0.0193339f * rgb[0] + 0.1191920f * rgb[1] + 0.9503041f * rgb[2];
    }



    /*! \brief Sort sampled spectrum values according to corresponding wavelengths.
     *  \param[in,out] lambdaSamples Vector of wavelengths of sampled spectrum.
     *  \param[in,out] valueSamples Vector of sampled spectrum values at given wavelengths.
     */
    template <int nCoeffs>
    void CoefficientSpectrum<nCoeffs>::sortSpectrum(std::vector<float>& lambdaSamples, std::vector<float>& valueSamples)
    {
        // copy data in std::pair data structure
        std::vector<std::pair<float, float> > sortingVector;
        sortingVector.reserve(lambdaSamples.size());
        for (unsigned int i = 0; i < lambdaSamples.size(); ++i)
        {
            sortingVector.push_back(std::make_pair(lambdaSamples[i], valueSamples[i]));
        }
        
        // sort data
        std::sort(sortingVector.begin(), sortingVector.end());
        for (unsigned int i = 0; i < lambdaSamples.size(); ++i)
        {
            lambdaSamples[i] = sortingVector[i].first;
            valueSamples[i] = sortingVector[i].second;
        }
    }



    /*! \brief Interpolates spectrum to match to inner class structure.
     *  \param[in] lambdaSamples Vector of wavelengths of sampled spectrum.
     *  \param[in] valueSamples Vector of sampled spectrum values at given wavelengths.
     *  \param[in] lambdaStart Start value of wavelength segment.
     *  \param[in] lambdaEnd End value of wavelength segment.
     */
    template <int nCoeffs>
    float CoefficientSpectrum<nCoeffs>::averageSpectrumSamples(const std::vector<float>& lambdaSamples, const std::vector<float> valueSamples, float lambdaStart, float lambdaEnd)
    {
        // handle case that range of interest lies at lower wavelengths than the provided sampled data
        if (lambdaEnd < lambdaSamples.front())
        {
            return valueSamples.front();
        }
        
        // handle case that range of interest lies beyond the wavelengths of the provided sampled data
        if (lambdaStart > lambdaSamples.back())
        {
            return valueSamples.back();
        }
        
        // handle case that sample spectral data only consists of one sample
        if (lambdaSamples.size() == 1)
        {
            return valueSamples.front();
        }
        
        float sum = 0.f;
        
        // add contribution of constant segments before sampled data
        if (lambdaStart < lambdaSamples.front())
        {
            sum += valueSamples.front() * (lambdaSamples.front() - lambdaStart);
        }
        
        // add contribution of constant segments after sampled data
        if (lambdaEnd > lambdaSamples.back())
        {
            sum += valueSamples.back() * (lambdaEnd - lambdaSamples.back());
        }
        
        // advance to first relevant wavelength segment
        unsigned int i = 0;
        while (lambdaStart > lambdaSamples[i + 1])
        {
            ++i;
        }
        
        // loop over wavelength sample segments and add contributions
        for ( ; i + 1 < lambdaSamples.size() && lambdaEnd >= lambdaSamples[i]; ++i)
        {
            float segmentStart = std::max(lambdaStart, lambdaSamples[i]);
            float segmentEnd = std::min(lambdaEnd, lambdaSamples[i+1]);
            
            float interp0 = linearInterpolate((segmentStart - lambdaSamples[i]) / (lambdaSamples[i + 1] - lambdaSamples[i]),
                                              valueSamples[i], valueSamples[i + 1]);
            float interp1 = linearInterpolate((segmentEnd - lambdaSamples[i]) / (lambdaSamples[i + 1] - lambdaSamples[i]),
                                              valueSamples[i], valueSamples[i + 1]);
            
            sum += 0.5f * (interp0 + interp1) * (segmentEnd - segmentStart);
        }
        
        return sum / (lambdaEnd - lambdaStart);
    }



    /*! \brief Linearly interpolates between two given sample values.
     *  \param[in] t Weight of samples, between 0 and 1.
     *  \param[in] v1 First sample value.
     *  \param[in] v2 Second sample value.
     */
    template <int nCoeffs>
    float CoefficientSpectrum<nCoeffs>::linearInterpolate(float t, float v1, float v2)
    {
        return (1.f - t) * v1 + t * v2;
    }

    } // end of namespace iiit

#endif // __COEFFICIENTSPECTRUM_HPP__
