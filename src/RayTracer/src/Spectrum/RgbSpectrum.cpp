// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file RgbSpectrum.cpp
 *  \author Thomas Nuernberg
 *  \date 14.12.2015
 *  \brief Definition of RgbSpectrum.
 *
 *  Implementation of RgbSpectrum class, that represents a color by three components.
 */

#include "RgbSpectrum.hpp"

#include <math.h>

#include "SampledSpectrum.hpp"



namespace iiit
{

    // Mean wavelengths of RGB color representation
    const std::array<int, 3> RgbSpectrum::rgbWaveLengthsNm = {{597, 541, 446}};



    /*! \brief Default constructor with uniform initialization.
     *  \param[in] value Default coefficient value.
     */
    RgbSpectrum::RgbSpectrum(float value)
    {
        for (int i = 0; i < 3; ++i)
        {
            coeffs[i] = value;
        }
    }



    /*! \brief Constructor with initialization from float array.
     *  \param[in] values Array of float with coefficients.
     */
    RgbSpectrum::RgbSpectrum(const float* values)
    {
        for (int i = 0; i < 3; ++i)
        {
            coeffs[i] = values[i];
        }
    }



    /*! \brief Constructor with initialization from float array.
     *  \param[in] values Array of float with coefficients.
     */
    RgbSpectrum::RgbSpectrum(const std::array<float, 3>& values)
    {
        for (int i = 0; i < 3; ++i)
        {
            coeffs[i] = values[i];
        }
    }



    /*! \brief Constructs RgbSpectrum object from sampled data.
     *  \param[in] lambdaSamples Vector of wavelengths of sampled spectrum.
     *  \param[in] valueSamples Vector of sampled spectrum values at given wavelengths.
     *
     *  Factory method to construct a SampleSpectrum object from a sampled spectrum. The method sorts
     *  the given samples if needed and interpolates the spectrum to match the inner representation of
     *  the class.
     */
    RgbSpectrum RgbSpectrum::fromSampled(const std::vector<float>& lambdaSamples, const std::vector<float>& valueSamples)
    {
        if (!std::is_sorted(lambdaSamples.begin(), lambdaSamples.end()))
        {
            // sort sampled data by wavelengths
            std::vector<float> lambdas = lambdaSamples;
            std::vector<float> values = valueSamples;
            sortSpectrum(values, lambdas);
            return fromSampled(values, lambdas);
        }
        std::array<float, 3> xyz = {{0.f, 0.f, 0.f}};
        float yIntegral = 0.f;
        for (int i = 0; i < rawNumSamples; ++i)
        {
            yIntegral += rawY[i];
            
            float value = interpolateSpectrumSamples(lambdaSamples, valueSamples, rawLambdaNm[i]);
            xyz[0] += value * rawX[i];
            xyz[1] += value * rawY[i];
            xyz[2] += value * rawZ[i];
        }
        xyz[0] /= yIntegral;
        xyz[1] /= yIntegral;
        xyz[2] /= yIntegral;
        
        return fromXyz(xyz);
    }



    /*! \brief Constructs RgbSpectrum object from RGB data.
     *  \param[in] rgb Color in RGB representation.
     *  \param[in] type Conversion type.
     *  \return Returns color in RGB representation.
     */
    RgbSpectrum RgbSpectrum::fromRgb(const std::array<float, 3>& rgb, SpectrumRepresentationType type)
    {
        return RgbSpectrum(rgb);
    }



    /*! \brief Constructs RgbSpectrum object from XYZ data.
     *  \param[in] xyz Color in XYZ representation.
     *  \param[in] type Conversion type.
     *  \return Returns color in RGB representation.
     */
    RgbSpectrum RgbSpectrum::fromXyz(const std::array<float, 3> &xyz, SpectrumRepresentationType type)
    {
        std::array<float, 3> rgb = {{0.f, 0.f, 0.f}};
        xyzToRgb(xyz, rgb);
        return RgbSpectrum(rgb);
    }



    /*! \brief Convert to CIE XYZ color representation.
     *  \param[out] xyz CIE XYZ color.
     */
    void RgbSpectrum::toXyz(std::array<float, 3>& xyz) const
    {
        std::array<float, 3> rgb = {{0.f, 0.f, 0.f}};
        rgb[0] = coeffs[0];
        rgb[1] = coeffs[1];
        rgb[2] = coeffs[2];
        rgbToXyz(rgb, xyz);
    }



    /*! \brief Convert to sRGB color representation.
     *  \param[out] rgb sRGB color.
     */
    void RgbSpectrum::toRgb(std::array<float, 3>& rgb) const
    {
        rgb[0] = coeffs[0];
        rgb[1] = coeffs[1];
        rgb[2] = coeffs[2];
    }
    

    /*! \brief Convert to sampledSpectrum.
     *  \param[out] SampledSpectrum color.
     */
    void RgbSpectrum::toSampled(SampledSpectrum& color) const
    {
        std::array<float, 3> rgb;
        rgb[0] = this->getCoeff(0);
        rgb[1] = this->getCoeff(1);
        rgb[2] = this->getCoeff(2);
        color = SampledSpectrum::fromRgb(rgb, SpectrumRepresentationType::IlluminationSpectrum);
    }


    
    /*! \brief Get wavelength of coefficient in nanometers.
     *  \param[in] index Index of coefficient.
     *  \returns Wavelength in nanometers.
     */
    int RgbSpectrum::getWaveLengthNm(int index)
    {
        return rgbWaveLengthsNm[index];
    }


    
    /*! \brief Assignment operator.
     *  \param[in] rhs Spectrum to assign.
     *  \return Returns reference to result.
     */
    RgbSpectrum& RgbSpectrum::operator=(const RgbSpectrum& rhs)
    {

        coeffs[0] = rhs.coeffs[0];
        coeffs[1] = rhs.coeffs[1];
        coeffs[2] = rhs.coeffs[2];
        return *this;
    }
    
    
    
    /*! \brief Add operator.
     *  \param[in] rhs Spectrum to add.
     *  \return Returns reference to summation result.
     */
    RgbSpectrum& RgbSpectrum::operator+=(const RgbSpectrum& rhs)
    {
        coeffs[0] += rhs.coeffs[0];
        coeffs[1] += rhs.coeffs[1];
        coeffs[2] += rhs.coeffs[2];
        return *this;
    }
    
    
    
    /*! \brief Add operator.
     *  \param[in] rhs Spectrum to add.
     *  \return Returns summation result.
     */
    RgbSpectrum RgbSpectrum::operator+(const RgbSpectrum& rhs) const
    {
        RgbSpectrum res = *this;
        res.coeffs[0] += rhs.coeffs[0];
        res.coeffs[1] += rhs.coeffs[1];
        res.coeffs[2] += rhs.coeffs[2];
        return res;
    }
    
    
    
    /*! \brief Unary minus operator.
     *  \return Returns negated result.
     */
    RgbSpectrum RgbSpectrum::operator-(void) const
    {
        RgbSpectrum res = *this;
        res.coeffs[0] = -coeffs[0];
        res.coeffs[1] = -coeffs[1];
        res.coeffs[2] = -coeffs[2];
        return res;
    }
    
    
    
    /*! \brief Minus operator.
     *  \param[in] rhs Spectrum to subtract.
     *  \return Returns reference to subtraction result.
     */
    RgbSpectrum& RgbSpectrum::operator-=(const RgbSpectrum& rhs)
    {
        coeffs[0] -= rhs.coeffs[0];
        coeffs[1] -= rhs.coeffs[1];
        coeffs[2] -= rhs.coeffs[2];
        return *this;
    }
    
    
    
    /*! \brief Minus operator.
     *  \param[in] rhs Spectrum to subtract.
     *  \return Returns subtraction result.
     */
    RgbSpectrum RgbSpectrum::operator-(const RgbSpectrum& rhs) const
    {
        RgbSpectrum res = *this;
        res.coeffs[0] -= rhs.coeffs[0];
        res.coeffs[1] -= rhs.coeffs[1];
        res.coeffs[2] -= rhs.coeffs[2];
        return res;
    }
    
    
    
    /*! \brief Scalar multiplication operator.
     *  \param[in] factor Scalar to multiply.
     *  \return Returns reference to multiplication result.
     */
    RgbSpectrum& RgbSpectrum::operator*=(float factor)
    {
        coeffs[0] *= factor;
        coeffs[1] *= factor;
        coeffs[2] *= factor;
        return *this;
    }
    
    
    
    /*! \brief Scalar multiplication operator.
     *  \param[in] factor Scalar to multiply.
     *  \return Returns multiplication result.
     */
    RgbSpectrum RgbSpectrum::operator*(float factor) const
    {
        RgbSpectrum res = *this;
        res.coeffs[0] *= factor;
        res.coeffs[1] *= factor;
        res.coeffs[2] *= factor;
        return res;
    }
    
    
    
    /*! \brief Element-wise multiplication operator.
     *  \param[in] rhs Spectrum to multiply.
     *  \return Returns reference to multiplication result.
     */
    RgbSpectrum& RgbSpectrum::operator*=(const RgbSpectrum& rhs)
    {
        coeffs[0] *= rhs.coeffs[0];
        coeffs[1] *= rhs.coeffs[1];
        coeffs[2] *= rhs.coeffs[2];
        return *this;
    }
    
    
    
    /*! \brief Element-wise multiplication operator.
     *  \param[in] rhs Spectrum to multiply.
     *  \return Returns multiplication result.
     */
    RgbSpectrum RgbSpectrum::operator*(const RgbSpectrum& rhs) const
    {
        RgbSpectrum res = *this;
        res.coeffs[0] *= rhs.coeffs[0];
        res.coeffs[1] *= rhs.coeffs[1];
        res.coeffs[2] *= rhs.coeffs[2];
        return res;
    }
    
    
    
    /*! \brief Scalar division operator.
     *  \param[in] factor Scalar to divide.
     *  \return Returns reference to division result.
     */
    RgbSpectrum& RgbSpectrum::operator/=(float factor)
    {
        coeffs[0] /= factor;
        coeffs[1] /= factor;
        coeffs[2] /= factor;
        return *this;
    }
    
    
    
    /*! \brief Scalar division operator.
     *  \param[in] factor Scalar to divide.
     *  \return Returns division result.
     */
    
    RgbSpectrum RgbSpectrum::operator/(float factor) const
    {
        RgbSpectrum res = *this;
        res.coeffs[0] /= factor;
        res.coeffs[2] /= factor;
        res.coeffs[1] /= factor;
        return res;
    }
    
    
    
    /*! \brief Element-wise division operator.
     *  \param[in] rhs Spectrum to divide.
     *  \return Returns reference to division result.
     */
    RgbSpectrum& RgbSpectrum::operator/=(const RgbSpectrum& rhs)
    {
        coeffs[0] /= rhs.coeffs[0];
        coeffs[1] /= rhs.coeffs[1];
        coeffs[2] /= rhs.coeffs[2];
        return *this;
    }
    
    
    
    /*! \brief Element-wise division operator.
     *  \param[in] rhs Spectrum to divide.
     *  \return Returns division result.
     */
    RgbSpectrum RgbSpectrum::operator/(const RgbSpectrum& rhs) const
    {
        RgbSpectrum res = *this;
        res.coeffs[0] /= rhs.coeffs[0];
        res.coeffs[1] /= rhs.coeffs[1];
        res.coeffs[2] /= rhs.coeffs[2];
        return res;
    }
    
    
    
    /*! \brief Raise to power.
     *  \param[in] p Power.
     *  \return Returns result.
     */
    RgbSpectrum RgbSpectrum::powc(float p) const
    {
        RgbSpectrum res = *this;
        res.coeffs[0] = pow(coeffs[0], p);
        res.coeffs[1] = pow(coeffs[1], p);
        res.coeffs[2] = pow(coeffs[2], p);
        return res;
    }



    /*! \brief Interpolates spectral value at given wavelength.
     *  \param[in] lambdas Sampled wavelength data.
     *  \param[in] values Sampled spectral data.
     *  \param[in] lambda Wavelength of interest.
     *  \return Return interpolated spectral value.
     */
    float RgbSpectrum::interpolateSpectrumSamples(const std::vector<float>& lambdas, const std::vector<float>& values, float lambda)
    {
        if (lambda < lambdas.front())
        {
            return values.front();
        }
        
        if (lambda > lambdas.back())
        {
            return values.back();
        }
        
        for (unsigned int i = 0; i < lambdas.size() - 1; ++i)
        {
            if (lambda >= lambdas[i] && lambda <= lambdas[i + 1])
            {
                return linearInterpolate((lambda - lambdas[i]) / (lambdas[i + 1] - lambdas[i]), values[i], values[i + 1]);
            }
        }
        return 0.f;
    }

} // end of namespace iiit
