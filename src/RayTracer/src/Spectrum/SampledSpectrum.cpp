// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file SampledSpectrum.cpp
 *  \author Thomas Nuernberg
 *  \date 11.12.2015
 *  \brief Definition of SampledSpectrum.
 *
 *  Implementation of SampledSpectrum class, that represents a continuous spectrum using equidistant
 *  samples.
 */

#include "SampledSpectrum.hpp"

#include <algorithm>
#include <utility>
#include <math.h>



namespace iiit
{

    // spectral data definitions
    SampledSpectrum SampledSpectrum::x_;
    SampledSpectrum SampledSpectrum::y_;
    SampledSpectrum SampledSpectrum::z_;
    float SampledSpectrum::yIntegral_;

    SampledSpectrum SampledSpectrum::rgbReflectanceSpectrumWhite_;
    SampledSpectrum SampledSpectrum::rgbReflectanceSpectrumCyan_;
    SampledSpectrum SampledSpectrum::rgbReflectanceSpectrumMagenta_;
    SampledSpectrum SampledSpectrum::rgbReflectanceSpectrumYellow_;
    SampledSpectrum SampledSpectrum::rgbReflectanceSpectrumRed_;
    SampledSpectrum SampledSpectrum::rgbReflectanceSpectrumGreen_;
    SampledSpectrum SampledSpectrum::rgbReflectanceSpectrumBlue_;

    SampledSpectrum SampledSpectrum::rgbIlluminationSpectrumWhite_;
    SampledSpectrum SampledSpectrum::rgbIlluminationSpectrumCyan_;
    SampledSpectrum SampledSpectrum::rgbIlluminationSpectrumMagenta_;
    SampledSpectrum SampledSpectrum::rgbIlluminationSpectrumYellow_;
    SampledSpectrum SampledSpectrum::rgbIlluminationSpectrumRed_;
    SampledSpectrum SampledSpectrum::rgbIlluminationSpectrumGreen_;
    SampledSpectrum SampledSpectrum::rgbIlluminationSpectrumBlue_;



    // initialization of static const member variables
    const std::array<float, numRgbSpectralSamples> SampledSpectrum::rgbSpectrumLambdaNm_ = {{
        380.000000f, 390.967743f, 401.935486f, 412.903229f, 423.870972f,
        434.838715f, 445.806458f, 456.774200f, 467.741943f, 478.709686f,
        489.677429f, 500.645172f, 511.612915f, 522.580627f, 533.548340f,
        544.516052f, 555.483765f, 566.451477f, 577.419189f, 588.386902f,
        599.354614f, 610.322327f, 621.290039f, 632.257751f, 643.225464f,
        654.193176f, 665.160889f, 676.128601f, 687.096313f, 698.064026f,
        709.031738f, 720.000000f}};



    const std::array<float, numRgbSpectralSamples> SampledSpectrum::rgbRawReflectanceSpectrumWhite_ = {{
        1.0618958571272863e+00f, 1.0615019980348779e+00f,
        1.0614335379927147e+00f, 1.0622711654692485e+00f,
        1.0622036218416742e+00f, 1.0625059965187085e+00f,
        1.0623938486985884e+00f, 1.0624706448043137e+00f,
        1.0625048144827762e+00f, 1.0624366131308856e+00f,
        1.0620694238892607e+00f, 1.0613167586932164e+00f,
        1.0610334029377020e+00f, 1.0613868564828413e+00f,
        1.0614215366116762e+00f, 1.0620336151299086e+00f,
        1.0625497454805051e+00f, 1.0624317487992085e+00f,
        1.0625249140554480e+00f, 1.0624277664486914e+00f,
        1.0624749854090769e+00f, 1.0625538581025402e+00f,
        1.0625326910104864e+00f, 1.0623922312225325e+00f,
        1.0623650980354129e+00f, 1.0625256476715284e+00f,
        1.0612277619533155e+00f, 1.0594262608698046e+00f,
        1.0599810758292072e+00f, 1.0602547314449409e+00f,
        1.0601263046243634e+00f, 1.0606565756823634e+00f}};



    const std::array<float, numRgbSpectralSamples> SampledSpectrum::rgbRawReflectanceSpectrumCyan_ = {{
         1.0414628021426751e+00f,  1.0328661533771188e+00f,
         1.0126146228964314e+00f,  1.0350460524836209e+00f,
         1.0078661447098567e+00f,  1.0422280385081280e+00f,
         1.0442596738499825e+00f,  1.0535238290294409e+00f,
         1.0180776226938120e+00f,  1.0442729908727713e+00f,
         1.0529362541920750e+00f,  1.0537034271160244e+00f,
         1.0533901869215969e+00f,  1.0537782700979574e+00f,
         1.0527093770467102e+00f,  1.0530449040446797e+00f,
         1.0550554640191208e+00f,  1.0553673610724821e+00f,
         1.0454306634683976e+00f,  6.2348950639230805e-01f,
         1.8038071613188977e-01f, -7.6303759201984539e-03f,
        -1.5217847035781367e-04f, -7.5102257347258311e-03f,
        -2.1708639328491472e-03f,  6.5919466602369636e-04f,
         1.2278815318539780e-02f, -4.4669775637208031e-03f,
         1.7119799082865147e-02f,  4.9211089759759801e-03f,
         5.8762925143334985e-03f,  2.5259399415550079e-02f}};



    const std::array<float, numRgbSpectralSamples> SampledSpectrum::rgbRawReflectanceSpectrumMagenta_ = {{
         9.9422138151236850e-01f,  9.8986937122975682e-01f,
         9.8293658286116958e-01f,  9.9627868399859310e-01f,
         1.0198955019000133e+00f,  1.0166395501210359e+00f,
         1.0220913178757398e+00f,  9.9651666040682441e-01f,
         1.0097766178917882e+00f,  1.0215422470827016e+00f,
         6.4031953387790963e-01f,  2.5012379477078184e-03f,
         6.5339939555769944e-03f,  2.8334080462675826e-03f,
        -5.1209675389074505e-11f, -9.0592291646646381e-03f,
         3.3936718323331200e-03f, -3.0638741121828406e-03f,
         2.2203936168286292e-01f,  6.3141140024811970e-01f,
         9.7480985576500956e-01f,  9.7209562333590571e-01f,
         1.0173770302868150e+00f,  9.9875194322734129e-01f,
         9.4701725739602238e-01f,  8.5258623154354796e-01f,
         9.4897798581660842e-01f,  9.4751876096521492e-01f,
         9.9598944191059791e-01f,  8.6301351503809076e-01f,
         8.9150987853523145e-01f,  8.4866492652845082e-01f}};



    const std::array<float, numRgbSpectralSamples> SampledSpectrum::rgbRawReflectanceSpectrumYellow_ = {{
         5.5740622924920873e-03f, -4.7982831631446787e-03f,
        -5.2536564298613798e-03f, -6.4571480044499710e-03f,
        -5.9693514658007013e-03f, -2.1836716037686721e-03f,
         1.6781120601055327e-02f,  9.6096355429062641e-02f,
         2.1217357081986446e-01f,  3.6169133290685068e-01f,
         5.3961011543232529e-01f,  7.4408810492171507e-01f,
         9.2209571148394054e-01f,  1.0460304298411225e+00f,
         1.0513824989063714e+00f,  1.0511991822135085e+00f,
         1.0510530911991052e+00f,  1.0517397230360510e+00f,
         1.0516043086790485e+00f,  1.0511944032061460e+00f,
         1.0511590325868068e+00f,  1.0516612465483031e+00f,
         1.0514038526836869e+00f,  1.0515941029228475e+00f,
         1.0511460436960840e+00f,  1.0515123758830476e+00f,
         1.0508871369510702e+00f,  1.0508923708102380e+00f,
         1.0477492815668303e+00f,  1.0493272144017338e+00f,
         1.0435963333422726e+00f,  1.0392280772051465e+00f}};



    const std::array<float, numRgbSpectralSamples> SampledSpectrum::rgbRawReflectanceSpectrumRed_ = {{
         1.6575604867086180e-01f,  1.1846442802747797e-01f,
         1.2408293329637447e-01f,  1.1371272058349924e-01f,
         7.8992434518899132e-02f,  3.2205603593106549e-02f,
        -1.0798365407877875e-02f,  1.8051975516730392e-02f,
         5.3407196598730527e-03f,  1.3654918729501336e-02f,
        -5.9564213545642841e-03f, -1.8444365067353252e-03f,
        -1.0571884361529504e-02f, -2.9375521078000011e-03f,
        -1.0790476271835936e-02f, -8.0224306697503633e-03f,
        -2.2669167702495940e-03f,  7.0200240494706634e-03f,
        -8.1528469000299308e-03f,  6.0772866969252792e-01f,
         9.8831560865432400e-01f,  9.9391691044078823e-01f,
         1.0039338994753197e+00f,  9.9234499861167125e-01f,
         9.9926530858855522e-01f,  1.0084621557617270e+00f,
         9.8358296827441216e-01f,  1.0085023660099048e+00f,
         9.7451138326568698e-01f,  9.8543269570059944e-01f,
         9.3495763980962043e-01f,  9.8713907792319400e-01f}};



    const std::array<float, numRgbSpectralSamples> SampledSpectrum::rgbRawReflectanceSpectrumGreen_ = {{
         2.6494153587602255e-03f, -5.0175013429732242e-03f,
        -1.2547236272489583e-02f, -9.4554964308388671e-03f,
        -1.2526086181600525e-02f, -7.9170697760437767e-03f,
        -7.9955735204175690e-03f, -9.3559433444469070e-03f,
         6.5468611982999303e-02f,  3.9572875517634137e-01f,
         7.5244022299886659e-01f,  9.6376478690218559e-01f,
         9.9854433855162328e-01f,  9.9992977025287921e-01f,
         9.9939086751140449e-01f,  9.9994372267071396e-01f,
         9.9939121813418674e-01f,  9.9911237310424483e-01f,
         9.6019584878271580e-01f,  6.3186279338432438e-01f,
         2.5797401028763473e-01f,  9.4014888527335638e-03f,
        -3.0798345608649747e-03f, -4.5230367033685034e-03f,
        -6.8933410388274038e-03f, -9.0352195539015398e-03f,
        -8.5913667165340209e-03f, -8.3690869120289398e-03f,
        -7.8685832338754313e-03f, -8.3657578711085132e-06f,
         5.4301225442817177e-03f, -2.7745589759259194e-03f}};



    const std::array<float, numRgbSpectralSamples> SampledSpectrum::rgbRawReflectanceSpectrumBlue_ = {{
         9.9209771469720676e-01f,  9.8876426059369127e-01f,
         9.9539040744505636e-01f,  9.9529317353008218e-01f,
         9.9181447411633950e-01f,  1.0002584039673432e+00f,
         9.9968478437342512e-01f,  9.9988120766657174e-01f,
         9.8504012146370434e-01f,  7.9029849053031276e-01f,
         5.6082198617463974e-01f,  3.3133458513996528e-01f,
         1.3692410840839175e-01f,  1.8914906559664151e-02f,
        -5.1129770932550889e-06f, -4.2395493167891873e-04f,
        -4.1934593101534273e-04f,  1.7473028136486615e-03f,
         3.7999160177631316e-03f, -5.5101474906588642e-04f,
        -4.3716662898480967e-05f,  7.5874501748732798e-03f,
         2.5795650780554021e-02f,  3.8168376532500548e-02f,
         4.9489586408030833e-02f,  4.9595992290102905e-02f,
         4.9814819505812249e-02f,  3.9840911064978023e-02f,
         3.0501024937233868e-02f,  2.1243054765241080e-02f,
         6.9596532104356399e-03f,  4.1733649330980525e-03f}};



    const std::array<float, numRgbSpectralSamples> SampledSpectrum::rgbRawIlluminationSpectrumWhite_ = {{
        1.1565232050369776e+00f, 1.1567225000119139e+00f,
        1.1566203150243823e+00f, 1.1555782088080084e+00f,
        1.1562175509215700e+00f, 1.1567674012207332e+00f,
        1.1568023194808630e+00f, 1.1567677445485520e+00f,
        1.1563563182952830e+00f, 1.1567054702510189e+00f,
        1.1565134139372772e+00f, 1.1564336176499312e+00f,
        1.1568023181530034e+00f, 1.1473147688514642e+00f,
        1.1339317140561065e+00f, 1.1293876490671435e+00f,
        1.1290515328639648e+00f, 1.0504864823782283e+00f,
        1.0459696042230884e+00f, 9.9366687168595691e-01f,
        9.5601669265393940e-01f, 9.2467482033511805e-01f,
        9.1499944702051761e-01f, 8.9939467658453465e-01f,
        8.9542520751331112e-01f, 8.8870566693814745e-01f,
        8.8222843814228114e-01f, 8.7998311373826676e-01f,
        8.7635244612244578e-01f, 8.8000368331709111e-01f,
        8.8065665428441120e-01f, 8.8304706460276905e-01f}};



    const std::array<float, numRgbSpectralSamples> SampledSpectrum::rgbRawIlluminationSpectrumCyan_ = {{
         1.1334479663682135e+00f,  1.1266762330194116e+00f,
         1.1346827504710164e+00f,  1.1357395805744794e+00f,
         1.1356371830149636e+00f,  1.1361152989346193e+00f,
         1.1362179057706772e+00f,  1.1364819652587022e+00f,
         1.1355107110714324e+00f,  1.1364060941199556e+00f,
         1.1360363621722465e+00f,  1.1360122641141395e+00f,
         1.1354266882467030e+00f,  1.1363099407179136e+00f,
         1.1355450412632506e+00f,  1.1353732327376378e+00f,
         1.1349496420726002e+00f,  1.1111113947168556e+00f,
         9.0598740429727143e-01f,  6.1160780787465330e-01f,
         2.9539752170999634e-01f,  9.5954200671150097e-02f,
        -1.1650792030826267e-02f, -1.2144633073395025e-02f,
        -1.1148167569748318e-02f, -1.1997606668458151e-02f,
        -5.0506855475394852e-03f, -7.9982745819542154e-03f,
        -9.4722817708236418e-03f, -5.5329541006658815e-03f,
        -4.5428914028274488e-03f, -1.2541015360921132e-02f}};



    const std::array<float, numRgbSpectralSamples> SampledSpectrum::rgbRawIlluminationSpectrumMagenta_ = {{
         1.0371892935878366e+00f,  1.0587542891035364e+00f,
         1.0767271213688903e+00f,  1.0762706844110288e+00f,
         1.0795289105258212e+00f,  1.0743644742950074e+00f,
         1.0727028691194342e+00f,  1.0732447452056488e+00f,
         1.0823760816041414e+00f,  1.0840545681409282e+00f,
         9.5607567526306658e-01f,  5.5197896855064665e-01f,
         8.4191094887247575e-02f,  8.7940070557041006e-05f,
        -2.3086408335071251e-03f, -1.1248136628651192e-03f,
        -7.7297612754989586e-11f, -2.7270769006770834e-04f,
         1.4466473094035592e-02f,  2.5883116027169478e-01f,
         5.2907999827566732e-01f,  9.0966624097105164e-01f,
         1.0690571327307956e+00f,  1.0887326064796272e+00f,
         1.0637622289511852e+00f,  1.0201812918094260e+00f,
         1.0262196688979945e+00f,  1.0783085560613190e+00f,
         9.8333849623218872e-01f,  1.0707246342802621e+00f,
         1.0634247770423768e+00f,  1.0150875475729566e+00f}};



    const std::array<float, numRgbSpectralSamples> SampledSpectrum::rgbRawIlluminationSpectrumYellow_ = {{
         2.7756958965811972e-03f,  3.9673820990646612e-03f,
        -1.4606936788606750e-04f,  3.6198394557748065e-04f,
        -2.5819258699309733e-04f, -5.0133191628082274e-05f,
        -2.4437242866157116e-04f, -7.8061419948038946e-05f,
         4.9690301207540921e-02f,  4.8515973574763166e-01f,
         1.0295725854360589e+00f,  1.0333210878457741e+00f,
         1.0368102644026933e+00f,  1.0364884018886333e+00f,
         1.0365427939411784e+00f,  1.0368595402854539e+00f,
         1.0365645405660555e+00f,  1.0363938240707142e+00f,
         1.0367205578770746e+00f,  1.0365239329446050e+00f,
         1.0361531226427443e+00f,  1.0348785007827348e+00f,
         1.0042729660717318e+00f,  8.4218486432354278e-01f,
         7.3759394894801567e-01f,  6.5853154500294642e-01f,
         6.0531682444066282e-01f,  5.9549794132420741e-01f,
         5.9419261278443136e-01f,  5.6517682326634266e-01f,
         5.6061186014968556e-01f,  5.8228610381018719e-01f}};



    const std::array<float, numRgbSpectralSamples> SampledSpectrum::rgbRawIlluminationSpectrumRed_ = {{
         5.4711187157291841e-02f,  5.5609066498303397e-02f,
         6.0755873790918236e-02f,  5.6232948615962369e-02f,
         4.6169940535708678e-02f,  3.8012808167818095e-02f,
         2.4424225756670338e-02f,  3.8983580581592181e-03f,
        -5.6082252172734437e-04f,  9.6493871255194652e-04f,
         3.7341198051510371e-04f, -4.3367389093135200e-04f,
        -9.3533962256892034e-05f, -1.2354967412842033e-04f,
        -1.4524548081687461e-04f, -2.0047691915543731e-04f,
        -4.9938587694693670e-04f,  2.7255083540032476e-02f,
         1.6067405906297061e-01f,  3.5069788873150953e-01f,
         5.7357465538418961e-01f,  7.6392091890718949e-01f,
         8.9144466740381523e-01f,  9.6394609909574891e-01f,
         9.8879464276016282e-01f,  9.9897449966227203e-01f,
         9.8605140403564162e-01f,  9.9532502805345202e-01f,
         9.7433478377305371e-01f,  9.9134364616871407e-01f,
         9.8866287772174755e-01f,  9.9713856089735531e-01f}};



    const std::array<float, numRgbSpectralSamples> SampledSpectrum::rgbRawIlluminationSpectrumGreen_ = {{
         2.5168388755514630e-02f, 3.9427438169423720e-02f,
         6.2059571596425793e-03f, 7.1120859807429554e-03f,
         2.1760044649139429e-04f, 7.3271839984290210e-12f,
        -2.1623066217181700e-02f, 1.5670209409407512e-02f,
         2.8019603188636222e-03f, 3.2494773799897647e-01f,
         1.0164917292316602e+00f, 1.0329476657890369e+00f,
         1.0321586962991549e+00f, 1.0358667411948619e+00f,
         1.0151235476834941e+00f, 1.0338076690093119e+00f,
         1.0371372378155013e+00f, 1.0361377027692558e+00f,
         1.0229822432557210e+00f, 9.6910327335652324e-01f,
        -5.1785923899878572e-03f, 1.1131261971061429e-03f,
         6.6675503033011771e-03f, 7.4024315686001957e-04f,
         2.1591567633473925e-02f, 5.1481620056217231e-03f,
         1.4561928645728216e-03f, 1.6414511045291513e-04f,
        -6.4630764968453287e-03f, 1.0250854718507939e-02f,
         4.2387394733956134e-02f, 2.1252716926861620e-02f}};



    const std::array<float, numRgbSpectralSamples> SampledSpectrum::rgbRawIlluminationSpectrumBlue_ = {{
         1.0570490759328752e+00f,  1.0538466912851301e+00f,
         1.0550494258140670e+00f,  1.0530407754701832e+00f,
         1.0579930596460185e+00f,  1.0578439494812371e+00f,
         1.0583132387180239e+00f,  1.0579712943137616e+00f,
         1.0561884233578465e+00f,  1.0571399285426490e+00f,
         1.0425795187752152e+00f,  3.2603084374056102e-01f,
        -1.9255628442412243e-03f, -1.2959221137046478e-03f,
        -1.4357356276938696e-03f, -1.2963697250337886e-03f,
        -1.9227081162373899e-03f,  1.2621152526221778e-03f,
        -1.6095249003578276e-03f, -1.3029983817879568e-03f,
        -1.7666600873954916e-03f, -1.2325281140280050e-03f,
         1.0316809673254932e-02f,  3.1284512648354357e-02f,
         8.8773879881746481e-02f,  1.3873621740236541e-01f,
         1.5535067531939065e-01f,  1.4878477178237029e-01f,
         1.6624255403475907e-01f,  1.6997613960634927e-01f,
         1.5769743995852967e-01f,  1.9069090525482305e-01f}};



    /*! \brief Default constructor with uniform initialization.
     *  \param[in] value Default coefficient value.
     */
    SampledSpectrum::SampledSpectrum(float value)
    {
        for (int i = 0; i < iiit::numSpectralSamples; ++i)
        {
            coeffs[i] = value;
        }
    }



    /*! \brief Constructor with initialization from float array.
     *  \param[in] values Array of float with coefficients.
     */
    SampledSpectrum::SampledSpectrum(const float* values)
    {
        for (int i = 0; i < iiit::numSpectralSamples; ++i)
        {
            coeffs[i] = values[i];
        }
    }



    /*! \brief Constructor with initialization from float vector.
     *  \param[in] values Vector of float with coefficients.
     */
    SampledSpectrum::SampledSpectrum(const std::vector<float>& values)
    {
        for (int i = 0; i < iiit::numSpectralSamples; ++i)
        {
            coeffs[i] = values[i];
        }
    }



    /*! \brief Constructor with initialization from float array.
     *  \param[in] values Vector of float with coefficients.
     */
    SampledSpectrum::SampledSpectrum(const std::array<float, iiit::numSpectralSamples>& values)
    {
        for (int i = 0; i < iiit::numSpectralSamples; ++i)
        {
            coeffs[i] = values[i];
        }
    }



    /*! \brief Copy constructor.
    *  \param[in] other SampledSpectrum to copy.
    */
    SampledSpectrum::SampledSpectrum(const SampledSpectrum& other)
    {
        for (int i = 0; i < iiit::numSpectralSamples; ++i)
        {
            coeffs[i] = other.coeffs[i];
        }
    }



    /*! \brief Initialize CIE XYZ base functions to match inner class structure.
     */
    void SampledSpectrum::init()
    {
        yIntegral_ = 0.f;
        for (int i = 0; i < iiit::numSpectralSamples; ++i)
        {
            float lambda0 = linearInterpolate(static_cast<float>(i) / static_cast<float>(numSpectralSamples),
                static_cast<float>(sampledLambdaStart), static_cast<float>(sampledLambdaEnd));
            float lambda1 = linearInterpolate(static_cast<float>(i + 1) / static_cast<float>(numSpectralSamples),
                static_cast<float>(sampledLambdaStart), static_cast<float>(sampledLambdaEnd));
            
            x_.coeffs[i] = averageSpectrumSamples<rawNumSamples>(rawLambdaNm, rawX, lambda0, lambda1);
            y_.coeffs[i] = averageSpectrumSamples<rawNumSamples>(rawLambdaNm, rawY, lambda0, lambda1);
            z_.coeffs[i] = averageSpectrumSamples<rawNumSamples>(rawLambdaNm, rawZ, lambda0, lambda1);
            
            yIntegral_ += y_.coeffs[i];
            
            rgbReflectanceSpectrumWhite_.coeffs[i] = averageSpectrumSamples<numRgbSpectralSamples>(rgbSpectrumLambdaNm_, rgbRawReflectanceSpectrumWhite_, lambda0, lambda1);
            rgbReflectanceSpectrumCyan_.coeffs[i] = averageSpectrumSamples<numRgbSpectralSamples>(rgbSpectrumLambdaNm_, rgbRawReflectanceSpectrumCyan_, lambda0, lambda1);
            rgbReflectanceSpectrumMagenta_.coeffs[i] = averageSpectrumSamples<numRgbSpectralSamples>(rgbSpectrumLambdaNm_, rgbRawReflectanceSpectrumMagenta_, lambda0, lambda1);
            rgbReflectanceSpectrumYellow_.coeffs[i] = averageSpectrumSamples<numRgbSpectralSamples>(rgbSpectrumLambdaNm_, rgbRawReflectanceSpectrumYellow_, lambda0, lambda1);
            rgbReflectanceSpectrumRed_.coeffs[i] = averageSpectrumSamples<numRgbSpectralSamples>(rgbSpectrumLambdaNm_, rgbRawReflectanceSpectrumRed_, lambda0, lambda1);
            rgbReflectanceSpectrumGreen_.coeffs[i] = averageSpectrumSamples<numRgbSpectralSamples>(rgbSpectrumLambdaNm_, rgbRawReflectanceSpectrumGreen_, lambda0, lambda1);
            rgbReflectanceSpectrumBlue_.coeffs[i] = averageSpectrumSamples<numRgbSpectralSamples>(rgbSpectrumLambdaNm_, rgbRawReflectanceSpectrumBlue_, lambda0, lambda1);
            
            rgbIlluminationSpectrumWhite_.coeffs[i] = averageSpectrumSamples<numRgbSpectralSamples>(rgbSpectrumLambdaNm_, rgbRawIlluminationSpectrumWhite_, lambda0, lambda1);
            rgbIlluminationSpectrumCyan_.coeffs[i] = averageSpectrumSamples<numRgbSpectralSamples>(rgbSpectrumLambdaNm_, rgbRawIlluminationSpectrumCyan_, lambda0, lambda1);
            rgbIlluminationSpectrumMagenta_.coeffs[i] = averageSpectrumSamples<numRgbSpectralSamples>(rgbSpectrumLambdaNm_, rgbRawIlluminationSpectrumMagenta_, lambda0, lambda1);
            rgbIlluminationSpectrumYellow_.coeffs[i] = averageSpectrumSamples<numRgbSpectralSamples>(rgbSpectrumLambdaNm_, rgbRawIlluminationSpectrumYellow_, lambda0, lambda1);
            rgbIlluminationSpectrumRed_.coeffs[i] = averageSpectrumSamples<numRgbSpectralSamples>(rgbSpectrumLambdaNm_, rgbRawIlluminationSpectrumRed_, lambda0, lambda1);
            rgbIlluminationSpectrumGreen_.coeffs[i] = averageSpectrumSamples<numRgbSpectralSamples>(rgbSpectrumLambdaNm_, rgbRawIlluminationSpectrumGreen_, lambda0, lambda1);
            rgbIlluminationSpectrumBlue_.coeffs[i] = averageSpectrumSamples<numRgbSpectralSamples>(rgbSpectrumLambdaNm_, rgbRawIlluminationSpectrumBlue_, lambda0, lambda1);
        }
    }


    //get sampledSpectrum from sampledSpectr, identity map
    void SampledSpectrum::toSampled(SampledSpectrum& color) const
    {
       //// \todo improve efficiency
        for (int i = 0; i < iiit::numSpectralSamples; ++i)
            color.setCoeff(i, this->getCoeff(i));
    }





    /*! \brief Constructs SampleSpectrum object from sampled data.
     *  \param[in] lambdaSamples Vector of wavelengths of sampled spectrum.
     *  \param[in] valueSamples Vector of sampled spectrum values at given wavelengths.
     *
     *  Factory method to construct a SampleSpectrum object from a sampled spectrum. The method sorts
     *  the given samples if needed and interpolates the spectrum to match the inner representation of
     *  the class.
     */
    SampledSpectrum SampledSpectrum::fromSampled(const std::vector<float>& lambdaSamples, const std::vector<float>& valueSamples)
    {
        if (!std::is_sorted(lambdaSamples.begin(), lambdaSamples.end()))
        {
            // sort sampled data by wavelengths
            std::vector<float> lambdas = lambdaSamples;
            std::vector<float> values = valueSamples;
            sortSpectrum(lambdas, values);
            return fromSampled(lambdas, values);
        }
        SampledSpectrum spectrum;
        for (unsigned int i = 0; i < numSpectralSamples; ++i)
        {
            // compute average value of sampled spectral density for current wavelength range
            float lambda0 = linearInterpolate(static_cast<float>(i) / static_cast<float>(numSpectralSamples),
                static_cast<float>(sampledLambdaStart), static_cast<float>(sampledLambdaEnd));
            float lambda1 = linearInterpolate(static_cast<float>(i + 1) / static_cast<float>(numSpectralSamples),
                static_cast<float>(sampledLambdaStart), static_cast<float>(sampledLambdaEnd));
            spectrum.coeffs[i] = CoefficientSpectrum::averageSpectrumSamples(lambdaSamples, valueSamples, lambda0, lambda1);
        }
        return spectrum;
    }



    /*! \brief Constructs SampleSpectrum object from RGB data.
     *  \param[in] rgb Color in RGB representation.
     *  \param[in] type Conversion type.
     *  \return Returns sampled spectrum.
     *
     *  Factory method to construct a SampleSpectrum object from an RGB color. The spectrum is
     *  constructed to be smooth and approximately constant in the case of identical RGB values.
     */
    SampledSpectrum SampledSpectrum::fromRgb(const std::array<float, 3> &rgb, SpectrumRepresentationType type)
    {
        SampledSpectrum spectrum;
        if (type == ReflectanceSpectrum)
        {
            if (rgb[0] <= rgb[1] && rgb[0] <= rgb[2])
            {
                // red channel with minimal contribution
                spectrum += (rgbReflectanceSpectrumWhite_ * rgb[0]);
                if (rgb[1] <= rgb[2])
                {
                    spectrum += rgbReflectanceSpectrumCyan_ * (rgb[1] - rgb[0]);
                    spectrum += rgbReflectanceSpectrumBlue_ * (rgb[2] - rgb[1]);
                }
                else
                {
                    spectrum += rgbReflectanceSpectrumCyan_ * (rgb[2] - rgb[0]);
                    spectrum += rgbReflectanceSpectrumGreen_ * (rgb[1] - rgb[2]);
                }
            }
            else if (rgb[1] <= rgb[0] && rgb[1] <= rgb[2])
            {
                // green channel with minimal contribution
                spectrum += rgbReflectanceSpectrumWhite_ * rgb[1];
                if (rgb[0] <= rgb[2])
                {
                    spectrum += rgbReflectanceSpectrumMagenta_ * (rgb[0] - rgb[1]);
                    spectrum += rgbReflectanceSpectrumBlue_ * (rgb[2] - rgb[0]);
                }
                else
                {
                    spectrum += rgbReflectanceSpectrumMagenta_ * (rgb[2] - rgb[1]);
                    spectrum += rgbReflectanceSpectrumRed_ * (rgb[0] - rgb[2]);
                }
            }
            else
            {
                // blue channel with minimal contribution
                spectrum += rgbReflectanceSpectrumWhite_ * rgb[2];
                if (rgb[0] <= rgb[1])
                {
                    spectrum += rgbReflectanceSpectrumYellow_ * (rgb[0] - rgb[2]);
                    spectrum += rgbReflectanceSpectrumGreen_ * (rgb[1] - rgb[0]);
                }
                else
                {
                    spectrum += rgbReflectanceSpectrumYellow_ * (rgb[1] - rgb[2]);
                    spectrum += rgbReflectanceSpectrumRed_ * (rgb[0] - rgb[1]);
                }
            }
        }
        else
        {
            if (rgb[0] <= rgb[1] && rgb[0] <= rgb[2])
            {
                // red channel with minimal contribution
                spectrum += rgbIlluminationSpectrumWhite_ * rgb[0];
                if (rgb[1] <= rgb[2])
                {
                    spectrum += rgbIlluminationSpectrumCyan_ * (rgb[1] - rgb[0]);
                    spectrum += rgbIlluminationSpectrumBlue_ * (rgb[2] - rgb[1]);
                }
                else
                {
                    spectrum += rgbIlluminationSpectrumCyan_ * (rgb[2] - rgb[0]);
                    spectrum += rgbIlluminationSpectrumGreen_ * (rgb[1] - rgb[2]);
                }
            }
            else if (rgb[1] <= rgb[0] && rgb[1] <= rgb[2])
            {
                // green channel with minimal contribution
                spectrum += rgbIlluminationSpectrumWhite_ * rgb[1];
                if (rgb[0] <= rgb[2])
                {
                    spectrum += rgbIlluminationSpectrumMagenta_ * (rgb[0] - rgb[1]);
                    spectrum += rgbIlluminationSpectrumBlue_ * (rgb[2] - rgb[0]);
                }
                else
                {
                    spectrum += rgbIlluminationSpectrumMagenta_ * (rgb[2] - rgb[1]);
                    spectrum += rgbIlluminationSpectrumRed_ * (rgb[0] - rgb[2]);
                }
            }
            else
            {
                // blue channel with minimal contribution
                spectrum += rgbIlluminationSpectrumWhite_ * rgb[2];
                if (rgb[0] <= rgb[1])
                {
                    spectrum += rgbIlluminationSpectrumYellow_ * (rgb[0] - rgb[2]);
                    spectrum += rgbIlluminationSpectrumGreen_ * (rgb[1] - rgb[0]);
                }
                else
                {
                    spectrum += rgbIlluminationSpectrumYellow_ * (rgb[1] - rgb[2]);
                    spectrum += rgbIlluminationSpectrumRed_ * (rgb[0] - rgb[1]);
                }
            }
        }
        return spectrum;
    }



    /*! \brief Constructs SampleSpectrum object from XYZ data.
     *  \param[in] xyz Color in XYZ representation.
     *  \param[in] type Conversion type.
     *  \return Returns sampled spectrum.
     *
     *  Method converts color to RGB and uses fromRgb() method.
     */
    SampledSpectrum SampledSpectrum::fromXyz(const std::array<float, 3> &xyz, SpectrumRepresentationType type)
    {
        std::array<float, 3> rgb = {{0.f, 0.f, 0.f}};
        xyzToRgb(xyz, rgb);
        return fromRgb(rgb, type);
    }



    /*! \brief Convert to CIE XYZ color representation.
     *  \param[out] xyz CIE XYZ color.
     */
    void SampledSpectrum::toXyz(std::array<float, 3>& xyz) const
    {
        xyz[0] = 0.f;
        xyz[1] = 0.f;
        xyz[2] = 0.f;
        for (int i = 0; i < numSpectralSamples; ++i)
        {
            xyz[0] += x_.coeffs[i] * coeffs[i];
            xyz[1] += y_.coeffs[i] * coeffs[i];
            xyz[2] += z_.coeffs[i] * coeffs[i];
        }
        xyz[0] /= yIntegral_;
        xyz[1] /= yIntegral_;
        xyz[2] /= yIntegral_;
    }



    /*! \brief Convert spectrum to sRGB color representation.
     *  \param[out] rgb sRGB color.
     */
    void SampledSpectrum::toRgb(std::array<float, 3>& rgb) const
    {
        std::array<float, 3> xyz = {{0.f, 0.f, 0.f}};
        toXyz(xyz);
        xyzToRgb(xyz, rgb);
    }



    /*! \brief Get wavelength of coefficient in nanometers.
     *  \param[in] index Index of coefficient.
     *  \returns Wavelength in nanometers.
     */
    int SampledSpectrum::getWaveLengthNm(int index)
    {
        return index * (sampledLambdaEnd - sampledLambdaStart) / numSpectralSamples + sampledLambdaStart + (sampledLambdaEnd - sampledLambdaStart) / numSpectralSamples / 2;
    }



    /*! \brief Assignment operator.
     *  \param[in] rhs Spectrum to assign.
     *  \return Returns reference to result.
     */
    SampledSpectrum& SampledSpectrum::operator=(const SampledSpectrum& rhs)
    {
        for (int i = 0; i < numSpectralSamples; ++i)
        {
            coeffs[i] = rhs.coeffs[i];
        }
        return *this;
    }



    /*! \brief Add operator.
     *  \param[in] rhs Spectrum to add.
     *  \return Returns reference to summation result.
     */
    SampledSpectrum& SampledSpectrum::operator+=(const SampledSpectrum& rhs)
    {
        for (int i = 0; i < numSpectralSamples; ++i)
        {
            coeffs[i] += rhs.coeffs[i];
        }
        return *this;
    }



    /*! \brief Add operator.
     *  \param[in] rhs Spectrum to add.
     *  \return Returns summation result.
     */
    SampledSpectrum SampledSpectrum::operator+(const SampledSpectrum& rhs) const
    {
        SampledSpectrum res = *this;
        for (int i = 0; i < numSpectralSamples; ++i)
        {
            res.coeffs[i] += rhs.coeffs[i];
        }
        return res;
    }



    /*! \brief Unary minus operator.
     *  \return Returns negated result.
     */
    SampledSpectrum SampledSpectrum::operator-(void) const
    {
        SampledSpectrum res = *this;
        for (int i = 0; i < numSpectralSamples; ++i)
        {
            res.coeffs[i] = -coeffs[i];
        }
        return res;
    }



    /*! \brief Minus operator.
     *  \param[in] rhs Spectrum to subtract.
     *  \return Returns reference to subtraction result.
     */
    SampledSpectrum& SampledSpectrum::operator-=(const SampledSpectrum& rhs)
    {
        for (int i = 0; i < numSpectralSamples; ++i)
        {
            coeffs[i] -= rhs.coeffs[i];
        }
        return *this;
    }



    /*! \brief Minus operator.
     *  \param[in] rhs Spectrum to subtract.
     *  \return Returns subtraction result.
     */
    SampledSpectrum SampledSpectrum::operator-(const SampledSpectrum& rhs) const
    {
        SampledSpectrum res = *this;
        for (int i = 0; i < numSpectralSamples; ++i)
        {
            res.coeffs[i] -= rhs.coeffs[i];
        }
        return res;
    }



    /*! \brief Scalar multiplication operator.
     *  \param[in] factor Scalar to multiply.
     *  \return Returns reference to multiplication result.
     */
    SampledSpectrum& SampledSpectrum::operator*=(float factor)
    {
        for (int i = 0; i < numSpectralSamples; ++i)
        {
            coeffs[i] *= factor;
        }
        return *this;
    }
    
    
    
    /*! \brief Scalar multiplication operator.
     *  \param[in] factor Scalar to multiply.
     *  \return Returns multiplication result.
     */
    SampledSpectrum SampledSpectrum::operator*(float factor) const
    {
        SampledSpectrum res = *this;
        for (int i = 0; i < numSpectralSamples; ++i)
        {
            res.coeffs[i] *= factor;
        }
        return res;
    }
    
    
    
    /*! \brief Element-wise multiplication operator.
     *  \param[in] rhs Spectrum to multiply.
     *  \return Returns reference to multiplication result.
     */
    SampledSpectrum& SampledSpectrum::operator*=(const SampledSpectrum& rhs)
    {
        for (int i = 0; i < numSpectralSamples; ++i)
        {
            coeffs[i] *= rhs.coeffs[i];
        }
        return *this;
    }
    
    
    
    /*! \brief Element-wise multiplication operator.
     *  \param[in] rhs Spectrum to multiply.
     *  \return Returns multiplication result.
     */
    SampledSpectrum SampledSpectrum::operator*(const SampledSpectrum& rhs) const
    {
        SampledSpectrum res = *this;
        for (int i = 0; i < numSpectralSamples; ++i)
        {
            res.coeffs[i] *= rhs.coeffs[i];
        }
        return res;
    }
    
    
    
    /*! \brief Scalar division operator.
     *  \param[in] factor Scalar to divide.
     *  \return Returns reference to division result.
     */
    SampledSpectrum& SampledSpectrum::operator/=(float factor)
    {
        for (int i = 0; i < numSpectralSamples; ++i)
        {
            coeffs[i] /= factor;
        }
        return *this;
    }
    
    
    
    /*! \brief Scalar division operator.
     *  \param[in] factor Scalar to divide.
     *  \return Returns division result.
     */

    SampledSpectrum SampledSpectrum::operator/(float factor) const
    {
        SampledSpectrum res = *this;
        for (int i = 0; i < numSpectralSamples; ++i)
        {
            res.coeffs[i] /= factor;
        }
        return res;
    }
    
    
    
    /*! \brief Element-wise division operator.
     *  \param[in] rhs Spectrum to divide.
     *  \return Returns reference to division result.
     */
    SampledSpectrum& SampledSpectrum::operator/=(const SampledSpectrum& rhs)
    {
        for (int i = 0; i < numSpectralSamples; ++i)
        {
            coeffs[i] /= rhs.coeffs[i];
        }
        return *this;
    }
    
    
    
    /*! \brief Element-wise division operator.
     *  \param[in] rhs Spectrum to divide.
     *  \return Returns division result.
     */
    SampledSpectrum SampledSpectrum::operator/(const SampledSpectrum& rhs) const
    {
        SampledSpectrum res = *this;
        for (int i = 0; i < numSpectralSamples; ++i)
        {
            res.coeffs[i] /= rhs.coeffs[i];
        }
        return res;
    }
    
    
    
    /*! \brief Raise to power.
     *  \param[in] p Power.
     *  \return Returns result.
     */
    SampledSpectrum SampledSpectrum::powc(float p) const
    {
        SampledSpectrum res = *this;
        for (int i = 0; i < numSpectralSamples; ++i)
        {
            res.coeffs[i] = pow(coeffs[i], p);
        }
        return res;
    }
    
} // end of namespace iiit
