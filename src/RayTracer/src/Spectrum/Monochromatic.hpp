// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Monochromatic.hpp
 *  \author Thomas Nuernberg
 *  \date 07.09.2016
 *  \brief Definition of Monochromatic.
 *
 *  Definition of Monochromatic class, that represents monochromatic light.
 */

#ifndef __MONOCHROMATIC_HPP__
#define __MONOCHROMATIC_HPP__

#include <vector>

#include "CoefficientSpectrum.hpp"
#include "SampledSpectrum.hpp"



namespace iiit
{
    
    /*! \brief Represents monochromatic light of wave length 555 nm.
     */
    class Monochromatic : public CoefficientSpectrum<1>
    {
    public:
        Monochromatic(float value = 0.f);
        
        // factory methods
        static Monochromatic fromSampled(const std::vector<float>& lambdaSamples, const std::vector<float>& valueSamples);
        static Monochromatic fromRgb(const std::array<float, 3>& rgb, SpectrumRepresentationType type);
        static Monochromatic fromXyz(const std::array<float, 3>& xyz, SpectrumRepresentationType type = ReflectanceSpectrum);
        
        // conversion methods
        void toXyz(std::array<float, 3>& xyz) const;
        void toRgb(std::array<float, 3>& rgb) const;
        void toSampled(SampledSpectrum& color) const;

        int getWaveLengthNm(int index);
        
        // operators
        Monochromatic& operator=(const Monochromatic& rhs);
        Monochromatic& operator+=(const Monochromatic& rhs);
        Monochromatic operator+(const Monochromatic& rhs) const;
        Monochromatic operator-(void) const;
        Monochromatic& operator-=(const Monochromatic& rhs);
        Monochromatic operator-(const Monochromatic& rhs) const;
        Monochromatic& operator*=(float factor);
        Monochromatic operator*(float factor) const;
        Monochromatic& operator*=(const Monochromatic& rhs);
        Monochromatic operator*(const Monochromatic& rhs) const;
        Monochromatic& operator/=(float factor);
        Monochromatic operator/(float factor) const;
        Monochromatic& operator/=(const Monochromatic& rhs);
        Monochromatic operator/(const Monochromatic& rhs) const;
        Monochromatic powc(float p) const;
        
    private:
        static float interpolateSpectrumSamples(const std::vector<float>& lambdas, const std::vector<float>& values, float lambda);
        
    };

} // end of namespace iiit

#endif // __MONOCHROMATIC_HPP__
