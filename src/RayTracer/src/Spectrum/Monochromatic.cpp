// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Monochromatic.cpp
 *  \author Thomas Nuernberg
 *  \date 07.09.2016
 *  \brief Definition of Monochromatic.
 *
 *  Implementation of Monochromatic class, that represents monochromatic light.
 */

#include "Monochromatic.hpp"

#include <math.h>

#include "SampledSpectrum.hpp"



namespace iiit
{

    /*! \brief Default constructor with uniform initialization.
     *  \param[in] value Coefficient value.
     */
    Monochromatic::Monochromatic(float value)
    {
        coeffs[0] = value;
    }



    /*! \brief Constructs Monochromatic object from sampled data.
     *  \param[in] lambdaSamples Vector of wavelengths of sampled spectrum.
     *  \param[in] valueSamples Vector of sampled spectrum values at given wavelengths.
     *
     *  Factory method to construct a Monochromatic object from a sampled spectrum. The method sorts
     *  the given samples if needed and interpolates the spectrum to match the inner representation of
     *  the class.
     */
    Monochromatic Monochromatic::fromSampled(const std::vector<float>& lambdaSamples, const std::vector<float>& valueSamples)
    {
        if (!std::is_sorted(lambdaSamples.begin(), lambdaSamples.end()))
        {
            // sort sampled data by wavelengths
            std::vector<float> lambdas = lambdaSamples;
            std::vector<float> values = valueSamples;
            sortSpectrum(values, lambdas);
            return fromSampled(values, lambdas);
        }
        std::array<float, 3> xyz = {{0.f, 0.f, 0.f}};
        float yIntegral = 0.f;
        for (int i = 0; i < rawNumSamples; ++i)
        {
            yIntegral += rawY[i];
            
            float value = interpolateSpectrumSamples(lambdaSamples, valueSamples, rawLambdaNm[i]);
            xyz[0] += value * rawX[i];
            xyz[1] += value * rawY[i];
            xyz[2] += value * rawZ[i];
        }
        xyz[0] /= yIntegral;
        xyz[1] /= yIntegral;
        xyz[2] /= yIntegral;
        
        return fromXyz(xyz);
    }



    /*! \brief Constructs Monochromatic object from RGB data.
     *  \param[in] rgb Color in RGB representation.
     *  \param[in] type Conversion type.
     *  \return Returns color in RGB representation.
     */
    Monochromatic Monochromatic::fromRgb(const std::array<float, 3>& rgb, SpectrumRepresentationType type)
    {
        float meanValue = (rgb[0] + rgb[1] + rgb[2]) / 3.f;
        return Monochromatic(meanValue);
    }



    /*! \brief Constructs Monochromatic object from XYZ data.
     *  \param[in] xyz Color in XYZ representation.
     *  \param[in] type Conversion type.
     *  \return Returns color in Monochromatic representation.
     */
    Monochromatic Monochromatic::fromXyz(const std::array<float, 3> &xyz, SpectrumRepresentationType type)
    {
        std::array<float, 3> rgb = {{0.f, 0.f, 0.f}};
        xyzToRgb(xyz, rgb);
        return fromRgb(rgb, type);
    }



    /*! \brief Convert to CIE XYZ color representation.
     *  \param[out] xyz CIE XYZ color.
     */
    void Monochromatic::toXyz(std::array<float, 3>& xyz) const
    {
        std::array<float, 3> rgb = {{0.f, 0.f, 0.f}};
        rgb[0] = coeffs[0];
        rgb[1] = coeffs[0];
        rgb[2] = coeffs[0];
        rgbToXyz(rgb, xyz);
    }



    /*! \brief Convert to sRGB color representation.
     *  \param[out] rgb sRGB color.
     */
    void Monochromatic::toRgb(std::array<float, 3>& rgb) const
    {
        rgb[0] = coeffs[0];
        rgb[1] = coeffs[0];
        rgb[2] = coeffs[0];
    }



    /*! \brief Convert to sampledSpectrum color representation.
     *  \param[out] sampled monochromatic spectrum.
     */
    void Monochromatic::toSampled(SampledSpectrum& color) const
    {
        float tmp = this->getCoeff(0);
        // Copy Monochromatic entry to sampledSpectrum color
        for (int i = 0; i < iiit::numSpectralSamples; ++i)
        {
            color.setCoeff(i, tmp);
        }
    }



    /*! \brief Get wavelength of coefficient in nanometers.
     *  \param[in] index Index of coefficient.
     *  \returns Wavelength of 555 nanometers.
     */
    int Monochromatic::getWaveLengthNm(int index)
    {
        return 555; // return wave length of maximum spectral sensitivity.
    }
    
    
    
    /*! \brief Assignment operator.
     *  \param[in] rhs Spectrum to assign.
     *  \return Returns reference to result.
     */
    Monochromatic& Monochromatic::operator=(const Monochromatic& rhs)
    {

        coeffs[0] = rhs.coeffs[0];
        return *this;
    }
    
    
    
    /*! \brief Add operator.
     *  \param[in] rhs Spectrum to add.
     *  \return Returns reference to summation result.
     */
    Monochromatic& Monochromatic::operator+=(const Monochromatic& rhs)
    {
        coeffs[0] += rhs.coeffs[0];
        return *this;
    }
    
    
    
    /*! \brief Add operator.
     *  \param[in] rhs Spectrum to add.
     *  \return Returns summation result.
     */
    Monochromatic Monochromatic::operator+(const Monochromatic& rhs) const
    {
        Monochromatic res = *this;
        res.coeffs[0] += rhs.coeffs[0];
        return res;
    }
    
    
    
    /*! \brief Unary minus operator.
     *  \return Returns negated result.
     */
    Monochromatic Monochromatic::operator-(void) const
    {
        Monochromatic res = *this;
        res.coeffs[0] = -coeffs[0];
        return res;
    }
    
    
    
    /*! \brief Minus operator.
     *  \param[in] rhs Spectrum to subtract.
     *  \return Returns reference to subtraction result.
     */
    Monochromatic& Monochromatic::operator-=(const Monochromatic& rhs)
    {
        coeffs[0] -= rhs.coeffs[0];
        return *this;
    }
    
    
    
    /*! \brief Minus operator.
     *  \param[in] rhs Spectrum to subtract.
     *  \return Returns subtraction result.
     */
    Monochromatic Monochromatic::operator-(const Monochromatic& rhs) const
    {
        Monochromatic res = *this;
        res.coeffs[0] -= rhs.coeffs[0];
        return res;
    }
    
    
    
    /*! \brief Scalar multiplication operator.
     *  \param[in] factor Scalar to multiply.
     *  \return Returns reference to multiplication result.
     */
    Monochromatic& Monochromatic::operator*=(float factor)
    {
        coeffs[0] *= factor;
        return *this;
    }
    
    
    
    /*! \brief Scalar multiplication operator.
     *  \param[in] factor Scalar to multiply.
     *  \return Returns multiplication result.
     */
    Monochromatic Monochromatic::operator*(float factor) const
    {
        Monochromatic res = *this;
        res.coeffs[0] *= factor;
        return res;
    }
    
    
    
    /*! \brief Element-wise multiplication operator.
     *  \param[in] rhs Spectrum to multiply.
     *  \return Returns reference to multiplication result.
     */
    Monochromatic& Monochromatic::operator*=(const Monochromatic& rhs)
    {
        coeffs[0] *= rhs.coeffs[0];
        return *this;
    }
    
    
    
    /*! \brief Element-wise multiplication operator.
     *  \param[in] rhs Spectrum to multiply.
     *  \return Returns multiplication result.
     */
    Monochromatic Monochromatic::operator*(const Monochromatic& rhs) const
    {
        Monochromatic res = *this;
        res.coeffs[0] *= rhs.coeffs[0];
        return res;
    }
    
    
    
    /*! \brief Scalar division operator.
     *  \param[in] factor Scalar to divide.
     *  \return Returns reference to division result.
     */
    Monochromatic& Monochromatic::operator/=(float factor)
    {
        coeffs[0] /= factor;
        return *this;
    }
    
    
    
    /*! \brief Scalar division operator.
     *  \param[in] factor Scalar to divide.
     *  \return Returns division result.
     */
    
    Monochromatic Monochromatic::operator/(float factor) const
    {
        Monochromatic res = *this;
        res.coeffs[0] /= factor;
        return res;
    }
    
    
    
    /*! \brief Element-wise division operator.
     *  \param[in] rhs Spectrum to divide.
     *  \return Returns reference to division result.
     */
    Monochromatic& Monochromatic::operator/=(const Monochromatic& rhs)
    {
        coeffs[0] /= rhs.coeffs[0];
        return *this;
    }
    
    
    
    /*! \brief Element-wise division operator.
     *  \param[in] rhs Spectrum to divide.
     *  \return Returns division result.
     */
    Monochromatic Monochromatic::operator/(const Monochromatic& rhs) const
    {
        Monochromatic res = *this;
        res.coeffs[0] /= rhs.coeffs[0];
        return res;
    }
    
    
    
    /*! \brief Raise to power.
     *  \param[in] p Power.
     *  \return Returns result.
     */
    Monochromatic Monochromatic::powc(float p) const
    {
        Monochromatic res = *this;
        res.coeffs[0] = pow(coeffs[0], p);
        return res;
    }



    /*! \brief Interpolates spectral value at given wavelength.
     *  \param[in] lambdas Sampled wavelength data.
     *  \param[in] values Sampled spectral data.
     *  \param[in] lambda Wavelength of interest.
     *  \return Return interpolated spectral value.
     */
    float Monochromatic::interpolateSpectrumSamples(const std::vector<float>& lambdas, const std::vector<float>& values, float lambda)
    {
        if (lambda < lambdas.front())
        {
            return values.front();
        }
        
        if (lambda > lambdas.back())
        {
            return values.back();
        }
        
        for (unsigned int i = 0; i < lambdas.size() - 1; ++i)
        {
            if (lambda >= lambdas[i] && lambda <= lambdas[i + 1])
            {
                return linearInterpolate((lambda - lambdas[i]) / (lambdas[i + 1] - lambdas[i]), values[i], values[i + 1]);
            }
        }
        return 0.f;
    }

} // end of namespace iiit
