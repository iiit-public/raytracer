// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Whitted.hpp
 *  \author Thomas Nuernberg
 *  \date 21.06.2016
 *  \brief Definition of class Whitted.
 */

#ifndef __WHITTED_HPP__
#define __WHITTED_HPP__

#include <memory>

#include "Tracer.hpp"

#include "Scene.hpp"
#include "Utilities/Ray.hpp"
#include "Utilities/Constants.h"



namespace iiit
{

    /*! \brief Class used for calculating interactions of rays with the scene with reflection.
     */
    template <class SpectrumType>
    class Whitted : public Tracer<SpectrumType>
    {
    public:
        Whitted();
        Whitted(bool tracesVignetting);
        Whitted(std::weak_ptr<const Scene<SpectrumType> > scene);
        Whitted(std::weak_ptr<const Scene<SpectrumType> > scene, bool tracesVignetting);
        virtual ~Whitted();
        virtual SpectrumType traceRay(const Ray& ray, const int depth) const;
        virtual SpectrumType traceRay(const Ray& ray, float& tMin, const int depth) const;
        virtual Whitted<SpectrumType>* clone() const;
        
        int getMaxDepth() const;
        void setMaxDepth(int maxDepth);
        
    private:
        int maxDepth_; ///< Maximum recursion depth for reflections.
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Whitted<SpectrumType>::Whitted()
        : Tracer<SpectrumType>()
        , maxDepth_(1)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] tracesVignetting Bool whether traces traces natrual vignetting.
     */
    template <class SpectrumType>
    Whitted<SpectrumType>::Whitted(bool tracesVignetting)
        : Tracer<SpectrumType>(false, tracesVignetting) // Does not trace property, traces vignetting
        , maxDepth_(1)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] scene Scene object.
     */
    template <class SpectrumType>
    Whitted<SpectrumType>::Whitted(std::weak_ptr<const Scene<SpectrumType> > scene)
        : Tracer<SpectrumType>(scene)
        , maxDepth_(1)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] scene Scene object.
     *  \param[in] tracesVignetting Bool whether traces traces natrual vignetting.
     */
    template <class SpectrumType>
    Whitted<SpectrumType>::Whitted(std::weak_ptr<const Scene<SpectrumType> > scene, bool tracesVignetting)
        : Tracer<SpectrumType>(scene, false, tracesVignetting) // Does not trace property, traces vignetting
        , maxDepth_(1)
    {
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    Whitted<SpectrumType>::~Whitted()
    {
    }


    /*! \brief Calculating interactions of ray with the scene.
     *  \param[in] ray Ray to calculate.
     *  \param[in] depth Recursion depth of ray bounces.
     *  \return Resulting color.
     */
    template <class SpectrumType>
    SpectrumType Whitted<SpectrumType>::traceRay(const Ray& ray, const int depth) const
    {
        if (depth > maxDepth_)
        {
            return SpectrumType(0.f);
        }
        else
        {
            double minRayParameter = std::numeric_limits<double>::infinity();
            ShadingData<SpectrumType> shadingData(this->scene_);
            std::shared_ptr<const Scene<SpectrumType> > scene = this->scene_.lock();
            
            // iterate over objects in scene
            for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = scene->objects_.begin(); it != scene->objects_.end(); ++it)
            {
                // check for intersection
                double rayParameter;
                ShadingData<SpectrumType> tmpShadingData(scene);
                bool hit = (*it)->hit(ray, rayParameter, tmpShadingData);
                
                // check for nearest intersection
                if (hit && rayParameter < minRayParameter && rayParameter > EPS)
                {
                    minRayParameter = rayParameter;
                    shadingData = tmpShadingData;
                    shadingData.normal_.normalize();
                }
            }
            // evaluate intersection
            if (shadingData.hitAnObject_)
            {
                shadingData.depth_ = depth;
                shadingData.ray_ = ray;
                return shadingData.material_->shade(shadingData);
            }
            else
            {
                // get background color
                return scene->backgroundColor_;
            }
        }
    }
    
    
    
    /*! \brief Calculating interactions of ray with the scene.
     *  \param[in] ray Ray to calculate.
     *  \param[out] tMin Ray parameter at nearest hit point.
     *  \param[in] depth Recursion depth of ray bounces.
     *  \return Resulting color.
     */
    template <class SpectrumType>
    SpectrumType Whitted<SpectrumType>::traceRay(const Ray& ray, float& tMin, const int depth) const
    {
        if (depth > maxDepth_)
        {
            return SpectrumType(0.f);
        }
        else
        {
            double minRayParameter = std::numeric_limits<double>::infinity();
            ShadingData<SpectrumType> shadingData(this->scene_);
            std::shared_ptr<const Scene<SpectrumType> > scene = this->scene_.lock();
            
            // iterate over objects in scene
            for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = scene->objects_.begin(); it != scene->objects_.end(); ++it)
            {
                // check for intersection
                double rayParameter;
                ShadingData<SpectrumType> tmpShadingData(scene);
                bool hit = (*it)->hit(ray, rayParameter, tmpShadingData);
                
                // check for nearest intersection
                if (hit && rayParameter < minRayParameter && rayParameter > EPS)
                {
                    minRayParameter = rayParameter;
                    shadingData = tmpShadingData;
                    shadingData.normal_.normalize();
                }
            }
            // evaluate intersection
            if (shadingData.hitAnObject_)
            {
                shadingData.depth_ = depth;
                shadingData.ray_ = ray;
                tMin = static_cast<float>(minRayParameter);
                return shadingData.material_->shade(shadingData);
            }
            else
            {
                tMin = static_cast<float>(HUGE_VALUE);
                
                // get background color
                return scene->backgroundColor_;
            }
        }
    }
    
    
    
    /*! \brief Deep copies the tracer.
     *  \return Returns the copied tracer.
     */
    template <class SpectrumType>
    Whitted<SpectrumType>* Whitted<SpectrumType>::clone() const
    {
        Whitted<SpectrumType>* whitted = new Whitted<SpectrumType>(this->tracesVignetting());
        whitted->setMaxDepth(maxDepth_);
        return whitted;
    }
    
    
    
    /*! \brief Get maximum recursion depth.
     *  \return Returns maximum recursion depth.
     */
    template <class SpectrumType>
    int Whitted<SpectrumType>::getMaxDepth() const
    {
        return maxDepth_;
    }
    
    
    
    /*! \brief Set maximum recursion depth.
     *  \param[in] maxDepth Maximum recursion depth.
     */
    template <class SpectrumType>
    void Whitted<SpectrumType>::setMaxDepth(int maxDepth)
    {
        maxDepth_ = maxDepth;
    }
    
} // end of namespace iiit

#endif // __WHITTED_HPP__
