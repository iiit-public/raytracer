// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file CoordinateTracer.hpp
 *  \author David Uhlig
 *  \date 20.03.2020
 *  \brief Definition of class CoordinateTracer.
 */

#ifndef __COORDINATETRACER_HPP__
#define __COORDINATETRACER_HPP__

#include <memory>

#include "Tracer.hpp"

#include "Scene.hpp"
#include "Utilities/Ray.hpp"



namespace iiit
{

    /*! \brief Class used for logging depth information.
     */
    template <class SpectrumType>
    class CoordinateTracer : public Tracer<SpectrumType>
    {
    public:
        CoordinateTracer();
        CoordinateTracer(std::weak_ptr<const Scene<SpectrumType> > scene, int coordinateType = 2, int reflectionDepth = 1);
        virtual ~CoordinateTracer();
        virtual SpectrumType traceRay(const Ray& ray, const int depth) const;
        virtual SpectrumType traceRay(const Ray& ray, float& tMin, const int depth) const;
        virtual CoordinateTracer<SpectrumType>* clone() const;

        int getMaxDepth() const;
        void setReflectionDepth(int maxDepth);
        void setCoordinateType(int coordType);

    private:
        int coordinateType_; ///< Type of coordinate. '2' for local 2D coordinate or '3' for global 3D coordinate.
        int reflectionDepth_; ///< Maximum recursion depth for reflections.
    };

    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    CoordinateTracer<SpectrumType>::CoordinateTracer()
        : Tracer<SpectrumType>(true, false) // traces property, does not trace vignetting
        , coordinateType_(2)
        , reflectionDepth_(1)
    {
    }


    /*! \brief Constructor with initialization.
     *  \param[in] scene Scene object.
     *  \param[in] coordinateType Type of coordinate that is returned.
     *  \param[in] reflectionDepth Recursion depth for reflections at which the object property should be evaluated.
     */
    template <class SpectrumType>
    CoordinateTracer<SpectrumType>::CoordinateTracer(std::weak_ptr<const Scene<SpectrumType> > scene, int coordinateType, int reflectionDepth)
        : Tracer<SpectrumType>(scene, true, false) // traces property, does not trace vignetting
        , coordinateType_(coordinateType)
        , reflectionDepth_(reflectionDepth)
    {
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    CoordinateTracer<SpectrumType>::~CoordinateTracer()
    {
    }


    /*! /brief Calculating interactions of ray with the scene.
     *  \param[in] ray Ray to calculate.
     *  \param[in] depth Recursion depth of ray bounces.
     *  \return Resulting color.
     */
    template <class SpectrumType>
    SpectrumType CoordinateTracer<SpectrumType>::traceRay(const Ray& ray, const int depth) const
    {
        double minRayParameter = std::numeric_limits<double>::infinity();
        ShadingData<SpectrumType> shadingData(this->scene_);
        std::shared_ptr<const Scene<SpectrumType> > scene = this->scene_.lock();
        
        // iterate over objects in scene
        for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = scene->objects_.begin(); it != scene->objects_.end(); ++it)
        {
            // check for intersection
            double rayParameter;
            ShadingData<SpectrumType> tmpShadingData(scene);
            bool hit = (*it)->hit(ray, rayParameter, tmpShadingData);
            
            // check for nearest intersection
            if (hit && rayParameter < minRayParameter && rayParameter > EPS)
            {
                minRayParameter = rayParameter;
                shadingData = tmpShadingData;
                shadingData.normal_.normalize();
            }
        }
        
        // evaluate intersection
        if (shadingData.hitAnObject_)
        {   
            if (depth == reflectionDepth_)
            {
                std::array<float, 3> coordinate = { {0.f, 0.f, 0.f} };
                if (coordinateType_ == 2)
                {
                    coordinate[0] = static_cast<float>(shadingData.localHitPoint2D_.x_);
                    coordinate[1] = static_cast<float>(shadingData.localHitPoint2D_.y_);
                }
                else if (coordinateType_ == 3)
                {
                    coordinate[0] = static_cast<float>(shadingData.hitPoint_.x_);
                    coordinate[1] = static_cast<float>(shadingData.hitPoint_.y_);
                    coordinate[2] = static_cast<float>(shadingData.hitPoint_.z_);
                }
                SpectrumType color = SpectrumType::fromRgb(coordinate, SpectrumRepresentationType::IlluminationSpectrum);
                return color;
            }
            else // go deeper
            {
                shadingData.depth_ = depth;
                shadingData.ray_ = ray;

                // reflect ray (perfect reflection)
                Vector3D outgoing(-(shadingData.ray_.direction_)); 
                Vector3D normal(shadingData.normal_);
                float dotProduct = static_cast<float>(normal * outgoing);
                Vector3D incoming(-outgoing + normal * 2.0 * dotProduct);

                Ray reflectedRay(shadingData.hitPoint_, incoming);

                // follow reflected ray
                return shadingData.scene_.lock()->getTracer().lock()->traceRay(reflectedRay, shadingData.depth_ + 1);
            }
        }
        else
        {
            // get background color
            return SpectrumType(0.f);
        }
    }



    /*! /brief Calculating interactions of ray with the scene.
     *  \param[in] ray Ray to calculate.
     *  \param[out] tMin Ray parameter at nearest hit point.
     *  \param[in] depth Recursion depth of ray bounces.
     *  \return Resulting color.
     */
    template <class SpectrumType>
    SpectrumType CoordinateTracer<SpectrumType>::traceRay(const Ray& ray, float& tMin, const int depth) const
    {
        double minRayParameter = std::numeric_limits<double>::infinity();
        ShadingData<SpectrumType> shadingData(this->scene_);
        std::shared_ptr<const Scene<SpectrumType> > scene = this->scene_.lock();

        // iterate over objects in scene
        for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = scene->objects_.begin(); it != scene->objects_.end(); ++it)
        {
            // check for intersection
            double rayParameter;
            ShadingData<SpectrumType> tmpShadingData(scene);
            bool hit = (*it)->hit(ray, rayParameter, tmpShadingData);

            // check for nearest intersection
            if (hit && rayParameter < minRayParameter && rayParameter > EPS)
            {
                minRayParameter = rayParameter;
                shadingData = tmpShadingData;
                shadingData.normal_.normalize();
            }
        }

        // evaluate intersection
        if (shadingData.hitAnObject_)
        {
            tMin = static_cast<float>(minRayParameter);
            if (depth == reflectionDepth_)
            {
                std::array<float, 3> coordinate = { {0.f, 0.f, 0.f} };
                if (coordinateType_ == 2)
                {
                    coordinate[0] = static_cast<float>(shadingData.localHitPoint2D_.x_);
                    coordinate[1] = static_cast<float>(shadingData.localHitPoint2D_.y_);
                }
                else if (coordinateType_ == 3)
                {
                    coordinate[0] = static_cast<float>(shadingData.hitPoint_.x_);
                    coordinate[1] = static_cast<float>(shadingData.hitPoint_.y_);
                    coordinate[2] = static_cast<float>(shadingData.hitPoint_.z_);
                }
                SpectrumType color = SpectrumType::fromRgb(coordinate, SpectrumRepresentationType::IlluminationSpectrum);
                return color;
            }
            else // go deeper
            {
                shadingData.depth_ = depth;
                shadingData.ray_ = ray;

                // reflect ray (perfect reflection)
                Vector3D outgoing(-(shadingData.ray_.direction_));
                Vector3D normal(shadingData.normal_);
                float dotProduct = static_cast<float>(normal * outgoing);
                Vector3D incoming(-outgoing + normal * 2.0 * dotProduct);

                Ray reflectedRay(shadingData.hitPoint_, incoming);

                // follow reflected ray
                return shadingData.scene_.lock()->getTracer().lock()->traceRay(reflectedRay, shadingData.depth_ + 1);
            }
        }
        else
        {
            tMin = static_cast<float>(HUGE_VALUE);

            // get background color
            return SpectrumType(0.f);
        }
    }



    /*! \brief Deep copies the tracer.
     *  \return Returns the copied tracer.
     */
    template <class SpectrumType>
    CoordinateTracer<SpectrumType>* CoordinateTracer<SpectrumType>::clone() const
    {
        CoordinateTracer<SpectrumType>* coordinateTracer = new CoordinateTracer<SpectrumType>();
        coordinateTracer->setCoordinateType(coordinateType_);
        coordinateTracer->setReflectionDepth(reflectionDepth_);
        return coordinateTracer;
    }



    /*! \brief Set minimum displayable distance.
     *  \param[in] minDistance Minimum displayable dinstance.
     */
    template <class SpectrumType>
    void CoordinateTracer<SpectrumType>::setCoordinateType(int coordType)
    {
        coordinateType_ = coordType;
    }



    /*! \brief Get maximum recursion depth.
 *  \return Returns maximum recursion depth.
 */
    template <class SpectrumType>
    int CoordinateTracer<SpectrumType>::getMaxDepth() const
    {
        return reflectionDepth_;
    }



    /*! \brief Set maximum recursion depth.
     *  \param[in] maxDepth Maximum recursion depth.
     */
    template <class SpectrumType>
    void CoordinateTracer<SpectrumType>::setReflectionDepth(int maxDepth)
    {
        reflectionDepth_ = maxDepth;
    }



} // end of namespace iiit

#endif // __COORDINATETRACER_HPP__
