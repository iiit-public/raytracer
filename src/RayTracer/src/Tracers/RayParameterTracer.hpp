// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file RayParameterTracer.hpp
 *  \author Davie Uhlig
 *  \date 06.04.2020
 *  \brief Definition of class RayParameterTracer.
 */


#ifndef __RAYPARAMETERTRACER_HPP__
#define __RAYPARAMETERTRACER_HPP__

#include <memory>

#include "Tracer.hpp"

#include "Scene.hpp"
#include "Utilities/Ray.hpp"



namespace iiit
{

    /*! \brief Class used for logging ray information.
     */
    template <class SpectrumType>
    class RayParameterTracer : public Tracer<SpectrumType>
    {
    public:
        RayParameterTracer();
        RayParameterTracer(std::weak_ptr<const Scene<SpectrumType> > scene);
        virtual ~RayParameterTracer();
        virtual SpectrumType traceRay(const Ray& ray, const int depth) const;
        virtual SpectrumType traceRay(const Ray& ray, float& tMin, const int depth) const;
        virtual RayParameterTracer<SpectrumType>* clone() const;
};

    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    RayParameterTracer<SpectrumType>::RayParameterTracer()
        : Tracer<SpectrumType>(true, false) // traces property, does not trace vignetting
    {
    }
    
    
    /*! \brief Constructor with initialization.
     *  \param[in] scene Scene object.
     */
    template <class SpectrumType>
    RayParameterTracer<SpectrumType>::RayParameterTracer(std::weak_ptr<const Scene<SpectrumType> > scene)
        : Tracer<SpectrumType>(scene, true, false) // traces property, does not trace vignetting
    {
    }    
    
    
    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    RayParameterTracer<SpectrumType>::~RayParameterTracer()
    {
    }
    
    
    /*! /brief Calculating interactions of ray with the scene.
     *  \param[in] ray Ray to calculate.
     *  \param[in] depth Recursion depth of ray bounces.
     *  \return Resulting color.
     */
    template <class SpectrumType>
    SpectrumType RayParameterTracer<SpectrumType>::traceRay(const Ray& ray, const int depth) const
    {
        SpectrumType color;

        // origin
        color.setCoeff(0, static_cast<float>(ray.origin_.x_));
        color.setCoeff(1, static_cast<float>(ray.origin_.y_));
        color.setCoeff(2, static_cast<float>(ray.origin_.z_));
        // direction
        color.setCoeff(3, static_cast<float>(ray.direction_.x_));
        color.setCoeff(4, static_cast<float>(ray.direction_.y_));
        color.setCoeff(5, static_cast<float>(ray.direction_.z_));

        return color;
    }
    
    
    
    /*! /brief Calculating interactions of ray with the scene.
     *  \param[in] ray Ray to calculate.
     *  \param[out] tMin Ray parameter at nearest hit point.
     *  \param[in] depth Recursion depth of ray bounces.
     *  \return Resulting color.
     */
    template <class SpectrumType>
    SpectrumType RayParameterTracer<SpectrumType>::traceRay(const Ray& ray, float& tMin, const int depth) const
    {
        tMin = static_cast<float>(HUGE_VALUE);

        SpectrumType color;

        // origin
        color.setCoeff(0, static_cast<float>(ray.origin_.x_));
        color.setCoeff(1, static_cast<float>(ray.origin_.y_));
        color.setCoeff(2, static_cast<float>(ray.origin_.z_));
        // direction
        color.setCoeff(3, static_cast<float>(ray.direction_.x_));
        color.setCoeff(4, static_cast<float>(ray.direction_.y_));
        color.setCoeff(5, static_cast<float>(ray.direction_.z_));

        return color;
    }
    
    
    
    /*! \brief Deep copies the tracer.
     *  \return Returns the copied tracer.
     */
    template <class SpectrumType>
    RayParameterTracer<SpectrumType>* RayParameterTracer<SpectrumType>::clone() const
    {
        RayParameterTracer<SpectrumType>* rayParameterTracer = new RayParameterTracer<SpectrumType>();
        return rayParameterTracer;
    }


    
} // end of namespace iiit

#endif // __RAYPARAMETERTRACER_HPP__
