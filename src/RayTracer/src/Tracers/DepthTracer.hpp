// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file DepthTracer.hpp
 *  \author Thomas Nuernberg
 *  \date 16.07.2015
 *  \brief Definition of class DepthTracer.
 */

#ifndef __DEPTHTRACER_HPP__
#define __DEPTHTRACER_HPP__

#include <memory>

#include "Tracer.hpp"

#include "Scene.hpp"
#include "Utilities/Ray.hpp"



namespace iiit
{

    /*! \brief Class used for logging depth information.
     */
    template <class SpectrumType>
    class DepthTracer : public Tracer<SpectrumType>
    {
    public:
        DepthTracer();
        DepthTracer(std::weak_ptr<const Scene<SpectrumType> > scene, float minDistance = -1.f);
        virtual ~DepthTracer();
        virtual SpectrumType traceRay(const Ray& ray, const int depth) const;
        virtual SpectrumType traceRay(const Ray& ray, float& tMin, const int depth) const;
        virtual DepthTracer<SpectrumType>* clone() const;

        void setMinDistance(float minDistance);

    private:
        float minDistance_; ///< Minimum distance that can displayed in the available color range.
};

    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    DepthTracer<SpectrumType>::DepthTracer()
        : Tracer<SpectrumType>(true, false) // traces property, does not trace vignetting
        , minDistance_(-1.f)
    {
    }
    
    
    /*! \brief Constructor with initialization.
     *  \param[in] scene Scene object.
     *  \param[in] minDistance Minimum distance to be uniquely displayed.
     */
    template <class SpectrumType>
    DepthTracer<SpectrumType>::DepthTracer(std::weak_ptr<const Scene<SpectrumType> > scene, float minDistance)
        : Tracer<SpectrumType>(scene, true, false) // traces property, does not trace vignetting
        , minDistance_(minDistance)
    {
    }
    
    
    
    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    DepthTracer<SpectrumType>::~DepthTracer()
    {
    }
    
    
    /*! /brief Calculating interactions of ray with the scene.
     *  \param[in] ray Ray to calculate.
     *  \param[in] depth Recursion depth of ray bounces.
     *  \return Resulting color.
     */
    template <class SpectrumType>
    SpectrumType DepthTracer<SpectrumType>::traceRay(const Ray& ray, const int depth) const
    {
        double minRayParameter = std::numeric_limits<double>::infinity();
        bool hitAnObject = false;
        std::shared_ptr<const Scene<SpectrumType> > scene = this->scene_.lock();
        
        // iterate over objects in scene
        for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = scene->objects_.begin(); it != scene->objects_.end(); ++it)
        {
            // check for intersection
            double rayParameter;
            bool hit = (*it)->shadowHit(ray, rayParameter);
            
            // check for nearest intersection
            if (hit && rayParameter < minRayParameter && rayParameter > EPS)
            {
                hitAnObject = true;
                minRayParameter = rayParameter;
            }
        }
        
        // evaluate intersection
        if (hitAnObject) 
        {
            // Trace true Depth values
            if (minDistance_ == -1)
            {
                return SpectrumType(static_cast<float>(minRayParameter));
            }
            // Trace normalized inverse Depth values
            else
            {
                return SpectrumType(static_cast<float>(minDistance_ / minRayParameter));
            }
        }
        else
        {
            // get background color
            return SpectrumType(0.f);
        }
    }
    
    
    
    /*! /brief Calculating interactions of ray with the scene.
     *  \param[in] ray Ray to calculate.
     *  \param[out] tMin Ray parameter at nearest hit point.
     *  \param[in] depth Recursion depth of ray bounces.
     *  \return Resulting color.
     */
    template <class SpectrumType>
    SpectrumType DepthTracer<SpectrumType>::traceRay(const Ray& ray, float& tMin, const int depth) const
    {
        double minRayParameter = std::numeric_limits<double>::infinity();
        bool hitAnObject = false;
        std::shared_ptr<const Scene<SpectrumType> > scene = this->scene_.lock();
        
        // iterate over objects in scene
        for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = scene->objects_.begin(); it != scene->objects_.end(); ++it)
        {
            // check for intersection
            double rayParameter;
            bool hit = (*it)->shadowHit(ray, rayParameter);
            
            // check for nearest intersection
            if (hit && rayParameter < minRayParameter && rayParameter > EPS)
            {
                hitAnObject = true;
                minRayParameter = rayParameter;
            }
        }
        
        // evaluate intersection
        if (hitAnObject)
        {
            tMin = static_cast<float>(minRayParameter);
            // Trace true Depth values
            if (minDistance_ == -1)
            {
                return SpectrumType(static_cast<float>(minRayParameter));
            }
            // Trace normalized inverse Depth values
            else
            {
                return SpectrumType(static_cast<float>(minDistance_ / minRayParameter));
            }
        }
        else
        {
            tMin = static_cast<float>(HUGE_VALUE);
            
            // get background color
            return SpectrumType(0.f);
        }
    }
    
    
    
    /*! \brief Deep copies the tracer.
     *  \return Returns the copied tracer.
     */
    template <class SpectrumType>
    DepthTracer<SpectrumType>* DepthTracer<SpectrumType>::clone() const
    {
        DepthTracer<SpectrumType>* depthTracer = new DepthTracer<SpectrumType>();
        depthTracer->setMinDistance(minDistance_);
        return depthTracer;
    }



    /*! \brief Set minimum displayable distance.
     *  \param[in] minDistance Minimum displayable dinstance.
     */
    template <class SpectrumType>
    void DepthTracer<SpectrumType>::setMinDistance(float minDistance)
    {
        minDistance_ = minDistance;
    }
    
} // end of namespace iiit

#endif // __DEPTHTRACER_HPP__
