// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file NormalTracer.hpp
 *  \author Thomas Nuernberg
 *  \date 04.09.2017
 *  \brief Definition of class NormalTracer.
 */

#ifndef __NORMALTRACER_HPP__
#define __NORMALTRACER_HPP__

#include <array>
#include <memory>
#include <math.h>

#include "Tracer.hpp"
#include "Scene.hpp"
#include "Spectrum/CoefficientSpectrum.hpp"
#include "Utilities/Constants.h"
#include "Utilities/Ray.hpp"



namespace iiit
{

    /*! \brief Class used for logging normal information.
     */
    template <class SpectrumType>
    class NormalTracer : public Tracer<SpectrumType>
    {
    public:
        NormalTracer();
        NormalTracer(std::weak_ptr<const Scene<SpectrumType> > scene);
        NormalTracer(std::weak_ptr<const Scene<SpectrumType> > scene, bool encodeNormals);
        virtual ~NormalTracer();
        virtual SpectrumType traceRay(const Ray& ray, const int depth) const;
        virtual SpectrumType traceRay(const Ray& ray, float& tMin, const int depth) const;
        virtual NormalTracer<SpectrumType>* clone() const;

        void setEncodeNormals(bool encodeNormals);

    private:
        bool encodeNormals_; ///< Specify wether to encode the normals or wether to return the raw values.
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    NormalTracer<SpectrumType>::NormalTracer()
        : Tracer<SpectrumType>(true, false) // traces property, does not trace vignetting
        , encodeNormals_(false)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] scene Scene object.
     */
    template <class SpectrumType>
    NormalTracer<SpectrumType>::NormalTracer(std::weak_ptr<const Scene<SpectrumType> > scene)
        : Tracer<SpectrumType>(scene, true, false) // traces property, does not trace vignetting
        , encodeNormals_(false)
    {
    }


    /*! \brief Constructor with initialization.
     *  \param[in] scene Scene object.
     *  \param[in] encodeNormals Specify wether the normals should be endoced.
     */
    template <class SpectrumType>
    NormalTracer<SpectrumType>::NormalTracer(std::weak_ptr<const Scene<SpectrumType> > scene, bool encodeNormals)
        : Tracer<SpectrumType>(scene, true, false) // traces property, does not trace vignetting
        , encodeNormals_(encodeNormals)
    {
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    NormalTracer<SpectrumType>::~NormalTracer()
    {
    }



    /*! /brief Calculating interactions of ray with the scene.
     *  \param[in] ray Ray to calculate.
     *  \param[in] depth Recursion depth of ray bounces.
     *  \return Resulting color.
     */
    template <class SpectrumType>
    SpectrumType NormalTracer<SpectrumType>::traceRay(const Ray& ray, const int depth) const
    {
        double minRayParameter = std::numeric_limits<double>::infinity();
        ShadingData<SpectrumType> shadingData(this->scene_);
        std::shared_ptr<const Scene<SpectrumType> > scene = this->scene_.lock();
        
        // iterate over objects in scene
        for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = scene->objects_.begin(); it != scene->objects_.end(); ++it)
        {
            // check for intersection
            double rayParameter;
            ShadingData<SpectrumType> tmpShadingData(scene);
            bool hit = (*it)->hit(ray, rayParameter, tmpShadingData);
            
            // check for nearest intersection
            if (hit && rayParameter < minRayParameter && rayParameter > EPS)
            {
                minRayParameter = rayParameter;
                shadingData = tmpShadingData;
                shadingData.normal_.normalize();
            }
        }
        
        // evaluate intersection
        if (shadingData.hitAnObject_)
        {
            std::array<float, 3> rgb = {{0.f, 0.f, 0.f}};
            if (encodeNormals_)
            {
                rgb[0] = 0.5f + 0.5f * static_cast<float>(shadingData.normal_.x_);
                rgb[1] = 0.5f + 0.5f * static_cast<float>(shadingData.normal_.y_);
                rgb[2] = 0.5f + 0.5f * static_cast<float>(shadingData.normal_.z_);

            }
            else
            {
                rgb[0] = static_cast<float>(shadingData.normal_.x_);
                rgb[1] = static_cast<float>(shadingData.normal_.y_);
                rgb[2] = static_cast<float>(shadingData.normal_.z_);
            }
            SpectrumType color = SpectrumType::fromRgb(rgb, SpectrumRepresentationType::IlluminationSpectrum);
            return color;
        }
        else
        {
            // get background color
            return SpectrumType(0.f);
        }
    }
    
    
    
    /*! /brief Calculating interactions of ray with the scene.
     *  \param[in] ray Ray to calculate.
     *  \param[out] tMin Ray parameter at nearest hit point.
     *  \param[in] depth Recursion depth of ray bounces.
     *  \return Resulting color.
     */
    template <class SpectrumType>
    SpectrumType NormalTracer<SpectrumType>::traceRay(const Ray& ray, float& tMin, const int depth) const
    {
        double minRayParameter = std::numeric_limits<double>::infinity();
        ShadingData<SpectrumType> shadingData(this->scene_);
        std::shared_ptr<const Scene<SpectrumType> > scene = this->scene_.lock();
        
        // iterate over objects in scene
        for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = scene->objects_.begin(); it != scene->objects_.end(); ++it)
        {
            // check for intersection
            double rayParameter;
            ShadingData<SpectrumType> tmpShadingData(scene);
            bool hit = (*it)->hit(ray, rayParameter, tmpShadingData);
            
            // check for nearest intersection
            if (hit && rayParameter < minRayParameter && rayParameter > EPS)
            {
                minRayParameter = rayParameter;
                shadingData = tmpShadingData;
                shadingData.normal_.normalize();
            }
        }
        
        // evaluate intersection
        if (shadingData.hitAnObject_)
        {
            tMin = static_cast<float>(minRayParameter);
            std::array<float, 3> rgb = {{0.f, 0.f, 0.f}};
            if (encodeNormals_)
            {
                rgb[0] = 0.5f + 0.5f * static_cast<float>(shadingData.normal_.x_);
                rgb[1] = 0.5f + 0.5f * static_cast<float>(shadingData.normal_.y_);
                rgb[2] = 0.5f + 0.5f * static_cast<float>(shadingData.normal_.z_);

            }
            else
            {
                rgb[0] = static_cast<float>(shadingData.normal_.x_);
                rgb[1] = static_cast<float>(shadingData.normal_.y_);
                rgb[2] = static_cast<float>(shadingData.normal_.z_);
            }
            SpectrumType color = SpectrumType::fromRgb(rgb, SpectrumRepresentationType::IlluminationSpectrum);
            return color;
        }
        else
        {
            tMin = static_cast<float>(HUGE_VALUE);
            
            // get background color
            return SpectrumType(0.f);
        }
    }
    
    
    
    /*! \brief Deep copies the tracer.
     *  \return Returns the copied tracer.
     */
    template <class SpectrumType>
    NormalTracer<SpectrumType>* NormalTracer<SpectrumType>::clone() const
    {   
        NormalTracer<SpectrumType>* normalTracer = new NormalTracer<SpectrumType>();
        normalTracer->setEncodeNormals(encodeNormals_);
        return normalTracer;
    }


    /*! \brief Set minimum displayable distance.
     *  \param[in] encodeNormals Specify wether the normals should be endoced.
     */
    template <class SpectrumType>
    void NormalTracer<SpectrumType>::setEncodeNormals(bool encodeNormals)
    {
        encodeNormals_ = encodeNormals;
    }


} // end of namespace iiit

#endif // __NORMALTRACER_HPP__
