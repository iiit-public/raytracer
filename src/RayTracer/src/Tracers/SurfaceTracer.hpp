// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file SurfaceTracer.hpp
 *  \author Thomas Nuernberg
 *  \date 21.07.2015
 *  \brief Definition of class DepthTracer.
 */

#ifndef __SURFACETRACER_HPP__
#define __SURFACETRACER_HPP__

#include <memory>
#include <math.h>

#include "Tracer.hpp"
#include "Scene.hpp"
#include "Utilities/Constants.h"
#include "Utilities/Ray.hpp"



namespace iiit
{

    /*! \brief Class used for logging surface information.
     */
    template <class SpectrumType>
    class SurfaceTracer : public Tracer<SpectrumType>
    {
    public:
        SurfaceTracer();
        SurfaceTracer(std::weak_ptr<const Scene<SpectrumType> > scene);
        virtual ~SurfaceTracer();
        virtual SpectrumType traceRay(const Ray& ray, const int depth) const;
        virtual SpectrumType traceRay(const Ray& ray, float& tMin, const int depth) const;
        virtual SurfaceTracer<SpectrumType>* clone() const;
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    SurfaceTracer<SpectrumType>::SurfaceTracer()
        : Tracer<SpectrumType>(true, false) // traces property, does not trace vignetting
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] scene Scene object.
     */
    template <class SpectrumType>
    SurfaceTracer<SpectrumType>::SurfaceTracer(std::weak_ptr<const Scene<SpectrumType> > scene)
        : Tracer<SpectrumType>(scene, true, false) // traces property, does not trace vignetting
    {
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    SurfaceTracer<SpectrumType>::~SurfaceTracer()
    {
    }



    /*! /brief Calculating interactions of ray with the scene.
     *  \param[in] ray Ray to calculate.
     *  \param[in] depth Recursion depth of ray bounces.
     *  \return Resulting color.
     */
    template <class SpectrumType>
    SpectrumType SurfaceTracer<SpectrumType>::traceRay(const Ray& ray, const int depth) const
    {
        double minRayParameter = std::numeric_limits<double>::infinity();
        ShadingData<SpectrumType> shadingData(this->scene_);
        std::shared_ptr<const Scene<SpectrumType> > scene = this->scene_.lock();
        
        // iterate over objects in scene
        for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = scene->objects_.begin(); it != scene->objects_.end(); ++it)
        {
            // check for intersection
            double rayParameter;
            ShadingData<SpectrumType> tmpShadingData(scene);
            bool hit = (*it)->hit(ray, rayParameter, tmpShadingData);
            
            // check for nearest intersection
            if (hit && rayParameter < minRayParameter && rayParameter > EPS)
            {
                minRayParameter = rayParameter;
                shadingData = tmpShadingData;
                shadingData.normal_.normalize();
            }
        }
        
        // evaluate intersection
        if (shadingData.hitAnObject_)
        {
            return SpectrumType(static_cast<float>(acos(-(ray.direction_) * shadingData.normal_ / ray.direction_.length()) / PI_2));
        }
        else
        {
            // get background color
            return SpectrumType(0.f);
        }
    }
    
    
    
    /*! /brief Calculating interactions of ray with the scene.
     *  \param[in] ray Ray to calculate.
     *  \param[out] tMin Ray parameter at nearest hit point.
     *  \param[in] depth Recursion depth of ray bounces.
     *  \return Resulting color.
     */
    template <class SpectrumType>
    SpectrumType SurfaceTracer<SpectrumType>::traceRay(const Ray& ray, float& tMin, const int depth) const
    {
        double minRayParameter = std::numeric_limits<double>::infinity();
        ShadingData<SpectrumType> shadingData(this->scene_);
        std::shared_ptr<const Scene<SpectrumType> > scene = this->scene_.lock();
        
        // iterate over objects in scene
        for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = scene->objects_.begin(); it != scene->objects_.end(); ++it)
        {
            // check for intersection
            double rayParameter;
            ShadingData<SpectrumType> tmpShadingData(scene);
            bool hit = (*it)->hit(ray, rayParameter, tmpShadingData);
            
            // check for nearest intersection
            if (hit && rayParameter < minRayParameter && rayParameter > EPS)
            {
                minRayParameter = rayParameter;
                shadingData = tmpShadingData;
                shadingData.normal_.normalize();
            }
        }
        
        // evaluate intersection
        if (shadingData.hitAnObject_)
        {
            tMin = static_cast<float>(minRayParameter);
            return SpectrumType(static_cast<float>(acos(-(ray.direction_) * shadingData.normal_ / ray.direction_.length()) / PI_2));
        }
        else
        {
            tMin = static_cast<float>(HUGE_VALUE);
            
            // get background color
            return SpectrumType(0.f);
        }
    }
    
    
    
    /*! \brief Deep copies the tracer.
     *  \return Returns the copied tracer.
     */
    template <class SpectrumType>
    SurfaceTracer<SpectrumType>* SurfaceTracer<SpectrumType>::clone() const
    {
        return new SurfaceTracer<SpectrumType>();
    }

} // end of namespace iiit

#endif // __SURFACETRACER_HPP__
