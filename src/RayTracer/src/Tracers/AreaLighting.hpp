// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file AreaLighting.hpp
 *  \author Thomas Nuernberg
 *  \date 08.06.2016
 *  \brief Definition of class AreaLighting.
 */

#ifndef __AREALIGHTING_HPP__
#define __AREALIGHTING_HPP__

#include <memory>

#include "Tracer.hpp"

#include "Scene.hpp"
#include "Utilities/Ray.hpp"
#include "Utilities/Constants.h"



namespace iiit
{

    /*! \brief Class used for calculating interactions of rays with the scene, including area lights.
     */
    template <class SpectrumType>
    class AreaLighting : public Tracer<SpectrumType>
    {
    public:
        AreaLighting();
        AreaLighting(bool tracesVignetting);
        AreaLighting(std::weak_ptr<const Scene<SpectrumType> > scene);
        AreaLighting(std::weak_ptr<const Scene<SpectrumType> > scene, bool tracesVignetting);
        virtual ~AreaLighting();
        virtual SpectrumType traceRay(const Ray& ray, const int depth) const;
        virtual SpectrumType traceRay(const Ray& ray, float& tMin, const int depth) const;
        virtual AreaLighting<SpectrumType>* clone() const;
        virtual void startNewPixel() const;
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    AreaLighting<SpectrumType>::AreaLighting()
        : Tracer<SpectrumType>()
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] tracesVignetting Bool whether traces traces natrual vignetting.
     */
    template <class SpectrumType>
    AreaLighting<SpectrumType>::AreaLighting(bool tracesVignetting)
        : Tracer<SpectrumType>(false, tracesVignetting) // Does not trace property, traces vignetting
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] scene Scene object.
     */
    template <class SpectrumType>
    AreaLighting<SpectrumType>::AreaLighting(std::weak_ptr<const Scene<SpectrumType> > scene)
        : Tracer<SpectrumType>(scene)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] scene Scene object.
     *  \param[in] tracesVignetting Bool whether traces traces natrual vignetting.
     */
    template <class SpectrumType>
    AreaLighting<SpectrumType>::AreaLighting(std::weak_ptr<const Scene<SpectrumType> > scene, bool tracesVignetting)
        : Tracer<SpectrumType>(scene, false, tracesVignetting) // Does not trace property, traces vignetting
    {
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    AreaLighting<SpectrumType>::~AreaLighting()
    {
    }

    

    /*! \brief Calculating interactions of ray with the scene.
     *  \param[in] ray Ray to calculate.
     *  \param[in] depth Recursion depth of ray bounces.
     *  \return Resulting color.
     */
    template <class SpectrumType>
    SpectrumType AreaLighting<SpectrumType>::traceRay(const Ray& ray, const int depth) const
    {
        double minRayParameter = std::numeric_limits<double>::infinity();
        ShadingData<SpectrumType> shadingData(this->scene_);
        std::shared_ptr<const Scene<SpectrumType> > scene = this->scene_.lock();
        
        // iterate over objects in scene
        for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = scene->objects_.begin(); it != scene->objects_.end(); ++it)
        {
            // check for intersection
            double rayParameter;
            ShadingData<SpectrumType> tmpShadingData(scene);
            bool hit = (*it)->hit(ray, rayParameter, tmpShadingData);
            
            // check for nearest intersection
            if (hit && rayParameter < minRayParameter && rayParameter > EPS)
            {
                minRayParameter = rayParameter;
                shadingData = tmpShadingData;
                shadingData.normal_.normalize();
            }
        }
        
        // evaluate intersection
        if (shadingData.hitAnObject_)
        {
            return shadingData.material_->areaLightShade(shadingData);
        }
        else
        {
            // get background color
            return scene->backgroundColor_;
        }
    }
    
    
    
    /*! \brief Calculating interactions of ray with the scene.
     *  \param[in] ray Ray to calculate.
     *  \param[out] tMin Ray parameter at nearest hit point.
     *  \param[in] depth Recursion depth of ray bounces.
     *  \return Resulting color.
     */
    template <class SpectrumType>
    SpectrumType AreaLighting<SpectrumType>::traceRay(const Ray& ray, float& tMin, const int depth) const
    {
        double minRayParameter = std::numeric_limits<double>::infinity();
        ShadingData<SpectrumType> shadingData(this->scene_);
        std::shared_ptr<const Scene<SpectrumType> > scene = this->scene_.lock();
        
        // iterate over objects in scene
        for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = scene->objects_.begin(); it != scene->objects_.end(); ++it)
        {
            // check for intersection
            double rayParameter;
            ShadingData<SpectrumType> tmpShadingData(scene);
            bool hit = (*it)->hit(ray, rayParameter, tmpShadingData);
            
            // check for nearest intersection
            if (hit && rayParameter < minRayParameter && rayParameter > EPS)
            {
                minRayParameter = rayParameter;
                shadingData = tmpShadingData;
                shadingData.normal_.normalize();
            }
        }
        
        // evaluate intersection
        if (shadingData.hitAnObject_)
        {
            tMin = static_cast<float>(minRayParameter);
            return shadingData.material_->areaLightShade(shadingData);
        }
        else
        {
            tMin = static_cast<float>(HUGE_VALUE);
            
            // get background color
            return scene->backgroundColor_;
        }
    }
    
    
    
    /*! \brief Deep copies the tracer.
     *  \return Returns the copied tracer.
     */
    template <class SpectrumType>
    AreaLighting<SpectrumType>* AreaLighting<SpectrumType>::clone() const
    {
        return new AreaLighting<SpectrumType>(this->tracesVignetting());
    }



    /*! \brief Start a new pixel.
     *  Resets area light samplers to keep sample framework aligned with pixel samples.
     */
    template <class SpectrumType>
    void AreaLighting<SpectrumType>::startNewPixel() const
    {
        std::shared_ptr<const Scene<SpectrumType> > scene = this->scene_.lock();
        for (unsigned int i = 0; i < scene->lights_.size(); ++i)
        {
            scene->lights_[i]->alignSampler();
        }
    }
    
} // end of namespace iiit

#endif // __AREALIGHTING_HPP__
