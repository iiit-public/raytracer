// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file ObjectTracer.hpp
 *  \author Davie Uhlig
 *  \date 24.03.2020
 *  \brief Definition of class ObjectTracer.
 */

#ifndef __OBJECTTRACER_HPP__
#define __OBJECTTRACER_HPP__

#include <memory>

#include "Tracer.hpp"

#include "Scene.hpp"
#include "Utilities/Ray.hpp"



namespace iiit
{

    /*! \brief Class used for logging object-id information.
     */
    template <class SpectrumType>
    class ObjectTracer : public Tracer<SpectrumType>
    {
    public:
        ObjectTracer();
        ObjectTracer(std::weak_ptr<const Scene<SpectrumType> > scene);
        virtual ~ObjectTracer();
        virtual SpectrumType traceRay(const Ray& ray, const int depth) const;
        virtual SpectrumType traceRay(const Ray& ray, float& tMin, const int depth) const;
        virtual ObjectTracer<SpectrumType>* clone() const;
    };

    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    ObjectTracer<SpectrumType>::ObjectTracer()
        : Tracer<SpectrumType>(true, false) // traces property, does not trace vignetting
    {
    }

    /*! \brief Constructor with initialization.
     *  \param[in] scene Scene object.
     *  \param[in] minDistance Minimum distance to be uniquely displayed.
     */
    template <class SpectrumType>
    ObjectTracer<SpectrumType>::ObjectTracer(std::weak_ptr<const Scene<SpectrumType> > scene)
        : Tracer<SpectrumType>(scene, true, false) // traces property, does not trace vignetting
    {
    }

    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    ObjectTracer<SpectrumType>::~ObjectTracer()
    {
    }


    /*! /brief Calculating interactions of ray with the scene.
     *  \param[in] ray Ray to calculate.
     *  \param[in] depth Recursion depth of ray bounces.
     *  \return Resulting color.
     */
    template <class SpectrumType>
    SpectrumType ObjectTracer<SpectrumType>::traceRay(const Ray& ray, const int depth) const
    {
        double minRayParameter = std::numeric_limits<double>::infinity();
        bool hitAnObject = false;
        float objectID;
        std::shared_ptr<const Scene<SpectrumType> > scene = this->scene_.lock();

        // iterate over objects in scene
        float oID = 0.0;
        for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = scene->objects_.begin(); it != scene->objects_.end(); ++it)
        {
            // check for intersection
            double rayParameter;
            bool hit = (*it)->shadowHit(ray, rayParameter);
            oID += 1.0;

            // check for nearest intersection
            if (hit && rayParameter < minRayParameter && rayParameter > EPS)
            {
                hitAnObject = true;
                minRayParameter = rayParameter;
                objectID = oID;
            }
        }

        // evaluate intersection
        if (hitAnObject)
        {
           return SpectrumType(objectID);
        }
        else
        {
            // get ID for no object hit
            return SpectrumType(0.0f);
        }
    }



    /*! /brief Calculating interactions of ray with the scene.
     *  \param[in] ray Ray to calculate.
     *  \param[out] tMin Ray parameter at nearest hit point.
     *  \param[in] depth Recursion depth of ray bounces.
     *  \return Resulting color.
     */
    template <class SpectrumType>
    SpectrumType ObjectTracer<SpectrumType>::traceRay(const Ray& ray, float& tMin, const int depth) const
    {
        double minRayParameter = std::numeric_limits<double>::infinity();
        bool hitAnObject = false;
        float objectID;
        std::shared_ptr<const Scene<SpectrumType> > scene = this->scene_.lock();

        // iterate over objects in scene
        float oID = 0.0;
        for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = scene->objects_.begin(); it != scene->objects_.end(); ++it)
        {
            // check for intersection
            double rayParameter;
            bool hit = (*it)->shadowHit(ray, rayParameter);
            oID += 1.0;

            // check for nearest intersection
            if (hit && rayParameter < minRayParameter && rayParameter > EPS)
            {
                hitAnObject = true;
                minRayParameter = rayParameter;
                objectID = oID;
            }
        }


        // evaluate intersection
        if (hitAnObject)
        {
            return SpectrumType(objectID);
        }
        else
        {
            tMin = static_cast<float>(HUGE_VALUE);
            // get ID for no object hit
            return SpectrumType(0.0f);
        }
    }



    /*! \brief Deep copies the tracer.
     *  \return Returns the copied tracer.
     */
    template <class SpectrumType>
    ObjectTracer<SpectrumType>* ObjectTracer<SpectrumType>::clone() const
    {
        ObjectTracer<SpectrumType>* objectTracer = new ObjectTracer<SpectrumType>();
        return objectTracer;
    }



} // end of namespace iiit

#endif // __OBJECTTRACER_HPP__
