// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file PathTracer.hpp
 *  \author Thomas Nuernberg
 *  \date 29.04.2017
 *  \brief Definition of PathTracer.
 */

#ifndef __PATHTRACER_HPP__
#define __PATHTRACER_HPP__

#include <memory>

#include "Tracer.hpp"

#include "Scene.hpp"
#include "Utilities/Ray.hpp"
#include "Utilities/Constants.h"



namespace iiit
{

    /*! \brief Class used for rendering with global illumination.
     */
    template <class SpectrumType>
    class PathTracer : public Tracer<SpectrumType>
    {
    public:
        PathTracer();
        PathTracer(bool tracesVignetting);
        PathTracer(std::weak_ptr<const Scene<SpectrumType> > scene);
        PathTracer(std::weak_ptr<const Scene<SpectrumType> > scene, bool tracesVignetting);
        virtual ~PathTracer();
        virtual SpectrumType traceRay(const Ray& ray, const int depth) const;
        virtual SpectrumType traceRay(const Ray& ray, float& tMin, const int depth) const;
        virtual PathTracer<SpectrumType>* clone() const;
        
        int getMaxDepth() const;
        void setMaxDepth(int maxDepth);
        
    private:
        int maxDepth_; ///< Maximum recursion depth for reflections.
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    PathTracer<SpectrumType>::PathTracer()
        : Tracer<SpectrumType>()
        , maxDepth_(1)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] tracesVignetting Bool whether traces traces natrual vignetting.
     */
    template <class SpectrumType>
    PathTracer<SpectrumType>::PathTracer(bool tracesVignetting)
        : Tracer<SpectrumType>(false, tracesVignetting) // Does not trace property, traces vignetting
        , maxDepth_(1)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] scene Scene object.
     */
    template <class SpectrumType>
    PathTracer<SpectrumType>::PathTracer(std::weak_ptr<const Scene<SpectrumType> > scene)
        : Tracer<SpectrumType>(scene)
        , maxDepth_(1)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] scene Scene object.
     *  \param[in] tracesVignetting Bool whether traces traces natrual vignetting.
     */
    template <class SpectrumType>
    PathTracer<SpectrumType>::PathTracer(std::weak_ptr<const Scene<SpectrumType> > scene, bool tracesVignetting)
        : Tracer<SpectrumType>(scene, false, tracesVignetting) // Does not trace property, traces vignetting
        , maxDepth_(1)
    {
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    PathTracer<SpectrumType>::~PathTracer()
    {
    }

    

    /*! \brief Calculating interactions of ray with the scene.
     *  \param[in] ray Ray to calculate.
     *  \param[in] depth Recursion depth of ray bounces.
     *  \return Resulting color.
     */
    template <class SpectrumType>
    SpectrumType PathTracer<SpectrumType>::traceRay(const Ray& ray, const int depth) const
    {
        if (depth > maxDepth_)
        {
            return SpectrumType(0.f);
        }
        else
        {
            double minRayParameter = std::numeric_limits<double>::infinity();
            ShadingData<SpectrumType> shadingData(this->scene_);
            std::shared_ptr<const Scene<SpectrumType> > scene = this->scene_.lock();
            
            // iterate over objects in scene
            for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = scene->objects_.begin(); it != scene->objects_.end(); ++it)
            {
                // check for intersection
                double rayParameter;
                ShadingData<SpectrumType> tmpShadingData(scene);
                bool hit = (*it)->hit(ray, rayParameter, tmpShadingData);
                
                // check for nearest intersection
                if (hit && rayParameter < minRayParameter && rayParameter > EPS)
                {
                    minRayParameter = rayParameter;
                    shadingData = tmpShadingData;
                    shadingData.normal_.normalize();
                }
            }
            // evaluate intersection
            if (shadingData.hitAnObject_)
            {
                shadingData.depth_ = depth;
                shadingData.ray_ = ray;
                return shadingData.material_->pathShade(shadingData);
            }
            else
            {
                // get background color
                return scene->backgroundColor_;
            }
        }
    }
    
    
    
    /*! \brief Calculating interactions of ray with the scene.
     *  \param[in] ray Ray to calculate.
     *  \param[out] tMin Ray parameter at nearest hit point.
     *  \param[in] depth Recursion depth of ray bounces.
     *  \return Resulting color.
     */
    template <class SpectrumType>
    SpectrumType PathTracer<SpectrumType>::traceRay(const Ray& ray, float& tMin, const int depth) const
    {
        if (depth > maxDepth_)
        {
            return SpectrumType(0.f);
        }
        else
        {
            double minRayParameter = std::numeric_limits<double>::infinity();
            ShadingData<SpectrumType> shadingData(this->scene_);
            std::shared_ptr<const Scene<SpectrumType> > scene = this->scene_.lock();
            
            // iterate over objects in scene
            for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = scene->objects_.begin(); it != scene->objects_.end(); ++it)
            {
                // check for intersection
                double rayParameter;
                ShadingData<SpectrumType> tmpShadingData(scene);
                bool hit = (*it)->hit(ray, rayParameter, tmpShadingData);
                
                // check for nearest intersection
                if (hit && rayParameter < minRayParameter && rayParameter > EPS)
                {
                    minRayParameter = rayParameter;
                    shadingData = tmpShadingData;
                    shadingData.normal_.normalize();
                }
            }
            // evaluate intersection
            if (shadingData.hitAnObject_)
            {
                shadingData.depth_ = depth;
                shadingData.ray_ = ray;
                tMin = static_cast<float>(minRayParameter);
                return shadingData.material_->pathShade(shadingData);
            }
            else
            {
                tMin = static_cast<float>(HUGE_VALUE);
                
                // get background color
                return scene->backgroundColor_;
            }
        }
    }
    
    
    
    /*! \brief Deep copies the tracer.
     *  \return Returns the copied tracer.
     */
    template <class SpectrumType>
    PathTracer<SpectrumType>* PathTracer<SpectrumType>::clone() const
    {
        PathTracer<SpectrumType>* pathTracer = new PathTracer<SpectrumType>(this->tracesVignetting());
        pathTracer->setMaxDepth(maxDepth_);
        return pathTracer;
    }
    
    
    
    /*! \brief Get maximum recursion depth.
     *  \return Returns maximum recursion depth.
     */
    template <class SpectrumType>
    int PathTracer<SpectrumType>::getMaxDepth() const
    {
        return maxDepth_;
    }
    
    
    
    /*! \brief Set maximum recursion depth.
     *  \param[in] maxDepth Maximum recursion depth.
     */
    template <class SpectrumType>
    void PathTracer<SpectrumType>::setMaxDepth(int maxDepth)
    {
        maxDepth_ = maxDepth;
    }
    
} // end of namespace iiit

#endif // __PATHTRACER_HPP__
