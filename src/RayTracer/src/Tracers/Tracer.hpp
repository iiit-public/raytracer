// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Tracer.hpp
 *  \author Thomas Nuernberg
 *  \date 10.06.2015
 *  \brief Definition of class Tracer.
 */

#ifndef __TRACER_HPP__
#define __TRACER_HPP__

#include <memory>

#include "Utilities/Ray.hpp"



namespace iiit
{
    
    template <class T> class Scene; // forward declaration



    /*! \brief Abstract base class for calculating interactions of rays with the scene.
     */
    template <class SpectrumType>
    class Tracer
    {
    public:
        Tracer();
        Tracer(bool tracesProperty, bool tracesVignetting);
        Tracer(std::weak_ptr<const Scene<SpectrumType> > scene);
        Tracer(std::weak_ptr<const Scene<SpectrumType> > scene, bool tracesProperty, bool tracesVignetting);
        virtual ~Tracer();
        
        /*! \brief Traces rays.
         *  \param[in] ray Ray to trace.
         *  \param[in] depth Recursion depth of ray bounces.
         *  \return Returns color that is transmitted along the ray.
         */
        virtual SpectrumType traceRay(const Ray& ray, const int depth) const = 0;
        
        /*! \brief Traces rays.
         *  \param[in] ray Ray to trace.
         *  \param[out] tMin Ray parameter at nearest hit point.
         *  \param[in] depth Recursion depth of ray bounces.
         *  \return Returns color that is transmitted along the ray.
         */
        virtual SpectrumType traceRay(const Ray& ray, float& tMin, const int depth) const = 0;
        
        /*! \brief Deep copies the tracer.
         *  \return Returns the copied tracer.
         */
        virtual Tracer<SpectrumType>* clone() const = 0;
        
        /*! \brief Set scene samplers to start of a new pixel.
         */
        virtual void startNewPixel() const {}

        void setScene(std::weak_ptr<const Scene<SpectrumType> > scene);

        bool tracesProperty() const;
        bool tracesVignetting() const;

    protected:
        std::weak_ptr<const Scene<SpectrumType> > scene_; ///< Pointer to the scene to render.
        bool tracesProperty_;
        bool tracesVignetting_;
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Tracer<SpectrumType>::Tracer()
        : tracesProperty_(false), tracesVignetting_(false)
    {
    }

    /*! \brief Constructor with initialization.
     *  \param[in] tracesProperty Bool if tracer traces abstract properties like depth or surface normal.
     *  \param[in] tracesVignetting Bool if tracer traces natural vignetting.
     */
    template <class SpectrumType>
    Tracer<SpectrumType>::Tracer(bool tracesProperty, bool tracesVignetting)
        : tracesProperty_(tracesProperty), tracesVignetting_(tracesVignetting)
    {
    }

    /*! \brief Constructor with initialization.
     *  \param[in] scene Scene object.
     */
    template <class SpectrumType>
    Tracer<SpectrumType>::Tracer(std::weak_ptr<const Scene<SpectrumType> > scene)
        : scene_(scene), tracesProperty_(false), tracesVignetting_(false)
    {
    }

    /*! \brief Constructor with initialization.
     *  \param[in] scene Scene object.
     *  \param[in] tracesProperty Bool if tracer traces abstract properties like depth or surface normal.
     *  \param[in] tracesVignetting Bool if tracer traces natural vignetting.
     */
    template <class SpectrumType>
    Tracer<SpectrumType>::Tracer(std::weak_ptr<const Scene<SpectrumType> > scene, bool tracesProperty, bool tracesVignetting)
        : scene_(scene), tracesProperty_(tracesProperty), tracesVignetting_(tracesVignetting)
    {
    }


    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    Tracer<SpectrumType>::~Tracer()
    {
    }



    /*! \brief Set scene.
     *  \param[in] scene Scene pointer.
     */
    template <class SpectrumType>
    void Tracer<SpectrumType>::setScene(std::weak_ptr<const Scene<SpectrumType> > scene)
    {
        scene_ = scene;
    }

    /*! \brief If tracer traces abstract properties like depth or surface normal
     */
    template <class SpectrumType>
    bool Tracer<SpectrumType>::tracesProperty() const
    {
        return tracesProperty_;
    }

    /*! \brief If tracer traces natural vignetting.
     */
    template <class SpectrumType>
    bool Tracer<SpectrumType>::tracesVignetting() const
    {
        return tracesVignetting_;
    }

} // end of namespace iiit

#endif // __TRACER_HPP__
