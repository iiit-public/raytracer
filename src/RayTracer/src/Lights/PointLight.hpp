// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file PointLight.hpp
 *  \author Thomas Nuernberg
 *  \date 10.06.2015
 *  \brief Definition of class PointLight.
 */

#ifndef __POINTLIGHT_HPP__
#define __POINTLIGHT_HPP__

#include "Light.hpp"

#include <vector>

#include "Objects/GeometricObject.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"



namespace iiit
{
    
    /*! \brief Class represent light source emitting light from a single point in space.
     */
    template <class SpectrumType>
    class PointLight : public Light<SpectrumType>
    {
    public:
        PointLight(const Point3D& location);
        PointLight(const PointLight<SpectrumType>& other);

        virtual Vector3D getDirection(const ShadingData<SpectrumType>& shadingDataObject);
        virtual SpectrumType L(const ShadingData<SpectrumType>& shadingDataObject);
        virtual bool shadowed(const Ray& shadowRay, const ShadingData<SpectrumType>& shadingData) const;
        virtual PointLight<SpectrumType>* clone() const;

        void setColor(const SpectrumType& color);
        void setRadianceScaling(float radianceScaling);

    private:
        float radianceScalingFactor_; ///< Factor scaling the radiance of the point light source.
        SpectrumType color_; ///< Color of the point light source.
        Point3D location_; ///< Location of the point light.
    };
    
    

    /*! \brief Default constructor.
     *  \param[in] location Location of the point light.
     */
    template <class SpectrumType>
    PointLight<SpectrumType>::PointLight(const Point3D& location)
        : Light<SpectrumType>(true)
        , radianceScalingFactor_(1.0)
        , color_(1.f)
        , location_(location)
    {
    }



    /*! \brief Constructor by reference.
     *  \param[in] other Other light to copy.
     */
    template <class SpectrumType>
    PointLight<SpectrumType>::PointLight(const PointLight& other)
        : Light<SpectrumType>(other)
        , radianceScalingFactor_(other.radianceScalingFactor_)
        , color_(other.color_)
        , location_(other.location_)
    {
    }



    /*! \brief Get direction of incident light for a point.
     *  \param[in] shadingDataObject Shading object (not used for ConstantColor).
     *  \return Returns direction.
     */
    template <class SpectrumType>
    Vector3D PointLight<SpectrumType>::getDirection(const ShadingData<SpectrumType>& shadingDataObject)
    {
        Vector3D direction = location_ - shadingDataObject.hitPoint_;
        direction.normalize();
        return direction;
    }



    /*! \brief Get the incident radiance at the surface hit point.
     *  \param[in] shadingDataObject Shading data of the ray-object intersection.
     *  \return Returns the incident radiance.
     */
    template <class SpectrumType>
    SpectrumType PointLight<SpectrumType>::L(const ShadingData<SpectrumType>& shadingDataObject)
    {
        return color_ * radianceScalingFactor_;
    }



    /*! \brief Indicates if light is shadowed by another object.
     *  \param[in] shadowRay Ray from light source to surface hit point.
     *  \param[in] shadingData Shading data of hit point.
     *  \return Returns TRUE if hit point is shadowed, else FALSE.
     */
    template <class SpectrumType>
    bool PointLight<SpectrumType>::shadowed(const Ray& shadowRay, const ShadingData<SpectrumType>& shadingData) const
    {
        double t;
        double d = location_.distance(shadowRay.origin_);
        
        for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = shadingData.scene_.lock()->objects_.begin(); it != shadingData.scene_.lock()->objects_.end(); ++it)
        {
            if ((*it)->shadowHit(shadowRay, t) && t < d)
            {
                return true;
            }
        }
        return false;
    }



    /*! \brief Cloning the ambient light.
     *  \return Cloned object.
     */
    template <class SpectrumType>
    PointLight<SpectrumType>* PointLight<SpectrumType>::clone() const
    {
        return new PointLight<SpectrumType>(*this);
    }



    /*! \brief Set color of light.
     *  \param[in] color Color of light.
     */
    template <class SpectrumType>
    void PointLight<SpectrumType>::setColor(const SpectrumType& color)
    {
        color_ = color;
    }



    /*! \brief Set radiance scaling factor.
     *  \param[in] radianceScaling Radiance scaling factor.
     */
    template <class SpectrumType>
    void PointLight<SpectrumType>::setRadianceScaling(float radianceScaling)
    {
        radianceScalingFactor_ = radianceScaling;
    }

} // end of namespace iiit

#endif // __POINTLIGHT_HPP__