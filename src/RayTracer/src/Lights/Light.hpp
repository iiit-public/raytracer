// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Light.hpp
 *  \author Christian Zimmermann
 *  \date 19.05.2015
 *  \brief Definition of class Light.
 *
 *  Definition of class Light representing a light base class.
 */

#ifndef __LIGHT_HPP__
#define __LIGHT_HPP__

#include "Utilities/Vector3D.hpp"
#include "Utilities/ShadingData.hpp"



namespace iiit
{
    
    /*! \brief Abstract base class for lights.
     */
    template <class SpectrumType>
    class Light
    {
    public:

        Light();
        Light(bool castsShadows);
        Light(const Light<SpectrumType>& other);
        virtual ~Light();

        /*! \brief Get the direction f the incoming light to a surface hit point.
         *  \param[in] shadingDataObject Shading data of the ray-object intersection.
         *  \return Returns the vector of the incoming light direction.
         */
        virtual Vector3D getDirection(const ShadingData<SpectrumType>& shadingDataObject) = 0;

        /*! \brief Get the incident radiance at the surface hit point.
         *  \param[in] shadingDataObject Shading data of the ray-object intersection.
         *  \return Returns the incident radiance.
         */
        virtual SpectrumType L(const ShadingData<SpectrumType>& shadingDataObject) = 0;

        /*! \brief Checks if the light is shadowed from the point of view of a surface hit point.
         *  \param[in] shadowRay Ray from the surface hit point to the light.
         *  \param[in] shadingData Shading data of the ray-object intersection.
         *  \return Returns TRUE, if the light is shadowed, else FALSE.
         */
        virtual bool shadowed(const Ray& shadowRay, const ShadingData<SpectrumType>& shadingData) const = 0;

        /*! \brief Deep copies the light.
         *  \return Returns the copied light.
         */
        virtual Light* clone() const = 0;

        /*! \brief Get geometry term of rendering equation.
         *  \param[in] shadingData Shading data of the ray-object intersection.
         *  \return Returns geometry term.
         */
        virtual float g(const ShadingData<SpectrumType>& shadingData) const {return 1.f;}

        /*! \brief Get probability density function of samples on light area.
         *  \param[in] shadingData Shading data of the ray-object intersection.
         *  \return Returns probability density function at sample position.
         */
        virtual float pdf(const ShadingData<SpectrumType>& shadingData) const {return 1.f;}
        
        /*! \brief Align light sampler.
         */
        virtual void alignSampler() {}

        void setShadows(bool castsShadows);
        bool castsShadows() const;


    protected:
        bool shadows_; ///< Flag indicating whether light casts shadows.
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Light<SpectrumType>::Light()
        : shadows_(true)
    {
    }
    
    
    
    /*! \brief Constructor with initialization.
     *  \param[in] castsShadows Flag indicating whether light casts shadows.
     */
    template <class SpectrumType>
    Light<SpectrumType>::Light(bool castsShadows)
        : shadows_(castsShadows)
    {
    }
    
    
    
    /*! \brief Copy constructor.
     *  \param[in] other Light to copy.
     */
    template <class SpectrumType>
    Light<SpectrumType>::Light(const Light<SpectrumType>& other)
        : shadows_(other.shadows_)
    {
    }
    
    
    
    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    Light<SpectrumType>::~Light()
    {
    }
    
    

    /*! \brief Set if light casts shadows.
     *  \param[in] castsShadows Flag indicating whether light casts shadows.
     */
    template <class SpectrumType>
    void Light<SpectrumType>::setShadows(bool castsShadows)
    {
        shadows_ = castsShadows;
    }



    /*! \brief Indicates whether light casts shadows.
     *  \return Returns TRUE if light casts shadows, else FALSE.
     */
    template <class SpectrumType>
    bool Light<SpectrumType>::castsShadows() const
    {
        return shadows_;
    }

} // end of namespace iiit

#endif // __LIGHT_HPP__
