// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Ambient.hpp
 *  \author Christian Zimmermann
 *  \date 19.05.2015
 *  \brief Definition of class Ambient.
 *
 *  Definition of class Ambient representing a light class.
 */

#ifndef __AMBIENT_HPP__
#define __AMBIENT_HPP__

#include "Light.hpp"

#include "Utilities/ShadingData.hpp"
#include "Utilities/Vector3D.hpp"



namespace iiit
{

    /*! \brief Class for ambient illumination.
     *
     *  Class for homogeneous ambient illumination in the whole 3D space.
     */
    template <class SpectrumType>
    class Ambient : public Light<SpectrumType>
    {
    public:
        Ambient();
        Ambient(const Ambient<SpectrumType>& other);
        virtual ~Ambient();

        virtual Vector3D getDirection(const ShadingData<SpectrumType>& shadingDataObject);
        virtual SpectrumType L(const ShadingData<SpectrumType>& shadingDataObject);
        virtual bool shadowed(const Ray& shadowRay, const ShadingData<SpectrumType>& shadingData) const;
        virtual Ambient<SpectrumType>* clone() const;

        void setColor(const SpectrumType& color);
        void setRadianceScaling(float radianceScaling);

    private:
        float radianceScalingFactor_; ///< Factor scaling the radiance of the ambient light source
        SpectrumType color_; ///< Color of the ambient light source
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Ambient<SpectrumType>::Ambient(void)
        : Light<SpectrumType>(false)
        , radianceScalingFactor_(1.0)
        , color_(1.f)
    {
    }



    /*! \brief Constructor by reference.
     *  \param[in] other Other light to copy.
     */
    template <class SpectrumType>
    Ambient<SpectrumType>::Ambient(const Ambient<SpectrumType>& other)
        : Light<SpectrumType>(other)
        , radianceScalingFactor_(other.radianceScalingFactor_)
        , color_(other.color_)
    {
    }


    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    Ambient<SpectrumType>::~Ambient()
    {
    }



    /*! \brief Get direction for a point.
     *  \param[in] shadingDataObject Shading object (not used for ConstantColor).
     *  \return Returns direction.
     */
    template <class SpectrumType>
    Vector3D Ambient<SpectrumType>::getDirection(const ShadingData<SpectrumType>& shadingDataObject)
    {
        return Vector3D(0.0, 0.0, 0.0);
    }



    /*! \brief Get the incident radiance at the surface hit point.
     *  \param[in] shadingDataObject Shading data of the ray-object intersection.
     *  \return Returns the incident radiance.
     */
    template <class SpectrumType>
    SpectrumType Ambient<SpectrumType>::L(const ShadingData<SpectrumType>& shadingDataObject)
    {
        return color_ * radianceScalingFactor_;
    }



    /*! \brief Indicates if light is shadowed by another object.
     *  \param[in] shadowRay Ray from light source to surface hit point.
     *  \param[in] shadingData Shading data of hit point.
     *  \return Returns TRUE if hit point is shadowed, else FALSE.
     */
    template <class SpectrumType>
    bool Ambient<SpectrumType>::shadowed(const Ray& shadowRay, const ShadingData<SpectrumType>& shadingData) const
    {
        return false;
    }



    /*! \brief Cloning the ambient light.
     *  \return Cloned object.
     */
    template <class SpectrumType>
    Ambient<SpectrumType>* Ambient<SpectrumType>::clone() const
    {
        return new Ambient(*this);
    }



    /*! \brief Set color of light.
     *  \param[in] color Color of light.
     */
    template <class SpectrumType>
    void Ambient<SpectrumType>::setColor(const SpectrumType& color)
    {
        color_ = color;
    }



    /*! \brief Set radiance scaling factor.
     *  \param[in] radianceScaling Radiance scaling factor.
     */
    template <class SpectrumType>
    void Ambient<SpectrumType>::setRadianceScaling(float radianceScaling)
    {
        radianceScalingFactor_ = radianceScaling;
    }

} // end of namespace iiit

#endif // __AMBIENT_HPP__