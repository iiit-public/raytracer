// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file AmbientOccluder.hpp
 *  \author Thomas Nuernberg
 *  \date 06.06.2016
 *  \brief Definition of class AmbientOccluder.
 *
 *  Definition of class AmbientOccluder representing a light class with ambient occlusion.
 */

#ifndef __AMBIENTOCCLUDER_HPP__
#define __AMBIENTOCCLUDER_HPP__

#include "Light.hpp"

#include "Sampler/Sampler.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Vector3D.hpp"



namespace iiit
{

    /*! \brief Class for ambient illumination with occlusion.
     *
     *  Class for homogeneous ambient illumination in the whole 3D space.
     */
    template <class SpectrumType>
    class AmbientOccluder : public Light<SpectrumType>
    {
    public:
        AmbientOccluder();
        AmbientOccluder(const AmbientOccluder<SpectrumType>& other);
        virtual ~AmbientOccluder();

        virtual Vector3D getDirection(const ShadingData<SpectrumType>& shadingDataObject);
        virtual SpectrumType L(const ShadingData<SpectrumType>& shadingDataObject);
        virtual bool shadowed(const Ray& shadowRay, const ShadingData<SpectrumType>& shadingData) const;
        virtual AmbientOccluder<SpectrumType>* clone() const;

        void setSampler(std::shared_ptr<Sampler> sampler);

        void setColor(const SpectrumType& color);
        void setMinAmount(float minAmount);
        void setRadianceScaling(float radianceScaling);


    private:
        float radianceScalingFactor_; ///< Factor scaling the radiance of the ambient light source
        SpectrumType color_; ///< Color of the ambient light source
        float minAmount_; ///< Minimum amount of light when shadowed.
        Vector3D u_; ///< u-Axis of local coordinate system.
        Vector3D v_; ///< v-Axis of local coordinate system.
        Vector3D w_; ///< w-Axis of local coordinate system.
        std::shared_ptr<Sampler> sampler_; ///< Sampler for sampling the hemisphere above a surface hit point.
        
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    AmbientOccluder<SpectrumType>::AmbientOccluder(void)
        : Light<SpectrumType>(false)
        , radianceScalingFactor_(1.0)
        , color_(1.f)
        , minAmount_(0.25f)
    {
    }



    /*! \brief Constructor by reference.
     *  \param[in] other Other light to copy.
     */
    template <class SpectrumType>
    AmbientOccluder<SpectrumType>::AmbientOccluder(const AmbientOccluder<SpectrumType>& other)
        : Light<SpectrumType>(other)
        , radianceScalingFactor_(other.radianceScalingFactor_)
        , color_(other.color_)
        , minAmount_(other.minAmount_)
        , sampler_(other.sampler_)
    {
    }


    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    AmbientOccluder<SpectrumType>::~AmbientOccluder()
    {
    }



    /*! \brief Get direction for a point.
     *  \param[in] shadingDataObject Shading object (not used for ConstantColor).
     *  \return Returns direction.
     */
    template <class SpectrumType>
    Vector3D AmbientOccluder<SpectrumType>::getDirection(const ShadingData<SpectrumType>& shadingDataObject)
    {
        Point3D samplePoint = sampler_->sampleHemisphere();
        return Vector3D(samplePoint.x_ * u_ + samplePoint.y_ * v_ + samplePoint.z_ * w_);
    }



    /*! \brief Get the incident radiance at the surface hit point.
     *  \param[in] shadingDataObject Shading data of the ray-object intersection.
     *  \return Returns the incident radiance.
     */
    template <class SpectrumType>
    SpectrumType AmbientOccluder<SpectrumType>::L(const ShadingData<SpectrumType>& shadingDataObject)
    {
        w_= Vector3D(shadingDataObject.normal_);
        
        // jitter up vector in case normal is vertical
        v_ = w_.crossProduct(Vector3D(0.0072, 1.0, 0.0034));
        v_.normalize();
        u_ = v_.crossProduct(w_);

        Point3D samplePoint = sampler_->sampleHemisphere();
        //Ray shadowRay(shadingDataObject.hitPoint_, Vector3D(samplePoint.x_ * u + samplePoint.y_ * v + samplePoint.z_ * w));
        Ray shadowRay(shadingDataObject.hitPoint_, getDirection(shadingDataObject));
        if (shadowed(shadowRay, shadingDataObject))
        {
            return color_ * radianceScalingFactor_ * minAmount_;
        }
        else
        {
            return color_ * radianceScalingFactor_;
        }
    }



    /*! \brief Indicates if light is shadowed by another object.
     *  \param[in] shadowRay Ray from light source to surface hit point.
     *  \param[in] shadingData Shading data of hit point.
     *  \return Returns TRUE if hit point is shadowed, else FALSE.
     */
    template <class SpectrumType>
    bool AmbientOccluder<SpectrumType>::shadowed(const Ray& shadowRay, const ShadingData<SpectrumType>& shadingData) const
    {
        double t;
        
        for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = shadingData.scene_.lock()->objects_.begin(); it != shadingData.scene_.lock()->objects_.end(); ++it)
        {
            if ((*it)->shadowHit(shadowRay, t))
            {
                return true;
            }
        }
        return false;
    }



    /*! \brief Cloning the ambient light.
     *  \return Cloned object.
     */
    template <class SpectrumType>
    AmbientOccluder<SpectrumType>* AmbientOccluder<SpectrumType>::clone() const
    {
        return new AmbientOccluder(*this);
    }



    /*! \brief Set sampling object.
     *  \param[in] sampler Sampling object.
     */
    template <class SpectrumType>
    void AmbientOccluder<SpectrumType>::setSampler(std::shared_ptr<Sampler> sampler)
    {
        sampler_ = sampler;
    }



    /*! \brief Set color of light.
     *  \param[in] color Color of light.
     */
    template <class SpectrumType>
    void AmbientOccluder<SpectrumType>::setColor(const SpectrumType& color)
    {
        color_ = color;
    }



    /*! \brief Set minimum amount of light, that is assigned when objects are shadowed.
     *  \param[in] minAmount Minimum amount of light.
     */
    template <class SpectrumType>
    void AmbientOccluder<SpectrumType>::setMinAmount(float minAmount)
    {
        minAmount_ = minAmount;
    }



    /*! \brief Set radiance scaling factor.
     *  \param[in] radianceScaling Radiance scaling factor.
     */
    template <class SpectrumType>
    void AmbientOccluder<SpectrumType>::setRadianceScaling(float radianceScaling)
    {
        radianceScalingFactor_ = radianceScaling;
    }

} // end of namespace iiit

#endif // __AMBIENTOCCLUDER_HPP__