// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file AreaLight.hpp
 *  \author Thomas Nuernberg
 *  \date 08.06.2016
 *  \brief Definition of class AreaLight.
 */

#ifndef __AREALIGHT_HPP__
#define __AREALIGHT_HPP__

#include "Light.hpp"

#include "Objects/GeometricObject.hpp"
#include "Objects/AreaLightObject.hpp"
#include "Materials/Material.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/Normal.hpp"



namespace iiit
{
    
    /*! \brief Class represent light source emitting light from an object surface.
     */
    template <class SpectrumType>
    class AreaLight : public Light<SpectrumType>
    {
    public:
        AreaLight(std::shared_ptr<AreaLightObject<SpectrumType> > object);
        AreaLight(const AreaLight<SpectrumType>& other);

        virtual Vector3D getDirection(const ShadingData<SpectrumType>& shadingDataObject);
        virtual SpectrumType L(const ShadingData<SpectrumType>& shadingDataObject);
        virtual bool shadowed(const Ray& shadowRay, const ShadingData<SpectrumType>& shadingData) const;

        virtual float g(const ShadingData<SpectrumType>& shadingDataObject) const;
        virtual float pdf(const ShadingData<SpectrumType>& shadingDataObject) const;
        virtual void alignSampler();

        virtual AreaLight<SpectrumType>* clone() const;

    private:
        std::shared_ptr<AreaLightObject<SpectrumType> > object_; ///< Object with emissive surface.
        std::shared_ptr<Material<SpectrumType> > material_; ///< Emissive material.
        Point3D samplePoint_; ///< Sample point on object surface.
        Normal lightNormal_; ///< Normal direction on object surface at sample point.
        Vector3D incomingDirection_; ///< Direction from area light to shaded surface.
    };
    
    

    /*! \brief Constructor with initialization.
     *  \param[in] object Object used as area light.
     */
    template <class SpectrumType>
    AreaLight<SpectrumType>::AreaLight(std::shared_ptr<AreaLightObject<SpectrumType> > object)
        : Light<SpectrumType>(true)
        , object_(object)
        , material_(object->objectMaterial_)
    {
    }



    /*! \brief Constructor by reference.
     *  \param[in] other Other light to copy.
     */
    template <class SpectrumType>
    AreaLight<SpectrumType>::AreaLight(const AreaLight& other)
        : Light<SpectrumType>(other)
        , material_(other.material_)
        , samplePoint_(other.samplePoint_)
        , lightNormal_(other.lightNormal_)
        , incomingDirection_(other.incomingDirection_)
    {
        object_ = std::shared_ptr<AreaLightObject<SpectrumType> >(other.object_->clone());
    }



    /*! \brief Get direction of incident light for a point.
     *  \param[in] shadingDataObject Shading object (not used for ConstantColor).
     *  \return Returns direction.
     */
    template <class SpectrumType>
    Vector3D AreaLight<SpectrumType>::getDirection(const ShadingData<SpectrumType>& shadingDataObject)
    {
        samplePoint_ = object_->sample();
        lightNormal_ = object_->getNormal(samplePoint_);
        incomingDirection_ = samplePoint_ - shadingDataObject.hitPoint_;
        incomingDirection_.normalize();
        return incomingDirection_;
    }



    /*! \brief Get the incident radiance at the surface hit point.
     *  \param[in] shadingDataObject Shading data of the ray-object intersection.
     *  \return Returns the incident radiance.
     */
    template <class SpectrumType>
    SpectrumType AreaLight<SpectrumType>::L(const ShadingData<SpectrumType>& shadingDataObject)
    {
        double dotProduct = -lightNormal_ * incomingDirection_;

        if (dotProduct > 0.0)
        {
            return material_->getLe(shadingDataObject);
        }
        else
        {
            return SpectrumType(0.f);
        }
    }



    /*! \brief Indicates if light is shadowed by another object.
     *  \param[in] shadowRay Ray from light source to surface hit point.
     *  \param[in] shadingData Shading data of hit point.
     *  \return Returns TRUE if hit point is shadowed, else FALSE.
     */
    template <class SpectrumType>
    bool AreaLight<SpectrumType>::shadowed(const Ray& shadowRay, const ShadingData<SpectrumType>& shadingData) const
    {
        double t;
        double d = (samplePoint_ - shadowRay.origin_) * shadowRay.direction_;
        
        for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = shadingData.scene_.lock()->objects_.begin(); it != shadingData.scene_.lock()->objects_.end(); ++it)
        {
            if ((*it)->shadowHit(shadowRay, t) && t /*+ EPS*/ < d)
            {
                return true;
            }
        }
        return false;
    }



    /*! \brief Get geometry term of rendering equation.
     *  \param[in] shadingDataObject Shading data of the ray-object intersection.
     *  \return Returns geometry term.
     */
    template <class SpectrumType>
    float AreaLight<SpectrumType>::g(const ShadingData<SpectrumType>& shadingDataObject) const
    {
        double dotProduct = -lightNormal_ * incomingDirection_;
        double d2 = (samplePoint_.x_ - shadingDataObject.hitPoint_.x_) * (samplePoint_.x_ - shadingDataObject.hitPoint_.x_)
            + (samplePoint_.y_ - shadingDataObject.hitPoint_.y_) * (samplePoint_.y_ - shadingDataObject.hitPoint_.y_)
            + (samplePoint_.z_ - shadingDataObject.hitPoint_.z_) * (samplePoint_.z_ - shadingDataObject.hitPoint_.z_);

        return static_cast<float>(dotProduct / d2);
    }



    /*! \brief Get probability density function of samples on light area.
     *  \param[in] shadingDataObject Shading data of the ray-object intersection.
     *  \return Returns probability density function at sample position.
     */
    template <class SpectrumType>
    float AreaLight<SpectrumType>::pdf(const ShadingData<SpectrumType>& shadingDataObject) const
    {
        return object_->pdf(shadingDataObject);
    }

    
    /*! \brief Align area light object sampler.
     */
    template <class SpectrumType>
    void AreaLight<SpectrumType>::alignSampler()
    {
        object_->alignSampler();
    }

    

    /*! \brief Cloning the ambient light.
     *  \return Cloned object.
     */
    template <class SpectrumType>
    AreaLight<SpectrumType>* AreaLight<SpectrumType>::clone() const
    {
        return new AreaLight<SpectrumType>(*this);
    }



} // end of namespace iiit

#endif // __AREALIGHT_HPP__
