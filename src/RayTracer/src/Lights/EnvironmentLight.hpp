// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file EnvironmentLight.hpp
 *  \author Thomas Nuernberg
 *  \date 17.06.2016
 *  \brief Definition of class EnvironmentLight.
 */

#ifndef __ENVIRONMENTLIGHT_HPP__
#define __ENVIRONMENTLIGHT_HPP__

#include "Light.hpp"

#include "Sampler/Sampler.hpp"
#include "Materials/Material.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/Normal.hpp"



namespace iiit
{
    
    /*! \brief Class represent light source emitting light from a large scale object surface.
     */
    template <class SpectrumType>
    class EnvironmentLight : public Light<SpectrumType>
    {
    public:
        EnvironmentLight(std::shared_ptr<Material<SpectrumType> > material);
        EnvironmentLight(const EnvironmentLight<SpectrumType>& other);

        virtual Vector3D getDirection(const ShadingData<SpectrumType>& shadingDataObject);
        virtual SpectrumType L(const ShadingData<SpectrumType>& shadingDataObject);
        virtual bool shadowed(const Ray& shadowRay, const ShadingData<SpectrumType>& shadingData) const;

        virtual float pdf(const ShadingData<SpectrumType>& shadingDataObject) const;

        virtual EnvironmentLight<SpectrumType>* clone() const;

        virtual void setSampler(std::shared_ptr<Sampler> sampler);

    private:
        std::shared_ptr<Sampler> sampler_; ///< Sampling object used for light surface.
        std::shared_ptr<Material<SpectrumType> > material_; ///< Emissive material.
        Vector3D u_; ///< u-Axis of local coordinate system.
        Vector3D v_; ///< v-Axis of local coordinate system.
        Vector3D w_; ///< w-Axis of local coordinate system.
        Vector3D incomingDirection_; ///< Direction from area light to shaded surface.
    };
    
    

    /*! \brief Constructor with initialization.
     *  \param[in] material Material of the environment light.
     */
    template <class SpectrumType>
    EnvironmentLight<SpectrumType>::EnvironmentLight(std::shared_ptr<Material<SpectrumType> > material)
        : Light<SpectrumType>(true)
        , material_(material)
    {
    }



    /*! \brief Constructor by reference.
     *  \param[in] other Other light to copy.
     */
    template <class SpectrumType>
    EnvironmentLight<SpectrumType>::EnvironmentLight(const EnvironmentLight& other)
        : Light<SpectrumType>(other)
        , material_(other.material_)
        , u_(other.u_)
        , v_(other.v_)
        , w_(other.w_)
        , incomingDirection_(other.incomingDirection_)
    {
        this->setSampler(std::shared_ptr<Sampler>(other.sampler_->clone()));
    }



    /*! \brief Get direction of incident light for a point.
     *  \param[in] shadingDataObject Shading object (not used for ConstantColor).
     *  \return Returns direction.
     */
    template <class SpectrumType>
    Vector3D EnvironmentLight<SpectrumType>::getDirection(const ShadingData<SpectrumType>& shadingDataObject)
    {
        w_= Vector3D(shadingDataObject.normal_);
        
        // jitter up vector in case normal is vertical
        v_ = Vector3D(0.0034, 1.0, 0.0071).crossProduct(w_);
        v_.normalize();
        u_ = v_.crossProduct(w_);
        Point3D samplePoint = sampler_->sampleHemisphere();
        incomingDirection_ = samplePoint.x_ * u_ + samplePoint.y_ * v_ + samplePoint.z_ * w_;

        return incomingDirection_;
    }



    /*! \brief Get the incident radiance at the surface hit point.
     *  \param[in] shadingDataObject Shading data of the ray-object intersection.
     *  \return Returns the incident radiance.
     */
    template <class SpectrumType>
    SpectrumType EnvironmentLight<SpectrumType>::L(const ShadingData<SpectrumType>& shadingDataObject)
    {
        return material_->getLe(shadingDataObject);
    }



    /*! \brief Indicates if light is shadowed by another object.
     *  \param[in] shadowRay Ray from light source to surface hit point.
     *  \param[in] shadingData Shading data of hit point.
     *  \return Returns TRUE if hit point is shadowed, else FALSE.
     */
    template <class SpectrumType>
    bool EnvironmentLight<SpectrumType>::shadowed(const Ray& shadowRay, const ShadingData<SpectrumType>& shadingData) const
    {
        double t;
        
        for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = shadingData.scene_.lock()->objects_.begin(); it != shadingData.scene_.lock()->objects_.end(); ++it)
        {
            if ((*it)->shadowHit(shadowRay, t))
            {
                return true;
            }
        }
        return false;
    }



    /*! \brief Get probability density function of samples on light area.
     *  \param[in] shadingDataObject Shading data of the ray-object intersection.
     *  \return Returns probability density function at sample position.
     */
    template <class SpectrumType>
    float EnvironmentLight<SpectrumType>::pdf(const ShadingData<SpectrumType>& shadingDataObject) const
    {
        return static_cast<float>(Vector3D(shadingDataObject.normal_) * incomingDirection_ * INV_PI);
    }



    /*! \brief Cloning the ambient light.
     *  \return Cloned object.
     */
    template <class SpectrumType>
    EnvironmentLight<SpectrumType>* EnvironmentLight<SpectrumType>::clone() const
    {
        return new EnvironmentLight<SpectrumType>(*this);
    }



    /*! \brief Set sampling object.
     *  \param[in] sampler Sampling object.
     */
    template <class SpectrumType>
    void EnvironmentLight<SpectrumType>::setSampler(std::shared_ptr<Sampler> sampler)
    {
        sampler_ = sampler;
    }

} // end of namespace iiit

#endif // __ENVIRONMENTLIGHT_HPP__