// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file DirectionalLight.hpp
 *  \author Thomas Nuernberg
 *  \date 10.06.2015
 *  \brief Definition of class DirectionalLight.
 */

#ifndef __DIRECTIONALLIGHT_HPP__
#define __DIRECTIONALLIGHT_HPP__

#include "Light.hpp"

#include "Scene.hpp"
#include "Objects/GeometricObject.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Vector3D.hpp"



namespace iiit
{

    /*! \brief Class represent light source emitting perfectly parallel light throughout the entire
     *  scene.
     */
    template <class SpectrumType>
    class DirectionalLight: public Light<SpectrumType>
    {
    public:
        DirectionalLight(const Vector3D& direction);
        DirectionalLight(const DirectionalLight& other);
        virtual Vector3D getDirection(const ShadingData<SpectrumType>& shadingDataObject);
        virtual SpectrumType L(const ShadingData<SpectrumType>& shadingDataObject);
        virtual bool shadowed(const Ray& shadowRay, const ShadingData<SpectrumType>& shadingData) const;
        virtual DirectionalLight<SpectrumType>* clone() const;

        void setColor(const SpectrumType& color);
        void setRadianceScaling(float radianceScaling);

    private:
        float radianceScalingFactor_; ///< Factor scaling the radiance of the directional light source.
        SpectrumType color_; ///< Color of the directional light source.
        Vector3D direction_; ///< Direction of the light.
    };
    
    
    
    /*! \brief Default constructor.
     *  \param[in] direction Direction of the light.
     */
    template <class SpectrumType>
    DirectionalLight<SpectrumType>::DirectionalLight(const Vector3D& direction)
        : Light<SpectrumType>(true)
        , radianceScalingFactor_(1.0)
        , color_(1.f)
        , direction_(direction)
    {
        direction_.normalize();
    }
    
    
    
    /*! \brief Constructor by reference.
     *  \param[in] other Other light to copy.
     */
    template <class SpectrumType>
    DirectionalLight<SpectrumType>::DirectionalLight(const DirectionalLight& other)
        : Light<SpectrumType>(other)
        , radianceScalingFactor_(other.radianceScalingFactor_)
        , color_(other.color_)
        , direction_(other.direction_)
    {
    }
    
    
    
    /*! \brief Get direction if incident light for a point.
     *  \param[in] shadingDataObject Shading object (not used for ConstantColor).
     *  \return Returns direction.
     */
    template <class SpectrumType>
    Vector3D DirectionalLight<SpectrumType>::getDirection(const ShadingData<SpectrumType>& shadingDataObject)
    {
        return -direction_;
    }
    
    
    
    /*! \brief Get the incident radiance at the surface hit point.
     *  \param[in] shadingDataObject Shading data of the ray-object intersection.
     *  \return Returns the incident radiance.
     */
    template <class SpectrumType>
    SpectrumType DirectionalLight<SpectrumType>::L(const ShadingData<SpectrumType>& shadingDataObject)
    {
        return color_ * radianceScalingFactor_;
    }
    
    
    
    /*! \brief Indicates if light is shadowed by another object.
     *  \param[in] shadowRay Ray from light source to surface hit point.
     *  \param[in] shadingData Shading data of hit point.
     *  \return Returns TRUE if hit point is shadowed, else FALSE.
     */
    template <class SpectrumType>
    bool DirectionalLight<SpectrumType>::shadowed(const Ray& shadowRay, const ShadingData<SpectrumType>& shadingData) const
    {
        for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = shadingData.scene_.lock()->objects_.begin(); it != shadingData.scene_.lock()->objects_.end(); ++it)
        {
            double t;
            if ((*it)->shadowHit(shadowRay, t))
            {
                return true;
            }
        }
        return false;
    }
    
    
    
    /*! \brief Cloning the ambient light.
     *  \return Cloned object.
     */
    template <class SpectrumType>
    DirectionalLight<SpectrumType>* DirectionalLight<SpectrumType>::clone() const
    {
        return new DirectionalLight(*this);
    }
    
    
    
    /*! \brief Set color of light.
     *  \param[in] color Color of light.
     */
    template <class SpectrumType>
    void DirectionalLight<SpectrumType>::setColor(const SpectrumType& color)
    {
        color_ = color;
    }
    
    
    
    /*! \brief Set radiance scaling factor.
     *  \param[in] radianceScaling Radiance scaling factor.
     */
    template <class SpectrumType>
    void DirectionalLight<SpectrumType>::setRadianceScaling(float radianceScaling)
    {
        radianceScalingFactor_ = radianceScaling;
    }

} // end of namespace iiit
    
#endif // __DIRECTIONALLIGHT_HPP__