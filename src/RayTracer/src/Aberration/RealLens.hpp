// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file RealLens.hpp
 *  \author Chihabeddine Ben Hamadi
 *  \date 09.03.2016
 *  \brief Definition of class RealLens.
 *
 *  Definition of class RealLens, which adds Aberration to rendered images.
 */

#ifndef __REALLENS_HPP__
#define __REALLENS_HPP__

#include "Utilities/Ray.hpp"
#include "Utilities/Vector3D.hpp"



namespace iiit
{
    /*! \brief Class implements real lens.
     *
     *  This class implements a real lens, for physically accurate ray-refraction simulation.
     */
    class RealLens 
    {
        /*! Enum to distinguish between image and object side.
         */
        enum SideSpecification
        {
            ImageSide, ///< Image side.
            ObjectSide, ///< Object Side.
        };


    public :
        RealLens(double objectSideRadius, double imageSideRadius, double lensRadius, double lensThickness, double refractionIndex);
        RealLens(const RealLens& other);

        ~RealLens();

        bool calcRefraction(const Ray& originalRay, Ray& objectSideRay) const;

        
    private:
        bool hitLensSurface(const Ray& ray, SideSpecification side, Point3D& hitPoint, Normal& normal) const;

        double objectSideRadius_; ///< Radius of curvature for object side lens surface.
        double imageSideRadius_; ///< Radius of curvature for image side lens surface.
        double lensRadius_; ///< Radius of lens perpendicular to the optical axis.
        double lensThickness_; ///< Lens thickness.
        double refractionIndex_; ///< Refraction index of lens material.
    };

} // end of namespace iiit

#endif // __REALLENS_HPP__