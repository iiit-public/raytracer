// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file ChromaticRealLens.hpp
 *  \author Chihabeddine Ben Hamadi
 *  \date 09.03.2016
 *  \brief Definition of class ChromaticRealLens.
 *
 *  Definition of class ChromaticRealLens, which adds Aberration to rendered images.
 */

#ifndef __CHROMATICREALLENS_HPP__
#define __CHROMATICREALLENS_HPP__

#include "Utilities/Ray.hpp"
#include "Utilities/Vector3D.hpp"
#include <memory>
#include <string>



namespace iiit
{

    /*! \brief Structure to store sellmeier coefficients.
     */
    struct SellmeierCoefficients
    {
        double B1; ///< Coefficient B1.
        double B2; ///< Coefficient B2.
        double B3; ///< Coefficient B3.
        double C1; ///< Coefficient C1 in μm^2.
        double C2; ///< Coefficient C2 in μm^2.
        double C3; ///< Coefficient C3 in μm^2.
    };

    /*! \brief Class implements real lens with chromatic aberration.
     *
     *  This class implements a real lens with chromatic aberration, for physically accurate ray-refraction simulation.
     */
    class ChromaticRealLens
    {
        /*! Enum to distinguish between image and object side.
         */
        enum SideSpecification
        {
            ImageSide, ///< Image side.
            ObjectSide, ///< Object Side.
        };


    public :
        ChromaticRealLens(double objectSideRadius, double imageSideRadius, double lensRadius, double lensThickness, std::shared_ptr<SellmeierCoefficients> glassType);
        ChromaticRealLens(const ChromaticRealLens& other);

        ~ChromaticRealLens();

        bool calcRefraction(const Ray& originalRay, int waveLength, Ray& objectSideRay) const;
        double calcChromaticRefraction(int waveLength) const;

    protected:
        double objectSideRadius_; ///< Radius of curvature for object side lens surface.
        double imageSideRadius_; ///< Radius of curvature for image side lens surface.
        double lensRadius_; ///< Radius of lens perpendicular to the optical axis.
        double lensThickness_; ///< Lens thickness.
        std::shared_ptr<SellmeierCoefficients> glassType_; ///< Sellmeier coefficients defining the lens material.
        
    private:
        bool hitLensSurface(const Ray& ray, SideSpecification side, Point3D& hitPoint, Normal& normal) const;
    };

}


#endif // __CHROMATICREALLENS_HPP__
