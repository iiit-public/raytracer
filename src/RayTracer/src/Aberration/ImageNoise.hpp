// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file ImageNoise.hpp
 *  \author Chihab Ben Hamadi
 *  \date 10.12.2015
 *  \brief Definition of class ImageNoise.
 *
 *  Definition of class Scene, which adds image noise to rendered images.
 */

#ifndef __IMAGENOISE_HPP__
#define __IMAGENOISE_HPP__

#include <random>
#include <math.h>

#include "Utilities/Constants.h"



namespace iiit
{

    /*! \brief Class implements emva 1288 noise model.
     *
     *  This class implements the EMVA 1288 noise model to add noise to rendered images. The added
     *  noise follows a normal distribution with zero-mean and the standard deviation:
     *  \f[ \sigma_\mathrm{y}^2 = K^2 \left( \sigma_\mathrm{d}^2 + \sigma_\mathrm{e}^2\right), \f]
     *  where \f$\sigma_\mathrm{d}^2\f$ denotes the variance of the dark noise and
     *  \f$\sigma_\mathrm{e}^2\f$ the variance of the photon noise, both in units of excited electrons
     *  squared. The variance of the photon noise is identical to the image intensity and the poisson
     *  distribution is approximated by a normal distribution. The dark noise variance consists of the
     *  fixed read noise variance and thermal noise depending on the exposure time
     *  \f[\sigma_\mathrm{d}^2 = \sigma_{\mathrm{d}0}^2 + \mu_I \cdot T,\f]
     *  with the dark current \f$\mu_I\f$. The read noise is assumed to be without mean, but the dark
     *  current adds an offset to the image value.
     *
     *  The noise/standard deviation is calculated in number of excited electrons. As the image
     *  intensity is directly calculated in color channel intensities, the intensities are converted
     *  using the system gain parameter.
     */
    template <class SpectrumType>
    class ImageNoise 
    {
    public :
        ImageNoise();
        ImageNoise(float readNoise, float darkCurrent, float exposure, float gain);
        ~ImageNoise();
      
        void addNoise(SpectrumType& color);

        //------------------ GETTERS
        float getReadNoise();
        float getDarkCurrent();
        float getExposure();
        float getGain();

        //------------------ SETTERS
        void setReadNoise(float readNoise);
        void setDarkCurrent(float darkCurrent);
        void setExposure(float exposure);
        void setGain(float gain);

    private:
        std::mt19937 generator_; ///< Random number generator.
        std::normal_distribution<double> distribution_; ///< Distribution object for random numbers.
        float readNoise_; ///< Standard deviation of read noise in number of electrons.
        float darkCurrent_; ///< Mean/Variance of dark current in number of electron per second.
        float exposure_; ///< Exposure time in seconds.
        float gain_; ///< System gain of camera.

    };



//---------------------- inline getters
    
    /*! \brief Get read noise standard deviation.
     *  \return Returns read noise standard deviation.
     */
    template <class SpectrumType>
    inline float ImageNoise<SpectrumType>::getReadNoise()
    {
        return readNoise_;
    }
    
    
    
    /*! \brief Get dark current mean/variance.
     *  \return Returns dark current mean/variance.
     */
    template <class SpectrumType>
    inline float ImageNoise<SpectrumType>::getDarkCurrent()
    {
        return darkCurrent_;
    }
    
    
    
    /*! \brief Get exposure time.
     *  \return Returns exposure time.
     */
    template <class SpectrumType>
    inline float ImageNoise<SpectrumType>::getExposure()
    {
        return exposure_;
    }
    
    

    /*! \brief Get gain parameter.
     *  \return Returns gain parameter.
     */
    template <class SpectrumType>
    inline float ImageNoise<SpectrumType>::getGain()
    {
        return gain_;
    }



//---------------------- inline setters

    /*! \brief Set read noise standard deviation.
     *  \param[in] readNoise Read noise standard deviation.
     */
    template <class SpectrumType>
    inline void ImageNoise<SpectrumType>::setReadNoise(float readNoise)
    {
        readNoise_ = readNoise;
    }
    
    
    
    /*! \brief Set dark current mean/variance.
     *  \param[in] darkCurrent Dark current mean/variance.
     */
    template <class SpectrumType>
    inline void ImageNoise<SpectrumType>::setDarkCurrent(float darkCurrent)
    {
        darkCurrent_ = darkCurrent;
    }
    
    
    
    /*! \brief Set exposure time.
     *  \param[in] exposure Exposure time.
     */
    template <class SpectrumType>
    inline void ImageNoise<SpectrumType>::setExposure(float exposure)
    {
        exposure_ = exposure;
    }
    
    
    
    /*! \brief Set gain parameter.
     *  \param[in] gain Gain parameter.
     */
    template <class SpectrumType>
    inline void ImageNoise<SpectrumType>::setGain(float gain)
    {
        gain_ = gain;
    }



//----------------------

    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    ImageNoise<SpectrumType>::ImageNoise()
        : generator_()
        , distribution_()
        , readNoise_(0.f)
        , darkCurrent_(0.f)
        , exposure_(0.f)
        , gain_(1.f)
    {
    }
    
    
    
    /*! \brief Constructor with initialization.
     *  \param[in] readNoise Standard deviation of read noise in e-.
     *  \param[in] darkCurrent Mean/Variance of dark current in e- per second.
     *  \param[in] exposure Exposure time in seconds.
     *  \param[in] gain Gain parameter in DN/e-.
     */
    template <class SpectrumType>
    ImageNoise<SpectrumType>::ImageNoise(float readNoise, float darkCurrent, float exposure, float gain)
        : generator_()
        , distribution_()
        , readNoise_(readNoise)
        , darkCurrent_(darkCurrent)
        , exposure_(exposure)
        , gain_(gain)
    {
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    ImageNoise<SpectrumType>::~ImageNoise()
    {
    }



    /*! \brief Adds noise according to the EMVA 1288 noise model to each color channel.
     *  \param[in,out] color Color object.
     */
    template <class SpectrumType>
    void ImageNoise<SpectrumType>::addNoise(SpectrumType& color)
    {
        for (int i = 0; i < color.getNumOfCoeffs(); ++i)
        {
            // noise standard deviation in e^-
            float darkNoiseVariance = readNoise_ * readNoise_ + darkCurrent_ * exposure_;
            float shotNoiseVariance = color.getCoeff(i) / gain_;
            float noiseStdDev = sqrt(darkNoiseVariance + shotNoiseVariance);
            // noise standard deviation in image numbers (in [0,1])
            noiseStdDev = noiseStdDev * gain_;
            // add noise
            color.setCoeff(i, color.getCoeff(i)
                + darkCurrent_ * exposure_ * gain_
                + static_cast<float>(distribution_(generator_)) * noiseStdDev);
        }
    }

} // end of namespace iiit

#endif // __IMAGENOISE_HPP__
