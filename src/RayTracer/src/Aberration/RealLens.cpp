// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file RealLens.cpp
 *  \author Chihabeddine Ben Hamadi
 *  \date 09.03.2016
 *  \brief Implementation of class RealLens.
 *
 */
#include "RealLens.hpp"

#include <math.h>

#include "Utilities/Constants.h"
#include "Utilities/Vector3D.hpp"
#include "Utilities/Normal.hpp"
#include "Utilities/Quaternion.hpp"



namespace iiit
{

    /*! \brief Constructor with initialization.
     *  \param[in] objectSideRadius Radius of curvature of lens surface on the object side.
     *  \param[in] imageSideRadius Radius of curvature of lens surface on the image side
     *  \param[in] lensRadius Radius of the lens (aperture resulting from finite lens body).
     *  \param[in] lensThickness Thickness of the lens at the optical axis.
     *  \param[in] refractionIndex Refraction index of lens material.
     *
     *  Constructor checks whether the chosen lens parameters are viable and shows a warning.
     */
    RealLens::RealLens(double objectSideRadius, double imageSideRadius, double lensRadius, double lensThickness, double refractionIndex)
        : objectSideRadius_(objectSideRadius)
        , imageSideRadius_(imageSideRadius)
        , lensRadius_(lensRadius)
        , lensThickness_(lensThickness)
        , refractionIndex_(refractionIndex)
    {
        // integrity check of image side lens surface
        if (lensRadius > imageSideRadius)
        {
            std::cout << "Image side lens radius is too small to achieve the desired aperture." << std::endl;
        }
        else
        {
            double alpha = asin(lensRadius / imageSideRadius);
            if ((imageSideRadius - cos(alpha) * imageSideRadius) > lensThickness / 2.0)
            {
                std::cout << "Image side lens radius is too small to achieve the desired aperture." << std::endl;
            }
        }

        // integrity check of object side lens surface
        if (lensRadius > objectSideRadius)
        {
            std::cout << "Object side lens radius is too small to achieve the desired aperture." << std::endl;
        }
        else
        {
            double alpha = asin(lensRadius / objectSideRadius);
            if ((objectSideRadius - cos(alpha) * objectSideRadius) > lensThickness / 2.0)
            {
                std::cout << "Object side lens radius is too small to achieve the desired aperture." << std::endl;
            }
        }
    }



    /*! \brief Copy constructor.
     *  \param[in] other Other lens to copy.
     */
    RealLens::RealLens(const RealLens& other)
        : objectSideRadius_(other.objectSideRadius_)
        , imageSideRadius_(other.imageSideRadius_)
        , lensRadius_(other.lensRadius_)
        , lensThickness_(other.lensThickness_)
        , refractionIndex_(other.refractionIndex_)
    {
    }



    /*! \brief Default destructor.
     */
    RealLens::~RealLens()
    {
    }



    /*! \brief Calculates refraction of given ray.
     *  \param[in] originalRay Ray to be refracted.
     *  \param[out] objectSideRay Refracted ray.
     *  \return Returns TRUE, if the ray passes the lens, else FALSE.
     */
    bool RealLens::calcRefraction(const Ray& originalRay, Ray& objectSideRay) const
    {
        // calculate intersection with image side lens surface
        Point3D imageSideHitPoint;
        Normal imageSideNormal;
        if (!hitLensSurface(originalRay, SideSpecification::ImageSide, imageSideHitPoint, imageSideNormal))
        {
            //LOG(LogLevel::LogDebugRay, "Ray did not hit first lens surface!");
            return false;
        }
       
        // calculate refraction at first lens surface
        double s = (-imageSideNormal * originalRay.direction_) / originalRay.direction_.length();

        // angle between the normal and the refracted ray
        double cosThetaT = sqrt(1 - 1 / (refractionIndex_ * refractionIndex_) * (1 - s * s));

        // direction of refracted ray
        Vector3D lensRayDirection = 1 / refractionIndex_ * originalRay.direction_ - (cosThetaT - 1 / refractionIndex_ * s) * imageSideNormal;

        Ray lensRay(imageSideHitPoint, lensRayDirection);
        
        // calculate intersection with object side lens surface
        Point3D objectSideHitPoint;
        Normal objectSideNormal;
        if (!hitLensSurface(lensRay, SideSpecification::ObjectSide, objectSideHitPoint, objectSideNormal))
        {
            //LOG(LogLevel::LogDebugRay, "Ray did not hit second lens surface!");
            return false;
        }

        // calculate refraction at second lens surface
        s = (objectSideNormal * lensRay.direction_);

        // angle between the normal and the object-side ray
        cosThetaT = sqrt(1 - refractionIndex_ * refractionIndex_ * (1 - s * s));

        // direction of object-side ray
        Vector3D objectSideDirection = refractionIndex_ * lensRay.direction_ - (cosThetaT - refractionIndex_ * s) * objectSideNormal * (-1.f);

        objectSideRay.set(objectSideHitPoint, objectSideDirection);
        return true;
    }



    /*! \brief Calculates hit point of ray on spherical lens surface.
     *  \param[in] ray Ray to intersect.
     *  \param[in] side Image side/object side flag.
     *  \param[out] hitPoint Hit point on lens surface.
     *  \param[out] normal Surface normal at  hit point.
     *  \return Returns TRUE, if the ray hit the lens surface, else FALSE.
     */
    bool RealLens::hitLensSurface(const Ray& ray, SideSpecification side, Point3D& hitPoint, Normal& normal) const
    {
        double radius;
        double offset;
        if (side == SideSpecification::ImageSide)
        {
            radius = -imageSideRadius_;
            offset = lensThickness_ / 2;
        }
        else
        {
            radius = objectSideRadius_;
            offset = -lensThickness_ / 2;
        }
        Vector3D temp;
        temp[0] = ray.origin_[0];
        temp[1] = ray.origin_[1];
        temp[2] = ray.origin_[2] + radius + offset;

        double a = ray.direction_ * ray.direction_;
        double b = 2 * (ray.direction_ * temp);
        double c = (temp * temp) - (radius * radius);
        double sqr = b * b - 4 * a * c;

        if (sqr < 0.0)
        {
            // no intersection
            return false;
        }
        else
        {
            double e = sqrt(sqr);
            double t = (-b - e) / (2 * a);
        
            // hit point of the ray on the lens
            if (t > 0)
            {
                hitPoint[0] = ray.origin_[0] + t * ray.direction_[0];
                hitPoint[1] = ray.origin_[1] + t * ray.direction_[1];
                hitPoint[2] = ray.origin_[2] + t * ray.direction_[2];
            }
            else
            {
                t = (-b + e) / (2 * a);
                hitPoint[0] = ray.origin_[0] + t * ray.direction_[0];
                hitPoint[1] = ray.origin_[1] + t * ray.direction_[1];
                hitPoint[2] = ray.origin_[2] + t * ray.direction_[2];
            }
            
            // normal
            normal[0] = (temp[0] + t * ray.direction_[0]) / fabs(radius);
            normal[1] = (temp[1] + t * ray.direction_[1]) / fabs(radius);
            normal[2] = (temp[2] + t * ray.direction_[2]) / fabs(radius);
            normal.normalize();
        }

        if ((hitPoint[0] * hitPoint[0] + hitPoint[1] * hitPoint[1]) > (lensRadius_ * lensRadius_))
        {
            return false;
        }
        return true;
    }

} // end of namespace iiit
