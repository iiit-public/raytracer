// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Torus.hpp
 *  \author Thomas Nuernberg
 *  \date 09.06.2015
 *  \brief Definition of class Torus.
 */

#ifndef __TORUS_HPP__
#define __TORUS_HPP__

#include <math.h>
#include <limits>

#include "GeometricObject.hpp"
#include "BoundingBox.hpp"
#include "Utilities/Constants.h"
#include "Utilities/Ray.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Normal.hpp"
#include "Utilities/QuarticSolver.hpp"
#include "Materials/Material.hpp"



namespace iiit
{

    /*! \brief %Torus object.
     *
     *  This class represents a generic torus surface, meaning that it can only be defined in one
     *  orientation. Use the Instance mechanism to transform it to arbitrary toroid surfaces.
     */
    template <class SpectrumType>
    class Torus : public GeometricObject<SpectrumType>
    {
    public:
        Torus();
        Torus(double innerRadius, double outerRadius);
        Torus(double innerRadius, double outerRadius, std::shared_ptr<Material<SpectrumType> > material);
        Torus(const Torus<SpectrumType>& other);
        virtual ~Torus();

        virtual Torus<SpectrumType>* clone() const;

        /*! \brief Get the type of the geometric object.
         *  \return Returns the type of the geometric object.
         */
        virtual typename GeometricObject<SpectrumType>::Type getType() const {return GeometricObject<SpectrumType>::Torus;}
        virtual bool hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const;
        virtual bool shadowHit(const Ray& shadowRay, double& tmin) const;
        virtual BoundingBox getBoundingBox() const;

        double getInnerRadius() const;
        void setInnerRadius(double innerRadius);
        double getOuterRadius() const;
        void setOuterRadius(double outerRadius);

    private:
        Normal computeNormal(const Point3D& hitPoint) const;

        //void calculateLocalHitPoint(ShadingData<SpectrumType>& shadingDataObject) const;

        double innerRadius_; ///< Radius of torus tube.
        double outerRadius_; ///< Outer Radius of torus.
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Torus<SpectrumType>::Torus()
        : GeometricObject<SpectrumType>(SpectrumType(0.5f))
        , innerRadius_(0)
        , outerRadius_(0)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] innerRadius Inner radius of torus.
     *  \param[in] outerRadius Outer radius of torus.
     */
    template <class SpectrumType>
    Torus<SpectrumType>::Torus(double innerRadius, double outerRadius)
        : GeometricObject<SpectrumType>(SpectrumType(0.5f))
        , innerRadius_(innerRadius)
        , outerRadius_(outerRadius)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] innerRadius Inner radius of torus.
     *  \param[in] outerRadius Outer radius of torus.
     *  \param[in] material Material assigned to the torus.
     */
    template <class SpectrumType>
    Torus<SpectrumType>::Torus(double innerRadius, double outerRadius, std::shared_ptr<Material<SpectrumType> > material)
        : GeometricObject<SpectrumType>(material)
        , innerRadius_(innerRadius)
        , outerRadius_(outerRadius)
    {
    }



    /*! \brief Copy constructor.
     *  \param[in] other Torus object to copy.
     */
    template <class SpectrumType>
    Torus<SpectrumType>::Torus(const Torus<SpectrumType>& other)
        : GeometricObject<SpectrumType>(other)
        , innerRadius_(other.innerRadius_)
        , outerRadius_(other.outerRadius_)
    {
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    Torus<SpectrumType>::~Torus()
    {
    }



    /*! \brief Clone (make deep copy of) object.
     *  \return Returned cloned object.
     */
    template <class SpectrumType>
    Torus<SpectrumType>* Torus<SpectrumType>::clone() const
    {
        return new Torus<SpectrumType>(*this);
    }



    /*! \brief Intersects object with given ray.
     *  \param[in] ray Ray for intersection.
     *  \param[out] tmin Ray-parameter of hit point.
     *  \param[out] shadingData Shading data structure of hit point.
     *  \return Returns TRUE, if ray and object intersect, else FLASE.
     */
    template <class SpectrumType>
    bool Torus<SpectrumType>::hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const
    {
        if (!getBoundingBox().hit(ray))
        {
            return false;
        }
        
        double sumDSqrd = ray.direction_ * ray.direction_;
        double f = Vector3D(ray.origin_) * Vector3D(ray.origin_) - innerRadius_ * innerRadius_ - outerRadius_ * outerRadius_;
        double g = Vector3D(ray.origin_) * ray.direction_;
        double fourASqrd = 4.0 * outerRadius_ * outerRadius_;
        
        double coefficients[5];
        coefficients[0] = f * f - fourASqrd * (innerRadius_ * innerRadius_ - ray.origin_.z_ * ray.origin_.z_);
        coefficients[1] = 4.0 * g * f + 2.0 * fourASqrd * ray.origin_.z_ * ray.direction_.z_;
        coefficients[2] = 2.0 * sumDSqrd * f + 4.0 * g * g + fourASqrd * ray.direction_.z_ * ray.direction_.z_;
        coefficients[3] = 4.0 * sumDSqrd * g;
        coefficients[4] = sumDSqrd * sumDSqrd;
        
        // find solution  > 0 closest to 0
        double roots[4];
        int numRealRoots = QuarticSolver::solveQuartic(coefficients, roots);
        
        if (numRealRoots == 0)
        {
            return false;
        }
        
        double t = std::numeric_limits<double>::infinity();
        bool hit = false;
        for (int i = 0; i < numRealRoots; ++i)
        {
            if (roots[i] > EPS)
            {
                hit = true;
                if (roots[i] < t)
                {
                    t = roots[i];
                }
            }
        }
        
        if (!hit)
        {
            return false;
        }
        
        tmin = t;
        shadingData.hitAnObject_ = true;
        shadingData.hitPoint_ = ray.origin_ + t * ray.direction_;

        // Define local hitpoint relative to minimum x, y, z coordinate of the bounding box
        double x0 = this->getBoundingBox().x0_;
        double y0 = this->getBoundingBox().y0_;
        double z0 = this->getBoundingBox().z0_;
        shadingData.localHitPoint3D_ = shadingData.hitPoint_ - Point3D(x0, y0, z0);

        shadingData.normal_ = computeNormal(shadingData.hitPoint_);
        shadingData.ray_ = ray;
        shadingData.material_ = this->objectMaterial_;
        
        return true;
    }



    /*! \brief Intersects object with given shadow ray.
     *  \param[in] shadowRay Ray for intersection.
     *  \param[out] tmin Ray-parameter of hit point.
     *  \return Returns TRUE, if ray and object intersect, else FLASE.
     */
    template <class SpectrumType>
    bool Torus<SpectrumType>::shadowHit(const Ray& shadowRay, double& tmin) const
    {
        if (!this->castsShadows_)
        {
            return false;
        }

        if (!getBoundingBox().hit(shadowRay))
        {
            return false;
        }
        
        double sumDSqrd = shadowRay.direction_ * shadowRay.direction_;
        double f = Vector3D(shadowRay.origin_) * Vector3D(shadowRay.origin_) - innerRadius_ * innerRadius_ - outerRadius_ * outerRadius_;
        double g = Vector3D(shadowRay.origin_) * shadowRay.direction_;
        double fourASqrd = 4.0 * outerRadius_ * outerRadius_;
        
        double coefficients[5];
        coefficients[0] = f * f - fourASqrd * (innerRadius_ * innerRadius_ - shadowRay.origin_.z_ * shadowRay.origin_.z_);
        coefficients[1] = 4.0 * g * f + 2.0 * fourASqrd * shadowRay.origin_.z_ * shadowRay.direction_.z_;
        coefficients[2] = 2.0 * sumDSqrd * f + 4.0 * g * g + fourASqrd * shadowRay.direction_.z_ * shadowRay.direction_.z_;
        coefficients[3] = 4.0 * sumDSqrd * g;
        coefficients[4] = sumDSqrd * sumDSqrd;
        
        // find solution  > 0 closest to 0
        double roots[4];
        roots[0] = 0.0;
        roots[1] = 0.0;
        roots[2] = 0.0;
        roots[3] = 0.0;
        int numRealRoots = QuarticSolver::solveQuartic(coefficients, roots);
        
        if (numRealRoots == 0)
        {
            return false;
        }
        
        double t = std::numeric_limits<double>::infinity();
        bool hit = false;
        for (int i = 0; i < numRealRoots; ++i)
        {
            if (roots[i] > EPS)
            {
                hit = true;
                if (roots[i] < t)
                {
                    t = roots[i];
                }
            }
        }
        
        if (!hit)
        {
            return false;
        }
        
        tmin = t;
        return true;
    }



    /*! \brief Computes and returns the bounding box of the torus.
     *  \return Bounding box of torus.
     */
    template <class SpectrumType>
    BoundingBox Torus<SpectrumType>::getBoundingBox() const
    {
        return BoundingBox(
            -outerRadius_ - innerRadius_,
            -outerRadius_ - innerRadius_,
            -innerRadius_,
            outerRadius_ + innerRadius_,
            outerRadius_ + innerRadius_,
            innerRadius_);
    }



    /*! \brief Returns inner radius of torus.
     *  \return Inner radius of torus.
     */
    template <class SpectrumType>
    double Torus<SpectrumType>::getInnerRadius() const
    {
        return innerRadius_;
    }



    /*! \brief Sets inner radius of torus.
     *  \param[in] innerRadius Inner radius of torus.
     */
    template <class SpectrumType>
    void Torus<SpectrumType>::setInnerRadius(double innerRadius)
    {
        innerRadius_ = innerRadius;
    }



    /*! \brief Returns outer radius of torus.
     *  \return Outer radius of torus.
     */
    template <class SpectrumType>
    double Torus<SpectrumType>::getOuterRadius() const
    {
        return outerRadius_;
    }



    /*! \brief Sets outer radius of torus.
     *  \param[in] outerRadius Outer radius of torus.
     */
    template <class SpectrumType>
    void Torus<SpectrumType>::setOuterRadius(double outerRadius)
    {
        outerRadius_ = outerRadius;
    }



    /*! \brief Computes normal of hit point.
     *  \param[in] hitPoint Hit point.
     *  \return Normal of hit point.
     */
    template <class SpectrumType>
    Normal Torus<SpectrumType>::computeNormal(const Point3D& hitPoint) const
    {
        double tmp = Vector3D(hitPoint) * Vector3D(hitPoint) - innerRadius_ * innerRadius_ - outerRadius_ * outerRadius_;
        return Normal(4.0 * hitPoint.x_ * tmp,
            4.0 * hitPoint.y_ * tmp,
            4.0 * hitPoint.z_ * (tmp + 2.0 * outerRadius_ * outerRadius_));
    }



    ///*! \brief Calculates the localHitpoint.
    // *  \param[in] shadingDataObject Shading data of this hit point. Member localHitPoint_ is set.
    // *
    // *  This method calculates the hit point coordinates in a spherical coordinate system originated in
    // *  this spheres center. Values for localHitPoint_(u, v) are in the ranges u:[0, 180deg] and
    // *  v:[0, 360deg].
    // */
    //void Sphere::calculateLocalHitPoint(ShadingData<SpectrumType>& shadingDataObject) const
    //{
    //    Vector3D vectorCenterToHitpoint(shadingDataObject.hitPoint_ - center_);
    //
    //    double d = vectorCenterToHitpoint.length();
    //    //double d = sqrt(vectorCenterToHitpoint(1) * vectorCenterToHitpoint(1) + vectorCenterToHitpoint(2) * vectorCenterToHitpoint(2) + vectorCenterToHitpoint(3) * vectorCenterToHitpoint(3));
    //    float u = static_cast<float>(acos(vectorCenterToHitpoint(3) / d) * 180 * INV_PI);
    //    float v = static_cast<float>(atan2(vectorCenterToHitpoint(2), vectorCenterToHitpoint(1)) * 180 * INV_PI);
    //    if (v < 0.0)
    //    {
    //        v += 360.0f;
    //    }
    //    shadingDataObject.localHitPoint_ = Point2D(u, v);
    //}

} // end of namespace iiit

#endif // __TORUS_HPP__
