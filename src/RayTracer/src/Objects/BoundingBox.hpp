// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file BoundingBox.hpp
 *  \author Thomas Nuernberg
 *  \date 08.05.2015
 *  \brief Definition of class BoundingBox.
 */
 
#ifndef __BOUNDINGBOX_HPP__
#define __BOUNDINGBOX_HPP__

#include "Utilities/Ray.hpp"
#include "Utilities/Point3D.hpp"



namespace iiit
{

    /*! \brief Class representing axis-aligned rectangular bounding box.
     *
     *  This represents an axis-aligned rectangular bounding box that completely encloses an object.
     *  This class is used to check if a ray potentially intersects with a complex compound object
     *  inside the bounding box without having to check all the primitives of the object.
     */
    class BoundingBox
    {
    public:

        BoundingBox();
        BoundingBox(double x0, double y0, double z0, double x1, double y1, double z1);
        BoundingBox(const Point3D& point0, const Point3D& point1);

        BoundingBox& operator=(const BoundingBox& other);

        bool hit(const Ray& ray) const;
        bool inside(const Point3D& point) const;

        double x0_; ///< Minimum x-coordinate.
        double y0_; ///< Minimum y-coordinate.
        double z0_; ///< Minimum z-coordinate.
        double x1_; ///< Maximum x-coordinate.
        double y1_; ///< Maximum y-coordinate.
        double z1_; ///< Maximum z-coordinate.
    };

} // end of namespace iiit

#endif // __BOUNDINGBOX_HPP__