// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Instance.hpp
 *  \author Thomas Nuernberg
 *  \date 18.05.2015
 *  \brief Definition of class Instance.
 */

#ifndef __INSTANCE_HPP__
#define __INSTANCE_HPP__

#include <memory>
#include <math.h>

#include "GeometricObject.hpp"
#include "BoundingBox.hpp"
#include "FullBoundingBox.hpp"
#include "Utilities/Ray.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Transformation.hpp"
#include "Utilities/Matrix4x4.hpp"



namespace iiit
{

    /*! \brief %Instance object.
     *
     *  This class is used as an abstract representation of complex objects. It stores the material and
     *  the object transformation. The object geometry is stored in a reference to a specific object.
     *
     *  Use this class for memory consuming objects, so that the geometry only has to be stored once.
     */
    template <class SpectrumType>
    class Instance : public GeometricObject<SpectrumType>
    {
    public:
        Instance(std::shared_ptr<GeometricObject<SpectrumType> > object);
        Instance(const Instance<SpectrumType>& other);
        virtual ~Instance();

        virtual Instance<SpectrumType>* clone() const;

        /*! \brief Get the type of the geometric object.
         *  \return Returns the type of the geometric object.
         */
        virtual typename GeometricObject<SpectrumType>::Type getType() const {return object_->getType();}
        virtual bool hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const;
        virtual bool shadowHit(const Ray& shadowRay, double& tmin) const;
        virtual BoundingBox getBoundingBox() const;
        virtual void setMaterial(std::shared_ptr<Material<SpectrumType> > material);

        void setTransformation(const Transformation& transformation);
        void translate(const float dx, const float dy, const float dz);
        void rotateX(const float phi);
        void rotateY(const float theta);
        void rotateZ(const float psi);
        void scale(const float ax, const float ay, const float az);
        void shear(const float hxy, const float hxz, const float hyx, const float hyz, const float hzx, const float hzy);
        void addTransformation(const Matrix4x4& transformation);

    private:
        void updateBoundingBox();

        std::shared_ptr<GeometricObject<SpectrumType> > object_; ///< Represented base object.
        Transformation transformation_; ///< Transformation applied to the base object.
        BoundingBox transformedBoundingBox_; ///< Bounding box of the transformed object.
        bool hasMaterial_; ///< Flag indicating whether a unique material has been assigned to the instance.
    };



    /*! \brief Default constructor.
     *  \param[in] object Object to represent.
     */
    template <class SpectrumType>
    Instance<SpectrumType>::Instance(std::shared_ptr<GeometricObject<SpectrumType> > object)
        : object_(object)
        , transformation_()
        , transformedBoundingBox_(object->getBoundingBox())
        , hasMaterial_(false)
    {
    }



    /*! \brief Copy constructor.
     *  \param[in] other Instance to copy.
     *  \attention Copies the transformation but NOT the stored geometric object, because this would
     *  contradict the purpose of instance objects.
     */
    template <class SpectrumType>
    Instance<SpectrumType>::Instance(const Instance<SpectrumType>& other)
        : object_(other.object_)
        , transformation_(other.transformation_)
        , transformedBoundingBox_(other.transformedBoundingBox_)
        , hasMaterial_(other.hasMaterial_)
    {
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    Instance<SpectrumType>::~Instance()
    {
    }



    /*! \brief Clone object.
     *  \return Cloned object.
     *  \attention Copies the transformation but NOT the stored geometric object, because this would
     *  contradict the purpose of instance objects.
     */
    template <class SpectrumType>
    Instance<SpectrumType>* Instance<SpectrumType>::clone() const
    {
        return new Instance<SpectrumType>(*this);
    }



    /*! \brief Intersects object with given ray.
     *  \param[in] ray Ray for intersection.
     *  \param[out] tmin Ray-parameter of hit point.
     *  \param[out] shadingData Shading data structure of hit point.
     *  \return Returns TRUE, if ray and object intersect, else FLASE.
     */
    template <class SpectrumType>
    bool Instance<SpectrumType>::hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const
    {
        Ray invTransRay(transformation_.invTransform(ray));
        
        if (object_->hit(invTransRay, tmin, shadingData))
        {
            shadingData.hitPoint_ = transformation_.transform(shadingData.hitPoint_);
            shadingData.localHitPoint3D_ = transformation_.transform(shadingData.localHitPoint3D_);
            shadingData.normal_ = transformation_.transform(shadingData.normal_);
            shadingData.normal_.normalize();
            shadingData.ray_ = ray;
            if (hasMaterial_)
            {
                shadingData.material_ = this->objectMaterial_;
            }
            return true;
        }
        return false;
    }



    /*! \brief Intersects object with given shadow ray.
     *  \param[in] shadowRay Ray for intersection.
     *  \param[out] tmin Ray-parameter of hit point.
     *  \return Returns TRUE, if ray and object intersect, else FLASE.
     */
    template <class SpectrumType>
    bool Instance<SpectrumType>::shadowHit(const Ray& shadowRay, double& tmin) const
    {
        if (!this->castsShadows_)
        {
            return false;
        }

        Ray invTransRay(transformation_.invTransform(shadowRay));
        
        if (object_->shadowHit(invTransRay, tmin))
        {
            return true;
        }
        return false;
    }



    /*! \brief Returns the bounding box of the instance object.
     *  \return Bounding box of instance object.
     */
    template <class SpectrumType>
    BoundingBox Instance<SpectrumType>::getBoundingBox() const
    {
        return transformedBoundingBox_;
    }



    /*! \brief Set material.
     *  \param[in] material Object material.
     */
    template <class SpectrumType>
    void Instance<SpectrumType>::setMaterial(std::shared_ptr<Material<SpectrumType> > material)
    {
        hasMaterial_ = true;
        this->objectMaterial_ = material;
    }



    /*! \brief Set transformation of instance.
     *  \param[in] transformation Transformation of object.
     */
    template <class SpectrumType>
    void Instance<SpectrumType>::setTransformation(const Transformation& transformation)
    {
        transformation_ = transformation;
        updateBoundingBox();
    }



    /*! \brief Translate instance object.
     *  \param[in] dx Translation in x-direction.
     *  \param[in] dy Translation in y-direction.
     *  \param[in] dz Translation in z-direction.
     */
    template <class SpectrumType>
    void Instance<SpectrumType>::translate(float dx, float dy, float dz)
    {
        Matrix4x4 translation;
        translation[0][3] = dx;
        translation[1][3] = dy;
        translation[2][3] = dz;
        
        Matrix4x4 invTranslation;
        invTranslation[0][3] = -dx;
        invTranslation[1][3] = -dy;
        invTranslation[2][3] = -dz;
        
        transformation_.addTransformation(translation, invTranslation);
        updateBoundingBox();
    }



    /*! \brief Rotate instance object about the x-axis.
     *  \param[in] phi Rotation angle in radians.
     */
    template <class SpectrumType>
    void Instance<SpectrumType>::rotateX(const float phi)
    {
        Matrix4x4 rotation;
        rotation[1][1] = cos(phi);
        rotation[1][2] = -sin(phi);
        rotation[2][1] = sin(phi);
        rotation[2][2] = cos(phi);
        
        Matrix4x4 invRotation;
        invRotation[1][1] = cos(phi);
        invRotation[1][2] = sin(phi);
        invRotation[2][1] = -sin(phi);
        invRotation[2][2] = cos(phi);
        
        transformation_.addTransformation(rotation, invRotation);
        updateBoundingBox();
    }



    /*! \brief Rotate instance object about the y-axis.
     *  \param[in] theta Rotation angle in radians.
     */
    template <class SpectrumType>
    void Instance<SpectrumType>::rotateY(const float theta)
    {
        Matrix4x4 rotation;
        rotation[0][0] = cos(theta);
        rotation[0][2] = sin(theta);
        rotation[2][0] = -sin(theta);
        rotation[2][2] = cos(theta);
        
        Matrix4x4 invRotation;
        invRotation[0][0] = cos(theta);
        invRotation[0][2] = -sin(theta);
        invRotation[2][0] = sin(theta);
        invRotation[2][2] = cos(theta);
        
        transformation_.addTransformation(rotation, invRotation);
        updateBoundingBox();
    }



    /*! \brief Rotate instance object about the z-axis.
     *  \param[in] psi Rotation angle in radians.
     */
    template <class SpectrumType>
    void Instance<SpectrumType>::rotateZ(const float psi)
    {
        Matrix4x4 rotation;
        rotation[0][0] = cos(psi);
        rotation[0][1] = -sin(psi);
        rotation[1][0] = sin(psi);
        rotation[1][1] = cos(psi);
        
        Matrix4x4 invRotation;
        invRotation[0][0] = cos(psi);
        invRotation[0][1] = sin(psi);
        invRotation[1][0] = -sin(psi);
        invRotation[1][1] = cos(psi);
        
        transformation_.addTransformation(rotation, invRotation);
        updateBoundingBox();
    }



    /*! \brief Scale instance object.
     *  \param[in] ax Scaling factor along x-axis.
     *  \param[in] ay Scaling factor along y-axis.
     *  \param[in] az Scaling factor along z-axis.
     */
    template <class SpectrumType>
    void Instance<SpectrumType>::scale(const float ax, const float ay, const float az)
    {
        Matrix4x4 scale;
        scale[0][0] = ax;
        scale[1][1] = ay;
        scale[2][2] = az;
        
        Matrix4x4 invScale;
        invScale[0][0] = 1.0 / ax;
        invScale[1][1] = 1.0 / ay;
        invScale[2][2] = 1.0 / az;
        
        transformation_.addTransformation(scale, invScale);
        updateBoundingBox();
    }



    /*! \brief Shear instance object.
     *  \param[in] hxy Shearing factor along y-axis in xy-plane.
     *  \param[in] hxz Shearing factor along z-axis in xz-plane.
     *  \param[in] hyx Shearing factor along x-axis in xy-plane.
     *  \param[in] hyz Shearing factor along z-axis in yz-plane.
     *  \param[in] hzx Shearing factor along x-axis in xz-plane.
     *  \param[in] hzy Shearing factor along y-axis in yz-plane.
     */
    template <class SpectrumType>
    void Instance<SpectrumType>::shear(const float hxy, const float hxz, const float hyx, const float hyz, const float hzx, const float hzy)
    {
        Matrix4x4 shear;
        shear[0][1] = hyx;
        shear[0][2] = hzx;
        shear[1][0] = hxy;
        shear[1][2] = hzy;
        shear[2][0] = hxz;
        shear[2][1] = hyz;
        
        transformation_.addTransformation(shear);
        updateBoundingBox();
    }



    /*! \brief Apply arbitrary affine transformation to instance object.
     *  \param[in] transformation Transformation matrix.
     */
    template <class SpectrumType>
    void Instance<SpectrumType>::addTransformation(const Matrix4x4& transformation)
    {
        transformation_.addTransformation(transformation);
        updateBoundingBox();
    }



    /*! \brief Updates bounding box of the transformed object.
     *
     *  The update always refers to the bounding box of the untransformed object. So this function can
     *  be called safely at any time, without loosing approximation precision.
     */
    template <class SpectrumType>
    void Instance<SpectrumType>::updateBoundingBox()
    {
        FullBoundingBox fullBoundingBox(object_->getBoundingBox());
        transformedBoundingBox_ = FullBoundingBox(
            transformation_.transform(fullBoundingBox.vertexes_[0]),
            transformation_.transform(fullBoundingBox.vertexes_[1]),
            transformation_.transform(fullBoundingBox.vertexes_[2]),
            transformation_.transform(fullBoundingBox.vertexes_[3]),
            transformation_.transform(fullBoundingBox.vertexes_[4]),
            transformation_.transform(fullBoundingBox.vertexes_[5]),
            transformation_.transform(fullBoundingBox.vertexes_[6]),
            transformation_.transform(fullBoundingBox.vertexes_[7])).getAxisAlignedBoundingBox();
    }

} // end of namespace iiit

#endif // __INSTANCE_HPP__
