// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file MeshTriangle.hpp
 *  \author Thomas Nuernberg
 *  \date 01.06.2015
 *  \brief Definition of class MeshTriangle.
 */

#ifndef __MESHTRIANGLE_HPP__
#define __MESHTRIANGLE_HPP__

#include <array>
#include <memory>

#include "GeometricObject.hpp"
#include "Mesh.hpp"
#include "Utilities/Constants.h"
#include "Utilities/Point3D.hpp"
#include "Utilities/Normal.hpp"
#include "Materials/Material.hpp"



namespace iiit
{

    /*! \brief %MeshTriangle object.
     */
    template <class SpectrumType>
    class MeshTriangle : public GeometricObject<SpectrumType>
    {
    public:
        MeshTriangle();
        MeshTriangle(const SpectrumType& color);
        MeshTriangle(std::shared_ptr<Material<SpectrumType> > material);
        MeshTriangle(std::weak_ptr<Mesh> mesh, int index0, int index1, int index2);
        MeshTriangle(std::weak_ptr<Mesh> mesh, int index0, int index1, int index2, const SpectrumType& color);
        MeshTriangle(std::weak_ptr<Mesh> mesh, int index0, int index1, int index2, std::shared_ptr<Material<SpectrumType> > material);
        MeshTriangle(const MeshTriangle<SpectrumType>& other);
        virtual ~MeshTriangle();

        virtual BoundingBox getBoundingBox() const;

        void computeNormal(bool reverse);

        std::array<int, 3> vertexIndices_; ///< Indices of vertexes in the mesh that form the triangle.
        std::weak_ptr<Mesh> mesh_; ///< Mesh data structure storing the vertexes.
        Normal normal_; ///< Normal of triangle.
        
    };

    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    MeshTriangle<SpectrumType>::MeshTriangle()
        : GeometricObject<SpectrumType>()
    {
    }


    /*! \brief Constructor with material initialization from color.
     *  \param[in] color Color of object.
     */
    template <class SpectrumType>
    MeshTriangle<SpectrumType>::MeshTriangle(const SpectrumType& color)
        : GeometricObject<SpectrumType>(color)
    {
    }



    /*! \brief Constructor with material initialization from Material object.
     *  \param[in] material Object material.
     */
    template <class SpectrumType>
    MeshTriangle<SpectrumType>::MeshTriangle(std::shared_ptr<Material<SpectrumType> > material)
        : GeometricObject<SpectrumType>(material)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] mesh Reference to mesh object.
     *  \param[in] index0 Index in mesh of first vertex.
     *  \param[in] index1 Index in mesh of second vertex.
     *  \param[in] index2 Index in mesh of third vertex.
     */
    template <class SpectrumType>
    MeshTriangle<SpectrumType>::MeshTriangle(std::weak_ptr<Mesh> mesh, int index0, int index1, int index2)
        : GeometricObject<SpectrumType>()
        , mesh_(mesh)
    {
        vertexIndices_[0] = index0;
        vertexIndices_[1] = index1;
        vertexIndices_[2] = index2;
    }



    /*! \brief Constructor with initialization.
     *  \param[in] mesh Reference to mesh object.
     *  \param[in] index0 Index in mesh of first vertex.
     *  \param[in] index1 Index in mesh of second vertex.
     *  \param[in] index2 Index in mesh of third vertex.
     *  \param[in] color Color of object.
     */
    template <class SpectrumType>
    MeshTriangle<SpectrumType>::MeshTriangle(std::weak_ptr<Mesh> mesh, int index0, int index1, int index2, const SpectrumType& color)
        : GeometricObject<SpectrumType>(color)
        , mesh_(mesh)
    {
        vertexIndices_[0] = index0;
        vertexIndices_[1] = index1;
        vertexIndices_[2] = index2;
    }



    /*! \brief Constructor with initialization.
     *  \param[in] mesh Reference to mesh object.
     *  \param[in] index0 Index in mesh of first vertex.
     *  \param[in] index1 Index in mesh of second vertex.
     *  \param[in] index2 Index in mesh of third vertex.
     *  \param[in] material Object material.
     */
    template <class SpectrumType>
    MeshTriangle<SpectrumType>::MeshTriangle(std::weak_ptr<Mesh> mesh, int index0, int index1, int index2, std::shared_ptr<Material<SpectrumType> > material)
        : GeometricObject<SpectrumType>(material)
        , mesh_(mesh)
    {
        vertexIndices_[0] = index0;
        vertexIndices_[1] = index1;
        vertexIndices_[2] = index2;
    }



    /*! \brief Copy constructor.
     *  \param[in] other Other object to copy.
     */
    template <class SpectrumType>
    MeshTriangle<SpectrumType>::MeshTriangle(const MeshTriangle<SpectrumType>& other)
        : GeometricObject<SpectrumType>(other)
        , vertexIndices_(other.vertexIndices_)
        , mesh_(other.mesh_)
    {
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    MeshTriangle<SpectrumType>::~MeshTriangle()
    {
    }



    /*! \brief Computes and returns the bounding box of the Triangle.
     *  \return Bounding box of Triangle.
     */
    template <class SpectrumType>
    BoundingBox MeshTriangle<SpectrumType>::getBoundingBox() const
    {
        std::shared_ptr<Mesh> mesh(mesh_.lock());
        return BoundingBox(std::min(std::min(mesh->vertices_[vertexIndices_[0]].x_, mesh->vertices_[vertexIndices_[1]].x_), mesh->vertices_[vertexIndices_[2]].x_),
            std::min(std::min(mesh->vertices_[vertexIndices_[0]].y_, mesh->vertices_[vertexIndices_[1]].y_), mesh->vertices_[vertexIndices_[2]].y_),
            std::min(std::min(mesh->vertices_[vertexIndices_[0]].z_, mesh->vertices_[vertexIndices_[1]].z_), mesh->vertices_[vertexIndices_[2]].z_),
            std::max(std::max(mesh->vertices_[vertexIndices_[0]].x_, mesh->vertices_[vertexIndices_[1]].x_), mesh->vertices_[vertexIndices_[2]].x_),
            std::max(std::max(mesh->vertices_[vertexIndices_[0]].y_, mesh->vertices_[vertexIndices_[1]].y_), mesh->vertices_[vertexIndices_[2]].y_),
            std::max(std::max(mesh->vertices_[vertexIndices_[0]].z_, mesh->vertices_[vertexIndices_[1]].z_), mesh->vertices_[vertexIndices_[2]].z_));
    }



    /*! \brief Compute normal from vertexes.
     *  \param[in] reverse Flag indicating to reverse the computed normal.
     */
    template <class SpectrumType>
    void MeshTriangle<SpectrumType>::computeNormal(bool reverse)
    {
        std::shared_ptr<Mesh> mesh(mesh_.lock());
        Vector3D v01 = mesh->vertices_[vertexIndices_[1]] - mesh->vertices_[vertexIndices_[0]];
        Vector3D v02 = mesh->vertices_[vertexIndices_[2]] - mesh->vertices_[vertexIndices_[0]];
        mesh.reset();
        
        normal_ = v01.crossProduct(v02);
        normal_.normalize();
        if (normal_.x_ != normal_.x_)
        {
            normal_.set(1.0, 0.0, 0.0);
        }
        
        if (reverse)
        {
            normal_ = -normal_;
        }
    }

} // end of namespace iiit

#endif // __MESHTRIANGLE_HPP__