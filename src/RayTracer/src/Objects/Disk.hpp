// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Disk.hpp
 *  \author Thomas Nuernberg
 *  \date 09.01.2015
 *  \brief Definition of class Disk.
 */

#ifndef __DISK_HPP__
#define __DISK_HPP__

#include <math.h>

#include "AreaLightObject.hpp"
#include "Sampler/Sampler.hpp"
#include "Utilities/Constants.h"
#include "Utilities/Ray.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Normal.hpp"
#include "Materials/Material.hpp"


namespace iiit
{

    /*! \brief %Disk object.
     */
    template <class SpectrumType>
    class Disk : public AreaLightObject<SpectrumType>
    {
    public:
        Disk();
        Disk(const Point3D& point, const Normal& normal, double radius);
        Disk(double x, double y, double z, double nx, double ny, double nz, double r);
        Disk(double x, double y, double z, double nx, double ny, double nz, double r, std::shared_ptr<Material<SpectrumType> > material);
        Disk(const Point3D& point, const Normal& normal, double radius, std::shared_ptr<Material<SpectrumType> > material);
        Disk(const Disk<SpectrumType>& other);
        virtual ~Disk();

        virtual Disk<SpectrumType>* clone() const;

        /*! \brief Get the type of the geometric object.
         *  \return Returns the type of the geometric object.
         */
        virtual typename GeometricObject<SpectrumType>::Type getType() const {return GeometricObject<SpectrumType>::Disk;}
        virtual bool hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const;
        virtual bool shadowHit(const Ray& shadowRay, double& tmin) const;
        virtual BoundingBox getBoundingBox() const;

        Point3D getPoint() const;
        void setPoint(const Point3D& point);
        void setPoint(double x, double y, double z);

        Normal getNormal() const;
        void setNormal(const Normal& normal);
        void setNormal(double x, double y, double z);

        double getRadius() const;
        void setRadius(double radius);

        virtual Point3D sample();
        virtual float pdf(const ShadingData<SpectrumType>& shadingData);
        virtual Normal getNormal(const Point3D& point);

    private:
        void calculatePlaneVectors(void);
        void calculateLocalHitPoint(ShadingData<SpectrumType>& shadingDataObject) const;
        void calculateInvArea();

        Point3D point_; ///< Center of disk.
        Normal normal_; ///< Normal of disk.
        double radius_; ///< Radius of disk.
        double radius2_; ///< Squared radius of disk.
        Vector3D planeVector1_; ///< One of the two vectors forming the disk.
        Vector3D planeVector2_; ///< One of the two vectors forming the disk.
        float invArea_; ///< Inverse of the spanned are used for calculating the probability density function of sample points.
    };

    
    
    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Disk<SpectrumType>::Disk()
        : AreaLightObject<SpectrumType>(SpectrumType(0.5f))
        , point_()
        , normal_()
        , radius_(0.0)
        , radius2_(0.0)
    {
        calculatePlaneVectors();
    }



    /*! \brief Constructor with initialization.
     *  \param[in] point Point on plane in 3D.
     *  \param[in] normal Normal vector of plane.
     *  \param[in] radius Radius of disk.
     */
    template <class SpectrumType>
    Disk<SpectrumType>::Disk(const Point3D& point, const Normal& normal, double radius)
        : AreaLightObject<SpectrumType>(SpectrumType(0.5f))
        , point_(point)
        , normal_(normal)
        , radius_(radius)
        , radius2_(radius * radius)
    {
        calculatePlaneVectors();
        calculateInvArea();
    }



    /*! \brief Constructor with initialization.
     *  \param[in] point Point on plane in 3D.
     *  \param[in] normal Normal vector of plane.
     \param[in] radius Radius of disk.
     *  \param[in] material Material assigned to the Plane.
     */
    template <class SpectrumType>
    Disk<SpectrumType>::Disk(const Point3D& point, const Normal& normal, double radius, std::shared_ptr<Material<SpectrumType> > material)
        : AreaLightObject<SpectrumType>(material)
        , point_(point)
        , normal_(normal)
        , radius_(radius)
        , radius2_(radius * radius)
    {
        calculatePlaneVectors();
        calculateInvArea();
    }



    /*! \brief Constructor with initialization.
     *  \param[in] x X-coordinate of point on disk.
     *  \param[in] y Y-coordinate of point on disk.
     *  \param[in] z Z-coordinate of point on disk.
     *  \param[in] nx X-coordinate of normal vector.
     *  \param[in] ny Y-coordinate of normal vector.
     *  \param[in] nz Z-coordinate of normal vector.
     *  \param[in] r Radius of disk.
     */
    template <class SpectrumType>
    Disk<SpectrumType>::Disk(double x, double y, double z, double nx, double ny, double nz, double r)
        : AreaLightObject<SpectrumType>(SpectrumType(0.5f))
        , point_(x, y, z)
        , normal_(nx, ny, nz)
        , radius_(r)
        , radius2_(r * r)
    {
        calculatePlaneVectors();
        calculateInvArea();
    }



    /*! \brief Constructor with initialization.
     *  \param[in] x X-coordinate of point on plane.
     *  \param[in] y Y-coordinate of point on plane.
     *  \param[in] z Z-coordinate of point on plane.
     *  \param[in] nx X-coordinate of normal vector.
     *  \param[in] ny Y-coordinate of normal vector.
     *  \param[in] nz Z-coordinate of normal vector.
     *  \param[in] r Radius of disk.
     *  \param[in] material Material assigned to the Plane.
     */
    template <class SpectrumType>
    Disk<SpectrumType>::Disk(double x, double y, double z, double nx, double ny, double nz, double r, std::shared_ptr<Material<SpectrumType> > material)
        : AreaLightObject<SpectrumType>(SpectrumType(0.5f))
        , point_(x, y, z)
        , normal_(nx, ny, nz)
        , radius_(r)
        , radius2_(r * r)
    {
        this->objectMaterial_ = material;
        calculatePlaneVectors();
        calculateInvArea();
    }



    /*! \brief Copy constructor.
     *  \param[in] other Disk object to copy.
     */
    template <class SpectrumType>
    Disk<SpectrumType>::Disk(const Disk<SpectrumType>& other)
        : AreaLightObject<SpectrumType>(other)
        , point_(other.point_)
        , normal_(other.normal_)
        , radius_(other.radius_)
        , radius2_(other.radius2_)
        , planeVector1_(other.planeVector1_)
        , planeVector2_(other.planeVector2_)
        , invArea_(other.invArea_)
    {
        this->objectMaterial_ = other.objectMaterial_;
        if (other.sampler_)
        {
            this->setSampler(std::shared_ptr<Sampler>(other.sampler_->clone()));
        }
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    Disk<SpectrumType>::~Disk()
    {
    }



    /*! \brief Clone (make deep copy of) object.
     *  \return Returned cloned object.
     */
    template <class SpectrumType>
    Disk<SpectrumType>* Disk<SpectrumType>::clone() const
    {
        return new Disk<SpectrumType>(*this);
    }



    /*! \brief Checks if the given ray hits the geometric object and computes hit point properties.
     *  \param[in] ray Ray object to intersect.
     *  \param[out] tmin Minimum ray parameter t to consider a hit.
     *  \param[out] shadingData Hit point properties used for shading.
     *  \return Returns TRUE, if the ray hits the object, else FALSE.
     */
    template <class SpectrumType>
    bool Disk<SpectrumType>::hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const
    {
        double t = (point_ - ray.origin_) * normal_ / (ray.direction_ * normal_);
        
        if (t <= EPS || t != t)
        {
            return false;
        }
        
        Point3D point = ray.origin_ + t * ray.direction_;
        
        if (((point_ - point) * (point_ - point)) < radius2_)
        {
            tmin = t;
            shadingData.hitAnObject_ = true;
            if (normal_ * ray.direction_ > 0)
            {
                shadingData.normal_ = -normal_;
                shadingData.normalFlipped_ = true;
            }
            else
            {
                shadingData.normal_ = normal_;
            }
            shadingData.hitPoint_ = point;
            calculateLocalHitPoint(shadingData);
            shadingData.ray_ = ray;
            shadingData.material_ = this->objectMaterial_;
            
            return true;
        }
        return false;
    }



    /*! \brief Intersects object with given shadow ray.
     *  \param[in] shadowRay Ray for intersection.
     *  \param[out] tmin Ray-parameter of hit point.
     *  \return Returns TRUE, if ray and object intersect, else FLASE.
     */
    template <class SpectrumType>
    bool Disk<SpectrumType>::shadowHit(const Ray& shadowRay, double& tmin) const
    {
        if (!this->castsShadows_)
        {
            return false;
        }

        double t = (point_ - shadowRay.origin_) * normal_ / (shadowRay.direction_ * normal_);
        
        if (t <= EPS || t != t)
        {
            return false;
        }
        
        Point3D point = shadowRay.origin_ + t * shadowRay.direction_;
        
        if (((point_ - point) * (point_ - point)) < radius2_)
        {
            tmin = t;
            return true;
        }
        return false;
    }



    /*! \brief Computes and returns the bounding box of the disk.
     *  \return Bounding box of disk.
     */
    template <class SpectrumType>
    BoundingBox Disk<SpectrumType>::getBoundingBox() const
    {
        double r = sqrt(radius2_);
        return BoundingBox(
            point_.x_ - r,
            point_.y_ - r,
            point_.z_ - r,
            point_.x_ + r,
            point_.y_ + r,
            point_.z_ + r);
    }



    /*! \brief Returns point of disk.
     *  \return Point of disk.
     */
    template <class SpectrumType>
    Point3D Disk<SpectrumType>::getPoint() const
    {
        return point_;
    }



    /*! \brief Sets point of disk.
     *  \param[in] point Point of disk.
     */
    template <class SpectrumType>
    void Disk<SpectrumType>::setPoint(const Point3D& point)
    {
        point_ = point;
    }



    /*! \brief Sets point of disk.
     *  \param[in] x X-coordinate of point of disk.
     *  \param[in] y Y-coordinate of point of disk.
     *  \param[in] z Z-coordinate of point of disk.
     */
    template <class SpectrumType>
    void Disk<SpectrumType>::setPoint(double x, double y, double z)
    {
        point_.x_ = x;
        point_.y_ = y;
        point_.z_ = z;
    }



    /*! \brief Returns normal of disk.
     *  \return Normal of disk.
     */
    template <class SpectrumType>
    Normal Disk<SpectrumType>::getNormal() const
    {
        return normal_;
    }



    /*! \brief Sets normal of disk.
     *  \param[in] normal Normal of disk.
     */
    template <class SpectrumType>
    void Disk<SpectrumType>::setNormal(const Normal& normal)
    {
        normal_ = normal;
    }



    /*! \brief Sets normal of disk.
     *  \param[in] x X-coordinate of normal of disk.
     *  \param[in] y Y-coordinate of normal of disk.
     *  \param[in] z Z-coordinate of normal of disk.
     */
    template <class SpectrumType>
    void Disk<SpectrumType>::setNormal(double x, double y, double z)
    {
        normal_.x_ = x;
        normal_.y_ = y;
        normal_.z_ = z;
    }



    /*! \brief Get disk radius.
     *  \return Returns disk radius.
     */
    template <class SpectrumType>
    double Disk<SpectrumType>::getRadius() const
    {
        return radius_;
    }



    /*! \brief Set disk radius.
     *  \param[in] radius Disk radius.
     */
    template <class SpectrumType>
    void Disk<SpectrumType>::setRadius(double radius)
    {
        radius_ = radius;
        radius2_ = radius * radius;
        calculateInvArea();
    }



    /*! \brief Sample object surface.
     *  \return Returns sample point on surface.
     */
    template <class SpectrumType>
    Point3D Disk<SpectrumType>::sample()
    {
        Point2D samplePoint = this->sampler_->sampleUnitDisk();
        return (point_ + samplePoint.x_ * planeVector1_ + samplePoint.y_ * planeVector2_);
    }



    /*! \brief Get probability density function of samples on light area.
     *  \param[in] shadingData Shading data of the ray-object intersection.
     *  \return Returns probability density function at sample position.
     */
    template <class SpectrumType>
    float Disk<SpectrumType>::pdf(const ShadingData<SpectrumType>& shadingData)
    {
        return invArea_;
    }



    /*! \brief Get normal at specified surface point.
     *  \param[in] point Point on object surface.
     *  \return Returns normal.
     */
    template <class SpectrumType>
    Normal Disk<SpectrumType>::getNormal(const Point3D& point)
    {
        return normal_;
    }



    /*! \brief Calculates the two vectors forming the plane used for a local coordinate system
     */
    template <class SpectrumType>
    void Disk<SpectrumType>::calculatePlaneVectors(void)
    {
        // first: find one vector which is not colinear to the normal vector <-> possible vector for the plane
        if (fabs(normal_(3)) > EPS)
        {
            planeVector1_ = Vector3D(1.0, 0.0, -normal_(1) / normal_(3));
        }
        else if (fabs(normal_(2)) > EPS)
        {
            planeVector1_ = Vector3D(1.0, -normal_(1) / normal_(2), 0.0);
        }
        else
        {
            planeVector1_ = Vector3D(0.0, 1.0, 0.0);
        }
        planeVector1_.normalize();
        
        // find second perpendicular to both the normal vector and planeVector1
        Vector3D normalVector(normal_.x_, normal_.y_, normal_.z_);
        planeVector2_ = normalVector.crossProduct(planeVector1_);
        planeVector2_.normalize();
    }



    /*! \brief Calculates the localHitpoint.
     *  \param[in,out] shadingDataObject Shading data of this hit point. Member localHitPoint_ is set.
     *
     *  This method calculates the hit point coordinates in a 2D cartesian coordinate system originated
     *  in the planes origin. Values for localHitPoint_(u, v) are calculated by projecting the hitPoint
     *  onto the two planeVectors.
     */
    template <class SpectrumType>
    void Disk<SpectrumType>::calculateLocalHitPoint(ShadingData<SpectrumType>& shadingDataObject) const
    {
        Vector3D vectorOnPlane(shadingDataObject.hitPoint_ - point_);
        // Normalize maximum distance to 1
        vectorOnPlane = (1/radius_) * vectorOnPlane;

        float u = static_cast<float>(planeVector1_ * vectorOnPlane);
        float v = static_cast<float>(planeVector2_ * vectorOnPlane);
        shadingDataObject.localHitPoint2D_ = Point2D(u, v);
        shadingDataObject.localHitPoint3D_ = Point3D(u, v, 0);
    }



    /*! \brief Calculates inverse area of surface.
     */
    template <class SpectrumType>
    void Disk<SpectrumType>::calculateInvArea()
    {
        invArea_ = static_cast<float>(1.f / radius2_ / PI);
    }

} // end of namespace iiit

#endif // __DISK_HPP__
