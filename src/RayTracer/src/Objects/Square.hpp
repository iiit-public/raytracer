// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Square.hpp
 *  \author Thomas Nuernberg
 *  \date 08.06.2015
 *  \brief Definition of class Square.
 */

#ifndef __SQUARE_HPP__
#define __SQUARE_HPP__

#include <math.h>
#include <algorithm>

#include "AreaLightObject.hpp"
#include "Sampler/Sampler.hpp"
#include "Utilities/Constants.h"
#include "Utilities/Ray.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/Normal.hpp"
#include "Materials/Material.hpp"



namespace iiit
{

    /*! \brief %Square object.
     */
    template <class SpectrumType>
    class Square : public AreaLightObject<SpectrumType>
    {
    public:
        Square();
        Square(const Point3D& point, const Vector3D& a, const Vector3D& b);
        Square(const Point3D& point, const Vector3D& a, const Vector3D& b, std::shared_ptr<Material<SpectrumType> > material);
        Square(const Point3D& point, const Vector3D& a, const Vector3D& b, const Normal& normal);
        Square(const Point3D& point, const Vector3D& a, const Vector3D& b, const Normal& normal, std::shared_ptr<Material<SpectrumType> > material);
        Square(const Square<SpectrumType>& other);
        virtual ~Square();

        virtual Square<SpectrumType>* clone() const;

        /*! \brief Get the type of the geometric object.
         *  \return Returns the type of the geometric object.
         */
        virtual typename GeometricObject<SpectrumType>::Type getType() const {return GeometricObject<SpectrumType>::Square;}
        virtual bool hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const;
        virtual bool shadowHit(const Ray& shadowRay, double& tmin) const;
        virtual BoundingBox getBoundingBox() const;

        Point3D getPoint() const;
        void setPoint(const Point3D& point);
        void setPoint(double x, double y, double z);

        Normal getNormal() const;
        void setNormal(const Normal& normal);
        void setNormal(double x, double y, double z);

        Vector3D getA() const;
        void setA(const Vector3D& a);
        Vector3D getB() const;
        void setB(const Vector3D& b);

        virtual Point3D sample();
        virtual float pdf(const ShadingData<SpectrumType>& shadingData);
        virtual Normal getNormal(const Point3D& point);

    private:
        void calculatePlaneVectors();
        void calculateLocalHitPoint(ShadingData<SpectrumType>& shadingDataObject) const;
        void calculateInvArea();

        Point3D point_; ///< Point on plane.
        Vector3D a_; ///< First vector spanning the square.
        double aLength2_; ///< Square length of first vector.
        Vector3D b_; ///< Second vector spanning the square.
        double bLength2_; ///< Square length of second vector.
        Normal normal_; ///< Normal of plane.
        float invArea_; ///< Inverse of the spanned are used for calculating the probability density function of sample points.
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Square<SpectrumType>::Square()
        : AreaLightObject<SpectrumType>(SpectrumType(0.5f))
        , point_()
        , a_()
        , aLength2_(0.0)
        , b_()
        , bLength2_(0.0)
        , normal_()
        , invArea_(std::numeric_limits<float>::infinity())
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] point Vertex point of square.
     *  \param[in] a First vector spanning the square.
     *  \param[in] b Second vector spanning the square.
     *
     *  \attention This constructor adjusts vector b to be perpendicular to a.
     */
    template <class SpectrumType>
    Square<SpectrumType>::Square(const Point3D& point, const Vector3D& a, const Vector3D& b)
        : AreaLightObject<SpectrumType>(SpectrumType(0.5f))
        , point_(point)
        , a_(a)
        , b_(b)
    {
        calculatePlaneVectors();
        calculateInvArea();
    }



    /*! \brief Constructor with initialization.
     *  \param[in] point Vertex point of square.
     *  \param[in] a First vector spanning the square.
     *  \param[in] b Second vector spanning the square.
     *  \param[in] material Material assigned to the Square.
     *
     *  \attention This constructor adjusts vector b to be perpendicular to a.
     */
    template <class SpectrumType>
    Square<SpectrumType>::Square(const Point3D& point, const Vector3D& a, const Vector3D& b, std::shared_ptr<Material<SpectrumType> > material)
        : AreaLightObject<SpectrumType>(material)
        , point_(point)
        , a_(a)
        , b_(b)
    {
        calculatePlaneVectors();
        calculateInvArea();
    }



    /*! \brief Constructor with initialization.
     *  \param[in] point Vertex point of square.
     *  \param[in] a First vector spanning the square.
     *  \param[in] b Second vector spanning the square.
     *  \param[in] normal Normal vector of square plane.
     *
     *  \attention This does not check whether a, b and normal are perpendicular.
     */
    template <class SpectrumType>
    Square<SpectrumType>::Square(const Point3D& point, const Vector3D& a, const Vector3D& b, const Normal& normal)
        : AreaLightObject<SpectrumType>(SpectrumType(0.5f))
        , point_(point)
        , a_(a)
        , b_(b)
        , normal_(normal)
    {
        aLength2_ = (a_ * a_);
        bLength2_ = (b_ * b_);
        normal_.normalize();
        calculateInvArea();
    }


    /*! \brief Constructor with initialization.
     *  \param[in] point Vertex point of square.
     *  \param[in] a First vector spanning the square.
     *  \param[in] b Second vector spanning the square.
     *  \param[in] normal Normal vector of square plane.
     *  \param[in] material Material assigned to the Square.
     */
    template <class SpectrumType>
    Square<SpectrumType>::Square(const Point3D& point, const Vector3D& a, const Vector3D& b, const Normal& normal, std::shared_ptr<Material<SpectrumType> > material)
    : AreaLightObject<SpectrumType>(material)
    , point_(point)
    , a_(a)
    , b_(b)
    , normal_(normal)
    {
        aLength2_ = (a_ * a_);
        bLength2_ = (b_ * b_);
        normal_.normalize();
        calculateInvArea();
    }



    /*! \brief Copy constructor.
     *  \param[in] other Square object to copy.
     */
    template <class SpectrumType>
    Square<SpectrumType>::Square(const Square<SpectrumType>& other)
        : AreaLightObject<SpectrumType>(other)
        , point_(other.point_)
        , a_(other.a_)
        , aLength2_(other.aLength2_)
        , b_(other.b_)
        , bLength2_(other.bLength2_)
        , normal_(other.normal_)
        , invArea_(other.invArea_)
    {
        this->objectMaterial_ = other.objectMaterial_;
        if (other.sampler_)
        {
            this->setSampler(std::shared_ptr<Sampler>(other.sampler_->clone()));
        }
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    Square<SpectrumType>::~Square()
    {
    }



    /*! \brief Clone (make deep copy of) object.
     *  \return Returns cloned object.
     */
    template <class SpectrumType>
    Square<SpectrumType>* Square<SpectrumType>::clone() const
    {
        return new Square<SpectrumType>(*this);
    }



    /*! \brief Checks if the given ray hits the geometric object and computes hit point properties.
     *  \param[in] ray Ray object to intersect.
     *  \param[out] tmin Minimum ray parameter t to consider a hit.
     *  \param[out] shadingData Hit point properties used for shading.
     *  \return Returns TRUE, if the ray hits the object, else FALSE.
     */
    template <class SpectrumType>
    bool Square<SpectrumType>::hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const
    {
        double t = (point_ - ray.origin_) * normal_ / (ray.direction_ * normal_);
        
        if (t <= EPS || t != t)
        {
            return false;
        }
        
        Point3D point = ray.origin_ + t * ray.direction_;
        Vector3D o(point - point_);
        
        double aCoordinate2 = o * a_;
        
        if (aCoordinate2 < 0.0 || aCoordinate2 > aLength2_)
        {
            return false;
        }
        
        double bCoordinate2 = o * b_;
        
        if (bCoordinate2 < 0.0 || bCoordinate2 > bLength2_)
        {
            return false;
        }
        
        tmin = t;
        shadingData.hitAnObject_ = true;
        if (normal_ * ray.direction_ > 0)
        {
            shadingData.normal_ = -normal_;
            shadingData.normalFlipped_ = true;
        }
        else
        {
            shadingData.normal_ = normal_;
        }
        shadingData.hitPoint_ = point;
        calculateLocalHitPoint(shadingData);
        shadingData.ray_ = ray;
        shadingData.material_ = this->objectMaterial_;
        
        return true;
    }



    /*! \brief Intersects object with given shadow ray.
     *  \param[in] shadowRay Ray for intersection.
     *  \param[out] tmin Ray-parameter of hit point.
     *  \return Returns TRUE, if ray and object intersect, else FLASE.
     */
    template <class SpectrumType>
    bool Square<SpectrumType>::shadowHit(const Ray& shadowRay, double& tmin) const
    {
        if (!this->castsShadows_)
        {
            return false;
        }

        double t = (point_ - shadowRay.origin_) * normal_ / (shadowRay.direction_ * normal_);
        
        if (t <= EPS || t != t)
        {
            return false;
        }
        
        Point3D point = shadowRay.origin_ + t * shadowRay.direction_;
        Vector3D o(point - point_);
        
        double aCoordinate2 = o * a_;
        
        if (aCoordinate2 < 0.0 || aCoordinate2 > aLength2_)
        {
            return false;
        }
        
        double bCoordinate2 = o * b_;
        
        if (bCoordinate2 < 0.0 || bCoordinate2 > bLength2_)
        {
            return false;
        }
        
        tmin = t;
        return true;
    }



    /*! \brief Computes and returns the bounding box of the square.
     *  \return Bounding box of square.
     */
    template <class SpectrumType>
    BoundingBox Square<SpectrumType>::getBoundingBox() const
    {
        Point3D pa = point_ + a_;
        Point3D pb = point_ + b_;
        Point3D pab = pa + b_;
        return BoundingBox(
            std::min(std::min(point_.x_, pa.x_), std::min(pb.x_, pab.x_)),
            std::min(std::min(point_.y_, pa.y_), std::min(pb.y_, pab.y_)),
            std::min(std::min(point_.z_, pa.z_), std::min(pb.z_, pab.z_)),
            std::max(std::max(point_.x_, pa.x_), std::max(pb.x_, pab.x_)),
            std::max(std::max(point_.y_, pa.y_), std::max(pb.y_, pab.y_)),
            std::max(std::max(point_.z_, pa.z_), std::max(pb.z_, pab.z_)));
    }



    /*! \brief Returns vertex point of square.
     *  \return Vertex point of square.
     */
    template <class SpectrumType>
    Point3D Square<SpectrumType>::getPoint() const
    {
        return point_;
    }



    /*! \brief Sets vertex point of square.
     *  \param[in] point Vertex point of square.
     */
    template <class SpectrumType>
    void Square<SpectrumType>::setPoint(const Point3D& point)
    {
        point_ = point;
    }



    /*! \brief Sets vertex point of square.
     *  \param[in] x X-coordinate of vertex point of square.
     *  \param[in] y Y-coordinate of vertex point of square.
     *  \param[in] z Z-coordinate of vertex point of square.
     */
    template <class SpectrumType>
    void Square<SpectrumType>::setPoint(double x, double y, double z)
    {
        point_.x_ = x;
        point_.y_ = y;
        point_.z_ = z;
    }



    /*! \brief Returns normal of square.
     *  \return Normal of square.
     */
    template <class SpectrumType>
    Normal Square<SpectrumType>::getNormal() const
    {
        return normal_;
    }



    /*! \brief Sets normal of square.
     *  \param[in] normal Normal of square.
     *
     *  \attention Method does not adjust vector a or b.
     */
    template <class SpectrumType>
    void Square<SpectrumType>::setNormal(const Normal& normal)
    {
        normal_ = normal;
    }



    /*! \brief Sets normal of square.
     *  \param[in] x X-coordinate of normal of square.
     *  \param[in] y Y-coordinate of normal of square.
     *  \param[in] z Z-coordinate of normal of square.
     *
     *  \attention Method does not adjust vector a or b.
     */
    template <class SpectrumType>
    void Square<SpectrumType>::setNormal(double x, double y, double z)
    {
        normal_.x_ = x;
        normal_.y_ = y;
        normal_.z_ = z;
    }



    /*! \brief Get first spanning vector of square.
     *  \return First spanning vector of square.
     */
    template <class SpectrumType>
    Vector3D Square<SpectrumType>::getA() const
    {
        return a_;
    }



    /*! \brief Set first spanning vector of square.
     *  \param[in] a First spanning vector of square.
     *
     *  \attention Method does not adjust vector b or normal.
     */
    template <class SpectrumType>
    void Square<SpectrumType>::setA(const Vector3D& a)
    {
        a_ = a;
        aLength2_ = (a_ * a_);
        calculateInvArea();
    }



    /*! \brief Get second spanning vector of square.
     *  \return Second spanning vector of square.
     */
    template <class SpectrumType>
    Vector3D Square<SpectrumType>::getB() const
    {
        return b_;
    }



    /*! \brief Set second spanning vector of square.
     *  \param[in] b Second spanning vector of square.
     *
     *  \attention Method does not adjust vector a or normal.
     */
    template <class SpectrumType>
    void Square<SpectrumType>::setB(const Vector3D& b)
    {
        b_ = b;
        bLength2_ = (b_ * b_);
        calculateInvArea();
    }



    /*! \brief Sample object surface.
     *  \return Returns sample point on surface.
     */
    template <class SpectrumType>
    Point3D Square<SpectrumType>::sample()
    {
        Point2D samplePoint = this->sampler_->sampleUnitSquare();
        return (point_ + samplePoint.x_ * a_ + samplePoint.y_ * b_);
    }



    /*! \brief Get probability density function of samples on light area.
     *  \param[in] shadingData Shading data of the ray-object intersection.
     *  \return Returns probability density function at sample position.
     */
    template <class SpectrumType>
    float Square<SpectrumType>::pdf(const ShadingData<SpectrumType>& shadingData)
    {
        return invArea_;
    }



    /*! \brief Get normal at specified surface point.
     *  \param[in] point Point on object surface.
     *  \return Returns normal.
     */
    template <class SpectrumType>
    Normal Square<SpectrumType>::getNormal(const Point3D& point)
    {
        return normal_;
    }



    /*! \brief Calculates the two vectors forming the plane used for a local coordinate system and their
     *  normal vector.
     */
    template <class SpectrumType>
    void Square<SpectrumType>::calculatePlaneVectors()
    {
        b_ = b_ - a_ * (b_ * a_); // make b perpendicular to a
        normal_ = a_.crossProduct(b_); // calculate normal
        normal_.normalize();
        aLength2_ = (a_ * a_);
        bLength2_ = (b_ * b_);
    }



    /*! \brief Calculates the localHitpoint.
     *  \param[in,out] shadingDataObject Shading data of this hit point. Member localHitPoint_ is set.
     *
     *  This method calculates the hit point coordinates in a 2D cartesian coordinate system originated
     *  in the planes origin. Values for localHitPoint_(u, v) are calculated by projecting the hitPoint
     *  onto the two planeVectors.
     */
    template <class SpectrumType>
    void Square<SpectrumType>::calculateLocalHitPoint(ShadingData<SpectrumType>& shadingDataObject) const
    {
        Vector3D vectorOnPlane(shadingDataObject.hitPoint_ - point_);
        float u = static_cast<float>(a_ * vectorOnPlane / aLength2_);
        float v = static_cast<float>(b_ * vectorOnPlane / bLength2_);
        shadingDataObject.localHitPoint2D_ = Point2D(u, v);
        shadingDataObject.localHitPoint3D_ = Point3D(u, v, 0);
    }



    /*! \brief Calculates inverse area of surface.
     */
    template <class SpectrumType>
    void Square<SpectrumType>::calculateInvArea()
    {
        invArea_ = 1.f / static_cast<float>((a_.crossProduct(b_)).length());
    }

} // end of namespace iiit

#endif // __SQUARE_HPP__
