// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file GeometricObject.hpp
 *  \author Thomas Nuernberg
 *  \date 09.01.2015
 *  \brief Definition of base class for geometric objects.
 */
 
#ifndef __GEOMETRICOBJECT_HPP__
#define __GEOMETRICOBJECT_HPP__


#include <memory>

#include "BoundingBox.hpp"
#include "Utilities/Ray.hpp"
#include "Utilities/ShadingData.hpp"
#include "Materials/Matte.hpp"



namespace iiit
{
    
    /*! \brief Abstract base class for geometric objects.
     */
    template <class SpectrumType>
    class GeometricObject
    {
    public:
        /*! This enum type defines valid geometric objects. The objects are defined in specialized
         *  classes.
         */
        enum Type
        {
            Sphere, ///< Sphere object.
            Plane, ///< Infinite Plane object.
            Triangle, ///< Finite planar triangle.
            Box, ///< Axis-aligned box.
            Disk, ///< Circular planar disk.
            Square, ///< Planar square.
            OpenCylinder, ///< Generic cylinder object.
            OpenCone, ///< Generic cone object.
            Torus, ///< Generic torus object.
            Compound, ///< Compound object.
            TriangleMesh, ///< Triangle mesh object.
            FlatTriangle, ///< Flat triangle of triangle mesh.
            SmoothTriangle ///< Smooth triangle of triangle mesh.
        };

        GeometricObject();
        GeometricObject(std::shared_ptr<Material<SpectrumType> > material);
        GeometricObject(const SpectrumType& color);
        GeometricObject(const GeometricObject<SpectrumType>& other);

        virtual ~GeometricObject() {};

        /*! \brief Deep copies the geometric object.
         *  \return Returns the copied geometric object.
         */
        virtual GeometricObject<SpectrumType>* clone() const = 0;

        /*! \brief Get the type of the geometric object.
         *  \return Returns the type of the geometric object.
         */
        virtual typename GeometricObject<SpectrumType>::Type getType() const = 0;

        /*! \brief Intersects object with given ray.
         *  \param[in] ray Ray for intersection.
         *  \param[out] tmin Ray-parameter of hit point.
         *  \param[out] shadingData Shading data structure of hit point.
         *  \return Returns TRUE, if ray and object intersect, else FLASE.
         */
        virtual bool hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const = 0;

        /*! \brief Intersects object with given shadow ray.
         *  \param[in] shadowRay Ray for intersection.
         *  \param[out] tmin Ray-parameter of hit point.
         *  \return Returns TRUE, if ray and object intersect, else FLASE.
         */
        virtual bool shadowHit(const Ray& shadowRay, double& tmin) const = 0;
        
        /*! \brief Get the bounding box of the object.
         *  \return Bounding box of object.
         *
         *  The default bounding box is an unbound box of infinite size. Therefore the default box is
         *  always hit by rays.
         *
         *  This method must be overloaded by objects that are to be placed in a Grid object. Depending
         *  on the complexity of the object, the BoundingBox object should either be computed on
         *  on function call or be stored as member of the object.
         */
        virtual BoundingBox getBoundingBox() const {return BoundingBox();};

        virtual void setMaterial(std::shared_ptr<Material<SpectrumType> > material);

        void setCastsShadows(bool castsShadows);

        std::shared_ptr<Material<SpectrumType> > objectMaterial_; ///< Material assigned to the object.
        
    protected:
        bool castsShadows_; ///< Flag indicating whether object casts shadows.
    };
    
    
    
    /*! \brief Default constructor.
     *
     *  \attention The object material is left uninitialized by default in order to reduce memory
     *  consumption of compound objects.
     */
    template <class SpectrumType>
    GeometricObject<SpectrumType>::GeometricObject()
        : castsShadows_(true)
    {
    }
    
    
    
    /*! \brief Constructor with material initialization.
     *  \param[in] material Material of object.
     */
    template <class SpectrumType>
    GeometricObject<SpectrumType>::GeometricObject(std::shared_ptr<Material<SpectrumType> > material)
        : objectMaterial_(material)
        , castsShadows_(true)
    {
    }
    
    
    
    /*! \brief Constructor with color initialization.
     *  \param[in] color Color of object.
     *
     *  Initializes object material to matte with given color.
     */
    template <class SpectrumType>
    GeometricObject<SpectrumType>::GeometricObject(const SpectrumType& color)
        : objectMaterial_(std::shared_ptr<Matte<SpectrumType> >(new Matte<SpectrumType>(color)))
        , castsShadows_(true)
    {
    }
    
    
    
    /*! \brief Copy constructor.
     *  \param[in] other Geometric object to copy.
     */
    template <class SpectrumType>
    GeometricObject<SpectrumType>::GeometricObject(const GeometricObject<SpectrumType>& other)
     : objectMaterial_(other.objectMaterial_)
     , castsShadows_(other.castsShadows_)
    {
    }
    
    
    
    /*! \brief Set material.
     *  \param[in] material Object material.
     */
    template <class SpectrumType>
    void GeometricObject<SpectrumType>::setMaterial(std::shared_ptr<Material<SpectrumType> > material)
    {
        objectMaterial_ = material;
    }



    /*! \brief Set whether object casts shadows.
     *  \param[in] castsShadows Shadow flag.
     */
    template <class SpectrumType>
    void GeometricObject<SpectrumType>::setCastsShadows(bool castsShadows)
    {
        castsShadows_ = castsShadows;
    }

} // end of namespace iiit

#endif // __GEOMETRICOBJECT_HPP__