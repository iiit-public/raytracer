// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file OpenCylinder.hpp
 *  \author Thomas Nuernberg
 *  \date 09.01.2015
 *  \brief Definition of class OpenCylinder.
 */

#ifndef __OPENCYLINDER_HPP__
#define __OPENCYLINDER_HPP__

#include <math.h>

#include "GeometricObject.hpp"
#include "BoundingBox.hpp"
#include "Utilities/Constants.h"
#include "Utilities/Ray.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Normal.hpp"
#include "Materials/Material.hpp"



namespace iiit
{

    /*! \brief Generic %OpenCylinder object.
     *
     *  This class represents a generic cylinder surface, meaning that it can only be defined in one
     *  orientation. Use the Instance mechanism to transform it to arbitrary cylindric surfaces.
     */
    template <class SpectrumType>
    class OpenCylinder : public GeometricObject<SpectrumType>
    {
    public:
        OpenCylinder();
        OpenCylinder(const Point3D& center, double radius, double zMin, double zMax);
        OpenCylinder(const Point3D& center, double radius, double zMin, double zMax, std::shared_ptr<Material<SpectrumType> > material);
        OpenCylinder(const OpenCylinder<SpectrumType>& other);
        virtual ~OpenCylinder();

        virtual OpenCylinder<SpectrumType>* clone() const;

        /*! \brief Get the type of the geometric object.
         *  \return Returns the type of the geometric object.
         */
        virtual typename GeometricObject<SpectrumType>::Type getType() const {return GeometricObject<SpectrumType>::OpenCylinder;}
        virtual bool hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const;
        virtual bool shadowHit(const Ray& shadowRay, double& tmin) const;
        virtual BoundingBox getBoundingBox() const;

        Point3D getCenter() const;
        void setCenter(const Point3D& center);
        void setCenter(double x, double y, double z);

        double getRadius() const;
        void setRadius(double radius);

        double getZMin() const;
        void setZMin(double zMin);

        double getZMax() const;
        void setZMax(double zMax);

    private:
        void calculateLocalHitPoint(ShadingData<SpectrumType>& shadingDataObject) const;

        Point3D center_; ///< Center of cylinder.
        double radius_; ///< Radius of cylinder.
        double zMin_; ///< Minimum z-coordinate of cylinder.
        double zMax_; ///< Maximum z-coordinate of cylinder.
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    OpenCylinder<SpectrumType>::OpenCylinder()
        : GeometricObject<SpectrumType>(SpectrumType(0.5f))
        , center_()
        , radius_(0.0)
        , zMin_(0.0)
        , zMax_(1.0)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] center Center of cylinder in 3D.
     *  \param[in] radius Radius of cylinder.
     *  \param[in] zMin Minimum z-coordinate of cylinder.
     *  \param[in] zMax Maximum z-coordinate of cylinder.
     */
    template <class SpectrumType>
    OpenCylinder<SpectrumType>::OpenCylinder(const Point3D& center, double radius, double zMin, double zMax)
        : GeometricObject<SpectrumType>(SpectrumType(0.5f))
        , center_(center)
        , radius_(radius)
        , zMin_(zMin)
        , zMax_(zMax)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] center Center of cylinder in 3D.
     *  \param[in] radius Radius of cylinder.
     *  \param[in] zMin Minimum z-coordinate of cylinder.
     *  \param[in] zMax Maximum z-coordinate of cylinder.
     *  \param[in] material Material assigned to the Plane.
     */
    template <class SpectrumType>
    OpenCylinder<SpectrumType>::OpenCylinder(const Point3D& center, double radius, double zMin, double zMax, std::shared_ptr<Material<SpectrumType> > material)
        : GeometricObject<SpectrumType>(material)
        , center_(center)
        , radius_(radius)
        , zMin_(zMin)
        , zMax_(zMax)
    {
    }



    /*! \brief Copy constructor.
     *  \param[in] other Cylinder object to copy.
     */
    template <class SpectrumType>
    OpenCylinder<SpectrumType>::OpenCylinder(const OpenCylinder<SpectrumType>& other)
        : GeometricObject<SpectrumType>(other)
        , center_(other.center_)
        , radius_(other.radius_)
        , zMin_(other.zMin_)
        , zMax_(other.zMax_)
    {
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    OpenCylinder<SpectrumType>::~OpenCylinder()
    {
    }



    /*! \brief Clone (make deep copy of) object.
     *  \return Returned cloned object.
     */
    template <class SpectrumType>
    OpenCylinder<SpectrumType>* OpenCylinder<SpectrumType>::clone() const
    {
        return new OpenCylinder<SpectrumType>(*this);
    }



    /*! \brief Intersects object with given ray.
     *  \param[in] ray Ray for intersection.
     *  \param[out] tmin Ray-parameter of hit point.
     *  \param[out] shadingData Shading data structure of hit point.
     *  \return Returns TRUE, if ray and object intersect, else FLASE.
     */
    template <class SpectrumType>
    bool OpenCylinder<SpectrumType>::hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const
    {
        double t;
        Vector3D temp = ray.origin_ - center_;
        double a = ray.direction_.x_ * ray.direction_.x_ + ray.direction_.y_ * ray.direction_.y_;
        double b = 2 * (temp.x_ * ray.direction_.x_ + temp.y_ * ray.direction_.y_);
        double c = temp.x_ * temp.x_ + temp.y_ * temp.y_ - radius_ * radius_;
        double disc = b * b - 4 * a * c;
        
        if (disc < 0.0)
        {
            return false;
        }
        else
        {
            double e = sqrt(disc);
            double denom = 2.0 * a;
            t = (-b - e) / denom;
            
            if (t > EPS)
            {
                double z = ray.origin_.z_ + t * ray.direction_.z_;
                if (z > zMin_ && z < zMax_)
                {
                    tmin = t;
                    shadingData.hitAnObject_ = true;
                    shadingData.normal_ = Normal(temp.x_ + t * ray.direction_.x_, temp.y_ + t * ray.direction_.y_, 0.0);
                    shadingData.normal_.normalize();
                    shadingData.hitPoint_ = ray.origin_ + t * ray.direction_;
                    shadingData.ray_ = ray;
                    shadingData.material_ = this->objectMaterial_;
                    
                    if (-ray.direction_ * shadingData.normal_ < 0.0)
                    {
                        shadingData.normal_ = -shadingData.normal_;
                        shadingData.normalFlipped_ = true;
                    }
                    
                    // calculate hit point in local coordinate system
                    calculateLocalHitPoint(shadingData);
                    
                    return true;
                }
            }
            t = (-b + e) / denom;
            if (t > EPS)
            {
                double z = ray.origin_.z_ + t * ray.direction_.z_;
                if (z > zMin_ && z < zMax_)
                {
                    tmin = t;
                    shadingData.hitAnObject_ = true;
                    shadingData.normal_ = Normal(temp.x_ + t * ray.direction_.x_, temp.y_ + t * ray.direction_.y_, 0.0);
                    shadingData.normal_.normalize();
                    shadingData.hitPoint_ = ray.origin_ + t * ray.direction_;
                    shadingData.ray_ = ray;
                    shadingData.material_ = this->objectMaterial_;
                    
                    if (-ray.direction_ * shadingData.normal_ < 0.0)
                    {
                        shadingData.normal_ = -shadingData.normal_;
                        shadingData.normalFlipped_ = true;
                    }
                    
                    // calculate hit point in local coordinate system
                    calculateLocalHitPoint(shadingData);
                    
                    return true;
                }
            }
        }
        
        return false;
    }



    /*! \brief Intersects object with given shadow ray.
     *  \param[in] shadowRay Ray for intersection.
     *  \param[out] tmin Ray-parameter of hit point.
     *  \return Returns TRUE, if ray and object intersect, else FLASE.
     */
    template <class SpectrumType>
    bool OpenCylinder<SpectrumType>::shadowHit(const Ray& shadowRay, double& tmin) const
    {
        if (!this->castsShadows_)
        {
            return false;
        }

        double t;
        Vector3D temp = shadowRay.origin_ - center_;
        double a = shadowRay.direction_.x_ * shadowRay.direction_.x_ + shadowRay.direction_.y_ * shadowRay.direction_.y_;
        double b = 2 * (temp.x_ * shadowRay.direction_.x_ + temp.y_ * shadowRay.direction_.y_);
        double c = temp.x_ * temp.x_ + temp.y_ * temp.y_ - radius_ * radius_;
        double disc = b * b - 4 * a * c;
        
        if (disc < 0.0)
        {
            return false;
        }
        else
        {
            double e = sqrt(disc);
            double denom = 2.0 * a;
            t = (-b - e) / denom;
            
            if (t > EPS)
            {
                double z = shadowRay.origin_.z_ + t * shadowRay.direction_.z_;
                if (z > zMin_ && z < zMax_)
                {
                    tmin = t;
                    return true;
                }
            }
            t = (-b + e) / denom;
            if (t > EPS)
            {
                double z = shadowRay.origin_.z_ + t * shadowRay.direction_.z_;
                if (z > zMin_ && z < zMax_)
                {
                    tmin = t;
                    return true;
                }
            }
        }
        
        return false;
    }



    /*! \brief Computes and returns the bounding box of the cylinder.
     *  \return Bounding box of cylinder.
     */
    template <class SpectrumType>
    BoundingBox OpenCylinder<SpectrumType>::getBoundingBox() const
    {
        return BoundingBox(
            center_.x_ - radius_,
            center_.y_ - radius_,
            zMin_,
            center_.x_ + radius_,
            center_.y_ + radius_,
            zMax_);
    }



    /*! \brief Returns center of cylinder.
     *  \return Center of cylinder.
     */
    template <class SpectrumType>
    Point3D OpenCylinder<SpectrumType>::getCenter() const
    {
        return center_;
    }



    /*! \brief Sets center of cylinder.
     *  \param[in] center Center of cylinder.
     */
    template <class SpectrumType>
    void OpenCylinder<SpectrumType>::setCenter(const Point3D& center)
    {
        center_ = center;
    }



    /*! \brief Sets center of cylinder.
     *  \param[in] x X-coordinate of center of cylinder.
     *  \param[in] y Y-coordinate of center of cylinder.
     *  \param[in] z Z-coordinate of center of cylinder.
     */
    template <class SpectrumType>
    void OpenCylinder<SpectrumType>::setCenter(double x, double y, double z)
    {
        center_.x_ = x;
        center_.y_ = y;
        center_.z_ = z;
    }



    /*! \brief Returns radius of cylinder.
     *  \return Radius of cylinder.
     */
    template <class SpectrumType>
    double OpenCylinder<SpectrumType>::getRadius() const
    {
        return radius_;
    }



    /*! \brief Sets radius of cylinder.
     *  \param[in] radius Radius of cylinder.
     */
    template <class SpectrumType>
    void OpenCylinder<SpectrumType>::setRadius(double radius)
    {
        radius_ = radius;
    }



    /*! \brief Get minimum cylinder z-coodinate.
     *  \return Minimum cylinder z-coordinate.
     */
    template <class SpectrumType>
    double OpenCylinder<SpectrumType>::getZMin() const
    {
        return zMin_;
    }



    /*! \brief Set minimum cylinder z-coodinate.
     *  \param[in] zMin Minimum cylinder z-coordinate.
     */
    template <class SpectrumType>
    void OpenCylinder<SpectrumType>::setZMin(double zMin)
    {
        zMin_ = zMin;
    }



    /*! \brief Get maximum cylinder z-coodinate.
     *  \return Maximum cylinder z-coordinate.
     */
    template <class SpectrumType>
    double OpenCylinder<SpectrumType>::getZMax() const
    {
        return zMax_;
    }



    /*! \brief Set maximum cylinder z-coodinate.
     *  \param[in] zMax Maximum cylinder z-coordinate.
     */
    template <class SpectrumType>
    void OpenCylinder<SpectrumType>::setZMax(double zMax)
    {
        zMax_ = zMax;
    }



    /*! \brief Calculates the localHitpoint.
     *  \param[in] shadingDataObject Shading data of this hit point. Member localHitPoint_ is set.
     *
     *  This method calculates the hit point coordinates in a polar coordinate system originated in
     *  this cylinders center. Values for localHitPoint_(u, v) are in the ranges u:[0, 1] and
     *  v:[0, 360deg].
     */
    template <class SpectrumType>
    void OpenCylinder<SpectrumType>::calculateLocalHitPoint(ShadingData<SpectrumType>& shadingDataObject) const
    {
        Vector3D vectorCenterToHitpoint(shadingDataObject.hitPoint_ - center_);
        float u = static_cast<float>((shadingDataObject.hitPoint_.z_ - zMin_) / (zMax_ - zMin_));
        float v = static_cast<float>(atan2(vectorCenterToHitpoint(2), vectorCenterToHitpoint(1)) * 180 * INV_PI);
        if (v < 0.0)
        {
            v += 360.0f;
        }
        shadingDataObject.localHitPoint2D_ = Point2D(u, v);
        shadingDataObject.localHitPoint3D_ = vectorCenterToHitpoint;
    }

} // end of namespace iiit

#endif // __OPENCYLINDER_HPP__
