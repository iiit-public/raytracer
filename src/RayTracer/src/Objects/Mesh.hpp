// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Mesh.hpp
 *  \author Thomas Nuernberg
 *  \date 18.05.2015
 *  \brief Definition of class Mesh.
 */

#ifndef __MESH_HPP__
#define __MESH_HPP__

#include <vector>

#include "Utilities/Point3D.hpp"
#include "Utilities/Normal.hpp"



namespace iiit
{

    /*! \brief %Mesh object.
     */
    class Mesh
    {
    public:
        Mesh();
        Mesh(const Mesh& other);
        ~Mesh();

        std::vector<Point3D> vertices_; ///< Vector storing all vertices of the mesh.
        std::vector<Normal> normals_; ///< Vector storing all average normals of all vertices of the mesh (only used for smooth shading).
        std::vector<std::vector<int> > vertexFaces_; ///< Vector storing the indices of all faces that share the respective vertex (only used for smooth shading).
        int numVertices_; ///< Total number of vertices.
        int numTriangles_; ///< Total number of triangle faces.
    };

} // end of namespace iiit

#endif // __MESH_HPP__