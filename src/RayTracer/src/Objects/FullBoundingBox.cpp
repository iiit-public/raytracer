// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file FullBoundingBox.cpp
 *  \author Thomas Nuernberg
 *  \date 21.05.2015
 *  \brief Implementation of class FullBoudingBox.
 */

#include "FullBoundingBox.hpp"

#include <limits>



namespace iiit
{

    /*! \brief Constructor from axis-aligned bounding box.
     *  \param[in] boundingBox Axis-aligned bounding box.
     */
    FullBoundingBox::FullBoundingBox(const BoundingBox& boundingBox)
    {
        vertexes_.reserve(8);
        vertexes_.push_back(Point3D(boundingBox.x0_, boundingBox.y0_, boundingBox.z0_));
        vertexes_.push_back(Point3D(boundingBox.x0_, boundingBox.y1_, boundingBox.z0_));
        vertexes_.push_back(Point3D(boundingBox.x1_, boundingBox.y0_, boundingBox.z0_));
        vertexes_.push_back(Point3D(boundingBox.x1_, boundingBox.y1_, boundingBox.z0_));
        vertexes_.push_back(Point3D(boundingBox.x0_, boundingBox.y0_, boundingBox.z1_));
        vertexes_.push_back(Point3D(boundingBox.x0_, boundingBox.y1_, boundingBox.z1_));
        vertexes_.push_back(Point3D(boundingBox.x1_, boundingBox.y0_, boundingBox.z1_));
        vertexes_.push_back(Point3D(boundingBox.x1_, boundingBox.y1_, boundingBox.z1_));
    }



    /*! \brief Constructor from vertexes.
     *  \param[in] p0 Vertex of bounding box.
     *  \param[in] p1 Vertex of bounding box.
     *  \param[in] p2 Vertex of bounding box.
     *  \param[in] p3 Vertex of bounding box.
     *  \param[in] p4 Vertex of bounding box.
     *  \param[in] p5 Vertex of bounding box.
     *  \param[in] p6 Vertex of bounding box.
     *  \param[in] p7 Vertex of bounding box.
     */
    FullBoundingBox::FullBoundingBox(const Point3D& p0, const Point3D& p1, const Point3D& p2, const Point3D& p3,
                                     const Point3D& p4, const Point3D& p5, const Point3D& p6, const Point3D& p7)
    {
        vertexes_.reserve(8);
        vertexes_.push_back(p0);
        vertexes_.push_back(p1);
        vertexes_.push_back(p2);
        vertexes_.push_back(p3);
        vertexes_.push_back(p4);
        vertexes_.push_back(p5);
        vertexes_.push_back(p6);
        vertexes_.push_back(p7);
    }



    /*! \brief Default destructor.
     */
    FullBoundingBox::~FullBoundingBox()
    {
    }



    /*! \brief Computes and returns axis-aligned bounding box.
     *  \return Axis-aligned bounding box.
     */
    BoundingBox FullBoundingBox::getAxisAlignedBoundingBox() const
    {
        Point3D p0(std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity());
        Point3D p1(-std::numeric_limits<float>::infinity(), -std::numeric_limits<float>::infinity(), -std::numeric_limits<float>::infinity());

        for (std::vector<Point3D>::const_iterator it = vertexes_.begin(); it != vertexes_.end(); ++it)
        {
            if (it->x_ < p0.x_)
            {
                p0.x_ = it->x_;
            }
            if (it->x_ > p1.x_)
            {
                p1.x_ = it->x_;
            }
            if (it->y_ < p0.y_)
            {
                p0.y_ = it->y_;
            }
            if (it->y_ > p1.y_)
            {
                p1.y_ = it->y_;
            }
            if (it->z_ < p0.z_)
            {
                p0.z_ = it->z_;
            }
            if (it->z_ > p1.z_)
            {
                p1.z_ = it->z_;
            }
        }
        return BoundingBox(p0.x_, p0.y_, p0.z_, p1.x_, p1.y_, p1.z_);
    }

} // end of namespace iiit