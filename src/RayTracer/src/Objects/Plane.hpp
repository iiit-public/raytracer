// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Plane.hpp
 *  \author Thomas Nuernberg
 *  \date 09.01.2015
 *  \brief Definition of class Plane.
 */

#ifndef __PLANE_HPP__
#define __PLANE_HPP__

#include <math.h>

#include "GeometricObject.hpp"
#include "Utilities/Constants.h"
#include "Utilities/Ray.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Normal.hpp"
#include "Materials/Material.hpp"



namespace iiit
{

    /*! \brief %Plane object.
     */
    template <class SpectrumType>
    class Plane : public GeometricObject<SpectrumType>
    {
    public:
        Plane();
        Plane(const Point3D& point, const Normal& normal);
        Plane(double x, double y, double z, double nx, double ny, double nz);
        Plane(double x, double y, double z, double nx, double ny, double nz, std::shared_ptr<Material<SpectrumType> > material);
        Plane(const Point3D& point, const Normal& normal, std::shared_ptr<Material<SpectrumType> > material);
        Plane(const Plane<SpectrumType>& other);
        virtual ~Plane();

        virtual Plane<SpectrumType>* clone() const;

        /*! \brief Get the type of the geometric object.
         *  \return Returns the type of the geometric object.
         */
        virtual typename GeometricObject<SpectrumType>::Type getType() const {return GeometricObject<SpectrumType>::Plane;}
        virtual bool hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const;
        virtual bool shadowHit(const Ray& shadowRay, double& tmin) const;

        Point3D getPoint() const;
        void setPoint(const Point3D& point);
        void setPoint(double x, double y, double z);

        Normal getNormal() const;
        void setNormal(const Normal& normal);
        void setNormal(double x, double y, double z);

    private:
        void calculatePlaneVectors(void);
        void calculateLocalHitPoint(ShadingData<SpectrumType>& shadingDataObject) const;

        Point3D point_; ///< Point on plane.
        Normal normal_; ///< Normal of plane.
        Vector3D planeVector1_; ///< One of the two vectors forming the plane.
        Vector3D planeVector2_; ///< One of the two vectors forming the plane.
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Plane<SpectrumType>::Plane()
        : GeometricObject<SpectrumType>(SpectrumType(0.5f))
        , point_()
        , normal_()
    {
        calculatePlaneVectors();
    }



    /*! \brief Constructor with initialization.
     *  \param[in] point Point on plane in 3D.
     *  \param[in] normal Normal vector of plane.
     */
    template <class SpectrumType>
    Plane<SpectrumType>::Plane(const Point3D& point, const Normal& normal)
        : GeometricObject<SpectrumType>(SpectrumType(0.5f))
        , point_(point)
        , normal_(normal)
    {
        calculatePlaneVectors();
    }



    /*! \brief Constructor with initialization.
     *  \param[in] point Point on plane in 3D.
     *  \param[in] normal Normal vector of plane.
     *  \param[in] material Material assigned to the Plane.
     */
    template <class SpectrumType>
    Plane<SpectrumType>::Plane(const Point3D& point, const Normal& normal, std::shared_ptr<Material<SpectrumType> > material)
        : GeometricObject<SpectrumType>(material)
        , point_(point)
        , normal_(normal)
    {
        calculatePlaneVectors();
    }



    /*! \brief Constructor with initialization.
     *  \param[in] x X-coordinate of point on plane.
     *  \param[in] y Y-coordinate of point on plane.
     *  \param[in] z Z-coordinate of point on plane.
     *  \param[in] nx X-coordinate of normal vector.
     *  \param[in] ny Y-coordinate of normal vector.
     *  \param[in] nz Z-coordinate of normal vector.
     */
    template <class SpectrumType>
    Plane<SpectrumType>::Plane(double x, double y, double z, double nx, double ny, double nz)
        : GeometricObject<SpectrumType>(SpectrumType(0.5f))
        , point_(x, y, z)
        , normal_(nx, ny, nz)
    {
        calculatePlaneVectors();
    }


    /*! \brief Constructor with initialization.
     *  \param[in] x X-coordinate of point on plane.
     *  \param[in] y Y-coordinate of point on plane.
     *  \param[in] z Z-coordinate of point on plane.
     *  \param[in] nx X-coordinate of normal vector.
     *  \param[in] ny Y-coordinate of normal vector.
     *  \param[in] nz Z-coordinate of normal vector.
     *  \param[in] material Material assigned to the Plane.
     */
    template <class SpectrumType>
    Plane<SpectrumType>::Plane(double x, double y, double z, double nx, double ny, double nz, std::shared_ptr<Material<SpectrumType> > material)
        : GeometricObject<SpectrumType>(SpectrumType(0.5f))
        , point_(x, y, z)
        , normal_(nx, ny, nz)
    {
        this->objectMaterial_ = material;
        calculatePlaneVectors();
    }



    /*! \brief Copy constructor.
     *  \param[in] other Plane object to copy.
     */
    template <class SpectrumType>
    Plane<SpectrumType>::Plane(const Plane<SpectrumType>& other)
        : GeometricObject<SpectrumType>(other)
        , point_(other.point_)
        , normal_(other.normal_)
        , planeVector1_(other.planeVector1_)
        , planeVector2_(other.planeVector2_)
    {
        this->objectMaterial_ = other.objectMaterial_;
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    Plane<SpectrumType>::~Plane()
    {
    }



    /*! \brief Clone (make deep copy of) object.
     *  \return Returns cloned object.
     */
    template <class SpectrumType>
    Plane<SpectrumType>* Plane<SpectrumType>::clone() const
    {
        return new Plane<SpectrumType>(*this);
    }



    /*! \brief Checks if the given ray hits the geometric object and computes hit point properties.
     *  \param[in] ray Ray object to intersect.
     *  \param[out] tmin Minimum ray parameter t to consider a hit.
     *  \param[out] shadingData Hit point properties used for shading.
     *  \return Returns TRUE, if the ray hits the object, else FALSE.
     */
    template <class SpectrumType>
    bool Plane<SpectrumType>::hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const
    {
        double t = (point_ - ray.origin_) * normal_ / (ray.direction_ * normal_);
        
        if (t > EPS)
        {
            tmin = t;
            shadingData.hitAnObject_ = true;
            shadingData.normal_ = normal_;
            shadingData.hitPoint_ = ray.origin_ + t * ray.direction_;
            calculateLocalHitPoint(shadingData);
            shadingData.ray_ = ray;
            shadingData.material_ = this->objectMaterial_;
            
            return true;
        }
        return false;
    }



    /*! \brief Intersects object with given shadow ray.
     *  \param[in] shadowRay Ray for intersection.
     *  \param[out] tmin Ray-parameter of hit point.
     *  \return Returns TRUE, if ray and object intersect, else FLASE.
     */
    template <class SpectrumType>
    bool Plane<SpectrumType>::shadowHit(const Ray& shadowRay, double& tmin) const
    {
        if (!this->castsShadows_)
        {
            return false;
        }

        double t = (point_ - shadowRay.origin_) * normal_ / (shadowRay.direction_ * normal_);
        
        if (t > EPS)
        {
            tmin = t;
            return true;
        }
        return false;
    }



    /*! \brief Returns point of plane.
     *  \return Point of plane.
     */
    template <class SpectrumType>
    Point3D Plane<SpectrumType>::getPoint() const
    {
        return point_;
    }



    /*! \brief Sets point of plane.
     *  \param[in] point Point of plane.
     */
    template <class SpectrumType>
    void Plane<SpectrumType>::setPoint(const Point3D& point)
    {
        point_ = point;
    }



    /*! \brief Sets point of plane.
     *  \param[in] x X-coordinate of point of plane.
     *  \param[in] y Y-coordinate of point of plane.
     *  \param[in] z Z-coordinate of point of plane.
     */
    template <class SpectrumType>
    void Plane<SpectrumType>::setPoint(double x, double y, double z)
    {
        point_.x_ = x;
        point_.y_ = y;
        point_.z_ = z;
    }



    /*! \brief Returns normal of plane.
     *  \return Normal of plane.
     */
    template <class SpectrumType>
    Normal Plane<SpectrumType>::getNormal() const
    {
        return normal_;
    }



    /*! \brief Sets normal of plane.
     *  \param[in] normal Normal of plane.
     */
    template <class SpectrumType>
    void Plane<SpectrumType>::setNormal(const Normal& normal)
    {
        normal_ = normal;
    }



    /*! \brief Sets normal of plane.
     *  \param[in] x X-coordinate of normal of plane.
     *  \param[in] y Y-coordinate of normal of plane.
     *  \param[in] z Z-coordinate of normal of plane.
     */
    template <class SpectrumType>
    void Plane<SpectrumType>::setNormal(double x, double y, double z)
    {
        normal_.x_ = x;
        normal_.y_ = y;
        normal_.z_ = z;
    }



    /*! \brief Calculates the two vectors forming the plane used for a local coordinate system
     */
    template <class SpectrumType>
    void Plane<SpectrumType>::calculatePlaneVectors(void)
    {
        // first: find one vector which is not colinear to the normal vector <-> possible vector for the plane
        if (fabs(normal_(3)) > EPS)
        {
            planeVector1_ = Vector3D(1.0, 0.0, -normal_(1) / normal_(3));
        }
        else if (fabs(normal_(2)) > EPS)
        {
            planeVector1_ = Vector3D(1.0, -normal_(1) / normal_(2), 0.0);
        }
        else
        {
            planeVector1_ = Vector3D(0.0, 1.0, 0.0);
        }
        planeVector1_.normalize();
        
        // find second perpendicular to both the normal vector and planeVector1
        Vector3D normalVector(normal_.x_, normal_.y_, normal_.z_);
        planeVector2_ = normalVector.crossProduct(planeVector1_);
        planeVector2_.normalize();
    }



    /*! \brief Calculates the localHitpoint.
     *  \param[in,out] shadingDataObject Shading data of this hit point. Member localHitPoint_ is set.
     *
     *  This method calculates the hit point coordinates in a 2D cartesian coordinate system originated
     *  in the planes origin. Values for localHitPoint_(u, v) are calculated by projecting the hitPoint
     *  onto the two planeVectors.
     */
    template <class SpectrumType>
    void Plane<SpectrumType>::calculateLocalHitPoint(ShadingData<SpectrumType>& shadingDataObject) const
    {
        Vector3D vectorOnPlane(shadingDataObject.hitPoint_ - point_);
        float u = static_cast<float>(planeVector1_ * vectorOnPlane);
        float v = static_cast<float>(planeVector2_ * vectorOnPlane);
        shadingDataObject.localHitPoint2D_ = Point2D(u, v);
        shadingDataObject.localHitPoint3D_ = Point3D(u, v, 0);
    }

} // end of namespace iiit

#endif // __PLANE_HPP__
