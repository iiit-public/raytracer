// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Mesh.cpp
 *  \author Thomas Nuernberg
 *  \date 18.05.2015
 *  \brief Implementation of class Mesh.
 */

#include "Mesh.hpp"



namespace iiit
{

    /*! \brief Default constructor.
     */
    Mesh::Mesh()
        : numVertices_(0)
        , numTriangles_(0)
    {
    }



    /*! \brief Copy constructor.
     *  \param[in] other Other mesh object to copy.
     */
    Mesh::Mesh(const Mesh& other)
        : vertices_(other.vertices_)
        , normals_(other.normals_)
        , vertexFaces_(other.vertexFaces_)
        , numVertices_(other.numVertices_)
        , numTriangles_(other.numTriangles_)
    {
    }



    /*! \brief Default destructor.
     */
    Mesh::~Mesh()
    {
    }

} // end of namespace iiit