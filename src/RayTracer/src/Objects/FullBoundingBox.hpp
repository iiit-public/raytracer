// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file FullBoundingBox.hpp
 *  \author Thomas Nuernberg
 *  \date 21.05.2015
 *  \brief Definition of class FullBoundingBox.
 */
 
#ifndef __FULLBOUNDINGBOX_HPP__
#define __FULLBOUNDINGBOX_HPP__

#include <vector>

#include "BoundingBox.hpp"
#include "Utilities/Point3D.hpp"



namespace iiit
{
    
    /*! \brief Class representing axis-aligned rectangular bounding box.
     *
     *  This represents an axis-aligned rectangular bounding box that completely encloses an object.
     *  This class is used to check if a ray potentially intersects with a complex compound object
     *  inside the bounding box without having to check all the primitives of the object.
     *
     *  In contrast to the base class BoundingBox, FullBoundingBox stores all 8 vertexes of the box. The
     *  class is therefore able to represent non-axis-aligned bounding boxes. This are necessary to
     *  compute an estimate of the axis-aligned bounding box of a transformed object.
     */
    class FullBoundingBox
    {
    public:
        FullBoundingBox(const BoundingBox& boundingBox);
        FullBoundingBox(const Point3D& p0, const Point3D& p1, const Point3D& p2, const Point3D& p3,
            const Point3D& p4, const Point3D& p5, const Point3D& p6, const Point3D& p7);
        ~FullBoundingBox();

        BoundingBox getAxisAlignedBoundingBox() const;

        std::vector<Point3D> vertexes_; ///< Vector of 8 vertexes forming the bounding box.
    };

} // end of namespace iiit

#endif // __FULLBOUNDINGBOX_HPP__