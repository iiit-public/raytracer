// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Box.hpp
 *  \author Thomas Nuernberg
 *  \date 08.06.2015
 *  \brief Definition of class Box.
 */

#ifndef __BOX_HPP__
#define __BOX_HPP__

#include <math.h>

#include "GeometricObject.hpp"
#include "BoundingBox.hpp"
#include "Utilities/Constants.h"
#include "Utilities/Ray.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Normal.hpp"
#include "Materials/Material.hpp"



namespace iiit
{

    /*! \brief %Box object.
     *  \tparam SpectrumType Type of spectrum representation.
     */
    template <class SpectrumType>
    class Box : public GeometricObject<SpectrumType>
    {
    public:
        Box();
        Box(const Point3D& point0, const Point3D& point1);
        Box(const Point3D& point0, const Point3D& point1, std::shared_ptr<Material<SpectrumType> > material);
        Box(const Box<SpectrumType>& other);
        virtual ~Box();

        virtual Box* clone() const;

        /*! \brief Get the type of the geometric object.
         *  \return Returns the type of the geometric object.
         */
        virtual typename GeometricObject<SpectrumType>::Type getType() const {return GeometricObject<SpectrumType>::Box;}
        virtual bool hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const;
        virtual bool shadowHit(const Ray& shadowRay, double& tmin) const;
        virtual BoundingBox getBoundingBox() const;

        Point3D getVertex0() const;
        Point3D getVertex1() const;
        void setVertex0(const Point3D& vertex);
        void setVertex1(const Point3D& vertex);

    private:
        Normal getNormal(int inFace) const;
        void calculateLocalHitPoint(ShadingData<SpectrumType>& shadingDataObject) const;

        double x0_; ///< Minimum x-coordinate.
        double y0_; ///< Minimum y-coordinate.
        double z0_; ///< Minimum z-coordinate.
        double x1_; ///< Maximum x-coordinate.
        double y1_; ///< Maximum y-coordinate.
        double z1_; ///< Maximum z-coordinate.

    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Box<SpectrumType>::Box()
        : GeometricObject<SpectrumType>(SpectrumType(0.5f))
        , x0_(0.0)
        , y0_(0.0)
        , z0_(0.0)
        , x1_(1.0)
        , y1_(1.0)
        , z1_(1.0)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] point0 Minimum vertex of box.
     *  \param[in] point1 Maximum vertex of box.
     */
    template <class SpectrumType>
    Box<SpectrumType>::Box(const Point3D& point0, const Point3D& point1)
        : GeometricObject<SpectrumType>(SpectrumType(0.5f))
    {
        x0_ = std::min(point0.x_, point1.x_);
        y0_ = std::min(point0.y_, point1.y_);
        z0_ = std::min(point0.z_, point1.z_);
        x1_ = std::max(point0.x_, point1.x_);
        y1_ = std::max(point0.y_, point1.y_);
        z1_ = std::max(point0.z_, point1.z_);
    }



    /*! \brief Constructor with initialization.
     *  \param[in] point0 Minimum vertex of box.
     *  \param[in] point1 Maximum vertex of box.
     *  \param[in] material Material assigned to the box.
     */
    template <class SpectrumType>
    Box<SpectrumType>::Box(const Point3D& point0, const Point3D& point1, std::shared_ptr<Material<SpectrumType> > material)
        : GeometricObject<SpectrumType>(material)
    {
        x0_ = std::min(point0.x_, point1.x_);
        y0_ = std::min(point0.y_, point1.y_);
        z0_ = std::min(point0.z_, point1.z_);
        x1_ = std::max(point0.x_, point1.x_);
        y1_ = std::max(point0.y_, point1.y_);
        z1_ = std::max(point0.z_, point1.z_);
    }



    /*! \brief Copy constructor.
     *  \param[in] other Box object to copy.
     */
    template <class SpectrumType>
    Box<SpectrumType>::Box(const Box<SpectrumType>& other)
        : GeometricObject<SpectrumType>(other)
        , x0_(other.x0_)
        , y0_(other.y0_)
        , z0_(other.z0_)
        , x1_(other.x1_)
        , y1_(other.y1_)
        , z1_(other.z1_)
    {
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    Box<SpectrumType>::~Box()
    {
    }



    /*! \brief Clone (make deep copy of) object.
     *  \return Returned cloned object.
     */
    template <class SpectrumType>
    Box<SpectrumType>* Box<SpectrumType>::clone() const
    {
        return new Box<SpectrumType>(*this);
    }



    /*! \brief Intersects object with given ray.
     *  \param[in] ray Ray for intersection.
     *  \param[out] tmin Ray-parameter of hit point.
     *  \param[out] shadingData Shading data structure of hit point.
     *  \return Returns TRUE, if ray and object intersect, else FLASE.
     */
    template <class SpectrumType>
    bool Box<SpectrumType>::hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const
    {
        double rayOriginX = ray.origin_.x_;
        double rayOriginY = ray.origin_.y_;
        double rayOriginZ = ray.origin_.z_;
        double rayDirectionX = ray.direction_.x_;
        double rayDirectionY = ray.direction_.y_;
        double rayDirectionZ = ray.direction_.z_;
        
        double txMin, txMax;
        double tyMin, tyMax;
        double tzMin, tzMax;
        
        double a = 1.0 / rayDirectionX;
        if (a >= 0.0)
        {
            txMin = (x0_ - rayOriginX) * a;
            txMax = (x1_ - rayOriginX) * a;
        }
        else
        {
            txMin = (x1_ - rayOriginX) * a;
            txMax = (x0_ - rayOriginX) * a;
        }
        
        double b = 1.0 / rayDirectionY;
        if (b >= 0.0)
        {
            tyMin = (y0_ - rayOriginY) * b;
            tyMax = (y1_ - rayOriginY) * b;
        }
        else
        {
            tyMin = (y1_ - rayOriginY) * b;
            tyMax = (y0_ - rayOriginY) * b;
        }
        
        double c = 1.0 / rayDirectionZ;
        if (c >= 0.0)
        {
            tzMin = (z0_ - rayOriginZ) * c;
            tzMax = (z1_ - rayOriginZ) * c;
        }
        else
        {
            tzMin = (z1_ - rayOriginZ) * c;
            tzMax = (z0_ - rayOriginZ) * c;
        }
        
        double t0;
        double t1;
        int inFace;
        int outFace;
        if (txMin > tyMin)
        {
            t0 = txMin;
            inFace = (a >= 0.0) ? 0 : 3;
        }
        else
        {
            t0 = tyMin;
            inFace = (b >= 0.0) ? 1 : 4;
        }
        if (tzMin > t0)
        {
            t0 = tzMin;
            inFace = (c >= 0.0) ? 2 : 5;
        }
        
        if (txMax < tyMax)
        {
            t1 = txMax;
            outFace = (a >= 0.0) ? 3 : 0;
        }
        else
        {
            t1 = tyMax;
            outFace = (b >= 0.0) ? 4 : 1;
        }
        if (tzMax < t1)
        {
            t1 = tzMax;
            outFace = (c >= 0.0) ? 5 : 2;
        }
        
        if (t0 < t1 && t1 > EPS)
        {
            if (t0 > EPS)
            {
                tmin = t0;
                shadingData.normal_ = getNormal(inFace);
            }
            else
            {
                tmin = t1;
                shadingData.normal_ = -getNormal(outFace);
            }

            if (shadingData.normal_ * ray.direction_ > 0)
            {
                shadingData.normal_ = -shadingData.normal_;
                shadingData.normalFlipped_ = true;
            }
            
            shadingData.hitPoint_ = ray.origin_ + tmin * ray.direction_;
            shadingData.hitAnObject_ = true;
            shadingData.ray_ = ray;
            calculateLocalHitPoint(shadingData);
            shadingData.material_ = this->objectMaterial_;
            return true;
        }
        return false;
    }



    /*! \brief Intersects object with given shadow ray.
     *  \param[in] shadowRay Ray for intersection.
     *  \param[out] tmin Ray-parameter of hit point.
     *  \return Returns TRUE, if ray and object intersect, else FLASE.
     */
    template <class SpectrumType>
    bool Box<SpectrumType>::shadowHit(const Ray& shadowRay, double& tmin) const
    {
        if (!this->castsShadows_)
        {
            return false;
        }

        double rayOriginX = shadowRay.origin_.x_;
        double rayOriginY = shadowRay.origin_.y_;
        double rayOriginZ = shadowRay.origin_.z_;
        double rayDirectionX = shadowRay.direction_.x_;
        double rayDirectionY = shadowRay.direction_.y_;
        double rayDirectionZ = shadowRay.direction_.z_;
        
        double txMin, txMax;
        double tyMin, tyMax;
        double tzMin, tzMax;
        
        double a = 1.0 / rayDirectionX;
        if (a >= 0.0)
        {
            txMin = (x0_ - rayOriginX) * a;
            txMax = (x1_ - rayOriginX) * a;
        }
        else
        {
            txMin = (x1_ - rayOriginX) * a;
            txMax = (x0_ - rayOriginX) * a;
        }
        
        double b = 1.0 / rayDirectionY;
        if (b >= 0.0)
        {
            tyMin = (y0_ - rayOriginY) * b;
            tyMax = (y1_ - rayOriginY) * b;
        }
        else
        {
            tyMin = (y1_ - rayOriginY) * b;
            tyMax = (y0_ - rayOriginY) * b;
        }
        
        double c = 1.0 / rayDirectionZ;
        if (c >= 0.0)
        {
            tzMin = (z0_ - rayOriginZ) * c;
            tzMax = (z1_ - rayOriginZ) * c;
        }
        else
        {
            tzMin = (z1_ - rayOriginZ) * c;
            tzMax = (z0_ - rayOriginZ) * c;
        }
        
        double t0;
        double t1;
        int inFace;
        int outFace;
        if (txMin > tyMin)
        {
            t0 = txMin;
            inFace = (a >= 0.0) ? 0 : 3;
        }
        else
        {
            t0 = tyMin;
            inFace = (b >= 0.0) ? 1 : 4;
        }
        if (tzMin > t0)
        {
            t0 = tzMin;
            inFace = (c >= 0.0) ? 2 : 5;
        }
        
        if (txMax < tyMax)
        {
            t1 = txMax;
            outFace = (a >= 0.0) ? 3 : 0;
        }
        else
        {
            t1 = tyMax;
            outFace = (b >= 0.0) ? 4 : 1;
        }
        if (tzMax < t1)
        {
            t1 = tzMax;
            outFace = (c >= 0.0) ? 5 : 2;
        }
        
        if (t0 < t1 && t1 > EPS)
        {
            if (t0 > EPS)
            {
                tmin = t0;
            }
            else
            {
                tmin = t1;
            }
            return true;
        }
        return false;
    }



    /*! \brief Computes and returns the bounding box of the box.
     *  \return Bounding box of box.
     */
    template <class SpectrumType>
    BoundingBox Box<SpectrumType>::getBoundingBox() const
    {
        return BoundingBox(x0_, y0_, z0_, x1_, y1_, z1_);
    }



    /*! Get first vertex of box.
     *  \return Returns first vertex.
     */
    template <class SpectrumType>
    Point3D Box<SpectrumType>::getVertex0() const
    {
        return Point3D(x0_, y0_, z0_);
    }



    /*! Get second vertex of box.
     *  \return Returns second vertex.
     */
    template <class SpectrumType>
    Point3D Box<SpectrumType>::getVertex1() const
    {
        return Point3D(x1_, y1_, z1_);
    }



    /*! Set first vertex of box.
     *  \param[in] vertex First vertex.
     */
    template <class SpectrumType>
    void Box<SpectrumType>::setVertex0(const Point3D& vertex)
    {
        x0_ = vertex.x_;
        y0_ = vertex.y_;
        z0_ = vertex.z_;
    }



    /*! Set second vertex of box.
     *  \param[in] vertex Second vertex.
     */
    template <class SpectrumType>
    void Box<SpectrumType>::setVertex1(const Point3D& vertex)
    {
        x1_ = vertex.x_;
        y1_ = vertex.y_;
        z1_ = vertex.z_;
    }



    /*! \brief Get normal from face index.
     *  \param[in] inFace Index of face.
     *  \return Return normal of face.
     */
    template <class SpectrumType>
    Normal Box<SpectrumType>::getNormal(int inFace) const
    {
        switch (inFace)
        {
            case 0: return Normal(-1.0, 0.0, 0.0);
            case 1: return Normal(0.0, -1.0, 0.0);
            case 2: return Normal(0.0, 0.0, -1.0);
            case 3: return Normal(1.0, 0.0, 0.0);
            case 4: return Normal(0.0, 1.0, 0.0);
            case 5: return Normal(0.0, 0.0, 1.0);
        }
        return Normal(); // to suppress warning
    }



    /*! \brief Calculates the localHitpoint.
     *  \param[in] shadingDataObject Shading data of this hit point. Member localHitPoint_ is set.
     *
     *  This method calculates the hit point coordinates in a 3D cartesian coordinates originating in the
     * box's lowest vertex p0.
    */
    template <class SpectrumType>
    void Box<SpectrumType>::calculateLocalHitPoint(ShadingData<SpectrumType>& shadingDataObject) const
    {
        double x = (shadingDataObject.hitPoint_.x_ - x0_);
        double y = (shadingDataObject.hitPoint_.y_ - y0_);
        double z = (shadingDataObject.hitPoint_.z_ - z0_);

        shadingDataObject.localHitPoint3D_ = Point3D(x, y, z);
    }

} // end of namespace iiit

#endif // __BOX_HPP__
