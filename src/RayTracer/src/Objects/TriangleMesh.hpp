// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file TriangleMesh.hpp
 *  \author Thomas Nuernberg
 *  \date 01.06.2015
 *  \brief Definition of class TriangleMesh.
 */

#ifndef __TRIANGLEMESH_HPP__
#define __TRIANGLEMESH_HPP__

#include <vector>
#include <memory>

#include "Grid.hpp"
#include "Mesh.hpp"
#include "MeshTriangle.hpp"
#include "GeometricObject.hpp"



namespace iiit
{

    /*! \brief %TriangleMesh object.
     */
    template <class SpectrumType>
    class TriangleMesh : public Grid<SpectrumType>
    {
    public:

        /*! This enum type defines valid MeshTriangle types.
         */
        enum Type
        {
            Flat, ///< Mesh triangle with flat shading.
            Smooth ///< Mesh triangle with smooth shading.
        };

        TriangleMesh();
        TriangleMesh(std::shared_ptr<Mesh> mesh);
        TriangleMesh(const TriangleMesh<SpectrumType>& other);
        virtual ~TriangleMesh();

        virtual TriangleMesh* clone() const;

        /*! \brief Get the type of the geometric object.
         *  \return Returns the type of the geometric object.
         */
        virtual typename GeometricObject<SpectrumType>::Type getType() const {return GeometricObject<SpectrumType>::TriangleMesh;}

        void computeMeshNormals();

        std::shared_ptr<Mesh> mesh_; ///< Mesh data structure storing the vertex and face data.
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    TriangleMesh<SpectrumType>::TriangleMesh()
        : Grid<SpectrumType>()
        , mesh_(new Mesh)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] mesh Pointer to mesh data structure.
     */
    template <class SpectrumType>
    TriangleMesh<SpectrumType>::TriangleMesh(std::shared_ptr<Mesh> mesh)
        : mesh_(mesh)
    {
    }



    /*! \brief Copy constructor.
     *  \param[in] other Other object to copy.
     */
    template <class SpectrumType>
    TriangleMesh<SpectrumType>::TriangleMesh(const TriangleMesh<SpectrumType>& other)
        : Grid<SpectrumType>(other)
        , mesh_(other.mesh_)
    {
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    TriangleMesh<SpectrumType>::~TriangleMesh()
    {
    }



    /*! \brief Clone (make deep copy of) object.
     *  \return Returned cloned object.
     */
    template <class SpectrumType>
    TriangleMesh<SpectrumType>* TriangleMesh<SpectrumType>::clone() const
    {
        return new TriangleMesh<SpectrumType>(*this);
    }



    /*! \brief Compute average vertex normals for smooth shading.
     */
    template <class SpectrumType>
    void TriangleMesh<SpectrumType>::computeMeshNormals()
    {
        if (this->objects_[0]->getType() == GeometricObject<SpectrumType>::SmoothTriangle)
        {
            mesh_->normals_.resize(mesh_->numVertices_, Normal());
            for (int i = 0; i < mesh_->numVertices_; ++i)
            {
                for (unsigned int j = 0; j < mesh_->vertexFaces_[i].size(); ++j)
                {
                    mesh_->normals_[i] += std::static_pointer_cast<MeshTriangle<SpectrumType> >(this->objects_[mesh_->vertexFaces_[i][j]])->normal_;
                }
                mesh_->normals_[i].normalize();
            }
            
            for (int i = 0; i < mesh_->numVertices_; ++i)
            {
                mesh_->vertexFaces_[i].erase(mesh_->vertexFaces_[i].begin(), mesh_->vertexFaces_[i].end());
            }
            mesh_->vertexFaces_.erase(mesh_->vertexFaces_.begin(), mesh_->vertexFaces_.end());
        }
    }


    ///*! \brief Compute normal from vertexes.
    // */
    //void MeshTriangle::computeNormal()
    //{
    //    Vector3D v01 = mesh_->vertices_[vertexIndices_[1]] - mesh_->vertices_[vertexIndices_[0]];
    //    Vector3D v02 = mesh_->vertices_[vertexIndices_[2]] - mesh_->vertices_[vertexIndices_[0]];
    //
    //    normal_ = v01.crossProduct(v02);
    //    normal_.normalize();
    //}

} // end of namespace iiit

#endif // __TRIANGLEMESH_HPP__