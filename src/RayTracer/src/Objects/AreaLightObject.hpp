// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file AreaLightObject.hpp
 *  \author Thomas Nuernberg
 *  \date 13.06.2016
 *  \brief Definition of base class for geometric objects that can serve as area light.
 */
 
#ifndef __AREALIGHTOBJECT_HPP__
#define __AREALIGHTOBJECT_HPP__


#include "GeometricObject.hpp"
#include "Sampler/Sampler.hpp"



namespace iiit
{
    
    /*! \brief Abstract base class for geometric objects that can serve as area light.
     */
    template <class SpectrumType>
    class AreaLightObject : public GeometricObject<SpectrumType>
    {
    public:
        AreaLightObject();
        AreaLightObject(std::shared_ptr<Material<SpectrumType> > material);
        AreaLightObject(const SpectrumType& color);
        AreaLightObject(const AreaLightObject<SpectrumType>& other);

        virtual ~AreaLightObject() {};

        /*! \brief Sample surface of area light.
         *  \return Return sample on surface.
         */
        virtual Point3D sample() = 0;

        /*! \brief Get value of probability density function at sample point on surface.
         *  \param[out] shadingData Hit point properties used for shading.
         *  \return Returns value of probability density function.
         */
        virtual float pdf(const ShadingData<SpectrumType>& shadingData) = 0;

        /*! \brief Get normal at surface point.
         *  \param[out] point Surface point.
         *  \return Returns normal at surface point.
         */
        virtual Normal getNormal(const Point3D& point) = 0;

        /*! \brief Deep copies the geometric object.
         *  \return Returns the copied geometric object.
         */
        virtual AreaLightObject<SpectrumType>* clone() const = 0;

        void setSampler(std::shared_ptr<Sampler> sampler);
        void alignSampler();
        
    protected:
        std::shared_ptr<Sampler> sampler_; ///< Sampling object used for area lights.
        
    };



    /*! \brief Default constructor.
     *
     *  \attention The object material is left uninitialized by default in order to reduce memory
     *  consumption of compound objects.
     */
    template <class SpectrumType>
    AreaLightObject<SpectrumType>::AreaLightObject()
        : GeometricObject<SpectrumType>()
    {
    }
    
    
    
    /*! \brief Constructor with material initialization.
     *  \param[in] material Material of object.
     */
    template <class SpectrumType>
    AreaLightObject<SpectrumType>::AreaLightObject(std::shared_ptr<Material<SpectrumType> > material)
        : GeometricObject<SpectrumType>(material)
    {
    }
    
    
    
    /*! \brief Constructor with color initialization.
     *  \param[in] color Color of object.
     *
     *  Initializes object material to matte with given color.
     */
    template <class SpectrumType>
    AreaLightObject<SpectrumType>::AreaLightObject(const SpectrumType& color)
        : GeometricObject<SpectrumType>(color)
    {
    }
    
    
    
    /*! \brief Copy constructor.
     *  \param[in] other Geometric object to copy.
     */
    template <class SpectrumType>
    AreaLightObject<SpectrumType>::AreaLightObject(const AreaLightObject<SpectrumType>& other)
        : GeometricObject<SpectrumType>(other)
    {
    }
    
    
    
    /*! \brief Set sampling object.
     *  \param[in] sampler Sampling object.
     */
    template <class SpectrumType>
    void AreaLightObject<SpectrumType>::setSampler(std::shared_ptr<Sampler> sampler)
    {
        sampler_ = sampler;
    }
    
    
    
    /*! \brief Align sampler.
     */
    template <class SpectrumType>
    void AreaLightObject<SpectrumType>::alignSampler()
    {
        sampler_->align();
    }


} // end of namespace iiit

#endif // __AREALIGHTOBJECT_HPP__
