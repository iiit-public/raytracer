// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file OpenCone.hpp
 *  \author Thomas Nuernberg
 *  \date 30.06.2015
 *  \brief Definition of class OpenCone.
 */

#ifndef __OPENCONE_HPP__
#define __OPENCONE_HPP__

#include <math.h>

#include "Logger.hpp"
#include "GeometricObject.hpp"
#include "BoundingBox.hpp"
#include "Utilities/Constants.h"
#include "Utilities/Ray.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Normal.hpp"
#include "Materials/Material.hpp"



namespace iiit
{

    /*! \brief Generic %OpenCone object.
     *
     *  This class represents a generic cone surface, meaning that it can only be defined in one
     *  orientation. Use the Instance mechanism to transform it to arbitrary cone surfaces.
     */
    template <class SpectrumType>
    class OpenCone : public GeometricObject<SpectrumType>
    {
    public:
        OpenCone();
        OpenCone(const Point3D& center, double minRadius, double maxRadius, double zMin, double zMax);
        OpenCone(const Point3D& center, double minRadius, double maxRadius, double zMin, double zMax, std::shared_ptr<Material<SpectrumType> > material);
        OpenCone(const OpenCone<SpectrumType>& other);
        virtual ~OpenCone();

        virtual OpenCone<SpectrumType>* clone() const;

        /*! \brief Get the type of the geometric object.
         *  \return Returns the type of the geometric object.
         */
        virtual typename GeometricObject<SpectrumType>::Type getType() const {return GeometricObject<SpectrumType>::OpenCone;}
        virtual bool hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const;
        virtual bool shadowHit(const Ray& shadowRay, double& tmin) const;
        virtual BoundingBox getBoundingBox() const;

        Point3D getCenter() const;
        void setCenter(const Point3D& center);
        void setCenter(double x, double y, double z);

        double getMinRadius() const;
        void setMinRadius(double minRadius);

        double getMaxRadius() const;
        void setMaxRadius(double maxRadius);

        double getZMin() const;
        void setZMin(double zMin);

        double getZMax() const;
        void setZMax(double zMax);

    private:
        void calculateLocalHitPoint(ShadingData<SpectrumType>& shadingDataObject) const;

        Point3D center_; ///< Center of cone.
        double minRadius_; ///< Radius of cone at zMin_.
        double maxRadius_; ///< Radius of cone at zMax_.
        double zMin_; ///< Minimum z-coordinate of cone.
        double zMax_; ///< Maximum z-coordinate of cone.
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    OpenCone<SpectrumType>::OpenCone()
        : GeometricObject<SpectrumType>(SpectrumType(0.5f))
        , center_()
        , minRadius_(0.0)
        , maxRadius_(0.0)
        , zMin_(0.0)
        , zMax_(1.0)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] center Center of cone in 3D.
     *  \param[in] minRadius Radius of cone at zMin.
     *  \param[in] maxRadius Radius of cone at zMax.
     *  \param[in] zMin Minimum z-coordinate of cone.
     *  \param[in] zMax Maximum z-coordinate of cone.
     */
    template <class SpectrumType>
    OpenCone<SpectrumType>::OpenCone(const Point3D& center, double minRadius, double maxRadius, double zMin, double zMax)
        : GeometricObject<SpectrumType>(SpectrumType(0.5f))
        , center_(center)
        , minRadius_(minRadius)
        , maxRadius_(maxRadius)
        , zMin_(zMin)
        , zMax_(zMax)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] center Center of cone in 3D.
     *  \param[in] minRadius Radius of cone at zMin.
     *  \param[in] maxRadius Radius of cone at zMax.
     *  \param[in] zMin Minimum z-coordinate of cone.
     *  \param[in] zMax Maximum z-coordinate of cone.
     *  \param[in] material Material assigned to the Plane.
     */
    template <class SpectrumType>
    OpenCone<SpectrumType>::OpenCone(const Point3D& center, double minRadius, double maxRadius, double zMin, double zMax, std::shared_ptr<Material<SpectrumType> > material)
        : GeometricObject<SpectrumType>(material)
        , center_(center)
        , minRadius_(minRadius)
        , maxRadius_(maxRadius)
        , zMin_(zMin)
        , zMax_(zMax)
    {
    }



    /*! \brief Copy constructor.
     *  \param[in] other Cone object to copy.
     */
    template <class SpectrumType>
    OpenCone<SpectrumType>::OpenCone(const OpenCone<SpectrumType>& other)
        : GeometricObject<SpectrumType>(other)
        , center_(other.center_)
        , minRadius_(other.minRadius_)
        , maxRadius_(other.maxRadius_)
        , zMin_(other.zMin_)
        , zMax_(other.zMax_)
    {
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    OpenCone<SpectrumType>::~OpenCone()
    {
    }



    /*! \brief Clone (make deep copy of) object.
     *  \return Returned cloned object.
     */
    template <class SpectrumType>
    OpenCone<SpectrumType>* OpenCone<SpectrumType>::clone() const
    {
        return new OpenCone<SpectrumType>(*this);
    }



    /*! \brief Intersects object with given ray.
     *  \param[in] ray Ray for intersection.
     *  \param[out] tmin Ray-parameter of hit point.
     *  \param[out] shadingData Shading data structure of hit point.
     *  \return Returns TRUE, if ray and object intersect, else FLASE.
     */
    template <class SpectrumType>
    bool OpenCone<SpectrumType>::hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const
    {
        if (!getBoundingBox().hit(ray))
        {
            return false;
        }
        
        double t;
        Vector3D temp = ray.origin_ - center_;

        // linear equation for effective radius: R(z) = m * z + e
        double m = (maxRadius_ - minRadius_) / (zMax_ - zMin_);
        double e = minRadius_ - m * zMin_;
        
        // surface equation x^2 + y^2 - (R(z))^2 = 0, solved for ray parameter t
        double a = ray.direction_.x_ * ray.direction_.x_ + ray.direction_.y_ * ray.direction_.y_ - m * m * ray.direction_.z_ * ray.direction_.z_;
        double b = 2.0 * ((temp.x_ * ray.direction_.x_ + temp.y_ * ray.direction_.y_) - m * ray.direction_.z_ * (m * ray.origin_.z_ + e));
        double c = temp.x_ * temp.x_ + temp.y_ * temp.y_ - (m * ray.origin_.z_ + e) * (m * ray.origin_.z_ + e);
        double disc = b * b - 4 * a * c;
        
        if (disc < 0.0)
        {
            return false;
        }
        else
        {
            double f = sqrt(disc);
            double denom = 2.0 * a;
            t = (-b - f) / denom;
            
            if (t > EPS)
            {
                double z = ray.origin_.z_ + t * ray.direction_.z_;
                if (z > zMin_ && z < zMax_)
                {
                    tmin = t;
                    shadingData.hitAnObject_ = true;
                    shadingData.normal_ = Normal(temp.x_ + t * ray.direction_.x_,
                        temp.y_ + t * ray.direction_.y_,
                        -m * (m * (ray.origin_.z_ + t * ray.direction_.z_) + e));
                    shadingData.normal_.normalize();
                    shadingData.hitPoint_ = ray.origin_ + t * ray.direction_;
                    shadingData.ray_ = ray;
                    shadingData.material_ = this->objectMaterial_;
                    
                    if (-ray.direction_ * shadingData.normal_ < 0.0)
                    {
                        shadingData.normal_ = -shadingData.normal_;
                        shadingData.normalFlipped_ = true;
                    }
                    
                    // calculate hit point in local coordinate system
                    calculateLocalHitPoint(shadingData);
                    
                    return true;
                }
            }
            t = (-b + f) / denom;
            if (t > EPS)
            {
                double z = ray.origin_.z_ + t * ray.direction_.z_;
                if (z > zMin_ && z < zMax_)
                {
                    tmin = t;
                    shadingData.hitAnObject_ = true;
                    shadingData.normal_ = Normal(temp.x_ + t * ray.direction_.x_,
                        temp.y_ + t * ray.direction_.y_,
                        -m * (m * (ray.origin_.z_ + t * ray.direction_.z_) + e));
                    shadingData.normal_.normalize();
                    shadingData.hitPoint_ = ray.origin_ + t * ray.direction_;
                    shadingData.ray_ = ray;
                    shadingData.material_ = this->objectMaterial_;
                    
                    if (-ray.direction_ * shadingData.normal_ < 0.0)
                    {
                        shadingData.normal_ = -shadingData.normal_;
                        shadingData.normalFlipped_ = true;
                    }
                    
                    // calculate hit point in local coordinate system
                    calculateLocalHitPoint(shadingData);
                    
                    return true;
                }
            }
        }
        
        return false;
    }



    /*! \brief Intersects object with given shadow ray.
     *  \param[in] shadowRay Ray for intersection.
     *  \param[out] tmin Ray-parameter of hit point.
     *  \return Returns TRUE, if ray and object intersect, else FLASE.
     */
    template <class SpectrumType>
    bool OpenCone<SpectrumType>::shadowHit(const Ray& shadowRay, double& tmin) const
    {
        if (!this->castsShadows_)
        {
            return false;
        }

        if (!getBoundingBox().hit(shadowRay))
        {
            return false;
        }
        
        double t;
        Vector3D temp = shadowRay.origin_ - center_;
        
        // linear equation for effective radius: R(z) = m * z + e
        double m = (maxRadius_ - minRadius_) / (zMax_ - zMin_);
        double e = minRadius_ - m * zMin_;
        
        // surface equation x^2 + y^2 - (R(z))^2 = 0, solved for ray parameter t
        double a = shadowRay.direction_.x_ * shadowRay.direction_.x_ + shadowRay.direction_.y_ * shadowRay.direction_.y_ - m * m * shadowRay.direction_.z_ * shadowRay.direction_.z_;
        double b = 2.0 * ((temp.x_ * shadowRay.direction_.x_ + temp.y_ * shadowRay.direction_.y_) - m * shadowRay.direction_.z_ * (m * shadowRay.origin_.z_ + e));
        double c = temp.x_ * temp.x_ + temp.y_ * temp.y_ - (m * shadowRay.origin_.z_ + e) * (m * shadowRay.origin_.z_ + e);
        double disc = b * b - 4 * a * c;
        
        if (disc < 0.0)
        {
            return false;
        }
        else
        {
            double f = sqrt(disc);
            double denom = 2.0 * a;
            t = (-b - f) / denom;
            
            if (t > EPS)
            {
                double z = shadowRay.origin_.z_ + t * shadowRay.direction_.z_;
                if (z > zMin_ && z < zMax_)
                {
                    tmin = t;
                    return true;
                }
            }
            t = (-b + f) / denom;
            if (t > EPS)
            {
                double z = shadowRay.origin_.z_ + t * shadowRay.direction_.z_;
                if (z > zMin_ && z < zMax_)
                {
                    tmin = t;
                    return true;
                }
            }
        }
        
        return false;
    }


    /*! \brief Computes and returns the bounding box of the cone.
     *  \return Bounding box of cone.
     */
    template <class SpectrumType>
    BoundingBox OpenCone<SpectrumType>::getBoundingBox() const
    {
        double radius = std::max<double>(minRadius_, maxRadius_);
        return BoundingBox(
            center_.x_ - radius,
            center_.y_ - radius,
            zMin_,
            center_.x_ + radius,
            center_.y_ + radius,
            zMax_);
    }



    /*! \brief Returns center of cone.
     *  \return Center of cone.
     */
    template <class SpectrumType>
    Point3D OpenCone<SpectrumType>::getCenter() const
    {
        return center_;
    }



    /*! \brief Sets center of cone.
     *  \param[in] center Center of cone.
     */
    template <class SpectrumType>
    void OpenCone<SpectrumType>::setCenter(const Point3D& center)
    {
        center_ = center;
    }



    /*! \brief Sets center of cone.
     *  \param[in] x X-coordinate of center of cone.
     *  \param[in] y Y-coordinate of center of cone.
     *  \param[in] z Z-coordinate of center of cone.
     */
    template <class SpectrumType>
    void OpenCone<SpectrumType>::setCenter(double x, double y, double z)
    {
        center_.x_ = x;
        center_.y_ = y;
        center_.z_ = z;
    }



    /*! \brief Returns radius of cone at zMin_.
     *  \return Radius of cone.
     */
    template <class SpectrumType>
    double OpenCone<SpectrumType>::getMinRadius() const
    {
        return minRadius_;
    }



    /*! \brief Sets radius of cone at zMin_.
     *  \param[in] minRadius Radius of cone.
     */
    template <class SpectrumType>
    void OpenCone<SpectrumType>::setMinRadius(double minRadius)
    {
        minRadius_ = minRadius;
    }



    /*! \brief Returns radius of cone at zMax_.
     *  \return Radius of cone.
     */
    template <class SpectrumType>
    double OpenCone<SpectrumType>::getMaxRadius() const
    {
        return maxRadius_;
    }



    /*! \brief Sets radius of cone at zMax_.
     *  \param[in] maxRadius Radius of cone.
     */
    template <class SpectrumType>
    void OpenCone<SpectrumType>::setMaxRadius(double maxRadius)
    {
        maxRadius_ = maxRadius;
    }



    /*! \brief Get minimum cylinder z-coodinate.
     *  \return Minimum cylinder z-coordinate.
     */
    template <class SpectrumType>
    double OpenCone<SpectrumType>::getZMin() const
    {
        return zMin_;
    }



    /*! \brief Set minimum cylinder z-coodinate.
     *  \param[in] zMin Minimum cylinder z-coordinate.
     */
    template <class SpectrumType>
    void OpenCone<SpectrumType>::setZMin(double zMin)
    {
        zMin_ = zMin;
    }



    /*! \brief Get maximum cylinder z-coodinate.
     *  \return Maximum cylinder z-coordinate.
     */
    template <class SpectrumType>
    double OpenCone<SpectrumType>::getZMax() const
    {
        return zMax_;
    }



    /*! \brief Set maximum cylinder z-coodinate.
     *  \param[in] zMax Maximum cylinder z-coordinate.
     */
    template <class SpectrumType>
    void OpenCone<SpectrumType>::setZMax(double zMax)
    {
        zMax_ = zMax;
    }



    /*! \brief Calculates the localHitpoint.
     *  \param[in] shadingDataObject Shading data of this hit point. Member localHitPoint_ is set.
     *
     *  This method calculates the hit point coordinates in a polar coordinate system originated in
     *  this cones center. Values for localHitPoint_(u, v) are in the ranges u:[0, 1] and
     *  v:[0, 360deg].
     */
    template <class SpectrumType>
    void OpenCone<SpectrumType>::calculateLocalHitPoint(ShadingData<SpectrumType>& shadingDataObject) const
    {
        Vector3D vectorCenterToHitpoint(shadingDataObject.hitPoint_ - center_);
        float u = static_cast<float>((shadingDataObject.hitPoint_.z_ - zMin_) / (zMax_ - zMin_));
        float v = static_cast<float>(atan2(vectorCenterToHitpoint(2), vectorCenterToHitpoint(1)) * 180 * INV_PI);

        if (v < 0.0)
        {
            v += 360.0f;
        }
        shadingDataObject.localHitPoint2D_ = Point2D(u, v);
        shadingDataObject.localHitPoint3D_ = Point3D(shadingDataObject.hitPoint_ - center_);
    }

} // end of namespace iiit

#endif // __OPENCONE_HPP__
