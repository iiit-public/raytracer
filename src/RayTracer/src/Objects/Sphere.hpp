// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Sphere.hpp
 *  \author Thomas Nuernberg
 *  \date 09.01.2015
 *  \brief Definition of class Sphere.
 */

#ifndef __SPHERE_HPP__
#define __SPHERE_HPP__

#include <math.h>

#include "AreaLightObject.hpp"
#include "BoundingBox.hpp"
#include "Utilities/Constants.h"
#include "Utilities/Ray.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Point3D.hpp"
#include "Materials/Material.hpp"



namespace iiit
{

    /*! \brief %Sphere object.
     */
    template <class SpectrumType>
    class Sphere : public AreaLightObject<SpectrumType>
    {
    public:
        Sphere();
        Sphere(const Point3D& center, double radius);
        Sphere(double x, double y, double z, double radius);
        Sphere(double x, double y, double z, double radius, std::shared_ptr<Material<SpectrumType> > material);
        Sphere(const Sphere& other);
        virtual ~Sphere();

        virtual Sphere* clone() const;

        /*! \brief Get the type of the geometric object.
         *  \return Returns the type of the geometric object.
         */
        virtual typename GeometricObject<SpectrumType>::Type getType() const {return GeometricObject<SpectrumType>::Sphere;}
        virtual bool hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const;
        virtual bool shadowHit(const Ray& shadowRay, double& tmin) const;
        virtual BoundingBox getBoundingBox() const;

        Point3D getCenter() const;
        void setCenter(const Point3D& center);
        void setCenter(double x, double y, double z);

        double getRadius() const;
        void setRadius(double radius);

        virtual Point3D sample();
        virtual float pdf(const ShadingData<SpectrumType>& shadingData);
        virtual Normal getNormal(const Point3D& point);

    private:
        void calculateLocalHitPoint(ShadingData<SpectrumType>& shadingDataObject) const;
        void calculateInvArea();

        Point3D center_; ///< Center of sphere.
        double radius_; ///< Radius of sphere.
        float invArea_; ///< Inverse of the spanned are used for calculating the probability density function of sample points.
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Sphere<SpectrumType>::Sphere()
        : AreaLightObject<SpectrumType>(SpectrumType(0.5f))
        , center_()
        , radius_(0)
        , invArea_(std::numeric_limits<float>::infinity())
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] center Center of sphere in 3D.
     *  \param[in] radius Radius of sphere.
     */
    template <class SpectrumType>
    Sphere<SpectrumType>::Sphere(const Point3D& center, double radius)
        : AreaLightObject<SpectrumType>(SpectrumType(0.5f))
        , center_(center)
        , radius_(radius)
    {
        calculateInvArea();
    }



    /*! \brief Constructor with initialization.
     *  \param[in] x X-coordinate of center of sphere.
     *  \param[in] y Y-coordinate of center of sphere.
     *  \param[in] z Z-coordinate of center of sphere.
     *  \param[in] radius Radius of sphere.
     */
    template <class SpectrumType>
    Sphere<SpectrumType>::Sphere(double x, double y, double z, double radius)
        : AreaLightObject<SpectrumType>(SpectrumType(0.5f))
        , center_(x, y, z)
        , radius_(radius)
    {
        calculateInvArea();
    }



    /*! \brief Constructor with initialization.
     *  \param[in] x X-coordinate of center of sphere.
     *  \param[in] y Y-coordinate of center of sphere.
     *  \param[in] z Z-coordinate of center of sphere.
     *  \param[in] radius Radius of sphere.
     *  \param[in] material Material assigned to the Sphere.
     */
    template <class SpectrumType>
    Sphere<SpectrumType>::Sphere(double x, double y, double z, double radius, std::shared_ptr<Material<SpectrumType> > material)
        : AreaLightObject<SpectrumType>(material)
        , center_(x, y, z)
        , radius_(radius)
    {
        calculateInvArea();
    }



    /*! \brief Copy constructor.
     *  \param[in] other Sphere object to copy.
     */
    template <class SpectrumType>
    Sphere<SpectrumType>::Sphere(const Sphere<SpectrumType>& other)
        : AreaLightObject<SpectrumType>(other)
        , center_(other.center_)
        , radius_(other.radius_)
        , invArea_(other.invArea_)
    {
        this->objectMaterial_ = other.objectMaterial_;
        this->setSampler(std::shared_ptr<Sampler>(other.sampler_->clone()));
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    Sphere<SpectrumType>::~Sphere()
    {
    }



    /*! \brief Clone (make deep copy of) object.
     *  \return Returned cloned object.
     */
    template <class SpectrumType>
    Sphere<SpectrumType>* Sphere<SpectrumType>::clone() const
    {
        return new Sphere<SpectrumType>(*this);
    }



    /*! \brief Intersects object with given ray.
     *  \param[in] ray Ray for intersection.
     *  \param[out] tmin Ray-parameter of hit point.
     *  \param[out] shadingData Shading data structure of hit point.
     *  \return Returns TRUE, if ray and object intersect, else FLASE.
     */
    template <class SpectrumType>
    bool Sphere<SpectrumType>::hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const
    {
        double t;
        Vector3D temp = ray.origin_ - center_;
        double a = ray.direction_ * ray.direction_;
        double b = 2 * temp * ray.direction_;
        double c = temp * temp - radius_ * radius_;
        double disc = b * b - 4 * a * c;
        
        if (disc < 0.0)
        {
            return false;
        }
        else
        {
            double e = sqrt(disc);
            double denom = 2.0 * a;
            t = (-b - e) / denom;
            
            if (t > EPS)
            {
                tmin = t;
                shadingData.hitAnObject_ = true;
                shadingData.normal_ = (temp + t * ray.direction_) / radius_;
                if (shadingData.normal_ * ray.direction_ > 0)
                {
                    shadingData.normal_ = -shadingData.normal_;
                    shadingData.normalFlipped_ = true;
                }
                shadingData.hitPoint_ = ray.origin_ + t * ray.direction_;
                shadingData.ray_ = ray;
                shadingData.material_ = this->objectMaterial_;
                
                // calculate hit point in local coordinate system
                calculateLocalHitPoint(shadingData);
                
                return true;
            }
            t = (-b + e) / denom;
            if (t > EPS)
            {
                tmin = t;
                shadingData.hitAnObject_ = true;
                shadingData.normal_ = (temp + t * ray.direction_) / radius_;
                if (shadingData.normal_ * ray.direction_ > 0)
                {
                    shadingData.normal_ = -shadingData.normal_;
                    shadingData.normalFlipped_ = true;
                }
                shadingData.hitPoint_ = ray.origin_ + t * ray.direction_;
                shadingData.ray_ = ray;
                shadingData.material_ = this->objectMaterial_;
                
                // calculate hit point in local coordinate system
                calculateLocalHitPoint(shadingData);
                
                return true;
            }
        }
        
        return false;
    }



    /*! \brief Intersects object with given shadow ray.
     *  \param[in] shadowRay Ray for intersection.
     *  \param[out] tmin Ray-parameter of hit point.
     *  \return Returns TRUE, if ray and object intersect, else FLASE.
     */
    template <class SpectrumType>
    bool Sphere<SpectrumType>::shadowHit(const Ray& shadowRay, double& tmin) const
    {
        if (!this->castsShadows_)
        {
            return false;
        }

        double t;
        Vector3D temp = shadowRay.origin_ - center_;
        double a = shadowRay.direction_ * shadowRay.direction_;
        double b = 2 * temp * shadowRay.direction_;
        double c = temp * temp - radius_ * radius_;
        double disc = b * b - 4 * a * c;
        
        if (disc < 0.0)
        {
            return false;
        }
        else
        {
            double e = sqrt(disc);
            double denom = 2.0 * a;
            t = (-b - e) / denom;
            
            if (t > EPS)
            {
                tmin = t;
                return true;
            }
            t = (-b + e) / denom;
            if (t > EPS)
            {
                tmin = t;
                return true;
            }
        }
        
        return false;
    }



    /*! \brief Computes and returns the bounding box of the sphere.
     *  \return Bounding box of sphere.
     */
    template <class SpectrumType>
    BoundingBox Sphere<SpectrumType>::getBoundingBox() const
    {
        return BoundingBox(
            center_.x_ - radius_,
            center_.y_ - radius_,
            center_.z_ - radius_,
            center_.x_ + radius_,
            center_.y_ + radius_,
            center_.z_ + radius_);
    }



    /*! \brief Returns center of sphere.
     *  \return Center of sphere.
     */
    template <class SpectrumType>
    Point3D Sphere<SpectrumType>::getCenter() const
    {
        return center_;
    }



    /*! \brief Sets center of sphere.
     *  \param[in] center Center of sphere.
     */
    template <class SpectrumType>
    void Sphere<SpectrumType>::setCenter(const Point3D& center)
    {
        center_ = center;
    }



    /*! \brief Sets center of sphere.
     *  \param[in] x X-coordinate of center of sphere.
     *  \param[in] y Y-coordinate of center of sphere.
     *  \param[in] z Z-coordinate of center of sphere.
     */
    template <class SpectrumType>
    void Sphere<SpectrumType>::setCenter(double x, double y, double z)
    {
        center_.x_ = x;
        center_.y_ = y;
        center_.z_ = z;
    }



    /*! \brief Returns radius of sphere.
     *  \return Radius of sphere.
     */
    template <class SpectrumType>
    double Sphere<SpectrumType>::getRadius() const
    {
        return radius_;
    }



    /*! \brief Sets radius of sphere.
     *  \param[in] radius Radius of sphere.
     */
    template <class SpectrumType>
    void Sphere<SpectrumType>::setRadius(double radius)
    {
        radius_ = radius;
        calculateInvArea();
    }



    /*! \brief Sample object surface.
     *  \return Returns sample point on surface.
     */
    template <class SpectrumType>
    Point3D Sphere<SpectrumType>::sample()
    {
        Point3D samplePoint = this->sampler_->sampleUnitSphere();
        return samplePoint * radius_ + center_;
    }



    /*! \brief Get probability density function of samples on light area.
     *  \param[in] shadingData Shading data of the ray-object intersection.
     *  \return Returns probability density function at sample position.
     */
    template <class SpectrumType>
    float Sphere<SpectrumType>::pdf(const ShadingData<SpectrumType>& shadingData)
    {
        return invArea_;
    }



    /*! \brief Get normal at specified surface point.
     *  \param[in] point Point on object surface.
     *  \return Returns normal.
     */
    template <class SpectrumType>
    Normal Sphere<SpectrumType>::getNormal(const Point3D& point)
    {
        return Normal(point - center_) / radius_;
    }



    /*! \brief Calculates the localHitpoint.
     *  \param[in] shadingDataObject Shading data of this hit point. Member localHitPoint_ is set.
     *
     *  This method calculates the hit point coordinates in a spherical coordinate system originated in
     *  this spheres center. Values for localHitPoint_(u, v) are in the ranges u:[0, 180deg] and
     *  v:[0, 360deg].
     */
    template <class SpectrumType>
    void Sphere<SpectrumType>::calculateLocalHitPoint(ShadingData<SpectrumType>& shadingDataObject) const
    {
        Vector3D vectorCenterToHitpoint(shadingDataObject.hitPoint_ - center_);
        
        //double d = vectorCenterToHitpoint.length();
        //double d = sqrt(vectorCenterToHitpoint(1) * vectorCenterToHitpoint(1) + vectorCenterToHitpoint(2) * vectorCenterToHitpoint(2) + vectorCenterToHitpoint(3) * vectorCenterToHitpoint(3));
        float u = static_cast<float>(acos(vectorCenterToHitpoint(3) / radius_) * 180 * INV_PI);
        float v = static_cast<float>(atan2(vectorCenterToHitpoint(2), vectorCenterToHitpoint(1)) * 180 * INV_PI);
        if (v < 0.0)
        {
            v += 360.0f;
        }
        shadingDataObject.localHitPoint2D_ = Point2D(u, v);
        shadingDataObject.localHitPoint3D_ = Point3D((shadingDataObject.hitPoint_ - center_) / radius_);
    }



    /*! \brief Calculates inverse area of surface.
     */
    template <class SpectrumType>
    void Sphere<SpectrumType>::calculateInvArea()
    {
        invArea_ = 1.f / static_cast<float>(4.0 * PI * radius_ * radius_);
    }

} // end of namespace iiit

#endif // __SPHERE_HPP__
