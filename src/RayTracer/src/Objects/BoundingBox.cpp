// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file BoundingBox.cpp
 *  \author Thomas Nuernberg
 *  \date 08.05.2015
 *  \brief Implementation of class BoudingBox.
 */

#include "BoundingBox.hpp"

#include <limits>

#include "Utilities/Constants.h"



namespace iiit
{

    /*! \brief Default constructor.
     *
     *  Initializes bounding box containing the whole space.
     */
    BoundingBox::BoundingBox()
        : x0_(-std::numeric_limits<double>::infinity())
        , y0_(-std::numeric_limits<double>::infinity())
        , z0_(-std::numeric_limits<double>::infinity())
        , x1_(std::numeric_limits<double>::infinity())
        , y1_(std::numeric_limits<double>::infinity())
        , z1_(std::numeric_limits<double>::infinity())
    {
    }



    /*! \brief Constructor with initialization from point coordinates.
     *  \param[in] x0 X-coordinate of first corner.
     *  \param[in] y0 Y-coordinate of first corner.
     *  \param[in] z0 Z-coordinate of first corner.
     *  \param[in] x1 X-coordinate of second corner.
     *  \param[in] y1 Y-coordinate of second corner.
     *  \param[in] z1 Z-coordinate of second corner.
     */
    BoundingBox::BoundingBox(double x0, double y0, double z0, double x1, double y1, double z1)
        : x0_(x0)
        , y0_(y0)
        , z0_(z0)
        , x1_(x1)
        , y1_(y1)
        , z1_(z1)
    {
    }



    /*! \brief Constructor with initialization from points.
     *  \param[in] point0 First corner.
     *  \param[in] point1 Second corner.
     */
    BoundingBox::BoundingBox(const Point3D& point0, const Point3D& point1)
        : x0_((float) point0.x_)
        , y0_((float) point0.y_)
        , z0_((float) point0.z_)
        , x1_((float) point1.x_)
        , y1_((float) point1.y_)
        , z1_((float) point1.z_)
    {
    }



    /*! \brief Assignment operator.
     *  \param[in] other Bounding box to assign.
     *  \return Reference of altered bounding box.
     */
    BoundingBox& BoundingBox::operator=(const BoundingBox& other)
    {
        x0_ = other.x0_;
        y0_ = other.y0_;
        z0_ = other.z0_;
        x1_ = other.x1_;
        y1_ = other.y1_;
        z1_ = other.z1_;

        return *this;
    }



    /*! \brief Checks for hit points with ray.
     *  \param[in] ray Ray to intersect.
     *  \return Returns TRUE if the ray hits the bounding box, else FALSE.
     */
    bool BoundingBox::hit(const Ray& ray) const
    {
        double rayOriginX = ray.origin_.x_;
        double rayOriginY = ray.origin_.y_;
        double rayOriginZ = ray.origin_.z_;
        double rayDirectionX = ray.direction_.x_;
        double rayDirectionY = ray.direction_.y_;
        double rayDirectionZ = ray.direction_.z_;

        double txMin, txMax;
        double tyMin, tyMax;
        double tzMin, tzMax;

        double a = 1.0 / rayDirectionX;
        if (a >= 0.0)
        {
            txMin = (x0_ - rayOriginX) * a;
            txMax = (x1_ - rayOriginX) * a;
        }
        else
        {
            txMin = (x1_ - rayOriginX) * a;
            txMax = (x0_ - rayOriginX) * a;
        }

        double b = 1.0 / rayDirectionY;
        if (b >= 0.0)
        {
            tyMin = (y0_ - rayOriginY) * b;
            tyMax = (y1_ - rayOriginY) * b;
        }
        else
        {
            tyMin = (y1_ - rayOriginY) * b;
            tyMax = (y0_ - rayOriginY) * b;
        }

        double c = 1.0 / rayDirectionZ;
        if (c >= 0.0)
        {
            tzMin = (z0_ - rayOriginZ) * c;
            tzMax = (z1_ - rayOriginZ) * c;
        }
        else
        {
            tzMin = (z1_ - rayOriginZ) * c;
            tzMax = (z0_ - rayOriginZ) * c;
        }

        double t0;
        double t1;
        if (txMin > tyMin)
        {
            t0 = txMin;
        }
        else
        {
            t0 = tyMin;
        }
        if (tzMin > t0)
        {
            t0 = tzMin;
        }

        if (txMax < tyMax)
        {
            t1 = txMax;
        }
        else
        {
            t1 = tyMax;
        }
        if (tzMax < t1)
        {
            t1 = tzMax;
        }

        return (t0 < t1 && t1 > EPS);
    }



    /*! \brief Checks if point lies inside the bounding box.
     *  \param[in] point Point to check.
     *  \return Return TRUE if point lies inside the bounding box, else FALSE.
     */
    bool BoundingBox::inside(const Point3D& point) const
    {
        return ((point.x_ > x0_ && point.x_ < x1_) &&
            (point.y_ > y0_ && point.y_ < y1_) &&
            (point.z_ > z0_ && point.z_ < z1_));
    }

} // end of namespace iiit