// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Grid.hpp
 *  \author Thomas Nuernberg
 *  \date 20.05.2015
 *  \brief Definition of class Grid.
 */

#ifndef __GRID_HPP__
#define __GRID_HPP__

#include <vector>
#include <memory>
#include <limits>
#include <math.h>

#include "Compound.hpp"
#include "BoundingBox.hpp"
#include "Utilities/Ray.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Constants.h"


namespace iiit
{

    /*! \brief %Grid object.
     */
    template <class SpectrumType>
    class Grid : public Compound<SpectrumType>
    {
    public:
        Grid();
        virtual ~Grid();

        virtual bool hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const;
        virtual bool shadowHit(const Ray& shadowRay, double& tmin) const;
        virtual BoundingBox getBoundingBox() const;

        void initGrid();

        int getNumXCells() const;
        int getNumYCells() const;
        int getNumZCells() const;

    protected:
        Point3D minCoordinates() const;
        Point3D maxCoordinates() const;

        std::vector<std::shared_ptr<GeometricObject<SpectrumType> > > cells_; ///< Vector of grid cells.
        BoundingBox boundingBox_; ///< Bounding box of grid.
        int numXCells_; ///< Number of cells in x-direction.
        int numYCells_; ///< Number of cells in y-direction.
        int numZCells_; ///< Number of cells in z-direction.

    private:
        double clamp(double x, double min, double max) const;
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Grid<SpectrumType>::Grid()
        : boundingBox_()
        , numXCells_(1)
        , numYCells_(1)
        , numZCells_(1)
    {
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    Grid<SpectrumType>::~Grid()
    {
    }



    /*! \brief Initialized the grid.
     *
     *  Calculates a proper number of cells depending on the sizes of the stored objects and builds the
     *  grid. For all grid cells, references to all objects overlapping the cell are stored.
     *  \attention This method should be called once after all objects have been added.
     */
    template <class SpectrumType>
    void Grid<SpectrumType>::initGrid()
    {
        // build bounding box
        Point3D p0 = minCoordinates();
        Point3D p1 = maxCoordinates();
        boundingBox_.x0_ = p0.x_;
        boundingBox_.y0_ = p0.y_;
        boundingBox_.z0_ = p0.z_;
        boundingBox_.x1_ = p1.x_;
        boundingBox_.y1_ = p1.y_;
        boundingBox_.z1_ = p1.z_;
        
        // compute number of cells
        float xExtent = static_cast<float>(p1.x_ - p0.x_);
        float yExtent = static_cast<float>(p1.y_ - p0.y_);
        float zExtent = static_cast<float>(p1.z_ - p0.z_);
        float s = static_cast<float>(pow(xExtent * yExtent * zExtent / this->objects_.size(), 1.0 / 3.0));
        float multiplier = 2.f;
        numXCells_ = static_cast<int>(multiplier * xExtent / s + 1);
        numYCells_ = static_cast<int>(multiplier * yExtent / s + 1);
        numZCells_ = static_cast<int>(multiplier * zExtent / s + 1);
        
        // init array of cells
        cells_.resize(numXCells_ * numYCells_ * numZCells_, std::shared_ptr<GeometricObject<SpectrumType> >());
        
        // array for counting number of objects per cell
        std::vector<int> counts;
        counts.resize(numXCells_ * numYCells_ * numZCells_, 0);
        
        BoundingBox boundingBox; // temporary bounding box of objects
        int index; // cell index of objects
        for (unsigned int i = 0; i < this->objects_.size(); ++i)
        {
            boundingBox = this->objects_[i]->getBoundingBox();
            
            // compute indices of cells containing the corners of the bounding box
            int minXIndex = static_cast<int>(clamp((boundingBox.x0_ - p0.x_) * numXCells_ / (p1.x_ - p0.x_), 0.0, numXCells_ - 1.0));
            int maxXIndex = static_cast<int>(clamp((boundingBox.x1_ - p0.x_) * numXCells_ / (p1.x_ - p0.x_), 0.0, numXCells_ - 1.0));
            int minYIndex = static_cast<int>(clamp((boundingBox.y0_ - p0.y_) * numYCells_ / (p1.y_ - p0.y_), 0.0, numYCells_ - 1.0));
            int maxYIndex = static_cast<int>(clamp((boundingBox.y1_ - p0.y_) * numYCells_ / (p1.y_ - p0.y_), 0.0, numYCells_ - 1.0));
            int minZIndex = static_cast<int>(clamp((boundingBox.z0_ - p0.z_) * numZCells_ / (p1.z_ - p0.z_), 0.0, numZCells_ - 1.0));
            int maxZIndex = static_cast<int>(clamp((boundingBox.z1_ - p0.z_) * numZCells_ / (p1.z_ - p0.z_), 0.0, numZCells_ - 1.0));
            
            // add object to cells
            for (int xIndex = minXIndex; xIndex <= maxXIndex; ++xIndex)
            {
                for (int yIndex = minYIndex; yIndex <= maxYIndex; ++yIndex)
                {
                    for (int zIndex = minZIndex; zIndex <= maxZIndex; ++zIndex)
                    {
                        index = xIndex + numXCells_ * yIndex + numXCells_ * numYCells_ * zIndex;
                        
                        if (counts[index] == 0) // first object in cell
                        {
                            cells_[index] = this->objects_[i];
                        }
                        else if (counts[index] == 1) // second object in cell
                        {
                            // combine objects to compound object
                            std::shared_ptr<Compound<SpectrumType> > compoundObject(new Compound<SpectrumType>());
                            compoundObject->addObject(cells_[index]);
                            compoundObject->addObject(this->objects_[i]);
                            
                            cells_[index] = compoundObject;
                        }
                        else // more than 1 object already in cell, therefore cells_[index] holds a compound object
                        {
                            std::shared_ptr<Compound<SpectrumType> > compoundObject = std::static_pointer_cast<Compound<SpectrumType> >(cells_[index]);
                            compoundObject->addObject(this->objects_[i]);
                        }
                        counts[index]++;
                    }
                }
            }
        }
        
        // objects_ member can be cleared, as pointers to all objects are also stored in cells_
        this->objects_.clear();
        counts.clear();
    }



    /*! \brief Intersects grid with given ray.
     *  \param[in] ray Ray for intersection.
     *  \param[out] tmin Ray-parameter of hit point.
     *  \param[out] shadingData Shading data structure of hit point.
     *  \return Returns TRUE, if ray and object intersect, else FLASE.
     */
    template <class SpectrumType>
    bool Grid<SpectrumType>::hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const
    {
        // check if ray intersects the bounding box (code copied from BoundingBox::hit())
        double rayOriginX = ray.origin_.x_;
        double rayOriginY = ray.origin_.y_;
        double rayOriginZ = ray.origin_.z_;
        double rayDirectionX = ray.direction_.x_;
        double rayDirectionY = ray.direction_.y_;
        double rayDirectionZ = ray.direction_.z_;
        
        double txMin, txMax;
        double tyMin, tyMax;
        double tzMin, tzMax;
        
        double a = 1.0 / rayDirectionX;
        if (a >= 0.0)
        {
            txMin = (boundingBox_.x0_ - rayOriginX) * a;
            txMax = (boundingBox_.x1_ - rayOriginX) * a;
        }
        else
        {
            txMin = (boundingBox_.x1_ - rayOriginX) * a;
            txMax = (boundingBox_.x0_ - rayOriginX) * a;
        }
        
        double b = 1.0 / rayDirectionY;
        if (b >= 0.0)
        {
            tyMin = (boundingBox_.y0_ - rayOriginY) * b;
            tyMax = (boundingBox_.y1_ - rayOriginY) * b;
        }
        else
        {
            tyMin = (boundingBox_.y1_ - rayOriginY) * b;
            tyMax = (boundingBox_.y0_ - rayOriginY) * b;
        }
        
        double c = 1.0 / rayDirectionZ;
        if (c >= 0.0)
        {
            tzMin = (boundingBox_.z0_ - rayOriginZ) * c;
            tzMax = (boundingBox_.z1_ - rayOriginZ) * c;
        }
        else
        {
            tzMin = (boundingBox_.z1_ - rayOriginZ) * c;
            tzMax = (boundingBox_.z0_ - rayOriginZ) * c;
        }
        
        double t0; // minimum ray parameter, when entering the grid (= 0 when starting inside grid)
        double t1; // maximum ray parameter, when leaving the grid
        if (txMin > tyMin)
        {
            t0 = txMin;
        }
        else
        {
            t0 = tyMin;
        }
        if (tzMin > t0)
        {
            t0 = tzMin;
        }
        
        if (txMax < tyMax)
        {
            t1 = txMax;
        }
        else
        {
            t1 = tyMax;
        }
        if (tzMax < t1)
        {
            t1 = tzMax;
        }
        
        if (t0 > t1)
        {
            return false;
        }
        
        // indices of first cell
        int xIndex;
        int yIndex;
        int zIndex;
        
        if (boundingBox_.inside(ray.origin_)) // ray starts inside bounding box
        {
            xIndex = static_cast<int>(clamp((rayOriginX - boundingBox_.x0_) * numXCells_ / (boundingBox_.x1_ - boundingBox_.x0_), 0, numXCells_ - 1));
            yIndex = static_cast<int>(clamp((rayOriginY - boundingBox_.y0_) * numYCells_ / (boundingBox_.y1_ - boundingBox_.y0_), 0, numYCells_ - 1));
            zIndex = static_cast<int>(clamp((rayOriginZ - boundingBox_.z0_) * numZCells_ / (boundingBox_.z1_ - boundingBox_.z0_), 0, numZCells_ - 1));
        }
        else // ray starts outside bounding box, find first cell
        {
            Point3D p = ray.origin_ + t0 * ray.direction_;
            xIndex = static_cast<int>(clamp((p.x_ - boundingBox_.x0_) * numXCells_ / (boundingBox_.x1_ - boundingBox_.x0_), 0, numXCells_ - 1));
            yIndex = static_cast<int>(clamp((p.y_ - boundingBox_.y0_) * numYCells_ / (boundingBox_.y1_ - boundingBox_.y0_), 0, numYCells_ - 1));
            zIndex = static_cast<int>(clamp((p.z_ - boundingBox_.z0_) * numZCells_ / (boundingBox_.z1_ - boundingBox_.z0_), 0, numZCells_ - 1));
        }
        
        // ray parameter increments for traversing the grid
        float deltaTx = static_cast<float>((txMax - txMin) / numXCells_);
        float deltaTy = static_cast<float>((tyMax - tyMin) / numYCells_);
        float deltaTz = static_cast<float>((tzMax - tzMin) / numZCells_);
        
        float nextTx;
        float nextTy;
        float nextTz;
        
        int xIndexStep;
        int xIndexStop;
        int yIndexStep;
        int yIndexStop;
        int zIndexStep;
        int zIndexStop;
        
        if (rayDirectionX > 0.0) // positive step size
        {
            nextTx = static_cast<float>(txMin + (xIndex + 1) * deltaTx);
            xIndexStep = 1;
            xIndexStop = numXCells_;
        }
        else if (rayDirectionX == 0.0) // infinite step size -> cell index never changes
        {
            nextTx = std::numeric_limits<float>::infinity();
            xIndexStep = -1;
            xIndexStop = -1;
        }
        else // negative step size
        {
            nextTx = static_cast<float>(txMin + (numXCells_ - xIndex) * deltaTx);
            xIndexStep = -1;
            xIndexStop = -1;
        }
        
        if (rayDirectionY > 0.0) // positive step size
        {
            nextTy = static_cast<float>(tyMin + (yIndex + 1) * deltaTy);
            yIndexStep = 1;
            yIndexStop = numYCells_;
        }
        else if (rayDirectionY == 0.0) // infinite step size -> cell index never changes
        {
            nextTy = std::numeric_limits<float>::infinity();
            yIndexStep = -1;
            yIndexStop = -1;
        }
        else // negative step size
        {
            nextTy = static_cast<float>(tyMin + (numYCells_ - yIndex) * deltaTy);
            yIndexStep = -1;
            yIndexStop = -1;
        }
        
        if (rayDirectionZ > 0.0) // positive step size
        {
            nextTz = static_cast<float>(tzMin + (zIndex + 1) * deltaTz);
            zIndexStep = 1;
            zIndexStop = numZCells_;
        }
        else if (rayDirectionZ == 0.0) // infinite step size -> cell index never changes
        {
            nextTz = std::numeric_limits<float>::infinity();
            zIndexStep = -1;
            zIndexStop = -1;
        }
        else // negative step size
        {
            nextTz = static_cast<float>(tzMin + (numZCells_ - zIndex) * deltaTz);
            zIndexStep = -1;
            zIndexStop = -1;
        }
        
        // traverse the grid
        while (true)
        {
            std::shared_ptr<GeometricObject<SpectrumType> > object = cells_[xIndex + numXCells_ * yIndex + numXCells_ * numYCells_ * zIndex];
            
            if (nextTx < nextTy && nextTx < nextTz)
            {
                // step in x-direction
                if (object && object->hit(ray, tmin, shadingData) && tmin <= nextTx)
                {
                    return true;
                }
                
                nextTx += deltaTx;
                xIndex += xIndexStep;
                
                if (xIndex == xIndexStop)
                {
                    return false;
                }
            }
            else if (nextTy < nextTz)
            {
                // step in y-direction
                if (object && object->hit(ray, tmin, shadingData) && tmin <= nextTy)
                {
                    return true;
                }
                
                nextTy += deltaTy;
                yIndex += yIndexStep;
                
                if (yIndex == yIndexStop)
                {
                    return false;
                }
            }
            else
            {
                // step in z-direction
                if (object && object->hit(ray, tmin, shadingData) && tmin <= nextTz)
                {
                    return true;
                }
                
                nextTz += deltaTz;
                zIndex += zIndexStep;
                
                if (zIndex == zIndexStop)
                {
                    return false;
                }
            }
        }
    }



    /*! \brief Intersects object with given shadow ray.
     *  \param[in] shadowRay Ray for intersection.
     *  \param[out] tmin Ray-parameter of hit point.
     *  \return Returns TRUE, if ray and object intersect, else FLASE.
     */
    template <class SpectrumType>
    bool Grid<SpectrumType>::shadowHit(const Ray& shadowRay, double& tmin) const
    {
        if (!this->castsShadows_)
        {
            return false;
        }

        // check if ray intersects the bounding box (code copied from BoundingBox::hit())
        double rayOriginX = shadowRay.origin_.x_;
        double rayOriginY = shadowRay.origin_.y_;
        double rayOriginZ = shadowRay.origin_.z_;
        double rayDirectionX = shadowRay.direction_.x_;
        double rayDirectionY = shadowRay.direction_.y_;
        double rayDirectionZ = shadowRay.direction_.z_;
        
        double txMin, txMax;
        double tyMin, tyMax;
        double tzMin, tzMax;
        
        double a = 1.0 / rayDirectionX;
        if (a >= 0.0)
        {
            txMin = (boundingBox_.x0_ - rayOriginX) * a;
            txMax = (boundingBox_.x1_ - rayOriginX) * a;
        }
        else
        {
            txMin = (boundingBox_.x1_ - rayOriginX) * a;
            txMax = (boundingBox_.x0_ - rayOriginX) * a;
        }
        
        double b = 1.0 / rayDirectionY;
        if (b >= 0.0)
        {
            tyMin = (boundingBox_.y0_ - rayOriginY) * b;
            tyMax = (boundingBox_.y1_ - rayOriginY) * b;
        }
        else
        {
            tyMin = (boundingBox_.y1_ - rayOriginY) * b;
            tyMax = (boundingBox_.y0_ - rayOriginY) * b;
        }
        
        double c = 1.0 / rayDirectionZ;
        if (c >= 0.0)
        {
            tzMin = (boundingBox_.z0_ - rayOriginZ) * c;
            tzMax = (boundingBox_.z1_ - rayOriginZ) * c;
        }
        else
        {
            tzMin = (boundingBox_.z1_ - rayOriginZ) * c;
            tzMax = (boundingBox_.z0_ - rayOriginZ) * c;
        }
        
        double t0; // minimum ray parameter, when entering the grid (= 0 when starting inside grid)
        double t1; // maximum ray parameter, when leaving the grid
        if (txMin > tyMin)
        {
            t0 = txMin;
        }
        else
        {
            t0 = tyMin;
        }
        if (tzMin > t0)
        {
            t0 = tzMin;
        }
        
        if (txMax < tyMax)
        {
            t1 = txMax;
        }
        else
        {
            t1 = tyMax;
        }
        if (tzMax < t1)
        {
            t1 = tzMax;
        }
        
        if (t0 > t1)
        {
            return false;
        }
        
        // indices of first cell
        int xIndex;
        int yIndex;
        int zIndex;
        
        if (boundingBox_.inside(shadowRay.origin_)) // ray starts inside bounding box
        {
            xIndex = static_cast<int>(clamp((rayOriginX - boundingBox_.x0_) * numXCells_ / (boundingBox_.x1_ - boundingBox_.x0_), 0, numXCells_ - 1));
            yIndex = static_cast<int>(clamp((rayOriginY - boundingBox_.y0_) * numYCells_ / (boundingBox_.y1_ - boundingBox_.y0_), 0, numYCells_ - 1));
            zIndex = static_cast<int>(clamp((rayOriginZ - boundingBox_.z0_) * numZCells_ / (boundingBox_.z1_ - boundingBox_.z0_), 0, numZCells_ - 1));
        }
        else // ray starts outside bounding box, find first cell
        {
            Point3D p = shadowRay.origin_ + t0 * shadowRay.direction_;
            xIndex = static_cast<int>(clamp((p.x_ - boundingBox_.x0_) * numXCells_ / (boundingBox_.x1_ - boundingBox_.x0_), 0, numXCells_ - 1));
            yIndex = static_cast<int>(clamp((p.y_ - boundingBox_.y0_) * numYCells_ / (boundingBox_.y1_ - boundingBox_.y0_), 0, numYCells_ - 1));
            zIndex = static_cast<int>(clamp((p.z_ - boundingBox_.z0_) * numZCells_ / (boundingBox_.z1_ - boundingBox_.z0_), 0, numZCells_ - 1));
        }
        
        // ray parameter increments for traversing the grid
        float deltaTx = static_cast<float>((txMax - txMin) / numXCells_);
        float deltaTy = static_cast<float>((tyMax - tyMin) / numYCells_);
        float deltaTz = static_cast<float>((tzMax - tzMin) / numZCells_);
        
        float nextTx;
        float nextTy;
        float nextTz;
        
        int xIndexStep;
        int xIndexStop;
        int yIndexStep;
        int yIndexStop;
        int zIndexStep;
        int zIndexStop;
        
        if (rayDirectionX > 0.0) // positive step size
        {
            nextTx = static_cast<float>(txMin + (xIndex + 1) * deltaTx);
            xIndexStep = 1;
            xIndexStop = numXCells_;
        }
        else if (rayDirectionX == 0.0) // infinite step size -> cell index never changes
        {
            nextTx = std::numeric_limits<float>::infinity();
            xIndexStep = -1;
            xIndexStop = -1;
        }
        else // negative step size
        {
            nextTx = static_cast<float>(txMin + (numXCells_ - xIndex) * deltaTx);
            xIndexStep = -1;
            xIndexStop = -1;
        }
        
        if (rayDirectionY > 0.0) // positive step size
        {
            nextTy = static_cast<float>(tyMin + (yIndex + 1) * deltaTy);
            yIndexStep = 1;
            yIndexStop = numYCells_;
        }
        else if (rayDirectionY == 0.0) // infinite step size -> cell index never changes
        {
            nextTy = std::numeric_limits<float>::infinity();
            yIndexStep = -1;
            yIndexStop = -1;
        }
        else // negative step size
        {
            nextTy = static_cast<float>(tyMin + (numYCells_ - yIndex) * deltaTy);
            yIndexStep = -1;
            yIndexStop = -1;
        }
        
        if (rayDirectionZ > 0.0) // positive step size
        {
            nextTz = static_cast<float>(tzMin + (zIndex + 1) * deltaTz);
            zIndexStep = 1;
            zIndexStop = numZCells_;
        }
        else if (rayDirectionZ == 0.0) // infinite step size -> cell index never changes
        {
            nextTz = std::numeric_limits<float>::infinity();
            zIndexStep = -1;
            zIndexStop = -1;
        }
        else // negative step size
        {
            nextTz = static_cast<float>(tzMin + (numZCells_ - zIndex) * deltaTz);
            zIndexStep = -1;
            zIndexStop = -1;
        }
        
        // traverse the grid
        while (true)
        {
            std::shared_ptr<GeometricObject<SpectrumType> > object = cells_[xIndex + numXCells_ * yIndex + numXCells_ * numYCells_ * zIndex];
            
            if (nextTx < nextTy && nextTx < nextTz)
            {
                // step in x-direction
                if (object && object->shadowHit(shadowRay, tmin) && tmin <= nextTx)
                {
                    return true;
                }
                
                nextTx += deltaTx;
                xIndex += xIndexStep;
                
                if (xIndex == xIndexStop)
                {
                    return false;
                }
            }
            else if (nextTy < nextTz)
            {
                // step in y-direction
                if (object && object->shadowHit(shadowRay, tmin) && tmin <= nextTy)
                {
                    return true;
                }
                
                nextTy += deltaTy;
                yIndex += yIndexStep;
                
                if (yIndex == yIndexStop)
                {
                    return false;
                }
            }
            else
            {
                // step in z-direction
                if (object && object->shadowHit(shadowRay, tmin) && tmin <= nextTz)
                {
                    return true;
                }
                
                nextTz += deltaTz;
                zIndex += zIndexStep;
                
                if (zIndex == zIndexStop)
                {
                    return false;
                }
            }
        }
    }



    /*! \brief Returns the bounding box of the grid object.
     *  \return Bounding box of grid object.
     */
    template <class SpectrumType>
    BoundingBox Grid<SpectrumType>::getBoundingBox() const
    {
        return boundingBox_;
    }



    /*! \brief Returns number of cells in x-dimension.
     *  \return Number of cells in x-dimension.
     */
    template <class SpectrumType>
    int Grid<SpectrumType>::getNumXCells() const
    {
        return numXCells_;
    }



    /*! \brief Returns number of cells in y-dimension.
     *  \return Number of cells in y-dimension.
     */
    template <class SpectrumType>
    int Grid<SpectrumType>::getNumYCells() const
    {
        return numYCells_;
    }



    /*! \brief Returns number of cells in z-dimension.
     *  \return Number of cells in z-dimension.
     */
    template <class SpectrumType>
    int Grid<SpectrumType>::getNumZCells() const
    {
        return numZCells_;
    }



    /*! \brief Get corner of grid with minimal coordinates.
     *  \return Corner with minimal coordinates.
     */
    template <class SpectrumType>
    Point3D Grid<SpectrumType>::minCoordinates() const
    {
        BoundingBox boundingBox;
        Point3D minCorner(std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity());
        
        for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = this->objects_.begin(); it != this->objects_.end(); ++it)
        {
            boundingBox = (*it)->getBoundingBox();
            
            if (boundingBox.x0_ < minCorner.x_)
            {
                minCorner.x_ = boundingBox.x0_;
            }
            if (boundingBox.y0_ < minCorner.y_)
            {
                minCorner.y_ = boundingBox.y0_;
            }
            if (boundingBox.z0_ < minCorner.z_)
            {
                minCorner.z_ = boundingBox.z0_;
            }
        }
        
        minCorner.x_ -= EPS;
        minCorner.y_ -= EPS;
        minCorner.z_ -= EPS;
        
        return minCorner;
    }



    /*! \brief Get corner of grid with maximum coordinates.
     *  \return Corner with maximum coordinates.
     */
    template <class SpectrumType>
    Point3D Grid<SpectrumType>::maxCoordinates() const
    {
        BoundingBox boundingBox;
        Point3D maxCorner(-std::numeric_limits<double>::infinity(), -std::numeric_limits<double>::infinity(), -std::numeric_limits<double>::infinity());
        
        for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = this->objects_.begin(); it != this->objects_.end(); ++it)
        {
            boundingBox = (*it)->getBoundingBox();
            
            if (boundingBox.x1_ > maxCorner.x_)
            {
                maxCorner.x_ = boundingBox.x1_;
            }
            if (boundingBox.y1_ > maxCorner.y_)
            {
                maxCorner.y_ = boundingBox.y1_;
            }
            if (boundingBox.z1_ > maxCorner.z_)
            {
                maxCorner.z_ = boundingBox.z1_;
            }
        }
        
        maxCorner.x_ += EPS;
        maxCorner.y_ += EPS;
        maxCorner.z_ += EPS;
        
        return maxCorner;
    }
    
    
    
    /*! \brief Clamp value to minimum and maximum value.
     *  \param[in] x Value.
     *  \param[in] min Minimum value.
     *  \param[in] max Maximum value.
     *  \return Clamped value.
     */
    template <class SpectrumType>
    double Grid<SpectrumType>::clamp(double x, double min, double max) const
    {
        return (x < min ? min : (x > max ? max : x));
    }

} // end of namespace iiit

#endif // __GRID_HPP__