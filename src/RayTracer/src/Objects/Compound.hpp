// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Compound.hpp
 *  \author Thomas Nuernberg
 *  \date 18.05.2015
 *  \brief Definition of class Compound.
 */

#ifndef __COMPOUND_HPP__
#define __COMPOUND_HPP__

#include <vector>
#include <memory>
#include <limits>

#include "GeometricObject.hpp"
#include "BoundingBox.hpp"
#include "Utilities/Ray.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Normal.hpp"
#include "Materials/Material.hpp"



namespace iiit
{

    /*! \brief %Compound object.
     *
     *  Class for compound objects.
     */
    template <class SpectrumType>
    class Compound : public GeometricObject<SpectrumType>
    {
    public:
        Compound();
        Compound(const Compound<SpectrumType>& other);
        virtual ~Compound();

        virtual Compound<SpectrumType>* clone() const;

        /*! \brief Get the type of the geometric object.
         *  \return Returns the type of the geometric object.
         */
        virtual typename GeometricObject<SpectrumType>::Type getType() const {return GeometricObject<SpectrumType>::Compound;}
        virtual bool hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const;
        virtual bool shadowHit(const Ray& shadowRay, double& tmin) const;
        virtual BoundingBox getBoundingBox() const;

        void addObject(std::shared_ptr<GeometricObject<SpectrumType> > object);

        virtual void setMaterial(std::shared_ptr<Material<SpectrumType> > material);

        void reserve(int size);

        std::vector<std::shared_ptr<GeometricObject<SpectrumType> > > objects_; ///< Objects forming the compound object.
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Compound<SpectrumType>::Compound()
    {
    }



    /*! \brief Copy constructor
     *  \param[in] other Compound object to copy.
     *
     *  Deep copies objects of compound object.
     */
    template <class SpectrumType>
    Compound<SpectrumType>::Compound(const Compound<SpectrumType>& other)
    {
        objects_.resize(other.objects_.size(), std::shared_ptr<GeometricObject<SpectrumType> >());
        typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::iterator it1 = objects_.begin();
        typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it2 = other.objects_.begin();
        for ( ; it1 != objects_.end(); ++it1, ++it2)
        {
            *it1 = std::shared_ptr<GeometricObject<SpectrumType> >((*it2)->clone());
        }
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    Compound<SpectrumType>::~Compound()
    {
    }



    /*! \brief Clone (make deep copy of) object.
     *  \return Returned cloned object.
     */
    template <class SpectrumType>
    Compound<SpectrumType>* Compound<SpectrumType>::clone() const
    {
        return new Compound<SpectrumType>(*this);
    }



    /*! \brief Intersects object with given ray.
     *  \param[in] ray Ray for intersection.
     *  \param[out] tmin Ray-parameter of hit point.
     *  \param[out] shadingData Shading data structure of hit point.
     *  \return Returns TRUE, if ray and object intersect, else FLASE.
     */
    template <class SpectrumType>
    bool Compound<SpectrumType>::hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const
    {
        bool b = false;
        tmin = std::numeric_limits<double>::infinity();
        for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = objects_.begin(); it != objects_.end(); ++it)
        {
            double t;
            ShadingData<SpectrumType> tmpShadingData(shadingData.scene_);
            bool hit = (*it)->hit(ray, t, tmpShadingData);
            if (hit && t < tmin && t > 0.0)
            {
                b = hit;
                tmin = t;
                shadingData = tmpShadingData;
            }
        }
        return b;
    }



    /*! \brief Intersects object with given shadow ray.
     *  \param[in] shadowRay Ray for intersection.
     *  \param[out] tmin Ray-parameter of hit point.
     *  \return Returns TRUE, if ray and object intersect, else FLASE.
     */
    template <class SpectrumType>
    bool Compound<SpectrumType>::shadowHit(const Ray& shadowRay, double& tmin) const
    {
        if (!this->castsShadows_)
        {
            return false;
        }

        bool b = false;
        tmin = std::numeric_limits<double>::infinity();
        for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = objects_.begin(); it != objects_.end(); ++it)
        {
            double t;
            bool hit = (*it)->shadowHit(shadowRay, t);
            if (hit && t < tmin && t > 0.0)
            {
                b = hit;
                tmin = t;
            }
        }
        return b;
    }



    /*! \brief Computes and returns the bounding box of the compound object.
     *  \return Bounding box of compound object.
     */
    template <class SpectrumType>
    BoundingBox Compound<SpectrumType>::getBoundingBox() const
    {
        Point3D p0(std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity(), std::numeric_limits<float>::infinity());
        Point3D p1(-std::numeric_limits<float>::infinity(), -std::numeric_limits<float>::infinity(), -std::numeric_limits<float>::infinity());
        
        BoundingBox boundingBox;
        for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::const_iterator it = objects_.begin(); it != objects_.end(); ++it)
        {
            boundingBox = (*it)->getBoundingBox();
            if (boundingBox.x0_ < p0.x_)
            {
                p0.x_ = boundingBox.x0_;
            }
            if (boundingBox.x1_ > p1.x_)
            {
                p1.x_ = boundingBox.x1_;
            }
            if (boundingBox.y0_ < p0.y_)
            {
                p0.y_ = boundingBox.y0_;
            }
            if (boundingBox.y1_ > p1.y_)
            {
                p1.y_ = boundingBox.y1_;
            }
            if (boundingBox.z0_ < p0.z_)
            {
                p0.z_ = boundingBox.z0_;
            }
            if (boundingBox.z1_ > p1.z_)
            {
                p1.z_ = boundingBox.z1_;
            }
        }
        return BoundingBox(p0.x_, p0.y_, p0.z_, p1.x_, p1.y_, p1.z_);
    }



    /*! \brief Add Object to compound.
     *  \param[in] object Object to add.
     */
    template <class SpectrumType>
    void Compound<SpectrumType>::addObject(std::shared_ptr<GeometricObject<SpectrumType> > object)
    {
        objects_.push_back(object);
    }



    /*! \brief Sets material of all stored objects.
     *  \param[in] material Object material.
     */
    template <class SpectrumType>
    void Compound<SpectrumType>::setMaterial(std::shared_ptr<Material<SpectrumType> > material)
    {
        for (typename std::vector<std::shared_ptr<GeometricObject<SpectrumType> > >::iterator it = objects_.begin(); it != objects_.end(); ++it)
        {
            (*it)->setMaterial(material);
        }
    }



    /*! \brief Reserve memory to hold object pointers.
     *  \param[in] size Required vector size.
     */
    template <class SpectrumType>
    void Compound<SpectrumType>::reserve(int size)
    {
        objects_.reserve(size);
    }

} // end of namespace iiit

#endif // __COMPOUND_HPP__