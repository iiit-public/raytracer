// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Triangle.hpp
 *  \author Thomas Nuernberg
 *  \date 15.05.2015
 *  \brief Definition of class Triangle.
 */

#ifndef __TRIANGLE_HPP__
#define __TRIANGLE_HPP__

#include <limits>
#include <algorithm>

#include "GeometricObject.hpp"
#include "Utilities/Constants.h"
#include "Utilities/Ray.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/Normal.hpp"



namespace iiit
{

    /*! \brief %Triangle object.
     */
    template <class SpectrumType>
    class Triangle : public GeometricObject<SpectrumType>
    {
    public:
        Triangle();
        Triangle(const Point3D& vertex0, const Point3D& vertex1, const Point3D& vertex2);
        Triangle(const Point3D& vertex0, const Point3D& vertex1, const Point3D& vertex2, std::shared_ptr<Material<SpectrumType> > material);
        Triangle(const Triangle<SpectrumType>& other);
        virtual ~Triangle();

        virtual Triangle<SpectrumType>* clone() const;

        /*! \brief Get the type of the geometric object.
         *  \return Returns the type of the geometric object.
         */
        virtual typename GeometricObject<SpectrumType>::Type getType() const {return GeometricObject<SpectrumType>::Triangle;}
        virtual bool hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const;
        virtual bool shadowHit(const Ray& shadowRay, double& tmin) const;
        virtual BoundingBox getBoundingBox() const;

        void computeNormal();

    //private:
        Point3D vertex0_; ///< First vertex of triangle.
        Point3D vertex1_; ///< Second vertex of triangle.
        Point3D vertex2_; ///< Third vertex of triangle.
        Normal normal_; ///< Normal of plane.
    };

    

    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Triangle<SpectrumType>::Triangle()
        : GeometricObject<SpectrumType>(SpectrumType(0.5f))
        , vertex0_(0.0, 0.0, 0.0)
        , vertex1_(0.0, 0.0, 1.0)
        , vertex2_(1.0, 0.0, 0.0)
        , normal_(0.0, 1.0, 0.0)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] vertex0 First vertex of triangle.
     *  \param[in] vertex1 Second vertex of triangle.
     *  \param[in] vertex2 Third vertex of triangle.
     */
    template <class SpectrumType>
    Triangle<SpectrumType>::Triangle(const Point3D& vertex0, const Point3D& vertex1, const Point3D& vertex2)
        : GeometricObject<SpectrumType>(SpectrumType(0.f))
        , vertex0_(vertex0)
        , vertex1_(vertex1)
        , vertex2_(vertex2)
    {
        computeNormal();
    }



    /*! \brief Constructor with initialization.
     *  \param[in] vertex0 First vertex of triangle.
     *  \param[in] vertex1 Second vertex of triangle.
     *  \param[in] vertex2 Third vertex of triangle.
     *  \param[in] material Material assigned to the triangle.
     */
    template <class SpectrumType>
    Triangle<SpectrumType>::Triangle(const Point3D& vertex0, const Point3D& vertex1, const Point3D& vertex2, std::shared_ptr<Material<SpectrumType> > material)
        : GeometricObject<SpectrumType>(material)
        , vertex0_(vertex0)
        , vertex1_(vertex1)
        , vertex2_(vertex2)
    {
        computeNormal();
    }



    /*! \brief Copy constructor.
     *  \param[in] other Triangle object to copy.
     */
    template <class SpectrumType>
    Triangle<SpectrumType>::Triangle(const Triangle<SpectrumType>& other)
        : GeometricObject<SpectrumType>(other)
        , vertex0_(other.vertex0_)
        , vertex1_(other.vertex1_)
        , vertex2_(other.vertex2_)
        , normal_(other.normal_)
    {
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    Triangle<SpectrumType>::~Triangle()
    {
    }



    /*! \brief Clone (make deep copy of) object.
     *  \return Returned cloned object.
     */
    template <class SpectrumType>
    Triangle<SpectrumType>* Triangle<SpectrumType>::clone() const
    {
        return new Triangle<SpectrumType>(*this);
    }



    /*! \brief Intersects object with given ray.
     *  \param[in] ray Ray for intersection.
     *  \param[out] tmin Ray-parameter of hit point.
     *  \param[out] shadingData Shading data structure of hit point.
     *  \return Returns TRUE, if ray and object intersect, else FLASE.
     */
    template <class SpectrumType>
    bool Triangle<SpectrumType>::hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const
    {
        double a = vertex0_.x_ - vertex1_.x_;
        double b = vertex0_.x_ - vertex2_.x_;
        double c = ray.direction_.x_;
        double d = vertex0_.x_ - ray.origin_.x_;
        double e = vertex0_.y_ - vertex1_.y_;
        double f = vertex0_.y_ - vertex2_.y_;
        double g = ray.direction_.y_;
        double h = vertex0_.y_ - ray.origin_.y_;
        double i = vertex0_.z_ - vertex1_.z_;
        double j = vertex0_.z_ - vertex2_.z_;
        double k = ray.direction_.z_;
        double l = vertex0_.z_ - ray.origin_.z_;
        
        double m = f * k - g * j;
        double n = h * k - g * l;
        double p = f * l - h * j;
        double q = g * i - e * k;
        double s = e * j - f * i;
        
        double invDenom = 1.0 / (a * m + b * q + c * s);
        
        double e1 = d * m - b * n - c * p;
        double beta = e1 * invDenom;
        if (beta < 0.0)
        {
            return false;
        }
        
        double r = e * l - h * i;
        double e2 = a * n + d * q + c * r;
        double gamma = e2 * invDenom;
        if (gamma < 0.0)
        {
            return false;
        }
        
        if ((beta + gamma) > 1.0)
        {
            return false;
        }
        
        double e3 = a * p - b * r + d * s;
        double t = e3 * invDenom;

        if (t < EPS || t != t)
        {
            return false;
        }
        
        tmin = t;
        shadingData.hitAnObject_ = true;
        shadingData.normal_ = normal_;
        shadingData.hitPoint_ = ray.origin_ + t * ray.direction_;
        shadingData.ray_ = ray;
        shadingData.material_ = this->objectMaterial_;

        // Define local hitpoint relative to minimum x, y, z coordinate of the bounding box
        double x0 = this->getBoundingBox().x0_;
        double y0 = this->getBoundingBox().y0_;
        double z0 = this->getBoundingBox().z0_;
        shadingData.localHitPoint3D_ = shadingData.hitPoint_ - Point3D(x0, y0, z0);
        
        return true;
    }



    /*! \brief Intersects object with given shadow ray.
     *  \param[in] shadowRay Ray for intersection.
     *  \param[out] tmin Ray-parameter of hit point.
     *  \return Returns TRUE, if ray and object intersect, else FLASE.
     */
    template <class SpectrumType>
    bool Triangle<SpectrumType>::shadowHit(const Ray& shadowRay, double& tmin) const
    {
        if (!this->castsShadows_)
        {
            return false;
        }

        double a = vertex0_.x_ - vertex1_.x_;
        double b = vertex0_.x_ - vertex2_.x_;
        double c = shadowRay.direction_.x_;
        double d = vertex0_.x_ - shadowRay.origin_.x_;
        double e = vertex0_.y_ - vertex1_.y_;
        double f = vertex0_.y_ - vertex2_.y_;
        double g = shadowRay.direction_.y_;
        double h = vertex0_.y_ - shadowRay.origin_.y_;
        double i = vertex0_.z_ - vertex1_.z_;
        double j = vertex0_.z_ - vertex2_.z_;
        double k = shadowRay.direction_.z_;
        double l = vertex0_.z_ - shadowRay.origin_.z_;
        
        double m = f * k - g * j;
        double n = h * k - g * l;
        double p = f * l - h * j;
        double q = g * i - e * k;
        double s = e * j - f * i;
        
        double invDenom = 1.0 / (a * m + b * q + c * s);
        
        double e1 = d * m - b * n - c * p;
        double beta = e1 * invDenom;
        if (beta < 0.0)
        {
            return false;
        }
        
        double r = e * l - h * i;
        double e2 = a * n + d * q + c * r;
        double gamma = e2 * invDenom;
        if (gamma < 0.0)
        {
            return false;
        }
        
        if ((beta + gamma) > 1.0)
        {
            return false;
        }
        
        double e3 = a * p - b * r + d * s;
        double t = e3 * invDenom;
        
        if (t < EPS || t != t)
        {
            return false;
        }
        
        tmin = t;
        return true;
    }




    /*! \brief Computes and returns the bounding box of the triangle.
     *  \return Bounding box of triangle.
     */
    template <class SpectrumType>
    BoundingBox Triangle<SpectrumType>::getBoundingBox() const
    {
        return BoundingBox(std::min(std::min(vertex0_.x_, vertex1_.x_), vertex2_.x_),
            std::min(std::min(vertex0_.y_, vertex1_.y_), vertex2_.y_),
            std::min(std::min(vertex0_.z_, vertex1_.z_), vertex2_.z_),
            std::max(std::max(vertex0_.x_, vertex1_.x_), vertex2_.x_),
            std::max(std::max(vertex0_.y_, vertex1_.y_), vertex2_.y_),
            std::max(std::max(vertex0_.z_, vertex1_.z_), vertex2_.z_));
    }




    /*! \brief Compute normal from vertexes.
     */
    template <class SpectrumType>
    void Triangle<SpectrumType>::computeNormal()
    {
        Vector3D v01 = vertex1_ - vertex0_;
        Vector3D v02 = vertex2_ - vertex0_;
        
        normal_ = v01.crossProduct(v02);
        normal_.normalize();
    }

} // end of namespace iiit

#endif // __TRIANGLE_HPP__
