// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file SmoothMeshTriangle.hpp
 *  \author Thomas Nuernberg
 *  \date 12.06.2015
 *  \brief Definition of class SmoothMeshTriangle.
 */

#ifndef __SMOOTHMESHTRIANGLE_HPP__
#define __SMOOTHMESHTRIANGLE_HPP__

#include <vector>
#include <memory>

#include "GeometricObject.hpp"
#include "MeshTriangle.hpp"
#include "Mesh.hpp"
#include "Utilities/Constants.h"
#include "Utilities/Point3D.hpp"
#include "Utilities/Normal.hpp"
#include "Materials/Material.hpp"



namespace iiit
{

    /*! \brief %FlatMeshTriangle object.
     */
    template <class SpectrumType>
    class SmoothMeshTriangle : public MeshTriangle<SpectrumType>
    {
    public:
        SmoothMeshTriangle();
        SmoothMeshTriangle(const SpectrumType& color);
        SmoothMeshTriangle(std::shared_ptr<Material<SpectrumType> > material);
        SmoothMeshTriangle(std::weak_ptr<Mesh> mesh, int index0, int index1, int index2);
        SmoothMeshTriangle(std::weak_ptr<Mesh> mesh, int index0, int index1, int index2, const SpectrumType& color);
        SmoothMeshTriangle(std::weak_ptr<Mesh> mesh, int index0, int index1, int index2, std::shared_ptr<Material<SpectrumType> > material);
        SmoothMeshTriangle(const SmoothMeshTriangle<SpectrumType>& other);
        virtual ~SmoothMeshTriangle();

        virtual SmoothMeshTriangle<SpectrumType>* clone() const;

        /*! \brief Get the type of the geometric object.
         *  \return Returns the type of the geometric object.
         */
        virtual typename GeometricObject<SpectrumType>::Type getType() const {return GeometricObject<SpectrumType>::SmoothTriangle;}
        virtual bool hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const;
        virtual bool shadowHit(const Ray& shadowRay, double& tmin) const;

    private:
        Normal interpolateNormal(double beta, double gamma) const;
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    SmoothMeshTriangle<SpectrumType>::SmoothMeshTriangle()
        : MeshTriangle<SpectrumType>()
    {
    }



    /*! \brief Constructor with material initialization from color.
     *  \param[in] color Color of object.
     */
    template <class SpectrumType>
    SmoothMeshTriangle<SpectrumType>::SmoothMeshTriangle(const SpectrumType& color)
        : MeshTriangle<SpectrumType>(color)
    {
    }



    /*! \brief Constructor with material initialization from Material object.
     *  \param[in] material Object material.
     */
    template <class SpectrumType>
    SmoothMeshTriangle<SpectrumType>::SmoothMeshTriangle(std::shared_ptr<Material<SpectrumType> > material)
        : MeshTriangle<SpectrumType>(material)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] mesh Pointer to Mesh object storing the vertices.
     *  \param[in] index0 Index of first vertex of the triangle.
     *  \param[in] index1 Index of second vertex of the triangle.
     *  \param[in] index2 Index of third vertex of the triangle.
     */
    template <class SpectrumType>
    SmoothMeshTriangle<SpectrumType>::SmoothMeshTriangle(std::weak_ptr<Mesh> mesh, int index0, int index1, int index2)
        : MeshTriangle<SpectrumType>(mesh, index0, index1, index2)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] mesh Pointer to Mesh object storing the vertices.
     *  \param[in] index0 Index of first vertex of the triangle.
     *  \param[in] index1 Index of second vertex of the triangle.
     *  \param[in] index2 Index of third vertex of the triangle.
     *  \param[in] color Color of object.
     */
    template <class SpectrumType>
    SmoothMeshTriangle<SpectrumType>::SmoothMeshTriangle(std::weak_ptr<Mesh> mesh, int index0, int index1, int index2, const SpectrumType& color)
        : MeshTriangle<SpectrumType>(mesh, index0, index1, index2, color)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] mesh Pointer to Mesh object storing the vertices.
     *  \param[in] index0 Index of first vertex of the triangle.
     *  \param[in] index1 Index of second vertex of the triangle.
     *  \param[in] index2 Index of third vertex of the triangle.
     *  \param[in] material Object material.
     */
    template <class SpectrumType>
    SmoothMeshTriangle<SpectrumType>::SmoothMeshTriangle(std::weak_ptr<Mesh> mesh, int index0, int index1, int index2, std::shared_ptr<Material<SpectrumType> > material)
        : MeshTriangle<SpectrumType>(mesh, index0, index1, index2, material)
    {
    }



    /*! \brief Copy constructor.
     *  \param[in] other Other triangle to copy.
     */
    template <class SpectrumType>
    SmoothMeshTriangle<SpectrumType>::SmoothMeshTriangle(const SmoothMeshTriangle<SpectrumType>& other)
        : MeshTriangle<SpectrumType>(other)
    {
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    SmoothMeshTriangle<SpectrumType>::~SmoothMeshTriangle()
    {
    }



    /*! \brief Clone (make deep copy of) object.
     *  \return Returned cloned object.
     */
    template <class SpectrumType>
    SmoothMeshTriangle<SpectrumType>* SmoothMeshTriangle<SpectrumType>::clone() const
    {
        return new SmoothMeshTriangle<SpectrumType>(*this);
    }



    /*! \brief Intersects object with given ray.
     *  \param[in] ray Ray for intersection.
     *  \param[out] tmin Ray-parameter of hit point.
     *  \param[out] shadingData Shading data structure of hit point.
     *  \return Returns TRUE, if ray and object intersect, else FLASE.
     */
    template <class SpectrumType>
    bool SmoothMeshTriangle<SpectrumType>::hit(const Ray& ray, double& tmin, ShadingData<SpectrumType>& shadingData) const
    {
        std::shared_ptr<Mesh> mesh(this->mesh_.lock());
        Point3D vertex0(mesh->vertices_[this->vertexIndices_[0]]);
        Point3D vertex1(mesh->vertices_[this->vertexIndices_[1]]);
        Point3D vertex2(mesh->vertices_[this->vertexIndices_[2]]);
        mesh.reset();
        
        double a = vertex0.x_ - vertex1.x_;
        double b = vertex0.x_ - vertex2.x_;
        double c = ray.direction_.x_;
        double d = vertex0.x_ - ray.origin_.x_;
        double e = vertex0.y_ - vertex1.y_;
        double f = vertex0.y_ - vertex2.y_;
        double g = ray.direction_.y_;
        double h = vertex0.y_ - ray.origin_.y_;
        double i = vertex0.z_ - vertex1.z_;
        double j = vertex0.z_ - vertex2.z_;
        double k = ray.direction_.z_;
        double l = vertex0.z_ - ray.origin_.z_;
        
        double m = f * k - g * j;
        double n = h * k - g * l;
        double p = f * l - h * j;
        double q = g * i - e * k;
        double s = e * j - f * i;
        
        double invDenom = 1.0 / (a * m + b * q + c * s);
        
        double e1 = d * m - b * n - c * p;
        double beta = e1 * invDenom;
        if (beta < 0.0)
        {
            return false;
        }
        
        double r = e * l - h * i;
        double e2 = a * n + d * q + c * r;
        double gamma = e2 * invDenom;
        if (gamma < 0.0)
        {
            return false;
        }
        
        if ((beta + gamma) > 1.0)
        {
            return false;
        }
        
        double e3 = a * p - b * r + d * s;
        double t = e3 * invDenom;
        
        if (t < EPS)
        {
            return false;
        }
        
        tmin = t;
        shadingData.hitAnObject_ = true;
        shadingData.normal_ = interpolateNormal(beta, gamma);
        shadingData.hitPoint_ = ray.origin_ + t * ray.direction_;
        shadingData.ray_ = ray;
        shadingData.material_ = this->objectMaterial_;
        
        // Define local hitpoint relative to minimum x, y, z coordinate of the bounding box
        double x0 = this->getBoundingBox().x0_;
        double y0 = this->getBoundingBox().y0_;
        double z0 = this->getBoundingBox().z0_;
        shadingData.localHitPoint3D_ = shadingData.hitPoint_; // - Point3D(x0, y0, z0);
        
        return true;
    }



    /*! \brief Intersects object with given shadow ray.
     *  \param[in] shadowRay Ray for intersection.
     *  \param[out] tmin Ray-parameter of hit point.
     *  \return Returns TRUE, if ray and object intersect, else FLASE.
     */
    template <class SpectrumType>
    bool SmoothMeshTriangle<SpectrumType>::shadowHit(const Ray& shadowRay, double& tmin) const
    {
        if (!this->castsShadows_)
        {
            return false;
        }

        std::shared_ptr<Mesh> mesh(this->mesh_.lock());
        Point3D vertex0(mesh->vertices_[this->vertexIndices_[0]]);
        Point3D vertex1(mesh->vertices_[this->vertexIndices_[1]]);
        Point3D vertex2(mesh->vertices_[this->vertexIndices_[2]]);
        mesh.reset();
        
        double a = vertex0.x_ - vertex1.x_;
        double b = vertex0.x_ - vertex2.x_;
        double c = shadowRay.direction_.x_;
        double d = vertex0.x_ - shadowRay.origin_.x_;
        double e = vertex0.y_ - vertex1.y_;
        double f = vertex0.y_ - vertex2.y_;
        double g = shadowRay.direction_.y_;
        double h = vertex0.y_ - shadowRay.origin_.y_;
        double i = vertex0.z_ - vertex1.z_;
        double j = vertex0.z_ - vertex2.z_;
        double k = shadowRay.direction_.z_;
        double l = vertex0.z_ - shadowRay.origin_.z_;
        
        double m = f * k - g * j;
        double n = h * k - g * l;
        double p = f * l - h * j;
        double q = g * i - e * k;
        double s = e * j - f * i;
        
        double invDenom = 1.0 / (a * m + b * q + c * s);
        
        double e1 = d * m - b * n - c * p;
        double beta = e1 * invDenom;
        if (beta < 0.0)
        {
            return false;
        }
        
        double r = e * l - h * i;
        double e2 = a * n + d * q + c * r;
        double gamma = e2 * invDenom;
        if (gamma < 0.0)
        {
            return false;
        }
        
        if ((beta + gamma) > 1.0)
        {
            return false;
        }
        
        double e3 = a * p - b * r + d * s;
        double t = e3 * invDenom;
        
        if (t < EPS)
        {
            return false;
        }
        
        tmin = t;
        return true;
    }



    /*! \brief Interpolate surface normal using normals at vertices.
     *  \param[in] beta Triangle coordinate of hit point.
     *  \param[in] gamma Triangle coordinate of hit point.
     *  \return Resulting Normal.
     */
    template <class SpectrumType>
    Normal SmoothMeshTriangle<SpectrumType>::interpolateNormal(double beta, double gamma) const
    {
        return (1 - beta - gamma) * this->mesh_.lock()->normals_[this->vertexIndices_[0]] +
            beta * this->mesh_.lock()->normals_[this->vertexIndices_[1]] +
            gamma * this->mesh_.lock()->normals_[this->vertexIndices_[2]];
    }

} // end of namespace iiit

#endif // __SMOOTHMESHTRIANGLE_HPP__
