// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file CircularAperture.hpp
 *  \author Thomas Nuernberg
 *  \date 22.11.2017
 *  \brief Implementation of basic circular aperture.
 */

#include "CircularAperture.hpp"



namespace iiit
{

    /*! \brief Default constructor.
     *  \param[in] zPosition Z position of aperture.
     *  \param[in] radius Radius of circular aperture.
     */
    CircularAperture::CircularAperture(float zPosition, float radius)
        : Aperture(zPosition)
        , radius_(radius)
    {
    }



    /*! \brief Checks the attenuation of a ray.
     *  \param[in] ray Ray to attenuate.
     *  \return Returns attenuation factor between 0 and 1.
     */
    float CircularAperture::attenuateRay(const Ray& ray) const
    {
        Point2D localHitPoint = calcHitPoint(ray);
        if (localHitPoint.norm() >= radius_)
        {
            return 0.f;
        }
        else
        {
            return 1.f;
        }

    }

} // end of namespace iiit
