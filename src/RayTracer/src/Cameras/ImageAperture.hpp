// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file ImageAperture.hpp
 *  \author Mohamed Salem Koubaa
 *  \date 26.04.2015
 *  \author Christian Zimmermann
 *  \date 05.08.2015
 *  \author Sandro Braun
 *  \date 01.08.2016
 *  \brief Definition of an image based aperture.
 *
 *  Definition of an aperture based on a pixel image.
 */

#ifndef __IMAGEAPERTURE_HPP__
#define __IMAGEAPERTURE_HPP__



#include <memory>

#include "CImg.h"

#include "Cameras/Aperture.hpp"
#include "Utilities/Ray.hpp"



namespace iiit
{

    /*! \brief Enumeration indicating the type of the mask attenuation calculation.
     */
    enum AttenuationFunction {
        INTERNAL, ///< Mask attenuation gets calculated internally.
        EXTERNAL ///< Mask attenuation gets calculated externally.
    };

    /*! \brief Enumeration indicating the scaling of the aperture mask.
     */
    enum ApertureScaling {
        ApertureInsideLens, ///< Aperture is scaled to completely fit into the lens.
        ApertureOutsideLens, ///< Aperture is scaled to completely contain the lens.
        CustomPixelSize ///< Physical size of mask pixels is specified explicitly.
    };

    /*! \brief Enumeration indicating the type of the aperture mask
     */
    enum ApertureType {
        Circular,           ///< circular Aperture
        CircularRing,       ///< circular Ring
        Levin,
        Rectangular,        ///< rectangular Aperture. Square is also possible
        CustomImage         ///< custom Image with provided path
    };

    /*! \brief Class defines the aperture of a ApertureCamera.
     *
     *  This class is used to check whether a ray passes the aperture or not. The mask is defined using
     *  uv-coordinates which relate to the camera frame as shown in the image below. The `mask_` is
     *  describing the shape of the aperture as an inscribing rectangular grid of the lens diameter
     *  aligned to the camera coordinate axes x and y.
     *  \image html pixel_frame.png
     *
     *  Mai 2016:
     * 
     */
    class ImageAperture : public Aperture
    {
    public:
        ImageAperture(float zPosition, float lensRadius);
        float attenuateRay(const Ray& ray) const;

        // aperture type setters
        bool setCircularAperture(int width, int height, int radius);
        bool setCircularRingAperture(int width, int height, int innerRadius, int outerRadius);
        bool setRectangularAperture(int width, int height, int rectHalfWidth, int rectHalfHeight);
        bool setRectangularAperture(int width, int height);
        bool setLevinAperture();
        bool setCustomImageAperture(std::string file);

        // geometric shifts and transforms setters
        void setApertureTranslation(int x, int y);
        void setInPlaneRotation(double zRotationAngle);

        // aperture scaling setters
        void setApertureInsideLens();
        void setApertureOutsideLens();
        void setCustomPixelSize(double desiredPixelWidth);

        // other options setters
        void setPeriodic(int numberOfPatterns_u, int numberOfPatterns_v);

        // other functions
        void setExternalAttenuationFunction(bool useExternalAttenuationFunction);
        void setPixelAspectRatio(float ratio);

        // getters
        int getMaskHeight() const;
        int getMaskWidth() const;
        float getOpacity() const;

        //void saveMask() const;

    private:
        void defineLevinAperture(std::shared_ptr<cimg_library::CImg<float> > mask);

        std::shared_ptr<cimg_library::CImg<float> > mask_; ///< Pixel image approximating the aperture shape.
        int widthAperturePx_; ///< width of aperture IN PIXELS.
        int heightAperturePx_; ///< height of aperture IN PIXELS.
        double pixelWidth_; ///< Physical WIDTH of pixels in meters.
        float pixelAspectRatio; ///< Aspect ratio of physical aperture pixels. This values is determined by width divided by height (u over v)
        float lensRadius_; ///< Radius of lens in meters.

        // if your mask is to complex or wide, use this
        bool useExternalAttenuationFunction_; ///< Flag indicating whether the attenuation function is calculated externally. \deprecated ?

        // parameters for periodic patterns
        bool isPeriodicPatternd_; ///< Flag indicating that the mask consists of a periodically repeated pattern.
        int numberOfUPatterns_; ///< Number of repetitions of the pattern in u dimension.
        int numberOfVPatterns_; ///< Number of repetitions of the pattern in v dimension.

        // special parameters for hitpoint calculation if mask is periodic
        double periodMaskHalfWidth_; ///< HALF OF the width of the mask in m. Used if mask is periodic to see if hitpoint is still on mask.
        double periodMaskHalfHeight_;  ///< HALF OF the height of the mask in m. Used if mask is periodic to see if hitpoint is still on mask.

        // enumerations describing the aperture
        ApertureScaling apertureScalingMethod_; ///< Enumeration describing the scaling method of the aperture.
        ApertureType apertureType_; ///< Enumeration describing the type of the aperture.
        AttenuationFunction attenuationFunction_; ///< Enumeration describing the attenuation Function. For the moment its just internal.

        // Rotational parameters
        bool maskIsRotated_; ///< Boolean indicating weather the mask is rotated around the z-axis of the camera coordinate frame.
        double inPlaneRotCos_; ///< Rotation around z axis - cosine value of angle. Precalculation saves times.
        double inPlaneRotSin_; ///< Rotation around z axis - sine value of angle. Precalculation saves times.

        // coordinate transformation functions
        void calcTransformation();
        void calcTransformationApertureInsideLens();
        void calcTransformationApertureOutsideLens();
        void calcTransformationCustomPixelSize();

        // coordinate transformation parameters
        float uSlope_; ///< Slope parameter of linear camera to aperture pixel u-coordinate conversion.
        float vSlope_; ///< Slope parameter of linear camera to aperture pixel v-coordinate conversion.
        float uOffset_; ///< Offset parameter of linear camera to aperture pixel u-coordinate conversion.
        float vOffset_; ///< Offset parameter of linear camera to aperture pixel v-coordinate conversion.

        // other parameters
        float opacity_; ///< Average gray level of the aperture. This factor can be used to modify noise level
        void calcOpacity(); ///< calculates the average gray level of the lens. Used to scale image noise
    };



    // inlined member functions

    /*! \brief Get pixel height of aperture mask.
     *  \return Returns pixel height.
     */
    inline int ImageAperture::getMaskHeight() const
    {
        return heightAperturePx_;
    }



    /*! \brief Get pixel width of aperture mask.
     *  \return Returns pixel width.
     */
    inline int ImageAperture::getMaskWidth() const
    {
        return widthAperturePx_;
    }



    /*! \brief Calculate average gray level of aperture as float between 0 and 1.
     *  \return Returns average gray level as float between 0 and 1
     */
    inline float ImageAperture::getOpacity() const
    {
        return opacity_;
    }

} // end of namespace iiit

#endif // __IMAGEAPERTURE_HPP__