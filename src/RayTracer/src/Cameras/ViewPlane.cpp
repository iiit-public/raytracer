// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file ViewPlane.cpp
 *  \author Thomas Nuernberg
 *  \date 16.12.2014
 *  \brief Implementation of class ViewPlane.
 *
 *  Implementation of class ViewPlane, which holds the parameters of the image plane.
 */

#include "ViewPlane.hpp"

#include "Sampler/Regular.hpp"
#include "Sampler/Jittered.hpp"



namespace iiit
{

    /*! \brief Default constructor
     */
    ViewPlane::ViewPlane()
        : uOffset_(0)
        , vOffset_(0)
    {
    }



    /*! \brief Default destructor.
     */
    ViewPlane::~ViewPlane()
    {
    }



    /*! \brief Copy constructor.
     *  \param[in] other ViewPlane to copy.
     *
     *  Makes a deep copy of the viewplane's sampler object to prevent access of the same sampler object
     *  by different threads. This would pollute the distribution of the random numbers.
     */
    ViewPlane& ViewPlane::operator=(const ViewPlane& other)
    {
        if (this != &other) // prevent self-assignment
        {
            uRes_ = other.uRes_;
            vRes_ = other.vRes_;
            uAbsRes_ = other.uAbsRes_;
            vAbsRes_ = other.vAbsRes_;
            uOffset_ = other.uOffset_;
            vOffset_ = other.vOffset_;
            s_ = other.s_;
            sampler_ = std::shared_ptr<Sampler>(other.sampler_->clone());
        }

        return *this;
    }



    /*! \brief Set number of samples per pixel.
     *  \param[in] n Number of samples.
     *
     *  Uses regular sampling, if the given number of samples is 1. Else a default jittered sampler is
     *  used.
     */
    void ViewPlane::setSamples(int n)
    {
        if (n > 1)
        {
            sampler_ = std::shared_ptr<Sampler>(new Jittered(n));
        }
        else
        {
            sampler_ = std::shared_ptr<Sampler>(new Regular());
        }
        sampler_->generateSamples();
    }

} // end of namespace iiit