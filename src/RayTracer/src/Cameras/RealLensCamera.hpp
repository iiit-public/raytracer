// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file RealLensCamera.hpp
 *  \author Chihab Ben Hamadi
 *  \date 14.03.2016
 *  \brief Definition of class RealLensCamera.
 *
 *  Definition of class RealLensCamera representing a camera with a real lens.
 */

#ifndef __REALLENSCAMERA_HPP__
#define __REALLENSCAMERA_HPP__

#include <memory>
#include <limits>

#include "CImg.h"

#include "Scene.hpp"
#include "Camera.hpp"
#include "Sensor.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Point2D.hpp"
#include "Utilities/Constants.h"
#include "Sampler/Sampler.hpp"
#include "Aberration/RealLens.hpp"



namespace iiit
{

    /*! \brief Class representing camera with a thick lens with circular aperture.
     *
     *  The camera frame is a cartesian coordinate system with the axes relative to the camera defined
     *  as pictured below. The z-axis points in the viewing direction, while the x and y-axes span the
     *  image plane, forming a right-handed system.
     *  \image html camera_frame.png
     *
     *  The world frame is cartesian coordinate system as depicted in the image below. The x and y-axis
     *  span the ground plane and the z-axis points straight upwards to form a right handed system.
     *  \image html world_frame.png
     *
     */
    template <class SpectrumType>
    class RealLensCamera : public Camera<SpectrumType>, public RealLens
    {
    public:
        RealLensCamera(std::weak_ptr<const Scene<SpectrumType> > scene, const Point3D& eye, const Point3D& lookAt, const Vector3D& up,
            const Sensor<SpectrumType>& sensor, float lensRadius, float imageDistance, double radius1, double radius2, double lensThickness,
            double refractionIndex);
        RealLensCamera(const RealLensCamera& other);
        ~RealLensCamera();

        Camera<SpectrumType>* clone() const;
        virtual void renderScene(std::shared_ptr<cimg_library::CImg<float> > image);
        CameraType::Type getType() const;

        float getLensRadius() const;
        float getImageDistance() const;
        float getZoom() const;

        void setSampler(std::shared_ptr<Sampler> sampler);
        std::shared_ptr<const Sampler> getSampler() const;

    protected:
        Vector3D rayDirection(const Point3D& pixelPoint, const Point3D& lensPoint) const;
        Point2D hitXYPlaneAt(double z, const Ray& ray) const;

        float lensRadius_; ///< Radius of lens in meters.
        float imageDistance_; ///< Distance between lens and image sensor in meters.
        float zoom_; ///< zoom factor
        std::shared_ptr<Sampler> sampler_; ///< Sampling object for sampling the lens.
    };



    /*! \brief Constructs camera and initializes coordinate system with look-at-transformation.
     *  \param[in] scene Scene to render.
     *  \param[in] eye Point in world coordinates defining the origin of the camera coordinate system.
     *  \param[in] lookAt Point in world coordinates that the camera looks at.
     *  \param[in] up Vector in world coordinates pointing in the up direction of the camera.
     *  \param[in] sensor Sensor object.
     *  \param[in] lensRadius Radius of lens.
     *  \param[in] imageDistance Distance between lens and sensor.
     *  \param[in] radius1 is the radius of curvature of the lens surface closest to the light source.
     *  \param[in] radius2 is the radius of curvature of the lens surface farthest from the light source.
     *  \param[in] lensThickness the thickness of the lens.
     *  \param[in] refractionIndex Refraction index of lens material.
     */
    template <class SpectrumType>
    RealLensCamera<SpectrumType>::RealLensCamera(std::weak_ptr<const Scene<SpectrumType> > scene, const Point3D& eye, const Point3D& lookAt, const Vector3D& up,
        const Sensor<SpectrumType>& sensor, float lensRadius, float imageDistance, double radius1, double radius2, double lensThickness, double refractionIndex)
        : Camera<SpectrumType>(scene, eye, lookAt, up, sensor)
        , RealLens(radius1,  radius2, lensRadius,  lensThickness, refractionIndex)
        , lensRadius_(lensRadius)
        , imageDistance_(imageDistance)
    {
        zoom_ = 1.0; ///< \todo Implement zoom interface
    }



    /*! \brief Copy constructor.
     *  \param[in] other Camera to copy.
     */
    template <class SpectrumType>
    RealLensCamera<SpectrumType>::RealLensCamera(const RealLensCamera& other)
        : Camera<SpectrumType>(other)
        , RealLens(other)
        , lensRadius_(other.lensRadius_)
        , imageDistance_(other.imageDistance_)
    {
        this->setSampler(std::shared_ptr<Sampler>(other.sampler_->clone()));
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    RealLensCamera<SpectrumType>::~RealLensCamera()
    {
    }



    /*! \brief Clone this camera.
     *  \return Cloned camera.
     */
    template <class SpectrumType>
    Camera<SpectrumType>* RealLensCamera<SpectrumType>::clone() const
    {
        RealLensCamera<SpectrumType>* camera = new RealLensCamera<SpectrumType>(*this);
        return camera;
    }



    /*! \brief Sets sampling object.
     *  \param[in] sampler Sampling object used for sampling the lens.
     */
    template <class SpectrumType>
    void RealLensCamera<SpectrumType>::setSampler(std::shared_ptr<Sampler> sampler)
    {
        sampler_ = sampler;
    }



    /*! \brief  Compute direction of ray between 2 points in 3D.
     *  \param[in] startPoint 3D starting point.
     *  \param[in] endPoint 3D end point.
     *  \return Ray direction as Vector3D
     */
    template <class SpectrumType>
    Vector3D RealLensCamera<SpectrumType>::rayDirection(const Point3D& startPoint, const Point3D& endPoint) const
    {
        return Vector3D(endPoint.x_ - startPoint.x_,
            endPoint.y_ - startPoint.y_,
            endPoint.z_ - startPoint.z_);
    }



    /*! \brief Renders the scene as seen by the camera.
     *  \param[out] image Rendered image.
     */
    template <class SpectrumType>
    void RealLensCamera<SpectrumType>::renderScene(std::shared_ptr<cimg_library::CImg<float> > image)
    {
        //// evaluate zoom parameter
        //scene.viewPlane_.s_ /= zoom_;
        
        std::shared_ptr<const Scene<SpectrumType> > scene = this->scene_.lock();
        
        //iterate over pixels
        for (int uAbs = scene->viewPlane_.uAbsBegin(); uAbs < scene->viewPlane_.uAbsEnd() ; ++uAbs)
        {
            for (int vAbs = scene->viewPlane_.vAbsBegin(); vAbs < scene->viewPlane_.vAbsEnd() ; ++vAbs)
            {
                // reset sampling framework to start a new pixel
                scene->getTracer().lock()->startNewPixel();

                // set initial pixel color black
                SpectrumType color(0.f);
                
                // sample pixel
                for (int n = 0; n < scene->viewPlane_.sampler_->getNumSamples(); ++n)
                {
                    // compute pixel point in camera coordinates (u, v -> x_c, y_c)
                    Point2D pixelPoint = scene->viewPlane_.sampler_->sampleUnitSquare();
                    pixelPoint.x_ = -1.f * scene->viewPlane_.uSampleToXc(uAbs + pixelPoint.x_);
                    pixelPoint.y_ = -1.f * scene->viewPlane_.vSampleToYc(vAbs + pixelPoint.y_);
                    
                    // compute lens point in camera coordinates
                    Point2D lensPoint = sampler_->sampleUnitDisk();
                    lensPoint = lensPoint * (lensRadius_);

                    // compute image side ray
                    Point3D origin(pixelPoint.x_, pixelPoint.y_, -1.0 * imageDistance_);
                    Vector3D direction(Point3D(lensPoint.x_, lensPoint.y_, 0.0) - origin);
                    direction.normalize();
                    Ray originalRay(origin, direction);

                    // check if ray passes the image side apertures
                    float attenuation = this->apertureAttenuation(originalRay, RayDomain::ImageSide);

                    // Calculate natural vignetting
                    float vignetting = this->naturalVignetting(originalRay);

                    // the attenuation describes how the ray's intensity is modified (multiplicatively). Therefore, if the
                    // attenuation IS NOT 0.0, the ray "passes" through the aperture
                    if (attenuation != 0.f)
                    {
                        // compute real lens refraction
                        Ray objectSideRay;
                        if (calcRefraction(originalRay, objectSideRay))
                        {
                            // check if ray passes the object side apertures
                            attenuation = attenuation * this->apertureAttenuation(objectSideRay, RayDomain::ObjectSide);

                            if (attenuation != 0.f)
                            {
                                // transform primary ray to world coordinates
                                objectSideRay = this->transformation_.camToWorld(objectSideRay);//ray goes positively into the world

                                // calculate interaction of ray with the scene
                                color += scene->getTracer().lock()->traceRay(objectSideRay, 0) * attenuation * vignetting;
                            }
                        }
                    }
                }
                
                // compute mean pixel color
                color /=  static_cast<float>(scene->viewPlane_.sampler_->getNumSamples());
                this->setPixel(scene->viewPlane_.uRel(uAbs), scene->viewPlane_.vRel(vAbs), color, image);
            }
        }
    }



    /*! \brief This function calculates the intersection point of a given ray with a XY Plane at
     *  Position z.
     *  \param[in] z z-Coordinate of XY plane that shall be hit.
     *  \param[in] ray Ray that shall intersect the XY Plane.
     */
    template <class SpectrumType>
    Point2D RealLensCamera<SpectrumType>::hitXYPlaneAt(double z, const Ray& ray) const
    {
        Point2D hitPoint(ray.origin_.x_ + (ray.direction_.x_ * z) / ray.direction_.z_,
                         ray.origin_.y_ + (ray.direction_.y_ * z) / ray.direction_.z_);
        return hitPoint;
    }



    /*! \brief Get the type of the camera.
     *  \return Returns the type of the camera.
     */
    template <class SpectrumType>
    CameraType::Type RealLensCamera<SpectrumType>::getType() const
    {
        return CameraType::Type::RealLensCamera;
    }



    /*! \brief Get lens radius.
     *  \return Lens radius in meters.
     */
    template <class SpectrumType>
    float RealLensCamera<SpectrumType>::getLensRadius() const
    {
        return lensRadius_;
    }



    /*! \brief Get image distance.
     *  \return Distance between lens and image sensor in meters.
     */
    template <class SpectrumType>
    float RealLensCamera<SpectrumType>::getImageDistance() const
    {
        return imageDistance_;
    }


    
    /*! \brief Get object distance.
     *  \return Distance between lens and point in perfect focus in meters.
     */
    template <class SpectrumType>
    float RealLensCamera<SpectrumType>::getZoom() const
    {
        return zoom_;
    }



    /*! \brief Get sampling object.
     *  \return Pointer to sampling object.
     */
    template <class SpectrumType>
    std::shared_ptr<const Sampler> RealLensCamera<SpectrumType>::getSampler() const
    {
        return std::shared_ptr<const Sampler>(sampler_);
    }

} // end of namespace iiit

#endif // __REALLENSCAMERA_HPP__
