// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file LightFieldCameraCalibration.hpp
 *  \author Maximilian Schambach
 *  \date 01.03.2019
 *  \brief Definition of class LightFieldCameraCalibration.
 *
 *  Definition of class LightFieldCameraCalibration representing a lenslet based light field camera used for calibration.
 */

#ifndef __LIGHTFIELDCAMERACALIBRATION_HPP__
#define __LIGHTFIELDCAMERACALIBRATION_HPP__

#include <memory>
#include <limits>

#include "CImg.h"

#include "Scene.hpp"
#include "Camera.hpp"
#include "Sensor.hpp"
#include "Utilities/AbstractGrid.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"
#include "Sampler/Sampler.hpp"
#include "Spectrum/CoefficientSpectrum.hpp"



namespace iiit
{
    /*! \brief Class representing a micro lens array based light field camera in the focused design.

     *  The camera frame is a cartesian coordinate system with the axes relative to the camera defined
     *  as pictured below. The z-axis points in the viewing direction, while the x and y-axes span the
     *  image plane, forming a right-handed system.
     *  \image html camera_frame.png
     *
     *  The world frame is cartesian coordinate system as depicted in the image below. The x and y-axis
     *  span the ground plane and the z-axis points straight upwards to form a right handed system.
     *  \image html world_frame.png
     *
     *  Current Implementation: 
     *  -# Sample Microlenses and main lens with individual sampler objects.
     *  -# set focalLength of ML according to Equation \f$ f_{ML} = \frac{f_{main}}{D_{main}} \cdot D_{ML} \f$. D is the diameter and f the focal Length
     *  of the main lens or microlens.
     *  -# Image Distance key equals distance from main lens to MicroLens array.
     *  -# actual view plane is positioned at `imageDistance` + \f$ f_{ML} \f$ behind the main lens.
     *  -# for every main lens point, do
     *      -# for every ML point, do
     *           -# calculate hitPoint with sensor
     *           -# calculate world direction
     *           -# save hitpoint color in temporary image
     *           -# increment ray contribution
     *  -# calculate mean picture from saved contribution and color
     */
    template <class SpectrumType>
    class LightFieldCameraCalibration : public Camera<SpectrumType>
    {
    public:
        LightFieldCameraCalibration(std::weak_ptr<const Scene<SpectrumType> > scene,
                                    const Point3D& eye,
                                    const Point3D& lookAt,
                                    const Vector3D& up,
                                    const Sensor<SpectrumType>& sensor,
                                    float lensRadius,
                                    float imageDistance,
                                    float focusDistance,
                                    float microLensRadius,
                                    std::shared_ptr<AbstractGrid<SpectrumType> > microLensArray);
        ~LightFieldCameraCalibration();

        Camera<SpectrumType>* clone() const;
        virtual void renderScene(std::shared_ptr<cimg_library::CImg<float> > image);
        CameraType::Type getType() const;

        float getLensRadius() const;
        float getImageDistance() const;
        float getFocusDistance() const;
        float getZoom() const;

        void setSampler(std::shared_ptr<Sampler> sampler);
        std::shared_ptr<const Sampler> getSampler() const;

    protected:
        Vector3D rayDirection(const Point2D& pixelPoint, const Point2D& lensPoint) const;

        float lensRadius_; ///< Radius of lens in meters.
        float imageDistance_; ///< Distance between lens and micro lens array in meters.
        float focusDistance_; ///< Distance between lens and point in perfect focus in meters.
        float zoom_; ///< zoom factor
        std::shared_ptr<Sampler> sampler_; ///< Sampling object for sampling the lens.

        std::shared_ptr<AbstractGrid<SpectrumType> > microLensArray_; ///< Grid defining the coordinates of micro lenses
        float microLensRadius_; ///< Radius of micro lenses in meters.
        std::shared_ptr<Sampler> microLensSampler_; ///< Sampling object for sampling the micro lenses.
    };



    /*! \brief Constructs camera and initializes coordinate system with look-at-transformation.
     *  \param[in] scene Scene to render.
     *  \param[in] eye Point in world coordinates defining the origin of the camera coordinate system.
     *  \param[in] lookAt Point in world coordinates that the camera looks at.
     *  \param[in] up Vector in world coordinates pointing in the up direction of the camera.
     *  \param[in] sensor Sensor object.
     *  \param[in] lensRadius Radius of lens.
     *  \param[in] imageDistance Distance between micro lens array and sensor.
     *  \param[in] focusDistance Distance of point in focus.
     *  \param[in] microLensRadius Radius of micro lenses.
     *  \param[in] microLensArray Grid defining the micro lens array.
     *
     */
    template <class SpectrumType>
    LightFieldCameraCalibration<SpectrumType>::LightFieldCameraCalibration(
            std::weak_ptr<const Scene<SpectrumType> > scene,
            const Point3D& eye,
            const Point3D& lookAt,
            const Vector3D& up,
            const Sensor<SpectrumType>& sensor,
            float lensRadius, float imageDistance, float focusDistance, float microLensRadius, std::shared_ptr<AbstractGrid<SpectrumType> > microLensArray)
        : Camera<SpectrumType>(scene, eye, lookAt, up, sensor)
        , lensRadius_(lensRadius)
        , imageDistance_(imageDistance)
        , focusDistance_(focusDistance)
        , microLensRadius_(microLensRadius)
        , microLensArray_(microLensArray)
    {
        zoom_ = 1.0; ///< \todo Implement zoom interface
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    LightFieldCameraCalibration<SpectrumType>::~LightFieldCameraCalibration()
    {
    }



    /*! \brief Clone this camera.
     *  \return Cloned camera.
     */
    template <class SpectrumType>
    Camera<SpectrumType>* LightFieldCameraCalibration<SpectrumType>::clone() const
    {
        LightFieldCameraCalibration<SpectrumType>* camera = new LightFieldCameraCalibration<SpectrumType>(*this);
        return camera;
    }



    /*! \brief Sets sampling object.
     *  \param[in] sampler Sampling object used for sampling the lens.
     */
    template <class SpectrumType>
    void LightFieldCameraCalibration<SpectrumType>::setSampler(std::shared_ptr<Sampler> sampler)
    {
        sampler_ = sampler;
    }



    /*! \brief Compute direction ray that starts at the (thin) lens and gets imaged on a specific  point.
     *  \param[in] pixelPoint Point that ray is imaged on.
     *  \param[in] lensPoint Point on lens.
     *  \return Ray direction.
     */
    template <class SpectrumType>
    Vector3D LightFieldCameraCalibration<SpectrumType>::rayDirection(const Point2D& pixelPoint, const Point2D& lensPoint) const
    {
        Point2D focalPoint; // Point on the focal plane
        focalPoint.x_ = -pixelPoint.x_ * (focusDistance_ / imageDistance_);
        focalPoint.y_ = -pixelPoint.y_ * (focusDistance_ / imageDistance_);
        
        Vector3D dir(focalPoint.x_ - lensPoint.x_, focalPoint.y_ - lensPoint.y_ , focusDistance_);
        dir.normalize();
        
        return dir;
    }



    /*! \brief Renders the scene as seen by the camera.
     *  \param[out] image Rendered image.
     */
    template <class SpectrumType>
    void LightFieldCameraCalibration<SpectrumType>::renderScene(std::shared_ptr<cimg_library::CImg<float> > image)
    {
        std::shared_ptr<const Scene<SpectrumType> > scene = this->scene_.lock();

        // temporary images
        SpectrumType tmpColor;
        std::shared_ptr<cimg_library::CImg<float> > tmpImage(new cimg_library::CImg<float>(image->width(), image->height(), 1, tmpColor.getNumOfCoeffs() ));
        std::shared_ptr<cimg_library::CImg<unsigned int> > counterImage(new cimg_library::CImg<unsigned int>(image->width(), image->height(), 1, 1));
        tmpImage->fill(0);
        counterImage->fill(0);
        
        // parameters
        double lensFocalLength = 1.0 / (1.0 / imageDistance_ + 1.0 / focusDistance_);
        double fNumber = lensFocalLength / 2.0 / lensRadius_;

        // f-number matching (choose a little smaller, no no exact overlap occurs)
        double microLensFocalLength = 0.975*fNumber * 2.0 * microLensRadius_;

        // iterate over micro lenses
        int num_lenses = microLensArray_->get_num_points();
        for (int i = 0; i < num_lenses; i++) {

            // sample micro lens
            for (int n = 0; n < scene->viewPlane_.sampler_->getNumSamples(); ++n)
            {
                // compute point on micro lens in local lens coordinates
                Point2D microLensDiskPoint = scene->viewPlane_.sampler_->sampleUnitDisk();
                Point3D localMicroLensPoint(microLensDiskPoint.x_ * microLensRadius_, microLensDiskPoint.y_ * microLensRadius_, 0);

                // get current micro lens point and sample disc
                Point3D microLensPoint = microLensArray_->get_point(i) + localMicroLensPoint; // this is not correct if MLA is tilted...


                // sample main lens
                for (int k = 0; k < sampler_->getNumSamples(); k++)
                {

                    // compute point on main lens in camera coordinates
                    Point2D lensDiscPoint = sampler_->sampleUnitDisk();
                    Point3D lensPoint(lensDiscPoint * lensRadius_, 0.0);

                    // compute image side ray, from microLensPoint to lensPoint
                    Ray imageSideRay(microLensPoint, lensPoint - microLensPoint);

                    // check if ray passes the image side apertures
                    float attenuation = this->apertureAttenuation(imageSideRay, RayDomain::ImageSide);

                    // the attenuation describes how the ray's intensity is modified (multiplicatively). Therefore, if the
                    // attenuation IS NOT 0.0, the ray "passes" through the aperture
                    if (attenuation != 0.f)
                    {
                        // construct primary (object side) ray,
                        // that gets imaged on the micro lens point using thin lens eq
                        Ray sceneRay(lensPoint, rayDirection(Point2D(microLensPoint.x_, microLensPoint.y_),
                                                             Point2D(lensPoint.x_, lensPoint.y_)));

                        // check if ray passes the object side apertures
                        attenuation = attenuation * this->apertureAttenuation(sceneRay, RayDomain::ObjectSide);

                        if (attenuation != 0.f)
                        {
                            // init empty color, no tracing is performed
                            SpectrumType color;

                            // calculate refraction of the micro lens
                            // Using Thin Lens Matrix Transfer Method
                            Vector3D cameraRayDirection = microLensPoint - lensPoint;
                            cameraRayDirection.normalize();

                            Vector3D radialUnitVector = localMicroLensPoint;
                            radialUnitVector.normalize();
                            Vector3D normalUnitVector(0.0, 0.0, 1.0);

                            Vector3D radialRayComponent = radialUnitVector * (cameraRayDirection * radialUnitVector);
                            Vector3D tangentialRayComponent(cameraRayDirection.x_ - radialRayComponent.x_, cameraRayDirection.y_ - radialRayComponent.y_, 0.0);
                            Vector3D normalRayComponent = normalUnitVector * (cameraRayDirection * normalUnitVector);

                            double psi = sqrt(localMicroLensPoint.x_ * localMicroLensPoint.x_ + localMicroLensPoint.y_ * localMicroLensPoint.y_) / microLensFocalLength;
                            if ((localMicroLensPoint * radialRayComponent) < 0.0)
                            {
                                psi *= -1.0;
                            }

                            double radialLength = radialRayComponent.length();
                            double normalLength = normalRayComponent.length();
                            double newRadialLength = cos(psi) * radialLength - sin(psi) * normalLength;
                            double newNormalLength = sin(psi) * radialLength + cos(psi) * normalLength;
                            Vector3D newCameraRayDirection = radialRayComponent / radialLength * newRadialLength +
                                normalRayComponent / normalLength * newNormalLength + tangentialRayComponent;

                            Ray cameraRay(microLensPoint, newCameraRayDirection);

                            // calculate pixel that gets hit by camera ray
                            double tSensor = microLensFocalLength / -newCameraRayDirection.z_;
                            Point3D sensorHitPoint = cameraRay.origin_ + tSensor * cameraRay.direction_;

                            double tmp_u = sensorHitPoint.x_ / scene->viewPlane_.s_ + 0.5 * scene->viewPlane_.uAbsRes_;
                            double tmp_v = sensorHitPoint.y_ / scene->viewPlane_.s_ + 0.5 * scene->viewPlane_.vAbsRes_;

                            // calculate natural vignetting
                            float vignetting = this->naturalVignetting(imageSideRay);

                            if (tmp_u >= 0 && tmp_u < scene->viewPlane_.uAbsEnd() && tmp_v >= 0 && tmp_v < scene->viewPlane_.vAbsEnd())
                            {
                                unsigned int u = static_cast<unsigned int>(tmp_u);
                                unsigned int v = static_cast<unsigned int>(tmp_v);

                                // flip image coordinates
                                u = scene->viewPlane_.uAbsRes_ - u - 1;
                                v = scene->viewPlane_.vAbsRes_ - v - 1;

                                // Color code spectrum, if microlens is colorcoded
                                if (microLensArray_->is_coded())
                                {
                                    color = microLensArray_->get_color(i);
                                }
                                else
                                {
                                    color = SpectrumType(1.0);
                                }

                                for (unsigned int k = 0; k < color.getNumOfCoeffs(); ++k)
                                {
                                    // set pixel white in all cases
                                    tmpImage->operator()(u, v, k) += color.getCoeff(k) * attenuation * vignetting;
                                }
                                counterImage->operator()(u, v) += 1;
                            }
                        }
                    }
                } // main lens sampling
            } // micro lens sampling
        } // micro lens iteration

        
        // find maximum counter value for normalization
        float max = 0.0;
        for (unsigned int i = 0; i < counterImage->width(); i++) {
            for (unsigned int j = 0; j < counterImage->height(); j++) {
                    max = max > counterImage->operator()(i,j) ? max : counterImage->operator()(i,j);
            }
        }

        // iterate over pixels
        for (int uAbs = scene->viewPlane_.uAbsBegin(); uAbs < scene->viewPlane_.uAbsEnd(); ++uAbs)
        {
            for (int vAbs = scene->viewPlane_.vAbsBegin(); vAbs < scene->viewPlane_.vAbsEnd(); ++vAbs)
            {
                // compute mean pixel color
                SpectrumType color;
                for (int k = 0; k < color.getNumOfCoeffs(); ++k)
                {
                    color.setCoeff(k, tmpImage->operator()(uAbs, vAbs, k) );
                }

                color /=  max;

                // set pixel value
                this->setPixel(scene->viewPlane_.uRel(uAbs), scene->viewPlane_.vRel(vAbs), color, image);
            }
        }
    }



    /*! \brief Get the type of the camera.
     *  \return Returns the type of the camera.
     */
    template <class SpectrumType>
    CameraType::Type LightFieldCameraCalibration<SpectrumType>::getType() const
    {
        return CameraType::Type::LightFieldCameraCalibration;
    }



    /*! \brief Get lens radius.
     *  \return Lens radius in meters.
     */
    template <class SpectrumType>
    float LightFieldCameraCalibration<SpectrumType>::getLensRadius() const
    {
        return lensRadius_;
    }



    /*! \brief Get image distance.
     *  \return Distance between lens and image sensor in meters.
     */
    template <class SpectrumType>
    float LightFieldCameraCalibration<SpectrumType>::getImageDistance() const
    {
        return imageDistance_;
    }



    /*! \brief Get focus distance.
     *  \return Distance between lens and point in perfect focus in meters.
     */
    template <class SpectrumType>
    float LightFieldCameraCalibration<SpectrumType>::getFocusDistance() const
    {
        return focusDistance_;
    }



    /*! \brief Get object distance.
     *  \return Distance between lens and point in perfect focus in meters.
     */
    template <class SpectrumType>
    float LightFieldCameraCalibration<SpectrumType>::getZoom() const
    {
        return zoom_;
    }



    /*! \brief Get sampling object.
     *  \return Pointer to sampling object.
     */
    template <class SpectrumType>
    std::shared_ptr<const Sampler> LightFieldCameraCalibration<SpectrumType>::getSampler() const
    {
        return std::shared_ptr<const Sampler>(sampler_);
    }

} // end of namespace iiit

#endif // __LIGHTFIELDCAMERACALIBRATION_HPP__
