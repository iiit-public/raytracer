// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Camera.hpp
 *  \author Mohamed Salem Koubaa
 *  \date 16.03.2015
 *  \brief Definition of class Camera.
 *
 *  Definition of class Camera representing a camera as a base class.
 */

#ifndef __CAMERA_HPP__
#define __CAMERA_HPP__

#include <memory>
#include <vector>
#include <array>
#include <algorithm>

#include "CImg.h"

#include "Utilities/Point3D.hpp"
#include "Utilities/Point2D.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/Transformation.hpp"
#include "Aberration/ImageNoise.hpp"
#include "Spectrum/SampledSpectrum.hpp"
#include "Cameras/Sensor.hpp"
#include "Cameras/Aperture.hpp"

namespace iiit
{

    template <class T> class Scene; // forward declaration
    
    
    
    namespace CameraType
    {
        /*! This enum type defines valid camera types. The objects are defined in specialized classes.
         */
        enum Type
        {
            Orthographic, ///< Orthographic camera with parallel projection.
            Pinhole, ///< Pinhole camera.
            ThinLens, ///< Camera with ideal thin lens.
            LightFieldCamera, ///< Light field camera with micro lens array.
            LightFieldCameraCalibration, ///< Light field camera with micro lens array (only used for calibration).
            LightFieldReferenceCamera, ///< Light field reference camera.
            ColorCodedLightFieldCamera, ///< Light field camera with color coded micro lens array.
            RealLensCamera, ///< Camera with real lens.
            ChromaticRealLensCamera, ///< Camera with real lens and chromatic aberration.
        };
    }



    /*! \brief Enumeration indicating whether a ray is on the image side or object side.
     */
    enum RayDomain
    {
        ImageSide, ///< Ray on image side.
        ObjectSide ///< Ray on object side.
    };



    /*! \brief Abstract class defining the camera interface for frontends.
     */
    class AbstractCamera
    {
    public:
        /*! \brief Renders the scene as seen by the camera.
         *  \param[out] image Rendered image.
         */
        virtual void renderScene(std::shared_ptr<cimg_library::CImg<float> > image) = 0;
    };



    /*! \brief Abstract class representing cameras.
     *
     *  This abstract class manages calculations that are in common for all camera types, e. g.
     *  coordinate transformations.
     *
     *  The camera frame is a cartesian coordinate system with the axes relative to the camera defined
     *  as pictured below. The z-axis points in the viewing direction, while the x and y-axes span the
     *  image plane, forming a right-handed system.
     *  \image html camera_frame.png
     *
     *  The world frame is cartesian coordinate system as depicted in the image below. The x and y-axis
     *  span the ground plane and the z-axis points straight upwards to form a right handed system.
     *  \image html world_frame.png
     *
     */
    template <class SpectrumType>
    class Camera : public AbstractCamera
    {
    public:
        Camera(std::weak_ptr<const Scene<SpectrumType> > scene, const Point3D& eye_,
            const Point3D& lookAt_, const Vector3D& up, const Sensor<SpectrumType>& sensor);
        Camera(const Camera& other);
        virtual ~Camera() {};

        /*! \brief Deep copies the camera.
         *  \return Returns the copied camera.
         */
        virtual Camera* clone() const = 0;

        /*! \brief Renders the scene as seen by the camera.
         *  \param[out] image Rendered image.
         */
        virtual void renderScene(std::shared_ptr<cimg_library::CImg<float> > image) = 0;

        /*! \brief Get the type of the camera.
         *  \return Returns the type of the camera.
         */
        virtual CameraType::Type getType() const = 0;

        virtual float getOpacity();
        void initTransformation();
        void setTransformation(const Transformation& transformation);
        void setTransformation(const Point3D& eye, const Point3D& lookAt, const Vector3D& up);
        Transformation getTransformation() const;

        void setScene(std::weak_ptr<const Scene<SpectrumType> > scene);
        void setEye(const Point3D& eye);
        Point3D getEye() const;
        void setLookAt(const Point3D& lookAt);
        Point3D getLookAt() const;
        void setUp(const Vector3D& up);
        Vector3D getUp() const;
        void setExposureTime (float time);
        float getExposureTime() const;
        void setImageNoise(ImageNoise<SpectrumType> imageNoise);
        void setNoise(bool noise);
        Sensor<SpectrumType> getSensor() const;
        void addAperture(std::shared_ptr<const Aperture> aperture);

    protected:
        void setPixel(unsigned int u, unsigned int v, SpectrumType& color, std::weak_ptr<cimg_library::CImg<float> > image);
        float apertureAttenuation(const Ray& ray, RayDomain domain);
        float naturalVignetting(const Ray& ray);

        std::weak_ptr<const Scene<SpectrumType> > scene_; ///< Scene to render.
        
        Point3D eye_; ///< Point in world coordinates defining the origin of the camera coordinate system.
        Point3D lookAt_; ///< Point in world coordinates that the camera looks at.
        Vector3D up_; ///< Vector in world coordinates pointing in the up direction of the camera.
        Sensor<SpectrumType> sensor_; ///< Sensor of the Camera. Incorporates sensorType and imageType.
        Transformation transformation_; ///< Transformation object transforming between world and camera coordinates.
        float exposureTime_; ///< Shutter speed of the camera. \todo Implement exposure time.
        ImageNoise<SpectrumType> imageNoise_; ///< Image noise source object.
        bool addNoise_; ///< Flag indicating whether to add noise to the image.

        std::vector<std::shared_ptr<const Aperture> > apertures_; ///< Vector of apertures of the camera.
    };



    /*! \brief Constructs camera and initializes coordinate system with look-at-transformation.
     *  \param[in] scene Scene to render.
     *  \param[in] eye Point in world coordinates defining the origin of the camera coordinate system.
     *  \param[in] lookAt Point in world coordinates that the camera looks at.
     *  \param[in] up Vector in world coordinates pointing in the up direction of the camera.
     *  \param[in] sensor Sensor object.
     */
    template <class SpectrumType>
    Camera<SpectrumType>::Camera(std::weak_ptr<const Scene<SpectrumType> > scene, const Point3D& eye,
        const Point3D& lookAt, const Vector3D& up, const Sensor<SpectrumType>& sensor)
        : scene_(scene)
        , eye_(eye)
        , lookAt_(lookAt)
        , up_(up)
        , sensor_(sensor)
        , imageNoise_()
        , addNoise_(false)
    {
        initTransformation();
    }



    /*! \brief Copy constructor.
     *  \param[in] other Camera to copy.
     */
    template <class SpectrumType>
    Camera<SpectrumType>::Camera(const Camera& other)
        : scene_(other.scene_)
        , eye_(other.getEye())
        , lookAt_(other.getLookAt())
        , up_(other.getUp())
        , sensor_(other.sensor_)
        , transformation_(other.getTransformation())
        , imageNoise_(other.imageNoise_)
        , addNoise_(other.addNoise_)
        , apertures_(other.apertures_)
    {
    }



    /*! \brief Get opacity of aperture.
     *   \return Returns opacity of aperture.
     */
    template <class SpectrumType>
    inline float Camera<SpectrumType>::getOpacity()
    {
        return 1.f;
    }



    /*! \brief (Re)calculates the transformation matrix according to the look-at-transformation.
     */
    template <class SpectrumType>
    void Camera<SpectrumType>::initTransformation()
    {
        transformation_ = Transformation(eye_, lookAt_, up_);
    }



    /*! \brief Sets the coordinate transformation.
     *  \param[in] transformation New coordinate transformation.
     */
    template <class SpectrumType>
    void Camera<SpectrumType>::setTransformation(const Transformation& transformation)
    {
        transformation_ = transformation;
    }



    /*! \brief (Re)calculates the coordinate transformation with given data.
     *  \param[in] eye Point in world coordinates defining the origin of the camera coordinate system.
     *  \param[in] lookAt Point in world coordinates that the camera looks at.
     *  \param[in] up Vector in world coordinates pointing in the up direction of the camera.
     */
    template <class SpectrumType>
    void Camera<SpectrumType>::setTransformation(const Point3D& eye, const Point3D& lookAt, const Vector3D& up)
    {
        transformation_ = Transformation(eye, lookAt, up);
    }



    /*! \brief Gets the coordinate transformation.
     *  \return Current coordinate transformation.
     */
    template <class SpectrumType>
    Transformation Camera<SpectrumType>::getTransformation() const
    {
        return transformation_;
    }

    
    
    /*! \brief Set scene.
     *  \param[in] scene Scene pointer.
     */
    template <class SpectrumType>
    void Camera<SpectrumType>::setScene(std::weak_ptr<const Scene<SpectrumType> > scene)
    {
        scene_ = scene;
    }
    


    /*! \brief Sets the camera origin.
     *  \param[in] eye Point in world coordinates defining the origin of the camera coordinate system.
     */
    template <class SpectrumType>
    void Camera<SpectrumType>::setEye(const Point3D& eye)
    {
        eye_ = eye;
    }



    /*! \brief Gets the camera origin.
     *  \return Point in world coordinates defining the origin of the camera coordinate system.
     */
    template <class SpectrumType>
    Point3D Camera<SpectrumType>::getEye() const
    {
        return eye_;
    }



    /*! \brief Gets the view point of the camera.
     *  \param[in] lookAt Point in world coordinates that the camera looks at.
     */
    template <class SpectrumType>
    void Camera<SpectrumType>::setLookAt(const Point3D& lookAt)
    {
        lookAt_ = lookAt;
    }



    /*! \brief Gets the camera's up direction.
     *  \return Vector in world coordinates pointing in the up direction of the camera.
     */
    template <class SpectrumType>
    Point3D Camera<SpectrumType>::getLookAt() const
    {
        return lookAt_;
    }



    /*! \brief Sets the camera view point.
     *  \param[in] up Vector in world coordinates pointing in the up direction of the camera.
     */
    template <class Spectrum>
    void Camera<Spectrum>::setUp(const Vector3D& up)
    {
        up_ = up;
    }



    /*! \brief Gets the camera's up direction.
     *  \return Vector in world coordinates pointing in the up direction of the camera.
     */
    template <class SpectrumType>
    Vector3D Camera<SpectrumType>::getUp() const
    {
        return up_;
    }



    /*! \brief Set exposure time.
     *  \param[in] time Exposure time.
     */
    template <class SpectrumType>
    void Camera<SpectrumType>::setExposureTime(float time)
    {
        exposureTime_ = time;
    }



    /*! \brief Get exposure time.
     *  \return Exposure time.
     */
    template <class SpectrumType>
    float Camera<SpectrumType>::getExposureTime() const
    {
        return exposureTime_;
    }



    /*! \brief Set image noise source.
     *  \param[in] imageNoise Image noise source.
     */
    template <class SpectrumType>
    void Camera<SpectrumType>::setImageNoise(ImageNoise<SpectrumType> imageNoise)
    {

        // imageNoise.setGain(imageNoise.getGain() / this->getOpacity()); // disable automatic noise scaling
        imageNoise_ = imageNoise;
        addNoise_ = true;
    }




    /*! \brief Set whether to add image noise source.
     *  \param[in] noise Flag.
     */
    template <class SpectrumType>
    void Camera<SpectrumType>::setNoise(bool noise)
    {
        addNoise_ = noise;
    }



    /*! \brief Get camera sensor.
     *  \return Sensor.
     *  \todo implement to return pointer to improve efficiency
     */
    template <class SpectrumType>
    Sensor<SpectrumType> Camera<SpectrumType>::getSensor() const
    {
        return sensor_;
    }



    /*! \brief Add aperture.
     *  \param[in] aperture Aperture to add.
     *
     *  \note The camera takes ownership of the aperture object.
     */
    template <class SpectrumType>
    void Camera<SpectrumType>::addAperture(std::shared_ptr<const Aperture> aperture)
    {
        apertures_.push_back(aperture);
    }



    /*! \brief Utility function to set pixel of image.
     *  \param[in] u u-coordinate.
     *  \param[in] v v-coordinate.
     *  \param[in] color to set.
     *  \param[in,out] image Image to set.
     *
     *  This function converts the given spectral representation to an RGB color value and handles
     *  out-of-gamut colors by cutting off to large or low values.
     */    
    /*! \todo Write as full template of imageType parameter to get rid of the if else statements*/
    template <class SpectrumType>
    void Camera<SpectrumType>::setPixel(unsigned int u, unsigned int v, SpectrumType& color, std::weak_ptr<cimg_library::CImg<float> > image)
    {
        if (color.isNan())
        {
            std::cout << "Warning: Color contains NaN." << std::endl;
            for (int i = 0; i < color.getNumOfCoeffs(); ++i)
            {
                color.setCoeff(i, 0.f);
            }
        }

        if (addNoise_)
        {
            imageNoise_.addNoise(color);
        }
        
        ImageType::Type imageType = this->getSensor().getImageType();
        Sensor<SpectrumType> tmpSensor = this->getSensor();

        if (imageType == ImageType::Type::Raw)
        {
            float raw = tmpSensor.spectrumToSensorRaw(color);

            // Clip values to 0...1 if tracer traces images and not properties light depth or surface
            if (!(scene_.lock()->getTracer().lock()->tracesProperty()))
            {
                raw = std::min<float>(1.f, std::max<float>(0.f, raw));
            }

            image.lock()->operator()(u, v, 0) = raw;
        }
        else if (imageType == ImageType::Type::Rgb)
        {
            std::array<float, 3> rgb = tmpSensor.spectrumToSensorRgb(color);

            // Clip values to 0...1 if tracer traces images and not properties light depth or surface
            if (!(scene_.lock()->getTracer().lock()->tracesProperty()))
            {
                rgb[0] = std::min<float>(1.f, std::max<float>(0.f, rgb[0]));
                rgb[1] = std::min<float>(1.f, std::max<float>(0.f, rgb[1]));
                rgb[2] = std::min<float>(1.f, std::max<float>(0.f, rgb[2]));
            }

            image.lock()->operator()(u, v, 0) = rgb[0];
            image.lock()->operator()(u, v, 1) = rgb[1];
            image.lock()->operator()(u, v, 2) = rgb[2];

        }
        else if (imageType == ImageType::Type::IdealRgb)
        {
            std::array<float, 3> rgb = tmpSensor.spectrumToIdealRgb(color);

            // Clip values to 0...1 if tracer traces images and not properties light depth or surface
            if (!(scene_.lock()->getTracer().lock()->tracesProperty()))
            {
                rgb[0] = std::min<float>(1.f, std::max<float>(0.f, rgb[0]));
                rgb[1] = std::min<float>(1.f, std::max<float>(0.f, rgb[1]));
                rgb[2] = std::min<float>(1.f, std::max<float>(0.f, rgb[2]));
            }

            image.lock()->operator()(u, v, 0) = rgb[0];
            image.lock()->operator()(u, v, 1) = rgb[1];
            image.lock()->operator()(u, v, 2) = rgb[2];
        }
        else if (imageType == ImageType::Type::MultiSpectral)
        {
            std::shared_ptr<std::array<float, iiit::numSpectralSamples> > spectrum = tmpSensor.spectrumToSensorSpectrum(color);

            // Clip values to 0...1 if tracer traces images and not properties light depth or surface
            if (!(scene_.lock()->getTracer().lock()->tracesProperty()))
            {
                for (int i = 0; i < iiit::numSpectralSamples; ++i)
                {
                    image.lock()->operator()(u, v, i) = std::min<float>(1.f, std::max<float>(0.f, (*spectrum)[i]));
                }
            }
            else
            {
                for (int i = 0; i < iiit::numSpectralSamples; ++i)
                {
                    image.lock()->operator()(u, v, i) = (*spectrum)[i];
                }
            }
        }
    }



    /*! \brief Calculate aperture attenuation.
     *  \param[in] ray Ray to attenuate.
     *  \param[in] domain Indicates whether ray is on the image side or the object side.
     *  \return Returns ray attenuations.
     *
     *  \note Apertures exactly on the lens plane are considered image side internally. As long as this
     *  method is called once for the image side part and once for the object side part of a ray, this
     *  has not to be considered when implementing new camera types.
     */
    template <class SpectrumType>
    float Camera<SpectrumType>::apertureAttenuation(const Ray& ray, RayDomain domain)
    {
        if (apertures_.empty())
        {
            // no apertures at all
            return 1.f;
        }

        // check if tracer traces a property
        if (scene_.lock()->getTracer().lock()->tracesProperty())
        {
            // no partial attenuation (attenuation=0 would have already returned)
            return 1.f;
        }

        // initial attenuation
        float attenuation = 1.f;

        // loop apertures
        for (std::vector<std::shared_ptr<const Aperture> >::const_iterator it = apertures_.begin(); it != apertures_.end(); ++it)
        {
            // check if aperture is on the image side
            if (((*it)->getZPosition() <= 0.f && domain == RayDomain::ImageSide) ||
                ((*it)->getZPosition() > 0.f && domain == RayDomain::ObjectSide))
            {
                // attenuate ray
                attenuation = attenuation * (*it)->attenuateRay(ray);
            }

            // check if ray is already blocked
            if (attenuation == 0.f)
            {
                // ray is blocked
                return attenuation;
            }
        }

        return attenuation;
    }

    /*! \brief Calculate natural vignetting.
     *  \param[in] ray Ray to vignetting for.
     *  \return Returns ray vignetting.
     *
     *  \note Simply calculates the cosine^4 PHI where PHI is the incident angle (angle with z-camera-coordinate).
     */
    template <class SpectrumType>
    float Camera<SpectrumType>::naturalVignetting(const Ray& ray)
    {
        // check if tracer traces natural vignetting
        if (!(scene_.lock()->getTracer().lock()->tracesVignetting()))
        {
            // no vignetting
            return 1.f;
        }
        // check if tracer traces a property
        if (scene_.lock()->getTracer().lock()->tracesProperty())
        {
            // no vignetting
            return 1.f;
        }

        float vignetting = float(ray.direction_.z_ / ray.direction_.length());
        vignetting = vignetting*vignetting*vignetting*vignetting;

        return vignetting;
    }

} // end of namespace iiit

#endif // __CAMERA_HPP__
