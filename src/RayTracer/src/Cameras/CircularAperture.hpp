// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file CircularAperture.hpp
 *  \author Thomas Nuernberg
 *  \date 22.11.2017
 *  \brief Definition of basic circular aperture.
 */

#ifndef __CIRCULARAPERTURE_HPP__
#define __CIRCULARAPERTURE_HPP__



#include <memory>

#include "Cameras/Aperture.hpp"

#include "Utilities/Ray.hpp"



namespace iiit
{
    /*! \brief Class defines a basic circular aperture.
     */
    class CircularAperture : public Aperture
    {
    public:
        CircularAperture(float zPosition, float radius);
        float attenuateRay(const Ray& ray) const;

    private:

        float radius_; ///< Radius of circular aperture
    };

} // end of namespace iiit

#endif // __CIRCULARAPERTURE_HPP__