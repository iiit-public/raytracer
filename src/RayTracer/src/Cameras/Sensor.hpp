// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Sensor.hpp
 *  \author Maximilian Schambach
 *  \date 04.04.2017
 *  \brief Definition of class Sensor.
 *
 *  Definition of class Sensor to implement physically motivated sensor properties and image types.
 */

#ifndef __SENSOR_HPP__
#define __SENSOR_HPP__

#include <memory>
#include <array>
#include <string>

#include "Spectrum/CoefficientSpectrum.hpp"
#include "Spectrum/Monochromatic.hpp"
#include "Spectrum/RgbSpectrum.hpp"
#include "Spectrum/SampledSpectrum.hpp"



namespace iiit
{

    namespace ImageType
    {
        /*! This enum type defines the types of the camera sensor image.
         */
        enum Type
        {
            Raw,  ///< Camera sensor image is simulated raw data.
            Rgb,  ///< Camera sensor image is an RGB image that is influenced by the sensor sensitivity.
            IdealRgb, ///< Camera sensor image is an RGB image that is not influenced by the sensor sensitivity.
            MultiSpectral, ///< Camera sensor image is multispectral image.
        };
    }



    // global responsivity curves
    extern const std::vector<float> ccdSonyICX618ResponsivityLambdaSamples;
    extern const std::vector<float> ccdSonyICX618ResponsivityValueSamples;
    extern const std::vector<float> ccdSonyICX814ResponsivityLambdaSamples;
    extern const std::vector<float> ccdSonyICX814ResponsivityValueSamples;
    extern const std::vector<float> ccdOnKAI4050ResponsivityLambdaSamples;
    extern const std::vector<float> ccdOnKAI4050ResponsivityValueSamples;
    extern const std::vector<float> ccdOnKAI16070ResponsivityLambdaSamples;
    extern const std::vector<float> ccdOnKAI16070ResponsivityValueSamples;
    extern const std::vector<float> ccdOnKAI16070RedResponsivityLambdaSamples;
    extern const std::vector<float> ccdOnKAI16070RedResponsivityValueSamples;
    extern const std::vector<float> ccdOnKAI16070GreenResponsivityLambdaSamples;
    extern const std::vector<float> ccdOnKAI16070GreenResponsivityValueSamples;
    extern const std::vector<float> ccdOnKAI16070BlueResponsivityLambdaSamples;
    extern const std::vector<float> ccdOnKAI16070BlueResponsivityValueSamples;
    extern const std::vector<float> cmosSonyIMX174ResponsivityLambdaSamples;
    extern const std::vector<float> cmosSonyIMX174ResponsivityValueSamples;
    extern const std::vector<float> idealResponsivityLambdaSamples;
    extern const std::vector<float> idealResponsivityValueSamples;



    /*! \brief Class defining the Sensor of the Camera.
     */
    template <class SpectrumType>
    class Sensor
    {
    public:
        Sensor();
        Sensor(ImageType::Type imageType, SampledSpectrum responsivity, std::string sensorName);
        Sensor(ImageType::Type imageType, std::vector<std::shared_ptr<const SampledSpectrum> > responsivities, std::string sensorName);
        ~Sensor();

        std::string getSensorName() const;
        ImageType::Type  getImageType() const;
        int getNumChannels() const;
        std::vector<std::shared_ptr<const SampledSpectrum> > getResponsivity() const;

        float spectrumToSensorRaw(SpectrumType color);
        std::array<float, 3> spectrumToSensorRgb(SpectrumType color);
        std::array<float, 3> spectrumToIdealRgb(SpectrumType color);
        std::shared_ptr<std::array<float, iiit::numSpectralSamples> > spectrumToSensorSpectrum(SpectrumType color);
        std::shared_ptr<std::array<float, 1 + iiit::numSpectralSamples> > spectrumToSensorRawSpectrum(SpectrumType color);

    private:
        std::string sensorName_; ///< Sensor name that is written in the ENVI header.
        std::vector<std::shared_ptr<const SampledSpectrum> > responsivity_; ///< Vector storing the responsivity curve(s).
        ImageType::Type imageType_; ///< Type of captured image.

    }; // end of class Sensor declaration



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Sensor<SpectrumType>::Sensor()
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] imageType type of image that sensor captures.
     *  \param[in] responsivity SampledSpectrum defining the sensor responsivity.
     *  \param[in] sensorName Name of the simulated sensor.
     */
    template <class SpectrumType>
    Sensor<SpectrumType>::Sensor(ImageType::Type imageType, SampledSpectrum responsivity, std::string sensorName)
        : sensorName_(sensorName)
        , imageType_(imageType)
    {
        responsivity_.push_back(std::shared_ptr<const SampledSpectrum>(new SampledSpectrum(responsivity)));
    }



    /*! \brief Constructor with initialization and color dependent color sensitivity.
     *  \param[in] imageType type of image that sensor captures.
     *  \param[in] responsivities Vector of SampledSpectrum objects defining the sensor responsivity for each color channel.
     *  \param[in] sensorName Name of the simulated sensor.
     */
    template <class SpectrumType>
    Sensor<SpectrumType>::Sensor(ImageType::Type imageType, std::vector<std::shared_ptr<const SampledSpectrum> > responsivities, std::string sensorName)
        : sensorName_(sensorName)
        , imageType_(imageType)
    {
        for (std::vector<std::shared_ptr<const SampledSpectrum> >::const_iterator it = responsivities.begin(); it != responsivities.end(); ++it)
        {
            responsivity_.push_back(std::shared_ptr<const SampledSpectrum>(new SampledSpectrum(**it)));
        }
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    Sensor<SpectrumType>::~Sensor()
    {
    }



    /*! \brief Get name of sensor.
     *  \return Returns name of sensor.
     */
    template <class SpectrumType>
    std::string Sensor<SpectrumType>::getSensorName() const
    {
        return sensorName_;
    }



    /*! \brief Get type of capture image.
     *  \return Returns type of captured image.
     */
    template <class SpectrumType>
    ImageType::Type Sensor<SpectrumType>::getImageType() const
    {
        return imageType_;
    }


    /*! \brief Get the number of channels of the image that the camera takes.
     *  \returns Returns the number of channels of the sensor image.
     */
    template <class SpectrumType>
    int Sensor<SpectrumType>::getNumChannels() const
    {
        switch (this->getImageType())
        {
        case ImageType::Type::Raw:
            return 1;
        case ImageType::Type::Rgb:
            return 3;
        case ImageType::Type::IdealRgb:
            return 3;
        case ImageType::Type::MultiSpectral:
            return iiit::numSpectralSamples;
        }
        return -1; // to produce error
    }



    /*! \brief Get responsivity curve(s)
     *  \return Returns vector with responsivity curve(s)
     */
    template <class SpectrumType>
    std::vector<std::shared_ptr<const SampledSpectrum> > Sensor<SpectrumType>::getResponsivity() const
    {
        return responsivity_;
    }



    /*! \brief Converts given color to raw sensor value.
     *  \param[in] color Color to convert.
     *  \return Returns raw value.
     *
     *  Converts given color to raw sensor value by first generating the full SampledSpectrum
     *  representation of the given color, then applying the sensor responsivity and then integrating
     *  over all wavelengths.
     */
    template <class SpectrumType>
    float Sensor<SpectrumType>::spectrumToSensorRaw(SpectrumType color)
    {
        SampledSpectrum resSpectrum;
        color.toSampled(resSpectrum);
        resSpectrum = resSpectrum * (*(responsivity_[0])); // responsivity of non-rgb sensor always in first element

        float sum = 0.f;
        for (unsigned int i = 0; i < numSpectralSamples; ++i)
        {
            sum += resSpectrum.getCoeff(i);
        }
        return sum / numSpectralSamples;
    }



    /*! \brief Converts given color to rgb values.
     *  \param[in] color Color to convert.
     *  \return Returns rgb values.
     *
     *  Converts given color to rgb values by first generating the full SampledSpectrum representation
     *  of the given color. When three responsivity curves are available, they are applied separately
     *  to the spectrum, thus converting the color to the sensor's own rgb color space. When there is
     *  only one responsivity curve defined, it is applied and the resulting spectrum is converted to
     *  rgb using the CIE standard observer curves.
     */
    template <class SpectrumType>
    std::array<float, 3> Sensor<SpectrumType>::spectrumToSensorRgb(SpectrumType color)
    {
        SampledSpectrum resSpectrum;
        color.toSampled(resSpectrum);
        std::array<float, 3> rgb;

        if (responsivity_.size() == 3)
        {
            // convert to rgb by applying channel-wise responsivities
            for (unsigned int i = 0; i < responsivity_.size(); ++i)
            {
                SampledSpectrum resChannelSpectrum = resSpectrum * (*(responsivity_[i]));
                float sum = 0.f;
                for (unsigned int j = 0; j < numSpectralSamples; ++j)
                {
                    sum += resChannelSpectrum.getCoeff(j);
                }
                rgb[i] = sum / numSpectralSamples;
            }
        }
        else
        {
            // apply single responsivity ...
            resSpectrum = resSpectrum * (*(responsivity_[0]));
            // ... and convert to rgb
            resSpectrum.toRgb(rgb);
        }
        
        return rgb;
    }



    /*! \brief Converts given color to ideal rgb values.
     *  \param[in] color Color to convert.
     *  \return Returns ideal rgb values.
     *
     *  Converts given color directly to rgb values without applying the sensor responsivity.
     */
    template <class SpectrumType>
    std::array<float, 3> Sensor<SpectrumType>::spectrumToIdealRgb(SpectrumType color)
    {
        std::array<float, 3> rgb;
        color.toRgb(rgb);
        return rgb;
    }



    /*! \brief Converts given color to spectrum.
     *  \param[in] color Color to convert.
     *  \return Returns spectrum.
     */
    template <class SpectrumType>
    std::shared_ptr<std::array<float, numSpectralSamples> > Sensor<SpectrumType>::spectrumToSensorSpectrum(SpectrumType color)
    {
        SampledSpectrum resSpectrum;
        color.toSampled(resSpectrum);
        resSpectrum = resSpectrum * (*(responsivity_[0])); // responsivity of non-rgb sensor always in first element

        std::shared_ptr<std::array<float, numSpectralSamples> > result =
            std::shared_ptr<std::array<float, numSpectralSamples> >(new std::array<float, numSpectralSamples>);
        for (unsigned int i = 0; i < numSpectralSamples; ++i)
        {
            (*result)[i] = resSpectrum.getCoeff(i);
        }
        return result;
    }



    /*! \brief Converts given color to spectrum plus additional raw value.
    *  \param[in] color Color to convert.
    *  \return Returns array containing spectrum plus additional raw value in last element.
    */
    template <class SpectrumType>
    std::shared_ptr<std::array<float, 1 + numSpectralSamples> > Sensor<SpectrumType>::spectrumToSensorRawSpectrum(SpectrumType color)
    {
        SampledSpectrum resSpectrum;
        color.toSampled(resSpectrum);
        resSpectrum = resSpectrum * (*(responsivity_[0])); // responsivity of non-rgb sensor always in first element

        std::shared_ptr<std::array<float, 1 + numSpectralSamples> > result =
            std::shared_ptr<std::array<float, 1 + numSpectralSamples> >(new std::array<float, 1 + numSpectralSamples>);
        float sum = 0.f;
        for (unsigned int i = 0; i < numSpectralSamples; ++i)
        {
            (*result)[i] = resSpectrum.getCoeff(i);
            sum += resSpectrum.getCoeff(i);
        }
        (*result)[numSpectralSamples] = sum / numSpectralSamples;
        return result;
    }

} // end of namespace iiit

#endif // __SENSOR_HPP__
