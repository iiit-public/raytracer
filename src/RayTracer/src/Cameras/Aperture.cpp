// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Aperture.cpp
 *  \author Thomas Nuernberg
 *  \date 22.11.2017
 *  \brief Implementation of abstract class Aperture.
 *
 *  Implementation of abstract class Aperture which represents the aperture in front of the lens.
 */

#include "Aperture.hpp"



namespace iiit
{

    /*! \brief Default constructor.
     *  \param[in] lensRadius Radius of camera lens.
     */
    Aperture::Aperture(float zPosition)
        : zPosition_(zPosition)
    {
    }



    /*! \brief This function calculates the intersection point of a given ray with a XY Plane at
     *  Position z = zPosition_.
     *  \param[in] z z-Coordinate of XY plane that shall be hit.
     *  \param[in] ray Ray that shall intersect the XY Plane.
     *  \return Returns 2D hit point.
     */
    Point2D Aperture::calcHitPoint(const Ray& ray) const
    {
        float t = (zPosition_ - ray.origin_.z_)/ ray.direction_.z_;
        Point2D hitPoint(ray.origin_.x_ + t * ray.direction_.x_, ray.origin_.y_ + t * ray.direction_.y_);
        return hitPoint;
    }

} // end of namespace iiit
