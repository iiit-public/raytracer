// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file ImageAperture.cpp
 *  \author Mohamed Salem Koubaa
 *  \date 26.04.2015
 *  \author Christian Zimmermann
 *  \date 05.08.2015
 *  \author Sandro Braun
 *  \date 01.07.2016
 *  \brief Implementation of an image based aperture.
 *
 *  Implementation of an aperture based on a pixel image.
 */

#include "ImageAperture.hpp"

#include <algorithm>

#include "Logger.hpp"



namespace iiit
{

    /*! \brief Default constructor.
     *  \param[in] zPosition Z position of aperture.
     *  \param[in] lensRadius Radius of camera lens.
     */
    ImageAperture::ImageAperture(float zPosition, float lensRadius)
        : Aperture(zPosition)
        , pixelAspectRatio(1.f)
        , lensRadius_(lensRadius)
        , useExternalAttenuationFunction_(false)
        , isPeriodicPatternd_(false)
        , apertureScalingMethod_(ApertureOutsideLens)
        , maskIsRotated_(false)
    {
        calcTransformation();   //do not change this. This is necessary to guarantee a working aperture at any time
    }



    /*! \brief Checks the attenuation of a ray.
     *  \param[in] ray Ray to attenuate.
     *  \return Returns attenuation factor between 0 and 1.
     */
    float ImageAperture::attenuateRay(const Ray& ray) const
    {
        Point2D localHitPoint = calcHitPoint(ray);
        if (useExternalAttenuationFunction_)
        {
            //TODO
            return 0.f;
        }
        else
        {
            // is there an in-plane rotation of the mask?
            if (maskIsRotated_)
            {
                double x = localHitPoint.x_;
                double y = localHitPoint.y_;
                localHitPoint.x_ = inPlaneRotCos_*x - inPlaneRotSin_*y;
                localHitPoint.y_ = inPlaneRotSin_*x + inPlaneRotCos_*y;
            }
            if (isPeriodicPatternd_)
            {
                //is localHitPoint on mask ?
                if (fabs(localHitPoint.x_) > periodMaskHalfWidth_) //f abs is abs for floats/double
                {
                    return 0.f;
                }
                if (fabs(localHitPoint.y_) > periodMaskHalfHeight_)
                {   //f abs is abs for float/double
                    return 0.f;
                }

                // transform camera coordinates to aperture pixel coordinates on unit pattern
                int u = static_cast<int>(floor(uSlope_ * localHitPoint.x_ + uOffset_));
                int v = static_cast<int>(floor(vSlope_ * localHitPoint.y_ + vOffset_));
                u = u % widthAperturePx_;
                if (u < 0)
                {
                    u += widthAperturePx_;
                }
                v = v % heightAperturePx_;
                if (v < 0)
                {
                    v += heightAperturePx_;
                }
                //LOG(LogLevel::LogInfo, u << "," << v);


                // check if point lies within defined mask
                if (u < 0 || u >= widthAperturePx_ || v < 0 || v >= heightAperturePx_)
                {
                    return 0.f; //point is out of mask
                }
                return (*mask_)(u, v);

            }
            else
            {
                // transform camera coordinates to aperture pixel coordinates
                int u = static_cast<int>(floor(uSlope_ * localHitPoint.x_ + uOffset_));
                int v = static_cast<int>(floor(vSlope_ * localHitPoint.y_ + vOffset_));

                // check if point lies within defined mask
                if (u < 0 || u >= widthAperturePx_ || v < 0 || v >= heightAperturePx_)
                {
                    return 0.f; //point is out of mask
                }
                return (*mask_)(u, v);
            }
        }
    }



    /*! \brief Builds a circular aperture mask.
     *  \param[in] width Width of mask measured in pixels. Represents the discrete resolution of the
     *  horizontal dimension of the mask. The real width equals the lenses diameter.
     *  \param[in] height Height of mask measured in pixels. Represents the discrete resolution of the
     *  vertical dimension of the mask. The real height equals the lenses diameter
     *  \param[in] radius Radius of the circular aperture.
     *  \return Returns TRUE if building was successful.
     */
    bool ImageAperture::setCircularAperture(int width, int height, int radius)
    {
        apertureType_ = ApertureType::Circular;
        // check input
        if (((2 * radius + 1) > width) || ((2 * radius + 1) > height))
        {
            std::cout << "width or height are too small for the chosen radius. Increasing width and height to minimal size." << std::endl;
            width = 2 * radius + 1;
            height = 2 * radius + 1;
        }
        if ((width % 2) == 0)
        {
            std::cout << "width should be an odd number. Increasing by one." << std::endl;
            width += 1;
        }
        if ((height % 2) == 0)
        {
            std::cout << "height should be an odd number. Increasing by one." << std::endl;
            height += 1;
        }
        widthAperturePx_ = width;
        heightAperturePx_ = height;

        int centerWidth = static_cast<int>(floor(width / 2.0));
        int centerHeight = static_cast<int>(floor(height / 2.0));
        mask_ = std::shared_ptr<cimg_library::CImg<float> >(new cimg_library::CImg<float>(width, height, 1, 1));

        for (int a = 0; a < width; ++a)
        {
            for (int b = 0; b < height; ++b)
            {
                if (sqrt(pow((a - centerWidth), 2) + pow((b - centerHeight), 2)) <= radius)
                {
                    // set pixel transparent
                    (*mask_)(a, b) = 1.f;
                }
                else
                {
                    // pixel black / intransparent
                    (*mask_)(a, b) = 0.f;
                }
            }
        }
        calcTransformation();
        calcOpacity();
        return true;
    }



    /*! \brief Builds a circular ring aperture mask.
     *  \param[in] width Width of mask measured in pixels. Represents the discrete resolution of the
     *  horizontal dimension of the mask. The real width is equal to the lens' diameter
     *  \param[in] height Height of mask measured in pixels. Represents the discrete resolution of the
     *  vertical dimension of the mask. The real height is equal to the lens' diameter
     *  \param[in] innerRadius Inner radius of the mask.
     *  \param[in] outerRadius Outer radius of the mask.
     *  \return Returns TRUE when building was successful.
     */
    bool ImageAperture::setCircularRingAperture(int width, int height, int innerRadius, int outerRadius)
    {
        // check input
        if (innerRadius > outerRadius) 
        {
            std::cout << "InnerRadius should be smaller than outerRadius. Choosing innerRadius as zero." << std::endl;
            innerRadius = 0;
        }
        if (((2 * outerRadius + 1) > width) || ((2 * outerRadius + 1) > height))
        {
            std::cout << "width or height are too small for the chosen outerRadius. Increasing width and height to minimal size." << std::endl;
            width = 2 * outerRadius + 1;
            height = 2 * outerRadius + 1;
        }
        if ((width % 2) == 0)
        {
            std::cout << "width should be an odd number. Increasing by one." << std::endl;
            width += 1;
        }
        if ((height % 2) == 0)
        {
            std::cout << "height should be an odd number. Increasing by one." << std::endl;
            height += 1;
        }
        widthAperturePx_ = width;
        heightAperturePx_ = height;

        int centerWidth = static_cast<int>( floor(width / 2.0) );
        int centerHeight = static_cast<int>( floor(height / 2.0) );
        mask_ = std::shared_ptr<cimg_library::CImg<float> >(new cimg_library::CImg<float>(width, height, 1, 1));

        for (int a = 0; a < width; ++a)
        {
            for (int b = 0; b < height; ++b)
            {
                auto dist = sqrt(pow((a - centerWidth), 2) + pow((b - centerHeight), 2));
                if ((dist > innerRadius) && (dist <= outerRadius))
                {
                    (*mask_)(a, b) = 1.f;
                }
                else
                {
                    (*mask_)(a, b) = 0.f;
                }
            }
        }
        calcTransformation();
        calcOpacity();
        return true;
    }



    /*! \brief Builds a rectangular aperture mask.
     *  \param[in] width Width of mask measured in pixels. Represents the discrete resolution
     *  of the horizontal dimension of the mask. (not necessarily the rectangle)
     *  \param[in] height Height of mask measured in pixels. Represents the discrete resolution
     *  of the vertical dimension of the mask. (not necessarily the rectangle)
     *  \param[in] rectHalfWidth The half of the width of the rectangle
     *  \param[in] rectHalfHeight The half of the height of the rectangle
     *  \return Returns TRUE when building was successful.
     */
    bool ImageAperture::setRectangularAperture(int width, int height, int rectHalfWidth, int rectHalfHeight)
    {
        apertureType_ = ApertureType::Rectangular;
        // we want width and height to be even
        if (height % 2 == 1)
        {
            height++;
        }
        if (width % 2 == 1)
        {
            width++;
        }
        widthAperturePx_ = width;
        heightAperturePx_ = height;

        mask_ = std::shared_ptr<cimg_library::CImg<float> >(new cimg_library::CImg<float>(width, height, 1, 1, 0.f));
        for (int u = 0; u < width; ++u)
        {
            for (int v = 0; v < height; ++v)
            {
                if ((v >= static_cast<int>(height / 2 - rectHalfHeight)) && (v <= static_cast<int>(height / 2 - 1 + rectHalfHeight)))
                {
                    if ((u >= static_cast<int>(width / 2 - rectHalfWidth)) && ( u <= static_cast<int>(width / 2 - 1 + rectHalfWidth)))
                    {
                        (*mask_)(u,v) = 1.f;
                    }
                }
            }
        }
        calcTransformation();
        calcOpacity();
        return true;
    }



    /*! \brief Builds levin style aperture mask (hard-coded).
     *  \return Returns TRUE if building was successful.
     * 
     * <a href="https://graphics.stanford.edu/courses/cs448a-08-spring/levin-coded-aperture-sig07.pdf"> See more about levin here.</a>
     */
    bool ImageAperture::setLevinAperture(void)
    {
        apertureType_ = ApertureType::Levin;

        // build base mask pattern
        int width = 16;
        int height = 16;
        std::shared_ptr<cimg_library::CImg<float> > maskSmall(new cimg_library::CImg<float>(width, height, 1, 1));
        defineLevinAperture(maskSmall);
        
        // rescale base pattern for increased resolution
        widthAperturePx_ = width * 10;
        heightAperturePx_ = height * 10;
        mask_ = std::shared_ptr<cimg_library::CImg<float> >(new cimg_library::CImg<float>(widthAperturePx_, heightAperturePx_, 1, 1));
        
        for (int i = 0; i < widthAperturePx_; ++i)
        {
            for (int j = 0; j < heightAperturePx_; ++j)
            {
                (*mask_)(i, j) = (*maskSmall)(static_cast<int>(floor(i / 10.0)), static_cast<int>(floor(j / 10.0)));
            }
        }
        calcTransformation();
        calcOpacity();
        return true;
    }



    /*! \brief Load aperture from specified file path to .bmp file.
     *  \param[in] file Path and name of the file. Path is relative from the working directory, which is the location of the basicfrontend.exe file.
     *  Currently only .bmp files are supported
     *  \return Returns TRUE if loading and building was successful.
     */
    bool ImageAperture::setCustomImageAperture(std::string file)
    {
        // open file to check for existence
        std::FILE* f = std::fopen(file.c_str(), "r");

        // open aperture image if existent
        if (f != NULL)
        {
            std::fclose(f);
            apertureType_ = ApertureType::CustomImage;
            mask_ = std::shared_ptr<cimg_library::CImg<float> >(new cimg_library::CImg<float>(file.c_str()));

            widthAperturePx_ = static_cast<int>(mask_->width());
            heightAperturePx_ = static_cast<int>(mask_->height());
            (*mask_) /= 255.f;
            calcTransformation();
            calcOpacity();
            return true;
        }

        return false;
    }



    /*! \brief Moves the aperture pattern in u & v direction. This is done by using cimg->shift method
     *  \param[in] u Translation in u direction.
     *  \param[in] v Translation in v direction.
     */
    void ImageAperture::setApertureTranslation(int u, int v)
    {
        mask_->shift(u, v);
    }



    /*! \brief Adds rotational Information to the aperture pattern. The Axis are taken from camera coordinate system (see dox)
     *  \param[in] zRotation rotation around z axis (right-handed positive)
     */
    void ImageAperture::setInPlaneRotation(double zRotationAngle)
    {
        maskIsRotated_ = true;
        inPlaneRotCos_ = cos(zRotationAngle);
        inPlaneRotSin_ = sin(zRotationAngle);
        calcTransformation();
    }



    /*! \brief Sets scaling of aperture mask to be apertureInsideLens
     *  This scaling is not supported by periodic apertues!
     *  All apertures are defined as a rectangular pixel image contained in a CIMG object.
     *  The rectangular aperture is scaled so that it completely fits into the circular lens. For
     *  the lens radius then holds
     *
     *  \f[r_\textrm{\scriptsize lens} = \sqrt{\left(\frac{h_\textrm{\scriptsize phys}}{2}\right)^2 + \left(\frac{w_\textrm{\scriptsize phys}}{2}\right)^2}\f]
     *  with physical aperture height \f$h_\textrm{\scriptsize phys}\f$ and width
     *  \f$w_\textrm{\scriptsize phys}\f$.
     *  \image html aperture_frame_true.png "apertureInsideLens set to TRUE."
     *
     */
    void ImageAperture::setApertureInsideLens()
    {
        apertureScalingMethod_ = ApertureInsideLens;
        calcTransformation();
    }



    /*! \brief Sets scaling of aperture mask.
     *  \param[in] apertureOutsideLens Desired scaling mode.
     *
     *  The rectangular aperture is scaled to completely enclose the circular lens. For
     *  the lens radius then holds
     *
     *  \f[r_\textrm{\scriptsize lens} = \frac{1}{2}\min\left(h_\textrm{\scriptsize phys}, w_\textrm{\scriptsize phys}\right).\f]
     *
     *  \image html aperture_frame_false.png "apertureInsideLens set to FALSE."
     *
     */
    void ImageAperture::setApertureOutsideLens()
    {
        apertureScalingMethod_ = ApertureOutsideLens;
        calcTransformation();
    }



    /*! \brief Sets pixels size to custom pixel size. Works for all types of masks.
     *  \param[in] desiredPixelWidth Desired WIDTH of the aperture Pixels
     */
    void ImageAperture::setCustomPixelSize(double desiredPixelWidth)
    {
        apertureScalingMethod_ = CustomPixelSize;
        pixelWidth_ = desiredPixelWidth;
        calcTransformation();
    }



    /*! \brief Sets pixel ratio factor.
     *  \param[in] ratio Ratio of physical aperture pixel size (width/height).
     *
     *  Use this method to change the aspect ratio of the physical size of the aperture mask pixels, in
     *  case they should not be simulated as quadratic. The aspect ration is defined as
     *
     *  \f[a = \frac{s_u}{s_v}.\f]
     */
    void ImageAperture::setPixelAspectRatio(float ratio)
    {
        pixelAspectRatio = ratio;
        calcTransformation();
    }



    /*! \brief Calculates linear coordinate transformation parameters for camera frame to aperture pixel
     *  frame conversion.
     *
     *  In general the conversion from camera frame to aperture pixel frame is described by the linear
     *  equation
     *
     *  \f[p_\textrm{\scriptsize pixel} = m \cdot p_\textrm{\scriptsize camera} + c,\f]
     *
     *  where \f$p_\textrm{\scriptsize pixel}\f$ denotes a aperture pixel coordinate and
     *  \f$p_\textrm{\scriptsize camera}\f$ a camera frame coordinate.
     *
     *  -# If the aperture mask is scaled to enclose the circular lens, the slope and offset parameters
     *  depending on the coordinate to transform are:
     *
     *  \f[m_u = \min\left(h_\textrm{\scriptsize pixel}, a \cdot w_\textrm{\scriptsize pixel}\right) \frac{1}{2 a r_\textrm{\scriptsize lens}}, \qquad c_u = \frac{w_\textrm{\scriptsize pixel}}{2},\f]
     *
     *  \f[m_v = \min\left(h_\textrm{\scriptsize pixel}, a \cdot w_\textrm{\scriptsize pixel}\right) \frac{1}{2 r_\textrm{\scriptsize lens}}, \qquad c_v = \frac{h_\textrm{\scriptsize pixel}}{2}.\f]
     *
     *  Here, \f$a\f$ denotes the aspect ratio of the physical size of the aperture mask pixels
     *
     *  \f[a = \frac{s_u}{s_v}.\f]
     *
     *  \image html aperture_frame_false.png "Rectangular aperture encloses circular lens."
     *
     *  -# In the case that the rectangular aperture is scaled to completely fit inside the circular lens,
     *  the conversion parameters are:
     *
     *  \f[m_u = \frac{w_\textrm{\scriptsize pixel}}{2 r_\textrm{\scriptsize lens} \cos\left(\alpha\right)}, \qquad c_u = \frac{w_\textrm{\scriptsize pixel}}{2},\f]
     *
     *  \f[m_v = \frac{h_\textrm{\scriptsize pixel}}{2 r_\textrm{\scriptsize lens} \sin\left(\alpha\right)}, \qquad c_v = \frac{h_\textrm{\scriptsize pixel}}{2}.\f]
     *
     *  To calculate the sine and cosine of \f$\alpha\f$, which denotes the angle between the
     *  \f$x_\textrm{\scriptsize c}\f$-axis and the marked radius in the image below, the following
     *  theorems are used:
     *  \f{eqnarray*}{
     *       \tan(\alpha) &=& \left(\frac{h_\textrm{\scriptsize pixel}}{a \cdot w_\textrm{\scriptsize pixel}}\right) \\ 
     *       \sin(\alpha) &=& \frac{\tan(\alpha)}{\sqrt{1 + \tan^2(\alpha)}} \\
     *       \cos(\alpha) &=& \frac{1}{\sqrt{1 + \tan^2(\alpha)}}
     *  \f}
     *
     *  -# If using a periodic aperture, the key customPixelWidth has to be used to define the size of the aperture together with a provided grayscale image*
     *
     *  \image html aperture_frame_true.png "Rectangular aperture completely fits into circular lens."
     */
    void ImageAperture::calcTransformation()
    {
        switch (apertureScalingMethod_)
        {
            {
                case ApertureInsideLens:
                    calcTransformationApertureInsideLens();
                    break;
            }
            {
                case ApertureOutsideLens:
                    calcTransformationApertureOutsideLens();
                    break;
            }
            {
                case CustomPixelSize:
                    calcTransformationCustomPixelSize();
                    break;
            }
        };
    }



    /*! \brief Subfunction which does the actual calculation.
     *
     */
    void ImageAperture::calcTransformationApertureInsideLens()
    {
        double width = static_cast<double>(widthAperturePx_);
        double height = static_cast<double>(heightAperturePx_);

        // cos(x) = 1 / sqrt(1 + tan^2(x))
        double invCosAlpha = sqrt(1.0 + (height / width / pixelAspectRatio) * (height / width / pixelAspectRatio));
        uSlope_ = static_cast<float>(width / (2.0 * lensRadius_) * invCosAlpha);
        uOffset_ = widthAperturePx_ / 2.f;

        // sin(x) = tan(x) / sqrt(1 + tan^2(x))
        double invSinAlpha =
                sqrt(1.0 + (height / width / pixelAspectRatio) * (height / width / pixelAspectRatio)) / height * width *
                pixelAspectRatio;
        vSlope_ = static_cast<float>(height / (2.0 * lensRadius_) * invSinAlpha);
        vOffset_ = heightAperturePx_ / 2.f;
    }



    /*! \brief Subfunction which does the actual calculation.
     *
     */
    void ImageAperture::calcTransformationApertureOutsideLens()
    {
        uSlope_ = static_cast<float>(std::min(widthAperturePx_ * pixelAspectRatio, static_cast<float>(heightAperturePx_)) / (2.0 * pixelAspectRatio * lensRadius_));
        uOffset_ = widthAperturePx_ / 2.f;
        vSlope_ = static_cast<float>(std::min(widthAperturePx_ * pixelAspectRatio, static_cast<float>(heightAperturePx_)) / (2.0 * lensRadius_));
        vOffset_ = heightAperturePx_ / 2.f;
    }



    /*! \brief Subfunction which does the actual calculation.
     *
     */
    void ImageAperture::calcTransformationCustomPixelSize()
    {
        uSlope_ = static_cast<float>(1/pixelWidth_);
        vSlope_ = static_cast<float>(pixelAspectRatio/pixelWidth_);

        if (isPeriodicPatternd_)
        {
            if (numberOfUPatterns_ % 2 == 0)
            {
                // even number of tiles
                uOffset_ = 0;
            }
            else
            {
                uOffset_ = widthAperturePx_ / 2.f;
            }

            if (numberOfVPatterns_ % 2 == 0)
            {
                vOffset_ = 0;
            }
            else
            {
                vOffset_ = heightAperturePx_ / 2.f;
            }
        }
        else
        {
            uOffset_ = widthAperturePx_ / 2.f;
            vOffset_ = heightAperturePx_ / 2.f;
        }
    }



    /*! \brief Hard coded mask according to Levin paper.
     *  \param[in,out] mask Aperture mask to define.
     *
     *  \image levinBig.bmp
     */
    void ImageAperture::defineLevinAperture(std::shared_ptr<cimg_library::CImg<float> > mask)
    {
        (*mask)(0, 0) = 0.f;
        (*mask)(0, 1) = 0.f;
        (*mask)(0, 2) = 0.f;
        (*mask)(0, 3) = 1.f;
        (*mask)(0, 4) = 1.f;
        (*mask)(0, 5) = 1.f;
        (*mask)(0, 6) = 0.f;
        (*mask)(0, 7) = 1.f;
        (*mask)(0, 8) = 1.f;
        (*mask)(0, 9) = 0.f;
        (*mask)(0, 10) = 1.f;
        (*mask)(0, 11) = 1.f;
        (*mask)(0, 12) = 1.f;
        (*mask)(0, 13) = 0.f;
        (*mask)(0, 14) = 0.f;
        (*mask)(0, 15) = 0.f;

        (*mask)(1, 0) = 0.f;
        (*mask)(1, 1) = 0.f;
        (*mask)(1, 2) = 0.f;
        (*mask)(1, 3) = 1.f;
        (*mask)(1, 4) = 1.f;
        (*mask)(1, 5) = 1.f;
        (*mask)(1, 6) = 0.f;
        (*mask)(1, 7) = 1.f;
        (*mask)(1, 8) = 1.f;
        (*mask)(1, 9) = 0.f;
        (*mask)(1, 10) = 1.f;
        (*mask)(1, 11) = 1.f;
        (*mask)(1, 12) = 1.f;
        (*mask)(1, 13) = 0.f;
        (*mask)(1, 14) = 0.f;
        (*mask)(1, 15) = 0.f;

        (*mask)(2, 0) = 0.f;
        (*mask)(2, 1) = 0.f;
        (*mask)(2, 2) = 0.f;
        (*mask)(2, 3) = 0.f;
        (*mask)(2, 4) = 0.f;
        (*mask)(2, 5) = 0.f;
        (*mask)(2, 6) = 0.f;
        (*mask)(2, 7) = 1.f;
        (*mask)(2, 8) = 1.f;
        (*mask)(2, 9) = 0.f;
        (*mask)(2, 10) = 0.f;
        (*mask)(2, 11) = 0.f;
        (*mask)(2, 12) = 0.f;
        (*mask)(2, 13) = 0.f;
        (*mask)(2, 14) = 0.f;
        (*mask)(2, 15) = 0.f;

        (*mask)(3, 0) = 1.f;
        (*mask)(3, 1) = 1.f;
        (*mask)(3, 2) = 1.f;
        (*mask)(3, 3) = 0.f;
        (*mask)(3, 4) = 0.f;
        (*mask)(3, 5) = 0.f;
        (*mask)(3, 6) = 0.f;
        (*mask)(3, 7) = 0.f;
        (*mask)(3, 8) = 0.f;
        (*mask)(3, 9) = 0.f;
        (*mask)(3, 10) = 0.f;
        (*mask)(3, 11) = 0.f;
        (*mask)(3, 12) = 0.f;
        (*mask)(3, 13) = 1.f;
        (*mask)(3, 14) = 1.f;
        (*mask)(3, 15) = 1.f;

        (*mask)(4, 0) = 1.f;
        (*mask)(4, 1) = 1.f;
        (*mask)(4, 2) = 1.f;
        (*mask)(4, 3) = 0.f;
        (*mask)(4, 4) = 0.f;
        (*mask)(4, 5) = 0.f;
        (*mask)(4, 6) = 1.f;
        (*mask)(4, 7) = 1.f;
        (*mask)(4, 8) = 1.f;
        (*mask)(4, 9) = 1.f;
        (*mask)(4, 10) = 0.f;
        (*mask)(4, 11) = 0.f;
        (*mask)(4, 12) = 0.f;
        (*mask)(4, 13) = 1.f;
        (*mask)(4, 14) = 1.f;
        (*mask)(4, 15) = 1.f;

        (*mask)(5, 0) = 0.f;
        (*mask)(5, 1) = 1.f;
        (*mask)(5, 2) = 1.f;
        (*mask)(5, 3) = 0.f;
        (*mask)(5, 4) = 0.f;
        (*mask)(5, 5) = 0.f;
        (*mask)(5, 6) = 1.f;
        (*mask)(5, 7) = 1.f;
        (*mask)(5, 8) = 1.f;
        (*mask)(5, 9) = 1.f;
        (*mask)(5, 10) = 0.f;
        (*mask)(5, 11) = 0.f;
        (*mask)(5, 12) = 0.f;
        (*mask)(5, 13) = 1.f;
        (*mask)(5, 14) = 1.f;
        (*mask)(5, 15) = 0.f;

        (*mask)(6, 0) = 1.f;
        (*mask)(6, 1) = 1.f;
        (*mask)(6, 2) = 0.f;
        (*mask)(6, 3) = 0.f;
        (*mask)(6, 4) = 0.f;
        (*mask)(6, 5) = 0.f;
        (*mask)(6, 6) = 0.f;
        (*mask)(6, 7) = 1.f;
        (*mask)(6, 8) = 1.f;
        (*mask)(6, 9) = 0.f;
        (*mask)(6, 10) = 0.f;
        (*mask)(6, 11) = 0.f;
        (*mask)(6, 12) = 0.f;
        (*mask)(6, 13) = 0.f;
        (*mask)(6, 14) = 1.f;
        (*mask)(6, 15) = 1.f;

        (*mask)(7, 0) = 1.f;
        (*mask)(7, 1) = 1.f;
        (*mask)(7, 2) = 0.f;
        (*mask)(7, 3) = 0.f;
        (*mask)(7, 4) = 0.f;
        (*mask)(7, 5) = 1.f;
        (*mask)(7, 6) = 1.f;
        (*mask)(7, 7) = 1.f;
        (*mask)(7, 8) = 1.f;
        (*mask)(7, 9) = 1.f;
        (*mask)(7, 10) = 1.f;
        (*mask)(7, 11) = 0.f;
        (*mask)(7, 12) = 0.f;
        (*mask)(7, 13) = 0.f;
        (*mask)(7, 14) = 1.f;
        (*mask)(7, 15) = 1.f;

        (*mask)(8, 0) = 1.f;
        (*mask)(8, 1) = 1.f;
        (*mask)(8, 2) = 0.f;
        (*mask)(8, 3) = 0.f;
        (*mask)(8, 4) = 0.f;
        (*mask)(8, 5) = 1.f;
        (*mask)(8, 6) = 1.f;
        (*mask)(8, 7) = 1.f;
        (*mask)(8, 8) = 1.f;
        (*mask)(8, 9) = 1.f;
        (*mask)(8, 10) = 1.f;
        (*mask)(8, 11) = 0.f;
        (*mask)(8, 12) = 0.f;
        (*mask)(8, 13) = 0.f;
        (*mask)(8, 14) = 1.f;
        (*mask)(8, 15) = 1.f;

        (*mask)(9, 0) = 1.f;
        (*mask)(9, 1) = 1.f;
        (*mask)(9, 2) = 0.f;
        (*mask)(9, 3) = 0.f;
        (*mask)(9, 4) = 0.f;
        (*mask)(9, 5) = 0.f;
        (*mask)(9, 6) = 0.f;
        (*mask)(9, 7) = 1.f;
        (*mask)(9, 8) = 1.f;
        (*mask)(9, 9) = 0.f;
        (*mask)(9, 10) = 0.f;
        (*mask)(9, 11) = 0.f;
        (*mask)(9, 12) = 0.f;
        (*mask)(9, 13) = 0.f;
        (*mask)(9, 14) = 1.f;
        (*mask)(9, 15) = 1.f;

        (*mask)(10, 0) = 0.f;
        (*mask)(10, 1) = 1.f;
        (*mask)(10, 2) = 1.f;
        (*mask)(10, 3) = 0.f;
        (*mask)(10, 4) = 0.f;
        (*mask)(10, 5) = 0.f;
        (*mask)(10, 6) = 1.f;
        (*mask)(10, 7) = 1.f;
        (*mask)(10, 8) = 1.f;
        (*mask)(10, 9) = 1.f;
        (*mask)(10, 10) = 0.f;
        (*mask)(10, 11) = 0.f;
        (*mask)(10, 12) = 0.f;
        (*mask)(10, 13) = 1.f;
        (*mask)(10, 14) = 1.f;
        (*mask)(10, 15) = 0.f;

        (*mask)(11, 0) = 1.f;
        (*mask)(11, 1) = 1.f;
        (*mask)(11, 2) = 1.f;
        (*mask)(11, 3) = 0.f;
        (*mask)(11, 4) = 0.f;
        (*mask)(11, 5) = 0.f;
        (*mask)(11, 6) = 1.f;
        (*mask)(11, 7) = 1.f;
        (*mask)(11, 8) = 1.f;
        (*mask)(11, 9) = 1.f;
        (*mask)(11, 10) = 0.f;
        (*mask)(11, 11) = 0.f;
        (*mask)(11, 12) = 0.f;
        (*mask)(11, 13) = 1.f;
        (*mask)(11, 14) = 1.f;
        (*mask)(11, 15) = 1.f;

        (*mask)(12, 0) = 1.f;
        (*mask)(12, 1) = 1.f;
        (*mask)(12, 2) = 1.f;
        (*mask)(12, 3) = 0.f;
        (*mask)(12, 4) = 0.f;
        (*mask)(12, 5) = 0.f;
        (*mask)(12, 6) = 0.f;
        (*mask)(12, 7) = 0.f;
        (*mask)(12, 8) = 0.f;
        (*mask)(12, 9) = 0.f;
        (*mask)(12, 10) = 0.f;
        (*mask)(12, 11) = 0.f;
        (*mask)(12, 12) = 0.f;
        (*mask)(12, 13) = 1.f;
        (*mask)(12, 14) = 1.f;
        (*mask)(12, 15) = 1.f;

        (*mask)(13, 0) = 0.f;
        (*mask)(13, 1) = 0.f;
        (*mask)(13, 2) = 0.f;
        (*mask)(13, 3) = 0.f;
        (*mask)(13, 4) = 0.f;
        (*mask)(13, 5) = 0.f;
        (*mask)(13, 6) = 0.f;
        (*mask)(13, 7) = 1.f;
        (*mask)(13, 8) = 1.f;
        (*mask)(13, 9) = 0.f;
        (*mask)(13, 10) = 0.f;
        (*mask)(13, 11) = 0.f;
        (*mask)(13, 12) = 0.f;
        (*mask)(13, 13) = 0.f;
        (*mask)(13, 14) = 0.f;
        (*mask)(13, 15) = 0.f;

        (*mask)(14, 0) = 0.f;
        (*mask)(14, 1) = 0.f;
        (*mask)(14, 2) = 0.f;
        (*mask)(14, 3) = 1.f;
        (*mask)(14, 4) = 1.f;
        (*mask)(14, 5) = 1.f;
        (*mask)(14, 6) = 0.f;
        (*mask)(14, 7) = 1.f;
        (*mask)(14, 8) = 1.f;
        (*mask)(14, 9) = 0.f;
        (*mask)(14, 10) = 1.f;
        (*mask)(14, 11) = 1.f;
        (*mask)(14, 12) = 1.f;
        (*mask)(14, 13) = 0.f;
        (*mask)(14, 14) = 0.f;
        (*mask)(14, 15) = 0.f;

        (*mask)(15, 0) = 0.f;
        (*mask)(15, 1) = 0.f;
        (*mask)(15, 2) = 0.f;
        (*mask)(15, 3) = 1.f;
        (*mask)(15, 4) = 1.f;
        (*mask)(15, 5) = 1.f;
        (*mask)(15, 6) = 0.f;
        (*mask)(15, 7) = 1.f;
        (*mask)(15, 8) = 1.f;
        (*mask)(15, 9) = 0.f;
        (*mask)(15, 10) = 1.f;
        (*mask)(15, 11) = 1.f;
        (*mask)(15, 12) = 1.f;
        (*mask)(15, 13) = 0.f;
        (*mask)(15, 14) = 0.f;
        (*mask)(15, 15) = 0.f;

        mask->transpose();
    }



    /*! \brief make the defined aperture be periodic
     *  \param[in] numberOfPatterns_u Number of repetitions of the provided pattern in horizontal direction.
     *  \param[in] numberOfPatterns_v Number of repetitions of the provided pattern in vertical direction.
     */
    void ImageAperture::setPeriodic(int numberOfPatterns_u, int numberOfPatterns_v)
    {
        isPeriodicPatternd_ = true;
        numberOfUPatterns_ = numberOfPatterns_u;
        numberOfVPatterns_ = numberOfPatterns_v;

        periodMaskHalfWidth_ = pixelWidth_ * mask_->width() * numberOfUPatterns_ / 2.f;
        periodMaskHalfHeight_ = pixelAspectRatio * pixelWidth_ * mask_->height() *  numberOfVPatterns_ / 2.f;
        calcTransformation();
    }



    /*! \brief calculates the average gray level of the defined mask as a float between 0 and 1.
     *  uses CIMG->mean() function.
     */
    void ImageAperture::calcOpacity()
    {
        opacity_ = static_cast<float>(mask_->mean());
        if (opacity_ > 1.f)
        {
            opacity_ /= 255.f;      // this means that the image range still was 0-255
        }
    }



    /*! \brief Builds a rectangular aperture mask.
     *  \param[in] width Width of mask measured in pixels. Represents the discrete resolution
     *  of the horizontal dimension of the mask and the rectangle.
     *  \param[in] height Height of mask measured in pixels. Represents the discrete resolution
     *  of the vertical dimension of the mask and the rectangle.

     *  \return Returns TRUE when building was successful.
     */
    bool ImageAperture::setRectangularAperture(int width, int height)
    {
        return setRectangularAperture(width, height, static_cast<int>(width/2), static_cast<int>(height/2));
    }



    //void ImageAperture::saveMask() const
    //{
    //    cimg_library::CImg<unsigned char> image(mask_->width(), mask_->height(), 1, 1);
    //    for (int u = 0; u < mask_->width(); ++u)
    //    {
    //        for (int v = 0; v < mask_->height(); ++v)
    //        {
    //            if ((*mask_)(v, u) != 0.f)
    //            {
    //                image(v, u) = 255;
    //            }
    //            else
    //            {
    //                image(v, u) = 0;
    //            }
    //        }
    //    }
    //    image.save_bmp("mask.bmp");
    //}

} // end of namespace iiit
