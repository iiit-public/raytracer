// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Aperture.hpp
 *  \author Thomas Nuernberg
 *  \date 22.11.2017
 *  \brief Definition of abstract class Aperture.
 *
 *  Definition of abstract class Aperture which represents a general aperture in the optical path.
 */

#ifndef __APERTURE_HPP__
#define __APERTURE_HPP__



#include <memory>

#include "Utilities/Ray.hpp"
#include "Utilities/Point2D.hpp"



namespace iiit
{

    /*! \brief Class defines the aperture.
     *
     *  This class is used to check whether a ray passes the aperture or not.
     */
    class Aperture
    {
    public:
        Aperture(float zPosition);
        float getZPosition() const;
        virtual float attenuateRay(const Ray& ray) const = 0;

    protected:
        Point2D calcHitPoint(const Ray& ray) const;

    private:
        float zPosition_;
    };



    // inlined member functions

    /*! \brief Get z position of aperture.
     *  \return Returns z position of aperture.
     */
    inline float Aperture::getZPosition() const
    {
        return zPosition_;
    }

} // end of namespace iiit

#endif // __APERTURE_HPP__