// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file ViewPlane.hpp
 *  \author Thomas Nuernberg
 *  \date 16.12.2014
 *  \brief Definition of class ViewPlane.
 *
 *  Definition of class ViewPlane, which holds the parameters of the image plane.
 */

#ifndef __VIEWPLANE_HPP__
#define __VIEWPLANE_HPP__

#include <memory>

#include "Sampler/Sampler.hpp"



namespace iiit
{

    /*! \brief Class defines the image plane corresponding to the image sensor.
     *
     *  This class defines the image plane of the simulated camera. It corresponds to the image sensor.
     *  The pixels are addressed by uv-coordinates which relate to the camera frame as shown in the
     *  image below.
     *  \image html pixel_frame.png
     *
     *  The class is suited to represent only a section of the complete plane enabling to partition the
     *  image plane. This is useful for rendering with multiple independent threads. This leads to the
     *  introduction of relative pixel coordinates, which denote the relative position of a pixel in the
     *  respective partition of the complete plane. Use the offset members and the conversion
     *  methods to switch between absolute and relative plane pixel coordinates.
     */
    class ViewPlane
    {
    public:
        ViewPlane();
        ~ViewPlane();

        ViewPlane& operator=(const ViewPlane& other);

        int uAbs(int uRel) const;
        int uRel(int uAbs) const;

        int vAbs(int vRel) const;
        int vRel(int vAbs) const;

        int uAbsBegin() const;
        int uRelBegin() const;

        int vAbsBegin() const;
        int vRelBegin() const;

        int uAbsEnd() const;
        int uRelEnd() const;

        int vAbsEnd() const;
        int vRelEnd() const;

        double uAbsToXc(int uAbs) const;
        double vAbsToYc(int vAbs) const;

        double uAbsToXc(double uAbs) const;
        double vAbsToYc(double vAbs) const;

        double uSampleToXc(double uAbs) const;
        double vSampleToYc(double vAbs) const;

        double uRelToXc(int uRel) const;
        double vRelToYc(int vRel) const;

        void setSampler(std::shared_ptr<Sampler> sampler);
        void setSamples(int n);

        int uRes_; ///< Resolution in u-dimension (horizontal).
        int vRes_; ///< Resolution in v-dimension (vertical).

        int uAbsRes_; ///< Resolution in u-dimension of original view plane
        int vAbsRes_; ///< Resolution in v-dimension of original view plane

        int uOffset_; ///< Pixel offset in u-dimension.
        int vOffset_; ///< Pixel offset in v-dimension.

        double s_; ///< Physical size of pixels in meters.

        std::shared_ptr<Sampler> sampler_; ///< Sampler object for antialiasing.
    };



    // inlined member functions

    /*! \brief Convert relative to absolute u-coordinate.
     *  \param[in] uRel Relative u-coordinate.
     *  \return Returns absolute u-coordinate.
     */
    inline int ViewPlane::uAbs(int uRel) const
    {
        return uRel + uOffset_;
    }



    /*! \brief Convert absolute to relative u-coordinate.
     *  \param[in] uAbs Absolute u-coordinate.
     *  \return Returns relative u-coordinate.
     */
    inline int ViewPlane::uRel(int uAbs) const
    {
        return uAbs - uOffset_;
    }



    /*! \brief Convert relative to absolute v-coordinate.
     *  \param[in] vRel Relative v-coordinate.
     *  \return Returns absolute v-coordinate.
     */
    inline int ViewPlane::vAbs(int vRel) const
    {
        return vRel + vOffset_;
    }



    /*! \brief Convert absolute to relative v-coordinate.
     *  \param[in] vAbs Absolute v-coordinate.
     *  \return Returns relative v-coordinate.
     */
    inline int ViewPlane::vRel(int vAbs) const
    {
        return vAbs - vOffset_;
    }



    /*! \brief Get absolute u-coordinate of first pixel in plane.
     *  \return Returns absolute u-coordinate.
     */
    inline int ViewPlane::uAbsBegin() const
    {
        return uOffset_;
    }



    /*! \brief Get relative u-coordinate of first pixel in plane.
     *  \return Returns relative u-coordinate.
     */
    inline int ViewPlane::uRelBegin() const
    {
        return 0;
    }



    /*! \brief Get absolute v-coordinate of first pixel in plane.
     *  \return Returns absolute v-coordinate.
     */
    inline int ViewPlane::vAbsBegin() const
    {
        return vOffset_;
    }



    /*! \brief Get relative v-coordinate of first pixel in plane.
     *  \return Returns relative v-coordinate.
     */
    inline int ViewPlane::vRelBegin() const
    {
        return 0;
    }



    /*! \brief Get absolute u-coordinate of the \a past-the-end pixel in plane.
     *  \return Returns absolute u-coordinate.
     */
    inline int ViewPlane::uAbsEnd() const
    {
        return uOffset_ + uRes_;
    }



    /*! \brief Get relative u-coordinate of the \a past-the-end pixel in plane.
     *  \return Returns relative u-coordinate.
     */
    inline int ViewPlane::uRelEnd() const
    {
        return uRes_;
    }



    /*! \brief Get absolute v-coordinate of the \a past-the-end pixel in plane.
     *  \return Returns absolute v-coordinate.
     */
    inline int ViewPlane::vAbsEnd() const
    {
        return vOffset_ + vRes_;
    }



    /*! \brief Get relative v-coordinate of the \a past-the-end pixel in plane.
     *  \return Returns relative v-coordinate.
     */
    inline int ViewPlane::vRelEnd() const
    {
        return vRes_;
    }



    /*! \brief Convert absolute horizontal pixel coordinate to horizontal camera coordinate.
     *  \param[in] uAbs Absolute pixel u-coordinate
     *  \return Horizontal camera x-coordinate.
     *
     *  This method returns the coordinates of the center of the pixel in camera coordinates, according
     *  to the image below.
     *  \image html pixel_frame_int.png
     */
    inline double ViewPlane::uAbsToXc(int uAbs) const
    {
        return s_ * (static_cast<float>(uAbs) - 0.5 * (uAbsRes_ - 1));
    }



    /*! \brief Convert absolute vertical pixel coordinate to vertical camera coordinate.
     *  \param[in] vAbs Absolute pixel v-coordinate
     *  \return Vertical camera y-coordinate.
     *
     *  This method returns the coordinates of the center of the pixel in camera coordinates, according
     *  to the image below.
     *  \image html pixel_frame_int.png
     */
    inline double ViewPlane::vAbsToYc(int vAbs) const
    {
        return s_ * (static_cast<float>(vAbs) - 0.5 * (vAbsRes_ - 1));
    }



    /*! \brief Convert absolute horizontal pixel coordinate to horizontal camera coordinate.
     *  \param[in] uAbs Absolute pixel u-coordinate
     *  \return Horizontal camera x-coordinate.
     *
     *  If the given coordinate is a whole number, this method returns the coordinates of the center of
     *  the pixel in camera coordinates, according to the image below.
     *  \image html pixel_frame_int.png
     *
     *  For sampling a pixels with offsets between 0 and 1 use uSampleToXc().
     */
    inline double ViewPlane::uAbsToXc(double uAbs) const
    {
        return s_ * (uAbs - 0.5 * (uAbsRes_ - 1));
    }



    /*! \brief Convert absolute vertical pixel coordinate to vertical camera coordinate.
     *  \param[in] vAbs Absolute pixel v-coordinate
     *  \return Vertical camera y-coordinate.
     *
     *  If the given coordinate is a whole number, this method returns the coordinates of the center of
     *  the pixel in camera coordinates, according to the image below.
     *  \image html pixel_frame_int.png
     *
     *  For sampling a pixels with offsets between 0 and 1 use vSampleToYc().
     */
    inline double ViewPlane::vAbsToYc(double vAbs) const
    {
        return s_ * (vAbs - 0.5 * (vAbsRes_ - 1));
    }



    /*! \brief Convert absolute horizontal pixel coordinate to horizontal camera coordinate.
     *  \param[in] uAbs Absolute pixel u-coordinate
     *  \return Horizontal camera x-coordinate.
     *
     *  If the given coordinate is a whole number, this method returns the coordinates of the upper left
     *  corner of the pixel in camera coordinates, according to the image below. This is useful for
     *  sampling a pixel with coordinate offsets between 0 and 1.
     *  \image html pixel_frame_sample.png
     *
     *  For sampling a pixels with offsets between -0.5 and 0.5 use uAbsToXc().
     */
    inline double ViewPlane::uSampleToXc(double uAbs) const
    {
        return s_ * (uAbs - 0.5 * uAbsRes_);
    }



    /*! \brief Convert absolute vertical pixel coordinate to vertical camera coordinate.
     *  \param[in] vAbs Absolute pixel u-coordinate
     *  \return Horizontal camera x-coordinate.
     *
     *  If the given coordinate is a whole number, this method returns the coordinates of the upper left
     *  corner of the pixel in camera coordinates, according to the image below. This is useful for
     *  sampling a pixel with coordinate offsets between 0 and 1.
     *  \image html pixel_frame_sample.png
     *
     *  For sampling a pixels with offsets between -0.5 and 0.5 use vAbsToYc().
     */
    inline double ViewPlane::vSampleToYc(double vAbs) const
    {
        return s_ * (vAbs - 0.5 * vAbsRes_);
    }



    /*! \brief Convert relative horizontal pixel coordinate to horizontal camera coordinate.
     *  \param[in] uRel Relative pixel u-coordinate
     *  \return Horizontal camera x-coordinate.
     */
    inline double ViewPlane::uRelToXc(int uRel) const
    {
        return s_ * (static_cast<float>(uRel + uOffset_) - 0.5 * (uAbsRes_ - 1));
    }



    /*! \brief Convert absolute vertical pixel coordinate to vertical camera coordinate.
     *  \param[in] vRel Relative pixel v-coordinate
     *  \return Vertical camera y-coordinate.
     */
    inline double ViewPlane::vRelToYc(int vRel) const
    {
        return s_ * (static_cast<float>(vRel + vOffset_) - 0.5 * (vAbsRes_ - 1));
    }



    /*! \brief Sets sampling object.
     *  \param[in] sampler Sampling object used for anti-aliasing.
     */
    inline void ViewPlane::setSampler(std::shared_ptr<Sampler> sampler)
    {
        sampler_ = sampler;
    }
        
} // end of namespace iiit

#endif // __VIEWPLANE_HPP__