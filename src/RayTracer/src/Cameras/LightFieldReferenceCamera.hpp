// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file LightFieldReferenceCamera.hpp
 *  \author Maximilian Schambach und David Uhlig
 *  \date 01.11.2017
 *  \brief Definition of class LightFieldReference.
 *
 *  Definition of class LightFieldReference representing light field reference camera.
 */

#ifndef __LIGHTFIELDREFERENCE_HPP__
#define __LIGHTFIELDREFERENCE_HPP__

#include <memory>
#include <limits>

#include "CImg.h"

#include "Scene.hpp"
#include "Camera.hpp"
#include "Sensor.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Point2D.hpp"
#include "Utilities/Constants.h"
#include "Sampler/Sampler.hpp"

//#include "Logger.hpp"



namespace iiit
{

    /*! \brief Class representing a reference light field camera.
     *
     *
     *
     */
    template <class SpectrumType>
    class LightFieldReference : public Camera<SpectrumType>
    {
    public:
        LightFieldReference(std::weak_ptr<const Scene<SpectrumType> > scene,
                            const Point3D& eye,
                            const Point3D& lookAt,
                            const Vector3D& up,
                            const Sensor<SpectrumType>& sensor,
                            float lensRadius,
                            float microLensRadius,
                            float subapertureRadius,
                            float imageDistance,
                            float focusDistance,
                            int numULenses,
                            int numVLenses,
                            int numAngsStep);
        LightFieldReference(const LightFieldReference& other);
        ~LightFieldReference();

        Camera<SpectrumType>* clone() const;
        virtual void renderScene(std::shared_ptr<cimg_library::CImg<float> > image);
        CameraType::Type getType() const;

        float getLensRadius() const;
        float getImageDistance() const;
        float getFocusDistance() const;

        void setLensSampler(std::shared_ptr<Sampler> sampler);
        void setVirtualSensorSampler(std::shared_ptr<Sampler> sampler);

//        std::shared_ptr<const Sampler> getSampler() const;  // needed??

    protected:
        Vector3D rayDirection(const Point2D& pixelPoint, const Point2D& lensPoint) const;
        Vector3D rayDirection(const Point3D& pixelPoint, const Point3D& lensPoint) const;
        Point2D hitXYPlaneAt(double z, const Ray& ray) const;

        float lensRadius_; ///< Radius of main lens in meters.
        float microLensRadius_; ///< Radius of microlenses of the corresponding lightfieldcamera in meters. This is equivalent to the virtual sensor pixel size
        float subapertureRadius_; ///< Radius of the subaperture. Usually, defined by the main lens radius and numAngSteps value.
		float sLens_; ///< Pixel size of the main lens raster
        int numULenses_; ///< Number of corresponding microlenses in u-direction.
        int numVLenses_; ///< Number of corresponding microlenses in v-direction.
        int numAngSteps_; ///< Number of angular sampling steps of the main lens. Corresponds the number of pixel underneath one microlens in one direction.
        float imageDistance_; ///< Distance between lens and image sensor in meters.
        float focusDistance_; ///< Distance between lens and point in perfect focus in meters.
        std::shared_ptr<Sampler> lensSampler_; ///< Sampling object for sampling the lens.
        std::shared_ptr<Sampler> virtualSensorSampler_; ///< Sampling object for sampling the virtual sensor plane.
    };



    /*! \brief Constructs camera and initializes coordinate system with look-at-transformation.
     *  \param[in] scene Scene to render.
     *  \param[in] eye Point in world coordinates defining the origin of the camera coordinate system.
     *  \param[in] lookAt Point in world coordinates that the camera looks at.
     *  \param[in] up Vector in world coordinates pointing in the up direction of the camera.
     *  \param[in] sensor Sensor object.
     *  \param[in] lensRadius Radius of main lens.
     *  \param[in] microLensRadius Radius of the microlenses of the corresponding lightfield camera. Is equivalent to the virtual sensor's pixel size.
     *  \param[in] imageDistance Distance between lens and sensor.
     *  \param[in] focusDistance Distance of point in focus.
     *  \param[in] numULenses Number of microlenses in u-direction. Corresponds to number of pixels of the virtual sensor in u-direction.
     *  \param[in] numVLenses Number of microlenses in v-direction. Corresponds to number of pixels of the virtual sensor in v-direction.
     *  \param[in] numAngSteps Number of pixels to sample the main lens with per dimension. Corresponds the number of pixels underneath each microlens in one direction.
     */
    template <class SpectrumType>
    LightFieldReference<SpectrumType>::LightFieldReference(
            std::weak_ptr<const Scene<SpectrumType> > scene,
            const Point3D& eye,
            const Point3D& lookAt,
            const Vector3D& up,
            const Sensor<SpectrumType>& sensor,
            float lensRadius,
            float microLensRadius,
            float subapertureRadius,
            float imageDistance,
            float focusDistance,
            int numULenses,
            int numVLenses,
            int numAngSteps)
        : Camera<SpectrumType>(scene, eye, lookAt, up, sensor)
        , lensRadius_(lensRadius)
        , microLensRadius_(microLensRadius)
        , subapertureRadius_(subapertureRadius)
        , sLens_(2 * lensRadius / float(numAngSteps))
        , numULenses_(numULenses)
        , numVLenses_(numVLenses)
        , numAngSteps_(numAngSteps)
        , imageDistance_(imageDistance)
        , focusDistance_(focusDistance)
    {
    }



    /*! \brief Copy constructor.
     *  \param[in] other Camera to copy.
     */
    template <class SpectrumType>
    LightFieldReference<SpectrumType>::LightFieldReference(const LightFieldReference& other)
        : Camera<SpectrumType>(other)
        , lensRadius_(other.lensRadius_)
        , microLensRadius_(other.microLensRadius_)
        , subapertureRadius_(other.subapertureRadius_)
        , sLens_(other.sLens_)
        , numULenses_(other.numULenses_)
        , numVLenses_(other.numVLenses_)
        , numAngSteps_(other.numAngSteps_)
        , imageDistance_(other.imageDistance_)
        , focusDistance_(other.focusDistance_)
    {
        this->setLensSampler(std::shared_ptr<Sampler>(other.lensSampler_->clone()));
        this->setVirtualSensorSampler(std::shared_ptr<Sampler>(other.virtualSensorSampler_->clone()));
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    LightFieldReference<SpectrumType>::~LightFieldReference()
    {
    }



    /*! \brief Clone this camera.
     *  \return Cloned camera.
     */
    template <class SpectrumType>
    Camera<SpectrumType>* LightFieldReference<SpectrumType>::clone() const
    {
        LightFieldReference<SpectrumType>* camera = new LightFieldReference<SpectrumType>(*this);
        return camera;
    }



    /*! \brief Sets sampling object for main Lens.
     *  \param[in] sampler Sampling object used for sampling the main lens.
     */
    template <class SpectrumType>
    void LightFieldReference<SpectrumType>::setLensSampler(std::shared_ptr<Sampler> sampler)
    {
        lensSampler_ = sampler;
    }

    /*! \brief Sets sampling object for virtual sensor plane.
     *  \param[in] sampler Sampling object used for sampling the virtual sensor plane.
     */
    template <class SpectrumType>
    void LightFieldReference<SpectrumType>::setVirtualSensorSampler(std::shared_ptr<Sampler> sampler)
    {
        virtualSensorSampler_ = sampler;
    }

    /*! \brief  Compute direction of ray that starts at the lens and goes through a constructed point on
     *  the focal plane.
     *  \param[in] pixelPoint 2D Point on pixel.
     *  \param[in] lensPoint 2D Point on lens.
     *  \return Ray direction as Vector3D
     *
     *  The constructed Point is calculated through similar triangle equations, given a PixelPoint [on
     *  the viewPlane], the focusDistance and the imageDistance
     */
    template <class SpectrumType>
    Vector3D LightFieldReference<SpectrumType>::rayDirection(const Point2D& pixelPoint, const Point2D& lensPoint) const
    {
        Point2D focalPoint; // Point on the focal plane
        focalPoint.x_ = pixelPoint.x_ * (focusDistance_ / imageDistance_);
        focalPoint.y_ = pixelPoint.y_ * (focusDistance_ / imageDistance_);
        
        Vector3D dir(focalPoint.x_ - lensPoint.x_, focalPoint.y_ - lensPoint.y_ , focusDistance_);
        dir.normalize();
        
        return(dir);
    }



    /*! \brief  Compute direction of ray between 2 points in 3D.
     *  \param[in] startPoint 3D starting point.
     *  \param[in] endPoint 3D end point.
     *  \return Ray direction as Vector3D
     */
    template <class SpectrumType>
    Vector3D LightFieldReference<SpectrumType>::rayDirection(const Point3D& startPoint, const Point3D& endPoint) const
    {
        return Vector3D(endPoint.x_ - startPoint.x_,
            endPoint.y_ - startPoint.y_,
            endPoint.z_ - startPoint.z_);
    }



    /*! \brief Renders the scene as seen by the camera.
     *  \param[out] image Rendered image.
     */
    template <class SpectrumType>
    void LightFieldReference<SpectrumType>::renderScene(std::shared_ptr<cimg_library::CImg<float> > image)
    {
                
        std::shared_ptr<const Scene<SpectrumType> > scene = this->scene_.lock();

        // Init (x,y,s,t) pixel coordinates
        int x,y,s,t;

        //iterate over pixels
        for (int uAbs = scene->viewPlane_.uAbsBegin(); uAbs < scene->viewPlane_.uAbsEnd() ; ++uAbs)
        {
            for (int vAbs = scene->viewPlane_.vAbsBegin(); vAbs < scene->viewPlane_.vAbsEnd() ; ++vAbs)
            {
                // reset sampling framework to start a new pixel
                scene->getTracer().lock()->startNewPixel();

                // set initial pixel color black
                SpectrumType color(0.f);

                // Calculate corresponding lens and virtual sensor pixel coordinate
                // (u, v) -> (x,y,s,t)

                // Spatial coordinates
                s = uAbs % numULenses_;
                t = vAbs % numVLenses_;
                // Angular coordinates
                x = (uAbs - s)/numULenses_;
                y = (vAbs - t)/numVLenses_;

                // sample pixel

                for (int n = 0; n < virtualSensorSampler_->getNumSamples(); ++n)
                {
                    // compute pixel point in camera coordinates (u, v -> x_c, y_c)
                    Point2D pixelPoint = virtualSensorSampler_->sampleUnitSquare();

                    pixelPoint.x_ = (2 * microLensRadius_) * ((s + pixelPoint.x_) - (0.5 * numULenses_));
                    pixelPoint.y_ = (2 * microLensRadius_) * ((t + pixelPoint.y_) - (0.5 * numVLenses_));

                    // compute lens point in camera coordinates
                    Point2D lensPoint = 2*(lensSampler_->sampleUnitSquare()) - Point2D(1, 1);
                    lensPoint.x_ = sLens_*(x + 0.5 - (0.5 * numAngSteps_)) + subapertureRadius_*lensPoint.x_;
                    lensPoint.y_ = sLens_ * (y + 0.5 - (0.5 * numAngSteps_)) + subapertureRadius_*lensPoint.y_;

                    // compute image side ray
                    Ray imageSideRay(Point3D(pixelPoint, -imageDistance_), Point3D(lensPoint, 0.0) - Point3D(pixelPoint, -imageDistance_));

                    // check if ray passes the image side apertures
                    float attenuation = this->apertureAttenuation(imageSideRay, RayDomain::ImageSide);

                    // the attenuation describes how the ray's intensity is modified (multiplicatively). Therefore, if the
                    // attenuation IS NOT 0.0, the ray "passes" through the aperture
                    if (attenuation != 0.f)
                    {

                        // construct primary ray, that gets imaged on the pixel point
                        Ray ray(Point3D(lensPoint.x_, lensPoint.y_, 0), rayDirection(pixelPoint, lensPoint));

                        // check if ray passes the object side apertures
                        attenuation = attenuation * this->apertureAttenuation(ray, RayDomain::ObjectSide);

                        if (attenuation != 0.f)
                        {
                            // calculate natural vignetting
                            float vignetting = this->naturalVignetting(imageSideRay);

                            // transform primary ray to world coordinates
                            ray = this->transformation_.camToWorld(ray);//ray goes positively into the world

                            // calculate interaction of ray with the scene, include vignetting and apertures
                            color += (scene->getTracer().lock()->traceRay(ray, 0)) * vignetting * attenuation;
                        }
                    }
                }

                color /= static_cast<float>(virtualSensorSampler_->getNumSamples());
                this->setPixel(scene->viewPlane_.uRel(uAbs), scene->viewPlane_.vRel(vAbs), color, image);
            }
        }
    }


    /*! \brief This function calculates the intersection point of a given ray with a XY Plane at
     *  Position z.
     *  \param[in] z z-Coordinate of XY plane that shall be hit.
     *  \param[in] ray Ray that shall intersect the XY Plane.
     */
    template <class SpectrumType>
    Point2D LightFieldReference<SpectrumType>::hitXYPlaneAt(double z, const Ray& ray) const
    {
        Point2D hitPoint(ray.origin_.x_ + (ray.direction_.x_ * z) / ray.direction_.z_,
                         ray.origin_.y_ + (ray.direction_.y_ * z) / ray.direction_.z_);
        return hitPoint;
    }



    /*! \brief Get the type of the camera.
     *  \return Returns the type of the camera.
     */
    template <class SpectrumType>
    CameraType::Type LightFieldReference<SpectrumType>::getType() const
    {
        return CameraType::Type::LightFieldReferenceCamera;
    }



    /*! \brief Get lens radius.
     *  \return Lens radius in meters.
     */
    template <class SpectrumType>
    float LightFieldReference<SpectrumType>::getLensRadius() const
    {
        return lensRadius_;
    }



    /*! \brief Get image distance.
     *  \return Distance between lens and image sensor in meters.
     */
    template <class SpectrumType>
    float LightFieldReference<SpectrumType>::getImageDistance() const
    {
        return imageDistance_;
    }



    /*! \brief Get focus distance.
     *  \return Distance between lens and point in perfect focus in meters.
     */
    template <class SpectrumType>
    float LightFieldReference<SpectrumType>::getFocusDistance() const
    {
        return focusDistance_;
    }



    /*! \brief Get sampling object.
     *  \return Pointer to sampling object.
     */
//    template <class SpectrumType>
//    std::shared_ptr<const Sampler> LightFieldReference<SpectrumType>::getSampler() const
//    {
//        return std::shared_ptr<const Sampler>(sampler_);
//    }

} // end of namespace iiit

#endif // __LightFieldReference_HPP__
