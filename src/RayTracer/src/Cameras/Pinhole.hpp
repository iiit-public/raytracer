// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Pinhole.hpp
 *  \author Mohamed Salem Koubaa
 *  \date 22.03.2015
 *  \brief Definition of class Pinhole.
 *
 *  Definition of class Pinhole representing a derived class of camera where the rays start from the
 *  camera's eye
 */

#ifndef __PINHOLE_HPP__
#define __PINHOLE_HPP__

#include <memory>

#include "CImg.h"

#include "Scene.hpp"
#include "Camera.hpp"
#include "Sensor.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"



namespace iiit
{

    /*! \brief Class representing a pinhole camera.
     */
    template <class SpectrumType>
    class Pinhole : public Camera<SpectrumType>
    {
    public:
        Pinhole(std::weak_ptr<const Scene<SpectrumType> > scene, Point3D eye, Point3D lookAt, Vector3D up,
            const Sensor<SpectrumType>& sensor, double focalLength);
        Pinhole(const Pinhole& other);

        virtual Camera<SpectrumType>* clone() const;
        virtual void renderScene(std::shared_ptr<cimg_library::CImg<float> > image);
        virtual CameraType::Type getType() const;

        double getFocalLength() const;
        void setFocalLength(double focalLength);

    private:
        double focalLength_; ///< Focal length, i. e. distance between hole and image plane, of the camera.
    };



    /*! \brief Constructs camera and initializes coordinate system with look-at-transformation.
     *  \param[in] scene Scene to render.
     *  \param[in] eye Point in world coordinates defining the origin of the camera coordinate system.
     *  \param[in] lookAt Point in world coordinates that the camera looks at.
     *  \param[in] up Vector in world coordinates pointing in the up direction of the camera.
     *  \param[in] sensor Sensor object.
     *  \param[in] focalLength Focal length of camera.
     */
    template <class SpectrumType>
    Pinhole<SpectrumType>::Pinhole(std::weak_ptr<const Scene<SpectrumType> > scene, Point3D eye, Point3D lookAt, Vector3D up,
        const Sensor<SpectrumType>& sensor, double focalLength)
        : Camera<SpectrumType>(scene, eye, lookAt, up, sensor)
        , focalLength_(focalLength)
    {
    }



    /*! \brief Copy constructor.
     *  \param[in] other Camera to copy.
     */
    template <class SpectrumType>
    Pinhole<SpectrumType>::Pinhole(const Pinhole& other)
        : Camera<SpectrumType>(other)
        , focalLength_(other.focalLength_)
    {
    }



    /*! \brief Clone this camera.
     *  \return Cloned camera.
     */
    template <class SpectrumType>
    Camera<SpectrumType>* Pinhole<SpectrumType>::clone() const
    {
        return new Pinhole<SpectrumType>(*this);
    }



    /*! \brief Renders the scene as seen by the camera.
     *  \param[out] image Rendered image.
     */
    template <class SpectrumType>
    void Pinhole<SpectrumType>::renderScene(std::shared_ptr<cimg_library::CImg<float> > image)
    {
        std::shared_ptr<const Scene<SpectrumType> > scene = this->scene_.lock();
        
        // iterate over pixels
        for (int uAbs = scene->viewPlane_.uAbsBegin(); uAbs < scene->viewPlane_.uAbsEnd(); ++uAbs)
        {
            for (int vAbs = scene->viewPlane_.vAbsBegin(); vAbs < scene->viewPlane_.vAbsEnd(); ++vAbs)
            {
                // reset sampling framework to start a new pixel
                scene->getTracer().lock()->startNewPixel();

                // set initial pixel color black
                SpectrumType color(0.f);
                
                // sample pixel
                for (int n = 0; n < scene->viewPlane_.sampler_->getNumSamples(); ++n)
                {
                    // compute pixel point in camera coordinates (u, v -> x_c, y_c)
                    Point2D offset = scene->viewPlane_.sampler_->sampleUnitSquare();
                    offset.x_ = scene->viewPlane_.uSampleToXc(uAbs + offset.x_);
                    offset.y_ = scene->viewPlane_.vSampleToYc(vAbs + offset.y_);
                    
                    // ray direction in camera coordinates
                    Vector3D direction(offset.x_, offset.y_, focalLength_);
                    direction.normalize();

                    // ray in camera coordinates
                    Ray ray(Point3D(0.0, 0.0, 1.0), direction);

                    // check if ray passes the apertures
                    float attenuation = this->apertureAttenuation(ray, RayDomain::ImageSide);
                    attenuation = attenuation * this->apertureAttenuation(ray, RayDomain::ObjectSide);

                    // calculate natural vignetting
                    float vignetting = this->naturalVignetting(ray);

                    // the attenuation describes how the ray's intensity is modified (multiplicatively). Therefore, if the
                    // attenuation IS NOT 0.0, the ray "passes" through the aperture
                    if (attenuation != 0.f)
                    {
                        // transform ray direction to world coordinates
                        direction = Camera<SpectrumType>::transformation_.camToWorld(direction);

                        // ray in world coordinates
                        Ray ray(Camera<SpectrumType>::eye_, direction);

                        // calculate interaction of ray with the scene
                        color += scene->getTracer().lock()->traceRay(ray, 0) * attenuation * vignetting;
                    }
                }
                
                // compute mean pixel color
                color /= static_cast<float>(scene->viewPlane_.sampler_->getNumSamples());
                this->setPixel(scene->viewPlane_.uRel(uAbs), scene->viewPlane_.vRel(vAbs), color, image);
            }
        }
    }



    /*! \brief Get the type of the camera.
     *  \return Returns the type of the camera.
     */
    template <class SpectrumType>
    CameraType::Type Pinhole<SpectrumType>::getType() const
    {
        return CameraType::Type::Pinhole;
    }



    /*! \brief Get focal length of camera.
     *  \return Returns focal length of camera.
     */
    template <class SpectrumType>
    double Pinhole<SpectrumType>::getFocalLength() const
    {
        return focalLength_;
    }



    /*! \brief Set focal length of camera.
     *  \param[in] focalLength Focal length of camera.
     */
    template <class SpectrumType>
    void Pinhole<SpectrumType>::setFocalLength(double focalLength)
    {
        focalLength_ = focalLength;
    }
        
} // end of namespace iiit

#endif //__PINHOLE_HPP__
