// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file ThinLens.hpp
 *  \author Mohamed Salem Koubaa
 *  \date 22.04.2015
 *  \brief Definition of class ThinLens.
 *
 *  Definition of class ThinLens representing a camera with an ideal thin lens.
 */

#ifndef __THINLENS_HPP__
#define __THINLENS_HPP__

#include <memory>
#include <limits>

#include "CImg.h"

#include "Scene.hpp"
#include "Camera.hpp"
#include "Sensor.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Point2D.hpp"
#include "Utilities/Constants.h"
#include "Sampler/Sampler.hpp"



namespace iiit
{

    /*! \brief Class representing camera with thin lens with circular aperture.
     *
     *  The camera frame is a cartesian coordinate system with the axes relative to the camera defined
     *  as pictured below. The z-axis points in the viewing direction, while the x and y-axes span the
     *  image plane, forming a right-handed system.
     *  \image html camera_frame.png
     *
     *  The world frame is cartesian coordinate system as depicted in the image below. The x and y-axis
     *  span the ground plane and the z-axis points straight upwards to form a right handed system.
     *  \image html world_frame.png
     *
     */
    template <class SpectrumType>
    class ThinLens : public Camera<SpectrumType>
    {
    public:
        ThinLens(std::weak_ptr<const Scene<SpectrumType> > scene, const Point3D& eye, const Point3D& lookAt, const Vector3D& up,
            const Sensor<SpectrumType>& sensor, float lensRadius, float imageDistance, float focusDistance);
        ThinLens(const ThinLens& other);
        ~ThinLens();

        Camera<SpectrumType>* clone() const;
        virtual void renderScene(std::shared_ptr<cimg_library::CImg<float> > image);
        CameraType::Type getType() const;

        float getLensRadius() const;
        float getImageDistance() const;
        float getFocusDistance() const;
        float getZoom() const;

        void setSampler(std::shared_ptr<Sampler> sampler);
        std::shared_ptr<const Sampler> getSampler() const;

    protected:
        Vector3D rayDirection(const Point2D& pixelPoint, const Point2D& lensPoint) const;
        Vector3D rayDirection(const Point3D& pixelPoint, const Point3D& lensPoint) const;

        float lensRadius_; ///< Radius of lens in meters.
        float imageDistance_; ///< Distance between lens and image sensor in meters.
        float focusDistance_; ///< Distance between lens and point in perfect focus in meters.
        float zoom_; ///< zoom factor
        std::shared_ptr<Sampler> sampler_; ///< Sampling object for sampling the lens.
    };



    /*! \brief Constructs camera and initializes coordinate system with look-at-transformation.
     *  \param[in] scene Scene to render.
     *  \param[in] eye Point in world coordinates defining the origin of the camera coordinate system.
     *  \param[in] lookAt Point in world coordinates that the camera looks at.
     *  \param[in] up Vector in world coordinates pointing in the up direction of the camera.
     *  \param[in] sensor Sensor object.
     *  \param[in] lensRadius Radius of lens.
     *  \param[in] imageDistance Distance between lens and sensor.
     *  \param[in] focusDistance Distance of point in focus.
     */
    template <class SpectrumType>
    ThinLens<SpectrumType>::ThinLens(std::weak_ptr<const Scene<SpectrumType> > scene, const Point3D& eye, const Point3D& lookAt,
        const Vector3D& up, const Sensor<SpectrumType>& sensor, float lensRadius, float imageDistance, float focusDistance)
        : Camera<SpectrumType>(scene, eye, lookAt, up, sensor)
        , lensRadius_(lensRadius)
        , imageDistance_(imageDistance)
        , focusDistance_(focusDistance)
    {
        zoom_ = 1.0; ///< \todo Implement zoom interface
    }



    /*! \brief Copy constructor.
     *  \param[in] other Camera to copy.
     */
    template <class SpectrumType>
    ThinLens<SpectrumType>::ThinLens(const ThinLens& other)
        : Camera<SpectrumType>(other)
        , lensRadius_(other.lensRadius_)
        , imageDistance_(other.imageDistance_)
        , focusDistance_(other.focusDistance_)
    {
        this->setSampler(std::shared_ptr<Sampler>(other.sampler_->clone()));
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    ThinLens<SpectrumType>::~ThinLens()
    {
    }



    /*! \brief Clone this camera.
     *  \return Cloned camera.
     */
    template <class SpectrumType>
    Camera<SpectrumType>* ThinLens<SpectrumType>::clone() const
    {
        ThinLens<SpectrumType>* camera = new ThinLens<SpectrumType>(*this);
        return camera;
    }



    /*! \brief Sets sampling object.
     *  \param[in] sampler Sampling object used for sampling the lens.
     */
    template <class SpectrumType>
    void ThinLens<SpectrumType>::setSampler(std::shared_ptr<Sampler> sampler)
    {
        sampler_ = sampler;
    }



    /*! \brief  Compute direction of ray that starts at the lens and goes through a constructed point on
     *  the focal plane.
     *  \param[in] pixelPoint 2D Point on pixel.
     *  \param[in] lensPoint 2D Point on lens.
     *  \return Ray direction as Vector3D
     *
     *  The constructed Point is calculated through similar triangle equations, given a PixelPoint [on
     *  the viewPlane], the focusDistance and the imageDistance
     */
    template <class SpectrumType>
    Vector3D ThinLens<SpectrumType>::rayDirection(const Point2D& pixelPoint, const Point2D& lensPoint) const
    {
        Point2D focalPoint; // Point on the focal plane
        focalPoint.x_ = pixelPoint.x_ * (focusDistance_ / imageDistance_);
        focalPoint.y_ = pixelPoint.y_ * (focusDistance_ / imageDistance_);
        
        Vector3D dir(focalPoint.x_ - lensPoint.x_, focalPoint.y_ - lensPoint.y_ , focusDistance_);
        dir.normalize();
        
        return(dir);
    }



    /*! \brief  Compute direction of ray between 2 points in 3D.
     *  \param[in] startPoint 3D starting point.
     *  \param[in] endPoint 3D end point.
     *  \return Ray direction as Vector3D
     */
    template <class SpectrumType>
    Vector3D ThinLens<SpectrumType>::rayDirection(const Point3D& startPoint, const Point3D& endPoint) const
    {
        return Vector3D(endPoint.x_ - startPoint.x_,
            endPoint.y_ - startPoint.y_,
            endPoint.z_ - startPoint.z_);
    }



    /*! \brief Renders the scene as seen by the camera.
     *  \param[out] image Rendered image.
     */
    template <class SpectrumType>
    void ThinLens<SpectrumType>::renderScene(std::shared_ptr<cimg_library::CImg<float> > image)
    {
        
        //// evaluate zoom parameter
        //scene.viewPlane_.s_ /= zoom_;
        
        std::shared_ptr<const Scene<SpectrumType> > scene = this->scene_.lock();
        
        //iterate over pixels
        for (int uAbs = scene->viewPlane_.uAbsBegin(); uAbs < scene->viewPlane_.uAbsEnd() ; ++uAbs)
        {
            for (int vAbs = scene->viewPlane_.vAbsBegin(); vAbs < scene->viewPlane_.vAbsEnd() ; ++vAbs)
            {
                // reset sampling framework to start a new pixel
                scene->getTracer().lock()->startNewPixel();

                // set initial pixel color black
                SpectrumType color(0.f);
                
                // sample pixel
                for (int n = 0; n < scene->viewPlane_.sampler_->getNumSamples(); ++n)
                {
                    // compute pixel point in camera coordinates (u, v -> x_c, y_c)
                    Point2D pixelPoint = scene->viewPlane_.sampler_->sampleUnitSquare();
                    pixelPoint.x_ = scene->viewPlane_.uSampleToXc(uAbs + pixelPoint.x_);
                    pixelPoint.y_ = scene->viewPlane_.vSampleToYc(vAbs + pixelPoint.y_);
                    
                    // compute lens point in camera coordinates
                    Point2D lensPoint = sampler_->sampleUnitDisk();
                    lensPoint = lensPoint * lensRadius_;

                    // compute image side ray
                    Point3D pixelPoint3D(pixelPoint.x_, pixelPoint.y_, -imageDistance_);
                    Ray imageSideRay(pixelPoint3D, Point3D(lensPoint.x_, lensPoint.y_, 0.0) - pixelPoint3D);

                    // check if ray passes the image side apertures
                    float attenuation = this->apertureAttenuation(imageSideRay, RayDomain::ImageSide);

                    // Calculate natural vignetting
                    float vignetting = this->naturalVignetting(imageSideRay);

                    // the attenuation describes how the ray's intensity is modified (multiplicatively). Therefore, if the
                    // attenuation IS NOT 0.0, the ray "passes" through the aperture
                    if (attenuation != 0.f)
                    {
                        // construct primary ray, that gets imaged on the pixel point
                        Ray ray(Point3D(lensPoint.x_, lensPoint.y_, 0), rayDirection(pixelPoint, lensPoint));

                        // check if ray passes the object side apertures
                        attenuation = attenuation * this->apertureAttenuation(ray, RayDomain::ObjectSide);

                        if (attenuation != 0.f)
                        {
                            // transform primary ray to world coordinates
                            ray = this->transformation_.camToWorld(ray);//ray goes positively into the world

                            // calculate interaction of ray with the scene
                            color += scene->getTracer().lock()->traceRay(ray, 0) * attenuation * vignetting;
                        }
                    }
                }
                
                // compute mean pixel color
                color /=  static_cast<float>(scene->viewPlane_.sampler_->getNumSamples());
                this->setPixel(scene->viewPlane_.uRel(uAbs), scene->viewPlane_.vRel(vAbs), color, image);
            }
        }
    }



    /*! \brief Get the type of the camera.
     *  \return Returns the type of the camera.
     */
    template <class SpectrumType>
    CameraType::Type ThinLens<SpectrumType>::getType() const
    {
        return CameraType::Type::ThinLens;
    }



    /*! \brief Get lens radius.
     *  \return Lens radius in meters.
     */
    template <class SpectrumType>
    float ThinLens<SpectrumType>::getLensRadius() const
    {
        return lensRadius_;
    }



    /*! \brief Get image distance.
     *  \return Distance between lens and image sensor in meters.
     */
    template <class SpectrumType>
    float ThinLens<SpectrumType>::getImageDistance() const
    {
        return imageDistance_;
    }



    /*! \brief Get focus distance.
     *  \return Distance between lens and point in perfect focus in meters.
     */
    template <class SpectrumType>
    float ThinLens<SpectrumType>::getFocusDistance() const
    {
        return focusDistance_;
    }



    /*! \brief Get object distance.
     *  \return Distance between lens and point in perfect focus in meters.
     */
    template <class SpectrumType>
    float ThinLens<SpectrumType>::getZoom() const
    {
        return zoom_;
    }



    /*! \brief Get sampling object.
     *  \return Pointer to sampling object.
     */
    template <class SpectrumType>
    std::shared_ptr<const Sampler> ThinLens<SpectrumType>::getSampler() const
    {
        return std::shared_ptr<const Sampler>(sampler_);
    }

} // end of namespace iiit

#endif // __THINLENS_HPP__
