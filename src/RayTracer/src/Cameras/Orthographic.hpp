// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Orthographic.hpp
 *  \author Mohamed Salem Koubaa
 *  \date 16.03.2015
 *  \brief Definition of class Orthographic.
 *
 *  Definition of class Orthographic representing a derived class of camera where the rays are
 *  parallel.
 */

#ifndef __ORTHOGRAPHIC_HPP__
#define __ORTHOGRAPHIC_HPP__

#include <memory>

#include "CImg.h"

#include "Scene.hpp"
#include "Camera.hpp"
#include "Sensor.hpp"
#include "Utilities/Point3D.hpp"
#include "Utilities/Vector3D.hpp"



class Scene; // forward declaration

namespace iiit
{

    /*! \brief Class represents an orthographic cameras for parallel projection.
     */
    template <class SpectrumType>
    class Orthographic : public Camera<SpectrumType>
    {
    public:
        Orthographic(std::weak_ptr<const Scene<SpectrumType> > scene, Point3D eye, Point3D lookAt, Vector3D up, const Sensor<SpectrumType>& sensor);
        Orthographic(const Orthographic& other);

        virtual Camera<SpectrumType>* clone() const;
        virtual void renderScene(std::shared_ptr<cimg_library::CImg<float> > image);
        virtual CameraType::Type getType() const;
    };



    /*! \brief Constructs camera and initializes coordinate system with look-at-transformation.
     *  \param[in] scene Scene to render.
     *  \param[in] eye Point in world coordinates defining the origin of the camera coordinate system.
     *  \param[in] lookAt Point in world coordinates that the camera looks at.
     *  \param[in] up Vector in world coordinates pointing in the up direction of the camera.
     *  \param[in] sensor Sensor object.
     */
    template <class SpectrumType>
    Orthographic<SpectrumType>::Orthographic(std::weak_ptr<const Scene<SpectrumType> > scene, Point3D eye, Point3D lookAt, Vector3D up,
        const Sensor<SpectrumType>& sensor)
        : Camera<SpectrumType>(scene, eye, lookAt, up, sensor)
    {
    }



    /*! \brief Copy constructor.
     *  \param[in] other Camera to copy.
     */
    template <class SpectrumType>
    Orthographic<SpectrumType>::Orthographic(const Orthographic& other)
        : Camera<SpectrumType>(other)
    {
    }



    /*! \brief Clone this orthographic camera.
     *  \return Cloned camera.
     */
    template <class SpectrumType>
    Camera<SpectrumType>* Orthographic<SpectrumType>::clone() const
    {
        return new Orthographic<SpectrumType>(*this);
    }



    /*! \brief Renders the scene as seen by the camera.
     *  \param[out] image Rendered image.
     */
    template <class SpectrumType>
    void Orthographic<SpectrumType>::renderScene(std::shared_ptr<cimg_library::CImg<float> > image)
    {
        std::shared_ptr<const Scene<SpectrumType> > scene = this->scene_.lock();
        
        // iterate over pixels
        for (int uAbs = scene->viewPlane_.uAbsBegin(); uAbs < scene->viewPlane_.uAbsEnd(); ++uAbs)
        {
            for (int vAbs = scene->viewPlane_.vAbsBegin(); vAbs < scene->viewPlane_.vAbsEnd(); ++vAbs)
            {
                // reset sampling framework to start a new pixel
                scene->getTracer().lock()->startNewPixel();

                // set initial pixel color black
                SpectrumType color(0.f);
                
                // sample pixel
                for (int n = 0; n < scene->viewPlane_.sampler_->getNumSamples(); ++n)
                {
                    // compute pixel point in camera coordinates (u, v -> x_c, y_c)
                    Point2D pixelPoint = scene->viewPlane_.sampler_->sampleUnitSquare();
                    pixelPoint.x_ = scene->viewPlane_.uSampleToXc(uAbs + pixelPoint.x_);
                    pixelPoint.y_ = scene->viewPlane_.vSampleToYc(vAbs + pixelPoint.y_);
                    
                    // ray in camera coordinates
                    Ray ray(Point3D(pixelPoint.x_, pixelPoint.y_, 0.0), Vector3D(0.0, 0.0, 1.0));

                    // check if ray passes the apertures
                    float attenuation = this->apertureAttenuation(ray, RayDomain::ImageSide);
                    attenuation = attenuation * this->apertureAttenuation(ray, RayDomain::ObjectSide);

                    // the attenuation describes how the ray's intensity is modified (multiplicatively). Therefore, if the
                    // attenuation IS NOT 0.0, the ray "passes" through the aperture
                    if (attenuation != 0.f)
                    {
                        // transform ray to world coordinates
                        ray = Camera<SpectrumType>::transformation_.camToWorld(ray);

                        // calculate interaction of ray with the scene
                        color += scene->getTracer().lock()->traceRay(ray, 0) * attenuation;
                    }
                }
                
                // compute mean pixel color
                color /= static_cast<float>(scene->viewPlane_.sampler_->getNumSamples());
                this->setPixel(scene->viewPlane_.uRel(uAbs), scene->viewPlane_.vRel(vAbs), color, image);
            }
        }
    }



    /*! \brief Get the type of the camera.
     *  \return Returns the type of the camera.
     */
    template <class SpectrumType>
    CameraType::Type Orthographic<SpectrumType>::getType() const
    {
        return CameraType::Type::Orthographic;
    }
    
} // end of namespace iiit


#endif // __ORTHOGRAPHIC_HPP__
