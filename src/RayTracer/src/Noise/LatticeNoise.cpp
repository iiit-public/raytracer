// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file LatticeNoise.cpp
 *  \author Christian Zimmermann
 *  \date 28.05.2015
 *  \brief Implementation of class LatticeNoise.
 *
 *  Implementation of class LatticeNoise representing a generic noise model.
 */


#include "Noise/LatticeNoise.hpp"



namespace iiit
{

    const unsigned char LatticeNoise::permutationTable_[256] = {
        225,    155,    210,    108,    175,    199,    221,    144,    203,    116,    70,     213,    69,     158,    33,     252,
        5,      82,     173,    133,    222,    139,    174,    27,     9,      71,     90,     246,    75,     130,    91,     191,
        169,    138,    2,      151,    194,    235,    81,     7,      25,     113,    228,    159,    205,    253,    134,    142, 
        248,    65,     224,    217,    22,     121,    229,    63,     89,     103,    96,     104,    156,    17,     201,    129, 
        36,     8,      165,    110,    237,    117,    231,    56,     132,    211,    152,    20,     181,    111,    239,    218, 
        170,    163,    51,     172,    157,    47,     80,     212,    176,    250,    87,     49,     99,     242,    136,    189, 
        162,    115,    44,     43,     124,    94,     150,    16,     141,    247,    32,     10,     198,    223,    255,    72,
        53,     131,    84,     57,     220,    197,    58,     50,     208,    11,     241,    28,     3,      192,    62,     202, 
        18,     215,    153,    24,     76,     41,     15,     179,    39,     46,     55,     6,      128,    167,    23,     188, 
        106,    34,     187,    140,    164,    73,     112,    182,    244,    195,    227,    13,     35,     77,     196,    185, 
        26,     200,    226,    119,    31,     123,    168,    125,    249,    68,     183,    230,    177,    135,    160,    180, 
        12,     1,      243,    148,    102,    166,    38,     238,    251,    37,     240,    126,    64,     74,     161,    40, 
        184,    149,    171,    178,    101,    66,     29,     59,     146,    61,     254,    107,    42,     86,     154,    4, 
        236,    232,    120,    21,     233,    209,    45,     98,     193,    114,    78,     19,     206,    14,     118,    127, 
        48,     79,     147,    85,     30,     207,    219,    54,     88,     234,    190,    122,    95,     67,     143,    109, 
        137,    214,    145,    93,     92,     100,    245,    0,      216,    186,    60,     83,     105,    97,     204,    52 };



    /*! \brief Default constructor.
     */
    LatticeNoise::LatticeNoise(void)
        : amplitudeFactor_(0.8f)
        , frequencyFactor_(2.0f)
        , iterations_(4)
    {
        initValueTable(1);
    }

    /*! \brief Constructor with parameters.
     */
    LatticeNoise::LatticeNoise(float amplitudeFactor, float frequencyFactor, int iterations, int seed)
    {
        initValueTable(seed);

        amplitudeFactor_ = amplitudeFactor; 
        frequencyFactor_ = frequencyFactor; 
        iterations_ = iterations;
    }


    /*! \brief Filling the valueTable_ with random numbers based on a given seed.
     *  \param[in] seed Value controlling which set of random numbers is used.   
     */
    void LatticeNoise::initValueTable(int seed)
    {
        std::mt19937 generator(seed);
        std::uniform_real_distribution<double> distribution(0.0, 1.0);
        
        for (int i = 0; i < 256; ++i)
        {
            valueTable_[i] = static_cast<float>( distribution(generator) );
        }
    }


    /*! \brief Returning the value of the fractal sum at a given world point.
     *  \param[in] point World point whose value should be evaluated.   
     */
    float LatticeNoise::valueFractalSum(const Point3D& point) const
    {
        float fractalSum = 0.0f;
        float amplitudeFactor = amplitudeFactor_;
        float frequencyFactor = frequencyFactor_;

        float fsMin = 0.2f;
        float fsMax = 0.0f;
        for (int j = 0; j < iterations_; ++j)
        {
            fractalSum += amplitudeFactor * valueNoise(frequencyFactor * point);
              
            fsMax += amplitudeFactor;

            amplitudeFactor *= 0.5f;
            frequencyFactor *= 2.0f;
        }

        fractalSum = (fractalSum - fsMin) / (fsMax - fsMin);

        return fractalSum;
    }



    /*! \brief Returning the value of the turbulence at a given world point.
     *  \param[in] point World point whose value should be evaluated.   
     */
    float LatticeNoise::valueTurbulence(const Point3D& point) const
    {
        float turbulence = 0.f;
        float amplitudeFactor = amplitudeFactor_;
        float frequencyFactor = frequencyFactor_;

        // float fsMin = 0.2f;
        float fsMax = 0.f;
        for (int j = 0; j < iterations_; ++j)
        {
            turbulence += amplitudeFactor * fabs(valueNoise(frequencyFactor * point));
              
            fsMax += amplitudeFactor;

            amplitudeFactor *= 0.5f;
            frequencyFactor *= 2.f;
        }

        turbulence /= fsMax;

        return turbulence;
    }



    /*! \brief Returning the value of the fractional Brownian motion at a given world point.
     *  \param[in] point World point whose value should be evaluated.   
     */
    float LatticeNoise::valueFractionalBrownMotion(const Point3D& point) const
    {
        float value = 0.f;
        float amplitudeFactor = 1.f;
        float frequencyFactor = 1.f;

        float valMax = 0.f;

        for (int j = 0; j < iterations_; ++j)
        {
            value += amplitudeFactor * valueNoise(frequencyFactor * point) - amplitudeFactor / 2.f;
              
            valMax += amplitudeFactor;

            amplitudeFactor *= amplitudeFactor_;
            frequencyFactor *= frequencyFactor_;
        }

        value = value / valMax;

        return value + 0.5f;
    }

} // end of namespace iiit