// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file LatticeNoise.hpp
 *  \author Christian Zimmermann
 *  \date 28.05.2015
 *  \brief Definition of class LatticeNoise.
 *
 *  Definition of class LatticeNoise representing a generic noise model.
 */

#ifndef __LATTICENOISE_HPP__
#define __LATTICENOISE_HPP__

#include <random>

#include "Utilities/ShadingData.hpp"



namespace iiit
{

    /*! \brief Macro implementing access to permutation table.
     */
    #define PERM(x) permutationTable_[(x)&255]

    /*! \brief Macro to randomize lattice indices with permutation table.
     */
    #define INDEX(ix, iy, iz) PERM((ix)+PERM((iy)+PERM(iz)))



    /*! \brief LatticeNoise class. Base class for Noise functions which operate on a global lattice.
     *  
     *  On creation the value_table_ variable is filled with random numbers within the range of [0, 1].
     *  The indexes of value_table_ accessed are randomized using a permutation table in order to reduce
     *  aliasing effects due to regular spacing of lattice points. The Value of noise for a given world
     *  point can be obtained by the value_noise function, which implements some kind of interpolation
     *  method in any child class.
     */
    class LatticeNoise
    {
    public:
        LatticeNoise(void);
        LatticeNoise(float amplitudeFactor, float frequencyFactor, int iterations, int seed = 1);

        /*! \brief Virtual function to return noise value.
         *  \param[in] point Hit point.
         *  \return Return noise value.
         */
        virtual float valueNoise(const Point3D& point) const = 0;
        float valueFractalSum(const Point3D& point) const;
        float valueTurbulence(const Point3D& point) const;
        float valueFractionalBrownMotion(const Point3D& point) const;

    protected:
        const static unsigned char permutationTable_[256]; ///< Permutation table for random access of valueTable_.
        float valueTable_[256]; ///< Table with random float values defining the grid.

    private:
        void initValueTable(int seed);
        float getRandFloat(void);
            
        float amplitudeFactor_; ///< Amplitude used for fractal sums and turbulence.
        float frequencyFactor_;  ///< Frequency used for fractal sums and turbulence.
        int iterations_;  ///< Number of iterations used for fractal sums and turbulence.
    };



    // inlined member functions

    /*! \brief Funtion return random float value.
     *  \return Random float value.
     */
    inline float LatticeNoise::getRandFloat(void)
    {
        return static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
    }

} // end of namespace iiit

#endif // __LATTICENOISE_HPP__