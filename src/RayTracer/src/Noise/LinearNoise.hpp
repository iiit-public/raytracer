// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file LinearNoise.hpp
 *  \author Christian Zimmermann
 *  \date 28.05.2015
 *  \brief Definition of LinearNoise.
 *
 *  Definition of LinearNoise class.
 */

#ifndef __LINEARNOISE_HPP__
#define __LINEARNOISE_HPP__

#include "Noise/LatticeNoise.hpp"



namespace iiit
{

    /*! \brief LinearNoise class.
     */
    class LinearNoise : public LatticeNoise
    {
    public:
        LinearNoise(void);
        LinearNoise(float amplitudeFactor, float frequencyFactor, int iterations, float gridSize, int seed = 1);
        virtual float valueNoise(const Point3D& point) const;

    private:
        float lerp(const float f, const float a, const float b) const;
        float gridSize_; ///< Size of the grid.
    };

} // end of namespace iiit

#endif // __LINEARNOISE_HPP__