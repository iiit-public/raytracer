// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file CubicNoise.hpp
 *  \author Christian Zimmermann
 *  \date 13.07.2015
 *  \brief Definition of CubicNoise.
 *
 *  Definition of LinearNoise class.
 */

#ifndef __CUBICNOISE_HPP__
#define __CUBICNOISE_HPP__

#include "Noise/LatticeNoise.hpp"



namespace iiit
{

    /*! \brief CubicNoise class.
     */
    class CubicNoise : public LatticeNoise
    {
    public:
        CubicNoise(void);
        CubicNoise(float amplitudeFactor, float frequencyFactor, int iterations, float gridSize, int seed = 1);
        virtual float valueNoise(const Point3D& point) const;

    private:
        float fourKnotSpline(const float factor, const float value1, const float value2, const float value3, const float value4) const;
        float clampValues(const float x, const float minValue, const float maxValue) const;

        float gridSize_; ///< Size of grid.
    };



    //inlined member functions

    /*! \brief Clamp value.
     *  \param[in] x Value to clamp.
     *  \param[in] minValue Lower bound.
     *  \param[in] maxValue Ppper bound.
     *  \return Returns clamped value.
     */
    inline float CubicNoise::clampValues(const float x, const float minValue, const float maxValue) const
    {
        if (x > maxValue)
        {
            return maxValue;
        }
        else if (x < minValue)
        {
            return minValue;
        }
        else
        {
            return x;
        }
    }

} // end of namespace iiit

#endif // __CUBICNOISE_HPP__