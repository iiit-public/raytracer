// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file NextNeighborNoise.hpp
 *  \author Christian Zimmermann
 *  \date 20.06.2015
 *  \brief Definition of NextNeighborNoise.
 *
 *  Definition of NextNeighborNoise class.
 */

#ifndef __NEXTNEIGHBORNOISE_HPP__
#define __NEXTNEIGHBORNOISE_HPP__

#include "Noise/LatticeNoise.hpp"



namespace iiit
{

    /*! \brief NextNeighborNoise class.
     */
    class NextNeighborNoise : public LatticeNoise
    {
    public:
        NextNeighborNoise(void);
        NextNeighborNoise(float amplitudeFactor, float frequencyFactor, int iterations, float gridSize, int seed = 1);
        virtual float valueNoise(const Point3D& point) const;

    private:
        float nextNeighborInterpolation(const float f, const float a, const float b) const;
        float gridSize_; ///< Size of grid.
    };

} // end of namespace

#endif // __NEXTNEIGHBORNOISE_HPP__