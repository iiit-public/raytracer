// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file LinearNoise.cpp
 *  \author Christian Zimmermann
 *  \date 28.05.2015
 *  \brief Implementation of LinearNoise.
 *
 *  Implementation of LinearNoise class.
 */

#include "Noise/LinearNoise.hpp"



namespace iiit
{

    /*! \brief Default constructor.
     */
    LinearNoise::LinearNoise(void):
        LatticeNoise()
    {
    }



    /*! \brief Constructor with arguments.
     */
    LinearNoise::LinearNoise(float amplitudeFactor, float frequencyFactor, int iterations, float gridSize, int seed)
        : LatticeNoise(amplitudeFactor, frequencyFactor, iterations, seed)
        , gridSize_(gridSize)
    {
    }



    /*! \brief Linear Interpolation.
     *  \param[in] factor Factor describing the point in between value1 and value2 that should be interpolated
     *  \param[in] value1 One value
     *  \param[in] value2 One value.
     *  \return Value of the point in between value1 and value2.
     *  
     *  Linear interpolates between value1 and value2 at a point being at factor in between the two points.
     *  Factor should be in between [0, 1] for interpolation otherwise it's an extrapolation.
     */
    float LinearNoise::lerp(const float factor, const float value1, const float value2) const
    { 
        return (value1 + factor * (value2 - value1));
    }



    /*! \brief Returns a noise value at a given world coordinate.
     *  \param[in] point World point whose noise should be evaluated. 
     *  \return Noise value out of the interval [0, 255]
     *  
     *  The variable value_table_ contains random numbers, which are filled on instantiation of the class LatticeNoise. 
     *  The indexes accessed are randomized using a permutation table in order to reduce aliasing effects due to regular spacing of lattice points.
     *  World coordinates are hashed into the range of value_table_ using two macros INDEX and PERM.
     */
    float LinearNoise::valueNoise(const Point3D& point) const
    {
        float invGridSize = 1 / gridSize_;
        Point3D scaledPoint= point * invGridSize;
        int ix, iy, iz;
        float fx, fy, fz;
        float valuesOnGrid[2][2][2];
        
        ix = (int) floor(scaledPoint.x_);
        fx = (float) scaledPoint.x_ - (float) ix;

        iy = (int) floor(scaledPoint.y_);
        fy = (float) scaledPoint.y_ - (float) iy;

        iz = (int) floor(scaledPoint.z_);
        fz = (float) scaledPoint.z_ - (float) iz;

        for (int k = 0; k <= 1; ++k)
        {
            for (int j = 0; j <= 1; ++j)
            {
                for (int i = 0; i <= 1; ++i)
                {
                    valuesOnGrid[k][j][i] = valueTable_[INDEX(ix + i, iy + j, iz + k)];
                }
            }
        }
        
        //Bilinear interpolation    
        float x0, x1, x2, x3, y0, y1, z0;
        x0 = lerp(fx, valuesOnGrid[0][0][0], valuesOnGrid[0][0][1]);
        x1 = lerp(fx, valuesOnGrid[0][1][0], valuesOnGrid[0][1][1]);
        x2 = lerp(fx, valuesOnGrid[1][0][0], valuesOnGrid[1][0][1]);
        x3 = lerp(fx, valuesOnGrid[1][1][0], valuesOnGrid[1][1][1]);
        y0 = lerp(fy, x0, x1);
        y1 = lerp(fy, x2, x3);
        z0 = lerp(fz, y0, y1);

        return z0;
    }

} // end of namespace iiit