// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file NextNeighborNoise.cpp
 *  \author Christian Zimmermann
 *  \date 20.06.2015
 *  \brief Implementation of NextNeighborNoise.
 *
 *  Implementation of NextNeighborNoise class.
 */

#include "Noise/NextNeighborNoise.hpp"



namespace iiit
{

    /*! \brief Default constructor.
     */
    NextNeighborNoise::NextNeighborNoise(void)
        : LatticeNoise()
    {
    }



    /*! \brief Constructor with arguments.
     */
    NextNeighborNoise::NextNeighborNoise(float amplitudeFactor, float frequencyFactor, int iterations, float gridSize, int seed )
        : LatticeNoise(amplitudeFactor, frequencyFactor, iterations, seed)
        , gridSize_(gridSize)
    {
    }



    /*! \brief Next neighbor interpolation.
     *  \param[in] factor Factor describing the point in between value1 and value2 that should be interpolated
     *  \param[in] value1 One value
     *  \param[in] value2 One value.
     *  \return Value of the point in between value1 and value2.
     *  
     *  Returns value1 or value2 whichever is closer.
     *  Factor should be in between [0, 1] for interpolation otherwise it's an extrapolation.
     */
    float NextNeighborNoise::nextNeighborInterpolation(const float factor, const float value1, const float value2) const
    { 
        if (factor < 0.5f)
        {
            return value1;
        }
        else
        {
            return value2;
        }
    }



    /*! \brief Returns a noise value at a given world coordinate.
     *  \param[in] point World point whose noise should be evaluated. 
     *  \return Noise value out of the interval [0, 255]
     *  
     *  The variable value_table_ contains random numbers, which are filled on instantiation of the class LatticeNoise. 
     *  The indexes accessed are randomized using a permutation table in order to reduce aliasing effects due to regular spacing of lattice points.
     *  World coordinates are hashed into the range of value_table_ using two macros INDEX and PERM.
     */
    float NextNeighborNoise::valueNoise(const Point3D& point) const
    {
        float invGridSize = 1 / gridSize_;
        Point3D scaledPoint= point * invGridSize;
        int ix, iy, iz;
        float fx, fy, fz;
        float valuesOnGrid[2][2][2]; 
        
        ix = (int) floor(scaledPoint.x_);
        fx = (float) scaledPoint.x_ - (float) ix;

        iy = (int) floor(scaledPoint.y_);
        fy = (float) scaledPoint.y_ - (float) iy;

        iz = (int) floor(scaledPoint.z_);
        fz = (float) scaledPoint.z_ - (float) iz;

        for (unsigned char k = 0; k <= 1; ++k)
        {
            for (unsigned char j = 0; j <= 1; ++j)
            {
                for (unsigned char i = 0; i <= 1; ++i)
                {
                    valuesOnGrid[k][j][i] = valueTable_[INDEX(ix + i, iy + j, iz + k)];
                }
            }
        }
        
        //Bilinear interpolation    
        float x0, x1, x2, x3, y0, y1, z0;
        x0 = nextNeighborInterpolation(fx, valuesOnGrid[0][0][0], valuesOnGrid[0][0][1]);
        x1 = nextNeighborInterpolation(fx, valuesOnGrid[0][1][0], valuesOnGrid[0][1][1]);
        x2 = nextNeighborInterpolation(fx, valuesOnGrid[1][0][0], valuesOnGrid[1][0][1]);
        x3 = nextNeighborInterpolation(fx, valuesOnGrid[1][1][0], valuesOnGrid[1][1][1]);
        y0 = nextNeighborInterpolation(fy, x0, x1);
        y1 = nextNeighborInterpolation(fy, x2, x3);
        z0 = nextNeighborInterpolation(fz, y0, y1);

        return z0;
    }

} // end of namespace iiit