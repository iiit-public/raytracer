// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file CubicNoise.cpp
 *  \author Christian Zimmermann
 *  \date 28.05.2015
 *  \brief Implementation of CubicNoise.
 *
 *  Implementation of CubicNoise class.
 */

#include "Noise/CubicNoise.hpp"



namespace iiit
{

    /*! \brief Default constructor.
     */
    CubicNoise::CubicNoise(void):
        LatticeNoise()
    {
    }



    /*! \brief Constructor with arguments.
     */
    CubicNoise::CubicNoise(float amplitudeFactor, float frequencyFactor, int iterations, float gridSize, int seed)
        : LatticeNoise(amplitudeFactor, frequencyFactor, iterations, seed)
        , gridSize_(gridSize)
    {
    }



    /*! \brief Spline Interpolation.
     *  \param[in] factor Factor describing the point in between value2 and value3 that should be interpolated. Value1 and value4 are only needed for smoothness.
     *  \param[in] value1,value2, value3, value4 Values at lattice points
     *  \return Value of the point in between value1 and value2.
     *  
     *  Linear interpolates between value1 and value2 at a point being at factor in between the two points.
     *  Factor should be in between [0, 1] for interpolation otherwise it's an extrapolation.
     */
    float CubicNoise::fourKnotSpline(const float factor, const float value1, const float value2, const float value3, const float value4) const
    { 
        // Calculate coefficients
        float c3 = -0.5f * value1 + 1.5f * value2 - 1.5f * value3 + 0.5f * value4;
        float c2 = value1 - 2.5f * value2 + 2.f * value3 - 0.5f * value4;    
        float c1 = -0.5f * value1 + 0.5f * value3;
        float c0 = value2;
        return (c0 + factor * (c1 + factor * (c2 + factor * c3)));
    }



    /*! \brief Returns a noise value at a given world coordinate.
     *  \param[in] point World point whose noise should be evaluated. 
     *  \return Noise value out of the interval [0, 1]
     *  
     *  The variable valueTable_ contains random numbers, which are filled on instantiation of the
     *  class LatticeNoise. The indexes accessed are randomized using a permutation table in order to
     *  reduce aliasing effects due to regular spacing of lattice points. World coordinates are hashed
     *  into the range of valueTable_ using two macros INDEX and PERM.
     */
    float CubicNoise::valueNoise(const Point3D& point) const
    {
        float invGridSize = 1 / gridSize_;
        Point3D scaledPoint= point * invGridSize;
        int ix, iy, iz;
        float fx, fy, fz;
        float valuesOnGridX[4], valuesOnGridY[4], valuesOnGridZ[4];
        
        ix = static_cast<int>(floor(scaledPoint.x_));
        fx = static_cast<float>(scaledPoint.x_ - ix);

        iy = static_cast<int>(floor(scaledPoint.y_));
        fy = static_cast<float>(scaledPoint.y_ - iy);

        iz = static_cast<int>(floor(scaledPoint.z_));
        fz = static_cast<float>(scaledPoint.z_ - iz);

        for (int k = -1; k <= 2; ++k)
        {
            for (int j = -1; j <= 2; ++j)
            {
                for (int i = -1; i <= 2; ++i)
                {
                    // float v = valueTable_[INDEX(ix + i, iy + j, iz + k)];
                    valuesOnGridX[i+1] = valueTable_[INDEX(ix + i, iy + j, iz + k)];
                }

                valuesOnGridY[j+1] = fourKnotSpline(fx, valuesOnGridX[0], valuesOnGridX[1], valuesOnGridX[2], valuesOnGridX[3]);
            }

            valuesOnGridZ[k+1] = fourKnotSpline(fy, valuesOnGridY[0], valuesOnGridY[1], valuesOnGridY[2], valuesOnGridY[3]);
        }
        
        float value = fourKnotSpline(fz, valuesOnGridZ[0], valuesOnGridZ[1], valuesOnGridZ[2], valuesOnGridZ[3]);
        return clampValues(value, 0.f, 1.f);
    }

} // end of namespace iiit