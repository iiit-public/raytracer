// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Grid3D.hpp
 *  \author Thomas Nuernberg
 *  \date 20.06.2016
 *  \brief Definition of class Grid3D.
 *
 *  Definition of class Grid3D representing a texture class.
 */

#ifndef __GRID3D_HPP__
#define __GRID3D_HPP__

#include "Textures/Texture.hpp"

#include <math.h>

#include "Utilities/ShadingData.hpp"
#include "Utilities/Constants.h"


namespace iiit
{

    /*! \brief Class represents grid texture in 3D world coordinates.
     */
    template <class SpectrumType>
    class Grid3D: public Texture<SpectrumType>
    {
    public:
        Grid3D(void);
        Grid3D(float faceSize, float edgeSize, const SpectrumType& faceColor, const SpectrumType& edgeColor);
        Grid3D(float faceSizeX, float edgeSizeX, float faceSizeY, float edgeSizeY, float faceSizeZ, float edgeSizeZ, const SpectrumType& faceColor, const SpectrumType& edgeColor);

        /*! \brief Get the type of the texture.
         *  \return Returns the type of the texture.
         */
        virtual typename Texture<SpectrumType>::Type getType() const {return Texture<SpectrumType>::Grid3D;}

        virtual SpectrumType getColor(const ShadingData<SpectrumType>& shadingDataObject) const;

        void setColors(const SpectrumType& faceColor, const SpectrumType& edgeColor);

    private:
        SpectrumType faceColor_; ///< Face color of texture.
        SpectrumType edgeColor_; ///< Edge color of texture.
        double faceSizeX_; ///< Size of texture faces in x-direction
        double edgeSizeX_; ///< Size of texture edges in x-direction
        double faceSizeY_; ///< Size of texture faces in y-direction
        double edgeSizeY_; ///< Size of texture edges in y-direction
        double faceSizeZ_; ///< Size of texture faces in z-direction
        double edgeSizeZ_; ///< Size of texture edges in z-direction

    };


    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Grid3D<SpectrumType>::Grid3D(void)
        : faceColor_(0.0, 0.0, 0.0)
        , edgeColor_(1.0, 1.0, 1.0)
        , faceSizeX_(0.04)
        , edgeSizeX_(0.01)
        , faceSizeY_(0.04)
        , edgeSizeY_(0.01)
        , faceSizeZ_(0.04)
        , edgeSizeZ_(0.01)
    {
    }



    /*! \brief Constructor with parameters.
     *  \param[in] faceSize Face size.
     *  \param[in] edgeSize Edge size.
     *  \param[in] faceColor Color of faces.
     *  \param[in] edgeColor Color of edges.
     */
    template <class SpectrumType>
    Grid3D<SpectrumType>::Grid3D(float faceSize, float edgeSize, const SpectrumType& faceColor, const SpectrumType& edgeColor)
        : faceColor_(faceColor)
        , edgeColor_(edgeColor)
        , faceSizeX_(faceSize)
        , edgeSizeX_(edgeSize)
        , faceSizeY_(faceSize)
        , edgeSizeY_(edgeSize)
        , faceSizeZ_(faceSize)
        , edgeSizeZ_(edgeSize)
    {
    }



    /*! \brief Constructor with parameters.
     *  \param[in] faceSizeX Face size in x-direction.
     *  \param[in] edgeSizeX Edge size in x-direction.
     *  \param[in] faceSizeY Face size in y-direction.
     *  \param[in] edgeSizeY Edge size in y-direction.
     *  \param[in] faceSizeZ Face size in z-direction.
     *  \param[in] edgeSizeZ Edge size in z-direction.
     *  \param[in] faceColor Color of faces.
     *  \param[in] edgeColor Color of edges.
     */
    template <class SpectrumType>
    Grid3D<SpectrumType>::Grid3D(float faceSizeX, float edgeSizeX, float faceSizeY, float edgeSizeY, float faceSizeZ, float edgeSizeZ, const SpectrumType& faceColor, const SpectrumType& edgeColor)
        : faceColor_(faceColor)
        , edgeColor_(edgeColor)
        , faceSizeX_(faceSizeX)
        , edgeSizeX_(edgeSizeX)
        , faceSizeY_(faceSizeY)
        , edgeSizeY_(edgeSizeY)
        , faceSizeZ_(faceSizeZ)
        , edgeSizeZ_(edgeSizeZ)
    {
    }



    /*! \brief Get color for the texture.
     *  \param[in] shadingDataObject Shading object
     *  \return Returns reflected color.
     */
    template <class SpectrumType>
    SpectrumType Grid3D<SpectrumType>::getColor(const ShadingData<SpectrumType>& shadingDataObject) const
    {
        float x = static_cast<float>(shadingDataObject.localHitPoint3D_.x_ + EPS);
        float y = static_cast<float>(shadingDataObject.localHitPoint3D_.y_ + EPS);
        float z = static_cast<float>(shadingDataObject.localHitPoint3D_.z_ + EPS);
        
        x = static_cast<float>(fabs(fmod(x, (faceSizeX_ + edgeSizeX_))));
        y = static_cast<float>(fabs(fmod(y, (faceSizeY_ + edgeSizeY_))));
        z = static_cast<float>(fabs(fmod(z, (faceSizeZ_ + edgeSizeZ_))));

        if ((x < (edgeSizeX_ / 2.f) || x > ((edgeSizeX_ / 2.f) + faceSizeX_)) ||
            (y < (edgeSizeY_ / 2.f) || y > ((edgeSizeY_ / 2.f) + faceSizeY_)) ||
            (z < (edgeSizeZ_ / 2.f) || z > ((edgeSizeZ_ / 2.f) + faceSizeZ_)))
        {
            return edgeColor_;
        }
        else
        {
            return faceColor_;
        }
    }



    /*! \brief Set colors of the texture.
     *  \param[in] faceColor Color of faces.
     *  \param[in] edgeColor Color of edges.
     */
    template <class SpectrumType>
    void Grid3D<SpectrumType>::setColors(const SpectrumType& faceColor, const SpectrumType& edgeColor)
    {
        faceColor_ = faceColor;
        edgeColor_ = edgeColor;
    }
} // end of namespace iiit

#endif // __GRID3D_HPP__
