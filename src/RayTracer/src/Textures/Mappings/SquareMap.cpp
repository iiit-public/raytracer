// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file SquareMap.cpp
 *  \author David Uhlig
 *  \date 04.09.2017
 *  \brief Implementation of class SquareMap.
 *
 *  Implementation of SquareMap that maps rectangular textures on a Square.
 */

#include "SquareMap.hpp"

#include <math.h>

#include "Utilities/Constants.h"



namespace iiit
{
	/*! \brief Maps local object coordinates to image coordinates (u, v).
	*  \param[in] localHitPoint Local hit point.
	*  \param[in] hRes Horizontal resolution of image.
	*  \param[in] vRes Vertical resolution of image.
	*  \param[out] row Row index of image.
	*  \param[out] column Column index of iamge.
	*/
    void SquareMap::getPixelCoordinates(const Point2D& localHitPoint, const int hRes, const int vRes, int& row, int& column) const
	{
		// map local coordinates to the texel coordinates
		row = static_cast<int>((hRes - 1) * localHitPoint.x_);
		column = static_cast<int>(vRes - (vRes - 1) * localHitPoint.y_);
	}

}// end of namespace iiit
