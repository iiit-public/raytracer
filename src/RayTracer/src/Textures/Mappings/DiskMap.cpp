// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file DiskMap.cpp
 *  \author Maximilian Schambach
 *  \date 11.12.2018
 *  \brief Implementation of class DiskMap.
 *
 *  Implementation of DiskMap that maps rectangular textures on a Disk.
 */

#include "DiskMap.hpp"

#include <math.h>

#include "Utilities/Constants.h"



namespace iiit
{
	/*! \brief Maps local object coordinates to image coordinates (u, v).
	*  \param[in] localHitPoint Local hit point.
	*  \param[in] hRes Horizontal resolution of image.
	*  \param[in] vRes Vertical resolution of image.
	*  \param[out] row Row index of image.
	*  \param[out] column Column index of iamge.
	*/
    void DiskMap::getPixelCoordinates(const Point2D& localHitPoint, const int hRes, const int vRes, int& row, int& column) const
	{
        // map local coordinates in [-1, 1] x [-1, 1] to [0, 1] x [0, 1]
        // map coordinates from [0, 1] x [0, 1] to image coordinates
        row = static_cast<int>((hRes - 1) * 0.5*(localHitPoint.x_ + 1.0));
        column = static_cast<int>(vRes - (vRes - 1) * 0.5*(localHitPoint.y_ + 1.0));
	}

}// end of namespace iiit
