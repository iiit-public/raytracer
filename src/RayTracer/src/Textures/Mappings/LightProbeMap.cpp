// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file LightProbeMap.cpp
 *  \author Thomas Nuernberg
 *  \date 29.06.2016
 *  \brief Definition of class LightProbeMap.
 *
 *  Implementation of LightProbeMap that maps light probe images on a sphere.
 */

#include "LightProbeMap.hpp"

#include <math.h>

#include "Utilities/Constants.h"



namespace iiit
{
    /*! \brief Maps local object coordinates to image coordinates (u, v).
     *  \param[in] localHitPoint Local hit point.
     *  \param[in] hRes Horizontal resolution of image.
     *  \param[in] vRes Vertical resolution of image.
     *  \param[out] row Row index of image.
     *  \param[out] column Column index of iamge.
     */
	void LightProbeMap::getPixelCoordinates(const Point2D& localHitPoint, const int hRes, const int vRes, int& row, int& column) const
	{
		// transform to cartesian coordinates
		float x = static_cast<float>(sin(localHitPoint.x_ / 180.f * PI)*cos(localHitPoint.y_ / 180.f * PI));
		float y = static_cast<float>(sin(localHitPoint.x_ / 180.f * PI)*sin(localHitPoint.y_ / 180.f * PI));
		float z = static_cast<float>(cos(localHitPoint.x_ / 180.f * PI));

		float alpha = static_cast<float>(acos(x));
		float sqrtX2Y2 = static_cast<float>(sqrt(y * y + z * z));
		float sinBeta = static_cast<float>(z) / sqrtX2Y2;
		float cosBeta = static_cast<float>(y) / sqrtX2Y2;

		// map theta and phi to (u, v) in [0, 1] x [0, 1]
		float u = (1.f + alpha / static_cast<float>(PI) * cosBeta) / 2.f;
		float v = (1.f + alpha / static_cast<float>(PI) * sinBeta) / 2.f;

		// map u and v to the texel coordinates
		row = static_cast<int>((hRes - 1) * u);
		column = static_cast<int>(vRes - (vRes - 1) * v);
	}

}// end of namespace iiit