// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file SquareMap.hpp
 *  \author David Uhlig
 *  \date 04.09.2017
 *  \brief Definition of class SquareMap.
 *
 *  Definition of SquareMap that maps rectangular textures on a Square.
 */

#ifndef __SQUAREMAP_HPP__
#define __SQUAREMAP_HPP__

#include "Mapping.hpp"

#include "Utilities/Point3D.hpp"



namespace iiit
{

	/*! \brief Class for mapping a rectangular textures on a plane.
	*/
    class SquareMap : public Mapping
	{

	public:
		virtual void getPixelCoordinates(const Point2D& localHitPoint, const int hRes, const int vRes, int& row, int& column) const;

	};

}// end of namespace iiit

#endif // __SQUAREMAP_HPP__
