// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file SphereMap.cpp
 *  \author Thomas Nuernberg
 *  \date 29.06.2016
 *  \brief Definition of class SphereMap.
 *
 *  Implementation of SphereMap that maps rectangular textures on a sphere.
 */

#include "SphereMap.hpp"

#include <math.h>

#include "Utilities/Constants.h"



namespace iiit
{
    /*! \brief Maps local object coordinates to image coordinates (u, v).
     *  \param[in] localHitPoint Local hit point.
     *  \param[in] hRes Horizontal resolution of image.
     *  \param[in] vRes Vertical resolution of image.
     *  \param[out] row Row index of image.
     *  \param[out] column Column index of iamge.
     */
    void SphereMap::getPixelCoordinates(const Point2D& localHitPoint, const int hRes, const int vRes, int& row, int& column) const
    {
        // compute theta and phi
        float theta = static_cast<float>(localHitPoint.x_ * PI / 180.0);
        float phi	= static_cast<float>(localHitPoint.y_ * PI / 180.0);

        if (phi < 0.f)
        {
            phi += static_cast<float>(TWO_PI);
        }

        // map theta and phi to (u, v) in [0, 1] x [0, 1]
        float u = phi * static_cast<float>(INV_TWO_PI);
        float v = 1.f - theta * static_cast<float>(INV_PI);

        // map u and v to the texel coordinates
        row = static_cast<int>((hRes - 1) * u);
        column = static_cast<int>(vRes - (vRes - 1) * v);
    }

}// end of namespace iiit
