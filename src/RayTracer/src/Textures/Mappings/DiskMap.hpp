// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file DiskMap.hpp
 *  \author Maximilian Schambach
 *  \date 11.12.2018
 *  \brief Definition of class DiskMap.
 *
 *  Definition of Disk that maps rectangular textures on a Disk.
 */

#ifndef __DISKMAP_HPP__
#define __DISKMAP_HPP__

#include "Mapping.hpp"

#include "Utilities/Point3D.hpp"



namespace iiit
{

	/*! \brief Class for mapping a rectangular textures on a plane.
	*/
    class DiskMap : public Mapping
	{

	public:
		virtual void getPixelCoordinates(const Point2D& localHitPoint, const int hRes, const int vRes, int& row, int& column) const;

	};

}// end of namespace iiit

#endif // __DISKMAP_HPP__
