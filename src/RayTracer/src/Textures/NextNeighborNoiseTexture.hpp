// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file NextNeighborNoiseTexture.hpp
 *  \author Christian Zimmermann
 *  \date 13.07.2015
 *  \brief Definition of class NextNeighborNoiseTexture.
 *
 *  Definition of class NextNeighborNoiseTexture representing a noise texture class.
 */

#ifndef __NEXTNEIGHBORNOISETEXTURE_HPP__
#define __NEXTNEIGHBORNOISETEXTURE_HPP__

#include "Texture.hpp"
#include "Noise/NextNeighborNoise.hpp"



namespace iiit
{

    /*! \brief Class represents noise texture with nearest neighbor interpolation.
     */
    template <class SpectrumType>
    class NextNeighborNoiseTexture: public Texture<SpectrumType>
    {
    public:
        NextNeighborNoiseTexture(void);
        NextNeighborNoiseTexture(const SpectrumType& color);
        NextNeighborNoiseTexture(const SpectrumType& color, float amplitudeFactor, float frequencyFactor, int iterations, float gridSize, int seed = 1);

        /*! \brief Get the type of the texture.
         *  \return Returns the type of the texture.
         */
        virtual typename Texture<SpectrumType>::Type getType() const {return Texture<SpectrumType>::NoiseTexture;}

        virtual SpectrumType getColor(const ShadingData<SpectrumType>& shadingDataObject) const;

    private:
        std::shared_ptr<LatticeNoise> noise_; ///< Pointer to noise object
        SpectrumType color_; ///< Base color of noise texture.
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    NextNeighborNoiseTexture<SpectrumType>::NextNeighborNoiseTexture(void)
        : color_(1.f)
    {
        noise_ = std::shared_ptr<LatticeNoise>(new NextNeighborNoise);
    }



    /*! \brief Constructor with parameters
     */
    template <class SpectrumType>
    NextNeighborNoiseTexture<SpectrumType>::NextNeighborNoiseTexture(const SpectrumType& color)
        : color_(color)
    {
        noise_ = std::shared_ptr<LatticeNoise>(new NextNeighborNoise);
    }



    /*! \brief Constructor with parameters
     */
    template <class SpectrumType>
    NextNeighborNoiseTexture<SpectrumType>::NextNeighborNoiseTexture(const SpectrumType& color, float amplitudeFactor, float frequencyFactor, int iterations, float gridSize, int seed)
        : color_(color)
    {
        noise_ = std::shared_ptr<LatticeNoise>(new NextNeighborNoise(amplitudeFactor, frequencyFactor, iterations, gridSize, seed));
    }



    /*! \brief Get color for the texture.
     *  \param[in] shadingDataObject Shading object
     *  \return Returns reflected color.
     */
    template <class SpectrumType>
    SpectrumType NextNeighborNoiseTexture<SpectrumType>::getColor(const ShadingData<SpectrumType>& shadingDataObject) const
    {
        float value = noise_->valueFractionalBrownMotion(shadingDataObject.hitPoint_);
        return color_ * value;
    }

} // end of namespace iiit

#endif // __NEXTNEIGHBORNOISETEXTURE_HPP__