// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Sine3D.hpp
 *  \author Thomas Nuernberg
 *  \date 09.08.2017
 *  \brief Definition of class Sine3D.
 *
 *  Definition of class Sine3D representing a texture class.
 */

#ifndef __SINE3D_HPP__
#define __SINE3D_HPP__

#include "Textures/Texture.hpp"

#include <math.h>

#include "Utilities/ShadingData.hpp"
#include "Utilities/Constants.h"



namespace iiit
{

    /*! \brief Class represents a sine wave texture mapped to 3D surface coordinates.
     */
    template <class SpectrumType>
    class Sine3D: public Texture<SpectrumType>
    {
    public:
        Sine3D(void);
        Sine3D(float frequency, float phase, const SpectrumType& color1, const SpectrumType& color2);
        Sine3D(float xFrequency, float yFrequency, float zFrequency, float xPhase, float yPhase, float zPhase, const SpectrumType& color1, const SpectrumType& color2);

        /*! \brief Get the type of the texture.
         *  \return Returns the type of the texture.
         */
        virtual typename Texture<SpectrumType>::Type getType() const {return Texture<SpectrumType>::Checker3D;}

        virtual SpectrumType getColor(const ShadingData<SpectrumType>& shadingDataObject) const;

        void setColors(const SpectrumType& color1, const SpectrumType& color2);

    private:
        SpectrumType color1_; ///< First color of sine texture.
        SpectrumType color2_; ///< Second color of sine texture.
        float xFrequency_; ///< Frequency of sine wave in x-direction.
        float yFrequency_; ///< Frequency of sine wave in y-direction.
        float zFrequency_; ///< Frequency of sine wave in z-direction.
        float xPhase_; ///< Phase of sine wave in x-direction.
        float yPhase_; ///< Phase of sine wave in y-direction.
        float zPhase_; ///< Phase of sine wave in z-direction.
    };


    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Sine3D<SpectrumType>::Sine3D(void)
        : color1_(0.0, 0.0, 0.0)
        , color2_(1.0, 1.0, 1.0)
        , xFrequency_(1.f)
        , yFrequency_(1.f)
        , zFrequency_(1.f)
        , xPhase_(0.f)
        , yPhase_(0.f)
        , zPhase_(0.f)
    {
    }



    /*! \brief Constructor with parameters.
     *  \param[in] frequency Frequency in x-, y- and z-direction.
     *  \param[in] phase Phase in x-, y- and z-direction.
     *  \param[in] color1 Color of first patch.
     *  \param[in] color2 Color of second patch.
     */
    template <class SpectrumType>
    Sine3D<SpectrumType>::Sine3D(float frequency, float phase, const SpectrumType& color1, const SpectrumType& color2)
        : color1_(color1)
        , color2_(color2)
        , xFrequency_(frequency)
        , yFrequency_(frequency)
        , zFrequency_(frequency)
        , xPhase_(phase)
        , yPhase_(phase)
        , zPhase_(phase)
    {
    }



    /*! \brief Constructor with parameters.
     *  \param[in] xFrequency Frequency in x-direction.
     *  \param[in] yFrequency Frequency in y-direction.
     *  \param[in] zFrequency Frequency in z-direction.
     *  \param[in] xPhase Phase in x-direction.
     *  \param[in] yPhase Phase in y-direction.
     *  \param[in] zPhase Phase in z-direction.
     *  \param[in] color1 First color.
     *  \param[in] color2 Second color.
     */
    template <class SpectrumType>
    Sine3D<SpectrumType>::Sine3D(float xFrequency, float yFrequency, float zFrequency, float xPhase, float yPhase, float zPhase, const SpectrumType& color1, const SpectrumType& color2)
        : color1_(color1)
        , color2_(color2)
        , xFrequency_(xFrequency)
        , yFrequency_(yFrequency)
        , zFrequency_(zFrequency)
        , xPhase_(xPhase)
        , yPhase_(yPhase)
        , zPhase_(zPhase)
    {
    }



    /*! \brief Get color for the texture.
     *  \param[in] shadingDataObject Shading object
     *  \return Returns reflected color.
     */
    template <class SpectrumType>
    SpectrumType Sine3D<SpectrumType>::getColor(const ShadingData<SpectrumType>& shadingDataObject) const
    {
        float x = static_cast<float>(shadingDataObject.hitPoint_.x_ + EPS);
        float y = static_cast<float>(shadingDataObject.hitPoint_.y_ + EPS);
        float z = static_cast<float>(shadingDataObject.hitPoint_.z_ + EPS);
        
        float a = 0.5f * (1.f + static_cast<float>(
            cos(2.f * PI * xFrequency_ * x + xPhase_)
            * cos(2.f * PI * yFrequency_ * y + yPhase_)
            * cos(2.f * PI * zFrequency_ * z + zPhase_)));
        return (color1_ * a + color2_ * (1.f - a));
    }



    /*! \brief Set colors of the texture.
     *  \param[in] color1 First color.
     *  \param[in] color2 Second color.
     */
    template <class SpectrumType>
    void Sine3D<SpectrumType>::setColors(const SpectrumType& color1, const SpectrumType& color2)
    {
        color1_ = color1;
        color2_ = color2;
    }
} // end of namespace iiit

#endif // __CHECKER3D_HPP__
