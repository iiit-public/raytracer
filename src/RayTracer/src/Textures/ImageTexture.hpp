// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file ImageTexture.hpp
 *  \author Thomas Nuernberg
 *  \date 29.06.2016
 *  \brief Definition of class ImageTexture.
 *
 *  Definition of class ImageTexture representing a texture class.
 */

#ifndef __IMAGETEXTURE_HPP__
#define __IMAGETEXTURE_HPP__

#include "Textures/Texture.hpp"

#include <array>

#include "CImg.h"

#include "Mappings/Mapping.hpp"
#include "Spectrum/CoefficientSpectrum.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Constants.h"



namespace iiit
{

    /*! \brief Class represents texture from an image file.
     */
    template <class SpectrumType>
    class ImageTexture: public Texture<SpectrumType>
    {

    public:
        ImageTexture(void);

        /*! \brief Get the type of the texture.
         *  \return Returns the type of the texture.
         */
        virtual typename Texture<SpectrumType>::Type getType() const {return Texture<SpectrumType>::ImageTexture;}

        virtual SpectrumType getColor(const ShadingData<SpectrumType>& shadingDataObject) const;

        void setImage(std::shared_ptr<cimg_library::CImg<unsigned char> > image);
        void setMapping(std::shared_ptr<Mapping> mapping);

    private:
        int hRes_; ///< Horizontal image resolution.
        int vRes_; ///< Vertical image resolution.
        std::shared_ptr<cimg_library::CImg<unsigned char> > image_; ///< Image defining the texture.
        std::shared_ptr<Mapping> mapping_; ///< Mapping object used to map texture on object.

    };
    
    
    
    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    ImageTexture<SpectrumType>::ImageTexture(void)
    {
    }



    /*! \brief Get color for the texture.
     *  \param[in] shadingDataObject Shading object (not used for ConstantColor).
     *  \return Returns reflected color.
     */
    template <class SpectrumType>
    SpectrumType ImageTexture<SpectrumType>::getColor(const ShadingData<SpectrumType>& shadingDataObject) const
    {
        int row;
        int column;

        if (mapping_)
        {
            mapping_->getPixelCoordinates(shadingDataObject.localHitPoint2D_, hRes_, vRes_, row, column );
        }
        else
        {
            column = 0;
            row = 0;
            //column = static_cast<int>((hRes_ - 1) * shadingDataObject.u_);
            //row = static_cast<int>((vRes_ - 1) * shadingDataObject.v_);
        }

        std::array<float, 3> rgb;
        rgb[0] = static_cast<float>(image_->operator()(row, column, 0, 0)) / TRUE_COLOR_MAX;
        rgb[1] = static_cast<float>(image_->operator()(row, column, 0, 1)) / TRUE_COLOR_MAX;
        rgb[2] = static_cast<float>(image_->operator()(row, column, 0, 2)) / TRUE_COLOR_MAX;
        return SpectrumType::fromRgb(rgb, SpectrumRepresentationType::ReflectanceSpectrum);
    }



    /*! \brief Set image of texture.
     *  \param[in] image Image.
     */
    template <class SpectrumType>
    void ImageTexture<SpectrumType>::setImage(std::shared_ptr<cimg_library::CImg<unsigned char> > image)
    {
        image_ = image;
        hRes_ = image->width();
        vRes_ = image->height();
    }



    /*! \brief Set mapping for texture.
     *  \param[in] mapping Mapping.
     */
    template <class SpectrumType>
    void ImageTexture<SpectrumType>::setMapping(std::shared_ptr<Mapping> mapping)
    {
        mapping_ = mapping;
    }

}// end of namespace iiit

#endif // __IMAGETEXTURE_HPP__