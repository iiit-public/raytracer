// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Sine2D.hpp
 *  \author Thomas Nuernberg
 *  \date 09.08.2017
 *  \brief Definition of class Sine2D.
 *
 *  Definition of class Sine2D representing a texture class.
 */

#ifndef __SINE2D_HPP__
#define __SINE2D_HPP__

#include "Textures/Texture.hpp"

#include <math.h>

#include "Utilities/ShadingData.hpp"
#include "Utilities/Constants.h"



namespace iiit
{
    /*! \brief Class represents a sine wave texture mapped to 2D surface coordinates.
     */
    template <class SpectrumType>
    class Sine2D: public Texture<SpectrumType>
    {
    public:
        Sine2D(void);
        Sine2D(float frequency, float phase, const SpectrumType& color1, const SpectrumType& color2);
        Sine2D(float uFrequency, float vFrequency, float uPhase, float vPhase, const SpectrumType& color1, const SpectrumType& color2);

        /*! \brief Get the type of the texture.
         *  \return Returns the type of the texture.
         */
        virtual typename Texture<SpectrumType>::Type getType() const {return Texture<SpectrumType>::Sine2D;}

        virtual SpectrumType getColor(const ShadingData<SpectrumType>& shadingDataObject) const;

        void setColors(const SpectrumType& color1, const SpectrumType& color2);

    private:
        SpectrumType color1_; ///< First color of sine texture.
        SpectrumType color2_; ///< Second color of sine texture.
        float uFrequency_; ///< Frequency of sine wave in u-direction.
        float vFrequency_; ///< Frequency of sine wave in v-direction.
        float uPhase_; ///< Phase of sine wave in u-direction.
        float vPhase_; ///< Phase of sine wave in v-direction.
    };
    
    

    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Sine2D<SpectrumType>::Sine2D(void)
        : color1_(0.0, 0.0, 0.0)
        , color2_(1.0, 1.0, 1.0)
        , uFrequency_(1.f)
        , vFrequency_(1.f)
        , uPhase_(0.f)
        , vPhase_(0.f)
    {
    }



    /*! \brief Constructor with parameters.
     *  \param[in] frequency Frequency in u- and v-direction.
     *  \param[in] phase Phase in u- and v-direction.
     *  \param[in] color1 First color.
     *  \param[in] color2 Second color.
     */
    template <class SpectrumType>
    Sine2D<SpectrumType>::Sine2D(float frequency, float phase, const SpectrumType& color1, const SpectrumType& color2)
        : color1_(color1)
        , color2_(color2)
        , uFrequency_(frequency)
        , vFrequency_(frequency)
        , uPhase_(phase)
        , vPhase_(phase)
    {
    }



    /*! \brief Constructor with parameters.
     *  \param[in] uFrequency Frequency in u-direction.
     *  \param[in] vFrequency Frequency in v-direction.
     *  \param[in] uPhase Phase in u-direction.
     *  \param[in] vPhase Phase in v-direction.
     *  \param[in] color1 First color.
     *  \param[in] color2 Second color.
     */
    template <class SpectrumType>
    Sine2D<SpectrumType>::Sine2D(float uFrequency, float vFrequency, float uPhase, float vPhase, const SpectrumType& color1, const SpectrumType& color2)
        : color1_(color1)
        , color2_(color2)
        , uFrequency_(uFrequency)
        , vFrequency_(vFrequency)
        , uPhase_(uPhase)
        , vPhase_(vPhase)
    {
    }



    /*! \brief Get color for the texture.
     *  \param[in] shadingDataObject Shading object
     *  \return Returns reflected color.
     */
    template <class SpectrumType>
    SpectrumType Sine2D<SpectrumType>::getColor(const ShadingData<SpectrumType>& shadingDataObject) const
    {
        float u = static_cast<float>(shadingDataObject.localHitPoint2D_.x_ + EPS);
        float v = static_cast<float>(shadingDataObject.localHitPoint2D_.y_ + EPS);
        
        float a = 0.5f * (1.f + static_cast<float>(
			cos(2.f * PI * uFrequency_ * u + uPhase_) 
			* cos(2.f * PI * vFrequency_ * v + vPhase_)));
        return (color1_ * a + color2_ * (1.f - a));
    }



    /*! \brief Set colors of the texture.
     *  \param[in] color1 First color.
     *  \param[in] color2 Second color.
     */
    template <class SpectrumType>
    void Sine2D<SpectrumType>::setColors(const SpectrumType& color1, const SpectrumType& color2)
    {
        color1_ = color1;
        color2_ = color2;
    }

} // end of namespace iiit

#endif // __SINE2D_HPP__
