// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file ConstantColor.hpp
 *  \author Christian Zimmermann
 *  \date 18.05.2015
 *  \brief Definition of class ConstantColor.
 *
 *  Definition of class ConstantColor representing a texture class.
 */

#ifndef __CONSTANTCOLOR_HPP__
#define __CONSTANTCOLOR_HPP__

#include "Textures/Texture.hpp"

#include "Utilities/ShadingData.hpp"



namespace iiit
{

    /*! \brief Class represents constant color texture.
     */
    template <class SpectrumType>
    class ConstantColor: public Texture<SpectrumType>
    {

    public:
        ConstantColor(void);
        ConstantColor(const SpectrumType& color);

        /*! \brief Get the type of the texture.
         *  \return Returns the type of the texture.
         */
        virtual typename Texture<SpectrumType>::Type getType() const {return Texture<SpectrumType>::ConstantColor;}

        virtual SpectrumType getColor(const ShadingData<SpectrumType>& shadingDataObject) const;

        void setColor(const SpectrumType& color);

    private:
        SpectrumType color_; ///< Base color.

    };
    
    
    
    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    ConstantColor<SpectrumType>::ConstantColor(void)
        : color_(SpectrumType(0.f))
    {
    }



    /*! \brief Constructor with initialization.
     */
    template <class SpectrumType>
    ConstantColor<SpectrumType>::ConstantColor(const SpectrumType& color)
        : color_(color)
    {
    }



    /*! \brief Get color for the texture.
     *  \param[in] shadingDataObject Shading object (not used for ConstantColor).
     *  \return Returns reflected color.
     */
    template <class SpectrumType>
    SpectrumType ConstantColor<SpectrumType>::getColor(const ShadingData<SpectrumType>& shadingDataObject) const
    {
        return color_;
    }



    /*! \brief Sets color for the texture.
     *  \param[in] color The constant color of the texture
     */
    template <class SpectrumType>
    void ConstantColor<SpectrumType>::setColor(const SpectrumType& color)
    {
        color_ = color;
    }

}// end of namespace iiit

#endif // __CONSTANTCOLOR_HPP__