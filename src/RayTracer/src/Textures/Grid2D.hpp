// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Grid2D.hpp
 *  \author Thomas Nuernberg
 *  \date 20.06.2016
 *  \brief Definition of class Grid2D.
 *
 *  Definition of class Grid2D representing a texture class.
 */

#ifndef __GRID2D_HPP__
#define __GRID2D_HPP__

#include "Textures/Texture.hpp"

#include <math.h>

#include "Utilities/ShadingData.hpp"
#include "Utilities/Constants.h"


namespace iiit
{

    /*! \brief Class represents grid texture in 2D world coordinates.
     */
    template <class SpectrumType>
    class Grid2D: public Texture<SpectrumType>
    {
    public:
        Grid2D(void);
        Grid2D(float faceSize, float edgeSize, const SpectrumType& faceColor, const SpectrumType& edgeColor);
        Grid2D(float faceSizeU, float edgeSizeU, float faceSizeV, float edgeSizeV, const SpectrumType& faceColor, const SpectrumType& edgeColor);

        /*! \brief Get the type of the texture.
         *  \return Returns the type of the texture.
         */
        virtual typename Texture<SpectrumType>::Type getType() const {return Texture<SpectrumType>::Grid2D;}

        virtual SpectrumType getColor(const ShadingData<SpectrumType>& shadingDataObject) const;

        void setColors(const SpectrumType& faceColor, const SpectrumType& edgeColor);

    private:
        SpectrumType faceColor_; ///< Face color of texture.
        SpectrumType edgeColor_; ///< Edge color of texture.
        double faceSizeU_; ///< Size of texture faces in u-direction
        double edgeSizeU_; ///< Size of texture edges in u-direction
        double faceSizeV_; ///< Size of texture faces in v-direction
        double edgeSizeV_; ///< Size of texture edges in v-direction

    };


    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Grid2D<SpectrumType>::Grid2D(void)
        : faceColor_(0.0, 0.0, 0.0)
        , edgeColor_(1.0, 1.0, 1.0)
        , faceSizeU_(0.04)
        , edgeSizeU_(0.01)
        , faceSizeV_(0.04)
        , edgeSizeV_(0.01)
    {
    }



    /*! \brief Constructor with parameters.
     *  \param[in] faceSize Face size.
     *  \param[in] edgeSize Edge size.
     *  \param[in] faceColor Color of faces.
     *  \param[in] edgeColor Color of edges.
     */
    template <class SpectrumType>
    Grid2D<SpectrumType>::Grid2D(float faceSize, float edgeSize, const SpectrumType& faceColor, const SpectrumType& edgeColor)
        : faceColor_(faceColor)
        , edgeColor_(edgeColor)
        , faceSizeU_(faceSize)
        , edgeSizeU_(edgeSize)
        , faceSizeV_(faceSize)
        , edgeSizeV_(edgeSize)
    {
    }



    /*! \brief Constructor with parameters.
     *  \param[in] faceSizeU Face size in x-direction.
     *  \param[in] edgeSizeU Edge size in x-direction.
     *  \param[in] faceSizeV Face size in y-direction.
     *  \param[in] edgeSizeV Edge size in y-direction.
     *  \param[in] faceColor Color of faces.
     *  \param[in] edgeColor Color of edges.
     */
    template <class SpectrumType>
    Grid2D<SpectrumType>::Grid2D(float faceSizeU, float edgeSizeU, float faceSizeV, float edgeSizeV, const SpectrumType& faceColor, const SpectrumType& edgeColor)
        : faceColor_(faceColor)
        , edgeColor_(edgeColor)
        , faceSizeU_(faceSizeU)
        , edgeSizeU_(edgeSizeU)
        , faceSizeV_(faceSizeV)
        , edgeSizeV_(edgeSizeV)
    {
    }



    /*! \brief Get color for the texture.
     *  \param[in] shadingDataObject Shading object
     *  \return Returns reflected color.
     */
    template <class SpectrumType>
    SpectrumType Grid2D<SpectrumType>::getColor(const ShadingData<SpectrumType>& shadingDataObject) const
    {
        float u = static_cast<float>(shadingDataObject.localHitPoint2D_.x_ + EPS);
        float v = static_cast<float>(shadingDataObject.localHitPoint2D_.y_ + EPS);

        u = static_cast<float>(fabs(fmod(u, (faceSizeU_ + edgeSizeU_))));
        v = static_cast<float>(fabs(fmod(v, (faceSizeV_ + edgeSizeV_))));

        if ((u < (edgeSizeU_ / 2.f) || u > ((edgeSizeU_ / 2.f) + faceSizeU_)) ||
            (v < (edgeSizeV_ / 2.f) || v > ((edgeSizeV_ / 2.f) + faceSizeV_)))
        {
            return edgeColor_;
        }
        else
        {
            return faceColor_;
        }
    }



    /*! \brief Set colors of the texture.
     *  \param[in] faceColor Color of faces.
     *  \param[in] edgeColor Color of edges.
     */
    template <class SpectrumType>
    void Grid2D<SpectrumType>::setColors(const SpectrumType& faceColor, const SpectrumType& edgeColor)
    {
        faceColor_ = faceColor;
        edgeColor_ = edgeColor;
    }
} // end of namespace iiit

#endif // __GRID2D_HPP__