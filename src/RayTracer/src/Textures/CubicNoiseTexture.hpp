// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file CubicNoiseTexture.hpp
 *  \author Christian Zimmermann
 *  \date 13.07.2015
 *  \brief Definition of class CubicNoiseTexture.
 *
 *  Definition of class CubicNoiseTexture representing a noise texture class.
 */

#ifndef __CUBICNOISETEXTURE_HPP__
#define __CUBICNOISETEXTURE_HPP__

#include "Texture.hpp"
#include "Noise/CubicNoise.hpp"



namespace iiit
{

    /*! \brief Class represents noise texture with cubic interpolation.
     */
    template <class SpectrumType>
    class CubicNoiseTexture: public Texture<SpectrumType>
    {
    public:
        CubicNoiseTexture(void);
        CubicNoiseTexture(const SpectrumType& color);
        CubicNoiseTexture(const SpectrumType& color, float amplitudeFactor, float frequencyFactor, int iterations, float gridSize, int seed = 1);

        /*! \brief Get the type of the texture.
         *  \return Returns the type of the texture.
         */
        virtual typename Texture<SpectrumType>::Type getType() const {return Texture<SpectrumType>::NoiseTexture;}

        virtual SpectrumType getColor(const ShadingData<SpectrumType>& shadingDataObject) const;

    private:
        std::shared_ptr< LatticeNoise > noise_; ///< Noise generating object.
        SpectrumType color_; ///< Base color of texture.
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    CubicNoiseTexture<SpectrumType>::CubicNoiseTexture(void)
        : color_(1.f)
    {
        noise_ = std::shared_ptr<LatticeNoise>(new CubicNoise);
    }



    /*! \brief Constructor with parameters
     */
    template <class SpectrumType>
    CubicNoiseTexture<SpectrumType>::CubicNoiseTexture(const SpectrumType& color)
        : color_(color)
    {
        noise_ = std::shared_ptr<LatticeNoise>(new CubicNoise);
    }



    /*! \brief Constructor with parameters
     */
    template <class SpectrumType>
    CubicNoiseTexture<SpectrumType>::CubicNoiseTexture(const SpectrumType& color, float amplitudeFactor, float frequencyFactor, int iterations, float gridSize, int seed)
        : color_(color)
    {
        noise_ = std::shared_ptr<LatticeNoise>(new CubicNoise(amplitudeFactor, frequencyFactor, iterations, gridSize, seed));
    }



    /*! \brief Get color for the texture.
     *  \param[in] shadingDataObject Shading object
     *  \return Returns reflected color.
     */
    template <class SpectrumType>
    SpectrumType CubicNoiseTexture<SpectrumType>::getColor(const ShadingData<SpectrumType>& shadingDataObject) const
    {
        float value = noise_->valueFractionalBrownMotion(shadingDataObject.hitPoint_);
        return color_ * value;
    }

} // end of namespace iiit

#endif // __CUBICNOISETEXTURE_HPP__