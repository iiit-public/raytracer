// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Checker3D.hpp
 *  \author Christian Zimmermann
 *  \date 25.05.2015
 *  \brief Definition of class Checker3D.
 *
 *  Definition of class Checker3D representing a texture class.
 */

#ifndef __CHECKER3D_HPP__
#define __CHECKER3D_HPP__

#include "Textures/Texture.hpp"

#include <math.h>

#include "Utilities/ShadingData.hpp"
#include "Utilities/Constants.h"



namespace iiit
{

    /*! \brief Class represents checker texture in 3D world coordinates.
     */
    template <class SpectrumType>
    class Checker3D: public Texture<SpectrumType>
    {
    public:
        Checker3D(void);
        Checker3D(float size, const SpectrumType& color1, const SpectrumType& color2);
        Checker3D(float sizeX, float sizeY, float sizeZ, const SpectrumType& color1, const SpectrumType& color2);

        /*! \brief Get the type of the texture.
         *  \return Returns the type of the texture.
         */
        virtual typename Texture<SpectrumType>::Type getType() const {return Texture<SpectrumType>::Checker3D;}

        virtual SpectrumType getColor(const ShadingData<SpectrumType>& shadingDataObject) const;

        void setColors(const SpectrumType& color1, const SpectrumType& color2);

    private:
        SpectrumType color1_; ///< First color of checker texture.
        SpectrumType color2_; ///< second color of checker texture.
        double sizeX_; ///< Size of texture patches in x-direction
        double sizeY_; ///< Size of texture patches in y-direction
        double sizeZ_; ///< Size of texture patches in z-direction
    };


    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Checker3D<SpectrumType>::Checker3D(void)
        : color1_(0.0, 0.0, 0.0)
        , color2_(1.0, 1.0, 1.0)
        , sizeX_(0.05)
        , sizeY_(0.05)
        , sizeZ_(0.05)
    {
    }



    /*! \brief Constructor with parameters.
     *  \param[in] size Patch size.
     *  \param[in] color1 Color of first patch.
     *  \param[in] color2 Color of second patch.
     */
    template <class SpectrumType>
    Checker3D<SpectrumType>::Checker3D(float size, const SpectrumType& color1, const SpectrumType& color2)
        : color1_(color1)
        , color2_(color2)
        , sizeX_(size)
        , sizeY_(size)
        , sizeZ_(size)
    {
    }



    /*! \brief Constructor with parameters.
     *  \param[in] sizeX Patch size in x-direction.
     *  \param[in] sizeY Patch size in y-direction.
     *  \param[in] sizeZ Patch size in z-direction.
     *  \param[in] color1 Color of first patch.
     *  \param[in] color2 Color of second patch.
     */
    template <class SpectrumType>
    Checker3D<SpectrumType>::Checker3D(float sizeX, float sizeY, float sizeZ, const SpectrumType& color1, const SpectrumType& color2)
        : color1_(color1)
        , color2_(color2)
        , sizeX_(sizeX)
        , sizeY_(sizeY)
        , sizeZ_(sizeZ)
    {
    }



    /*! \brief Get color for the texture.
     *  \param[in] shadingDataObject Shading object
     *  \return Returns reflected color.
     */
    template <class SpectrumType>
    SpectrumType Checker3D<SpectrumType>::getColor(const ShadingData<SpectrumType>& shadingDataObject) const
    {
        float x = static_cast<float>(shadingDataObject.localHitPoint3D_.x_ + EPS);
        float y = static_cast<float>(shadingDataObject.localHitPoint3D_.y_ + EPS);
        float z = static_cast<float>(shadingDataObject.localHitPoint3D_.z_ + EPS);
        
        if ((static_cast<int>(floor(x / sizeX_)) + static_cast<int>(floor(y / sizeY_)) + static_cast<int>(floor(z / sizeZ_))) % 2 == 0)
        {
            return color1_;
        }
        else
        {
            return color2_;
        }
    }



    /*! \brief Set colors of the texture.
     *  \param[in] color1 Color of first patch.
     *  \param[in] color2 Color of second patch.
     */
    template <class SpectrumType>
    void Checker3D<SpectrumType>::setColors(const SpectrumType& color1, const SpectrumType& color2)
    {
        color1_ = color1;
        color2_ = color2;
    }
} // end of namespace iiit

#endif // __CHECKER3D_HPP__
