// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file SpectralImageTexture.hpp
 *  \author Maximilian Schambach
 *  \date 10.12.2018
 *  \brief Definition of class SpectralImageTexture.
 *
 *  Definition of class SpectralImageTexture representing a spectral texture class.
 */

#ifndef __SPECTRALIMAGETEXTURE_HPP__
#define __SPECTRALIMAGETEXTURE_HPP__

#include "Textures/Texture.hpp"

#include <array>

#include "CImg.h"

#include "Mappings/Mapping.hpp"
#include "Spectrum/CoefficientSpectrum.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Constants.h"



namespace iiit
{

    /*! \brief Class represents multispectral texture from an image file.
     */
    template <class SpectrumType>
    class SpectralImageTexture: public Texture<SpectrumType>
    {

    public:
        SpectralImageTexture(void);

        /*! \brief Get the type of the texture.
         *  \return Returns the type of the texture.
         */
        virtual typename Texture<SpectrumType>::Type getType() const {return Texture<SpectrumType>::SpectralImageTexture;}

        virtual SpectrumType getColor(const ShadingData<SpectrumType>& shadingDataObject) const;

        void setData(std::shared_ptr<std::vector<SpectrumType> > data);
        void setShape(unsigned long width, unsigned long height);
        void setMapping(std::shared_ptr<Mapping> mapping);

    private:
        int nRows_; ///< Number of rows (height).
        int nCols_; ///< Number of columns (width).
        std::shared_ptr<std::vector<SpectrumType> > data_; ///< Image defining the texture.
        std::shared_ptr<Mapping> mapping_; ///< Mapping object used to map texture on object.

    };
    
    
    
    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    SpectralImageTexture<SpectrumType>::SpectralImageTexture(void)
    {
    }



    /*! \brief Get color for the texture.
     *  \param[in] shadingDataObject Shading object (not used for ConstantColor).
     *  \return Returns reflected color.
     */
    template <class SpectrumType>
    SpectrumType SpectralImageTexture<SpectrumType>::getColor(const ShadingData<SpectrumType>& shadingDataObject) const
    {
        int row;
        int column;

        if (mapping_)
        {
            // Carefull: row is not the index of the row but the index in the row
            // Same holds for column. So, the index of the row is 'column' and the index
            // of the column is 'row'
            mapping_->getPixelCoordinates(shadingDataObject.localHitPoint2D_, nCols_, nRows_, row, column);
        }
        else
        {
            column = 0;
            row = 0;
        }

        return (*data_)[column*nCols_ + row];
    }



    /*! \brief Set data of spectral image texture.
     *  \param[in] data Data.
     */
    template <class SpectrumType>
    void SpectralImageTexture<SpectrumType>::setData(std::shared_ptr<std::vector<SpectrumType> > data)
    {
        data_ = data;
    }

    /*! \brief Set shape of data of texture.
     *  \param[in] width Width.
     * *  \param[in] heigth Height.
     */
    template <class SpectrumType>
    void SpectralImageTexture<SpectrumType>::setShape(unsigned long nCols, unsigned long nRows)
    {
        nRows_ = nRows;
        nCols_ = nCols;
    }



    /*! \brief Set mapping for texture.
     *  \param[in] mapping Mapping.
     */
    template <class SpectrumType>
    void SpectralImageTexture<SpectrumType>::setMapping(std::shared_ptr<Mapping> mapping)
    {
        mapping_ = mapping;
    }

}// end of namespace iiit

#endif // __SPECTRALIMAGETEXTURE_HPP__
