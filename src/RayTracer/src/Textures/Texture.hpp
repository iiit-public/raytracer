// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Texture.hpp
 *  \author Christian Zimmermann
 *  \date 18.05.2015
 *  \brief Definition of class Texture.
 *
 *  Definition of class Texture representing a texture base class.
 */

#ifndef __TEXTURE_HPP__
#define __TEXTURE_HPP__

#include "Utilities/ShadingData.hpp"



namespace iiit
{
    
    /*! \brief Abstract class for textures.
     */
    template <class SpectrumType>
    class Texture
    {
    public:
        /*! This enum type defines valid geometric objects. The objects are defined in specialized
         *  classes.
         */
        enum Type
        {
            ConstantColor, ///< ConstantColor texture.
            Checker3D, ///< Checker3D texture.
            Checker2D, ///< Checker2D texture.
            Grid3D, ///< Grid3D texture.
            Grid2D, ///< Grid2D texture.
            Sine3D, ///< Grid3D texture.
            Sine2D, ///< Grid2D texture.
            NoiseTexture, ///< NoiseTexture texture.
            ImageTexture, ///< Texture from image file.
            SpectralImageTexture, ///< Spectral Texture from image file.
        };

        
        /*! \brief Get the type of the texture.
         *  \return Returns the type of the texture.
         */
        virtual typename Texture<SpectrumType>::Type getType() const = 0;


        /*! \brief Get color for the texture.
         *  \param[in] shadingDataObject Shading object
         *  \return Returns reflected color.
         */
        virtual SpectrumType getColor(const ShadingData<SpectrumType>& shadingDataObject) const = 0;
    };
        
} // end of namespace iiit

#endif // __TEXTURE_HPP__
