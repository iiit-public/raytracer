// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Checker2D.hpp
 *  \author Christian Zimmermann
 *  \date 25.05.2015
 *  \brief Definition of class Checker2D.
 *
 *  Definition of class Checker2D representing a texture class.
 */

#ifndef __CHECKER2D_HPP__
#define __CHECKER2D_HPP__

#include "Textures/Texture.hpp"

#include <math.h>

#include "Utilities/ShadingData.hpp"
#include "Utilities/Constants.h"



namespace iiit
{
    /*! \brief Class represents checker texture mapped to 2D surface coordinates.
     */
    template <class SpectrumType>
    class Checker2D: public Texture<SpectrumType>
    {
    public:
        Checker2D(void);
        Checker2D(float size, const SpectrumType& color1, const SpectrumType& color2);
        Checker2D(float sizeU, float sizeV, const SpectrumType& color1, const SpectrumType& color2) ;

        /*! \brief Get the type of the texture.
         *  \return Returns the type of the texture.
         */
        virtual typename Texture<SpectrumType>::Type getType() const {return Texture<SpectrumType>::Checker2D;}

        virtual SpectrumType getColor(const ShadingData<SpectrumType>& shadingDataObject) const;

        void setColors(const SpectrumType& color1, const SpectrumType& color2);

    private:
        SpectrumType color1_; ///< First color of checker texture.
        SpectrumType color2_; ///< Second color of checker texture.
        double sizeU_; ///< Size of texture patches in u-direction
        double sizeV_; ///< Size of texture patches in v-direction
    };
    
    

    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Checker2D<SpectrumType>::Checker2D(void)
        : color1_(0.0, 0.0, 0.0)
        , color2_(1.0, 1.0, 1.0)
        , sizeU_(1.0)
        , sizeV_(1.0)
    {
    }



    /*! \brief Constructor with parameters.
     *  \param[in] size Patch size.
     *  \param[in] color1 Color of first patch.
     *  \param[in] color2 Color of second patch.
     */
    template <class SpectrumType>
    Checker2D<SpectrumType>::Checker2D(float size, const SpectrumType& color1, const SpectrumType& color2)
        : color1_(color1)
        , color2_(color2)
        , sizeU_(size)
        , sizeV_(size)
    {
    }



    /*! \brief Constructor with parameters.
     *  \param[in] sizeU Patch size in u-direction.
     *  \param[in] sizeV Patch size in v-direction.
     *  \param[in] color1 Color of first patch.
     *  \param[in] color2 Color of second patch.
     */
    template <class SpectrumType>
    Checker2D<SpectrumType>::Checker2D(float sizeU, float sizeV, const SpectrumType& color1, const SpectrumType& color2)
        : color1_(color1)
        , color2_(color2)
        , sizeU_(sizeU)
        , sizeV_(sizeV)
    {
    }



    /*! \brief Get color for the texture.
     *  \param[in] shadingDataObject Shading object
     *  \return Returns reflected color.
     */
    template <class SpectrumType>
    SpectrumType Checker2D<SpectrumType>::getColor(const ShadingData<SpectrumType>& shadingDataObject) const
    {
        float u = static_cast<float>(shadingDataObject.localHitPoint2D_.x_ + EPS);
        float v = static_cast<float>(shadingDataObject.localHitPoint2D_.y_ + EPS);
        
        if ((static_cast<int>(floor(u / sizeU_)) + static_cast<int>(floor(v / sizeV_)) ) % 2 == 0)
        {
            return color1_;
        }
        else
        {
            return color2_;
        }
    }



    /*! \brief Set colors of the texture.
     *  \param[in] color1 Color of first patch.
     *  \param[in] color2 Color of second patch.
     */
    template <class SpectrumType>
    void Checker2D<SpectrumType>::setColors(const SpectrumType& color1, const SpectrumType& color2)
    {
        color1_ = color1;
        color2_ = color2;
    }

} // end of namespace iiit

#endif // __CHECKER2D_HPP__
