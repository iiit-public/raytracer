// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Point2D.hpp
 *  \author Mohamed Salem Koubaa
 *  \date 30.03.2015
 *  \brief Definition of class Point2D.
 *
 *  Definition of class Point2D representing a point in a plane with coodinates x and y.
 */

#ifndef __POINT2D_HPP__
#define __POINT2D_HPP__

#include <iostream>
#include <stdexcept>



namespace iiit
{

    /*! \brief Class of point in 2D-
     *
     *  Class represents a point in a plane with coordinates x and y.
     */
    class Point2D
    {
    public:
        // constructors
        Point2D();
        Point2D(double x, double y);
        Point2D(const Point2D& point);


        // destructor
        ~Point2D();

        // access operators
        double& operator()(unsigned int row, unsigned int col);
        double& operator()(unsigned int row);
        double& operator[](unsigned int row);
        double operator()(unsigned int row, unsigned int col) const;
        double operator()(unsigned int row) const;
        double operator[](unsigned int row) const;


        // operations
        bool operator==(const Point2D& rhs) const;
        bool operator!=(const Point2D& rhs) const;
        Point2D& operator=(const Point2D& rhs);
        Point2D operator-(void) const;
        Point2D operator-(const Point2D& rhs) const;
        Point2D operator+(const Point2D& rhs) const;
        Point2D operator*(const double a) const;
        double distance(const Point2D& point) const;
        double norm() const;

        double x_; //!< x-coordinate
        double y_; //!< y-coordinate
    };



    // inlined member functions

    /*! \brief Assignment operator.
     *
     *  Assign point elements with index beginning at 1.
     *  \param[in] row Row index.
     *  \param[in] col Column index.
     *  \return Reference to element.
     */
    inline double& Point2D::operator()(unsigned int row, unsigned int col)
    {
        switch (row)
        {
        case 1 :
            return x_;
            break;
        case 2 :
            return y_;
            break;
        default :
            throw std::out_of_range("Index of Point2D out of range.");
        }
    }



    /*! \brief Assignment operator.
     *
     *  Assign point elements with index beginning at 1.
     *  \param[in] row Row index.
     *  \return Reference to element.
     */
    inline double& Point2D::operator()(unsigned int row)
    {
        switch (row)
        {
        case 1 :
            return x_;
            break;
        case 2 :
            return y_;
            break;

        default :
            throw std::out_of_range("Index of Point2D out of range.");
        }
    }



    /*! \brief Assignment operator.
     *
     *  Assign point elements with index beginning at 0.
     *  \param[in] row Row index.
     *  \return Reference to element.
     */
    inline double& Point2D::operator[](unsigned int row)
    {
        switch (row)
        {
        case 0 :
            return x_;
            break;
        case 1 :
            return y_;
            break;
        default :
            throw std::out_of_range("Index of Point2D out of range.");
        }
    }



    /*! \brief Access operator.
     *
     *  Access point elements with index beginning at 1.
     *  \param[in] row Row index.
     *  \param[in] col Column index.
     *  \return Element.
     */
    inline double Point2D::operator()(unsigned int row, unsigned int col) const
    {
        switch (row)
        {
        case 1 :
            return x_;
            break;
        case 2 :
            return y_;
            break;

        default :
            throw std::out_of_range("Index of Point2D out of range.");
        }
    }



    /*! \brief Access operator.
     *
     *  Access point elements with index beginning at 1.
     *  \param[in] row Row index.
     *  \return Element.
     */
    inline double Point2D::operator()(unsigned int row) const
    {
        switch (row)
        {
        case 1 :
            return x_;
            break;
        case 2 :
            return y_;
            break;
        default :
            throw std::out_of_range("Index of Point2D out of range.");
        }
    }



    /*! \brief Access operator.
     *
     *  Access point elements with index beginning at 0.
     *  \param[in] row Row index.
     *  \return Element.
     */
    inline double Point2D::operator[](unsigned int row) const
    {
        switch (row)
        {
        case 0 :
            return x_;
            break;
        case 1 :
            return y_;
            break;
        default :
            throw std::out_of_range("Index of Point2D out of range.");
        }
    }



    /*! \brief Unary minus operator.
     *  \return Mirrored Point.
     */
    inline Point2D Point2D::operator-(void) const
    {
        return Point2D(-x_, -y_);
    }



    /*! \brief Vector between 2 points.
     *  \param[in] rhs Point.
     *  \return Vector.
     */
    inline Point2D Point2D::operator-(const Point2D& rhs) const
    {
        return Point2D(x_ - rhs.x_, y_ - rhs.y_);
    }



    /*! \brief Point after adding vector.
     *  \param[in] rhs Vector.
     *  \return Point.
     */
    inline Point2D Point2D::operator+(const Point2D& rhs) const
    {
        return Point2D(x_ + rhs.x_, y_ + rhs.y_);
    }


    /*! \brief Multiplication with scalar from the right.
     *  \param[in] a Scalar.
     *  \return Point.
     */
    inline Point2D Point2D::operator*(const double a) const
    {
         return Point2D(x_ * a, y_ * a);
    }



    // inlined non-member functions
    Point2D operator*(double a, const Point2D& rhs);

    /*! \brief Multiplication with scalar from the left.
     *  \param[in] a Scalar.
     *  \param[in] rhs Point.
     *  \return Point.
     */
    inline Point2D operator*(double a, const Point2D& rhs)
    {
        return Point2D(a * rhs.x_, a * rhs.y_);
    }

} // end of namespace iiit

#endif // __POINT2D_HPP__
