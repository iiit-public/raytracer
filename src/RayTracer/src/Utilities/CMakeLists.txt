# Project
project(raytracer)

# Add source files for source_group
set (SOURCE_UTILITIES
    ${CMAKE_CURRENT_SOURCE_DIR}/AbstractGrid.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Constants.h
    ${CMAKE_CURRENT_SOURCE_DIR}/HexGrid.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Matrix4x4.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Matrix4x4.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Normal.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Normal.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Point2D.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Point2D.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Point3D.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Point3D.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/QuarticSolver.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/QuarticSolver.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Quaternion.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Quaternion.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Ray.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Ray.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/RectGrid.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/RGBColor.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/RGBColor.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/ShadingData.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Transformation.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Transformation.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Vector3D.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/Vector3D.hpp
    PARENT_SCOPE
)