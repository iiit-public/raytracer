// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


///*! \file RGBColor.hpp
// *  \author Mohamed Salem Koubaa
// *  \date 04.02.2015
// *  \brief Definition of class RGBColor.
// *
// *  Definition of class RGBColor representing a color from its r,g,b-values.
// */
//
//#ifndef __RGBCOLOR_HPP__
//#define __RGBCOLOR_HPP__
//
//#include <iostream>
//#include <stdexcept>
//#include <algorithm>
//
//
//
///*! \brief Class represents RGB color.
// */
//class RGBColor
//{
//public:
//
//    RGBColor();
//    RGBColor(const RGBColor& other);
//    RGBColor(float gray);
//    RGBColor(float r, float g, float b);
//    ~RGBColor(void);
//    RGBColor operator* (float a) const;
//    RGBColor operator* (const RGBColor& color) const;
//    RGBColor& operator*= (float a);
//    RGBColor& operator+= (const RGBColor& color);
//    RGBColor& operator/= (float a);
//
//    void reset();
//    void set(unsigned int r, unsigned int g, unsigned int b);
//    void set(float r, float g, float b);
//    bool outOfGamut() const;
//    float getMax() const;
//    float operator()(unsigned int index) const;
//    unsigned int getR() const;
//    unsigned int getG() const;
//    unsigned int getB() const;
//    float getFloatR() const;
//    float getFloatG() const;
//    float getFloatB() const;
//
//    float r_; ///< Channel red.
//    float g_; ///< Channel green.
//    float b_; ///< Channel blue.
//
//    static const unsigned int trueColorMax_; ///< Maximum true color integer value.
//};
//
//
//
//// inlined member functions
//
///*! \brief Access operator.
// *  \param[in] index Row index.
// *  \return Element.
// *
// *  Access vector elements with index beginning at 1.
// */
//inline float RGBColor::operator()(unsigned int index) const
//{
//    switch (index)
//    {
//    case 1 :
//        return r_;
//        break;
//    case 2 :
//        return g_;
//        break;
//    case 3 :
//        return b_;
//        break;
//    default :
//        throw std::out_of_range("Index of RGBColor out of range.");
//    }
//}
//
//
//
///*! \brief Addition operator.
// *  \param[in] color Color to add.
// *  \return Resulting color.
// */
//inline RGBColor& RGBColor::operator+= (const RGBColor& color)
//{
//    r_ += color.r_;
//    g_ += color.g_;
//    b_ += color.b_;
//
//    return *this;
//}
//
//
//
///*! \brief Multiplication operator.
// *  \param[in] a Scaling factor.
// *  \return Resulting color.
// */
//inline RGBColor RGBColor::operator* (float a) const
//{
//    if (a < 0.0)
//    {
//        a = -a;
//    }
//    return RGBColor(r_ * a, g_ * a, b_ * a);
//}
//
//
//
///*! \brief Channel-wise multiplication.
// *  \param[in] color Color to multiply.
// *  \return Resulting color.
// */
//inline RGBColor RGBColor::operator* (const RGBColor& color) const
//{
//    return RGBColor(r_ * color.r_, g_ * color.g_, b_ * color.b_);
//}
//
//
//
///*! \brief Multiplication operator.
// *  \param[in] a Scaling factor.
// *  \return Resulting color.
// */
//inline RGBColor& RGBColor::operator*= (float a)
//{
//    r_ = r_ * a;
//    g_ = g_ * a;
//    b_ = b_ * a;
//
//    return *this;
//}
//
//
//
///*! \brief Division operator.
// *  \param[in] a Scaling factor.
// *  \return Resulting color.
// */
//inline RGBColor& RGBColor::operator/= (float a)
//{
//    r_ = r_ / a;
//    g_ = g_ / a;
//    b_ = b_ / a;
//
//    return *this;
//}
//
//
//
///*! \brief Resets color to black.
// */
//inline void RGBColor::reset()
//{
//    r_ = 0.0;
//    g_ = 0.0;
//    b_ = 0.0;
//}
//
//
//
///*! brief Set color.
// *  \param[in] r Value of red channel.
// *  \param[in] g Value of green channel.
// *  \param[in] b Value of blue channel.
// */
//inline void RGBColor::set(unsigned int r, unsigned int g, unsigned int b)
//{
//    r_ = static_cast<float>(r / trueColorMax_);
//    g_ = static_cast<float>(g / trueColorMax_);
//    b_ = static_cast<float>(b / trueColorMax_);
//}
//
//
//
///*! brief Set color.
// *  \param[in] r Value of red channel.
// *  \param[in] g Value of green channel.
// *  \param[in] b Value of blue channel.
// */
//inline void RGBColor::set(float r, float g, float b)
//{
//    r_ = r;
//    g_ = g;
//    b_ = b;
//}
//
//
//
///*! \brief Get red channel.
// *  \return Value of red channel.
// */
//inline unsigned int RGBColor::getR() const
//{
//    return static_cast<unsigned int>(r_ * trueColorMax_);
//}
//
//
//
///*! \brief Get green channel.
// *  \return Value of green channel.
// */
//inline unsigned int RGBColor::getG() const
//{
//    return static_cast<unsigned int>(g_ * trueColorMax_);
//}
//
//
//
///*! \brief Get blue channel.
// *  \return Value of blue channel.
// */
//inline unsigned int RGBColor::getB() const
//{
//    return static_cast<unsigned int>(b_ * trueColorMax_);
//}
//
//
///*! \brief Get float value of red channel.
// *  \return Float value of red channel.
// */
//inline float RGBColor::getFloatR() const
//{
//    return r_;
//}
//
//
//
///*! \brief Get float value of green channel.
// *  \return Float value of green channel.
// */
//inline float RGBColor::getFloatG() const
//{
//    return g_;
//}
//
//
//
///*! \brief Get float value of blue channel.
// *  \return Float value of blue channel.
// */
//inline float RGBColor::getFloatB() const
//{
//    return b_;
//}
//
//
//
///*! \brief Check if color is out of gamut.
// *  \return Returns TRUE if color is out of gamut, else FALSE.
// *
// *  A color is out of gamut when at least one of its channels exceeds the maximum displayable value
// *  of 1.
// */
//inline bool RGBColor::outOfGamut() const
//{
//    return ((r_ > 1.0) | (g_ > 1.0) | (b_ > 1.0));
//}
//
//
//
///*! \brief Returns maximum value of color channels.
// *  \return Maximum value.
// */
//inline float RGBColor::getMax() const
//{
//    return std::max<float>(std::max<float>(r_, g_), b_);
//}
//
//#endif // __RGBCOLOR_HPP__