// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file ShadingData.hpp
 *  \author Mohamed Salem Koubaa
 *  \date 16.03.2015
 *  \brief Definition of class ShadingData.
 */

#ifndef __SHADINGDATA_HPP__
#define __SHADINGDATA_HPP__

#include <memory>

#include "Normal.hpp"
#include "Point2D.hpp"
#include "Point3D.hpp"
#include "Ray.hpp"



namespace iiit
{
    
    template <class T> class Scene; // forward declaration
    template <class T> class Material; // forward declaration
    
    

    /*! \brief Class holds data used for shading.
     *
     *  This class holds all necessary data to shade an object. It is build for every ray-object
     *  intersection.
     */
    template <class SpectrumType>
    class ShadingData
    {
    public:
        ShadingData(std::weak_ptr<const Scene<SpectrumType> > scene);
        ShadingData(const ShadingData<SpectrumType>& shadingData);
        ~ShadingData(void);
        ShadingData<SpectrumType>& operator= (const ShadingData<SpectrumType>& shadingData);

        bool hitAnObject_; ///< Did the ray hit an object?
        Point3D hitPoint_; ///< Hit point in world coordinates.
        Normal normal_; ///< Normal at hit point.
        bool normalFlipped_; ///< Flag indicating whether the normal has been flipped for shading.
        Point3D localHitPoint3D_; ///< Hit point on 3D generic object coordinates.
        Point2D localHitPoint2D_; ///< Hit point on 2D object coordinates.
        std::weak_ptr<const Scene<SpectrumType> > scene_; ///< Scene reference for shading.
        Ray ray_; ///< Ray that hit an object (only valid, when hitAnObject is true).
        std::shared_ptr<const Material<SpectrumType> > material_; ///< Material that was hit.
        int depth_; ///< Recursion depth.

        //Vector3D direction_; //Direction for are lights.
    };
    
    
    
    /*! \brief Default constructor.
     *  \param[in] scene Reference to scene for shading.
     */
    template <class SpectrumType>
    ShadingData<SpectrumType>::ShadingData(std::weak_ptr<const Scene<SpectrumType> > scene)
        : hitAnObject_(false)
        , normalFlipped_(false)
        , scene_(scene)
        , material_()
        , depth_(0)
    {
    }
    
    
    
    /*! \brief Copy constructor.
     *  \param[in] shadingData ShadingData object to copy.
     */
    template <class SpectrumType>
    ShadingData<SpectrumType>::ShadingData(const ShadingData<SpectrumType>& shadingData)
        : hitAnObject_(shadingData.hitAnObject_)
        , hitPoint_(shadingData.hitPoint_)
        , normal_(shadingData.normal_)
        , normalFlipped_(shadingData.normalFlipped_)
        , localHitPoint2D_(shadingData.localHitPoint2D_)
        , localHitPoint3D_(shadingData.localHitPoint3D_)
        , scene_(shadingData.scene_)
        , ray_(shadingData.ray_)
        , material_(shadingData.material_)
        , depth_(shadingData.depth_)
        //, direction_(shadingData.direction_)
    {
    }
    
    
    
    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    ShadingData<SpectrumType>::~ShadingData(void)
    {
    }
    
    
    
    /*! \brief Assignment operator.
     *  \param[in] shadingData ShadingData object to be assigned.
     *  \return Assigned ShadingData object.
     */
    template <class SpectrumType>
    ShadingData<SpectrumType>& ShadingData<SpectrumType>::operator= (const ShadingData<SpectrumType>& shadingData)
    {
        hitAnObject_ = shadingData.hitAnObject_;
        localHitPoint3D_ = shadingData.localHitPoint3D_;
        localHitPoint2D_ = shadingData.localHitPoint2D_;
        hitPoint_ = shadingData.hitPoint_;
        normal_ = shadingData.normal_;
        normalFlipped_ = shadingData.normalFlipped_;
        scene_= shadingData.scene_;
        ray_ = shadingData.ray_;
        material_ = shadingData.material_;
        depth_ = shadingData.depth_;
        return *this;
    }
    
} // end of namespace iiit

#endif // __SHADINGDATA_HPP__
