// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Point3D.cpp
 *  \author Thomas Nuernberg
 *  \date 21.11.2014
 *  \brief Implementation of class Point3D.
 */

#include "Point3D.hpp"

#include <math.h>



namespace iiit
{

    /*! \brief Default constructor.
     * 
     *  Default constructor of Point in origin (0,0,0).
     */
    Point3D::Point3D()
        : x_(0), y_(0), z_(0)
    {}



    /*! \brief Constructor with initialization.
     *
     *  Constructor with explicit specification of coordinates.
     *  \param[in] x x-coordinate.
     *  \param[in] y y-coordinate.
     *  \param[in] z z-coordinate.
     */
    Point3D::Point3D(double x, double y, double z)
        : x_(x), y_(y), z_(z)
    {}



    /*! \brief Copy constructor.
     *  \param[in] point Point to copy.
     */
    Point3D::Point3D(const Point3D& point)
        : x_(point.x_),y_(point.y_),z_(point.z_)
    {}



    /*! \brief Constructor from position vector.
     *  \param[in] vector Position vector.
     */
    Point3D::Point3D(const Vector3D& vector)
        : x_(vector.x_),y_(vector.y_),z_(vector.z_)
    {}



    /*! \brief Constructor from 2D point and z-coordinate.
     *  \param[in] point 2D point.
     *  \param[in] z z-coordinate.
     */
    Point3D::Point3D(const Point2D& point, double z)
        : x_(point.x_), y_(point.y_), z_(z)
    {}



    /*! \brief Default destructor.
     */
    Point3D::~Point3D()
    {}



    /*! \brief Display operator.
     *  \param[in] os Output stream.
     *  \param[in] point Point to display.
     *  \return Output stream.
     */
    std::ostream& operator<<(std::ostream& os, const Point3D& point)
    {
        os << "[" << point.x_ << ", " << point.y_ << ", " << point.z_ << "]";
        return os;
    }



    /*! \brief Equality operator.
     *  \param[in] rhs Point to compare.
     *  \return Returns TRUE if points are equal, else FALSE.
     */
    bool Point3D::operator==(const Point3D& rhs) const
    {
        bool res = true;
        res = res & (x_ == rhs.x_);
        res = res & (y_ == rhs.y_);
        res = res & (z_ == rhs.z_);

        return res;
    }



    /*! \brief Inequality operator.
     *  \param[in] rhs Point to compare.
     *  \return Returns TRUE if points are unequal, else FALSE.
     */
    bool Point3D::operator!=(const Point3D& rhs) const
    {
        return !(this->operator==(rhs));
    }



    /*! \brief Assignment operator.
     *  \param[in] rhs Point to be assign.
     *  \return Reference of assigned Point.
     */
    Point3D& Point3D::operator=(const Point3D& rhs)
    {
        if (this == &rhs)
        {
            return *this;
        }
        x_ = rhs.x_;
        y_ = rhs.y_;
        z_ = rhs.z_;
        return *this;
    }



    /*! \brief Euclidean distance of 2 points.
     *  \param[in] point Distant point.
     *  \return Distance.
     */
    double Point3D::distance(const Point3D& point) const
    {
        return sqrt( (x_ - point.x_) * (x_ - point.x_) 
            + (y_ - point.y_) * (y_ - point.y_)
            + (z_ - point.z_) * (z_ - point.z_));
    }


    /*! \brief In place rotation around x axis.
     *  \param[in] phi Rotation angle in radians.
     */
    void Point3D::rotate_x(const double& phi)
    {
        double y_tmp, z_tmp;
        y_tmp = y_ * cos(phi) - z_ * sin(phi);
        z_tmp = y_ * sin(phi) + z_ * cos(phi);

        y_ = y_tmp;
        z_ = z_tmp;
    }


    /*! \brief In place rotation around y axis.
     *  \param[in] phi Rotation angle in radians.
     */
    void Point3D::rotate_y(const double& phi)
    {
        double x_tmp, z_tmp;
        x_tmp = x_ * cos(phi) + z_ * sin(phi);
        z_tmp = - x_ * sin(phi) + z_ * cos(phi);

        x_ = x_tmp;
        z_ = z_tmp;
    }


    /*! \brief In place rotation around z axis.
     *  \param[in] phi Rotation angle in radians.
     */
    void Point3D::rotate_z(const double& phi)
    {
        double x_tmp, y_tmp;
        x_tmp = x_ * cos(phi) - y_ * sin(phi);
        y_tmp = x_ * sin(phi) + y_ * cos(phi);

        x_ = x_tmp;
        y_ = y_tmp;
    }

} // end of namespace iiit
