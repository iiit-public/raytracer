// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Ray.cpp
 *  \author Mohamed Salem Koubaa
 *  \date 05.02.2015
 *  \brief Definition of class Ray.cpp.
 *
 *  Definition of class Ray.cpp representing a ray with an origin and a direction.
 */

#include "Ray.hpp"



namespace iiit
{

    /*! \brief Default constructor.
     */
    Ray::Ray()
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] origin Origin of the ray.
     *  \param[in] direction Ray direction.
     */
    Ray::Ray(const Point3D& origin, const Vector3D& direction)
        : origin_(origin)
        , direction_(direction)
    {
    }



    /*! \brief Copy constructor.
     *  \param[in] ray Ray to copy.
     */
    Ray::Ray(const Ray& ray)
        : origin_(ray.origin_)
        , direction_(ray.direction_)
    {
    }



    /*! \brief Default destructor.
     */
    Ray::~Ray()
    {
    }



    /*! \brief Sets data members of ray.
     *  \param[in] origin Point on ray.
     *  \param[in] direction Vector of ray direction.
     */
    void Ray::set(const Point3D& origin, const Vector3D& direction)
    {
        origin_ = origin;
        direction_ = direction;
    }

} // end of namespace iiit