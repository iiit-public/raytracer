// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Transformation.hpp
 *  \author Thomas Nuernberg
 *  \date 03.03.2015
 *  \brief Definition of class Transformation.
 *
 *  Definition of class Transformation representing a 4x4 transformation matrix used to convert
 *  between different coordinate systems and to transform objects.
 */

#ifndef __TRANSFORMATION_HPP__
#define __TRANSFORMATION_HPP__

#include "Matrix4x4.hpp"
#include "Point3D.hpp"
#include "Vector3D.hpp"
#include "Normal.hpp"
#include "Ray.hpp"



namespace iiit
{

    /*! \brief Class to perform affine transformations in homogeneous coordinates.
     *
     *  This class performs affine transformations in homogeneous coordinates. It mainly serves two
     *  purposes:
     *  -# Coordinate transformation between the camera and the world frame.
     *  -# Affine transformation of objects.
     *
     *  The camera frame is a cartesian coordinate system with the axes relative to the camera defined
     *  as pictured below. The z-axis points in the viewing direction, while the x and y-axes span the
     *  image plane, forming a right-handed system.
     *  \image html camera_frame.png
     *
     *  The world frame is cartesian coordinate system as depicted in the image below. The x and y-axis
     *  span the ground plane and the z-axis points straight upwards to form a right handed system.
     *  \image html world_frame.png
     */
    class Transformation
    {
    public:
        Transformation();
        Transformation(const Matrix4x4& matrix);
        Transformation(const Matrix4x4& matrix, const Matrix4x4& invMatrix);
        Transformation(const Point3D& eye, const Point3D& lookat, const Vector3D& up);
        Transformation(const Transformation& other);
        ~Transformation();

        Transformation& operator=(const Transformation& other);

        Point3D camToWorld(const Point3D& point) const;
        Point3D worldToCam(const Point3D& point) const;
        Vector3D camToWorld(const Vector3D& vector) const;
        Vector3D worldToCam(const Vector3D& vector) const;
        Ray camToWorld(const Ray& ray) const;
        Ray worldToCam(const Ray& ray) const;

        Point3D transform(const Point3D& point) const;
        Point3D invTransform(const Point3D& point) const;
        Vector3D transform(const Vector3D& vector) const;
        Vector3D invTransform(const Vector3D& vector) const;
        Normal transform(const Normal& normal) const;
        Normal invTransform(const Normal& normal) const;
        Ray transform(const Ray& ray) const;
        Ray invTransform(const Ray& ray) const;

        void addTransformation(Matrix4x4 transformation);
        void addTransformation(Matrix4x4 transformation, Matrix4x4 invTransformation);

        Matrix4x4 getMatrix() const;
        void setMatrix(const Matrix4x4& matrix);
        Matrix4x4 getInvMatrix() const;
        void setInvMatrix(const Matrix4x4& invMatrix);
        
    private:
        Matrix4x4 matrix_; ///< Transformation matrix.
        Matrix4x4 invMatrix_; ///< Inverse Transformation matrix.
    };

} // end of namespace iiit

#endif // __TRANSFORMATION_HPP__