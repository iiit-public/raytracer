// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file QuarticSolver.hpp
 *  \author Thomas Nuernberg
 *  \date 09.06.2015
 *  \brief Declaration of class to solve quartic equation.
 */

#ifndef __QUARTICSOLVER_HPP__
#define __QUARTICSOLVER_HPP__

namespace iiit
{

    /*! \brief Class solves quartic equations.
     *
     *  Code taken from
     *  http://www.gamedev.net/topic/451048-best-way-of-solving-a-polynomial-of-the-fourth-degree/.
     */
    class QuarticSolver
    {
    public:
        //static bool solveQuartic(double a, double b, double c, double d, double e, double& root);
        //static bool solveQuadraticOther(double a, double b, double c, double &root);
        //static bool solveCubic(double a, double b, double c, double d, double& root);
        //static bool solveQuadratic(double a, double b, double c, double& root);

        static int solveQuadric(double c[3], double s[2]);
        static int solveCubic(double c[4], double s[3]);
        static int solveQuartic(double c[5], double s[4]);
    };

} // end of namespace iiit

#endif // __TORUS_HPP__