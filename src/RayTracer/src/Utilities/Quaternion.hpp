// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Quaternion.hpp
 *  \author Thomas Nuernberg
 *  \date 27.25.2016
 *  \brief Definition of class Quaternion.
 *
 *  Definition of class Quaternion.
 */

#ifndef __QUATERNION_HPP__
#define __QUATERNION_HPP__

#include <iostream>
#include <stdexcept>

#include "Vector3D.hpp"



namespace iiit
{

    /*! \brief Class for quaternions.
     */
    class Quaternion
    {
    public:
        // constructors
        Quaternion();
        Quaternion(double x0, double x1, double x2, double x3);
        Quaternion(const Quaternion& other);

        // destructor
        ~Quaternion();

        // access operators
        double& operator()(unsigned int row, unsigned int col);
        double& operator()(unsigned int row);
        double& operator[](unsigned int row);
        double operator()(unsigned int row, unsigned int col) const;
        double operator()(unsigned int row) const;
        double operator[](unsigned int row) const;
        void set(double x0, double x1, double x2, double x3);

        // display operator
        friend std::ostream& operator<<(std::ostream& os, const Quaternion& quaternion);

        // operations
        bool operator==(const Quaternion& rhs) const;
        bool operator!=(const Quaternion& rhs) const;
        Quaternion& operator=(const Quaternion& rhs);
        Quaternion operator*(const Quaternion& rhs) const;
        Quaternion conjugate() const;
        static Vector3D rotateVector(Vector3D vector, Vector3D axis, double angle);

        double x0_; ///< real component
        double x1_; ///< i component
        double x2_; ///< j component
        double x3_; ///< k component
    };



    // inlined member functions

    /*! \brief Assignment operator.
     *
     *  Assign quaternion elements with index beginning at 1.
     *  \param[in] row Row index.
     *  \param[in] col Column index.
     *  \return Reference to element.
     */
    inline double& Quaternion::operator()(unsigned int row, unsigned int col)
    {
        switch (row)
        {
        case 1 :
            return x0_;
            break;
        case 2 :
            return x1_;
            break;
        case 3 :
            return x2_;
            break;
        case 4 :
            return x3_;
            break;
        default :
            throw std::out_of_range("Index of Quaternion out of range.");
        }
    }



    /*! \brief Assignment operator.
     *
     *  Assign quaternion elements with index beginning at 1.
     *  \param[in] row Row index.
     *  \return Reference to element.
     */
    inline double& Quaternion::operator()(unsigned int row)
    {
        switch (row)
        {
        case 1 :
            return x0_;
            break;
        case 2 :
            return x1_;
            break;
        case 3 :
            return x2_;
            break;
        case 4 :
            return x3_;
            break;
        default :
            throw std::out_of_range("Index of Quaternion out of range.");
        }
    }


    /*! \brief Assignment operator.
     *
     *  Assign quaternion elements with index beginning at 0.
     *  \param[in] row Row index.
     *  \return Reference to element.
     */
    inline double& Quaternion::operator[](unsigned int row)
    {
        switch (row)
        {
        case 0 :
            return x0_;
            break;
        case 1 :
            return x1_;
            break;
        case 2 :
            return x2_;
            break;
        case 3 :
            return x3_;
            break;
        default :
            throw std::out_of_range("Index of Quaternion out of range.");
        }
    }



    /*! \brief Access operator.
     *
     *  Access quaternion elements with index beginning at 0.
     *  \param[in] row Row index.
     *  \param[in] col Column index.
     *  \return Element.
     */
    inline double Quaternion::operator()(unsigned int row, unsigned int col) const
    {
        switch (row)
        {
        case 0 :
            return x0_;
            break;
        case 1 :
            return x1_;
            break;
        case 2 :
            return x2_;
            break;
        case 3 :
            return x3_;
            break;
        default :
            throw std::out_of_range("Index of Quaternion out of range.");
        }
    }



    /*! \brief Access operator.
     *
     *  Access quaternion elements with index beginning at 1.
     *  \param[in] row Row index.
     *  \return Element.
     */
    inline double Quaternion::operator()(unsigned int row) const
    {
        switch (row)
        {
        case 1 :
            return x0_;
            break;
        case 2 :
            return x1_;
            break;
        case 3 :
            return x2_;
            break;
        case 4 :
            return x3_;
            break;
        default :
            throw std::out_of_range("Index of Quaternion out of range.");
        }
    }



    /*! \brief Access operator.
     *
     *  Access quaternion elements with index beginning at 0.
     *  \param[in] row Row index.
     *  \return Element.
     */
    inline double Quaternion::operator[](unsigned int row) const
    {
        switch (row)
        {
        case 0 :
            return x0_;
            break;
        case 1 :
            return x1_;
            break;
        case 2 :
            return x2_;
            break;
        case 3 :
            return x2_;
            break;
        default :
            throw std::out_of_range("Index of Quaternion out of range.");
        }
    }



    /*! \brief Set quaternion elements.
     *  \param[in] x0 real coordinate of quaternion.
     *  \param[in] x1 i-coordinate of quaternion.
     *  \param[in] x2 j-coordinate of quaternion.
     *  \param[in] x3 k-coordinate of quaternion.
     */
    inline void Quaternion::set(double x0, double x1, double x2, double x3)
    {
        x0_ = x0;
        x1_ = x1;
        x2_ = x2;
        x3_ = x3;
    }



    /*! \brief Hamilton product.
     *  \param[in] rhs Quaternion.
     *  \return Result of Hamilton product.
     */
    inline Quaternion Quaternion::operator*(const Quaternion& rhs) const
    {
        return Quaternion(
            x0_ * rhs.x0_ + x1_ * rhs.x1_ + x2_ * rhs.x2_ + x3_ * rhs.x3_,
            x0_ * rhs.x1_ + x1_ * rhs.x0_ + x2_ * rhs.x3_ - x3_ * rhs.x2_,
            x0_ * rhs.x2_ - x1_ * rhs.x3_ + x2_ * rhs.x0_ + x3_ * rhs.x1_,
            x0_ * rhs.x3_ + x1_ * rhs.x2_ - x2_ * rhs.x1_ + x3_ * rhs.x0_);
    }



    /*! \brief Conjugation operator.
     *  \return Result of conjugation.
     */
    inline Quaternion Quaternion::conjugate() const
    {
        return Quaternion(x0_, -x1_, -x2_, -x3_);
    }

} // end of namespace iiit

#endif // __QUATERNION_HPP__