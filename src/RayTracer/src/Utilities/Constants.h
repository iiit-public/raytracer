// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Constants.h
 *  \author Thomas Nuernberg
 *  \date 21.11.2014
 *  \brief Various constants.
 */

#ifndef __CONSTANTS_H__
#define __CONSTANTS_H__

#include <limits>



namespace iiit
{

    const double PI = 3.1415926535897932384; ///< Constant \f$\pi\f$.
    const double TWO_PI = 6.2831853071795864769; ///< Constant \f$2\pi\f$.
    const double INV_PI = 0.3183098861837906715; ///< Constant \f$\frac{1}{\pi}\f$.
    const double INV_TWO_PI = 0.1591549430918953358; ///< Constant \f$\frac{1}{2\pi}\f$.
    const double PI_2 = 1.5707963267948966192; ///< Constant \f$\frac{\pi}{2}\f$.
    const double PI_4 = 0.78539816339744830962; ///< Constant \f$\frac{\pi}{4}\f$.
    const double SQRT_TWO = 1.4142135623730950488016887242097; ///< Constant \f$\sqrt{2}\f$.
    const double SQRT_THREE = 1.7320508075688772935274463415058; ///< Constant \f$\sqrt{3}\f$.
    
    const unsigned int TRUE_COLOR_MAX = 255; ///< Maximum true color integer value.
    const unsigned int HIGH_COLOR_MAX = 65535; ///< Maximum high color integer value.
    const double EPS = 1e10 * std::numeric_limits<double>::epsilon(); ///< Constant \f$\epsilon > 0\f$.
    const double HUGE_VALUE = 1e9; ///< Arbitrary big number.

} // end of namespace iiit

#endif // __CONSTANTS_H_
