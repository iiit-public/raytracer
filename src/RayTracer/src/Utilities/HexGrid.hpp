// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file HexGrid.hpp
 *  \author Maximilian Schambach
 *  \date 27.02.2019
 *  \brief Definition of class HexGrid
 *
 *  Definition of class HexGrid representing a collection of grid points on hexagonal grid.
 */

#ifndef __HEXGRID_HPP__
#define __HEXGRID_HPP__

#include "AbstractGrid.hpp"
#include "Spectrum/CoefficientSpectrum.hpp"
#include "Utilities/Constants.h"


namespace iiit
{
    template <class SpectrumType>
    class HexGrid : public AbstractGrid<SpectrumType>
    {
    public:
        HexGrid();
        HexGrid(double x_max, double y_max,
                double offset_x, double offset_y,
                double spacing, double rotation,
                double phi, double theta,
                double z,
                bool is_coded);
        HexGrid(const HexGrid& grid);

        ~HexGrid();
    };

    /*! \brief Default constructor.
     *
     *  Default constructor.
     */

    template <class SpectrumType>
    HexGrid<SpectrumType>::HexGrid()
    {}


    /*! \brief Constructor with initialization by grid spacing
     *
     *  Constructor with explicit specification of coordinates.
     *  \param[in] x_max Maximum x coordinate value of grid points.
     *  \param[in] y_max Maximum y coordinate value of grid points.
     *  \param[in] offset_x Grid offset in x coordinate.
     *  \param[in] offset_y Grid offset in y coordinate.
     *  \param[in] spacing Grid spacing.
     *  \param[in] rotation Grid roation in xy plane.
     *  \param[in] phi Grid tilt phi (rotation around x-axis).
     *  \param[in] theta Grid tilt theta (rotation around y-axis).
     *  \param[in] z z-coordinate of (untilted) grid.
     */
    template <class SpectrumType>
    HexGrid<SpectrumType>::HexGrid(double x_max, double y_max,
                                   double offset_x, double offset_y,
                                   double spacing, double rotation,
                                   double phi, double theta,
                                   double z,
                                   bool is_coded)
    {
        // Calculate a_x, a_y and b_x, b_y
        double a_x, a_y, b_x, b_y;
        Point3D a(spacing, 0.0, 0.0);
        Point3D b(0.5*spacing, 0.5 * SQRT_THREE * spacing, 0.0);

        if (rotation != 0.0)
        {
            a.rotate_z(rotation);
            b.rotate_z(rotation);
        }

        a_x = a.x_;
        a_y = a.y_;
        b_x = b.x_;
        b_y = b.y_;

        // call standard constructor methods
        HexGrid<SpectrumType>::set_vals(x_max, y_max, offset_x, offset_y, a_x, a_y, b_x, b_y, phi, theta, z, is_coded);
        HexGrid<SpectrumType>::calc_points();
    }


    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    HexGrid<SpectrumType>::~HexGrid()
    {}

} // end of namespace iiit

#endif // __HEXGRID_HPP__
