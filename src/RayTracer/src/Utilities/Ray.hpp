// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Ray.hpp
 *  \author Mohamed Salem Koubaa
 *  \date 05.02.2015
 *  \brief Definition of class Ray.hpp.
 *
 *  Definition of class Ray.cpp representing a ray with an origin and a direction.
 */

#ifndef __RAY__
#define __RAY__

#include "Point3D.hpp"
#include "Vector3D.hpp"



namespace iiit
{

    /*! \brief Class of ray.
     *
     *  Class represents a ray in 3D space with origin point and direction vector
     */
    class Ray
    {
    public:
        Ray(void); // Default Constructor
        Ray(const Point3D& origin, const Vector3D& direction); //Constructor
        Ray(const Ray& ray) ; // Copy constructor
        ~Ray(void); //Destructor

        Ray& operator=(const Ray& rhs); // Assignment Operator

        void set(const Point3D& origin, const Vector3D& direction);

        Point3D origin_; ///< Ray origin.
        Vector3D direction_ ; ///< Ray direction.
    };



    //inlined member functions

    /*! \brief Assignment Operator.
     *  \param[in] rhs Ray to assign.
     *  \return Assigned ray.
     */
    inline Ray& Ray::operator=(const Ray& rhs)
    {
        direction_ = rhs.direction_;
        origin_ = rhs.origin_;

        return *this;
    }

} // end of namespace iiit

#endif // __RAY__