// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Transformation.cpp
 *  \author Mohamed Salem Koubaa
 *  \date 16.03.2015
 *  \brief Implementation of class Transformation.
 */

#include "Transformation.hpp"

#include <stdexcept>



namespace iiit
{

    /*! \brief Default constructor.
     */
    Transformation::Transformation()
        : matrix_()
        , invMatrix_()
    {
    }



    /*! \brief Constructor with matrix initialization.
     *  \param[in] matrix 4x4 Matrix of homogeneous coordinate transformation.
     *  \exception std::runtime_error if matrix is singular.
     */
    Transformation::Transformation(const Matrix4x4& matrix)
        : matrix_(matrix)
        , invMatrix_(matrix.inverse())
    {
    }



    /*! \brief Default destructor.
     */
    Transformation::~Transformation()
    {
    }


    /*! \brief Assignment operator.
     *  \param[in] other Transformation to be assigned.
     *  \return Assigned transformation.
     */
    Transformation& Transformation::operator=(const Transformation& other)
    {
        matrix_ = other.getMatrix();
        invMatrix_ = other.getInvMatrix();
        return *this;
    }



    /*! \brief Constructor with matrix and inverse matrix initialization.
     *  \param[in] matrix 4x4 Matrix of homogeneous coordinate transformation.
     *  \param[in] invMatrix 4x4 Matrix of inverse homogeneous coordinate transformation.
     */
    Transformation::Transformation(const Matrix4x4& matrix, const Matrix4x4& invMatrix)
        : matrix_(matrix)
        , invMatrix_(invMatrix)
    {
    }



    /*! \brief Creates transformation from camera origin, point of interest and up vector.
     *  \param[in] eye %Camera position.
     *  \param[in] lookat Point of interest.
     *  \param[in] up Up vector.
     *  \exception std::runtime_error if up vector and viewing direction are linearly dependent.
     */
    Transformation::Transformation(const Point3D& eye, const Point3D& lookat, const Vector3D& up)
    {
        Vector3D dir = lookat - eye;
        dir.normalize();

        Vector3D temp = up;
        temp.normalize();
        Vector3D xc = -temp.crossProduct(dir);

        if (xc.length() == 0.0)
        {
            throw std::runtime_error("Direction vector and up vector linearly dependent.");
        }

        Vector3D newUp = dir.crossProduct(xc);

        matrix_.m_[0][3] = eye.x_;
        matrix_.m_[1][3] = eye.y_;
        matrix_.m_[2][3] = eye.z_;
        matrix_.m_[3][3] = 1;

        matrix_.m_[0][0] = xc.x_;
        matrix_.m_[1][0] = xc.y_;
        matrix_.m_[2][0] = xc.z_;
        matrix_.m_[3][0] = 0;

        matrix_.m_[0][1] = newUp.x_;
        matrix_.m_[1][1] = newUp.y_;
        matrix_.m_[2][1] = newUp.z_;
        matrix_.m_[3][1] = 0;

        matrix_.m_[0][2] = dir.x_;
        matrix_.m_[1][2] = dir.y_;
        matrix_.m_[2][2] = dir.z_;
        matrix_.m_[3][2] = 0;

        invMatrix_ = matrix_.inverse();
    }



    /*! \brief Copy constructor.
     *  \param[in] other Transformation to copy.
     */
    Transformation::Transformation(const Transformation& other)
        : matrix_(other.getMatrix())
        , invMatrix_(other.getInvMatrix())
    {
    }



    /*! \brief Transform point from camera to world frame.
     *  \param[in] point Point in camera frame.
     *  \return Transformed point in world frame.
     */
    Point3D Transformation::camToWorld(const Point3D& point) const
    {
        return transform(point);
    }



    /*! \brief Transform point from world to camera frame.
     *  \param[in] point Point in world frame.
     *  \return Transformed point in camera frame.
     */
    Point3D Transformation::worldToCam(const Point3D& point) const
    {
        return invTransform(point);
    }



    /*! \brief Transform vector from camera to world frame.
     *  \param[in] vector Vector in camera frame.
     *  \return Transformed vector in world frame.
     */
    Vector3D Transformation::camToWorld(const Vector3D& vector) const
    {
        return transform(vector);
    }



    /*! \brief Transform vector from world to camera frame.
     *  \param[in] vector Vector in world frame.
     *  \return Transformed vector in camera frame.
     */
    Vector3D Transformation::worldToCam(const Vector3D& vector) const
    {
        return invTransform(vector);
    }



    /*! \brief Transform ray from camera to world frame.
     *  \param[in] ray Ray in camera frame.
     *  \return Transformed ray in world frame.
     */
    Ray Transformation::camToWorld(const Ray& ray) const
    {
        return transform(ray);
    }



    /*! \brief Transform ray from world to camera frame.
     *  \param[in] ray Ray in world frame.
     *  \return Transformed ray in camera frame.
     */
    Ray Transformation::worldToCam(const Ray& ray) const
    {
        return invTransform(ray);
    }



    /*! \brief Transform point.
     *  \param[in] point Point to transform.
     *  \return Transformed point.
     */
    Point3D Transformation::transform(const Point3D& point) const
    {
        double x = point.x_;
        double y = point.y_;
        double z = point.z_;

        double xp = matrix_.m_[0][0] * x + matrix_.m_[0][1] * y + matrix_.m_[0][2] * z + matrix_.m_[0][3];
        double yp = matrix_.m_[1][0] * x + matrix_.m_[1][1] * y + matrix_.m_[1][2] * z + matrix_.m_[1][3];
        double zp = matrix_.m_[2][0] * x + matrix_.m_[2][1] * y + matrix_.m_[2][2] * z + matrix_.m_[2][3];
        double wp = matrix_.m_[3][0] * x + matrix_.m_[3][1] * y + matrix_.m_[3][2] * z + matrix_.m_[3][3];

        if (wp == 1.0)
        {
            return Point3D (xp, yp, zp);
        }
        else
        {
            return Point3D (xp / wp, yp / wp, zp / wp);
        }
    }



    /*! \brief Inverse-transform point.
     *  \param[in] point Point to transform.
     *  \return Transformed point.
     */
    Point3D Transformation::invTransform(const Point3D& point) const
    {
        double x = point.x_;
        double y = point.y_;
        double z = point.z_;

        double xp = invMatrix_(1, 1) * x + invMatrix_(1, 2) * y + invMatrix_(1, 3) * z + invMatrix_(1, 4);
        double yp = invMatrix_(2, 1) * x + invMatrix_(2, 2) * y + invMatrix_(2, 3) * z + invMatrix_(2, 4);
        double zp = invMatrix_(3, 1) * x + invMatrix_(3, 2) * y + invMatrix_(3, 3) * z + invMatrix_(3, 4);
        double wp = invMatrix_(4, 1) * x + invMatrix_(4, 2) * y + invMatrix_(4, 3) * z + invMatrix_(4, 4);

        if (wp == 1)
        {
            return Point3D (xp, yp, zp);
        }
        else
        {
            return Point3D (xp / wp, yp / wp, zp / wp);
        }
    }



    /*! \brief Transform vector.
     *  \param[in] vector Vector to transform.
     *  \return Transformed vector.
     */
    Vector3D Transformation::transform(const Vector3D& vector) const
    {
        double x = vector.x_;
        double y = vector.y_;
        double z = vector.z_;

        return Vector3D(matrix_.m_[0][0] * x + matrix_.m_[0][1] * y + matrix_.m_[0][2] * z,
                        matrix_.m_[1][0] * x + matrix_.m_[1][1] * y + matrix_.m_[1][2] * z,
                        matrix_.m_[2][0] * x + matrix_.m_[2][1] * y + matrix_.m_[2][2] * z);
    }



    /*! \brief Inverse-transform vector.
     *  \param[in] vector Vector to transform.
     *  \return Transformed vector.
     */
    Vector3D Transformation::invTransform(const Vector3D& vector) const
    {
        double x = vector.x_;
        double y = vector.y_;
        double z = vector.z_;

        return Vector3D(invMatrix_.m_[0][0] * x + invMatrix_.m_[0][1] * y + invMatrix_.m_[0][2] * z,
                        invMatrix_.m_[1][0] * x + invMatrix_.m_[1][1] * y + invMatrix_.m_[1][2] * z,
                        invMatrix_.m_[2][0] * x + invMatrix_.m_[2][1] * y + invMatrix_.m_[2][2] * z);
    }



    /*! \brief Transform normal.
     *  \param[in] normal Normal to transform.
     *  \return Transformed normal.
     */
    Normal Transformation::transform(const Normal& normal) const
    {
        double x = normal.x_;
        double y = normal.y_;
        double z = normal.z_;

        return Normal(invMatrix_.m_[0][0] * x + invMatrix_.m_[1][0] * y + invMatrix_.m_[2][0] * z,
                      invMatrix_.m_[0][1] * x + invMatrix_.m_[1][1] * y + invMatrix_.m_[2][1] * z,
                      invMatrix_.m_[0][2] * x + invMatrix_.m_[1][2] * y + invMatrix_.m_[2][2] * z);
    }



    /*! \brief Inverse-transform normal.
     *  \param[in] normal Normal to transform.
     *  \return Transformed normal.
     */
    Normal Transformation::invTransform(const Normal& normal) const
    {
        double x = normal.x_;
        double y = normal.y_;
        double z = normal.z_;

        return Normal(matrix_.m_[0][0] * x + matrix_.m_[1][0] * y + matrix_.m_[2][0] * z,
                      matrix_.m_[0][1] * x + matrix_.m_[1][1] * y + matrix_.m_[2][1] * z,
                      matrix_.m_[0][2] * x + matrix_.m_[1][2] * y + matrix_.m_[2][2] * z);
    }



    /*! \brief Transform ray.
     *  \param[in] ray Ray to transform.
     *  \return Transformed ray.
     */
    Ray Transformation::transform(const Ray& ray) const
    {
        return Ray(transform(ray.origin_), transform(ray.direction_));
    }



    /*! \brief Inverse-transform Ray.
     *  \param[in] ray Ray to transform.
     *  \return Transformed ray.
     */
    Ray Transformation::invTransform(const Ray& ray) const
    {
        return Ray(invTransform(ray.origin_), invTransform(ray.direction_));
    }



    /*! \brief Adds transformation to the current composition of transformations.
     *  \param[in] transformation Transformation-matrix to add.
     *
     *  Also calculates the inverse of the given transformation matrix.
     */
    void Transformation::addTransformation(Matrix4x4 transformation)
    {
        matrix_ = transformation * matrix_;
        invMatrix_ = invMatrix_ * transformation.inverse();
    }



    /*! \brief Adds transformation to the current composition of transformations.
     *  \param[in] transformation Transformation-matrix to add.
     *  \param[in] invTransformation Inverse of transformation-matrix to add.
     */
    void Transformation::addTransformation(Matrix4x4 transformation, Matrix4x4 invTransformation)
    {
        matrix_ = transformation * matrix_;
        invMatrix_ = invMatrix_ * invTransformation;
    }


    /*! \brief Get matrix for transforming from camera to world frame.
     *  \return 4x4 transformation matrix.
     *
     *  This method returns the transformation matrix for converting from camera frame to world frame in
     *  homogeneous coordinates.
     */
    Matrix4x4 Transformation::getMatrix() const
    {
        return matrix_;
    }



    /*! \brief Set matrix for transforming from camera to world frame.
     *  \param[in] matrix 4x4 transformation matrix.
     *
     *  This method sets the transformation matrix for converting from camera frame to world frame in
     *  homogeneous coordinates.
     *  \attention This method does not adjust the matrix for transforming in the other direction.
     */
    void Transformation::setMatrix(const Matrix4x4& matrix)
    {
        matrix_ = matrix;
    }



    /*! \brief Get matrix for transforming from world to camera frame.
     *  \return 4x4 transformation matrix.
     *
     *  This method returns the transformation matrix for converting from world frame to camera frame in
     *  homogeneous coordinates.
     */
    Matrix4x4 Transformation::getInvMatrix() const
    {
        return invMatrix_;
    }



    /*! \brief Set matrix for transforming from world to camera frame.
     *  \param[in] invMatrix 4x4 transformation matrix.
     *
     *  This method sets the transformation matrix for converting from world frame to camera frame in
     *  homogeneous coordinates.
     *  \attention This method does not adjust the matrix for transforming in the other direction.
     */
    void Transformation::setInvMatrix(const Matrix4x4& invMatrix)
    {
        invMatrix_ = invMatrix;
    }

} // end of namespace iiit