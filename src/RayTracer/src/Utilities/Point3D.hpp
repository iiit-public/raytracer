// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Point3D.hpp
 *  \author Thomas Nuernberg
 *  \date 21.11.2014
 *  \brief Definition of class Point3D.
 *
 *  Definition of class Point3D representing a point in 3D space with coordinates x, y, and z.
 */

#ifndef __POINT3D_HPP__
#define __POINT3D_HPP__

#include <iostream>
#include <stdexcept>

#include "Vector3D.hpp"
#include "Point2D.hpp"



namespace iiit
{

    /*! \brief Class of point in 3D space.
     *
     *  Class represents a point in 3D space with coordinates x, y, and z.
     */
    class Point3D
    {
    public:
        // constructors
        Point3D();
        Point3D(double x, double y, double z);
        Point3D(const Point3D& point);
        Point3D(const Vector3D& vector);
        Point3D(const Point2D& point, double z);

        // destructor
        ~Point3D();

        // access operators
        double& operator()(unsigned int row, unsigned int col);
        double& operator()(unsigned int row);
        double& operator[](unsigned int row);
        double operator()(unsigned int row, unsigned int col) const;
        double operator()(unsigned int row) const;
        double operator[](unsigned int row) const;
        void set(double x, double y, double z);

        // display operator
        friend std::ostream& operator<<(std::ostream& os, const Point3D& point);

        // operations
        bool operator==(const Point3D& rhs) const;
        bool operator!=(const Point3D& rhs) const;
        Point3D& operator=(const Point3D& rhs);
        Point3D operator-(void) const;
        Vector3D operator-(const Point3D& rhs) const;
        Point3D operator+(const Vector3D& rhs) const;
        Point3D operator-(const Vector3D& rhs) const;
        Point3D operator*(const double a) const;
        double distance(const Point3D& point) const;

        double x_; //!< x-coordinate
        double y_; //!< y-coordinate
        double z_; //!< z-coordinate

        void rotate_x(const double& phi);
        void rotate_y(const double& phi);
        void rotate_z(const double& phi);
    };



    // inlined member functions

    /*! \brief Assignment operator.
     *
     *  Assign point elements with index beginning at 1.
     *  \param[in] row Row index.
     *  \param[in] col Column index.
     *  \return Reference to element.
     */
    inline double& Point3D::operator()(unsigned int row, unsigned int col)
    {
        switch (row)
        {
        case 1 :
            return x_;
            break;
        case 2 :
            return y_;
            break;
        case 3 :
            return z_;
            break;
        default :
            throw std::out_of_range("Index of Point3D out of range.");
        }
    }



    /*! \brief Assignment operator.
     *
     *  Assign point elements with index beginning at 1.
     *  \param[in] row Row index.
     *  \return Reference to element.
     */
    inline double& Point3D::operator()(unsigned int row)
    {
        switch (row)
        {
        case 1 :
            return x_;
            break;
        case 2 :
            return y_;
            break;
        case 3 :
            return z_;
            break;
        default :
            throw std::out_of_range("Index of Point3D out of range.");
        }
    }



    /*! \brief Assignment operator.
     *
     *  Assign point elements with index beginning at 0.
     *  \param[in] row Row index.
     *  \return Reference to element.
     */
    inline double& Point3D::operator[](unsigned int row)
    {
        switch (row)
        {
        case 0 :
            return x_;
            break;
        case 1 :
            return y_;
            break;
        case 2 :
            return z_;
            break;
        default :
            throw std::out_of_range("Index of Point3D out of range.");
        }
    }



    /*! \brief Access operator.
     *
     *  Access point elements with index beginning at 1.
     *  \param[in] row Row index.
     *  \param[in] col Column index.
     *  \return Element.
     */
    inline double Point3D::operator()(unsigned int row, unsigned int col) const
    {
        switch (row)
        {
        case 1 :
            return x_;
            break;
        case 2 :
            return y_;
            break;
        case 3 :
            return z_;
            break;
        default :
            throw std::out_of_range("Index of Point3D out of range.");
        }
    }



    /*! \brief Access operator.
     *
     *  Access point elements with index beginning at 1.
     *  \param[in] row Row index.
     *  \return Element.
     */
    inline double Point3D::operator()(unsigned int row) const
    {
        switch (row)
        {
        case 1 :
            return x_;
            break;
        case 2 :
            return y_;
            break;
        case 3 :
            return z_;
            break;
        default :
            throw std::out_of_range("Index of Point3D out of range.");
        }
    }



    /*! \brief Access operator.
     *
     *  Access point elements with index beginning at 0.
     *  \param[in] row Row index.
     *  \return Element.
     */
    inline double Point3D::operator[](unsigned int row) const
    {
        switch (row)
        {
        case 0 :
            return x_;
            break;
        case 1 :
            return y_;
            break;
        case 2 :
            return z_;
            break;
        default :
            throw std::out_of_range("Index of Point3D out of range.");
        }
    }



    /*! \brief Set point elements.
     *  \param[in] x X-coordinate of point.
     *  \param[in] y Y-coordinate of point.
     *  \param[in] z Z-coordinate of point.
     */
    inline void Point3D::set(double x, double y, double z)
    {
        x_ = x;
        y_ = y;
        z_ = z;
    }



    /*! \brief Unary minus operator.
     *  \return Mirrored Point.
     */
    inline Point3D Point3D::operator-(void) const
    {
        return Point3D(-x_, -y_, -z_);
    }



    /*! \brief Vector between 2 points.
     *  \param[in] rhs Point.
     *  \return Vector.
     */
    inline Vector3D Point3D::operator-(const Point3D& rhs) const
    {
        return Vector3D(x_ - rhs.x_, y_ - rhs.y_, z_ - rhs.z_);
    }



    /*! \brief Point after adding vector.
     *  \param[in] rhs Vector.
     *  \return Point.
     */
    inline Point3D Point3D::operator+(const Vector3D& rhs) const
    {
        return Point3D(x_ + rhs.x_, y_ + rhs.y_, z_ + rhs.z_);
    }



    /*! \brief Point after subtracting vector.
     *  \param[in] rhs Vector.
     *  \return Point.
     */
    inline Point3D Point3D::operator-(const Vector3D& rhs) const
    {
        return Point3D(x_ - rhs.x_, y_ - rhs.y_, z_ - rhs.z_);
    }



    /*! \brief Multiplication with scalar from the right.
     *  \param[in] a Scalar.
     *  \return Point.
     */
    inline Point3D Point3D::operator*(const double a) const
    {
        return Point3D(x_ * a, y_ * a, z_ * a);
    }



    // inlined non-member functions
    Point3D operator*(double a, const Point3D& rhs);

    /*! \brief Multiplication with scalar from the left.
     *  \param[in] a Scalar.
     *  \param[in] rhs Point.
     *  \return Point.
     */
    inline Point3D operator*(double a, const Point3D& rhs)
    {
        return Point3D(a * rhs.x_, a * rhs.y_, a * rhs.z_);
    }

} // end of namespace iiit

#endif // __POINT3D_HPP__
