// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Matrix4x4.cpp
 *  \author Thomas Nuernberg
 *  \date 03.03.2015
 *  \brief Implementation of class Matrix4x4.
 *
 *  Implementation of class of a 4x4 Matrix.
 */

#include "Matrix4x4.hpp"

#include <math.h>
#include <stdexcept>



namespace iiit
{

    /*! \brief Default constructor.
     *
     *  Default constructor with initialization as identity matrix.
     */
    Matrix4x4::Matrix4x4()
    {
        for (int i = 0; i < 4; ++i)
        {
            for (int j = 0; j < 4; ++j)
            {
                if (i == j)
                {
                    m_[i][j] = 1;
                }
                else
                {
                    m_[i][j] = 0;
                }
            }
        }
    }



    /*! \brief Copy constructor
     *  \param[in] m Matrix to copy.
     */
    Matrix4x4::Matrix4x4(const Matrix4x4& m)
    {
        for (int i = 0; i < 4; ++i)
        {
            for (int j = 0; j < 4; ++j)
            {
                m_[i][j] = m.m_[i][j];
            }
        }
    }



    /*! \brief Constructor with initialization from two dimensional array.
     *  \param[in] m Array of matrix elements.
     */
    Matrix4x4::Matrix4x4(double m[4][4])
    {
        for (int i = 0; i < 4; ++i)
        {
            for (int j = 0; j < 4; ++j)
            {
                m_[i][j] = m[i][j];
            }
        }
    }



    /*! \brief Constructor with element initialization.
     *  \param[in] m11 Matrix element (1, 1).
     *  \param[in] m12 Matrix element (1, 2).
     *  \param[in] m13 Matrix element (1, 3).
     *  \param[in] m14 Matrix element (1, 4).
     *  \param[in] m21 Matrix element (2, 1).
     *  \param[in] m22 Matrix element (2, 2).
     *  \param[in] m23 Matrix element (2, 3).
     *  \param[in] m24 Matrix element (2, 4).
     *  \param[in] m31 Matrix element (3, 1).
     *  \param[in] m32 Matrix element (3, 2).
     *  \param[in] m33 Matrix element (3, 3).
     *  \param[in] m34 Matrix element (3, 4).
     *  \param[in] m41 Matrix element (4, 1).
     *  \param[in] m42 Matrix element (4, 2).
     *  \param[in] m43 Matrix element (4, 3).
     *  \param[in] m44 Matrix element (4, 4).
     */
    Matrix4x4::Matrix4x4(double m11, double m12, double m13, double m14,
        double m21, double m22, double m23, double m24,
        double m31, double m32, double m33, double m34,
        double m41, double m42, double m43, double m44)
    {
        m_[0][0] = m11;
        m_[0][1] = m12;
        m_[0][2] = m13;
        m_[0][3] = m14;
        m_[1][0] = m21;
        m_[1][1] = m22;
        m_[1][2] = m23;
        m_[1][3] = m24;
        m_[2][0] = m31;
        m_[2][1] = m32;
        m_[2][2] = m33;
        m_[2][3] = m34;
        m_[3][0] = m41;
        m_[3][1] = m42;
        m_[3][2] = m43;
        m_[3][3] = m44;
    }



    /*! \brief Default destructor.
     */
    Matrix4x4::~Matrix4x4()
    {
    }



    /*! \brief Access operator.
     *  \param[in] i Row index.
     *  \param[in] j Column index.
     *  \return Reference to matrix element.
     *
     *  Access matrix elements with index beginning at 1.
     */
    double& Matrix4x4::operator() (int i, int j)
    {
        return m_[i - 1][j - 1];
    }



    /*! \brief Access operator.
     *  \param[in] i Row index.
     *  \param[in] j Column index.
     *  \return Matrix element.
     *
     *  Access matrix elements with index beginning at 1.
     */
    double Matrix4x4::operator() (int i, int j) const
    {
        return m_[i - 1][j - 1];
    }



    /*! \brief Access operator.
     *  \param[in] i Row index.
     *  \return Pointer to matrix row.
     *
     *  Access matrix elements with index beginning at 0.
     *
     *  \b Example:
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
     *  Matrix4x4 m;
     *  ...
     *  double element = m[2][1];
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     */
    double* Matrix4x4::operator[] (int i)
    {
        return m_[i];
    }



    /*! \brief Access operator.
     *  \param[in] i Row index.
     *  \return Pointer to matrix row.
     *
     *  Access matrix elements with index beginning at 0.
     *
     *  \b Example:
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
     *  const Matrix4x4 m;
     *  ...
     *  double element = m[2][1];
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     */
    const double* Matrix4x4::operator[] (int i) const
    {
        return m_[i];
    }



    /*! \brief Set matrix elements.
     *  \param[in] m11 Matrix element (1, 1).
     *  \param[in] m12 Matrix element (1, 2).
     *  \param[in] m13 Matrix element (1, 3).
     *  \param[in] m14 Matrix element (1, 4).
     *  \param[in] m21 Matrix element (2, 1).
     *  \param[in] m22 Matrix element (2, 2).
     *  \param[in] m23 Matrix element (2, 3).
     *  \param[in] m24 Matrix element (2, 4).
     *  \param[in] m31 Matrix element (3, 1).
     *  \param[in] m32 Matrix element (3, 2).
     *  \param[in] m33 Matrix element (3, 3).
     *  \param[in] m34 Matrix element (3, 4).
     *  \param[in] m41 Matrix element (4, 1).
     *  \param[in] m42 Matrix element (4, 2).
     *  \param[in] m43 Matrix element (4, 3).
     *  \param[in] m44 Matrix element (4, 4).
     */
    void Matrix4x4::set(double m11, double m12, double m13, double m14,
        double m21, double m22, double m23, double m24,
        double m31, double m32, double m33, double m34,
        double m41, double m42, double m43, double m44)
    {
        m_[0][0] = m11;
        m_[0][1] = m12;
        m_[0][2] = m13;
        m_[0][3] = m14;
        m_[1][0] = m21;
        m_[1][1] = m22;
        m_[1][2] = m23;
        m_[1][3] = m24;
        m_[2][0] = m31;
        m_[2][1] = m32;
        m_[2][2] = m33;
        m_[2][3] = m34;
        m_[3][0] = m41;
        m_[3][1] = m42;
        m_[3][2] = m43;
        m_[3][3] = m44;
    }



    /*! \brief Display operator.
     *  \param[in] os Output stream.
     *  \param[in] m Matrix to display.
     *  \return Output stream.
     */
    std::ostream& operator<<(std::ostream& os, const Matrix4x4& m)
    {
        for (int i = 0; i < 4; ++i)
        {
            os << "[" << m.m_[i][0] << ", " << m.m_[i][1] << ", " << m.m_[i][2] << ", "  << m.m_[i][3] <<  "]" << std::endl;
        }
        return os;
    }



    /*! \brief Equality operator.
     *  \param[in] m Matrix to compare.
     *  \return Returns TRUE if matrices are equal, else FALSE.
     */
    bool Matrix4x4::operator==(const Matrix4x4& m) const
    {
        bool res = true;
        for (int i = 0; i < 4; ++i)
        {
            for (int j = 0; j < 4; ++j)
            {
                res = res & (m_[i][j] == m.m_[i][j]);
            }
        }
        return res;
    }



    /*! \brief Inequality operator.
     *  \param[in] m Matrix to compare.
     *  \return Returns TRUE if matrices are unequal, else FALSE.
     */
    bool Matrix4x4::operator!=(const Matrix4x4& m) const
    {
        return !(this->operator==(m));
    }



    /*! \brief Assignment operator.
     *  \param[in] m Matrix to assign.
     *  \return Assigned matrix.
     */
    Matrix4x4& Matrix4x4::operator=(const Matrix4x4& m)
    {
        if (this == &m)
        {
            return *this;
        }
        for (int i = 0; i < 4; ++i)
        {
            for (int j = 0; j < 4; ++j)
            {
                m_[i][j] = m.m_[i][j];
            }
        }
        return *this;
    }



    /*! \brief Transpose matrix.
     *  \return Transposed matrix.
     */
    Matrix4x4 Matrix4x4::transpose() const
    {
        Matrix4x4 m(
            m_[0][0],
            m_[1][0],
            m_[2][0],
            m_[3][0],
            m_[0][1],
            m_[1][1],
            m_[2][1],
            m_[3][1],
            m_[0][2],
            m_[1][2],
            m_[2][2],
            m_[3][2],
            m_[0][3],
            m_[1][3],
            m_[2][3],
            m_[3][3]);
        return m;
    }



    /*! \brief Matrix multiplication.
     *  \param[in] rhs Right hand side of product.
     *  \return Matrix product.
     */
    Matrix4x4 Matrix4x4::operator*(const Matrix4x4 rhs) const
    {
        Matrix4x4 res;
        double sum;
        for (int c = 0; c < 4; ++c)
        {
            for (int r = 0; r < 4; ++r)
            {
                sum = 0.0;

                for (int j = 0; j < 4; ++j)
                {
                    sum += m_[r][j] * rhs.m_[j][c];
                }
                res[r][c] = sum;			
            }
        }
        return res;
    }



    /*! \brief Scalar multiplication.
     *  \param[in] a Scalar.
     *  \return Scaled matrix.
     */
    Matrix4x4 Matrix4x4::operator*(double a) const
    {
        Matrix4x4 m(*this);
        for (int i = 0; i < 4; ++i)
        {
            for (int j = 0; j < 4; ++j)
            {
                m[i][j] *= a;
            }
        }
        return m;
    }



    /*! \brief Matrix inversion.
     *  \return Inversed matrix.
     *  \exception std::runtime_error if matrix is singular.
     *
     *  Method implements numeric stable version of gauss-jordan elimination.
     */
    Matrix4x4 Matrix4x4::inverse() const
    {
        Matrix4x4 m(*this);
        Matrix4x4 inv;
        for (int k = 0; k < 4; ++k)
        {
            // find k-th pivot element
            int iMax = k;
            double maxAbs = 0.0;
            for (int i = k; i < 4; ++i)
            {
                if (fabs(m[i][k]) > maxAbs)
                {
                    iMax = i;
                    maxAbs = fabs(m[i][k]);
                }
            }

            // check for singular matrix
            if (m[iMax][k] == 0.0)
            {
                throw std::runtime_error("Matrix singular!");
                return inv;
            }

            // swap rows
            for (int j = 0; j < 4; ++j)
            {
                double tmp = m[iMax][j];
                m[iMax][j] = m[k][j];
                m[k][j] = tmp;
                tmp = inv[iMax][j];
                inv[iMax][j] = inv[k][j];
                inv[k][j] = tmp;
            }

            for (int i = k + 1; i < 4; ++i)
            {
                for (int j = k + 1; j < 4; ++j)
                {
                    m[i][j] = m[i][j] - m[k][j] * (m[i][k] / m[k][k]);
                }
                
                for (int j = 0; j < 4; ++j)
                {
                    inv[i][j] = inv[i][j] - inv[k][j] * (m[i][k] / m[k][k]);
                }

                // set elements below pivot elements to zeros
                m[i][k] = 0.0;
            }
        }

        // matrix now in row-echelon form, solved by back-substitution
        for (int k = 3; k >= 0; --k)
        {
            for (int i = k - 1; i >= 0; --i)
            {
                for (int j = 0; j < 4; ++j)
                {
                    inv[i][j] = inv[i][j] - inv[k][j] * (m[i][k] / m[k][k]);
                }
                for (int j = k - 1; j < 4; ++j)
                {
                    m[i][j] = m[i][j] - m[k][j] * (m[i][k] / m[k][k]);
                }
            }

            // normalize row
            for (int j = 3; j >= 0; --j)
            {
                inv[k][j] = inv[k][j] / m[k][k];
            }
            for (int j = 3; j >= k; --j)
            {
                m[k][j] = m[k][j] / m[k][k];
            }
        }

        return inv;
    }

} // end of namespace iiit