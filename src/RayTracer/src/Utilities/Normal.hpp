// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Normal.hpp
 *  \author Thomas Nuernberg
 *  \date 24.11.2014
 *  \brief Definition of class Normal.
 *
 *  Definition of class Normal representing a normal vector of a surface.
 */

#ifndef __NORMAL_HPP__
#define __NORMAL_HPP__

#include <iostream>
#include <stdexcept>

#include "Vector3D.hpp"
#include "Point3D.hpp"



namespace iiit
{

    /*! \brief Class of normal vector in 3D space.
     *
     *  Class represents a normal vector of a surface in 3D space with coordinates x, y, and z.
     */
    class Normal
    {
    public:
        // constructors
        Normal();
        Normal(double x, double y, double z);
        Normal(const Normal& normal);
        Normal(const Vector3D& vector);

        // destructor
        ~Normal();

        // access operators
        double& operator()(unsigned int row, unsigned int col);
        double& operator()(unsigned int row);
        double& operator[](unsigned int row);
        double operator()(unsigned int row, unsigned int col) const;
        double operator()(unsigned int row) const;
        double operator[](unsigned int row) const;
        void set(double x, double y, double z);

        // display operator
        friend std::ostream& operator<<(std::ostream& os, const Normal& normal);

        // operations
        bool operator==(const Normal& rhs) const;
        bool operator!=(const Normal& rhs) const;
        Normal& operator=(const Normal& rhs);
        Normal& operator=(const Point3D& rhs);
        Normal& operator=(const Vector3D& rhs);
        Normal operator-(void) const;
        Normal operator+(const Normal& rhs) const;
        Normal& operator+=(const Normal& rhs);
        Normal operator-(const Normal& rhs) const;
        Normal& operator-=(const Normal& rhs);
        Normal operator*(const double a) const;
        Normal operator/(const double a) const;
        double operator*(const Vector3D& rhs) const;
        void normalize(void);


        double x_; //!< x-coordinate
        double y_; //!< y-coordinate
        double z_; //!< z-coordinate
    };



    // inlined member functions

    /*! \brief Assignment operator.
     *
     *  Assign normal elements with index beginning at 1.
     *  \param[in] row Row index.
     *  \param[in] col Column index.
     *  \return Reference to element.
     */
    inline double& Normal::operator()(unsigned int row, unsigned int col)
    {
        switch (row)
        {
        case 1 :
            return x_;
            break;
        case 2 :
            return y_;
            break;
        case 3 :
            return z_;
            break;
        default :
            throw std::out_of_range("Index of Normal out of range.");
        }
    }



    /*! \brief Assignment operator.
     *
     *  Assign normal elements with index beginning at 1.
     *  \param[in] row Row index.
     *  \return Reference to element.
     */
    inline double& Normal::operator()(unsigned int row)
    {
        switch (row)
        {
        case 1 :
            return x_;
            break;
        case 2 :
            return y_;
            break;
        case 3 :
            return z_;
            break;
        default :
            throw std::out_of_range("Index of Normal out of range.");
        }
    }



    /*! \brief Assignment operator.
     *
     *  Assign normal elements with index beginning at 0.
     *  \param[in] row Row index.
     *  \return Reference to element.
     */
    inline double& Normal::operator[](unsigned int row)
    {
        switch (row)
        {
        case 0 :
            return x_;
            break;
        case 1 :
            return y_;
            break;
        case 2 :
            return z_;
            break;
        default :
            throw std::out_of_range("Index of Normal out of range.");
        }
    }



    /*! \brief Access operator.
     *
     *  Access normal elements with index beginning at 1.
     *  \param[in] row Row index.
     *  \param[in] col Column index.
     *  \return Element.
     */
    inline double Normal::operator()(unsigned int row, unsigned int col) const
    {
        switch (row)
        {
        case 1 :
            return x_;
            break;
        case 2 :
            return y_;
            break;
        case 3 :
            return z_;
            break;
        default :
            throw std::out_of_range("Index of Normal out of range.");
        }
    }



    /*! \brief Access operator.
     *
     *  Access normal elements with index beginning at 1.
     *  \param[in] row Row index.
     *  \return Element.
     */
    inline double Normal::operator()(unsigned int row) const
    {
        switch (row)
        {
        case 1 :
            return x_;
            break;
        case 2 :
            return y_;
            break;
        case 3 :
            return z_;
            break;
        default :
            throw std::out_of_range("Index of Normal out of range.");
        }
    }



    /*! \brief Access operator.
     *
     *  Access normal elements with index beginning at 0.
     *  \param[in] row Row index.
     *  \return Element.
     */
    inline double Normal::operator[](unsigned int row) const
    {
        switch (row)
        {
        case 0 :
            return x_;
            break;
        case 1 :
            return y_;
            break;
        case 2 :
            return z_;
            break;
        default :
            throw std::out_of_range("Index of Normal out of range.");
        }
    }



    /*! \brief Set normal elements.
     *  \param[in] x X-coordinate of normal.
     *  \param[in] y Y-coordinate of normal.
     *  \param[in] z Z-coordinate of normal.
     */
    inline void Normal::set(double x, double y, double z)
    {
        x_ = x;
        y_ = y;
        z_ = z;
    }



    /*! \brief Unary minus operator.
     *  \return Inverted normal.
     */
    inline Normal Normal::operator-(void) const
    {
        return Normal(-x_, -y_, -z_);
    }



    /*! \brief Add normals.
     *  \param[in] rhs Normal to add.
     *  \return Added normal.
     */
    inline Normal Normal::operator+(const Normal& rhs) const
    {
        return Normal(x_ + rhs.x_, y_ + rhs.y_, z_ + rhs.z_);
    }



    /*! \brief Add normals.
     *  \param[in] rhs Normal to add.
     *  \return Reference of assigned normal.
     */
    inline Normal& Normal::operator+=(const Normal& rhs)
    {
        x_ += rhs.x_;
        y_ += rhs.y_;
        z_ += rhs.z_;
        return *this;
    }



    /*! \brief Substract normal.
     *  \param[in] rhs Normal to subtract.
     *  \return Subtracted normal.
     */
    inline Normal Normal::operator-(const Normal& rhs) const
    {
        return Normal(x_ - rhs.x_, y_ - rhs.y_, z_ - rhs.z_);
    }



    /*! \brief Substract normals.
     *  \param[in] rhs Normal to subtract.
     *  \return Reference of assigned normal.
     */
    inline Normal& Normal::operator-=(const Normal& rhs)
    {
        x_ -= rhs.x_;
        y_ -= rhs.y_;
        z_ -= rhs.z_;
        return *this;
    }



    /*! \brief Multiplication with scalar from the right.
     *  \param[in] a Scalar.
     *  \return Scaled normal.
     */
    inline Normal Normal::operator*(const double a) const
    {
        return Normal(x_ * a, y_ * a, z_ * a);
    }



    /*! \brief Division with scalar from the right.
     *  \param[in] a Scalar.
     *  \return Scaled vector.
     */
    inline Normal Normal::operator/(const double a) const
    {
        return Normal(x_ / a, y_ / a, z_ / a);
    }



    /*! \brief Dot product with vector.
     *  \param[in] rhs Vector.
     *  \return Result of dot product.
     */
    inline double Normal::operator*(const Vector3D& rhs) const
    {
        return (x_ * rhs.x_ + y_ * rhs.y_ + z_ * rhs.z_);
    }



    // inlined non-member functions
    Normal operator*(double a, const Normal& rhs);

    /*! \brief Multiplication with scalar from the left.
     *  \param[in] a Scalar.
     *  \param[in] rhs Normal.
     *  \return Scaled normal.
     */
    inline Normal operator*(double a, const Normal& rhs)
    {
        return Normal(a * rhs.x_, a * rhs.y_, a * rhs.z_);
    }



    Vector3D operator+(const Vector3D& vector, const Normal& normal);

    /*! \brief Addition of a vector from the left.
     *  \param[in] vector Vector.
     *  \param[in] normal Normal.
     *  \return Added vector.
     */
    inline Vector3D operator+(const Vector3D& vector, const Normal& normal)
    {
        return Vector3D(vector.x_ + normal.x_, vector.y_ + normal.y_, vector.z_ + normal.z_);
    }



    Vector3D operator-(const Vector3D& vector, const Normal& normal);

    /*! \brief Subtraction of a vector from the left.
     *  \param[in] vector Vector.
     *  \param[in] normal Normal.
     *  \return Subtracted vector.
     */
    inline Vector3D operator-(const Vector3D& vector, const Normal& normal)
    {
        return Vector3D(vector.x_ - normal.x_, vector.y_ - normal.y_, vector.z_ - normal.z_);
    }



    // prototype
    inline double operator*(const Vector3D& vector, const Normal& normal);

    /*! \brief Dot product with vector from the left.
     *  \param[in] vector Vector.
     *  \param[in] normal Normal.
     *  \return Result of dot product.
     */
    inline double operator*(const Vector3D& vector, const Normal& normal)
    {
        return (vector.x_ * normal.x_ + vector.y_ * normal.y_ +vector. z_ * normal.z_);
    }

} // end of namespace iiit

#endif // __NORMAL_HPP__