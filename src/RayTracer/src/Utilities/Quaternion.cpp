// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Quaternion.cpp
 *  \author Thomas Nuernberg
 *  \date 27.05.2016
 *  \brief Implementation of class Quaternion.
 */

#include "Quaternion.hpp"

#include <math.h>



namespace iiit
{

    /*! \brief Default constructor.
     * 
     *  Default constructor of zero vector.
     */
    Quaternion::Quaternion()
        : x0_(0.0), x1_(0.0), x2_(0.0), x3_(0.0)
    {}



    /*! \brief Constructor with initialization.
     *
     *  Constructor with explicit specification of coordinates.
     *  \param[in] x0 real coordinate of quaternion.
     *  \param[in] x1 i-coordinate of quaternion.
     *  \param[in] x2 j-coordinate of quaternion.
     *  \param[in] x3 k-coordinate of quaternion.
     */
    Quaternion::Quaternion(double x0, double x1, double x2, double x3)
        : x0_(x0), x1_(x1), x2_(x2), x3_(x3)
    {}



    /*! \brief Copy constructor.
     *  \param[in] other Quaternion to copy.
     */
    Quaternion::Quaternion(const Quaternion& other)
        : x0_(other.x0_), x1_(other.x1_), x2_(other.x2_), x3_(other.x3_)
    {}



    /*! \brief Default destructor.
     */
    Quaternion::~Quaternion()
    {}



    /*! \brief Display operator.
     *  \param[in] os Output stream.
     *  \param[in] quaternion Quaternion to display.
     *  \return Output stream.
     */
    std::ostream& operator<<(std::ostream& os, const Quaternion& quaternion)
    {
        os << "[" << quaternion.x0_ << ", " << quaternion.x1_ << ", " << quaternion.x2_ << ", " << quaternion.x3_ << "]";
        return os;
    }



    /*! \brief Equality operator.
     *  \param[in] rhs Quaternion to compare.
     *  \return Returns TRUE if quaternions are equal, else FALSE.
     */
    bool Quaternion::operator==(const Quaternion& rhs) const
    {
        bool res = true;
        res = res & (x0_ == rhs.x0_);
        res = res & (x1_ == rhs.x1_);
        res = res & (x2_ == rhs.x2_);
        res = res & (x3_ == rhs.x3_);

        return res;
    }



    /*! \brief Inequality operator.
     *  \param[in] rhs Quaternion to compare.
     *  \return Returns TRUE if quaternions are unequal, else FALSE.
     */
    bool Quaternion::operator!=(const Quaternion& rhs) const
    {
        return !(this->operator==(rhs));
    }



    /*! \brief Assignment operator.
     *  \param[in] rhs Quaternion to be assign.
     *  \return Reference of assigned quaternion.
     */
    Quaternion& Quaternion::operator=(const Quaternion& rhs)
    {
        if (this == &rhs)
        {
            return *this;
        }
        x0_ = rhs.x0_;
        x1_ = rhs.x1_;
        x2_ = rhs.x2_;
        x3_ = rhs.x3_;
        return *this;
    }



    /*! \brief Rotate vector about specified rotation axis.
     *  \param[in] vector Vector to rotate.
     *  \param[in] axis Rotation axis.
     *  \param[in] angle Angle to rotate about.
     *  \return Rotated vector.
     */
    Vector3D Quaternion::rotateVector(Vector3D vector, Vector3D axis, double angle)
    {
           axis.normalize();
           double sinHalfSigma = sin((angle) / 2);
           double cosHalfSigma = cos((angle) / 2);
           Quaternion q(cosHalfSigma, sinHalfSigma * axis.x_, sinHalfSigma * axis.y_, sinHalfSigma * axis.z_);
           Quaternion newDirectionQuaternion = q * Quaternion(0.0, vector.x_, vector.y_, vector.z_) * q.conjugate();
           return Vector3D(newDirectionQuaternion.x1_, newDirectionQuaternion.x2_, newDirectionQuaternion.x3_);
    }

} // end of namespace iiit
