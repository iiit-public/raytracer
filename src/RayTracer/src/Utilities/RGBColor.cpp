// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


///*! \file RGBColor.cpp
// *  \author Mohamed Salem Koubaa
// *  \date 04.02.2015
// *  \brief Implementation of class RGBColor.
// */
//
//#include "RGBColor.hpp"
//
//
//const unsigned int RGBColor::trueColorMax_ = 255;
//
//
//
///*! \brief Default constructor.
// *
// *  Initializes color as black by default.
// */
//RGBColor::RGBColor()
//    : r_(0.0)
//    , g_(0.0)
//    , b_(0.0)
//{
//}
//
//
//
///*! \brief Constructor with initialization.
// *  \param[in] gray Gray value.
// *
// *  Constructor for gray color.
// */
//RGBColor::RGBColor(float gray)
//    : r_(gray)
//    , g_(gray)
//    , b_(gray)
//{
//}
//
//
//
///*! \brief Constructor with initialization.
// *  \param[in] r Red channel.
// *  \param[in] g Green channel.
// *  \param[in] b Blue channel.
// *
// *  Constructor with explicit specification of channels.
// */
//RGBColor::RGBColor(float r, float g, float b)
//    : r_(r)
//    , g_(g)
//    , b_(b)
//{
//}
//
//
//
///*! \brief Copy constructor.
// *  \param[in] other RGBColor to copy.
// */
//RGBColor::RGBColor(const RGBColor& other)
//    : r_(other.r_)
//    , g_(other.g_)
//    , b_(other.b_)
//{
//}
//
//
//
///*! \brief Default destructor.
// */
//RGBColor::~RGBColor(void)
//{
//}