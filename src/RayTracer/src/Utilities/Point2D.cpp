// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Point2D.cpp
 *  \author Mohamed Salem Koubaa
 *  \date 30.03.2015
 *  \brief Implementation of class Point2D.
 */

#include "Point2D.hpp"

#include <math.h>


namespace iiit
{

    /*! \brief Default constructor.
     * 
     *  Default constructor of Point in origin (0,0,0).
     */
    Point2D::Point2D()
        : x_(0), y_(0)
    {}



    /*! \brief Constructor with initialization.
     *
     *  Constructor with explicit specification of coordinates.
     *  \param[in] x x-coordinate.
     *  \param[in] y y-coordinate.
     */
    Point2D::Point2D(double x, double y)
        : x_(x), y_(y)
    {}



    /*! \brief Copy constructor.
     *  \param[in] point Point to copy.
     */
    Point2D::Point2D(const Point2D& point)
        : x_(point.x_),y_(point.y_)
    {}




    /*! \brief Default destructor.
     */
    Point2D::~Point2D()
    {}



    /*! \brief Display operator.
     *  \param[in] os Output stream.
     *  \param[in] point Point to display.
     *  \return Output stream.
     */
    std::ostream& operator<<(std::ostream& os, const Point2D& point)
    {
        os << "[" << point.x_ << ", " << point.y_ << "]";
        return os;
    }



    /*! \brief Equality operator.
     *  \param[in] rhs Point to compare.
     *  \return Returns TRUE if points are equal, else FALSE.
     */
    bool Point2D::operator==(const Point2D& rhs) const
    {
        bool res = true;
        res = res & (x_ == rhs.x_);
        res = res & (y_ == rhs.y_);

        return res;
    }



    /*! \brief Inequality operator.
     *  \param[in] rhs Point to compare.
     *  \return Returns TRUE if points are unequal, else FALSE.
     */
    bool Point2D::operator!=(const Point2D& rhs) const
    {
        return !(this->operator==(rhs));
    }



    /*! \brief Assignment operator.
     *  \param[in] rhs Point to be assign.
     *  \return Reference of assigned Point.
     */
    Point2D& Point2D::operator=(const Point2D& rhs)
    {
        if (this == &rhs)
        {
            return *this;
        }
        x_ = rhs.x_;
        y_ = rhs.y_;
        return *this;
    }



    /*! \brief Euclidean distance of 2 points.
     *  \param[in] point Distant point.
     *  \return Distance.
     */
    double Point2D::distance(const Point2D& point) const
    {
        return sqrt( (x_ - point.x_) * (x_ - point.x_) 
            + (y_ - point.y_) * (y_ - point.y_));
    }


    /*! \brief Euclidean Norm of 2D vector.
     *  \return Norm.
     */
    double Point2D::norm() const
    {
        return sqrt( (x_ * x_) + (y_ * y_));
    }

} // end of namespace iiit
