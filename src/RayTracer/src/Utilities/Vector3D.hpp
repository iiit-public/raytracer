// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Vector3D.hpp
 *  \author Thomas Nuernberg
 *  \date 21.11.2014
 *  \brief Definition of class Vector3D.
 *
 *  Definition of class Vector3D representing a vector in 3D space with coodinates x, y, and z.
 */

#ifndef __VECTOR3D_HPP__
#define __VECTOR3D_HPP__

#include <iostream>
#include <stdexcept>


namespace iiit
{

    class Point3D; // forward declaration
    class Normal; // forward declaration



    /*! \brief Class of vector in 3D space.
     *
     *  Class represents a vector in 3D space with coordinates x, y, and z.
     */
    class Vector3D
    {
    public:
        // constructors
        Vector3D();
        Vector3D(double x, double y, double z);
        Vector3D(const Vector3D& vector);
        Vector3D(const Point3D& point);
        Vector3D(const Normal& normal);

        // destructor
        ~Vector3D();

        // access operators
        double& operator()(unsigned int row, unsigned int col);
        double& operator()(unsigned int row);
        double& operator[](unsigned int row);
        double operator()(unsigned int row, unsigned int col) const;
        double operator()(unsigned int row) const;
        double operator[](unsigned int row) const;
        void set(double x, double y, double z);

        // display operator
        friend std::ostream& operator<<(std::ostream& os, const Vector3D& vector);

        // operations
        bool operator==(const Vector3D& rhs) const;
        bool operator!=(const Vector3D& rhs) const;
        Vector3D& operator=(const Vector3D& rhs);
        Vector3D operator-(void) const;
        Vector3D operator+(const Vector3D& rhs) const;
        Vector3D& operator+=(const Vector3D& rhs);
        Vector3D operator-(const Vector3D& rhs) const;
        Vector3D& operator-=(const Vector3D& rhs);
        Vector3D operator*(const double a) const;
        Vector3D operator/(const double a) const;
        double operator*(const Vector3D& rhs) const;
        double length(void) const;
        void normalize(void);
        Vector3D crossProduct(const Vector3D& rhs) const;
        Vector3D crossProduct(const Normal& rhs) const;		
        double angle(const Vector3D& rhs) const;


		
        double x_; //!< x-coordinate
        double y_; //!< y-coordinate
        double z_; //!< z-coordinate
    };



    // inlined member functions

    /*! \brief Assignment operator.
     *
     *  Assign vector elements with index beginning at 1.
     *  \param[in] row Row index.
     *  \param[in] col Column index.
     *  \return Reference to element.
     */
    inline double& Vector3D::operator()(unsigned int row, unsigned int col)
    {
        switch (row)
        {
        case 1 :
            return x_;
            break;
        case 2 :
            return y_;
            break;
        case 3 :
            return z_;
            break;
        default :
            throw std::out_of_range("Index of Vector3D out of range.");
        }
    }



    /*! \brief Assignment operator.
     *
     *  Assign vector elements with index beginning at 1.
     *  \param[in] row Row index.
     *  \return Reference to element.
     */
    inline double& Vector3D::operator()(unsigned int row)
    {
        switch (row)
        {
        case 1 :
            return x_;
            break;
        case 2 :
            return y_;
            break;
        case 3 :
            return z_;
            break;
        default :
            throw std::out_of_range("Index of Vector3D out of range.");
        }
    }


    /*! \brief Assignment operator.
     *
     *  Assign vector elements with index beginning at 0.
     *  \param[in] row Row index.
     *  \return Reference to element.
     */
    inline double& Vector3D::operator[](unsigned int row)
    {
        switch (row)
        {
        case 0 :
            return x_;
            break;
        case 1 :
            return y_;
            break;
        case 2 :
            return z_;
            break;
        default :
            throw std::out_of_range("Index of Vector3D out of range.");
        }
    }



    /*! \brief Access operator.
     *
     *  Access vector elements with index beginning at 0.
     *  \param[in] row Row index.
     *  \param[in] col Column index.
     *  \return Element.
     */
    inline double Vector3D::operator()(unsigned int row, unsigned int col) const
    {
        switch (row)
        {
        case 1 :
            return x_;
            break;
        case 2 :
            return y_;
            break;
        case 3 :
            return z_;
            break;
        default :
            throw std::out_of_range("Index of Vector3D out of range.");
        }
    }



    /*! \brief Access operator.
     *
     *  Access vector elements with index beginning at 1.
     *  \param[in] row Row index.
     *  \return Element.
     */
    inline double Vector3D::operator()(unsigned int row) const
    {
        switch (row)
        {
        case 1 :
            return x_;
            break;
        case 2 :
            return y_;
            break;
        case 3 :
            return z_;
            break;
        default :
            throw std::out_of_range("Index of Vector3D out of range.");
        }
    }



    /*! \brief Access operator.
     *
     *  Access vector elements with index beginning at 0.
     *  \param[in] row Row index.
     *  \return Element.
     */
    inline double Vector3D::operator[](unsigned int row) const
    {
        switch (row)
        {
        case 0 :
            return x_;
            break;
        case 1 :
            return y_;
            break;
        case 2 :
            return z_;
            break;
        default :
            throw std::out_of_range("Index of Vector3D out of range.");
        }
    }



    /*! \brief Set vector elements.
     *  \param[in] x X-coordinate of vector.
     *  \param[in] y Y-coordinate of vector.
     *  \param[in] z Z-coordinate of vector.
     */
    inline void Vector3D::set(double x, double y, double z)
    {
        x_ = x;
        y_ = y;
        z_ = z;
    }



    /*! \brief Unary minus operator.
     *  \return Inverted vector.
     */
    inline Vector3D Vector3D::operator-(void) const
    {
        return Vector3D(-x_, -y_, -z_);
    }



    /*! \brief Add vectors.
     *  \param[in] rhs Vector to add.
     *  \return Added vector.
     */
    inline Vector3D Vector3D::operator+(const Vector3D& rhs) const
    {
        return Vector3D(x_ + rhs.x_, y_ + rhs.y_, z_ + rhs.z_);
    }



    /*! \brief Add vectors.
     *  \param[in] rhs Vector to add.
     *  \return Reference of assigned vector.
     */
    inline Vector3D& Vector3D::operator+=(const Vector3D& rhs)
    {
        x_ += rhs.x_;
        y_ += rhs.y_;
        z_ += rhs.z_;
        return *this;
    }



    /*! \brief Substract vectors.
     *  \param[in] rhs Vector to subtract.
     *  \return Subtracted vector.
     */
    inline Vector3D Vector3D::operator-(const Vector3D& rhs) const
    {
        return Vector3D(x_ - rhs.x_, y_ - rhs.y_, z_ - rhs.z_);
    }



    /*! \brief Substract vectors.
     *  \param[in] rhs Vector to subtract.
     *  \return Reference of assigned vector.
     */
    inline Vector3D& Vector3D::operator-=(const Vector3D& rhs)
    {
        x_ -= rhs.x_;
        y_ -= rhs.y_;
        z_ -= rhs.z_;
        return *this;
    }



    /*! \brief Multiplication with scalar from the right.
     *  \param[in] a Scalar.
     *  \return Scaled vector.
     */
    inline Vector3D Vector3D::operator*(const double a) const
    {
        return Vector3D(x_ * a, y_ * a, z_ * a);
    }



    /*! \brief Division with scalar from the right.
     *  \param[in] a Scalar.
     *  \return Scaled vector.
     */
    inline Vector3D Vector3D::operator/(const double a) const
    {
        return Vector3D(x_ / a, y_ / a, z_ / a);
    }



    /*! \brief Dot product.
     *  \param[in] rhs Vector.
     *  \return Result of dot product.
     */
    inline double Vector3D::operator*(const Vector3D& rhs) const
    {
        return (x_ * rhs.x_ + y_ * rhs.y_ + z_ * rhs.z_);
    }



    // inlined non-member functions
    Vector3D operator*(double a, const Vector3D& rhs);

    /*! \brief Multiplication with scalar from the left.
     *  \param[in] a Scalar.
     *  \param[in] rhs Vector.
     *  \return Scaled vector.
     */
    inline Vector3D operator*(double a, const Vector3D& rhs)
    {
        return Vector3D(a * rhs.x_, a * rhs.y_, a * rhs.z_);
    }

} // end of namespace iiit

#endif // __VECTOR3D_HPP__
