// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Matrix4x4.hpp
 *  \author Thomas Nuernberg
 *  \date 03.03.2015
 *  \brief Definition of class Matrix4x4.
 *
 *  Definition of class of a 4x4 Matrix4x4.
 */

#ifndef __MATRIX4X4_HPP__
#define __MATRIX4X4_HPP__

#include <iostream>

namespace iiit
{

    /*! \brief Class representing a 4x4 Matrix.
     *
     *  This class represents a 4 dimensional Matrix suited for linear transformations of homogeneous
     *  coordinates.
     */
    class Matrix4x4
    {
    public:
        // constructors
        Matrix4x4();
        Matrix4x4(const Matrix4x4& m);
        Matrix4x4(double m[4][4]);
        Matrix4x4(double m11, double m12, double m13, double m14,
            double m21, double m22, double m23, double m24,
            double m31, double m32, double m33, double m34,
            double m41, double m42, double m43, double m44);

        // destructor
        ~Matrix4x4();
            
        // access operators
        double& operator() (int i, int j);
        double operator() (int i, int j) const;
        double* operator[] (int i);
        const double* operator[] (int i) const;
        void set(double m11, double m12, double m13, double m14,
            double m21, double m22, double m23, double m24,
            double m31, double m32, double m33, double m34,
            double m41, double m42, double m43, double m44);

        // display operator
        friend std::ostream& operator<<(std::ostream& os, const Matrix4x4& m);
        
        // operations
        bool operator==(const Matrix4x4& m) const;
        bool operator!=(const Matrix4x4& m) const;
        Matrix4x4& operator=(const Matrix4x4& m);
        Matrix4x4 transpose() const;
        Matrix4x4 operator*(const Matrix4x4 rhs) const;
        Matrix4x4 operator*(double a) const;
        Matrix4x4 inverse() const;

        double m_[4][4]; ///< Actual array holding the data.
    };

} // end of namespace iiit

#endif // __MATRIX4X4_HPP__