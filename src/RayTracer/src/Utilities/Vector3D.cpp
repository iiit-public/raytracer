// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Vector3D.cpp
 *  \author Thomas Nuernberg
 *  \date 21.11.2014
 *  \brief Implementation of class Vector3D.
 */

#include "Vector3D.hpp"

#include <math.h>

#include "Point3D.hpp"
#include "Normal.hpp"



namespace iiit
{

    /*! \brief Default constructor.
     * 
     *  Default constructor of zero vector.
     */
    Vector3D::Vector3D()
        : x_(0.0), y_(0.0), z_(0.0)
    {}



    /*! \brief Constructor with initialization.
     *
     *  Constructor with explicit specification of coordinates.
     *  \param[in] x x-coordinate.
     *  \param[in] y y-coordinate.
     *  \param[in] z z-coordinate.
     */
    Vector3D::Vector3D(double x, double y, double z)
        : x_(x), y_(y), z_(z)
    {}



    /*! \brief Copy constructor.
     *  \param[in] vector Vector to copy.
     */
    Vector3D::Vector3D(const Vector3D& vector)
        : x_(vector.x_),y_(vector.y_),z_(vector.z_)
    {}



    /*! \brief Position vector constructor.
     *  \param[in] point Point.
     */
    Vector3D::Vector3D(const Point3D& point)
        : x_(point.x_),y_(point.y_),z_(point.z_)
    {}



    /*! \brief Normal constructor.
     *  \param[in] normal Normal.
     */
    Vector3D::Vector3D(const Normal& normal)
        : x_(normal.x_),y_(normal.y_),z_(normal.z_)
    {}



    /*! \brief Default destructor.
     */
    Vector3D::~Vector3D()
    {}



    /*! \brief Display operator.
     *  \param[in] os Output stream.
     *  \param[in] vector Vector to display.
     *  \return Output stream.
     */
    std::ostream& operator<<(std::ostream& os, const Vector3D& vector)
    {
        os << "[" << vector.x_ << ", " << vector.y_ << ", " << vector.z_ << "]";
        return os;
    }



    /*! \brief Equality operator.
     *  \param[in] rhs Vector to compare.
     *  \return Returns TRUE if vectors are equal, else FALSE.
     */
    bool Vector3D::operator==(const Vector3D& rhs) const
    {
        bool res = true;
        res = res & (x_ == rhs.x_);
        res = res & (y_ == rhs.y_);
        res = res & (z_ == rhs.z_);

        return res;
    }



    /*! \brief Inequality operator.
     *  \param[in] rhs Vector to compare.
     *  \return Returns TRUE if vectors are unequal, else FALSE.
     */
    bool Vector3D::operator!=(const Vector3D& rhs) const
    {
        return !(this->operator==(rhs));
    }



    /*! \brief Assignment operator.
     *  \param[in] rhs Vector to be assign.
     *  \return Reference of assigned vector.
     */
    Vector3D& Vector3D::operator=(const Vector3D& rhs)
    {
        if (this == &rhs)
        {
            return *this;
        }
        x_ = rhs.x_;
        y_ = rhs.y_;
        z_ = rhs.z_;
        return *this;
    }



    /*! \brief Length of vector.
     *  \return Length.
     */
    double Vector3D::length(void) const
    {
        return sqrt(x_ * x_ + y_ * y_ + z_ * z_);
    }



    /*! \brief Cross product of two vectors.
     *  \param[in] rhs Right hand side of cross product.
     *  \return Result of cross product.
     */
    Vector3D Vector3D::crossProduct(const Vector3D& rhs) const
    {
        Vector3D res;

        res.x_ = y_ * rhs.z_ - rhs.y_ * z_;
        res.y_ = z_ * rhs.x_ - rhs.z_ * x_;
        res.z_ = x_ * rhs.y_ - rhs.x_ * y_;

        return res;
    }



    /*! \brief Cross product of vector and normal.
     *  \param[in] rhs Right hand side of cross product.
     *  \return Result of cross product.
     */
    Vector3D Vector3D::crossProduct(const Normal& rhs) const
    {
        Vector3D res;

        res.x_ = y_ * rhs.z_ - rhs.y_ * z_;
        res.y_ = z_ * rhs.x_ - rhs.z_ * x_;
        res.z_ = x_ * rhs.y_ - rhs.x_ * y_;

        return res;
    }



    /*! \brief Normalize vector to length 1.
     */
    void Vector3D::normalize(void)
    {
        double length = this->length();
        x_ /= length;
        y_ /= length;
        z_ /= length;
    }

	/*! \brief Angle between two vectors in radians
	*  \param[in] second vector.
	*  \return Result angle.
	*/
    double Vector3D::angle(const Vector3D& rhs) const
	{
		return acos(*this * rhs /  (*this).length() / rhs.length());
	}


} // end of namespace iiit
