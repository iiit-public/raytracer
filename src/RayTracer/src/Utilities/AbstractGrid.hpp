// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file AbstractGrid.hpp
 *  \author Maximilian Schambach
 *  \date 26.02.2019
 *  \brief Definition of class Abstractgrid
 *
 *  Definition of class AbstractGrid representing a collection of grid points as 3D vectors (in camera coordinates).
 */

#ifndef __ABSTRACTGRID_HPP__
#define __ABSTRACTGRID_HPP__

#include <cmath>
#include <memory>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>      // std::setprecision
#include <random>

#include "Point3D.hpp"
#include "Spectrum/CoefficientSpectrum.hpp"


namespace iiit
{
    // Create Random generator device
    static std::random_device RAND_DEVICE;     // only used once to initialise (seed) engine
    static std::mt19937 RAND_ENGINE(RAND_DEVICE());    // random-number engine used (Mersenne-Twister in this case)

    /*! \brief Abstract base class for grids used by HexGrid and RectGrid
     */
    template <class SpectrumType>
    class AbstractGrid
    {
    public:
        AbstractGrid();
        AbstractGrid(double x_max, double y_max,
                     double offset_x, double offset_y,
                     double a_x, double a_y, double b_x, double b_y,
                     double phi, double theta,
                     double z,
                     bool is_coded);
        AbstractGrid(const AbstractGrid& grid);

        ~AbstractGrid();

        // Read access private variables
        double get_x_max();
        double get_y_max();
        double get_offset_x();
        double get_offset_y();
        double get_a_x();
        double get_a_y();
        double get_b_x();
        double get_b_y();
        double get_phi();
        double get_theta();
        double get_z();
        bool is_coded();
        Point3D get_point(unsigned int index);
        Point3D get_point_safe(unsigned int index);
        SpectrumType get_color(unsigned int index);
        SpectrumType get_color_safe(unsigned int index);
        std::vector<Point3D>::size_type get_num_points(); // Usually unsigned int

        bool operator==(const AbstractGrid& rhs) const;
        bool operator!=(const AbstractGrid& rhs) const;

        // common functions
        void add_point(Point3D point);
        void add_color(SpectrumType color);
        void add_noise(double mu, double sigma);
        void save(std::string file);

    protected:
        // Get member variables
        std::vector<Point3D> points_;
        std::vector<SpectrumType> colors_;
        double x_max_;
        double y_max_;
        double offset_x_;
        double offset_y_;
        double a_x_;
        double a_y_;
        double b_x_;
        double b_y_;
        double phi_;
        double theta_;
        double z_;
        bool is_coded_;

        // construction helpers
        void set_vals(double x_max, double y_max,
                      double offset_x, double offset_y,
                      double a_x, double a_y, double b_x, double b_y,
                      double phi, double theta,
                      double z,
                      bool is_coded);
        void calc_points();
     };

    // define construction helpers first
    /*! \brief Set private member variables.
     */
    template <class SpectrumType>
    void AbstractGrid<SpectrumType>::set_vals(double x_max, double y_max,
                                              double offset_x, double offset_y,
                                              double a_x, double a_y, double b_x, double b_y,
                                              double phi, double theta,
                                              double z,
                                              bool is_coded){
        x_max_ = x_max;
        y_max_ = y_max;
        offset_x_ = offset_x;
        offset_y_ = offset_y;
        a_x_ = a_x;
        a_y_ =a_y ;
        b_x_ = b_x;
        b_y_ = b_y;
        phi_ = phi;
        theta_ = theta;
        z_ = z;
        is_coded_ = is_coded;
    }


    /*! \brief Calculate grid points.
     */
    template <class SpectrumType>
    void AbstractGrid<SpectrumType>::calc_points(){
        // calculate all grid points obeying
        // x = offset + n*a + m*b for n, m in Z
        // within the x_max_, y_max_ specification
        // Apply color coded if grid is coded

        // calculate maximum combination numbers n and m
        double max_dist_x = std::abs(offset_x_) + x_max_;
        double max_dist_y = std::abs(offset_y_) + y_max_;

        int max_n_x = max_n_x = int(max_dist_x / std::abs(a_x_ + b_x_));
        int max_n_y = max_n_y = int(max_dist_y / std::abs(a_y_ + b_y_));

        int n_max = max_n_x > max_n_y ? max_n_x : max_n_y;

        // safety padding
        n_max *= 2;

        // calculate all combinations
        Point3D tmp;
        Point2D tmp2d;
        for (int n = -n_max; n <= n_max; n++) {
            for (int m = -n_max; m <= n_max; m++) {
                tmp2d = Point2D(offset_x_ + n*a_x_ + m*b_x_,
                                offset_y_ + n*a_y_ + m*b_y_);

                tmp = Point3D(tmp2d, z_);

                // tilt point by phi, theta
                if (phi_ != 0.0) tmp.rotate_x(phi_);
                if (theta_ != 0.0) tmp.rotate_y(theta_);

                // check that point is in boundaries
                if ((std::abs(tmp.x_) <= x_max_) && (std::abs(tmp.y_) <= y_max_))
                {
                    add_point(tmp);

                    // Initialize color
                    SpectrumType color(1.0);


                    if (is_coded_)
                    {   // Put random bandpass for one coefficient on mask
                        // Calculate random color index
                        int numChannels = color.getNumOfCoeffs();

                        // Set all values to zero, except for randomly drawn channel
                        std::uniform_int_distribution<> randInterval(0, numChannels - 1); // guaranteed unbiased
                        int rand_channel = randInterval(RAND_ENGINE);

                        for (unsigned int i = 0; i < numChannels; i++) {
                            if (i != rand_channel) color.setCoeff(i, 0.0);
                        }
                    }
                    // Add spectrum to color vector
                    add_color(color);
                }
            }
        }
    }

    /*! \brief Default constructor.
     *
     *  Default constructor.
     */
    template <class SpectrumType>
    AbstractGrid<SpectrumType>::AbstractGrid()
    {}


    /*! \brief Constructor with initialization.
     *
     *  Constructor with explicit specification of coordinates.
     *  \param[in] x_max Maximum x coordinate value of grid points.
     *  \param[in] y_max Maximum y coordinate value of grid points.
     *  \param[in] offset_x Grid offset in x coordinate.
     *  \param[in] offset_y Grid offset in y coordinate.
     *  \param[in] a_x Grid vector a x coordinate.
     *  \param[in] a_y Grid vector a y coordinate.
     *  \param[in] b_x Grid vector b x coordinate.
     *  \param[in] b_y Grid vector b y coordinate.
     *  \param[in] phi Grid tilt phi (rotation around x-axis).
     *  \param[in] theta Grid tilt theta (rotation around y-axis).
     *  \param[in] z z-coordinate of (untilted) grid.
     *  \param[in] is_coded Flag indicating whether the grid is spectrally coded.
     */
    template <class SpectrumType>
    AbstractGrid<SpectrumType>::AbstractGrid(double x_max, double y_max,
                                             double offset_x, double offset_y,
                                             double a_x, double a_y, double b_x, double b_y,
                                             double phi, double theta,
                                             double z,
                                             bool is_coded)
        : points_()
    {
        set_vals(x_max, y_max, offset_x, offset_y, a_x, a_y, b_x, b_y, phi, theta, z, is_coded);
        calc_points();
    }


    /*! \brief Copy constructor.
     *  \param[in] grid AbstractGrid to copy.
     */
    template <class SpectrumType>
    AbstractGrid<SpectrumType>::AbstractGrid(const AbstractGrid& grid)
        : points_(grid.points_)
    {
        set_vals(grid.x_max_, grid.y_max_,
                 grid.offset_x_, grid.offset_y_,
                 grid.a_x_, grid.a_y_, grid.b_x_, grid.b_y_,
                 grid.phi_, grid.theta_,
                 grid.z_,
                 grid.is_coded_);
        calc_points();
    }


    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    AbstractGrid<SpectrumType>::~AbstractGrid()
    {}


    /*! \brief Equality operator.
     *  \param[in] rhs Point to compare.
     *  \return Returns TRUE if points are equal, else FALSE.
     */
    template <class SpectrumType>
    bool AbstractGrid<SpectrumType>::operator==(const AbstractGrid& rhs) const
    {
        bool res = true;
        res = res & (points_.size() == rhs.points_.size());
        res = res & (x_max_ == rhs.x_max_);
        res = res & (y_max_ == rhs.y_max_);
        res = res & (offset_x_ == rhs.offset_x_);
        res = res & (offset_y_ == rhs.offset_y_);
        res = res & (a_x_ == rhs.a_x_);
        res = res & (a_y_ == rhs.a_y_);
        res = res & (b_x_ == rhs.b_x_);
        res = res & (b_y_ == rhs.b_y_);
        res = res & (phi_ == rhs.phi_);
        res = res & (theta_ == rhs.theta_);
        res = res & (z_ == rhs.z_);
        res = res & (is_coded_ == rhs.is_coded_);

        // If comparison failed so far, return false directly
        if (!res){return false;}
        else {
            for (std::vector<Point3D>::size_type i = 0; i < points_.size(); i++) {
                res = res & (points_[i] == rhs.points_[i]);
            }
            return res;
        }
    }


    /*! \brief Inequality operator.
     *  \param[in] rhs Point to compare.
     *  \return Returns TRUE if points are unequal, else FALSE.
     */
    template <class SpectrumType>
    bool AbstractGrid<SpectrumType>::operator!=(const AbstractGrid& rhs) const
    {
        return !(this->operator==(rhs));
    }

    template <class SpectrumType>
    double AbstractGrid<SpectrumType>::get_x_max(){return x_max_;}

    template <class SpectrumType>
    double AbstractGrid<SpectrumType>::get_y_max(){return y_max_;}

    template <class SpectrumType>
    double AbstractGrid<SpectrumType>::get_offset_x(){return offset_x_;}

    template <class SpectrumType>
    double AbstractGrid<SpectrumType>::get_offset_y(){return offset_y_;}

    template <class SpectrumType>
    double AbstractGrid<SpectrumType>::get_a_x(){return a_x_;}

    template <class SpectrumType>
    double AbstractGrid<SpectrumType>::get_a_y(){return a_y_;}

    template <class SpectrumType>
    double AbstractGrid<SpectrumType>::get_b_x(){return b_x_;}

    template <class SpectrumType>
    double AbstractGrid<SpectrumType>::get_b_y(){return b_y_;}

    template <class SpectrumType>
    double AbstractGrid<SpectrumType>::get_phi(){return phi_;}

    template <class SpectrumType>
    double AbstractGrid<SpectrumType>::get_theta(){return theta_;}

    template <class SpectrumType>
    double AbstractGrid<SpectrumType>::get_z(){return z_;}

    template <class SpectrumType>
    bool AbstractGrid<SpectrumType>::is_coded(){return is_coded_;}

    /*! \brief Get point of grid. No boundary checking is performed.
     * \param[in] index Index of grid point (starting at 0)
     * \return Returns grid point
     */
    template <class SpectrumType>
    Point3D AbstractGrid<SpectrumType>::get_point(unsigned int index){return points_[index];}

    /*! \brief Get point of grid. Safe version using boundary checking.
     * \param[in] index Index of grid point (starting at 0)
     * \return Returns grid point
     */
    template <class SpectrumType>
    Point3D AbstractGrid<SpectrumType>::get_point_safe(unsigned int index){return points_.at(index);}

    /*! \brief Get spectrum of gridpoint. No boundary checking is performed.
     * \param[in] index Index of grid point (starting at 0)
     * \return Returns grid point's color spectraum
     */
    template <class SpectrumType>
    SpectrumType AbstractGrid<SpectrumType>::get_color(unsigned int index){return colors_[index];}

    /*! \brief Get spectrum of gridpoint. Safe version using boundary checking.
     * \param[in] index Index of grid point (starting at 0)
     * \return Returns grid point's color spectraum
     */
    template <class SpectrumType>
    SpectrumType AbstractGrid<SpectrumType>::get_color_safe(unsigned int index){return colors_.at(index);}

    /*! \brief Get number of total grid points.
     * \return Returns number of grid points.
     */
    template <class SpectrumType>
    std::vector<Point3D>::size_type AbstractGrid<SpectrumType>::get_num_points(){return points_.size();}


    /*! \brief Add a point to the grid. This should only be used by inherited classes and for testing.
     *  \param[in] point Point to add.
     */
    template <class SpectrumType>
    void AbstractGrid<SpectrumType>::add_point(Point3D point){

        points_.push_back(point);
        return;
    }

    /*! \brief Add color spectrum to the grid. This should only be used by inherited classes and for testing.
     *  \param[in] point Spectrum to add.
     */
    template <class SpectrumType>
    void AbstractGrid<SpectrumType>::add_color(SpectrumType color){

        colors_.push_back(color);
        return;
    }


    /*! \brief Add bivariate gaussian noise to grid points.
     *  \param[in] mu Noise mean.
     *  \param[in] sigma Noise standard deviation.
     */
    template <class SpectrumType>
    void AbstractGrid<SpectrumType>::add_noise(double mu, double sigma){

        // random device class instance, source of 'true' randomness for initializing random seed
        std::random_device rd;

        // Mersenne twister PRNG, initialized with seed from previous random device instance
        std::mt19937 generator(rd());

        std::normal_distribution<double> distribution(mu, sigma);


        for (std::vector<Point3D>::size_type i = 0; i < points_.size(); ++i) {
            // draw random number independently for x and y
            double x = distribution(generator);
            double y = distribution(generator);

            // Add to point coordinates
            points_[i].x_ += x;
            points_[i].y_ += y;
        }
        return;
    }



    /*! \brief Save grid points as CSV.
     *  \param[in] file File path.
     */
    template <class SpectrumType>
    void AbstractGrid<SpectrumType>::save(std::string file){
        // number of grid points to save
        std::vector<Point3D>::size_type num = points_.size();

        std::ofstream outfile;
        outfile.open (file.c_str());
        outfile << "# x; y; z\n";

        // string to stream data in
        std::ostringstream str_stream;

        // save point data x y z
        for (std::vector<Point3D>::size_type n = 0; n < num; n++) {
            str_stream << std::setprecision(16) << points_[n].x_ << "; " << points_[n].y_ << "; " << points_[n].z_ << "\n";
        }

        // write string to file
        outfile << str_stream.str();

        outfile.close();
        return;
    }

} // end of namespace iiit

#endif // __ABSTRACTGRID_HPP__
