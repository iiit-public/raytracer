// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Normal.cpp
 *  \author Thomas Nuernberg
 *  \date 24.11.2014
 *  \brief Implementation of class Normal.
 */

#include "Normal.hpp"

#include <math.h>



namespace iiit
{

    /*! \brief Default constructor.
     * 
     *  Default constructor of normal (0,0,0).
     */
    Normal::Normal()
        : x_(0), y_(0), z_(0)
    {}



    /*! \brief Constructor with initialization.
     *
     *  Constructor with explicit specification of coordinates.
     *  \param[in] x x-coordinate.
     *  \param[in] y y-coordinate.
     *  \param[in] z z-coordinate.
     */
    Normal::Normal(double x, double y, double z)
        : x_(x), y_(y), z_(z)
    {}



    /*! \brief Copy constructor.
     *  \param[in] normal Normal to copy.
     */
    Normal::Normal(const Normal& normal)
        : x_(normal.x_),y_(normal.y_),z_(normal.z_)
    {}



    /*! \brief Constructor from vector.
     *  \param[in] vector Vector to assign.
     */
    Normal::Normal(const Vector3D& vector)
        : x_(vector.x_),y_(vector.y_),z_(vector.z_)
    {}



    /*! \brief Default destructor.
     */
    Normal::~Normal()
    {}



    /*! \brief Display operator.
     *  \param[in] os Output stream.
     *  \param[in] normal Normal to display.
     *  \return Output stream.
     */
    std::ostream& operator<<(std::ostream& os, const Normal& normal)
    {
        os << "[" << normal.x_ << ", " << normal.y_ << ", " << normal.z_ << "]";
        return os;
    }



    /*! \brief Equality operator.
     *  \param[in] rhs Normal to compare.
     *  \return Returns TRUE if normals are equal, else FALSE.
     */
    bool Normal::operator==(const Normal& rhs) const
    {
        bool res = true;
        res = res & (x_ == rhs.x_);
        res = res & (y_ == rhs.y_);
        res = res & (z_ == rhs.z_);

        return res;
    }



    /*! \brief Inequality operator.
     *  \param[in] rhs Normal to compare.
     *  \return Returns TRUE if normals are unequal, else FALSE.
     */
    bool Normal::operator!=(const Normal& rhs) const
    {
        return !(this->operator==(rhs));
    }



    /*! \brief Assignement operator.
     *  \param[in] rhs Normal to be assign.
     *  \return Reference of assigned normal.
     */
    Normal& Normal::operator=(const Normal& rhs)
    {
        if (this == &rhs)
        {
            return *this;
        }
        x_ = rhs.x_;
        y_ = rhs.y_;
        z_ = rhs.z_;
        return *this;
    }



    /*! \brief Assignement operator.
     *  \param[in] rhs Vector to be assign.
     *  \return Reference of assigned normal.
     */
    Normal& Normal::operator=(const Vector3D& rhs)
    {
        x_ = rhs.x_;
        y_ = rhs.y_;
        z_ = rhs.z_;
        return *this;
    }



    /*! \brief Assignement operator.
     *  \param[in] rhs Point to be assign.
     *  \return Reference of assigned normal.
     */
    Normal& Normal::operator=(const Point3D& rhs)
    {
        x_ = rhs.x_;
        y_ = rhs.y_;
        z_ = rhs.z_;
        return *this;
    }



    /*! \brief Normalize normal.
     */
    void Normal::normalize()
    {
        double length = sqrt(x_ * x_ + y_ * y_ + z_ * z_);
        x_ = x_ / length;
        y_ = y_ / length;
        z_ = z_ / length;
    }

} // end of namespace iiit