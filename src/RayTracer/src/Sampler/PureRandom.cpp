// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*!
 *  \file PureRandom.hpp
 *  \author Mohamed Salem Koubaa
 *  \date 17.04.2015
 *  \brief implementation of the pure random sampler.
 */

#include "PureRandom.hpp"

#include <random>



namespace iiit
{

    /*! \brief Default constructor.
     */
    PureRandom::PureRandom ()
        : Sampler(), seed_(std::mt19937::default_seed)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] numSamples Number of samples per set.
     *  \param[in] numSets Number of sample sets.
     */
    PureRandom::PureRandom(int numSamples, int numSets)
        : Sampler(numSamples, numSets), seed_(std::mt19937::default_seed)
    {
    }



    /*! \brief Copy constructor.
     *  \param[in] other Sampler object to copy.
     */
    PureRandom::PureRandom(const PureRandom& other)
        : Sampler(other), seed_(std::mt19937::default_seed)
    {
    }



    /*! \brief Clone (make deep copy of) sampler.
     *  \return Returned cloned sampler.
     */
    PureRandom* PureRandom::clone() const
    {
        return new PureRandom(*this);
    }



    /*! \brief Get the type of the sampler object.
     *  \return Returns the type of the sampler object.
     */
    Sampler::Type PureRandom::getType() const
    {
        return Sampler::PureRandom;
    }

    /*! \brief Sets seed for Mersenne Twister PRNG (default: 5489u).
     */
    void PureRandom::setSeed(unsigned seed)
    {
        seed_ = seed;
    }

    /*! \brief Randomly generates samples.
     */
    void PureRandom::generateSamples()
    {
        samples_->reserve(numSets_ * numSamples_);

        Point2D pp;

        std::mt19937 generator(seed_); // use specified seed

        // random number distribution
        std::uniform_real_distribution<double> distribution(0.0, 1.0);

        // sampling
        for (int j = 0; j < numSets_; ++j)
        {
            for (int i = 0; i < numSamples_; ++i)
            {
                pp.x_ = distribution(generator);
                pp.y_ = distribution(generator);

                // store Point
                samples_->push_back(pp);
            }
        }

        this->shuffleIndices();
    }

} // end of namespace iiit