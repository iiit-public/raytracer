// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Hammersley.hpp
 *  \author Thomas Nuernberg
 *  \date 01.06.2016
 *  \brief Implementation of Hammersley sampler.
 */


#include "Hammersley.hpp"

#include <math.h>
#include <random>
#include <iostream>



namespace iiit
{

    /*! \brief Default constructor.
     */
    Hammersley::Hammersley()
        : Sampler()
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] numSamples Number of samples per set.
     *
     *  \attention The number of samples should be a square number to evenly cover the unit square with
     *  samples.
     */
    Hammersley::Hammersley(int numSamples)
        : Sampler(numSamples, 1)
    {
        if (numSamples_ < 0)
        {
            std::cout << "Warning: Negative number of samples not allowed. Using 1 sample." << std::endl;
            numSamples_ = 1;
        }
        else
        {
            int root = static_cast<int>(floor(sqrt(numSamples_) + 0.5));
            if (numSamples_ != root * root)
            {
                std::cout << "Warning: Number of samples for Hammersley sampling should be a square number. Using " << root * root << " samples." << std::endl;
                numSamples_ = root * root;
            }
        }
    }



    /*! \brief Copy constructor.
     *  \param[in] other Sampler object to copy.
     */
    Hammersley::Hammersley(const Hammersley& other)
        : Sampler(other)
    {
    }



    /*! \brief Clone (make deep copy of) sampler.
     *  \return Returned cloned sampler.
     */
    Hammersley* Hammersley::clone() const
    {
        return new Hammersley(*this);
    }



    /*! \brief Get the type of the sampler object.
     *  \return Returns the type of the sampler object.
     */
    Sampler::Type Hammersley::getType() const
    {
        return Sampler::Hammersley;
    }



    /*! \brief Randomly generates samples.
     */
    void Hammersley::generateSamples()
    {
        samples_->reserve(numSamples_);
        for (int i = 0; i < numSamples_; ++i)
        {
            samples_->push_back(Point2D(static_cast<float>(i) / static_cast<float>(numSamples_), phi(i)));
        }
        this->shuffleIndices();
    }



    /*! \brief Radical inverse function.
     *  \param[in] n Input number.
     *  \return Result.
     */
    double Hammersley::phi(int n)
    {
        double x = 0.0;
        double f = 0.5;

        while (n)
        {
            x += f * static_cast<double>(n % 2);
            n /= 2;
            f *= 0.5;
        }
        return x;
    }

} // end of namespace iiit