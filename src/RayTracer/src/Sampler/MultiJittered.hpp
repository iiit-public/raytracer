// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file MultiJittered.hpp
 *  \author Thomas Nuernberg
 *  \date 01.06.2016
 *  \brief Definition of multi-jittered sampler.
 */

#ifndef __MULTIJITTERED_HPP__
#define __MULTIJITTERED_HPP__

#include "Sampler.hpp"



namespace iiit
{

    /*! \brief Class for n-rooks sampling.
     */
    class MultiJittered : public Sampler
    {
    public:
        MultiJittered();
        MultiJittered(int numSamples, int numSets = 83);
        MultiJittered(const MultiJittered& other);

        virtual MultiJittered* clone() const;

        void setSeed(unsigned seed);
        void generateSamples();
        Sampler::Type getType() const;

    private:
        unsigned seed_; ///< Seed of random number generator.
    };

} // end of namespace iiit

#endif // __MULTIJITTERED_HPP__