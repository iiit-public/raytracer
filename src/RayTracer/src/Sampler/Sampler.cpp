// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Sampler.cpp
 *  \author Thomas Nuernberg
 *  \date 12.02.2015
 *  \brief Implementation of base class for samplers.
 */

#include "Sampler.hpp"

#include <math.h>
#include <chrono>

#include "Utilities/Constants.h"



namespace iiit
{

    /*! \brief Default constructor.
     *
     *  Sets number of samples per set and number of sets to 1.
     */
    Sampler::Sampler()
        : numSamples_(1)
        , numSets_(1)
        , exp_(1.f)
        , distribution_(0, 0)
        , count_(0)
        , jump_(0)
        , jumpSeed_(static_cast<unsigned int>(std::chrono::system_clock::now().time_since_epoch().count()))
        , shuffleSeed_(static_cast<unsigned int>(std::chrono::system_clock::now().time_since_epoch().count()))
    {
        samples_ = std::shared_ptr<std::vector<Point2D> >(new std::vector<Point2D>());
    }



    /*! \brief Constructor with data initialization.
     *  \param[in] numSamples Number of samples per set.
     *  \param[in] numSets Number of sample sets to compute in advance.
     */
    Sampler::Sampler(int numSamples, int numSets)
        : numSamples_(numSamples)
        , numSets_(numSets)
        , exp_(1.f)
        , distribution_(0, numSets - 1)
        , count_(0)
        , jump_(0)
        , jumpSeed_(static_cast<unsigned int>(std::chrono::system_clock::now().time_since_epoch().count()))
        , shuffleSeed_(static_cast<unsigned int>(std::chrono::system_clock::now().time_since_epoch().count()))
    {
        samples_ = std::shared_ptr<std::vector<Point2D> >(new std::vector<Point2D>());
    }



    /*! \brief Copy constructor.
     *  \param[in] other Sampler object to copy.
     */
    Sampler::Sampler(const Sampler& other)
        : numSamples_(other.numSamples_)
        , numSets_(other.numSets_)
        , samples_(other.samples_)
        , diskSamples_(other.diskSamples_)
        , hemisphereSamples_(other.hemisphereSamples_)
        , sphereSamples_(other.sphereSamples_)
        , suffledIndices_(other.suffledIndices_)
        , exp_(other.exp_)
        , generator_(other.generator_)
        , distribution_(other.distribution_)
        , count_(other.count_)
        , jump_(other.jump_)
        , shuffleSeed_(other.shuffleSeed_)
    {
    }



    /*! \brief Get number of sampler per sample set.
     *  \return Number of samples.
     */
    int Sampler::getNumSamples() const
    {
        return numSamples_;
    }



    /*! \brief Get number of sample sets.
     *  \return Number of sample sets.
     */
    int Sampler::getNumSets() const
    {
        return static_cast<int>(samples_->size()) / numSamples_;
    }



    /*! \brief Set up the randomly shuffled indices.
     */
    void Sampler::shuffleIndices()
    {
        suffledIndices_ = std::shared_ptr<std::vector<int> >(new std::vector<int>());

        suffledIndices_->reserve(numSamples_ * numSets_);
        std::vector<int> indices;

        for (int i = 0; i < numSamples_; ++i)
        {
            indices.push_back(i);
        }

        auto engine = std::mt19937(shuffleSeed_);
        for (int j = 0; j < numSets_; ++j)
        {
            std::shuffle(std::begin(indices), std::end(indices), engine);
            for (int i = 0; i < numSamples_; ++i)
            {
                suffledIndices_->push_back(indices[i]);
            }
        }
    }



    /*! \brief Maps samples from unit square to unit disk.
     */
    void Sampler::mapSamplesToUnitDisk()
    {
        diskSamples_ = std::shared_ptr<std::vector<Point2D> >(new std::vector<Point2D>());

        float r, phi;
        Point2D sp;

        diskSamples_->reserve(samples_->size());

        for (unsigned int j = 0 ; j < samples_->size(); j++)
        {
            sp.x_ = 2.0 * (*samples_)[j].x_ - 1.0;
            sp.y_ = 2.0 * (*samples_)[j].y_ - 1.0;

            if (sp.x_ > - sp.y_)
            {
                if (sp.x_ > sp.y_)
                {
                    r = static_cast<float>(sp.x_);
                    phi= static_cast<float>(sp.y_ / sp.x_);
                }
                else
                {
                    r = static_cast<float>(sp.y_);
                    phi= static_cast<float>(2.0 - sp.x_ / sp.y_);
                }
            }
            else
            {
                if (sp.x_ < sp.y_)
                {
                    r = static_cast<float>(-sp.x_);
                    phi = static_cast<float>(4.0 + sp.y_ / sp.x_);
                }
                else
                {
                    r = static_cast<float>(-sp.y_);
                    if (sp.y_ != 0.0)
                    {
                        phi = static_cast<float>(6.0 - sp.x_ / sp.y_);
                    }
                    else
                    {
                        phi = 0.0;
                    }
                }
            }

            phi *= static_cast<float>(PI / 4.0);

            diskSamples_->push_back(Point2D(r * cos(phi), r * sin(phi)));
        }
    }


    /*! \brief Map samples to hemisphere.
     *  \param[in] exp Exponent that controls the density decrease towards flat angles.
     */
    void Sampler::mapSamplesToHemisphere(float exp)
    {
        hemisphereSamples_ = std::shared_ptr<std::vector<Point3D> >(new std::vector<Point3D>());

        exp_ = exp;
        hemisphereSamples_->reserve(samples_->size());
        for (unsigned int i = 0; i < samples_->size(); ++i)
        {
            double cosPhi = cos(2.f * PI * (*samples_)[i].x_);
            double sinPhi = sin(2.f * PI * (*samples_)[i].x_);
            double cosTheta = pow((1.f - (*samples_)[i].y_), 1.f / (exp + 1.f));
            double sinTheta = sqrt(1.f - cosTheta * cosTheta);
            double pu = sinTheta * cosPhi;
            double pv = sinTheta * sinPhi;
            double pw = cosTheta;

            hemisphereSamples_->push_back(Point3D(pu, pv, pw));
        }
    }



    /*! \brief Map samples to sphere.
     */
    void Sampler::mapSamplesToSphere()
    {
        sphereSamples_ = std::shared_ptr<std::vector<Point3D> >(new std::vector<Point3D>());

        sphereSamples_->reserve(samples_->size());
        for (unsigned int i = 0; i < samples_->size(); ++i)
        {
            double r1 = (*samples_)[i].x_;
            double r2 = (*samples_)[i].y_;
            double z = 1.0 - 2.0 * r1;
            double r = sqrt(1.0 - z * z);
            double phi = TWO_PI * r2;
            double x = r * cos(phi);
            double y = r * sin(phi);

            sphereSamples_->push_back(Point3D(x, y, z)); 
        }
    }
    

    
    /*! \brief Align sample counter.
     *  Sets sample counter to beginning of a new pixel to maintain sample framework alignment.
     */
    void Sampler::align()
    {
        count_ += numSamples_ - (count_ % numSamples_);
    }



    /*! \brief Set seed for random number generator used to jump to random sample sets.
     *  \param[in] seed New seed.
     */
    void Sampler::setJumpSeed(unsigned int seed)
    {
        jumpSeed_ = seed;
        generator_.seed(seed);
    }



    /*! \brief Set seed for random number generator used to shuffle indices.
     *  \param[in] seed New seed.
     */
    void Sampler::setShuffleSeed(unsigned int seed)
    {
        shuffleSeed_ = seed;
    }

} // end of namespace iiit
