// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*!
 *  \file Sampler.hpp
 *  \author Thomas Nuernberg
 *  \date 12.02.2015
 *  \brief Definition of base class for samplers.
 */
 
#ifndef __SAMPLER_HPP__
#define __SAMPLER_HPP__

#include <vector>
#include <random>
#include <algorithm>
#include <chrono>
#include <memory>
//#include <map>
//#include <unordered_map>
//#include <thread>

#include "Utilities/Point2D.hpp"
#include "Utilities/Point3D.hpp"



namespace iiit
{

    /*! \brief Abstract base class for samplers.
     */
    class Sampler
    {

    public:
        /*! This enum type defines valid samplers. The samplers are defined in specialized classes.
         */
        enum Type
        {
            Regular = 0, ///< Purely regular sampler.
            PureRandom = 1, ///< Purely random sampler.
            Jittered = 2, ///< Jittered sampler.
            NRooks = 3, ///< N-rooks sampler.
            MultiJittered = 4, ///< Multi-jittered sampler.
            Hammersley = 5, ///< Hammersley sampler.
        };

        Sampler();
        Sampler(int numSamples, int numSets);
        Sampler(const Sampler& other);
        virtual ~Sampler() {};

        /*! \brief Deep copies the sampler object.
         *  \return Returns the copied sampler object.
         */
        virtual Sampler* clone() const = 0;

        /*! \brief Generates sample patterns in a unit square.
         */
        virtual void generateSamples() = 0;

        /*! \brief Sets seed of PRNG used for sample generation.
         */
        virtual void setSeed(unsigned) {}
        void shuffleIndices() ; //set up the randomly shuffled indices
        int getNumSamples() const;
        int getNumSets() const;

        /*! \brief Get the type of the sampler object.
         *  \return Returns the type of the sampler object.
         */
        virtual Sampler::Type getType() const = 0;
        
        /*! \brief Sample unit square.
         *  \returns Returns the next sample.
         */
        inline Point2D sampleUnitSquare() // get next sample on unit square
        {
            if (count_ % numSamples_ == 0) // start of a new pixel
            {
                jump_ = distribution_(generator_) * numSamples_; // jump to random sample set
            }
            return ((*samples_)[jump_ + (*suffledIndices_)[jump_ + count_++ % numSamples_]]);
        };

        /*! \brief Sample unit disk.
         *  \returns Returns the next sample.
         */
        inline Point2D sampleUnitDisk() // get next sample on unit disk
        {
            if (count_ % numSamples_ == 0) // start of a new pixel
            {
                jump_ = distribution_(generator_) * numSamples_; // jump to random sample set
            }
            return ((*diskSamples_)[jump_ + (*suffledIndices_)[jump_ + count_++ % numSamples_]]);
        };

        /*! \brief Sample hemisphere.
         *  \returns Returns the next sample.
         */
        inline Point3D sampleHemisphere() // get next sample on hemisphere
        {
            if (count_ % numSamples_ == 0) // start of a new pixel
            {
                jump_ = distribution_(generator_) * numSamples_; // jump to random sample set
            }
            return ((*hemisphereSamples_)[jump_ + (*suffledIndices_)[jump_ + count_++ % numSamples_]]);
        };

        /*! \brief Sample unit sphere.
         *  \returns Returns the next sample.
         */
        inline Point3D sampleUnitSphere() // get next sample on sphere
        {
            if (count_ % numSamples_ == 0) // start of a new pixel
            {
                jump_ = distribution_(generator_) * numSamples_; // jump to random sample set
            }
            return ((*sphereSamples_)[jump_ + (*suffledIndices_)[jump_ + count_++ % numSamples_]]);
        };

        void mapSamplesToUnitDisk();
        void mapSamplesToHemisphere(float exp);
        void mapSamplesToSphere();
        
        void align();

        void setJumpSeed(unsigned int seed);
        void setShuffleSeed(unsigned int seed);

    protected:
        int numSamples_; ///< Number of sample point per set.
        int numSets_; ///< Number of sample sets.
        std::shared_ptr<std::vector<Point2D> > samples_; ///< Sample points on a unit square.
        std::shared_ptr<std::vector<Point2D> > diskSamples_; ///< Sample points on unit disk.
        std::shared_ptr<std::vector<Point3D> > hemisphereSamples_; ///< Sample points on hemisphere.
        std::shared_ptr<std::vector<Point3D> > sphereSamples_; ///< Sample points on unit sphere.
        std::shared_ptr<std::vector<int> > suffledIndices_; ///< Vector for random sample access per sample set.
        float exp_; ///< Exponent used for sample generation on hemisphere.
        std::mt19937 generator_; ///< Random number generator for index jumps.
        std::uniform_int_distribution<int> distribution_; ///< Random number distribution for index jumps.
        unsigned long count_; ///< Index of current sample point.
        int jump_; ///< Random index jump.
        unsigned int jumpSeed_; ///< Random number generator for random jumps to sample sets.
        unsigned int shuffleSeed_; ///< Random number generator for index shuffling.
    };

} // end of namespace iiit

#endif // __SAMPLER_HPP__
