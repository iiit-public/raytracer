// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file PureRandom.hpp
 *  \author Mohamed Salem Koubaa
 *  \date 17.04.2015
 *  \brief Definition of the pure random sampler.
 */

#ifndef __PURERANDOM_HPP__
#define __PURERANDOM_HPP__

#include "Sampler.hpp"



namespace iiit
{

    /*! \brief Class providing purely random samples in the unit square.
     */
    class PureRandom : public Sampler
    {
    public:
        PureRandom();
        PureRandom(int numSamples, int numSets = 83);
        PureRandom(const PureRandom& other);

        virtual PureRandom* clone() const;

        void setSeed(unsigned seed);
        void generateSamples();

        Sampler::Type getType() const;

    private:
        unsigned seed_; ///< Seed of random number generator.
    };

} // end of namespace iiit

#endif // __PURERANDOM_HPP__