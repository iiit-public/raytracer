// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Regular.cpp
 *  \author Mohamed Salem Koubaa
 *  \date 20.04.2015
 *  \brief Implementation of the regular sampler.
 */

#include "Regular.hpp"

#include <iostream>
#include <math.h>



namespace iiit
{

    /*! \brief Default constructor.
     *
     *  Sets number of samples per set and number of sets to 1.
     */
    Regular::Regular()
        : Sampler()
    {
    }



    /*! \brief Constructor with data initialization.
     *  \param[in] numSamples Number of samples per set.
     *
     *  \attention The number of samples should be a square number to evenly cover the unit square with
     *  samples.
     */
    Regular::Regular(int numSamples)
        : Sampler(numSamples, 1)
    {
        if (numSamples_ < 0)
        {
            std::cout << "Warning: Negative number of samples not allowed. Using 1 sample." << std::endl;
            numSamples_ = 1;
        }
        else
        {
            int root = static_cast<int>(floor(sqrt(numSamples_) + 0.5));
            if (numSamples_ != root * root)
            {
                std::cout << "Warning: Number of samples for regular sampling should be a square number. Using " << root * root << " samples." << std::endl;
                numSamples_ = root * root;
            }
        }
    }



    /*! \brief Copy constructor.
     *  \param[in] other Sampler object to copy.
     */
    Regular::Regular(const Regular& other)
        : Sampler(other)
    {
    }



    /*! \brief Clone (make deep copy of) sampler.
     *  \return Returned cloned sampler.
     */
    Regular* Regular::clone() const
    {
        return new Regular(*this);
    }



    /*! \brief Get the type of the sampler object.
     *  \return Returns the type of the sampler object.
     */
    Sampler::Type Regular::getType() const
    {
        return Sampler::Regular;
    }



    /*! \brief Generates samples.
     */
    void Regular::generateSamples()
    {
        samples_->reserve(numSamples_);

        Point2D pp;
        double sqrtNumSamples = sqrt(numSamples_);

        for (int p = 0; p < sqrtNumSamples; ++p)
        {
            for (int q = 0; q < sqrtNumSamples; ++q)
            {
                pp.x_ = (1.0 / (2 * sqrtNumSamples)) + (p / sqrtNumSamples);
                pp.y_ = (1.0 / (2 * sqrtNumSamples)) + (q / sqrtNumSamples);

                samples_->push_back(pp);
            }
        }
        this->shuffleIndices();
    }

} // end of namespace iiit