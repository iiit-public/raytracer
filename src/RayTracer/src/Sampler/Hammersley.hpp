// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Hammersley.hpp
 *  \author Thomas Nuernberg
 *  \date 01.06.2016
 *  \brief Definition of Hammersley sampler.
 */

#ifndef __HAMMERSLEY_HPP__
#define __HAMMERSLEY_HPP__

#include "Sampler.hpp"



namespace iiit
{

    /*! \brief Class for Hammerslay sampling.
     */
    class Hammersley : public Sampler
    {
    public:
        Hammersley();
        Hammersley(int numSamples);
        Hammersley(const Hammersley& other);

        virtual Hammersley* clone() const;

        void generateSamples();
        Sampler::Type getType() const;

    private:
        double phi(int n);
    };

} // end of namespace iiit

#endif // __HAMMERSLEY_HPP__