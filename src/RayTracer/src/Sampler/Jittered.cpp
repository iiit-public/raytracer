// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Jittered.cpp
 *  \author Mohamed Salem Koubaa
 *  \date 13.04.2015
 *  \brief Implementation of one type of samplers.
 */

#include "Jittered.hpp"

#include <math.h>
#include <random>
#include <iostream>
#include <chrono>



namespace iiit
{

    /*! \brief Default constructor.
     */
    Jittered::Jittered()
        : Sampler(), seed_(std::mt19937::default_seed)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] numSamples Number of samples per set.
     *  \param[in] numSets Number of sample sets.
     *
     *  \attention The number of samples should be a square number to evenly cover the unit square with
     *  samples.
     */
    Jittered::Jittered(int numSamples, int numSets)
        : Sampler(numSamples, numSets), seed_(std::mt19937::default_seed)
    {
        if (numSamples_ < 0)
        {
            std::cout << "Warning: Negative number of samples not allowed. Using 1 sample." << std::endl;
            numSamples_ = 1;
        }
        else
        {
            int root = static_cast<int>(floor(sqrt(numSamples_) + 0.5));
            if (numSamples_ != root * root)
            {
                std::cout << "Warning: Number of samples for jittered sampling should be a square number. Using " << root * root << " samples." << std::endl;
                numSamples_ = root * root;
            }
        }
    }



    /*! \brief Copy constructor.
     *  \param[in] other Sampler object to copy.
     */
    Jittered::Jittered(const Jittered& other)
        : Sampler(other), seed_(std::mt19937::default_seed)
    {
    }



    /*! \brief Clone (make deep copy of) sampler.
     *  \return Returned cloned sampler.
     */
    Jittered* Jittered::clone() const
    {
        return new Jittered(*this);
    }



    /*! \brief Get the type of the sampler object.
     *  \return Returns the type of the sampler object.
     */
    Sampler::Type Jittered::getType() const
    {
        return Sampler::Jittered;
    }



    /*! \brief Set seed for rand() function
     */
    void Jittered::setSeed(unsigned seed)
    {
        seed_ = seed;
    }



    /*! \brief Randomly generates samples.
     */
    void Jittered::generateSamples()
    {
        samples_->reserve(numSets_ * numSamples_);

        Point2D pp;
        double sqrtNumSamples = sqrt(numSamples_);
        std::mt19937 generator(seed_);
        std::uniform_real_distribution<double> distribution(0.0, 1.0);
        for (int i = 0; i < numSets_; ++i)
        {
            for ( int p= 0; p < sqrtNumSamples; p++)
            {
                for (int q = 0; q < sqrtNumSamples; q++)
                {
                    pp.x_ = ( p / sqrtNumSamples) + distribution(generator) / sqrtNumSamples;
                    pp.y_ = ( q / sqrtNumSamples) + distribution(generator) / sqrtNumSamples;

                    samples_->push_back(pp);
                }
            }
        }
        this->shuffleIndices();
    }

} // end of namespace iiit