// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file MultiJittered.hpp
 *  \author Thomas Nuernberg
 *  \date 01.06.2016
 *  \brief Implementation of multi-jittered sampler.
 */


#include "MultiJittered.hpp"

#include <math.h>
#include <random>
#include <iostream>



namespace iiit
{

    /*! \brief Default constructor.
     */
    MultiJittered::MultiJittered()
        : Sampler(), seed_(std::mt19937::default_seed)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] numSamples Number of samples per set.
     *  \param[in] numSets Number of sample sets.
     *
     *  \attention The number of samples should be a square number to evenly cover the unit square with
     *  samples.
     */
    MultiJittered::MultiJittered(int numSamples, int numSets)
        : Sampler(numSamples, numSets), seed_(std::mt19937::default_seed)
    {
        if (numSamples_ < 0)
        {
            std::cout << "Warning: Negative number of samples not allowed. Using 1 sample." << std::endl;
            numSamples_ = 1;
        }
        else
        {
            int root = static_cast<int>(floor(sqrt(numSamples_) + 0.5));
            if (numSamples_ != root * root)
            {
                std::cout << "Warning: Number of samples for multi-jittered sampling should be a square number. Using " << root * root << " samples." << std::endl;
                numSamples_ = root * root;
            }
        }
    }



    /*! \brief Copy constructor.
     *  \param[in] other Sampler object to copy.
     */
    MultiJittered::MultiJittered(const MultiJittered& other)
        : Sampler(other), seed_(std::mt19937::default_seed)
    {
    }



    /*! \brief Clone (make deep copy of) sampler.
     *  \return Returned cloned sampler.
     */
    MultiJittered* MultiJittered::clone() const
    {
        return new MultiJittered(*this);
    }



    /*! \brief Get the type of the sampler object.
     *  \return Returns the type of the sampler object.
     */
    Sampler::Type MultiJittered::getType() const
    {
        return Sampler::MultiJittered;
    }



    /*! \brief Set seed for rand() function
     */
    void MultiJittered::setSeed(unsigned seed)
    {
        seed_ = seed;
    }



    /*! \brief Randomly generates samples.
     */
    void MultiJittered::generateSamples()
    {
        double sqrtNumSamples = sqrt(numSamples_);
        double subcellWidth = 1.0 / (static_cast<double>(numSamples_));
        std::mt19937 doubleGenerator(seed_);
        std::uniform_real_distribution<double> doubleDistribution(0.0, subcellWidth);
        std::mt19937 intGenerator(seed_);

        // fill the samples array with dummy points to allow us to use the [ ] notation when we set the initial patterns
        Point2D fillPoint;
        samples_->resize(numSamples_ * numSets_, fillPoint);

        // distribute points in the initial patterns
        for (int p = 0; p < numSets_; ++p)
        {
            for (int i = 0; i < sqrtNumSamples; ++i)
            {
                for (int j = 0; j < sqrtNumSamples; j++)
                {
                    (*samples_)[i * static_cast<int>(sqrtNumSamples) + j + p * numSamples_].x_ = (i * static_cast<int>(sqrtNumSamples) + j) * subcellWidth + doubleDistribution(doubleGenerator);
                    (*samples_)[i * static_cast<int>(sqrtNumSamples) + j + p * numSamples_].y_ = (j * static_cast<int>(sqrtNumSamples) + i) * subcellWidth + doubleDistribution(doubleGenerator);
                }
            }
        }

        // shuffle x-coordinates
        for (int p = 0; p < numSets_; ++p)
        {
            for (int i = 0; i < sqrtNumSamples; ++i)
            {
                for (int j = 0; j < sqrtNumSamples; ++j)
                {
                    std::uniform_int_distribution<int> intDistribution(j, static_cast<int>(sqrtNumSamples) - 1);
                    int k = intDistribution(intGenerator);
                    double t = (*samples_)[i * static_cast<int>(sqrtNumSamples) + j + p * numSamples_].x_;
                    (*samples_)[i * static_cast<int>(sqrtNumSamples) + j + p * numSamples_].x_ = (*samples_)[i * static_cast<int>(sqrtNumSamples) + k + p * numSamples_].x_;
                    (*samples_)[i * static_cast<int>(sqrtNumSamples) + k + p * numSamples_].x_ = t;
                }
            }
        }

        // shuffle y coordinates
        for (int p = 0; p < numSets_; ++p)
        {
            for (int i = 0; i < sqrtNumSamples; ++i)
            {
                for (int j = 0; j < sqrtNumSamples; ++j)
                {
                    std::uniform_int_distribution<int> intDistribution(j, static_cast<int>(sqrtNumSamples) - 1);
                    int k = intDistribution(intGenerator);
                    double t = (*samples_)[j * static_cast<int>(sqrtNumSamples) + i + p * numSamples_].y_;
                    (*samples_)[j * static_cast<int>(sqrtNumSamples) + i + p * numSamples_].y_ = (*samples_)[k * static_cast<int>(sqrtNumSamples) + i + p * numSamples_].y_;
                    (*samples_)[k * static_cast<int>(sqrtNumSamples) + i + p * numSamples_].y_ = t;
                }
            }
        }
        this->shuffleIndices();
    }

} // end of namespace iiit