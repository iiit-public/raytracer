// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*!
 *  \file Jittered.hpp
 *  \author Mohamed Salem Koubaa
 *  \date 13.04.2015
 *  \brief Definition of one type of samplers.
 */

#ifndef __JITTERED_HPP__
#define __JITTERED_HPP__

#include "Sampler.hpp"



namespace iiit
{

    /*! \brief Class for jittered sampling.
     *
     *  Based in regular sampling, samples are jittered by a margin.
     */
    class Jittered : public Sampler {
    public:
        Jittered();
        Jittered(int numSamples, int numSets = 83);
        Jittered(const Jittered& other);

        virtual Jittered* clone() const;

        void setSeed(unsigned seed);
        void generateSamples();
        Sampler::Type getType() const;

    private:
        unsigned seed_; ///< Seed of random number generator.
    };

} // end of namespace iiit

#endif // __JITTERED_HPP__