// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Regular.hpp
 *  \author Mohamed Salem Koubaa
 *  \date 20.04.2015
 *  \brief Definition of the regular sampler.
 */

#ifndef __REGULAR_HPP__
#define __REGULAR_HPP__

#include "Sampler.hpp" 



namespace iiit
{

    /*! \brief Class providing regular samples in the unit square.
     */
    class Regular : public Sampler
    {
    public:
        Regular();
        Regular(int numSamples);
        Regular(const Regular& other);

        virtual Regular* clone() const;
        void generateSamples();

        Sampler::Type getType() const;
    };

} // end of namespace iiit

#endif // __REGULAR_HPP__