// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file NRooks.hpp
 *  \author Thomas Nuernberg
 *  \date 01.06.2016
 *  \brief Implementation of n-rooks sampler.
 */

#include "NRooks.hpp"

#include <math.h>
#include <random>
#include <iostream>



namespace iiit
{

    /*! \brief Default constructor.
     */
    NRooks::NRooks()
        : Sampler(), seed_(std::mt19937::default_seed)
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] numSamples Number of samples per set.
     *  \param[in] numSets Number of sample sets.
     *
     *  \attention The number of samples should be a square number to evenly cover the unit square with
     *  samples.
     */
    NRooks::NRooks(int numSamples, int numSets)
        : Sampler(numSamples, numSets), seed_(std::mt19937::default_seed)
    {
        if (numSamples_ < 0)
        {
            std::cout << "Warning: Negative number of samples not allowed. Using 1 sample." << std::endl;
            numSamples_ = 1;
        }
    }



    /*! \brief Copy constructor.
     *  \param[in] other Sampler object to copy.
     */
    NRooks::NRooks(const NRooks& other)
        : Sampler(other), seed_(std::mt19937::default_seed)
    {
    }



    /*! \brief Clone (make deep copy of) sampler.
     *  \return Returned cloned sampler.
     */
    NRooks* NRooks::clone() const
    {
        return new NRooks(*this);
    }



    /*! \brief Get the type of the sampler object.
     *  \return Returns the type of the sampler object.
     */
    Sampler::Type NRooks::getType() const
    {
        return Sampler::NRooks;
    }



    /*! \brief Set seed for rand() function
     */
    void NRooks::setSeed(unsigned seed)
    {
        seed_ = seed;
    }



    /*! \brief Randomly generates samples.
     */
    void NRooks::generateSamples()
    {
        samples_->reserve(numSets_ * numSamples_);

        Point2D pp;
        std::mt19937 doubleGenerator(seed_);
        std::uniform_real_distribution<double> doubleDistribution(0.0, 1.0);
        for (int i = 0; i < numSets_; ++i)
        {
            for ( int p = 0; p < numSamples_; p++)
            {
                pp.x_ = (p + doubleDistribution(doubleGenerator)) / numSamples_;
                pp.y_ = (p + doubleDistribution(doubleGenerator)) / numSamples_;
                samples_->push_back(pp);
            }
        }

        std::mt19937 intGenerator(seed_);
        std::uniform_int_distribution<int> intDistribution(0, numSamples_ - 1);

        // shuffle x-coordinates
        for (int i = 0; i < numSets_; ++i)
        {
            for (int j = 0; j < numSamples_ - 1; ++j)
            {
                int target = intDistribution(intGenerator) + i * numSamples_;
                double temp = (*samples_)[j + i * numSamples_ + 1].x_;
                (*samples_)[j + i * numSamples_ + 1].x_ = (*samples_)[target].x_;
                (*samples_)[target].x_ = temp;
            }
        }

        // shuffle y-coordinates
        for (int i = 0; i < numSets_; ++i)
        {
            for (int j = 0; j < numSamples_ - 1; ++j)
            {
                int target = intDistribution(intGenerator) + i * numSamples_;
                double temp = (*samples_)[j + i * numSamples_ + 1].y_;
                (*samples_)[j + i * numSamples_ + 1].y_ = (*samples_)[target].y_;
                (*samples_)[target].y_ = temp;
            }
        }
        this->shuffleIndices();
    }

} // end of namespace iiit