// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Phong.hpp
 *  \author Thomas Nuernberg
 *  \date 12.06.2015
 *  \brief Definition of phong material class.
 */

#ifndef __PHONG_HPP__
#define __PHONG_HPP__

#include <memory>

#include "Scene.hpp"
#include "Materials/Material.hpp"
#include "BRDF/Lambertian.hpp"
#include "BRDF/GlossySpecular.hpp"
#include "Textures/Texture.hpp"
#include "Textures/ConstantColor.hpp"
#include "Utilities/ShadingData.hpp"



namespace iiit
{

    /*! \brief Material with diffuse and specular reflection.
     */
    template <class SpectrumType>
    class Phong : public Material<SpectrumType>
    {
    public:
        Phong(void);
        Phong(SpectrumType color);
        ~Phong(void);

        void setAmbientReflectionFactor(const float k);
        void setDiffuseReflectionFactor(const float k);
        void setDiffuseColor(const SpectrumType& color);
        void setDiffuseTexture(std::shared_ptr<Texture<SpectrumType> > texture);
        void setSpecularReflectionFactor(const float k);
        void setSpecularExponent(float exp);
        void setSpecularColor(const SpectrumType& color);
        void setSpecularTexture(std::shared_ptr<Texture<SpectrumType> > texture);

        virtual SpectrumType shade(ShadingData<SpectrumType>& shadingDataObject) const;
        virtual SpectrumType areaLightShade(ShadingData<SpectrumType>& shadingDataObject) const;
        virtual SpectrumType pathShade(ShadingData<SpectrumType>& shadingDataObject) const {return SpectrumType(0.f);}

    private:
        std::shared_ptr<Lambertian<SpectrumType> > ambientBrdf_; ///< Brdf for ambient shading.
        std::shared_ptr<Lambertian<SpectrumType> > diffuseBrdf_; ///< Brdf for diffuse shading.
        std::shared_ptr<GlossySpecular<SpectrumType> > specularBrdf_; ///< Brdf for specular shading.
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Phong<SpectrumType>::Phong(void)
    {
        ambientBrdf_ = std::shared_ptr<Lambertian<SpectrumType> >(new Lambertian<SpectrumType>(1.0, std::shared_ptr<Texture<SpectrumType> >(new ConstantColor<SpectrumType>)));
        diffuseBrdf_ = std::shared_ptr<Lambertian<SpectrumType> >(new Lambertian<SpectrumType>(1.0, std::shared_ptr<Texture<SpectrumType> >(new ConstantColor<SpectrumType>)));
        specularBrdf_ = std::shared_ptr<GlossySpecular<SpectrumType> >(new GlossySpecular<SpectrumType>(1.0, 1.0, std::shared_ptr<Texture<SpectrumType> >(new ConstantColor<SpectrumType>(SpectrumType(1.f)))));
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    Phong<SpectrumType>::~Phong(void)
    {
    }



    /*! \brief Constructor with arguments.
     */
    template <class SpectrumType>
    Phong<SpectrumType>::Phong(SpectrumType color)
    {
        ambientBrdf_ = std::shared_ptr<Lambertian<SpectrumType> >(new Lambertian<SpectrumType>(1.0, std::shared_ptr<Texture<SpectrumType> >(new ConstantColor<SpectrumType>(color))));
        diffuseBrdf_ = std::shared_ptr<Lambertian<SpectrumType> >(new Lambertian<SpectrumType>(1.0, std::shared_ptr<Texture<SpectrumType> >(new ConstantColor<SpectrumType>(color))));
        specularBrdf_ = std::shared_ptr<GlossySpecular<SpectrumType> >(new GlossySpecular<SpectrumType>(1.0, std::shared_ptr<Texture<SpectrumType> >(new ConstantColor<SpectrumType>(SpectrumType(1.f)))));
    }



    /*! \brief Set reflection factor for ambient light.
     *  \param[in] k Reflection factor.
     */
    template <class SpectrumType>
    void Phong<SpectrumType>::setAmbientReflectionFactor(const float k)
    {
        ambientBrdf_->setKd(k);
    }



    /*! \brief Set reflection factor for diffuse light.
     *  \param[in] k Reflection factor.
     */
    template <class SpectrumType>
    void Phong<SpectrumType>::setDiffuseReflectionFactor(const float k)
    {
        diffuseBrdf_->setKd(k);
    }



    /*! \brief Set color of the diffuse reflection.
     *  \param[in] color Spectrum object.
     */
    template <class SpectrumType>
    void Phong<SpectrumType>::setDiffuseColor(const SpectrumType& color)
    {
        ambientBrdf_->setCd(color);
        diffuseBrdf_->setCd(color);
    }



    /*! \brief Set diffuse texture.
     *  \param[in] texture Diffuse texture.
     */
    template <class SpectrumType>
    void Phong<SpectrumType>::setDiffuseTexture(std::shared_ptr<Texture<SpectrumType> > texture)
    {
        ambientBrdf_->setTexture(texture);
        diffuseBrdf_->setTexture(texture);
    }



    /*! \brief Set reflection factor for specular light.
     *  \param[in] k Reflection factor.
     */
    template <class SpectrumType>
    void Phong<SpectrumType>::setSpecularReflectionFactor(const float k)
    {
        specularBrdf_->setKs(k);
    }



    /*! \brief Set exponent for specular light.
     *  \param[in] exp Exponent.
     */
    template <class SpectrumType>
    void Phong<SpectrumType>::setSpecularExponent(float exp)
    {
        specularBrdf_->setExp(exp);
    }



    /*! \brief Set color of the specular reflection.
     *  \param[in] color Spectrum object.
     */
    template <class SpectrumType>
    void Phong<SpectrumType>::setSpecularColor(const SpectrumType& color)
    {
        specularBrdf_->setCs(color);
    }



    /*! \brief Set specular texture.
     *  \param[in] texture Specular texture.
     */
    template <class SpectrumType>
    void Phong<SpectrumType>::setSpecularTexture(std::shared_ptr<Texture<SpectrumType> > texture)
    {
        specularBrdf_->setTexture(texture);
    }



    /*! \brief Calculate color of the material.
     *  \param[in] shadingData Shading data of ray-object intersection.
     *  \return Returns resulting color.
     */
    template <class SpectrumType>
    SpectrumType Phong<SpectrumType>::shade(ShadingData<SpectrumType>& shadingData) const
    {
        Vector3D outgoingRayDirection = -(shadingData.ray_.direction_);
        outgoingRayDirection.normalize();
        std::shared_ptr<const Scene<SpectrumType>> scene(shadingData.scene_.lock());
        
        // Share of the illumination from ambient light
        SpectrumType L;
        if (!scene->getAmbientLight().expired())
        {
            L = ambientBrdf_->rho(shadingData, outgoingRayDirection) * scene->getAmbientLight().lock()->L(shadingData);
        }
        
        // Share of illumination by diffuse shading
        for (unsigned int i = 0; i < scene->lights_.size(); ++i)
        {
            Vector3D incomingLightDirection = scene->lights_[i]->getDirection(shadingData);
            float dotProduct = static_cast<float>(shadingData.normal_ * incomingLightDirection);
            
            if (dotProduct > 0.0)
            {
                bool shadowed = false;
                if (scene->lights_[i]->castsShadows())
                {
                    Ray shadowRay(shadingData.hitPoint_, incomingLightDirection);
                    shadowed = scene->lights_[i]->shadowed(shadowRay, shadingData);
                }
                
                if (!shadowed)
                {
                    SpectrumType color = diffuseBrdf_->f(shadingData, incomingLightDirection, outgoingRayDirection);
                    color += specularBrdf_->f(shadingData, incomingLightDirection, outgoingRayDirection);
                    color = color * scene->lights_[i]->L(shadingData) * dotProduct;
                    L += color;
                }
            }
        }
        return L;
    }
    
    
    
    /*! \brief Calculate color of the material.
     *  \param[in] shadingData Shading data of ray-object intersection.
     *  \return Returns resulting color.
     */
    template <class SpectrumType>
    SpectrumType Phong<SpectrumType>::areaLightShade(ShadingData<SpectrumType>& shadingData) const
    {
        Vector3D outgoingRayDirection = -(shadingData.ray_.direction_);
        outgoingRayDirection.normalize();
        std::shared_ptr<const Scene<SpectrumType>> scene(shadingData.scene_.lock());
        
        // Share of the illumination from ambient light
        SpectrumType L;
        if (!scene->getAmbientLight().expired())
        {
            L = ambientBrdf_->rho(shadingData, outgoingRayDirection) * scene->getAmbientLight().lock()->L(shadingData);
        }
        
        // Share of illumination by diffuse shading
        for (unsigned int i = 0; i < scene->lights_.size(); ++i)
        {
            Vector3D incomingLightDirection = scene->lights_[i]->getDirection(shadingData);
            float dotProduct = static_cast<float>(shadingData.normal_ * incomingLightDirection);
            
            if (dotProduct > 0.0)
            {
                bool shadowed = false;
                if (scene->lights_[i]->castsShadows())
                {
                    Ray shadowRay(shadingData.hitPoint_, incomingLightDirection);
                    shadowed = scene->lights_[i]->shadowed(shadowRay, shadingData);
                }
                
                if (!shadowed)
                {
                    SpectrumType color = diffuseBrdf_->f(shadingData, incomingLightDirection, outgoingRayDirection);
                    color += specularBrdf_->f(shadingData, incomingLightDirection, outgoingRayDirection);
                    color = color * scene->lights_[i]->L(shadingData)
                    * scene->lights_[i]->g(shadingData)
                    * dotProduct
                    / scene->lights_[i]->pdf(shadingData);;
                    L += color;
                }
            }
        }
        return L;
    }

} // end of namespace iiit

#endif // __PHONG_HPP__
