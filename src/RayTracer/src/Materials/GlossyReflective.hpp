// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file GlossyReflective.hpp
 *  \author Thomas Nuernberg
 *  \date 25.06.2016
 *  \brief Definition of GlossyReflective material class.
 */

#ifndef __GLOSSYREFLECTIVE_HPP__
#define __GLOSSYREFLECTIVE_HPP__

#include <memory>

#include "Scene.hpp"
#include "Materials/Phong.hpp"
#include "BRDF/Lambertian.hpp"
#include "BRDF/GlossySpecular.hpp"
#include "Textures/Texture.hpp"
#include "Textures/ConstantColor.hpp"
#include "Utilities/ShadingData.hpp"



namespace iiit
{

    /*! \brief Material with diffuse and specular reflection.
     */
    template <class SpectrumType>
    class GlossyReflective : public Phong<SpectrumType>
    {
    public:
        GlossyReflective(void);
        GlossyReflective(SpectrumType color);
        ~GlossyReflective(void);

        void setReflectionFactor(const float k);
        void setReflectionExponent(float exp);
        void setReflectionColor(const SpectrumType& color);
        void setSampler(std::shared_ptr<Sampler> sampler);

        virtual SpectrumType shade(ShadingData<SpectrumType>& shadingDataObject) const;
        virtual SpectrumType areaLightShade(ShadingData<SpectrumType>& shadingDataObject) const;
        virtual SpectrumType pathShade(ShadingData<SpectrumType>& shadingDataObject) const;

    private:
        std::shared_ptr<GlossySpecular<SpectrumType> > glossyReflectiveBrdf_; ///< Brdf for glossy reflection.
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    GlossyReflective<SpectrumType>::GlossyReflective()
        : Phong<SpectrumType>()
    {
        glossyReflectiveBrdf_ = std::shared_ptr<GlossySpecular<SpectrumType> >(new GlossySpecular<SpectrumType>(1.0, 1.0, std::shared_ptr<Texture<SpectrumType> >(new ConstantColor<SpectrumType>(SpectrumType(1.f)))));
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    GlossyReflective<SpectrumType>::~GlossyReflective(void)
    {
    }



    /*! \brief Constructor with arguments.
     */
    template <class SpectrumType>
    GlossyReflective<SpectrumType>::GlossyReflective(SpectrumType color)
        : Phong<SpectrumType>(color)
    {
        glossyReflectiveBrdf_ = std::shared_ptr<GlossySpecular<SpectrumType> >(new GlossySpecular<SpectrumType>(1.0, std::shared_ptr<Texture<SpectrumType> >(new ConstantColor<SpectrumType>(SpectrumType(1.f)))));
    }



    /*! \brief Set reflection factor for reflected light.
     *  \param[in] k Reflection factor.
     */
    template <class SpectrumType>
    void GlossyReflective<SpectrumType>::setReflectionFactor(const float k)
    {
        glossyReflectiveBrdf_->setKs(k);
    }



    /*! \brief Set exponent for reflected light.
     *  \param[in] exp Exponent.
     */
    template <class SpectrumType>
    void GlossyReflective<SpectrumType>::setReflectionExponent(float exp)
    {
        glossyReflectiveBrdf_->setExp(exp);
    }



    /*! \brief Set color of the glossy reflection.
     *  \param[in] color Spectrum object.
     */
    template <class SpectrumType>
    void GlossyReflective<SpectrumType>::setReflectionColor(const SpectrumType& color)
    {
        glossyReflectiveBrdf_->setCs(color);
    }
    
    
    
    /*! \brief Set sampling object.
     *  \param[in] sampler Sampling object.
     */
    template <class SpectrumType>
    void GlossyReflective<SpectrumType>::setSampler(std::shared_ptr<Sampler> sampler)
    {
        glossyReflectiveBrdf_->setSampler(sampler);
    }



    /*! \brief Calculate color of the material.
     *  \param[in] shadingData Shading data of ray-object intersection.
     *  \return Returns resulting color.
     */
    template <class SpectrumType>
    SpectrumType GlossyReflective<SpectrumType>::shade(ShadingData<SpectrumType>& shadingData) const
    {
        SpectrumType L = Phong<SpectrumType>::shade(shadingData); // direct illumination

        Vector3D outgoing(-(shadingData.ray_.direction_));
        Vector3D incoming;
        float pdf;
        SpectrumType fr = glossyReflectiveBrdf_->fSample(shadingData, incoming, outgoing, pdf);
        Ray reflectedRay(shadingData.hitPoint_, incoming);

        L += fr * shadingData.scene_.lock()->getTracer().lock()->traceRay(reflectedRay, shadingData.depth_ + 1)
            * static_cast<float>(Vector3D(shadingData.normal_) * incoming);

        return L;
    }



    /*! \brief Calculate color of the material for area lights.
     *  \param[in] shadingData Shading data of ray-object intersection.
     *  \return Returns resulting color.
     */
    template <class SpectrumType>
    SpectrumType GlossyReflective<SpectrumType>::areaLightShade(ShadingData<SpectrumType>& shadingData) const
    {
        SpectrumType L = Phong<SpectrumType>::areaLightShade(shadingData); // direct illumination
        
        Vector3D outgoing(-(shadingData.ray_.direction_));
        Vector3D incoming;
        float pdf;
        SpectrumType fr = glossyReflectiveBrdf_->fSample(shadingData, incoming, outgoing, pdf);
        Ray reflectedRay(shadingData.hitPoint_, incoming);
        
        L += fr * shadingData.scene_.lock()->getTracer().lock()->traceRay(reflectedRay, shadingData.depth_ + 1)
            * static_cast<float>(Vector3D(shadingData.normal_) * incoming) / pdf;
        
        return L;
    }
    
    
    
    /*! \brief Calculate color of the material for global illumination.
     *  \param[in] shadingData Shading data of ray-object intersection.
     *  \return Returns resulting color.
     */
    template <class SpectrumType>
    SpectrumType GlossyReflective<SpectrumType>::pathShade(ShadingData<SpectrumType>& shadingData) const
    {
        
        Vector3D outgoing(-(shadingData.ray_.direction_));
        Vector3D incoming;
        float pdf;
        SpectrumType fr = glossyReflectiveBrdf_->fSample(shadingData, incoming, outgoing, pdf);
        Ray reflectedRay(shadingData.hitPoint_, incoming);
        
        SpectrumType L = fr * shadingData.scene_.lock()->getTracer().lock()->traceRay(reflectedRay, shadingData.depth_ + 1)
            * static_cast<float>(Vector3D(shadingData.normal_) * incoming) / pdf;
        
        return L;
    }

} // end of namespace iiit

#endif // __GLOSSYREFLECTIVE_HPP__