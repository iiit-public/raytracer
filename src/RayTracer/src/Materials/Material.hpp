// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Material.hpp
 *  \author Christian Zimmermann
 *  \date 18.05.2015
 *  \brief Definition of class Material.
 *
 *  Definition of class Material representing the material of a GeometricObject.
 */

#ifndef __MATERIAL_HPP__
#define __MATERIAL_HPP__



namespace iiit
{
    
    template <class T> class ShadingData; // forward declaration
    
    

    /*! \brief Abstract base class for materials.
     */
    template <class SpectrumType>
    class Material
    {
    public:
        /*! \brief Virtual method to perform shading at ray-object intersection.
         *  \param[in] shadingDataObject Shading data of the ray-object intersection.
         *  \return Returns shaded color.
         */
        virtual SpectrumType shade(ShadingData<SpectrumType>& shadingDataObject) const = 0;

        /*! \brief Virtual method to perform shading at ray-object intersection for emissive materials.
         *  \param[in] shadingDataObject Shading data of the ray-object intersection.
         *  \return Returns shaded color.
         */
        virtual SpectrumType areaLightShade(ShadingData<SpectrumType>& shadingDataObject) const = 0;
        
        /*! \brief Virtual method to perform shading at ray-object intersection for global illumination.
         *  \param[in] shadingDataObject Shading data of the ray-object intersection.
         *  \return Returns shaded color.
         */
        virtual SpectrumType pathShade(ShadingData<SpectrumType>& shadingDataObject) const = 0;
        
        /*! \brief Get color of emitted ligth.
         *  \param[in] shadingDataObject Shading data of the ray-object intersection.
         *  \return Returns emitted color.
         */
        virtual SpectrumType getLe(const ShadingData<SpectrumType>& shadingDataObject) const {return SpectrumType(0.f);};
    };

} // end of namespace iiit

#endif // __MATERIAL_HPP__