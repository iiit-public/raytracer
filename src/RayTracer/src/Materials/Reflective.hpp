// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Reflective.hpp
 *  \author Thomas Nuernberg
 *  \date 21.06.2016
 *  \brief Definition of reflective material class.
 */

#ifndef __REFLECTIVE_HPP__
#define __REFLECTIVE_HPP__

#include <memory>

#include "Materials/Phong.hpp"
#include "BRDF/PerfectSpecular.hpp"
#include "Utilities/Ray.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Vector3D.hpp"



namespace iiit
{

    /*! \brief Reflective material.
     */
    template <class SpectrumType>
    class Reflective : public Phong<SpectrumType>
    {
    public:
        Reflective(void);
        ~Reflective(void);
        
        void setReflectiveFactor(float k);
        void setReflectiveColor(const SpectrumType& color);

        virtual SpectrumType shade(ShadingData<SpectrumType>& shadingDataObject) const;
        virtual SpectrumType areaLightShade(ShadingData<SpectrumType>& shadingDataObject) const;
        virtual SpectrumType pathShade(ShadingData<SpectrumType>& shadingDataObject) const;

    private:
        std::shared_ptr<PerfectSpecular<SpectrumType> > reflectiveBrdf_; ///< Brdf for reflective shading.
    };
    
    
    
    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Reflective<SpectrumType>::Reflective(void)
        : Phong<SpectrumType>()
    {
        reflectiveBrdf_ = std::shared_ptr<PerfectSpecular<SpectrumType> >(new PerfectSpecular<SpectrumType>(1.0, SpectrumType(1.f)));
    }
    
    
    
    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    Reflective<SpectrumType>::~Reflective(void)
    {
    }
    
    
    
    /*! \brief Set reflection factor for perfect specular light.
     *  \param[in] k Reflection factor.
     */
    template <class SpectrumType>
    void Reflective<SpectrumType>::setReflectiveFactor(float k)
    {
        reflectiveBrdf_->setK(k);
    }
    
    
    
    /*! \brief Set color of the perfect specular reflection.
     *  \param[in] color Spectrum object.
     */
    template <class SpectrumType>
    void Reflective<SpectrumType>::setReflectiveColor(const SpectrumType& color)
    {
        reflectiveBrdf_->setCr(color);
    }
    
    
    
    /*! \brief Calculate color of the material.
     *  \param[in] shadingData Shading data of ray-object intersection.
     *  \return Returns resulting color.
     */
    template <class SpectrumType>
    SpectrumType Reflective<SpectrumType>::shade(ShadingData<SpectrumType>& shadingData) const
    {
        SpectrumType L = Phong<SpectrumType>::shade(shadingData); // direct illumination
        
        Vector3D outgoing(-(shadingData.ray_.direction_));
        Vector3D incoming;
        SpectrumType fr = reflectiveBrdf_->fSample(shadingData, incoming, outgoing);
        Ray reflectedRay(shadingData.hitPoint_, incoming);
        
        L += fr * shadingData.scene_.lock()->getTracer().lock()->traceRay(reflectedRay, shadingData.depth_ + 1)
            * static_cast<float>(Vector3D(shadingData.normal_) * incoming);
        
        return L;
    }



    /*! \brief Calculate color of the material for direct lighting.
     *  \param[in] shadingData Shading data of ray-object intersection.
     *  \return Returns resulting color.
     */
    template <class SpectrumType>
    SpectrumType Reflective<SpectrumType>::areaLightShade(ShadingData<SpectrumType>& shadingData) const
    {
        SpectrumType L = Phong<SpectrumType>::areaLightShade(shadingData); // direct illumination
        
        Vector3D outgoing(-(shadingData.ray_.direction_));
        Vector3D incoming;
        SpectrumType fr = reflectiveBrdf_->fSample(shadingData, incoming, outgoing);
        Ray reflectedRay(shadingData.hitPoint_, incoming);
        
        L += fr * shadingData.scene_.lock()->getTracer().lock()->traceRay(reflectedRay, shadingData.depth_ + 1)
            * static_cast<float>(Vector3D(shadingData.normal_) * incoming);

        return L;
    }
    
    
    
    /*! \brief Virtual method to perform shading at ray-object intersection for global illumination.
     *  \param[in] shadingDataObject Shading data of the ray-object intersection.
     *  \return Returns shaded color.
     */
    template <class SpectrumType>
    SpectrumType Reflective<SpectrumType>::pathShade(ShadingData<SpectrumType>& shadingDataObject) const
    {
        Vector3D incoming;
        Vector3D outgoing = -shadingDataObject.ray_.direction_;
        float pdf;
        
        SpectrumType f = reflectiveBrdf_->fSample(shadingDataObject, incoming, outgoing, pdf);
        float dotProduct = static_cast<float>(shadingDataObject.normal_ * incoming);
        Ray reflectedRay(shadingDataObject.hitPoint_, incoming);
        
        return (f * shadingDataObject.scene_.lock()->getTracer().lock()->traceRay(reflectedRay, shadingDataObject.depth_ + 1) * dotProduct / pdf);
    }

} // end of namespace iiit

#endif // __REFLECTIVE_HPP__