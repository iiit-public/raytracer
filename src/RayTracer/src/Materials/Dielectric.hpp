// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Dielectric.hpp
 *  \author Thomas Nuernberg
 *  \date 07.05.2017
 *  \brief Definition of dielectric material class.
 */

#ifndef __DIELECTRIC_HPP__
#define __DIELECTRIC_HPP__

#include <memory>

#include "Materials/Phong.hpp"
#include "BRDF/FresnelReflector.hpp"
#include "BTDF/FresnelTransmitter.hpp"
#include "Utilities/Ray.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/Constants.h"



namespace iiit
{

    /*! \brief Transparent material.
     */
    template <class SpectrumType>
    class Dielectric : public Phong<SpectrumType>
    {
    public:
        Dielectric(void);
        ~Dielectric(void);
        
        void setEtaIn(float etaIn);
        void setEtaOut(float etaOut);
        void setCfIn(SpectrumType cfIn);
        void setCfOut(SpectrumType cfOut);
        
        virtual SpectrumType shade(ShadingData<SpectrumType>& shadingDataObject) const;
        virtual SpectrumType areaLightShade(ShadingData<SpectrumType>& shadingDataObject) const;
        virtual SpectrumType pathShade(ShadingData<SpectrumType>& shadingDataObject) const;

    private:
        std::shared_ptr<FresnelReflector<SpectrumType> > fresnelBrdf_; ///< Brdf for fresnel reflection.
        std::shared_ptr<FresnelTransmitter<SpectrumType> > fresnelBtdf_; ///< Btdf for fresnel transmission.
        
        SpectrumType cfIn_; ///< Interior color filter.
        SpectrumType cfOut_; ///< Exterior color filter.
    };
    
    
    
    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Dielectric<SpectrumType>::Dielectric(void)
        : Phong<SpectrumType>()
        , cfIn_(1.f)
        , cfOut_(1.f)
    {
        fresnelBrdf_ = std::shared_ptr<FresnelReflector<SpectrumType> >(new FresnelReflector<SpectrumType>(1.f, 1.f));
        fresnelBtdf_ = std::shared_ptr<FresnelTransmitter<SpectrumType> >(new FresnelTransmitter<SpectrumType>(1.f, 1.f));
    }
    
    
    
    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    Dielectric<SpectrumType>::~Dielectric(void)
    {
    }
    
    
    
    /*! \brief Set index of refraction on the inside of the object.
     *  \param[in] etaIn Index of refraction on the inside of the object.
     */
    template <class SpectrumType>
    void Dielectric<SpectrumType>::setEtaIn(float etaIn)
    {
        fresnelBrdf_->setEtaIn(etaIn);
        fresnelBtdf_->setEtaIn(etaIn);
    }
    
    
    
    /*! \brief Set index of refraction on the outside of the object.
     *  \param[in] etaOut Index of refraction on the outside of the object.
     */
    template <class SpectrumType>
    void Dielectric<SpectrumType>::setEtaOut(float etaOut)
    {
        fresnelBrdf_->setEtaOut(etaOut);
        fresnelBtdf_->setEtaOut(etaOut);
    }
    
    
    
    /*! \brief Set interior color filter.
     *  \param[in] cfIn Interior color filter.
     */
    template <class SpectrumType>
    void Dielectric<SpectrumType>::setCfIn(SpectrumType cfIn)
    {
        cfIn_ = cfIn;
    }
    
    
    
    /*! \brief Set exterior color filter.
     *  \param[in] cfOut Exterior color filter.
     */
    template <class SpectrumType>
    void Dielectric<SpectrumType>::setCfOut(SpectrumType cfOut)
    {
        cfOut_ = cfOut;
    }

    
    
    /*! \brief Calculate color of the material.
     *  \param[in] shadingData Shading data of ray-object intersection.
     *  \return Returns resulting color.
     */
    template <class SpectrumType>
    SpectrumType Dielectric<SpectrumType>::shade(ShadingData<SpectrumType>& shadingData) const
    {
        SpectrumType L = Phong<SpectrumType>::shade(shadingData); // direct illumination
        
        Vector3D outgoing(-(shadingData.ray_.direction_));
        Vector3D incoming;
        SpectrumType fr = fresnelBrdf_->fSample(shadingData, incoming, outgoing);
        Ray reflectedRay(shadingData.hitPoint_, incoming);
        float t = HUGE_VALUE;
        SpectrumType Lr;
        SpectrumType Lt;
        float dotProduct = static_cast<float>(shadingData.normal_ * incoming);
        
        if (fresnelBtdf_->checkTotalInternalReflection(shadingData))
        {
            if ((dotProduct < 0.f && !shadingData.normalFlipped_) || (dotProduct > 0.f && shadingData.normalFlipped_))
            {
                // reflected ray inside
                Lr = shadingData.scene_.lock()->getTracer().lock()->traceRay(reflectedRay, t, shadingData.depth_ + 1);
                L += cfIn_.powc(t) * Lr;
            }
            else
            {
                // reflected ray outside
                Lr = shadingData.scene_.lock()->getTracer().lock()->traceRay(reflectedRay, t, shadingData.depth_ + 1);
                L += cfOut_.powc(t) * Lr;
            }
        }
        else
        {
            // no total internal reflection
            Vector3D transmitted;
            SpectrumType ft = fresnelBtdf_->fSample(shadingData, transmitted, outgoing);
            Ray transmittedRay(shadingData.hitPoint_, transmitted);
            float transDotProduct = static_cast<float>(shadingData.normal_ * transmitted);
            
            if ((dotProduct < 0.f && !shadingData.normalFlipped_) || (dotProduct > 0.f && shadingData.normalFlipped_))
            {
                // reflected ray inside
                Lr = fr * shadingData.scene_.lock()->getTracer().lock()->traceRay(reflectedRay, t, shadingData.depth_ + 1)
                    * fabs(dotProduct);
                L += cfIn_.powc(t) * Lr;
                
                // transmitted ray outside
                Lt = ft * shadingData.scene_.lock()->getTracer().lock()->traceRay(transmittedRay, t, shadingData.depth_ + 1)
                    * fabs(transDotProduct);
                L += cfOut_.powc(t) * Lt;
            }
            else
            {
                // reflected ray outside
                Lr = fr * shadingData.scene_.lock()->getTracer().lock()->traceRay(reflectedRay, t, shadingData.depth_ + 1)
                    * fabs(dotProduct);
                L += cfOut_.powc(t) * Lr;
                
                // transmitted ray inside
                Lt = ft * shadingData.scene_.lock()->getTracer().lock()->traceRay(transmittedRay, t, shadingData.depth_ + 1)
                    * fabs(transDotProduct);
                L += cfIn_.powc(t) * Lt;
            }
        }

        return L;
    }



    /*! \brief Calculate color of the material for direct lighting.
     *  \param[in] shadingData Shading data of ray-object intersection.
     *  \return Returns resulting color.
     */
    template <class SpectrumType>
    SpectrumType Dielectric<SpectrumType>::areaLightShade(ShadingData<SpectrumType>& shadingData) const
    {
        return shade(shadingData);
    }
    
    
    
    /*! \brief Virtual method to perform shading at ray-object intersection for global illumination.
     *  \param[in] shadingDataObject Shading data of the ray-object intersection.
     *  \return Returns shaded color.
     */
    template <class SpectrumType>
    SpectrumType Dielectric<SpectrumType>::pathShade(ShadingData<SpectrumType>& shadingData) const
    {
        return shade(shadingData);
    }

} // end of namespace iiit

#endif // __DIELECTRIC_HPP__