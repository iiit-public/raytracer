// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Emissive.hpp
 *  \author Thomas Nuernberg
 *  \date 08.06.2016
 *  \brief Definition of emissive material class used for area lights.
 */

#ifndef __EMISSIVE_HPP__
#define __EMISSIVE_HPP__

#include <memory>

#include "Materials/Material.hpp"
#include "Lights/Light.hpp"
#include "Utilities/ShadingData.hpp"



namespace iiit
{

    /*! \brief Isotropic emissive material.
     */
    template <class SpectrumType>
    class Emissive : public Material<SpectrumType>
    {
    public:
        Emissive(void);
        Emissive(SpectrumType color);
        ~Emissive(void);

        void setRadianceScaling(float ls);
        float getRadianceScaling() const;
        void setColor(const SpectrumType& color);
        SpectrumType getColor() const;

        virtual SpectrumType shade(ShadingData<SpectrumType>& shadingDataObject) const;
        virtual SpectrumType areaLightShade(ShadingData<SpectrumType>& shadingDataObject) const;
        virtual SpectrumType pathShade(ShadingData<SpectrumType>& shadingDataObject) const;
        virtual SpectrumType getLe(const ShadingData<SpectrumType>& shadingDataObject) const;

    private:
        float ls_; ///< Radiance scaling factor.
        SpectrumType color_; ///< Color of emitted light.
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Emissive<SpectrumType>::Emissive(void)
        : ls_(1.f)
        , color_(1.f)
    {
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    Emissive<SpectrumType>::~Emissive(void)
    {
    }



    /*! \brief Constructor with arguments.
     */
    template <class SpectrumType>
    Emissive<SpectrumType>::Emissive(SpectrumType color)
        : ls_(1.f)
        , color_(color)
    {
    }



    /*! \brief Set radiance scaling factor.
     *  \param[in] ls Radiance scaling factor.
     */
    template <class SpectrumType>
    void Emissive<SpectrumType>::setRadianceScaling(float ls)
    {
        ls_ = ls;
    }



    /*! \brief Get radiance scaling factor.
     *  \return Returns radiance scaling factor.
     */
    template <class SpectrumType>
    float Emissive<SpectrumType>::getRadianceScaling() const
    {
        return ls_;
    }



    /*! \brief Set color of emitted light.
     *  \param[in] color Color of emitted light.
     */
    template <class SpectrumType>
    void Emissive<SpectrumType>::setColor(const SpectrumType& color)
    {
        color_ = color;
    }



    /*! \brief Get color of emitted light.
     *  \return Returns color of emitted light.
     */
    template <class SpectrumType>
    SpectrumType Emissive<SpectrumType>::getColor() const
    {
        return color_;
    }



    /*! \brief Calculate color of the material.
     *  \param[in] shadingData Shading data of ray-object intersection.
     *  \return Returns resulting color.
     */
    template <class SpectrumType>
    SpectrumType Emissive<SpectrumType>::shade(ShadingData<SpectrumType>& shadingData) const
    {
        if (-shadingData.normal_ * shadingData.ray_.direction_ > 0.0)
        {
            return color_ * ls_;
        }
        else
        {
            return SpectrumType(0.f);
        }
    }



    /*! \brief Calculate color of the material for direct lighting.
     *  \param[in] shadingData Shading data of ray-object intersection.
     *  \return Returns resulting color.
     */
    template <class SpectrumType>
    SpectrumType Emissive<SpectrumType>::areaLightShade(ShadingData<SpectrumType>& shadingData) const
    {
        if (-shadingData.normal_ * shadingData.ray_.direction_ > 0.0)
        {
            return color_ * ls_;
        }
        else
        {
            return SpectrumType(0.f);
        }
    }
    
    
    
    /*! \brief Virtual method to perform shading at ray-object intersection for global illumination.
     *  \param[in] shadingDataObject Shading data of the ray-object intersection.
     *  \return Returns shaded color.
     */
    template <class SpectrumType>
    SpectrumType Emissive<SpectrumType>::pathShade(ShadingData<SpectrumType>& shadingData) const
    {
        if (-shadingData.normal_ * shadingData.ray_.direction_ > 0.0)
        {
            return color_ * ls_;
        }
        else
        {
            return SpectrumType(0.f);
        }
    }
    
    
    
    /*! \brief Get emitted radiance.
     *  \param[in] shadingData Shading data of ray-object intersection.
     *  \return Returns emitted radiance.
     */
    template <class SpectrumType>
    SpectrumType Emissive<SpectrumType>::getLe(const ShadingData<SpectrumType>& shadingData) const
    {
         return color_ * ls_;
    }

} // end of namespace iiit

#endif // __EMISSIVE_HPP__