// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Transparent.hpp
 *  \author Thomas Nuernberg
 *  \date 06.05.2017
 *  \brief Definition of transparent material class.
 */

#ifndef __TRANSPARENT_HPP__
#define __TRANSPARENT_HPP__

#include <memory>

#include "Materials/Phong.hpp"
#include "BRDF/PerfectSpecular.hpp"
#include "BTDF/PerfectTransmitter.hpp"
#include "Utilities/Ray.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Vector3D.hpp"



namespace iiit
{

    /*! \brief Transparent material.
     */
    template <class SpectrumType>
    class Transparent : public Phong<SpectrumType>
    {
    public:
        Transparent(void);
        ~Transparent(void);
        
        void setReflectiveFactor(float k);
        void setReflectiveColor(const SpectrumType& color);
        void setTransmittanceFactor(float k);
        void setIndexOfRefraction(float eta);
        
        virtual SpectrumType shade(ShadingData<SpectrumType>& shadingDataObject) const;
        virtual SpectrumType areaLightShade(ShadingData<SpectrumType>& shadingDataObject) const;
        virtual SpectrumType pathShade(ShadingData<SpectrumType>& shadingDataObject) const;

    private:
        std::shared_ptr<PerfectSpecular<SpectrumType> > reflectiveBrdf_; ///< Brdf for reflective shading.
        std::shared_ptr<PerfectTransmitter<SpectrumType> > transparentBtdf_; ///< Btdf for transparent shading.
    };
    
    
    
    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Transparent<SpectrumType>::Transparent(void)
        : Phong<SpectrumType>()
    {
        reflectiveBrdf_ = std::shared_ptr<PerfectSpecular<SpectrumType> >(new PerfectSpecular<SpectrumType>(1.0, SpectrumType(1.f)));
        transparentBtdf_ = std::shared_ptr<PerfectTransmitter<SpectrumType> >(new PerfectTransmitter<SpectrumType>(1.f, 1.f));
    }
    
    
    
    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    Transparent<SpectrumType>::~Transparent(void)
    {
    }
    
    
    
    /*! \brief Set reflection factor for perfect specular brdf.
     *  \param[in] k Reflection factor.
     */
    template <class SpectrumType>
    void Transparent<SpectrumType>::setReflectiveFactor(float k)
    {
        reflectiveBrdf_->setK(k);
    }
    
    
    
    /*! \brief Set color of the perfect specular reflection.
     *  \param[in] color Spectrum object.
     */
    template <class SpectrumType>
    void Transparent<SpectrumType>::setReflectiveColor(const SpectrumType& color)
    {
        reflectiveBrdf_->setCr(color);
    }
    
    
    
    /*! \brief Set transmittance factor for perfect transmitter btdf.
     *  \param[in] k Transmittance factor.
     */
    template <class SpectrumType>
    void Transparent<SpectrumType>::setTransmittanceFactor(float k)
    {
        transparentBtdf_->setKt(k);
    }
    
    
    
    /*! \brief Set index of refraction for perfect transmitter btdf.
     *  \param[in] eta Index of refracrion.
     */
    template <class SpectrumType>
    void Transparent<SpectrumType>::setIndexOfRefraction(float eta)
    {
        transparentBtdf_->setEta(eta);
    }
    
    
    
    /*! \brief Calculate color of the material.
     *  \param[in] shadingData Shading data of ray-object intersection.
     *  \return Returns resulting color.
     */
    template <class SpectrumType>
    SpectrumType Transparent<SpectrumType>::shade(ShadingData<SpectrumType>& shadingData) const
    {
        SpectrumType L = Phong<SpectrumType>::shade(shadingData); // direct illumination
        
        Vector3D outgoing(-(shadingData.ray_.direction_));
        Vector3D incoming;
        SpectrumType fr = reflectiveBrdf_->fSample(shadingData, incoming, outgoing);
        Ray reflectedRay(shadingData.hitPoint_, incoming);
        
        if (transparentBtdf_->checkTotalInternalReflection(shadingData))
        {
            L += shadingData.scene_.lock()->getTracer().lock()->traceRay(reflectedRay, shadingData.depth_ + 1);
            // kr = 1
        }
        else
        {
            Vector3D transmitted;
            SpectrumType ft = transparentBtdf_->fSample(shadingData, transmitted, outgoing);
            Ray transmittedRay(shadingData.hitPoint_, transmitted);
            
            L += fr * shadingData.scene_.lock()->getTracer().lock()->traceRay(reflectedRay, shadingData.depth_ + 1)
                * fabs(static_cast<float>(Vector3D(shadingData.normal_) * incoming));
            
            L += ft * shadingData.scene_.lock()->getTracer().lock()->traceRay(transmittedRay, shadingData.depth_ + 1)
                * fabs(static_cast<float>(Vector3D(shadingData.normal_) * transmitted));
        }

        return L;
    }



    /*! \brief Calculate color of the material for direct lighting.
     *  \param[in] shadingData Shading data of ray-object intersection.
     *  \return Returns resulting color.
     */
    template <class SpectrumType>
    SpectrumType Transparent<SpectrumType>::areaLightShade(ShadingData<SpectrumType>& shadingData) const
    {
        return shade(shadingData);
    }
    
    
    
    /*! \brief Virtual method to perform shading at ray-object intersection for global illumination.
     *  \param[in] shadingDataObject Shading data of the ray-object intersection.
     *  \return Returns shaded color.
     */
    template <class SpectrumType>
    SpectrumType Transparent<SpectrumType>::pathShade(ShadingData<SpectrumType>& shadingData) const
    {
        return shade(shadingData);
    }

} // end of namespace iiit

#endif // __TRANSPARENT_HPP__