// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Matte.hpp
 *  \author Christian Zimmermann
 *  \date 18.05.2015
 *  \brief Definition of matte material class (pure Lambertian).
 *
 *  Definition of matte material class representing the pure Lambertian reflection for ambient light.
 */

#ifndef __MATTE_HPP__
#define __MATTE_HPP__

#include <memory>

#include "Materials/Material.hpp"
#include "BRDF/Lambertian.hpp"
#include "Textures/Texture.hpp"
#include "Textures/ConstantColor.hpp"
#include "Lights/Light.hpp"
#include "Utilities/ShadingData.hpp"



namespace iiit
{

    /*! \brief Material with only diffuse reflection.
     */
    template <class SpectrumType>
    class Matte : public Material<SpectrumType>
    {
    public:
        Matte(void);
        Matte(SpectrumType color);
        ~Matte(void);

        void setAmbientReflectionFactor(const float k);
        void setDiffuseReflectionFactor(const float k);
        void setDiffuseColor(const SpectrumType& color);
        void setDiffuseTexture(std::shared_ptr<Texture<SpectrumType> > texture);

        virtual SpectrumType shade(ShadingData<SpectrumType>& shadingDataObject) const;
        virtual SpectrumType areaLightShade(ShadingData<SpectrumType>& shadingDataObject) const;
        virtual SpectrumType pathShade(ShadingData<SpectrumType>& shadingDataObject) const;
        
        void setSampler(std::shared_ptr<Sampler> sampler);

    private:
        std::shared_ptr<Lambertian<SpectrumType> > ambientBrdf_; ///< BRDF for ambient illumination.
        std::shared_ptr<Lambertian<SpectrumType> > diffuseBrdf_; ///< BRDF for diffuse reflection.
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Matte<SpectrumType>::Matte(void)
    {
        ambientBrdf_ = std::shared_ptr<Lambertian<SpectrumType> >(new Lambertian<SpectrumType>(1.0, std::shared_ptr<Texture<SpectrumType> >(new ConstantColor<SpectrumType>)));
        diffuseBrdf_ = std::shared_ptr<Lambertian<SpectrumType> >(new Lambertian<SpectrumType>(1.0, std::shared_ptr<Texture<SpectrumType> >(new ConstantColor<SpectrumType>)));
    }



    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    Matte<SpectrumType>::~Matte(void)
    {
    }



    /*! \brief Constructor with arguments.
     */
    template <class SpectrumType>
    Matte<SpectrumType>::Matte(SpectrumType color)
        : ambientBrdf_(new Lambertian<SpectrumType>(1.0, std::shared_ptr<Texture<SpectrumType> >(new ConstantColor<SpectrumType>(color))))
        , diffuseBrdf_(new Lambertian<SpectrumType>(1.0, std::shared_ptr<Texture<SpectrumType> >(new ConstantColor<SpectrumType>(color))))
    {
    }



    /*! \brief Set reflection factor for ambient light.
     *  \param[in] k Reflection factor.
     */
    template <class SpectrumType>
    void Matte<SpectrumType>::setAmbientReflectionFactor(const float k)
    {
        ambientBrdf_->setKd(k);
    }



    /*! \brief Set reflection factor for diffuse light.
     *  \param[in] k Reflection factor.
     */
    template <class SpectrumType>
    void Matte<SpectrumType>::setDiffuseReflectionFactor(const float k)
    {
        diffuseBrdf_->setKd(k);
    }



    /*! \brief Set color of the lights.
     *  \param[in] color Spectrum object.
     */
    template <class SpectrumType>
    void Matte<SpectrumType>::setDiffuseColor(const SpectrumType& color)
    {
        diffuseBrdf_->setCd(color);
        ambientBrdf_->setCd(color);
    }



    /*! \brief Set the diffuse texture.
     *  \param[in] texture Diffuse texture.
     */
    template <class SpectrumType>
    void Matte<SpectrumType>::setDiffuseTexture(std::shared_ptr<Texture<SpectrumType> > texture)
    {
        diffuseBrdf_->setTexture(texture);
        ambientBrdf_->setTexture(texture);
    }



    /*! \brief Calculate color of the material.
     *  \param[in] shadingData Shading data of ray-object intersection.
     *  \return Returns resulting color.
     */
    template <class SpectrumType>
    SpectrumType Matte<SpectrumType>::shade(ShadingData<SpectrumType>& shadingData) const
    {
        Vector3D outgoingRayDirection = -shadingData.ray_.direction_;
        std::shared_ptr<const Scene<SpectrumType> > scene(shadingData.scene_.lock());
        
        // Share of the illumination from ambient light
        SpectrumType L;
        if (!scene->getAmbientLight().expired())
        {
            L = ambientBrdf_->rho(shadingData, outgoingRayDirection) * scene->getAmbientLight().lock()->L(shadingData);
        }
        
        // Share of illumination by diffuse shading
        for (unsigned int i = 0; i < scene->lights_.size(); ++i)
        {
            Vector3D incomingLightDirection = scene->lights_[i]->getDirection(shadingData);
            float dotProduct = static_cast<float>(shadingData.normal_ * incomingLightDirection);
            
            if (dotProduct > 0.0)
            {
                bool shadowed = false;
                if (scene->lights_[i]->castsShadows())
                {
                    Ray shadowRay(shadingData.hitPoint_, incomingLightDirection);
                    shadowed = scene->lights_[i]->shadowed(shadowRay, shadingData);
                }
                
                if (!shadowed)
                {
                    L += diffuseBrdf_->f(shadingData, incomingLightDirection, outgoingRayDirection) * scene->lights_[i]->L(shadingData) * dotProduct;
                }
            }
        }
        return L;
    }



    /*! \brief Calculate color of the material for area lights.
     *  \param[in] shadingData Shading data of ray-object intersection.
     *  \return Returns resulting color.
     */
    template <class SpectrumType>
    SpectrumType Matte<SpectrumType>::areaLightShade(ShadingData<SpectrumType>& shadingData) const
    {
        Vector3D outgoingRayDirection = -shadingData.ray_.direction_;
        std::shared_ptr<const Scene<SpectrumType> > scene(shadingData.scene_.lock());
        
        // Share of the illumination from ambient light
        SpectrumType L;
        if (!scene->getAmbientLight().expired())
        {
            L = ambientBrdf_->rho(shadingData, outgoingRayDirection) * scene->getAmbientLight().lock()->L(shadingData);
        }
        
        // Share of illumination by diffuse shading
        for (unsigned int i = 0; i < scene->lights_.size(); ++i)
        {
            Vector3D incomingLightDirection = scene->lights_[i]->getDirection(shadingData);
            float dotProduct = static_cast<float>(shadingData.normal_ * incomingLightDirection);
            
            if (dotProduct > 0.0)
            {
                bool shadowed = false;
                if (scene->lights_[i]->castsShadows())
                {
                    Ray shadowRay(shadingData.hitPoint_, incomingLightDirection);
                    shadowed = scene->lights_[i]->shadowed(shadowRay, shadingData);
                }
                
                if (!shadowed)
                {
                    L += diffuseBrdf_->f(shadingData, incomingLightDirection, outgoingRayDirection)
                        * scene->lights_[i]->L(shadingData)
                        * scene->lights_[i]->g(shadingData)
                        * dotProduct
                        / scene->lights_[i]->pdf(shadingData);
                }
            }
        }
        return L;
    }
    
    
    
    /*! \brief Virtual method to perform shading at ray-object intersection for global illumination.
     *  \param[in] shadingDataObject Shading data of the ray-object intersection.
     *  \return Returns shaded color.
     */
    template <class SpectrumType>
    SpectrumType Matte<SpectrumType>::pathShade(ShadingData<SpectrumType>& shadingDataObject) const
    {
        Vector3D incoming;
        Vector3D outgoing = -shadingDataObject.ray_.direction_;
        float pdf;
        
        SpectrumType f = diffuseBrdf_->fSample(shadingDataObject, incoming, outgoing, pdf);
        float dotProduct = static_cast<float>(shadingDataObject.normal_ * incoming);
        Ray reflectedRay(shadingDataObject.hitPoint_, incoming);
        
        return (f * shadingDataObject.scene_.lock()->getTracer().lock()->traceRay(reflectedRay, shadingDataObject.depth_ + 1) * dotProduct / pdf);
    }
    
    
    
    /*! \brief Set sampling object.
     *  \param[in] sampler Sampling object.
     */
    template <class SpectrumType>
    void Matte<SpectrumType>::setSampler(std::shared_ptr<Sampler> sampler)
    {
        diffuseBrdf_->setSampler(sampler);
    }

} // end of namespace iiit

#endif // __MATTE_HPP__