// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file RayTracer.cpp
 *  \author Thomas Nuernberg
 *  \date 15.12.2014
 *  \brief Implementation of class RayTracer.
 *
 *  Implementation of class RayTracer, the main class of the project.
 */

#include "RayTracer.hpp"

#include <iostream>
#include <random>

#include "Cameras/Camera.hpp"



namespace iiit
{

    /*! \brief Default constructor.
     */ 
    RayTracer::RayTracer()
    {
    }



    /*! \brief Constructor with initialization.
     *  \param[in] scene Scene to render.
     *  \param[in] callback Callback function when finished rendering.
     */ 
    RayTracer::RayTracer(std::shared_ptr<AbstractScene> scene, std::function<void(const ViewPlane&, std::shared_ptr<const cimg_library::CImg<float> >)> callback)
        : callback_(callback)
        , scene_(scene)
    {
    }



    /*! \brief Default destructor.
     */ 
    RayTracer::~RayTracer()
    {

    }



    /*! \brief Set scene to render.
     *  \param[in] scene Scene object.
     */
    void RayTracer::setScene(std::shared_ptr<AbstractScene> scene)
    {
        scene_ = scene;
    }



    /*! \brief Set callback function.
     *  \param[in] callback std::function object of callback function.
     *
     *  Sets callback function that gets called when rendering is finished.
     */
    void RayTracer::setCallback(std::function<void(const ViewPlane&, std::shared_ptr<const cimg_library::CImg<float> >)> callback)
    {
        callback_ = callback;
    }



    /*! Main method to render a scene.
     */
    void RayTracer::run()
    {
        // init image
        std::shared_ptr<cimg_library::CImg<float> > image(new cimg_library::CImg<float>(scene_->viewPlane_.uRes_, scene_->viewPlane_.vRes_, 1, scene_->getNumChannels()));

        // render image
        scene_->renderScene(image);

        // submit image
        callback_(scene_->viewPlane_, std::const_pointer_cast<const cimg_library::CImg<float> > (image));
    }
    
} // end of namespace iiit
