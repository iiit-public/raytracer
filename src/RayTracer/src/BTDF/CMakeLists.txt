# Project
project(raytracer)

# Add source files for source_group
set (SOURCE_BTDF
    ${CMAKE_CURRENT_SOURCE_DIR}/Btdf.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/FresnelTransmitter.hpp
    ${CMAKE_CURRENT_SOURCE_DIR}/PerfectTransmitter.hpp
    PARENT_SCOPE
)