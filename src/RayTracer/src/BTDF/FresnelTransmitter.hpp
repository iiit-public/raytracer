// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file FresnelTransmitter.hpp
 *  \author Thomas Nuernberg
 *  \date 07.05.2017
 *  \brief Definition of class representing a Fresnel transmitting BTDF.
 */

#ifndef __FRESNELTRANSMITTER_HPP__
#define __FRESNELTRANSMITTER_HPP__

#include "Btdf.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/Normal.hpp"



namespace iiit
{

    /*! \brief Class representing a Fresnel transmitting BTDF.
     */
    template <class SpectrumType>
    class FresnelTransmitter : public Btdf<SpectrumType>
    {
    public:
        FresnelTransmitter();
        FresnelTransmitter(float etaIn, float etaOut);
        ~FresnelTransmitter();
        
        void setEtaIn(float etaIn);
        float getEtaIn() const;
        void setEtaOut(float etaOut);
        float getEtaOut() const;
        
        virtual SpectrumType f(const ShadingData<SpectrumType>& shadingData, const Vector3D& incoming, const Vector3D& outgoing) const;
        virtual SpectrumType fSample(const ShadingData<SpectrumType>& shadingData, Vector3D& incoming, const Vector3D& outgoing) const;
        virtual SpectrumType rho(const ShadingData<SpectrumType>& shadingData, const Vector3D& outgoing) const;
        virtual bool checkTotalInternalReflection(const ShadingData<SpectrumType>& shadingData) const;
        
    private:
        float fresnel(const ShadingData<SpectrumType>& shadingData) const;
        
        float etaIn_; ///< Index of refraction on the inside of the object.
        float etaOut_; ///< Index of refraction on the outside of the object.
    };
    
    
    
    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    FresnelTransmitter<SpectrumType>::FresnelTransmitter()
        : etaIn_(1.f)
        , etaOut_(1.f)
    {
    }
    
    
    
    /*! \brief Constructor with arguments.
     *  \param[in] k Specular reflection coefficient.
     *  \param[in] cr Specular color.
     */
    template <class SpectrumType>
    FresnelTransmitter<SpectrumType>::FresnelTransmitter(float etaIn, float etaOut)
        : etaIn_(etaIn)
        , etaOut_(etaOut)
    {
    }
    
    
    
    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    FresnelTransmitter<SpectrumType>::~FresnelTransmitter()
    {
    }
    
    
    
    /*! \brief Set index of refraction on the inside of the object.
     *  \param[in] etaIn Index of refraction on the inside of the object.
     */
    template <class SpectrumType>
    void FresnelTransmitter<SpectrumType>::setEtaIn(float etaIn)
    {
        etaIn_ = etaIn;
    }
    
    
    
    /*! \brief Get index of refraction on the inside of the object.
     *  \return Returns index of refraction on the inside of the object.
     */
    template <class SpectrumType>
    float FresnelTransmitter<SpectrumType>::getEtaIn() const
    {
        return etaIn_;
    }
    
    
    
    /*! \brief Set index of refraction on the outside of the object.
     *  \param[in] etaOut Index of refraction on the outside of the object.
     */
    template <class SpectrumType>
    void FresnelTransmitter<SpectrumType>::setEtaOut(float etaOut)
    {
        etaOut_ = etaOut;
    }
    
    
    
    /*! \brief Get index of refraction on the outside of the object.
     *  \return Returns index of refraction on the outside of the object.
     */
    template <class SpectrumType>
    float FresnelTransmitter<SpectrumType>::getEtaOut() const
    {
        return etaOut_;
    }
    
    
    
    /*! \brief Bidirectional transmittance distribution function, which is the constant of
     *  proportionality between transmitted radiance and irradiance.
     *  \param[in] shadingData Shading data of ray object intersection.
     *  \param[in] incoming Incoming ray direction.
     *  \param[in] outgoing Outgoing ray direction.
     *  \return Return Bidirectional transmittance distribution function applied to irradiance.
     */
    template <class SpectrumType>
    SpectrumType FresnelTransmitter<SpectrumType>::f(const ShadingData<SpectrumType>& shadingData, const Vector3D& incoming, const Vector3D& outgoing) const
    {
        return SpectrumType(0.f);
    }
    
    
    
    /*! \brief Bidirectional reflectance distribution function, which is the constant of
     *  proportionality between transmitted radiance and irradiance.
     *  \param[in] shadingData Shading data of ray object intersection.
     *  \param[in] transmitted Transmitted ray direction.
     *  \param[in] outgoing Outgoing ray direction.
     *  \return Return Bidirectional transmittance distribution function applied to irradiance.
     *
     *  The btdf is calculated by sampling the directions of incoming rays.
     */
    template <class SpectrumType>
    SpectrumType FresnelTransmitter<SpectrumType>::fSample(const ShadingData<SpectrumType>& shadingData, Vector3D& transmitted, const Vector3D& outgoing) const
    {
        Normal n(shadingData.normal_);
        float cosThetaI = static_cast<float>(n * outgoing);
        float eta = etaIn_ / etaOut_;
        if ( cosThetaI > 0.f && shadingData.normalFlipped_)
        {
            eta = 1.f / eta;
        }
        else if (cosThetaI < 0.f && !shadingData.normalFlipped_)
        {
            eta = 1.f / eta;
            n = -n;
            cosThetaI = -cosThetaI;
        }
        
        float root = 1.f - (1.f - cosThetaI * cosThetaI) / (eta * eta);
        float sqrCosTheta = sqrt(root);
        transmitted = -outgoing / eta - (sqrCosTheta - cosThetaI / eta) * n;
        
        return (SpectrumType(1.f) * fresnel(shadingData) / (eta * eta) / fabs(static_cast<float>(shadingData.normal_ * transmitted)));
    }
    
    
    
    /*! \brief Bihemispherical transmittance.
     *  \param[in] shadingData Shading data of ray object intersection.
     *  \param[in] outgoing Outgoing ray direction.
     *  \return Returns resulting bihemispherical transmittance as color.
     */
    template <class SpectrumType>
    SpectrumType FresnelTransmitter<SpectrumType>::rho(const ShadingData<SpectrumType>& shadingData, const Vector3D& outgoing) const
    {
        return SpectrumType(0.f);
    }
    
    
    
    /*! \brief Calculate Fresnel transmission coefficient.
     *  \param[in] shadingData Shading object.
     *  \return Returns Fresnel transmission coefficient.
     */
    template <class SpectrumType>
    float FresnelTransmitter<SpectrumType>::fresnel(const ShadingData<SpectrumType>& shadingData) const
    {
        Normal normal(shadingData.normal_);
        float dotProduct = static_cast<float>(-normal * shadingData.ray_.direction_);
        float eta;
        
        if (dotProduct > 0.f && shadingData.normalFlipped_)
        {
            // ray hits inside surface
            eta = etaOut_ / etaIn_;
        }
        else if (dotProduct < 0.f && !shadingData.normalFlipped_)
        {
            // ray hits inside surface
            eta = etaOut_ / etaIn_;
            normal = -normal;
        }
        else
        {
            // ray hits outside surface
            eta = etaIn_ / etaOut_;
        }
        
        float cosThetaI = static_cast<float>(-normal * shadingData.ray_.direction_);
        float root = 1.f - (1.f - cosThetaI * cosThetaI) / (eta * eta);
        float cosThetaT = sqrt(root);
        float rParallel = (eta * cosThetaI - cosThetaT) / (eta * cosThetaI + cosThetaT);
        float rPerpendicular = (cosThetaI - eta * cosThetaT) / (cosThetaI + eta * cosThetaT);
        return (1.f - 0.5f * (rParallel * rParallel + rPerpendicular * rPerpendicular));
    }
    
    
    
    /*! \brief Check for total internal reflection.
     *  \param[in] shadingData Shading data of ray object intersection.
     *  \return Return TRUE when total internal reflection occurs, else FALSE.
     */
    template <class SpectrumType>
    bool FresnelTransmitter<SpectrumType>::checkTotalInternalReflection(const ShadingData<SpectrumType>& shadingData) const
    {
        Vector3D outgoing(-shadingData.ray_.direction_);
        float cosThetaI = static_cast<float>(shadingData.normal_ * outgoing);
        float eta = etaIn_ / etaOut_;
        if ((cosThetaI < 0.f && !shadingData.normalFlipped_) || (cosThetaI > 0.f && shadingData.normalFlipped_))
        {
            eta = 1.f / eta;
        }
        
        return (1.f - (1.f - cosThetaI * cosThetaI) / (eta * eta) < 0.f);
    }
} // end of namespace iiit

#endif // __FRESNELTRANSMITTER_HPP__