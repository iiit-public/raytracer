// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file PerfectTransmitter.hpp
 *  \author Thomas Nuernberg
 *  \date 06.05.2017
 *  \brief Definition of class representing a perfectly transmitting BTDF.
 */

#ifndef __PERFECTTRANSMITTER_HPP__
#define __PERFECTTRANSMITTER_HPP__

#include "Btdf.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/Normal.hpp"



namespace iiit
{

    /*! \brief Class representing a perfectly transmitting BTDF.
     */
    template <class SpectrumType>
    class PerfectTransmitter : public Btdf<SpectrumType>
    {
    public:
        PerfectTransmitter();
        PerfectTransmitter(float kt, float eta);
        ~PerfectTransmitter();
        
        float getKt() const;
        void setKt(float kt);
        float getEta() const;
        void setEta(float eta);
        
        virtual SpectrumType f(const ShadingData<SpectrumType>& shadingData, const Vector3D& incoming, const Vector3D& outgoing) const;
        virtual SpectrumType fSample(const ShadingData<SpectrumType>& shadingData, Vector3D& incoming, const Vector3D& outgoing) const;
        virtual SpectrumType rho(const ShadingData<SpectrumType>& shadingData, const Vector3D& outgoing) const;
        virtual bool checkTotalInternalReflection(const ShadingData<SpectrumType>& shadingData) const;
        
    private:
        float kt_; ///< Transmittance coefficient.
        float eta_; ///< Index of refraction.
    };
    
    
    
    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    PerfectTransmitter<SpectrumType>::PerfectTransmitter()
        : kt_(1.f)
        , eta_(1.f)
    {
    }
    
    
    
    /*! \brief Constructor with initialization.
     *  \param[in] kt Transmittance coefficient.
     *  \param[in] eta Index of refraction.
     */
    template <class SpectrumType>
    PerfectTransmitter<SpectrumType>::PerfectTransmitter(float kt, float eta)
        : kt_(kt)
        , eta_(eta)
    {
    }
    
    
    
    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    PerfectTransmitter<SpectrumType>::~PerfectTransmitter()
    {
    }
    
    
    
    /*! \brief Get transmittance coefficient.
     *  \return Returns transmittance coefficient.
     */
    template <class SpectrumType>
    inline float PerfectTransmitter<SpectrumType>::getKt() const
    {
        return kt_;
    }
    
    
    
    /*! \brief Set transmittance coefficient.
     *  \param[in] kt Transmittance coefficient.
     */
    template <class SpectrumType>
    inline void PerfectTransmitter<SpectrumType>::setKt(float kt)
    {
        kt_ = kt;
    }
    
    
    
    /*! \brief Get index of refraction.
     *  \return Returns index of refraction.
     */
    template <class SpectrumType>
    inline float PerfectTransmitter<SpectrumType>::getEta() const
    {
        return eta_;
    }
    
    
    
    /*! \brief Set index of refraction.
     *  \param[in] eta Index of refraction.
     */
    template <class SpectrumType>
    inline void PerfectTransmitter<SpectrumType>::setEta(float eta)
    {
        eta_ = eta;
    }
    
    
    
    /*! \brief Bidirectional transmittance distribution function, which is the constant of
     *  proportionality between transmitted radiance and irradiance.
     *  \param[in] shadingData Shading data of ray object intersection.
     *  \param[in] incoming Incoming ray direction.
     *  \param[in] outgoing Outgoing ray direction.
     *  \return Return Bidirectional transmittance distribution function applied to irradiance.
     */
    template <class SpectrumType>
    SpectrumType PerfectTransmitter<SpectrumType>::f(const ShadingData<SpectrumType>& shadingData, const Vector3D& incoming, const Vector3D& outgoing) const
    {
        return SpectrumType(0.f);
    }
    
    
    
    /*! \brief Bidirectional reflectance distribution function, which is the constant of
     *  proportionality between transmitted radiance and irradiance.
     *  \param[in] shadingData Shading data of ray object intersection.
     *  \param[in] transmitted Transmitted ray direction.
     *  \param[in] outgoing Outgoing ray direction.
     *  \return Return Bidirectional transmittance distribution function applied to irradiance.
     *
     *  The brdf is calculated by sampling the directions of incoming rays.
     */
    template <class SpectrumType>
    SpectrumType PerfectTransmitter<SpectrumType>::fSample(const ShadingData<SpectrumType>& shadingData, Vector3D& transmitted, const Vector3D& outgoing) const
    {
        Normal n(shadingData.normal_);
        float cosThetaI = static_cast<float>(n * outgoing);
        float eta = eta_;
        if (cosThetaI < 0.f)
        {
            cosThetaI = -cosThetaI;
            n = -n;
            eta = 1.f / eta;
        }
        
        float root = 1.f - (1.f - cosThetaI * cosThetaI) / (eta * eta);
        float sqrCosTheta = sqrt(root);
        transmitted = -outgoing / eta - (sqrCosTheta - cosThetaI / eta) * n;
        return (SpectrumType(1.f) * kt_ / (eta * eta) / fabs(static_cast<float>(shadingData.normal_ * transmitted)));
    }
    
    
    
    /*! \brief Bihemispherical transmittance.
     *  \param[in] shadingData Shading data of ray object intersection.
     *  \param[in] outgoing Outgoing ray direction.
     *  \return Returns resulting bihemispherical transmittance as color.
     */
    template <class SpectrumType>
    SpectrumType PerfectTransmitter<SpectrumType>::rho(const ShadingData<SpectrumType>& shadingData, const Vector3D& outgoing) const
    {
        return SpectrumType(0.f);
    }
    
    
    
    /*! \brief Check for total internal reflection.
     *  \param[in] shadingData Shading data of ray object intersection.
     *  \return Return TRUE when total internal reflection occurs, else FALSE.
     */
    template <class SpectrumType>
    bool PerfectTransmitter<SpectrumType>::checkTotalInternalReflection(const ShadingData<SpectrumType>& shadingData) const
    {
        Vector3D outgoing(-shadingData.ray_.direction_);
        float cosThetaI = static_cast<float>(shadingData.normal_ * outgoing);
        float eta = eta_;
        if ((cosThetaI < 0.f && !shadingData.normalFlipped_) || (cosThetaI > 0.f && shadingData.normalFlipped_))
        {
            eta = 1.f / eta;
        }
        
        return (1.f - (1.f - cosThetaI * cosThetaI) / (eta * eta) < 0.f);
    }
    
} // end of namespace iiit

#endif // __PERFECTTRANSMITTER_HPP__