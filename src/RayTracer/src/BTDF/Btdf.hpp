// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Btdf.hpp
 *  \author Thomas Nuernberg
 *  \date 06.05.2017
 *  \brief Definition of base class Btdf, representing bidirectional transmittance distribution
 *  functions (BTDF)).
 */

#ifndef __BTDF_HPP__
#define __BTDF_HPP__

#include <memory>

#include "Sampler/Sampler.hpp"
#include "Utilities/Normal.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Vector3D.hpp"



namespace iiit
{

    /*! \brief Abstract base class for bidirectional reflectance distribution functions (BRDF).
     */
    template <class SpectrumType>
    class Btdf
    {
    public:
        /*! \brief Bidirectional transmittance distribution function, which is the constant of
         *  proportionality between transmitted radiance and irradiance.
         *  \param[in] shadingData Shading data of ray object intersection.
         *  \param[in] incoming Incoming ray direction.
         *  \param[in] outgoing Outgoing ray direction.
         *  \return Return Bidirectional transmittance distribution function applied to irradiance.
         */
        virtual SpectrumType f(const ShadingData<SpectrumType>& shadingData, const Vector3D& incoming, const Vector3D& outgoing) const = 0;

        /*! \brief Bidirectional reflectance distribution function, which is the constant of
         *  proportionality between transmitted radiance and irradiance.
         *  \param[in] shadingData Shading data of ray object intersection.
         *  \param[in] transmitted Transmitted ray direction.
         *  \param[in] outgoing Outgoing ray direction.
         *  \return Return Bidirectional transmittance distribution function applied to irradiance.
         *
         *  The brdf is calculated by sampling the directions of incoming rays.
         */
        virtual SpectrumType fSample(const ShadingData<SpectrumType>& shadingData, Vector3D& transmitted, const Vector3D& outgoing) const = 0;

        /*! \brief Bihemispherical transmittance.
         *  \param[in] shadingData Shading data of ray object intersection.
         *  \param[in] outgoing Outgoing ray direction.
         *  \return Returns resulting bihemispherical transmittance as color.
         */
        virtual SpectrumType rho(const ShadingData<SpectrumType>& shadingData, const Vector3D& outgoing) const = 0;
        
        /*! \brief Check for total internal reflection.
         *  \param[in] shadingData Shading data of ray object intersection.
         *  \return Return TRUE when total internal reflection occurs, else FALSE.
         */
        virtual bool checkTotalInternalReflection(const ShadingData<SpectrumType>& shadingData) const = 0;
    };
        
} // end of namespace iiit

#endif // __BTDF_HPP__