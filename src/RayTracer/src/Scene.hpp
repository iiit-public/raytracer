// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Scene.hpp
 *  \author Thomas Nuernberg
 *  \date 16.12.2014
 *  \brief Definition of class Scene.
 *
 *  Definition of class Scene, containing the whole scene geometry and camera configuration.
 */

#ifndef __SCENE_HPP__
#define __SCENE_HPP__

#include <vector>
#include <memory>

#include "AbstractScene.hpp"
#include "Objects/GeometricObject.hpp"
#include "Lights/Light.hpp"
#include "Lights/AreaLight.hpp"
#include "Cameras/ViewPlane.hpp"
#include "Cameras/Camera.hpp"
#include "Tracers/Tracer.hpp"



namespace iiit
{
    
    /*! \brief Class for storing scene data.
     * 
     *  This class stores all relevant scene data. This includes all geometric objects of the scene as
     *  well as the properties of the simulated camera.
     *
     *  \attention tracer_ member variable has to be initialized after the construction of an instance
     *  of class Scene. This is necessary because tracer_ holds a weak pointer the owning Scene object,
     *  which is not fully constructed during the constructor call. Use the default constructor followed
     *  by an assignment instead.
     */
    template <class SpectrumType>
    class Scene : public AbstractScene, public std::enable_shared_from_this<Scene<SpectrumType> >
    {
    public:
        Scene();
        ~Scene();

        Scene<SpectrumType>& operator=(const Scene<SpectrumType>& other);
        
        std::shared_ptr<AbstractScene> clone() const;

        // scene objects
        void addObject(std::shared_ptr<GeometricObject<SpectrumType> > object);

        // scene lights
        void addLight(std::shared_ptr<Light<SpectrumType> > light);
        void setAmbientLight(std::shared_ptr<Light<SpectrumType> > ambientLight);
        std::weak_ptr<Light<SpectrumType> > getAmbientLight() const;

        // scene camera
        void setCamera(std::shared_ptr<Camera<SpectrumType> > camera);
        void renderScene(std::shared_ptr<cimg_library::CImg<float> > image) const;
        std::shared_ptr<Camera<SpectrumType> > getCamera() const;
        
        // scene tracer
        void setTracer(std::shared_ptr<Tracer<SpectrumType> > tracer);
        std::weak_ptr<const Tracer<SpectrumType> > getTracer() const;

        SpectrumType backgroundColor_; ///< Color to be assigned to rays that don't hit an object.
        
        std::vector<std::shared_ptr<GeometricObject<SpectrumType> > > objects_; ///< Vector of geometric objects of the scene. The class takes ownership of the objects and deletes them on destruction.
        std::vector< std::shared_ptr<Light<SpectrumType> > > lights_; ///< Vector of Lights used in the scene.

        int getNumChannels() const;
        ImageType::Type getImageType() const;
        std::string getSensorName() const;

    private:
        Scene(const Scene<SpectrumType>& other); // declare copy constructor private to disable it

        std::shared_ptr<Light<SpectrumType> > ambientLight_; ///< Ambient light of the scene.
        std::shared_ptr<Camera<SpectrumType> > camera_; ///< Camera that takes the picture.
        std::shared_ptr<Tracer<SpectrumType> > tracer_; ///< Tracer to calculate ray-objects-interactions.
    };



    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Scene<SpectrumType>::Scene()
    {
    }



    /*! \brief Copy constructor.
     *  \param[in] other Scene object to copy.
     *
     *  \attention Declared private. Disabled intentionally. Use default constructor followed by an
     *  assignment.
     */
    template <class SpectrumType>
    Scene<SpectrumType>::Scene(const Scene<SpectrumType>& other)
    {
    }



    /*! \brief Default destructor.
     *
     *  Object members are deleted according to reference count of shared pointers.
     */
    template <class SpectrumType>
    Scene<SpectrumType>::~Scene()
    {
    }



    /*! \brief Assignment operator.
     *  \param[in] other Scene object to be assigned.
     *  \return Reference to scene object.
     *
     *  Also resets the scene_ reference of the Tracer object tracer_.
     */
    template <class SpectrumType>
    Scene<SpectrumType>& Scene<SpectrumType>::operator=(const Scene<SpectrumType>& other)
    {
        // hard copy tracer,  because it owns a scene reference
        tracer_ = other.tracer_;
        if (tracer_)
        {
            tracer_->setScene(this->shared_from_this());
        }
        viewPlane_ = other.viewPlane_;
        objects_ = other.objects_;
        lights_ = other.lights_;
        backgroundColor_ = other.backgroundColor_;
        // hard copy camera,  because it owns a scene reference
        camera_ = std::shared_ptr<Camera<SpectrumType> >(other.camera_->clone());
        if (camera_)
        {
            camera_->setScene(this->shared_from_this());
        }
        ambientLight_ = other.ambientLight_;
        
        return *this;
    }
    
    
    
    /*! \brief Clone scene object.
     *  \return Reference to cloned scene object.
     */
    template <class SpectrumType>
    std::shared_ptr<AbstractScene> Scene<SpectrumType>::clone() const
    {
        // hard copy tracer,  because it owns a scene reference
        std::shared_ptr<Scene<SpectrumType> > scene(new Scene<SpectrumType>());
        if (tracer_)
        {
            scene->tracer_ = std::shared_ptr<Tracer<SpectrumType> >(tracer_->clone());
            scene->tracer_->setScene(scene->shared_from_this());
        }
        scene->viewPlane_ = viewPlane_;
        scene->objects_ = objects_;
        // hard copy lights,  because they might own a sampler object
        for (typename std::vector<std::shared_ptr<Light<SpectrumType> > >::const_iterator it = lights_.begin(); it != lights_.end(); ++it)		
        {
            scene->lights_.push_back(std::shared_ptr<Light<SpectrumType> >((*it)->clone()));
        }
        if (ambientLight_)
        {
            scene->ambientLight_ = std::shared_ptr<Light<SpectrumType> >(ambientLight_->clone());
        }
        scene->backgroundColor_ = backgroundColor_;
        // hard copy camera,  because it owns a scene reference
        scene->camera_ = std::shared_ptr<Camera<SpectrumType> >(camera_->clone());
        if (camera_)
        {
            scene->camera_->setScene(scene->shared_from_this());
        }
        return scene;
    }



    /*! \brief Add geometric object to scene.
     *  \param[in] object Geometric object to add.
     */
    template <class SpectrumType>
    void Scene<SpectrumType>::addObject(std::shared_ptr<GeometricObject<SpectrumType> > object)
    {
        objects_.push_back(object);
    }



    /*! \brief Add light to scene.
     *  \param[in] light Light to add.
     */
    template <class SpectrumType>
    void Scene<SpectrumType>::addLight(std::shared_ptr<Light<SpectrumType> > light)
    {
        lights_.push_back(light);
    }



    /*! \brief Set ambient light of scene.
     *  \param[in] ambientLight Ambient light.
     */
    template <class SpectrumType>
    void Scene<SpectrumType>::setAmbientLight(std::shared_ptr<Light<SpectrumType> > ambientLight)
    {
        ambientLight_ = ambientLight;
    }



    /*! \brief Get ambient light of scene.
     *  \return Ambient light.
     */
    template <class SpectrumType>
    std::weak_ptr<Light<SpectrumType> > Scene<SpectrumType>::getAmbientLight() const
    {
        return ambientLight_;
    }



    /*! \brief Set camera of scene.
     *  \param[in] camera Camera of scene.
     */
    template <class SpectrumType>
    void Scene<SpectrumType>::setCamera(std::shared_ptr<Camera<SpectrumType> > camera)
    {
        camera_ = camera;
    }


    /*! \brief Get camera light of scene.
     *  \return Camera.
     */
    template <class SpectrumType>
    std::shared_ptr<Camera<SpectrumType> > Scene<SpectrumType>::getCamera() const
    {
        return camera_;
    }




    
    /*! \brief Render scene viewed by camera.
     *  \param[in,out] image Rendered image.
     */
    template <class SpectrumType>
    void Scene<SpectrumType>::renderScene(std::shared_ptr<cimg_library::CImg<float> > image) const
    {
        camera_->renderScene(image);
    }



    /*! \brief Get the number of channels of the image that the camera takes.
     *  \returns Returns the number of channels of the sensor image.
     */
    template <class SpectrumType>
    int Scene<SpectrumType>::getNumChannels() const
    {
        return this->camera_->getSensor().getNumChannels();
    }



    /*! \brief Get the name of the simulated sensor
     *  \returns Returns the name of the sensor.
     */
    template <class SpectrumType>
    std::string Scene<SpectrumType>::getSensorName() const
    {
        return this->camera_->getSensor().getSensorName();
    }



    /*! \brief Get the ImageType of the image that the camera takes.
     *  \returns Returns the ImageType of the sensor image. Equivalent to getNumChannels();
     */
    template <class SpectrumType>
    ImageType::Type Scene<SpectrumType>::getImageType() const
    {
        // Number of channels equals image type as it is mapped in class Sensor
        return this->camera_->getSensor().getImageType();
    }



    ///*! \brief Get the SensorType of the image sensor of the camera.
    // *  \returns SensorType of the camera sensor
    // */
    //template <class SpectrumType>
    //SensorType::Type Scene<SpectrumType>::getSensorType() const
    //{
    //    return this->camera_->getSensor().getType();
    //}



    /*! \brief Set tracer of scene.
     *  \param[in] tracer Tracer of scene.
     */
    template <class SpectrumType>
    void Scene<SpectrumType>::setTracer(std::shared_ptr<Tracer<SpectrumType> > tracer)
    {
        tracer_ = tracer;
        tracer_->setScene(this->shared_from_this());
    }
    
    
    
    /*! \brief Get Tracer of scene.
     *  \return Tracer of scene.
     */
    template <class SpectrumType>
    std::weak_ptr<const Tracer<SpectrumType> > Scene<SpectrumType>::getTracer() const
    {
        return tracer_;
    }
    
} // end of namespace iiit

#endif // __SCENE_HPP__
