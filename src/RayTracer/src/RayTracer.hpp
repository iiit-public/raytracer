// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file RayTracer.hpp
 *  \author Thomas Nuernberg
 *  \date 15.12.2014
 *  \brief Definition of class RayTracer.
 *
 *  Definition of class RayTracer, the main class of the project.
 */

#ifndef __RAYTRACER_HPP__
#define __RAYTRACER_HPP__

#include <functional>
#include <memory>

#include "CImg.h"

#include "AbstractScene.hpp"
#include "Cameras/ViewPlane.hpp"
#include "Objects/Plane.hpp"


/*! \namespace iiit
 *  \brief Contains all \a classes, \a functions and \a variables of the RayTracer.
 */
namespace iiit
{
    
    /*! \brief Main ray tracing class.
     */
    class RayTracer
    {
    public:
        RayTracer();
        RayTracer(std::shared_ptr<AbstractScene> scene, std::function<void(const ViewPlane&, std::shared_ptr<const cimg_library::CImg<float> >)> callback);
        ~RayTracer();

        void setScene(std::shared_ptr<AbstractScene> scene);
        void setCallback(std::function<void(const ViewPlane&, std::shared_ptr<const cimg_library::CImg<float> >)> callback);
        void run();

    private:
        std::function<void(const ViewPlane&, std::shared_ptr<const cimg_library::CImg<float> >)> callback_; ///< Function to call when finished rendering.
        std::shared_ptr<AbstractScene> scene_; ///< Scene to render.
    };
    
} // end of namespace iiit

#endif // __RAYTRACER_HPP__