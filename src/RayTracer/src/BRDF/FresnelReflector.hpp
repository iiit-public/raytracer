// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file FresnelReflector.hpp
 *  \author Thomas Nuernberg
 *  \date 07.06.2017
 *  \brief Definition of class FresnelReflector, a BRDF for Fresnel reflection.
 */

#ifndef __FRESNELREFLECTOR_HPP__
#define __FRESNELREFLECTOR_HPP__

#include <math.h>

#include "Brdf.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/Normal.hpp"
#include "Utilities/Constants.h"



namespace iiit
{
    /*! \brief Class of the bidirectional reflectance distribution function (BRDF) of Fresnel
     *  reflection.
     */
    template <class SpectrumType>
    class FresnelReflector : public Brdf<SpectrumType>
    {
    public:
        FresnelReflector();
        FresnelReflector(float etaIn, float etaOut);
        ~FresnelReflector();

        void setEtaIn(float etaIn);
        float getEtaIn() const;
        void setEtaOut(float etaOut);
        float getEtaOut() const;

        SpectrumType f(const ShadingData<SpectrumType>& shadingData, const Vector3D& incoming, const Vector3D& outgoing) const {return SpectrumType(0.f);};
        SpectrumType fSample(const ShadingData<SpectrumType>& shadingData, Vector3D& incoming, const Vector3D& outgoing) const;
        SpectrumType fSample(const ShadingData<SpectrumType>& shadingData, Vector3D& incoming, const Vector3D& outgoing, float& pdf) const;
        SpectrumType rho(const ShadingData<SpectrumType>& shadingData, const Vector3D& outgoing) const {return SpectrumType(0.f);};

    private:
        float fresnel(const ShadingData<SpectrumType>& shadingData) const;
        
        float etaIn_; ///< Index of refraction on the inside of the object.
        float etaOut_; ///< Index of refraction on the outside of the object.
    };

    
    
    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    FresnelReflector<SpectrumType>::FresnelReflector()
        : etaIn_(1.f)
        , etaOut_(1.f)
    {
    }
    
    
    
    /*! \brief Constructor with arguments.
     *  \param[in] k Specular reflection coefficient.
     *  \param[in] cr Specular color.
     */
    template <class SpectrumType>
    FresnelReflector<SpectrumType>::FresnelReflector(float etaIn, float etaOut)
        : etaIn_(etaIn)
        , etaOut_(etaOut)
    {
    }
    
    
    
    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    FresnelReflector<SpectrumType>::~FresnelReflector()
    {
    }
    

    
    /*! \brief Set index of refraction on the inside of the object.
     *  \param[in] etaIn Index of refraction on the inside of the object.
     */
    template <class SpectrumType>
    void FresnelReflector<SpectrumType>::setEtaIn(float etaIn)
    {
        etaIn_ = etaIn;
    }



    /*! \brief Get index of refraction on the inside of the object.
     *  \return Returns index of refraction on the inside of the object.
     */
    template <class SpectrumType>
    float FresnelReflector<SpectrumType>::getEtaIn() const
    {
        return etaIn_;
    }
    
    
    
    /*! \brief Set index of refraction on the outside of the object.
     *  \param[in] etaOut Index of refraction on the outside of the object.
     */
    template <class SpectrumType>
    void FresnelReflector<SpectrumType>::setEtaOut(float etaOut)
    {
        etaOut_ = etaOut;
    }
    
    
    
    /*! \brief Get index of refraction on the outside of the object.
     *  \return Returns index of refraction on the outside of the object.
     */
    template <class SpectrumType>
    float FresnelReflector<SpectrumType>::getEtaOut() const
    {
        return etaOut_;
    }
    
    
    
    //    /*! \brief Set specular texture.
//     *  \param[in] texture Specular texture.
//     */
//    template <class SpectrumType>
//    void FresnelReflector<SpectrumType>::setTexture(std::shared_ptr<Texture<SpectrumType> > texture)
//    {
//        texture_ = texture;
//    }
    
    
    /*! \brief Get sampled BRDF for Fresnel reflection.
     *  \param[in] shadingData Shading object.
     *  \param[in] incoming Direction of incoming ray.
     *  \param[in] outgoing Direction of outgoing ray.
     *  \return Returns reflected color.
     */
    template <class SpectrumType>
    SpectrumType FresnelReflector<SpectrumType>::fSample(const ShadingData<SpectrumType>& shadingData, Vector3D& incoming, const Vector3D& outgoing) const
    {
        Vector3D normal(shadingData.normal_);
        float dotProduct = static_cast<float>(normal * outgoing);
        incoming = -outgoing + normal * 2.0 * dotProduct;
        
        return SpectrumType(1.f) * fresnel(shadingData) / static_cast<float>(normal * incoming);
    }
    
    
    
    /*! \brief Get sampled BRDF for Fresnel reflection.
     *  \param[in] shadingData Shading object.
     *  \param[in] incoming Direction of incoming ray.
     *  \param[in] outgoing Direction of outgoing ray.
     *  \param[in] pdf Probability density of incoming ray.
     *  \return Returns reflected color.
     */
    template <class SpectrumType>
    SpectrumType FresnelReflector<SpectrumType>::fSample(const ShadingData<SpectrumType>& shadingData, Vector3D& incoming, const Vector3D& outgoing, float& pdf) const
    {
        Vector3D normal(shadingData.normal_);
        float dotProduct = static_cast<float>(normal * outgoing);
        incoming = -outgoing + normal * 2.0 * dotProduct;
        pdf = normal * incoming;
        
        return SpectrumType(1.f) * fresnel(shadingData) / static_cast<float>(normal * incoming);
    }
    
    
    
    /*! \brief Calculate Fresnel reflection coefficient.
     *  \param[in] shadingData Shading object.
     *  \return Returns Fresnel reflection coefficient.
     */
    template <class SpectrumType>
    float FresnelReflector<SpectrumType>::fresnel(const ShadingData<SpectrumType>& shadingData) const
    {
        Normal normal(shadingData.normal_);
        float dotProduct = static_cast<float>(-normal * shadingData.ray_.direction_);
        float eta;
        
        if (dotProduct > 0.f && shadingData.normalFlipped_)
        {
            // ray hits inside surface
            eta = etaOut_ / etaIn_;
        }
        else if (dotProduct < 0.f && !shadingData.normalFlipped_)
        {
            // ray hits inside surface
            eta = etaOut_ / etaIn_;
            normal = -normal;
        }
        else
        {
            // ray hits outside surface
            eta = etaIn_ / etaOut_;
        }
        
        float cosThetaI = static_cast<float>(-normal * shadingData.ray_.direction_);
        float root = 1.f - (1.f - cosThetaI * cosThetaI) / (eta * eta);
        float cosThetaT = sqrt(root);
        float rParallel = (eta * cosThetaI - cosThetaT) / (eta * cosThetaI + cosThetaT);
        float rPerpendicular = (cosThetaI - eta * cosThetaT) / (cosThetaI + eta * cosThetaT);
        return (0.5f * (rParallel * rParallel + rPerpendicular * rPerpendicular));
    }
    
} // end of namespace iiit

#endif // __FRESNELREFLECTOR_HPP__