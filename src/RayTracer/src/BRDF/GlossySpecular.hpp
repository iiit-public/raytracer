// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file GlossySpecular.hpp
 *  \author Thomas Nuernberg
 *  \date 12.06.2015
 *  \brief Definition of class GlossySpecular, a BRDF for glossy specular reflection.
 *  functions (BRDF)).
 */

#ifndef __GLOSSYSPECULAR_HPP__
#define __GLOSSYSPECULAR_HPP__

#include <math.h>

#include "Brdf.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/Constants.h"
#include "Textures/Texture.hpp"
#include "Textures/ConstantColor.hpp"
#include "Sampler/Sampler.hpp"



namespace iiit
{
    /*! \brief Class of the bidirectional reflectance distribution function (BRDF) of glossy specular
     *  reflection.
     */
    template <class SpectrumType>
    class GlossySpecular : public Brdf<SpectrumType>
    {
    public:
        GlossySpecular();
        GlossySpecular(float k, float exp, std::shared_ptr<Texture<SpectrumType> > texture);
        ~GlossySpecular();

        void setKs(float ks);
        float getKs() const;
        void setCs(const SpectrumType& cs);
        void setExp(float exp);
        float getExp() const;
        void setTexture(std::shared_ptr<Texture<SpectrumType> > texture);
        void setSampler(std::shared_ptr<Sampler> sampler);

        SpectrumType f(const ShadingData<SpectrumType>& shadingData, const Vector3D& incoming, const Vector3D& outgoing) const;
        SpectrumType fSample(const ShadingData<SpectrumType>& shadingData, Vector3D& incoming, const Vector3D& outgoing) const { return SpectrumType(0.f); }
        SpectrumType fSample(const ShadingData<SpectrumType>& shadingData, Vector3D& incoming, const Vector3D& outgoing, float& pdf) const;
        SpectrumType rho(const ShadingData<SpectrumType>& shadingData, const Vector3D& outgoing) const;

    private:
        float ks_; ///< Specular reflection coefficient.
        float exp_; ///< Specular exponent.
        std::shared_ptr<Texture<SpectrumType> > texture_; ///< Texture assigned.
        std::shared_ptr<Sampler> sampler_; ///< Sampler object used to sample the ray direction for glossy reflection.
    };

    
    
    /*! \brief Default constructor.
     *
     *  Sets both reflection coefficient and exponent to 1 and assigns a constant white texture.
     */
    template <class SpectrumType>
    GlossySpecular<SpectrumType>::GlossySpecular()
        : ks_(1.0)
        , exp_(1.0)
    {
        texture_ = std::shared_ptr<ConstantColor<SpectrumType> >(new ConstantColor<SpectrumType>(SpectrumType(1.f)));
    }
    
    
    
    /*! \brief Constructor with arguments.
     *  \param[in] ks Specular reflection coefficient.
     *  \param[in] exp Specular exponent.
     *  \param[in] texture Texture.
     */
    template <class SpectrumType>
    GlossySpecular<SpectrumType>::GlossySpecular(float ks, float exp, std::shared_ptr<Texture<SpectrumType> > texture)
        : ks_(ks)
        , exp_(exp)
        , texture_(texture)
    {
    }
    
    
    
    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    GlossySpecular<SpectrumType>::~GlossySpecular()
    {
    }
    

    
    /*! \brief Set specular reflection coefficient.
     *  \param[in] ks Specular reflection coefficient.
     */
    template <class SpectrumType>
    void GlossySpecular<SpectrumType>::setKs(float ks)
    {
        ks_ = ks;
    }



    /*! \brief Get specular reflection coefficient.
     *  \return Specular reflection coefficient.
     */
    template <class SpectrumType>
    float GlossySpecular<SpectrumType>::getKs() const
    {
        return ks_;
    }
    
    
    
    /*! \brief Set specular color.
     *  \param[in] cs Specular color.
     */
    template <class SpectrumType>
    void GlossySpecular<SpectrumType>::setCs(const SpectrumType& cs)
    {
        texture_ = std::shared_ptr<Texture<SpectrumType> >(new ConstantColor<SpectrumType>(cs));
    }



    /*! \brief Set specular reflection exponent.
     *  \param[in] exp Specular reflection exponent.
     */
    template <class SpectrumType>
    void GlossySpecular<SpectrumType>::setExp(float exp)
    {
        exp_ = exp;
    }



    /*! \brief Get specular reflection exponent.
     *  \return Specular reflection exponent.
     */
    template <class SpectrumType>
    float GlossySpecular<SpectrumType>::getExp() const
    {
        return exp_;
    }



    /*! \brief Set specular texture.
     *  \param[in] texture Specular texture.
     */
    template <class SpectrumType>
    void GlossySpecular<SpectrumType>::setTexture(std::shared_ptr<Texture<SpectrumType> > texture)
    {
        texture_ = texture;
    }
    
    
    
    /*! \brief Set sampling object.
     *  \param[in] sampler Sampling object.
     */
    template <class SpectrumType>
    void GlossySpecular<SpectrumType>::setSampler(std::shared_ptr<Sampler> sampler)
    {
        sampler_ = sampler;
    }
    
    
    
    /*! \brief Get BRDF for glossy specular reflection.
     *  \param[in] shadingData Shading object.
     *  \param[in] incoming Direction of incoming ray.
     *  \param[in] outgoing Direction of outgoing ray.
     *  \return Returns reflected color.
     */
    template <class SpectrumType>
    SpectrumType GlossySpecular<SpectrumType>::f(const ShadingData<SpectrumType>& shadingData, const Vector3D& incoming, const Vector3D& outgoing) const
    {
        Vector3D r = (-incoming + 2.0 * shadingData.normal_ * (shadingData.normal_ * incoming));
        
        float dotProduct = static_cast<float>(r * outgoing);
        
        if (dotProduct > 0.0)
        {
            SpectrumType color(texture_->getColor(shadingData));
            color = color * ks_;
            color = color * pow(dotProduct, exp_);
            return texture_->getColor(shadingData) * ks_ * pow(dotProduct, exp_);
        }
        else
        {
            return SpectrumType();
        }
    }
    
    
    
    /*! \brief Bidirectional reflectance distribution function, which is the constant of
     *  proportionality between reflected radiance and irradiance.
     *  \param[in] shadingData Shading data of ray object intersection.
     *  \param[out] incoming Incoming ray direction.
     *  \param[in] outgoing Outgoing ray direction.
     *  \param[out] pdf Probability density function for ray samples.
     *  \return Return Bidirectional reflectance distribution function applied to irradiance.
     *
     *  The brdf is calculated by sampling the directions of incoming rays.
     */
    template <class SpectrumType>
    SpectrumType GlossySpecular<SpectrumType>::fSample(const ShadingData<SpectrumType>& shadingData, Vector3D& incoming, const Vector3D& outgoing, float& pdf) const
    {
        float dotProduct = static_cast<float>(shadingData.normal_ * outgoing);
        Vector3D r = (-outgoing + 2.0 * shadingData.normal_ * dotProduct);
        
        Vector3D w = r;
        Vector3D u = Vector3D(0.00424, 1.0, 0.00764).crossProduct(w);
        u.normalize();
        Vector3D v = u.crossProduct(w);
        
        Point3D samplePoint = sampler_->sampleHemisphere();
        incoming = samplePoint.x_ * u + samplePoint.y_ * v + samplePoint.z_ * w; // reflected ray direction
        
        if (shadingData.normal_ * incoming < 0.0) // reflected ray is below surface
        {
            incoming = -samplePoint.x_ * u - samplePoint.y_ * v + samplePoint.z_ * w;
        }
        
        float phongLobe = static_cast<float>(pow(r * incoming, exp_));
        pdf = phongLobe * static_cast<float>(shadingData.normal_ * incoming);
        
        return texture_->getColor(shadingData) * ks_ * phongLobe;
    }
    
    
    
    /*! \brief Get bihemispherical reflectance.
     *  \param[in] shadingData Shading object.
     *  \param[in] outgoing Direction of outgoing ray.
     *  \return Returns reflected color.
     */
    template <class SpectrumType>
    SpectrumType GlossySpecular<SpectrumType>::rho(const ShadingData<SpectrumType>& shadingData, const Vector3D& outgoing) const
    {
        return texture_->getColor(shadingData) * static_cast<float>(ks_);
    }
        
} // end of namespace iiit

#endif // __GLOSSYSPECULAR_HPP__