// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Brdf.hpp
 *  \author Thomas Nuernberg
 *  \date 22.04.2015
 *  \brief Definition of base class Brdf, representing bidirectional reflectance distribution
 *  functions (BRDF)).
 */

#ifndef __BRDF_HPP__
#define __BRDF_HPP__

#include <memory>

#include "Sampler/Sampler.hpp"
#include "Utilities/Normal.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Vector3D.hpp"



namespace iiit
{

    /*! \brief Abstract base class for bidirectional reflectance distribution functions (BRDF).
     */
    template <class SpectrumType>
    class Brdf
    {
    public:
        /*! \brief Bidirectional reflectance distribution function, which is the constant of
         *  proportionality between reflected radiance and irradiance.
         *  \param[in] shadingData Shading data of ray object intersection.
         *  \param[in] incoming Incoming ray direction.
         *  \param[in] outgoing Outgoing ray direction.
         *  \return Return Bidirectional reflectance distribution function applied to irradiance.
         */
        virtual SpectrumType f(const ShadingData<SpectrumType>& shadingData, const Vector3D& incoming, const Vector3D& outgoing) const = 0;

        /*! \brief Bidirectional reflectance distribution function, which is the constant of
         *  proportionality between reflected radiance and irradiance.
         *  \param[in] shadingData Shading data of ray object intersection.
         *  \param[in] incoming Incoming ray direction.
         *  \param[in] outgoing Outgoing ray direction.
         *  \return Return Bidirectional reflectance distribution function applied to irradiance.
         *
         *  The brdf is calculated by sampling the directions of incoming rays.
         */
        virtual SpectrumType fSample(const ShadingData<SpectrumType>& shadingData, Vector3D& incoming, const Vector3D& outgoing) const = 0;

        /*! \brief Bihemispherical reflectance.
         *  \param[in] shadingData Shading data of ray object intersection.
         *  \param[in] outgoing Outgoing ray direction.
         *  \return Returns resulting bihemispherical reflectance as color.
         */
        virtual SpectrumType rho(const ShadingData<SpectrumType>& shadingData, const Vector3D& outgoing) const = 0;
        
        /*! \brief Set sampling object.
         *  \param[in] sampler Sampling object.
         */
        virtual void setSampler(std::shared_ptr<Sampler> sampler) {};
        
    protected:
        std::shared_ptr<Sampler> sampler_; ///< Sampler object for sampling the BRDF.
        Normal normal_; ///< Normal vector of surface.
    };
        
} // end of namespace iiit

#endif // __BRDF_HPP__