// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file PerfectSpecular.hpp
 *  \author Thomas Nuernberg
 *  \date 12.06.2015
 *  \brief Definition of class PerfectSpecular, a BRDF for perfect specular reflection functions
 *  (BRDF).
 */

#ifndef __PERFECTSPECULAR_HPP__
#define __PERFECTSPECULAR_HPP__

#include <math.h>

#include "Brdf.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/Constants.h"
#include "Textures/Texture.hpp"
#include "Textures/ConstantColor.hpp"



namespace iiit
{
    /*! \brief Class of the bidirectional reflectance distribution function (BRDF) of perfect
     *  specular reflection.
     */
    template <class SpectrumType>
    class PerfectSpecular : public Brdf<SpectrumType>
    {
    public:
        PerfectSpecular();
        PerfectSpecular(float k, SpectrumType cr);
        ~PerfectSpecular();

        void setK(float k);
        float getK() const;
        void setCr(const SpectrumType& cr);

        SpectrumType f(const ShadingData<SpectrumType>& shadingData, const Vector3D& incoming, const Vector3D& outgoing) const {return SpectrumType(0.f);};
        SpectrumType fSample(const ShadingData<SpectrumType>& shadingData, Vector3D& incoming, const Vector3D& outgoing) const;
        SpectrumType fSample(const ShadingData<SpectrumType>& shadingData, Vector3D& incoming, const Vector3D& outgoing, float& pdf) const;
        SpectrumType rho(const ShadingData<SpectrumType>& shadingData, const Vector3D& outgoing) const {return SpectrumType(0.f);};

    private:
        float k_; ///< Specular reflection coefficient.
        SpectrumType cr_; ///< Specular color.
    };

    
    
    /*! \brief Default constructor.
     *
     *  Sets both reflection coefficient and exponent to 1 and assigns a constant white texture.
     */
    template <class SpectrumType>
    PerfectSpecular<SpectrumType>::PerfectSpecular()
        : k_(1.0)
        , cr_(1.0)
    {
    }
    
    
    
    /*! \brief Constructor with arguments.
     *  \param[in] k Specular reflection coefficient.
     *  \param[in] cr Specular color.
     */
    template <class SpectrumType>
    PerfectSpecular<SpectrumType>::PerfectSpecular(float k, SpectrumType cr)
        : k_(k)
        , cr_(cr)
    {
    }
    
    
    
    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    PerfectSpecular<SpectrumType>::~PerfectSpecular()
    {
    }
    

    
    /*! \brief Set specular reflection coefficient.
     *  \param[in] k Specular reflection coefficient.
     */
    template <class SpectrumType>
    void PerfectSpecular<SpectrumType>::setK(float k)
    {
        k_ = k;
    }



    /*! \brief Get specular reflection coefficient.
     *  \return Specular reflection coefficient.
     */
    template <class SpectrumType>
    float PerfectSpecular<SpectrumType>::getK() const
    {
        return k_;
    }
    
    
    
    /*! \brief Set specular color.
     *  \param[in] cs Specular color.
     */
    template <class SpectrumType>
    void PerfectSpecular<SpectrumType>::setCr(const SpectrumType& cr)
    {
        cr_ = cr;
        //texture_ = std::shared_ptr<Texture<SpectrumType> >(new ConstantColor<SpectrumType>(cs));
    }



//    /*! \brief Set specular texture.
//     *  \param[in] texture Specular texture.
//     */
//    template <class SpectrumType>
//    void GlossySpecular<SpectrumType>::setTexture(std::shared_ptr<Texture<SpectrumType> > texture)
//    {
//        texture_ = texture;
//    }
    
    
    /*! \brief Get BRDF for perfect specular reflection.
     *  \param[in] shadingData Shading object.
     *  \param[in] incoming Direction of incoming ray.
     *  \param[in] outgoing Direction of outgoing ray.
     *  \return Returns reflected color.
     */
    template <class SpectrumType>
    SpectrumType PerfectSpecular<SpectrumType>::fSample(const ShadingData<SpectrumType>& shadingData, Vector3D& incoming, const Vector3D& outgoing) const
    {
        Vector3D normal(shadingData.normal_);
        
        float dotProduct = static_cast<float>(normal * outgoing);
        
        incoming = -outgoing + normal * 2.0 * dotProduct;
        
        return cr_ * k_ / static_cast<float>(normal * incoming);
    }
    
    
    
    /*! \brief Get sampeld BRDF for perfect specular reflection.
     *  \param[in] shadingData Shading object.
     *  \param[in] incoming Direction of incoming ray.
     *  \param[in] outgoing Direction of outgoing ray.
     *  \param[in] pdf Probability density of incoming ray.
     *  \return Returns reflected color.
     */
    template <class SpectrumType>
    SpectrumType PerfectSpecular<SpectrumType>::fSample(const ShadingData<SpectrumType>& shadingData, Vector3D& incoming, const Vector3D& outgoing, float& pdf) const
    {
        Vector3D normal(shadingData.normal_);
        float dotProduct = static_cast<float>(normal * outgoing);
        incoming = -outgoing + normal * 2.0 * dotProduct;
        pdf = static_cast<float>(normal * incoming);
        
        return cr_ * k_ / static_cast<float>(normal * incoming);
    }
    
} // end of namespace iiit

#endif // __PERFECTSPECULAR_HPP__