// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Lambertian.hpp
 *  \author Thomas Nuernberg
 *  \date 23.04.2015
 *  \brief Definition of class Lambertian, a BRDF for Lambertian reflection.
 *  functions (BRDF)).
 */

#ifndef __LAMBERTIAN_HPP__
#define __LAMBERTIAN_HPP__

#include "Brdf.hpp"
#include "Utilities/ShadingData.hpp"
#include "Utilities/Vector3D.hpp"
#include "Utilities/Constants.h"
#include "Textures/Texture.hpp"
#include "Textures/ConstantColor.hpp"



namespace iiit
{
    
    /*! \brief Class of the bidirectional reflectance distribution function (BRDF) of Lambertian
     *  reflection.
     */
    template <class SpectrumType>
    class Lambertian : public Brdf<SpectrumType>
    {
    public:
        Lambertian();
        Lambertian(float kd, std::shared_ptr<Texture<SpectrumType> > texture);
        ~Lambertian();

        void setKd(float kd);
        float getKd() const;
        void setCd(const SpectrumType& cd);
        void setTexture(std::shared_ptr<Texture<SpectrumType> > texture);


        SpectrumType f(const ShadingData<SpectrumType>& shadingData, const Vector3D& incoming, const Vector3D& outgoing) const;
        SpectrumType fSample(const ShadingData<SpectrumType>& shadingData, Vector3D& incoming, const Vector3D& outgoing) const {return SpectrumType(0.f);}
        SpectrumType fSample(const ShadingData<SpectrumType>& shadingData, Vector3D& incoming, const Vector3D& outgoing, float& pdf) const;
        SpectrumType rho(const ShadingData<SpectrumType>& shadingData, const Vector3D& outgoing) const;
        
        virtual void setSampler(std::shared_ptr<Sampler> sampler);

    private:
        float kd_; ///< Diffuse reflection coefficient.
        SpectrumType cd_; ///< Diffuse color.
        std::shared_ptr<Texture<SpectrumType> > texture_; ///< Texture assigned.
    };

    
    
    /*! \brief Default constructor.
     */
    template <class SpectrumType>
    Lambertian<SpectrumType>::Lambertian()
        : kd_(1.0)
    {
        texture_ = std::shared_ptr<ConstantColor<SpectrumType> >(new ConstantColor<SpectrumType>(SpectrumType(1.f)));
    }
    
    
    
    /*! \brief Constructor with arguments.
     */
    template <class SpectrumType>
    Lambertian<SpectrumType>::Lambertian(float kd, std::shared_ptr<Texture<SpectrumType> > texture)
        : kd_(kd)
        , texture_(texture)
    {
    }
    
    
    
    /*! \brief Default destructor.
     */
    template <class SpectrumType>
    Lambertian<SpectrumType>::~Lambertian()
    {
    }
    
    
    
    /*! \brief Set diffuse reflection coefficient.
     *  \param[in] kd Diffuse reflection coefficient.
     */
    template <class SpectrumType>
    inline void Lambertian<SpectrumType>::setKd(float kd)
    {
        kd_ = kd;
    }
    
    
    
    /*! \brief Get diffuse reflection coefficient.
     *  \return Diffuse reflection coefficient.
     */
    template <class SpectrumType>
    inline float Lambertian<SpectrumType>::getKd() const
    {
        return kd_;
    }
    
    
    
    /*! \brief Set diffuse color.
     *  \param[in] cd Diffuse color.
     */
    template <class SpectrumType>
    void Lambertian<SpectrumType>::setCd(const SpectrumType& cd)
    {
        texture_ = std::shared_ptr<Texture<SpectrumType> >(new ConstantColor<SpectrumType>(cd));
    }
    
    

    /*! \brief Set diffuse texture.
     *  \param[in] texture Diffuse texture.
     */
    template <class SpectrumType>
    inline void Lambertian<SpectrumType>::setTexture(std::shared_ptr<Texture<SpectrumType> > texture)
    {
        texture_ = texture;
    }
    
    
    
    /*! \brief Get BRDF for lambertian reflection.
     *  \param[in] shadingData Shading object.
     *  \param[in] incoming Direction of incoming ray.
     *  \param[in] outgoing Direction of outgoing ray.
     *  \return Returns reflected color.
     */
    template <class SpectrumType>
    SpectrumType Lambertian<SpectrumType>::f(const ShadingData<SpectrumType>& shadingData, const Vector3D& incoming, const Vector3D& outgoing) const
    {
        return texture_->getColor(shadingData) * static_cast<float>(kd_ * INV_PI);
    }
    
    
    
    /*! \brief Get sampled BRDF for lambertian reflection.
     *  \param[in] shadingData Shading object.
     *  \param[in] incoming Direction of incoming ray.
     *  \param[in] outgoing Direction of outgoing ray.
     *  \param[in] pdf Probability density of incoming ray.
     *  \return Returns reflected color.
     */
    template <class SpectrumType>
    SpectrumType Lambertian<SpectrumType>::fSample(const ShadingData<SpectrumType>& shadingData, Vector3D& incoming, const Vector3D& outgoing, float& pdf) const
    {
        Vector3D w = shadingData.normal_;
        Vector3D v = w.crossProduct(Vector3D(0.0072, 1.0, 0.0034)); // jitter up vector in case normal is vertical
        v.normalize();
        Vector3D u = v.crossProduct(w);
        
        Point3D samplePoint = this->sampler_->sampleHemisphere();
        incoming = samplePoint.x_ * u + samplePoint.y_ * v + samplePoint.z_ * w;
        incoming.normalize();
        pdf = static_cast<float>(shadingData.normal_ * incoming * INV_PI);
        
        return texture_->getColor(shadingData) * static_cast<float>(kd_ * INV_PI);
    }
    
    
    
    /*! \brief Get bihemispherical reflectance.
     *  \param[in] shadingData Shading object.
     *  \param[in] outgoing Direction of outgoing ray.
     *  \return Returns reflected color.
     */
    template <class SpectrumType>
    SpectrumType Lambertian<SpectrumType>::rho(const ShadingData<SpectrumType>& shadingData, const Vector3D& outgoing) const
    {
        return texture_->getColor(shadingData) * static_cast<float>(kd_);
    }
    
    
    
    /*! \brief Set sampling object.
     *  \param[in] sampler Sampling object.
     */
    template <class SpectrumType>
    void Lambertian<SpectrumType>::setSampler(std::shared_ptr<Sampler> sampler)
    {
        this->sampler_ = sampler;
    }
    
} // end of namespace iiit

#endif // __LAMBERTIAN_HPP__