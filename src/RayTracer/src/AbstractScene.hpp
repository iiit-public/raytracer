// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file AbstractScene.hpp
 *  \author Thomas Nuernberg
 *  \date 16.12.2015
 *  \brief Definition of class AbstractScene.
 *
 *  Definition of class AbstractScene, serving as prototype for scene factory.
 */

#ifndef __ABSTRACTSCENE_HPP__
#define __ABSTRACTSCENE_HPP__

#include <vector>
#include <memory>
#include <string>

#include "Cameras/ViewPlane.hpp"
#include "Cameras/Camera.hpp"



namespace iiit
{

    /*! \brief Abstract base class for scenes, defining the interface used by the rayTracer class.
     */
    class AbstractScene
    {
    public:
        /*! \brief Clone scene object.
         *  \return Reference to cloned scene object.
         */
        virtual std::shared_ptr<AbstractScene> clone() const = 0;
        
        /*! \brief Render scene viewed by camera.
         *  \param[in,out] image Rendered image.
         */
        virtual void renderScene(std::shared_ptr<cimg_library::CImg<float> > image) const = 0;

        virtual int getNumChannels() const = 0;
        virtual std::string getSensorName() const = 0;
        ViewPlane viewPlane_;  ///< ViewPlane object defining the image plane.
        
    };
        
} // end of namespace iiit

#endif // __ABSTRACTSCENE_HPP__
