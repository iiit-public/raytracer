// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file FileLogTarget.hpp
 *  \author Thomas Nuernberg
 *  \date 27.11.2015
 *  \brief File log target.
 *
 *  Definition file log target to log data in file.
 */

#ifndef __FILELOGTARGET_HPP__
#define __FILELOGTARGET_HPP__

#include <iostream>
#include <sstream>
#include <string>
#include <cstdio>


namespace iiit
{

    /*! \brief Class of a file log target.
     */
    class FileLogTarget
    {
    public:
        static FILE*& stream();
        static void log(const std::string& msg);
    };



    /*! \brief Sets the output stream to the default stderr stream and return a reference to it.
     *  \return Reference to output stream.
     */
    inline FILE*& FileLogTarget::stream()
    {
        static FILE* pStream = stderr;
        return pStream;
    }

} // end of namespace

#endif // __FILELOGTARGET_HPP__