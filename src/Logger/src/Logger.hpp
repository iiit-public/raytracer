// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file Logger.hpp
 *  \author Thomas Nuernberg
 *  \date 26.11.2015
 *  \brief Logging framework.
 *
 *  Definition of logging framework.
 */

#ifndef __LOGGER_HPP__
#define __LOGGER_HPP__

#include <iostream>
#include <sstream>
#include <string>
#include <ctime>

#include "FileLogTarget.hpp"



namespace iiit
{

    /*! This enum defines the logging levels hierarchy.
     */
    enum LogLevel
    {
        LogNone = 0, ///< Level used for no logging outputs.
        LogError = 1, ///< Level for errors.
        LogWarning = 2, ///< Level for warnings.
        LogInfo = 3, ///< Level for additional information.
        LogDebug = 4, ///< Level for debug information.
        LogDebugImage = 5, ///< Level for debug information at image level.
        LogDebugChunk = 6, ///< Level for debug information at chunk level.
        LogDebugPixel = 7, ///< Level for debug information at pixel level.
        LogDebugRay = 8, ///< Level for debug information at ray level.
    };



    #ifndef LOG_MAX_LEVEL
        /*! \brief Maximum logging level.
         */
        #define LOG_MAX_LEVEL 8
    #endif



    #ifndef LOG_SHORT
        /*! \brief Flag indicating whether to use short logging outputs.
         */
        #define LOG_SHORT 0
    #endif



    #if LOG_SHORT
        /*! \brief Helper macro to switch between short and detailed logging.
         *  \param[in] msg Message to log.
         */
        #define GET_LOG(msg, ...) msg __VA_ARGS__
    #else
        /*! \brief Helper macro to switch between short and detailed logging.
         *  \param[in] msg Message to log.
         */
        #define GET_LOG(msg, ...) __FILE__ << "(" << __LINE__ << "): " << msg __VA_ARGS__
    #endif



    #if (LOG_MAX_LEVEL != 0)
        /*! \brief Macro for logging a message of the given log level.
         *  \param[in] level Log level of message.
         *  \param[in] msg Message to log.
         */
        #define LOG(level, msg, ...) \
            if (level > LOG_MAX_LEVEL) ;\
            else if (level > FileLogger::reportingLevel() || !FileLogTarget::stream()) ; \
            else FileLogger().get(level)  << GET_LOG(msg __VA_ARGS__);
    #else
        /*! \brief Disabled macro for logging a message of the given log level.
         *  \param[in] level Log level of message.
         *  \param[in] msg Message to log.
         */
        #define LOG(...) do {} while (0)
    #endif



    /*! \brief Class for logging text data.
     *  \tparam LogTarget Target of logging information.
     *
     *  This class is used for taking logging information and passing it to a logging target. There are
     *  different logging levels supported to dynamically choose, what information should be logged. The
     *  levels are defined in the LogLevel enumeration.
     *
     *  Set the level of information to be logged with
     *
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
     *  FileLogger::reportingLevel() = LogLevel::<Level>;
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *
     *  The mechanism for logging data to a file can be accessed via the `LOG()` macro.
     *  
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
     *  LOG(<level>, <message>);
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *
     *  The class internally uses std::ostringstream, so the `<<`-operator to log multiple strings can
     *  be used.
     *
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
     *  LOG(<level>, <message1> << <message2>);
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *
     *  Only data of an equal or lower level of the currently defined reporting level will be logged.
     *
     *  The target file can be specified with the LogTarget class:
     *
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
     *  FILE* pFile = fopen("<filename>", "w"); // write mode -> existing log file gets replaced
     *  FileLogTarget::stream() = pFile;
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *
     *  The behavior of the `LOG()`-macro depends on two preprocessor variables. Use the `LOG_MAX_LEVEL`
     *  variable to to specify a global log level limit (Compile with option
     *  `-DLOG_MAX_LEVEL=<number>`). Set this variable to 0 to disable logging completely. Because of
     *  the implementation with preprocessor macros, all logging statements are omitted. The variable
     *  `LOG_SHORT` is used to shorten the logging output in order to decrease file sizes and to
     *  increase the performance (Compile with option `-DLOG_SHORT=<0/1>`).
     *
     *  \b Example:
     *
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~{.cpp}
     *  // setup logging
     *  if (LOG_MAX_LEVEL)
     *  {
     *      FILE* pFile = fopen("myApplication.log", "w"); // write mode -> existing log file gets replaced
     *      FileLogTarget::stream() = pFile;
     *      FileLogger::reportingLevel() = LogLevel(LogLevel::LogDebug);
     *  }
     *  else
     *  {
     *      FileLogger::reportingLevel() = LogLevel(LogLevel::LogNone);
     *  }
     *  ...
     *  int a = 10;
     *  int b = a*2
     *
     *  // Log message
     *  LOG(LogLevel::LogDebug, "This is a debug message" << a << "," << b);
     *  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     */
    template <typename LogTarget>
    class Logger
    {
    public:
        /*! \brief Default constructor.
         */
        Logger() {};
        virtual ~Logger();
        std::ostringstream& get(LogLevel level = LogLevel::LogInfo);

        static LogLevel& reportingLevel();

    protected:
        std::ostringstream os; ///< Output string stream to pass logging data to.

    private:
        /*! \brief Copy constructor.
         *
         *  Intentionally left empty.
         */
        Logger(const Logger&) {};

        /*! \brief Assignment constructor.
         *
         *  Intentionally left empty.
         */
        Logger& operator=(const Logger&) {};
        const std::string currentDateTime();
        std::string levelToString(LogLevel level) const;
    private:

    };



    /*! \brief Abbreviation of Logger class with FileLogTarget.
     */
    typedef Logger<FileLogTarget> FileLogger;



    /*! \brief Get string stream object used for logging
     *  \param[in] level Logging level of messages to be logged.
     *  \return Returns a reference to the string stream to the log target.
     *
     *  The function passes additional logging information for better formating to the string stream and
     *  return a reference to it.
     */
    template <typename LogTarget>
    std::ostringstream& Logger<LogTarget>::get(LogLevel level)
    {
        #if !LOG_SHORT
            os << currentDateTime();
        #endif
        os << " " << levelToString(level) << ": ";
        os << std::string(level < LogLevel::LogDebugImage ? 0 : level - LogLevel::LogDebugImage, '\t');
        return os;
    }



    /*! \brief Destructor of Logger that adds an end line and flushes the buffered data.
     */
    template <typename LogTarget>
    Logger<LogTarget>::~Logger()
    {
        os << std::endl;
        LogTarget::log(os.str());
    }



    /*! \brief Sets the current logging level to the lowest level and return a reference to it.
     *  \return Reference to current logging level.
     */
    template <typename LogTarget>
    LogLevel& Logger<LogTarget>::reportingLevel()
    {
        static LogLevel reportingLevel = LogLevel::LogDebugRay;
        return reportingLevel;
    }



    /*! \brief Returns string with current date and time.
     *  \return Current date and time.
     */
    template <typename LogTarget>
    const std::string Logger<LogTarget>::currentDateTime()
    {
        time_t     now = time(0);
        struct tm  tstruct;
        char       buf[80];
        tstruct = *localtime(&now);
        strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

        return buf;
    }



    /*! \brief Returns strings with the names of the different log levels.
     *  \param[in] level Log level.
     *  \return String with log level name.
     */
    template <typename LogTarget>
    std::string Logger<LogTarget>::levelToString(LogLevel level) const
    {
        static const char* const buffer[] = {"NONE", "ERROR", "WARNING", "INFO", "DEBUG", "DEBUG_IMAGE", "DEBUG_CHUNK", "DEBUG_PIXEL", "DEBUG_RAY"};
        return buffer[level];
    }

} // end of namespace iiit

#endif // __LOGGER_HPP__
