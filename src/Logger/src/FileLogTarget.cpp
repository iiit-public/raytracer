// Copyright (C) 2018-2019  The IIIT-RayTracer Authors
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.


/*! \file FileLogTarget.hpp
 *  \author Thomas Nuernberg
 *  \date 27.11.2015
 *  \brief File log target.
 *
 *  Implementation file log target to log data in file.
 */

#include <FileLogTarget.hpp>



namespace iiit
{

    /*! \brief Function to output the given message to the file.
     *  \param[in] msg Message to log.
     */
    void FileLogTarget::log(const std::string& msg)
    {
        FILE* pStream = stream();
        if (!pStream)
        {
            return;
        }

        fprintf(pStream, "%s", msg.c_str());
        fflush(pStream);
    }

} // end of namespace iiit