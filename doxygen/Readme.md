IIIT %RayTracer                         {#mainpage}
============

Quicklinks
----------

[Project Repository](https://gitlab.com/iiit-public/raytracer)<BR>
\ref json_interface "JSON Interface"<BR>
\ref installation_notes "Installation Notes"<BR>
\ref build_options "List of build Options"

Introduction
------------

IIIT %RayTracer is a software tool to simulate the image capture process of a computational camera.
The simulation is based on ray tracing algorithms to render a synthetic scene.

The project consists of the main algorithm and interchangeable frontends providing user interfaces.
The project can be run with any frontend selected. To build the frontend with graphical user
interface (GUI), the [Qt Library](http://qt-project.org/) is required.

### Overview

The following diagram shows an overview of the program flow and the involved modules and class
hierarchies.

\htmlonly
<div class="center">
<img src="structure.png" usemap="#structure_map" alt=""/>
<map id="structure_map" name="structure_map">
<area href="singletoniiit_1_1_scene.html" title="Class for storing scene data." alt="Scene" shape="rect" coords="602,13,642,27"/>
<area href="classiiit_1_1_ray_tracer.html" title="Main ray tracing class." alt="RayTracer" shape="rect" coords="144,81,211,98"/>
<area href="classiiit_1_1_camera.html" title="Abstract class representing cameras." alt="Camera" shape="rect" coords="415,81,466,98"/>
<area href="classiiit_1_1_tracer.html" title="Abstract base class for calculating interactions of rays with the scene." alt="Tracer" shape="rect" coords="684,81,726,98"/>
<area href="classiiit_1_1_geometric_object.html" title="Abstract base class for geometric objects." alt="GeometricObject" shape="rect" coords="849,74,915,107"/>
<area href="classiiit_1_1_basic_frontend.html" title="Derived class for a basic frontend." alt="BasicFrontend" shape="rect" coords="146,235,204,251"/>
<area href="classiiit_1_1_texture.html" title="Abstract class for textures." alt="Texture" shape="rect" coords="329,235,375,251"/>
<area href="classiiit_1_1_brdf.html" title="Abstract base class for bidirectional reflectance distribution functions (BRDF)." alt="BRDF" shape="rect" coords="508,235,546,251"/>
<area href="classiiit_1_1_material.html" title="Abstract base class for materials." alt="Material" shape="rect" coords="679,235,730,251"/>
<area href="classiiit_1_1_light.html" title="Abstract base class for lights." alt="Light" shape="rect" coords="867,235,900,251"/>
<area href="singletoniiit_1_1_scene.html" title="Class for storing scene data." alt="Scene" shape="rect" coords="116,163,156,176"/>
<area href="classiiit_1_1_ray.html" title="Class of ray." alt="Ray" shape="rect" coords="557,57,584,75"/>
<area href="classiiit_1_1_ray.html" title="Class of ray." alt="Ray" shape="rect" coords="781,57,808,75"/>
<area href="singletoniiit_1_1_shading_data.html" title="Class holds data used for shading." alt="ShadingData" shape="rect" coords="770,107,822,137"/>
<area href="singletoniiit_1_1_shading_data.html" title="Class holds data used for shading." alt="ShadingData" shape="rect" coords="726,156,778,186"/>
<area href="singletoniiit_1_1_shading_data.html" title="Class holds data used for shading." alt="ShadingData" shape="rect" coords="591,197,642,227"/>
<area href="singletoniiit_1_1_shading_data.html" title="Class holds data used for shading." alt="ShadingData" shape="rect" coords="415,197,467,227"/>
<!--<area href="class_r_g_b_color.html" title="Class represents RGB color." alt="RGBColor" shape="rect" coords="408,260,473,274"/>-->
<!--<area href="class_r_g_b_color.html" title="Class represents RGB color." alt="RGBColor" shape="rect" coords="585,260,650,274"/>-->
<area href="singletoniiit_1_1_shading_data.html" title="Class holds data used for shading." alt="ShadingData" shape="rect" coords="767,260,820,290"/>
<area href="classiiit_1_1_ray.html" title="Class of ray." alt="Ray" shape="rect" coords="901,164,928,178"/>
<!--<area href="class_r_g_b_color.html" title="Class represents RGB color." alt="RGBColor" shape="rect" coords="761,210,826,224"/>-->
<!--<area href="class_r_g_b_color.html" title="Class represents RGB color." alt="RGBColor" shape="rect" coords="621,163,686,176"/>-->
<!--<area href="class_r_g_b_color.html" title="Class represents RGB color." alt="RGBColor" shape="rect" coords="538,107,603,121"/>-->
<area href="structcimg__library__suffixed_1_1_c_img.html" title="Class representing an image (up to 4 dimensions wide), each pixel being of type T." alt="CImg" shape="rect" coords="237,107,330,122"/>
<area href="structcimg__library__suffixed_1_1_c_img.html" title="Class representing an image (up to 4 dimensions wide), each pixel being of type T." alt="CImg" shape="rect" coords="200,160,233,178"/>
</map>
</div>
\endhtmlonly

The scene to render is defined by a [JSON](http://www.json.org) description file, that is parsed by
the user frontend. With the parsed data, a Scene object is constructed and passed to the RayTracer
class. The RayTracer class calls the Camera of the Scene object to render the image.

Inside the Camera object, Ray objects are constructed according to the optical characteristics of
the camera. The Tracer object handles the simulation of the interaction of the Ray with the
environment, like ray-object-intersections, reflection and shading. The resulting light contribution
of the Ray is passed back to the Camera to form the image. Once the rendering process has finished,
the image is passed back to the user frontend.

#### Further Information

For further information about the [JSON](http://www.json.org) user interface, please consult to the
\ref json_interface "JSON Interface" page.

Installation
------------

The project uses the [CMake](http://www.cmake.org/) (version 2.8.11 or newer) cross-platform build
system. To generate a project/makefile for the compiler environment of your choice, run the
following commands from the root folder to build the default project (or use the CMake GUI).

    $ mkdir build
    $ cd build
    $ cmake ..

Use the `-G` option to use a different generator than the system default.

To include the GUI frontend in the build, use the `-D` option as follows:

    $ cmake -DUSE_GUI_FRONTEND=ON ..

Alternatively you can point the CMake GUI tool to the root `CMakeList.txt` file and generate
platform specific build project or IDE workspace.

For more advanced use or questions about CMake please read
[http://www.cmake.org/Wiki/CMake_FAQ](http://www.cmake.org/Wiki/CMake_FAQ).

### Visual Studio

After building the project with CMake, the Visual Studio Solution contains several projects. The
following up to three projects are created by default by CMake:
1. ALL_BUILD: This project can be used to build all other projects of the solution. Use this if you
do not want to build every single project.
2. ZERO_BUILD: This projects checks, if the `CMakeList.txt` files have changed since the last build
of the project and invokes a rebuild of the solution with CMake. Note that this will overwrite any
changes of the project that have been done outside of the `CMakeList.txt` files, e. g. changes done
with Visual Studio.
3. RUN_TESTS: The [googletest](http://code.google.com/p/googletest/) library is required for this
project. Therefore it is only included in the solution if the library has been found on the system.
If necessary, specify the root directory of the library by using the CMake option

    -DGTEST_ROOT=<...>
The projects invokes the execution of all unit test projects of the solution.

The next up to five projects contain the executable applications:
1. basicfrontend: This projects contains a basic frontend to run the ray tracer from console. It
builds an executable. Therefore this project is suited as startup project. To select this
project as startup project, right click the project in the Solution Explorer and select "Set as
StartUp Project". The project only contains the `main.cpp` file, the actual frontend is implemented
in the project basicfronentdlib.
2. guifrontend: This projects builds a GUI frontend. It depends on the
[Qt Library](http://qt-project.org/)
and can only be included in the solution if the libraries are found on the system. To select
this project as startup project, right click the project in the Solution Explorer and select "Set as
StartUp Project". The project only contains the `main.cpp` file, the actual frontend is implemented
in the project guifronentdlib.
3. raytracer_test: This projects contains unit tests of the classes of the raytracer project. The
[googletest](http://code.google.com/p/googletest/) library is required for this project.
4. utilities_test: This projects contains unit tests of the classes of the utilities project. The
[googletest](http://code.google.com/p/googletest/) library is required for this project.
5. basicfrontend_test: This projects contains unit tests of the classes of the basicfronentdlib
project. The [googletest](http://code.google.com/p/googletest/) library is required for this
project.

The following projects are non-executable libraries:
1. raytracer: This is the main project of of the ray tracing algorithm. It builds a library to be
used by frontends and is therefore not executable by itself.
2. utilities: This projects builds a helper library which provides a parser to read
[JSON](http://www.json.org) syntax and methods for multithreading.
3. basicfronentdlib: Library implementing the basic frontend.
4. guifronentdlib: Library implementing the gui frontend. It depends on the
[Qt Library](http://qt-project.org/)
and can only be included in the solution if the libraries are found on the system.

The last projects are for additional resources:
1. data: This projects contains all [JSON](http://www.json.org) files found in the project
directory. Only visible with 

    -DJSON=ON
2. doc: This project is only available, if
[Doxygen](http://www.stack.nl/~dimitri/doxygen/index.html)
has been found on the system. It builds the documentation of the %RayTracer project.


### Linux

After building the project with CMake, invoke `make` to compile the program (variables in `<>` are
optional):

    $ mkdir build
    $ cd build
    $ cmake <-DUSE_GUI_FRONTEND=ON> <-DCMAKE_BUILD_TYPE=Debug> ..
    $ make
    
After successful compilation and provided that the required libraries were found, four executables
can be started directly from the build directory (variables in `[]` are optional):

    $ ./basicfrontend <path-to-scene-description> [-p #] [-w #] [-h #] [-o <filename>] [-i <interleave mode>] [-d #] [-n]
    $ ./guifrontend
    $ ./raytracer_test
    $ ./utilities_test
    $ ./basicfrontend_test


### CMake Options

The CMakefile provides the following options and default values to configure the build process, see below for details:

    USE_GUI_FRONTEND=ON
    NUM_SPEC_SAMPLES=30
    COVERAGE=OFF
    GCC_OPTIMIZATION=OFF
    INTEL_OPTIMIZATION=OFF
    JSON=ON

#### GUI Frontend
To build the GUI frontend, specify ``-DUSE_GUI_FRONTEND=ON`` option when running ``cmake``. See below for the dependencies of the frontend.

#### Number of spectral samples
If you wish to set a different number of spectral samples used in true spectral rendering of a scene, set the ``NUM_SPEC_SAMPLES`` option accordingly, e.g.

    $ cmake -DNUM_SPEC_SAMPLES=10 ..

**CAUTION:** If the number of samples deviates from the default value (30), you cannot build the tests! That is, you can only build the ``basicfrontend`` and ``guifrontend`` via

    $ make basicfrontend
    $ make guifrontend


#### Compiler optimization
If you want to utilize code optimization for the Release builds, set 


    $ cmake -DGCC_OPTIMIZATION=ON ..

when using the GNU C++ compiler or


    $ cmake -DINTEL_OPTIMIZATION=ON ..


if you are using an Intel C++ compiler. The Intel optimization option also utilizes the Intel MKL library which has to be installed seperately. To alter the optimization level used, change the main ``CMakeLists.txt`` file to your liking.


### Documentation
If [Doxygen](http://www.stack.nl/~dimitri/doxygen/index.html) is installed on the system, the
documentation can be built from the build directory as follows:

    $ make doc



### Dependencies

The GUI frontend of the %RayTracer project uses the [Qt Library](http://qt-project.org/) for
graphical user interfaces. To build the project, a valid installation of the %Qt Library is
required. CMake should be able to automatically locate the library and link the project.

The project uses the [googletest](http://code.google.com/p/googletest/) Google C++ Testing Framework
for testing. It is possible to build the project without the library installed, though the unit
tests won't be included.

If found on the system, CMake also adds an additional build target for the generation of the project
documentation with [Doxygen](http://www.stack.nl/~dimitri/doxygen/index.html). The local
documentation of the installed %Qt Library is automatically linked with the project's documentation.

The project also uses the [CIMG Library](http://cimg.sourceforge.net/) and the
[JsonCpp Library](https://github.com/open-source-parsers/jsoncpp), which are included in the project
source files.

The basic frontend tries to save the rendered images in the PNG file format. This requires a valid
installation of the [ImageMagick](http://www.imagemagick.org/script/index.php) suite on the system.
If the attempt to save a PNG file fails, the BMP file format is used as backup.

#### Further Information

For further information about installing the dependencies, please consult to the \ref
installation_notes "Installation Notes"

### Your first raytraced image

After building, you can start raytracing. For example, using the ``basicfrontend`` with an example scene:

    cd ~/.../raytracer/build #change to build folder
    ./basicfrontend ../data/example_scene.json -p

You can see your result in the imagefile `build/RayTracer_Image_0000.png`. It should look like this:

\image html RayTracer_Image_0000.png

Congratulations - now check out the \ref json_interface "JSON Interface" to make your own scenes.


### Tests

To see if everything works fine, run the test executables. From a command line, run:

    # Change to build folder first
    cd <some_path>/raytracer/build/

    # Run the tests
    ./raytracer_test
    ./utilities_test
    ./basicfrontend_test



For Programmers
---------------

The Project is hosted at:
[GitLab IIIT](https://gitlab.com/iiit-public/raytracer)

Throughout the project, the following coding guidelines are used:
[C++ Programming Style Guidelines](http://geosoft.no/development/cppstyle.html)

For the git commit massages, the following reference is used:
[How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/)

Smart pointers are used wherever possible instead of old style pointers. For an introduction to
smart pointers, consult [Using C++11’s Smart Pointers](http://umich.edu/~eecs381/handouts/C++11_smart_ptrs.pdf)


License
-------

Copyright (C) 2018-2019  The IIIT-RayTracer Authors
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see https://www.gnu.org/licenses/.