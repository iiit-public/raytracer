# IIIT-RayTracer
[![pipeline status](https://gitlab.com/iiit-public/raytracer/badges/master/pipeline.svg)](https://gitlab.com/iiit-public/raytracer/commits/master) [![coverage report](https://gitlab.com/iiit-public/raytracer/badges/master/coverage.svg)](https://gitlab.com/iiit-public/raytracer/commits/master)


## License and Usage

This software is licensed under the GNU GPLv3 license (see below).

If you use this software in your scientific research, please cite

    @InProceedings{Nuernberg:SPIE:2019, 
      AUTHOR = {N\"{u}rnberg, Thomas and Schambach, Maximilian and Uhlig, David and Heizmann, Michael and Puente Le\'{o}n, Fernando }, 
      TITLE = {A simulation framework for the design and evaluation of computational cameras}, 
      YEAR = {2019}, 
      BOOKTITLE = {Automated Visual Inspection and Machine Vision III}, 
      VOLUME = {11061}, 
      SERIES = {Proceedings of SPIE}, 
      ADDRESS = {Bellingham, WA}
    }



## Quicklinks

[Official Documentation](https://iiit-public.gitlab.io/raytracer).



## Introduction

IIIT-RayTracer is a software tool to simulate the image capture process of a computational camera.
The simulation is based on ray tracing algorithms to render a synthetic scene.

The project consists of the main algorithm and interchangeable frontends providing user interfaces.
The project can be run with any frontend selected. To build the frontend with graphical user
interface (GUI), the [Qt Library](http://qt-project.org/) is required.



## Installation

The project uses the [CMake](http://www.cmake.org/) (version 2.8.11 or newer) cross-platform build
system. To generate a project/makefile for the compiler environment of your choice, run the
following commands from the root folder to build the default project (or use the CMake GUI).

    $ mkdir build
    $ cd build
    $ cmake ..

Use the `-G` option to use a different generator than the system default.

To include the GUI frontend in the build, use the `-D` option as follows:

    $ cmake -DUSE_GUI_FRONTEND=ON ..

Alternatively you can point the CMake GUI tool to the root `CMakeList.txt` file and generate
platform specific build project or IDE workspace.

For more advanced use or questions about CMake please read
[http://www.cmake.org/Wiki/CMake_FAQ](http://www.cmake.org/Wiki/CMake_FAQ).

### Visual Studio

After building the project with CMake, the Visual Studio Solution contains several projects. The
following up to three projects are created by default by CMake:
1. ALL_BUILD: This project can be used to build all other projects of the solution. Use this if you
do not want to build every single project.
2. ZERO_BUILD: This projects checks, if the `CMakeList.txt` files have changed since the last build
of the project and invokes a rebuild of the solution with CMake. Note that this will overwrite any
changes of the project that have been done outside of the `CMakeList.txt` files, e. g. changes done
with Visual Studio.
3. RUN_TESTS: The [googletest](http://code.google.com/p/googletest/) library is required for this
project. Therefore it is only included in the solution if the library has been found on the system.
If necessary, specify the root directory of the library by using the CMake option

    -DGTEST_ROOT=<...>
The projects invokes the execution of all unit test projects of the solution.

The next up to five projects contain the executable applications:
1. basicfrontend: This projects contains a basic frontend to run the ray tracer from console. It
builds an executable. Therefore this project is suited as startup project. To select this
project as startup project, right click the project in the Solution Explorer and select "Set as
StartUp Project". The project only contains the `main.cpp` file, the actual frontend is implemented
in the project basicfronentdlib.
2. guifrontend: This projects builds a GUI frontend. It depends on the
[Qt Library](http://qt-project.org/)
and can only be included in the solution if the libraries are found on the system. To select
this project as startup project, right click the project in the Solution Explorer and select "Set as
StartUp Project". The project only contains the `main.cpp` file, the actual frontend is implemented
in the project guifronentdlib.
3. raytracer_test: This projects contains unit tests of the classes of the raytracer project. The
[googletest](http://code.google.com/p/googletest/) library is required for this project.
4. utilities_test: This projects contains unit tests of the classes of the utilities project. The
[googletest](http://code.google.com/p/googletest/) library is required for this project.
5. basicfrontend_test: This projects contains unit tests of the classes of the basicfronentdlib
project. The [googletest](http://code.google.com/p/googletest/) library is required for this
project.

The following projects are non-executable libraries:
1. raytracer: This is the main project of of the ray tracing algorithm. It builds a library to be
used by frontends and is therefore not executable by itself.
2. utilities: This projects builds a helper library which provides a parser to read
[JSON](http://www.json.org) syntax and methods for multithreading.
3. basicfronentdlib: Library implementing the basic frontend.
4. guifronentdlib: Library implementing the gui frontend. It depends on the
[Qt Library](http://qt-project.org/)
and can only be included in the solution if the libraries are found on the system.

The last projects are for additional resources:
1. data: This projects contains all [JSON](http://www.json.org) files found in the project
directory. Only visible with 

    -DJSON=ON
2. doc: This project is only available, if
[Doxygen](http://www.stack.nl/~dimitri/doxygen/index.html)
has been found on the system. It builds the documentation of the %RayTracer project.


### Linux

After building the project with CMake, invoke `make` to compile the program (variables in `<>` are
optional):

    $ mkdir build
    $ cd build
    $ cmake <-DUSE_GUI_FRONTEND=ON> <-DCMAKE_BUILD_TYPE=Debug> ..
    $ make
    
After successful compilation and provided that the required libraries were found, four executables
can be started directly from the build directory (variables in `[]` are optional):

    $ ./basicfrontend <path-to-scene-description> [-p #] [-w #] [-h #] [-o <filename>] [-i <interleave mode>] [-d #]  [-n]
    $ ./guifrontend
    $ ./raytracer_test
    $ ./utilities_test
    $ ./basicfrontend_test


### CMake Options

The CMakefile provides the following options and default values to configure the build process, see below for details:

    USE_GUI_FRONTEND=ON
    NUM_SPEC_SAMPLES=30
    COVERAGE=OFF
    GCC_OPTIMIZATION=OFF
    INTEL_OPTIMIZATION=OFF
    JSON=ON


#### GUI Frontend
To build the GUI frontend, specify ``-DUSE_GUI_FRONTEND=ON`` option when running ``cmake``. See below for the dependencies of the frontend.


#### Number of spectral samples
If you wish to set a different number of spectral samples used in true spectral rendering of a scene, set the ``NUM_SPEC_SAMPLES`` option accordingly, e.g.

    $ cmake -DNUM_SPEC_SAMPLES=10 ..

CAUTION: If the number of samples deviates from the default value (30), you cannot build the tests! That is, you can only build the ``basicfrontend`` and ``guifrontend`` via

    $ make basicfrontend
    $ make guifrontend


#### Compiler optimization
If you want to utilize code optimization for the Release builds, set 


    $ cmake -DGCC_OPTIMIZATION=ON ..

when using the GNU C++ compiler or


    $ cmake -DINTEL_OPTIMIZATION=ON ..


if you are using an Intel C++ compiler. The Intel optimization option also utilizes the Intel MKL library which has to be installed seperately. To alter the optimization level used, change the main ``CMakeLists.txt`` file to your liking. Please see the \ref
installation_notes "Installation Notes" for more information on how to use the Intel C++ compiler.


### Documentation
If you do not wish to use the [Official Documentation](https://iiit-public.gitlab.io/raytracer), you can build it yourself. 
If [Doxygen](http://www.stack.nl/~dimitri/doxygen/index.html) is installed on the system, the
documentation can be built from the build directory as follows:

    $ make doc


### Dependencies

The GUI frontend of the %RayTracer project uses the [Qt Library](http://qt-project.org/) for
graphical user interfaces. To build the project, a valid installation of the %Qt Library is
required. CMake should be able to automatically locate the library and link the project.

The project uses the [googletest](http://code.google.com/p/googletest/) Google C++ Testing Framework
for testing. It is possible to build the project without the library installed, though the unit
tests won't be included.

If found on the system, CMake also adds an additional build target for the generation of the project
documentation with [Doxygen](http://www.stack.nl/~dimitri/doxygen/index.html). The local
documentation of the installed %Qt Library is automatically linked with the project's documentation.

The project also uses the [CIMG Library](http://cimg.sourceforge.net/) and the
[JsonCpp Library](https://github.com/open-source-parsers/jsoncpp), which are included in the project
source files.

The basic frontend tries to save the rendered images in the PNG file format. This requires a valid
installation of the [ImageMagick](http://www.imagemagick.org/script/index.php) suite on the system.
If the attempt to save a PNG file fails, the BMP file format is used as backup.


### Further Information

For further information about installing the dependencies, please consult to the \ref
installation_notes "Installation Notes"



## For Programmers

Throughout the project, the following coding guidelines are used:
[C++ Programming Style Guidelines](http://geosoft.no/development/cppstyle.html)

For the git commit massages, the following reference is used:
[How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/)




## License
Copyright (C) 2018-2019  The IIIT-RayTracer Authors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
